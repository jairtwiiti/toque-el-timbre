<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_propertytypefeature extends MY_Model {

    const TABLE_ID = "cac_id";
    const TABLE_NAME = "categoria_caracteristica";

    protected $_categoryId;
    protected $_featureId;
    protected $_order;
    protected $_status;
    //Due to autoload is necessary to set the default data as empty string
    function __construct($categoryId = "", $featureId = "", $order = "", $status = "")
    {
        parent::__construct();
        $this->_categoryId = $categoryId;
        $this->_featureId = $featureId;
        $this->_order = $order;
        $this->_status = $status;
    }

    /**
     * Parse the current object to key => value array
     * @return array
     */
    public function toArray()
    {
        $tableAttributes = array(
                                "cac_id" => $this->_id,
                                "cat_id" => $this->_categoryId,
                                "car_id" => $this->_featureId,
                                "orden" => $this->_order,
                                "estado" => $this->_status
                                );
        return $tableAttributes;
    }

    /**
     * @param $className
     * @param $object
     * @return object
     */
    protected static function recast($className, $object)
    {
        $response =  null;
        if ($object instanceof stdClass)
        {
            if (!class_exists($className))
                throw new InvalidArgumentException(sprintf('Inexistant class %s.', $className));

            //Let's set the values to payment object using the data from stdObject
            $instance = new $className(
                    $object->cat_id,
                    $object->car_id,
                    $object->orden,
                    $object->estado

            );
            $instance->_id = $object->cac_id;
            $response = $instance;
        }
        return $response;
    }
    ################################################################################################# BEGIN - GETTERS

    public function getType()
    {
        return $this->_type;
    }

    public static function getFullDetailComposedList()
    {
        $ci=&get_instance();
        $ci->load->database();
        $sql = "
            #GET PROPERTY TYPE LIST AND ITS FEATURES DETAIL
            SELECT
                ca.cat_id,
                ca.cat_nombre,
                c.car_id,
                c.car_descripcion,
                c.car_tipo,
                GROUP_CONCAT(cd.det_valor ORDER BY cd.det_orden ) valores
            FROM
                categoria_caracteristica cc
            LEFT JOIN categoria ca on ca.cat_id = cc.cat_id
            LEFT JOIN caracteristica c on c.car_id = cc.car_id
            LEFT JOIN caracteristica_detalle cd on c.car_id = cd.det_car_id
            GROUP BY cc.cac_id
        ";
        $query = $ci->db->query($sql);
        $response = $query->result();
        return $response;
    }

    ################################################################################################# END - GETTERS

    ################################################################################################# BEGIN - SETTERS

    ################################################################################################# END - SETTERS
    public static function search($text, $limit, $offset, $orderBy = null, $orderType = 'asc', $colsArray = null)
    {
        if ($orderBy === null)
        {
            $orderBy = static::TABLE_ID;
        }
        $ci = &get_instance();
        $ci->load->database();

        $sql = 'select * from ' . static::TABLE_NAME;
        $sql .= ' where (';
        foreach ($colsArray as $var)
        {
            $sql .= ' ' . $var . ' like \'%' . $text . '%\' or ';
        }

        $sql = substr($sql, 0, -3);
        $sql .= ') order by ' . $orderBy . ' ' . $orderType . ' limit ' . $limit . ' offset ' . $offset;
        $query = $ci->db->query($sql);
        return static::recastArray(get_called_class(), $query->result());
    }
}