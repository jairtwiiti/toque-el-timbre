<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_publicacion extends CI_Model {

    private $table = 'publicacion';
    private $id = 'pub_id';

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    /**
     * Gets all results for this model but get a one result by id too.
     *
     * @param bool $id
     * @return []
     */
    function get_all($id = FALSE) {
        if ($id === FALSE) {
            $query = $this->db->get($this->table);
            return $query->result_array();
        }

        $query = $this->db->get_where($this->table, array($this->id => $id));
        return $query->row_array();
    }

    /**
     * Add new Model
     *
     * @param $data
     * @return mixed
     */
    function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    /**
     * Update Model
     *
     * @param $id
     * @param $data
     * @return mixed
     */
    function update($id, $data) {
        $this->db->where($this->id, $id);
        return $this->db->update($this->table, $data);
    }

    /**
     * Delete Model
     *
     * @param $id
     * @return mixed
     */
    function delete($id) {
        return $this->db->update($this->table, array($this->id => $id));
    }

    /**
     * Get all pay projects publishied
     *
     * @return []
     */
    function get_pay_projects() {
        $sql = "
            SELECT DISTINCT
                pub_id, inm_id, inm_nombre, inm_direccion, cat_nombre, for_descripcion, inm_precio, pub_vig_ini, for_descripcion, zon_nombre, mon_abreviado, fot_archivo, inm_seo
            FROM
                publicacion
                INNER JOIN inmueble ON inm_id = pub_inm_id
                INNER JOIN categoria ON cat_id = inm_cat_id
                INNER JOIN forma ON for_id = inm_for_id
                INNER JOIN zona ON zon_id = inm_zon_id
                INNER JOIN moneda ON mon_id = inm_mon_id
                LEFT JOIN inmueble_foto ON fot_inm_id = inm_id
                INNER JOIN pagos p ON (p.`pag_pub_id`= pub_id AND p.`pag_estado` = 'Pagado')
                INNER JOIN servicios s ON s.ser_tipo = 'Banner'
                INNER JOIN pago_servicio ps ON (ps.pag_id = p.pag_id AND ps.ser_id = s.ser_id AND CURDATE() BETWEEN ps.`fech_ini` AND ps.`fech_fin`)
            WHERE
              pub_estado = 'Aprobado'
            GROUP BY
              pub_id
            ORDER BY
              RAND()
              LIMIT 8
        ";
        return $this->db->query($sql)->result();
    }

    /**
     * Gets all features from property.
     *
     * @param bool $id
     * @return []
     */
    function get_features_property($id = FALSE) {
        $this->db->where("eca_inm_id", $id);
        $query = $this->db->get("inmueble_caracteristica");
        return $query->result_array();
    }

    function get_pay_publications($forma = "", $posicion = "Principal") {

        if(!empty($forma)){
            $forma = " AND for_descripcion = '$forma'";
        }


        $sql = "
            SELECT DISTINCT
                pub_id,
                inm_nombre,
                cat_nombre,
                for_descripcion,
                inm_precio,
                pub_vig_ini,
                mon_abreviado,
                fot_archivo,
                inm_seo,
                ciu_nombre,
                inm_id,
                inm_superficie,
                inm_latitud,
                inm_longitud,
                dep_nombre,
                usu_id,
                usu_tipo,
                usu_foto,
                usu_nombre,
                usu_empresa
            FROM
                publicacion
                INNER JOIN inmueble ON inm_id = pub_inm_id
                INNER JOIN categoria ON cat_id = inm_cat_id
                INNER JOIN forma ON for_id = inm_for_id
                INNER JOIN moneda ON mon_id = inm_mon_id
                LEFT JOIN inmueble_foto ON fot_inm_id = inm_id
                INNER JOIN pagos p ON (p.`pag_pub_id`= pub_id AND p.`pag_estado` = 'Pagado')
                INNER JOIN servicios s ON s.ser_tipo = '".$posicion."'
                INNER JOIN ciudad ON ciu_id = inm_ciu_id
                INNER JOIN departamento ON dep_id = ciu_dep_id
                INNER JOIN usuario ON (usu_id = pub_usu_id)
                INNER JOIN pago_servicio ps ON (ps.pag_id = p.pag_id AND ps.ser_id = s.ser_id AND CURDATE() BETWEEN ps.`fech_ini` AND ps.`fech_fin`)
            WHERE
                pub_estado = 'Aprobado'
                $forma
            GROUP BY
                pub_id
            ORDER BY
                RAND()
                LIMIT 8
            ";

        $publications = $this->db->query($sql)->result();
        foreach($publications as $publication){
            $publication->features = $this->get_features_property($publication->inm_id);
            $result[] = $publication;
        }
        return $result;
    }

    function get_all_publications_query($limit,$page,$config){

        $filters = $this->convert_filter_sql($config);

        $page = $page * $limit;

        $sql = "
        (
        SELECT DISTINCT bus_inmueble_3.*, 'Si' AS patrocinado FROM bus_inmueble_3
        INNER JOIN pagos p ON (p.`pag_pub_id`= pub_id AND p.`pag_estado` = 'Pagado')
        INNER JOIN servicios s ON s.ser_tipo = 'Busqueda'
        INNER JOIN pago_servicio ps ON (ps.pag_id = p.pag_id AND ps.ser_id = s.ser_id AND CURDATE() BETWEEN ps.`fech_ini` AND ps.`fech_fin`)
        ".$filters["filter"]."
        WHERE pub_estado = 'Aprobado'
        AND inm_publicado = 'Si'
        ".$filters["search"]."
        ".$filters["type"]."
        ".$filters["in"]."
        ".$filters["city"]."
        ".$filters["currency"]."
        ".$filters["price"]."
        )
        UNION
        (
        SELECT DISTINCT bus_inmueble_3.*, 'No' AS patrocinado FROM bus_inmueble_3
        ".$filters["filter"]."
        WHERE pub_estado = 'Aprobado' AND pub_vig_fin >=  CURDATE()
        AND inm_publicado = 'Si'
        AND inm_proy_id = 0
        ".$filters["search"]."
        ".$filters["type"]."
        ".$filters["in"]."
        ".$filters["city"]."
        ".$filters["currency"]."
        ".$filters["price"]."
        )
        ".$filters["sort"]."
        LIMIT $page, $limit
        ";

        $result = $this->db->query($sql)->result();
        $num = $this->db->query("SELECT FOUND_ROWS() as total")->row();
        if($num->total > 0) {
            foreach ($result as $publication) {
                $publication->features = $this->get_features_property($publication->inm_id);
                $publication->fot_archivo = $this->get_image_publication($publication->inm_id);
                $publication->images = $this->get_all_images_publication($publication->inm_id);
                $data_search[] = $publication;
            }
        }else{
            $data_search = array();
        }

        $data_result = array(
            "num_total" => $num->total,
            "results" => $data_search
        );
        return $data_result;
    }

    function convert_filter_sql($config)
    {
        $search = !empty($config->search) ? "AND (general LIKE '%" . $config->search . "%')" : '';
        $type = !empty($config->type) ? "AND cat_nombre = '" . strtoupper($config->type) . "'" : '';
        $in = !empty($config->in) ? "AND for_descripcion = '" . strtoupper($config->in) . "'" : '';
        $city = !empty($config->city) ? "AND dep_nombre = '" . $config->city . "'" : '';
        $currency = !empty($config->currency) ? "AND mon_abreviado = '" . $config->currency . "'" : '';

        if(!empty($config->sort)){
            $aux = explode("-",$config->sort);
            if($aux[1]){
                $sort = $aux[1] == "asc" ? "ASC" : "DESC";
            }

            if($aux[0] == "price"){
                $campo = "inm_precio";
            }
            if($aux[0] == "relevance"){
                $campo = "inm_proy_id";
                $sort = "DESC";
            }
            if($aux[0] == "date"){
                $campo = "pub_vig_ini";
            }
            $sort_cad = "ORDER BY $campo $sort";
        }else{
            $sort_cad = "ORDER BY inm_proy_id DESC";
        }

        if(($config->price_max == "2000" && $config->in == "Alquiler") || ($config->price_max == "500000" && $config->in == "Venta") || ($config->price_max == "150000" && $config->in == "Anticretico") ) {
            $price = ($config->price_min != null && $config->price_max != null) ? "AND inm_precio >= " . $config->price_min : '';
        }else{
            $price = ($config->price_min != null && $config->price_max != null) ? "AND (inm_precio >= " . $config->price_min . " AND inm_precio <= " . $config->price_max . ")" : '';
        }

        if (!empty($config->room) || !empty($config->bathroom) || !empty($config->parking)) {
            $filter = "INNER JOIN inmueble_caracteristica ic ON ic.`eca_inm_id` = inm_id  ";
        }


        if(!empty($config->room)){
            $filter .= " AND (ic.`eca_car_id` = 4 AND ic.`eca_valor` = '".$config->room."')";
        }

        if(!empty($config->bathroom)){
            $filter .= " AND (ic.`eca_car_id` = 5 AND ic.`eca_valor` = '".$config->bathroom."')";
        }

        if(!empty($config->parking)){
            $filter .= " AND (ic.`eca_car_id` = 9 AND ic.`eca_valor` = '".$config->parking."')";
        }

        return array(
            "search" => $search,
            "type" => $type,
            "in" => $in,
            "city" => $city,
            "currency" => $currency,
            "price" => $price,
            "sort" => $sort_cad,
            "filter" => $filter
        );
    }

    function get_image_publication($inm_id) {
        $sql = "SELECT fot_archivo FROM inmueble_foto WHERE fot_inm_id = " . $inm_id . " ORDER BY fot_id LIMIT 1";
        $obj = $this->db->query($sql)->row_array();
        return $obj["fot_archivo"];
    }

    function get_all_images_publication($inm_id) {
        $sql = "SELECT fot_archivo FROM inmueble_foto WHERE fot_inm_id = " . $inm_id . " ORDER BY fot_id";
        return $this->db->query($sql)->result_array();
    }

    function get_similar_properties($ciu_id, $cat_id, $zon_id, $inm_id){
        $sql = "
            SELECT DISTINCT bus_inmueble_3.*, 'No' AS patrocinado FROM bus_inmueble_3
            WHERE pub_estado = 'Aprobado' AND pub_vig_fin >=  CURDATE()
            AND inm_publicado = 'Si'
            AND inm_proy_id = 0
            AND inm_ciu_id = '$ciu_id'
            AND inm_cat_id = '$cat_id'
            AND inm_zon_id = '$zon_id'
            AND inm_id <> '$inm_id'
            ORDER BY RAND()
            LIMIT 3
        ";

        $result = $this->db->query($sql)->result();
        $num = $this->db->query("SELECT FOUND_ROWS() as total")->row();
        if($num->total > 0) {
            foreach ($result as $publication) {
                $publication->features = $this->get_features_property($publication->inm_id);
                $publication->fot_archivo = $this->get_image_publication($publication->inm_id);
                $data_search[] = $publication;
            }
        }else{
            $data_search = array();
        }
        $data_result = array(
            "results" => $data_search
        );
        return $data_result;
    }

    function get_properties_inmobiliarias_id($limit, $page, $id){
        $page = $page * $limit;
        $sql = "
            SELECT DISTINCT bus_inmueble_3.* FROM bus_inmueble_3
            WHERE pub_estado = 'Aprobado' AND pub_vig_fin >=  CURDATE()
            AND inm_publicado = 'Si'
            AND inm_proy_id = 0
            AND pub_usu_id = '$id'
            ORDER BY pub_id DESC
            LIMIT $page, $limit
        ";

        $result = $this->db->query($sql)->result();
        $num = $this->db->query("
        SELECT DISTINCT COUNT(pub_id) AS total FROM bus_inmueble_3
        WHERE pub_estado = 'Aprobado' AND pub_vig_fin >=  CURDATE()
        AND inm_publicado = 'Si'
        AND inm_proy_id = 0
        AND pub_usu_id = '$id'
        ")->row();
        if($num->total > 0) {
            foreach ($result as $publication) {
                $publication->features = $this->get_features_property($publication->inm_id);
                $publication->fot_archivo = $this->get_image_publication($publication->inm_id);
                $data_search[] = $publication;
            }
        }else{
            $data_search = array();
        }
        $data_result = array(
            "results" => $data_search,
            "num_total" => $num->total
        );
        return $data_result;
    }


    function get_favorites_by_ids($favorite_array){

        foreach($favorite_array as $fav){
            $sql = "
            SELECT bus_inmueble_3.*, 'No' AS patrocinado FROM bus_inmueble_3
            WHERE inm_id = '$fav'
            ";

            $publication = $this->db->query($sql)->row();
            $publication->features = $this->get_caracteristicas($publication->inm_id);
            $publication->fot_archivo = $this->get_image_publication($publication->inm_id);
            $data_search[] = $publication;
        }

        return $data_search;
    }

    function get_caracteristicas($id) {
        $sql = "
            SELECT
                inm_car.`eca_inm_id` AS inmueble_id,
                car.`car_nombre` AS `caracteristica`,
                inm_car.`eca_valor` AS valor
            FROM
                `inmueble_caracteristica` inm_car
                INNER JOIN caracteristica car ON (car.car_id = inm_car.eca_car_id)
            WHERE
                inm_car.eca_inm_id = $id";

        return $this->db->query($sql)->result();
    }

    function get_num_publications_by_user($user_id, $searchText=""){
        $sql = "
            SELECT COUNT(inm.`inm_id`) AS cantidad FROM publicacion pub
            INNER JOIN inmueble inm ON (inm.`inm_id` = pub.`pub_inm_id` AND inm.inm_visible = 'Si' AND inm.inm_nombre LIKE '%$searchText%')
            WHERE pub.`pub_usu_id` = '$user_id'
            ";

        $num = $this->db->query($sql)->row();
        return $num->cantidad;
    }

    function get_publications_by_user($user_id, $page, $limit, $searchText = ""){
        $page = ($page - 1) >= 0 ?  (($page-1)*$limit) : 0;
        $sql = "
<<<<<<< local
            SELECT inm.*, MAX(pub.`pub_vig_fin`) AS finish, pub.`pub_estado`, dep.`dep_nombre`, cat.`cat_nombre`, fm.`for_descripcion`, pub.pub_id FROM publicacion pub
=======
            SELECT inm.*, MAX(pub.`pub_vig_fin`) AS finish, pub.`pub_estado`, dep.`dep_nombre`, cat.`cat_nombre`, fm.`for_descripcion`, pub.`pub_id`
            FROM publicacion pub
>>>>>>> other
            INNER JOIN inmueble inm ON (inm.`inm_id` = pub.`pub_inm_id` AND inm.inm_visible = 'Si' AND inm.inm_nombre LIKE '%$searchText%')
            INNER JOIN ciudad ciu ON (ciu.`ciu_id` = inm.`inm_ciu_id`)
            INNER JOIN departamento dep ON (dep.`dep_id` = ciu.`ciu_dep_id`)
            INNER JOIN categoria cat ON (cat.`cat_id` = inm.`inm_cat_id`)
            INNER JOIN forma fm ON (fm.`for_id` = inm.`inm_for_id`)
            WHERE pub.`pub_usu_id` = '$user_id'
            GROUP BY inm.`inm_id`
            ORDER BY pub.pub_vig_ini DESC
            LIMIT $page, $limit
            ";

        return $this->db->query($sql)->result();
    }

    function has_payments($publicationId){
        $sql = "
            SELECT *
            FROM pagos
            WHERE pagos.pag_pub_id =  $publicationId
            AND pagos.pag_entidad IS NOT NULL
        ";

        $num = $this->db->query($sql)->result();
        return count($num) > 0;
    }

    function exist_url_seo($url_seo){
        $sql = "
          SELECT COUNT(inm.`inm_id`) AS cantidad FROM inmueble inm
          WHERE inm.`inm_seo` = '$url_seo'
        ";
        $num = $this->db->query($sql)->row();
        return $num->cantidad > 0 ? true : false;
    }

    function insert_images($data) {
        $this->db->insert("inmueble_foto", $data);
        return $this->db->insert_id();
    }

    function insert_planos($data) {
        $this->db->insert("inmueble_planos", $data);
        return $this->db->insert_id();
    }

    function delete_inmueble($id){
        $value = array(
            'inm_visible' => 'No'
        );
        $this->db->where('inm_id', $id);
        $this->db->update('inmueble', $value);
    }

    function get_publication($pub_id){
        $this->db->where('pub_id', $pub_id);
        $this->db->where('pub_estado', 'Aprobado');
        return $this->db->get($this->table)->row();
    }

    function is_expired($pub_id){
        $sql = "
        SELECT COUNT(pub.`pub_id`) AS cantidad FROM publicacion pub
        WHERE CURDATE() <= pub.`pub_vig_fin` AND pub.`pub_id` = $pub_id
        ";
        $row = $this->db->query($sql)->row();
        return $row->cantidad > 0 ? false : true;
    }

}