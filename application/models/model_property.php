<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_property extends MY_Model {

    const TABLE_ID = "inm_id";
    const TABLE_NAME = "inmueble";

    const PUBLISHED = "Si";
    const NOT_PUBLISHED = "No";
    const EXCLUDE_PROJECTS = " inm_proy_id = 0 ";

    /** property status - begin */
    const STATUS_PRE_SALE = 0;
    const STATUS_BRAND_NEW = 1;
    const STATUS_GOOD = 2;
    const STATUS_REQUIRE_MAINTENANCE = 3;
    /** property status - end */

    protected $_name;
    protected $_image;
    protected $_video;
    protected $_cityId;
    protected $_price;
    protected $_currencyId;
    protected $_address;
    protected $_surface;
    protected $_detail;
    protected $_propertyTypeId;
    protected $_zoneId;
    protected $_transactionTypeId;
    protected $_latitude;
    protected $_longitude;
    protected $_uv;
    protected $_block;
    protected $_surfaceType;
    protected $_order;
    protected $_calls;
    protected $_visits;
    protected $_isPublished;
    protected $_seo;
    protected $_shortSeo;
    protected $_isVisible;
    protected $_projectId;
    protected $_status;
    protected $_amenities;

    private $_arrayPropertiesType;
    private $_arrayTransactionsType;
    private $_arrayCities;

    //Due to autoload is necessary to set the default data as empty string
    function __construct($name = "", $image = "", $video = "", $cityId = "", $price = "", $currencyId = "", $address = "", $surface = "", $detail = "", $propertyTypeId = "",
                        $zoneId = "", $transactionTypeId = "", $latitude = "", $longitude = "", $uv = "", $block = "", $surfaceType = "", $order = "", $calls = "", $visits = "",
                        $isPublished = "", $seo = "", $shortSeo = "", $isVisible = "", $projectId = "", $status = "", $amenities = "")
    {
        parent::__construct();
        $this->load->model('model_message');
        $this->_name = $name;
        $this->_image = $image;
        $this->_video = $video;
        $this->_cityId = $cityId;
        $this->_price = $price;
        $this->_currencyId = $currencyId;
        $this->_address = $address;
        $this->_surface = $surface;
        $this->_detail = $detail;
        $this->_propertyTypeId = $propertyTypeId;
        $this->_zoneId = $zoneId;
        $this->_transactionTypeId = $transactionTypeId;
        $this->_latitude = $latitude;
        $this->_longitude = $longitude;
        $this->_uv = $uv;
        $this->_block = $block;
        $this->_surfaceType = $surfaceType;
        $this->_order = $order;
        $this->_calls = $calls;
        $this->_visits = $visits;
        $this->_isPublished = $isPublished;
        $this->_seo = $seo;
        $this->_shortSeo = $shortSeo;
        $this->_isVisible = $isVisible;
        $this->_projectId = $projectId;
        $this->_status = $status;
        $this->_amenities = $amenities;

        $this->_arrayPropertiesType = array(1 => "casas", 2 => "departamentos", 3 => "oficinas", 4 => "locales-comerciales", 13 => "terrenos", 14 => "quintas-y-propiedades");
        $this->_arrayTransactionsType = array(1 => "venta", 2 => "alquiler", 3 => "anticretico");
        $this->_arrayCities = array(1 => "santa-cruz", 2 => "la-paz", 3 => "cochabamba", 4 => "tarija", 5 => "chuquisaca", 6 => "oruro", 7 => "potosi", 8 => "pando", 9 => "beni");
    }

    public function toArray()
    {
        $tableAttributes = array(
                            "inm_id" => $this->_id,
                            "inm_nombre" => $this->_name,
                            "inm_foto" => $this->_image,
                            "inm_video" => $this->_video,
                            "inm_ciu_id" => $this->_cityId,
                            "inm_precio" => $this->_price,
                            "inm_mon_id" => $this->_currencyId,
                            "inm_direccion" => $this->_address,
                            "inm_superficie" => $this->_surface,
                            "inm_detalle" => $this->_detail,
                            "inm_cat_id" => $this->_propertyTypeId,
                            "inm_zon_id" => $this->_zoneId,
                            "inm_for_id" => $this->_transactionTypeId,
                            "inm_latitud" => $this->_latitude,
                            "inm_longitud" => $this->_longitude,
                            "inm_uv" =>  $this->_uv,
                            "inm_manzano" => $this->_block,
                            "inm_tipo_superficie" => $this->_surfaceType,
                            "inm_orden" => $this->_order,
                            "inm_llamadas" => $this->_calls,
                            "inm_visitas" => $this->_visits,
                            "inm_publicado" => $this->_isPublished,
                            "inm_seo" => $this->_seo,
                            "inm_short_seo" => $this->_shortSeo,
                            "inm_visible" => $this->_isVisible,
                            "inm_proy_id" => $this->_projectId,
                            "inm_status" => $this->_status,
                            "inm_amenities" => $this->_amenities
                                );
        return $tableAttributes;
    }

    /**
     * @param $className
     * @param $object
     * @return null
     */
    protected static function recast($className, $object)
    {
        $response =  null;
        if ($object instanceof stdClass)
        {
            if (!class_exists($className))
                throw new InvalidArgumentException(sprintf('Inexistant class %s.', $className));

            //Let's set the values to payment object using the data from stdObject
            $instance = new $className(
                        $object->inm_nombre,
                        $object->inm_foto,
                        $object->inm_video,
                        $object->inm_ciu_id,
                        $object->inm_precio,
                        $object->inm_mon_id,
                        $object->inm_direccion,
                        $object->inm_superficie,
                        $object->inm_detalle,
                        $object->inm_cat_id,
                        $object->inm_zon_id,
                        $object->inm_for_id,
                        $object->inm_latitud,
                        $object->inm_longitud,
                        $object->inm_uv,
                        $object->inm_manzano,
                        $object->inm_tipo_superficie,
                        $object->inm_orden,
                        $object->inm_llamadas,
                        $object->inm_visitas,
                        $object->inm_publicado,
                        $object->inm_seo,
                        $object->inm_short_seo,
                        $object->inm_visible,
                        $object->inm_proy_id,
                        $object->inm_status,
                        $object->inm_amenities
                        );
            $instance->_id = $object->inm_id;
            $response = $instance;
        }
        return $response;
    }

    ################################################################################################# BEGIN - GETTERS

    public static function getByProjectId($projectId)
    {
        $ci=&get_instance();
        $ci->load->database();
        $sql = "
            select * from ".self::TABLE_NAME." where inm_proy_id = ".$ci->db->escape($projectId)."
        ";
        $query = $ci->db->query($sql);
        $response = self::recast(get_called_class(),$query->row());
        return $response;
    }

    /**
     * Return a instance of model_property taking the publicationId as parameter
     * @param $publicationId
     * @return model_property object
     */
    public static function getByPublicationId($publicationId)
    {
        $ci=&get_instance();
        $ci->load->database();
        $sql = "
            select ".self::TABLE_NAME.".* from ".self::TABLE_NAME."
            left join publicacion on pub_inm_id = inm_id
            where pub_id = ".$ci->db->escape($publicationId)."
        ";
        $query = $ci->db->query($sql);
        $response = self::recast(get_called_class(),$query->row());
        return $response;
    }

    /**
     * @return string
     */
    public function getIsPublished()
    {
        return $this->_isPublished;
    }

    public function getName()
    {
        return $this->_name;
    }

    public function getSeo($defaultFullUrl = FALSE)
    {
        $url = $this->_seo;
        if($defaultFullUrl)
        {
            $url = base_url("buscar/inmuebles-en-venta-en-bolivia/".$this->_seo);
        }
        return $url;
    }
    public static function getCharacteristicsByPropertyId($propertyId, $characteristicId)
    {
        $ci=&get_instance();
        $ci->load->database();
        $sql = "
            select inmueble_caracteristica.* from inmueble_caracteristica
            where eca_inm_id = ".$ci->db->escape($propertyId)." and eca_car_id = ".$ci->db->escape($characteristicId)."
        ";
        $query = $ci->db->query($sql);
        $response = $query->row();
        return $response;
    }

    ################################################################################################# END - GETTERS

    ################################################################################################# BEGIN - SETTERS

    public function setName($name)
    {
        $this->_name = $name;
    }

    public function setDetail($detail)
    {
        $this->_detail = $detail;
    }

    public function setLatitude($latitude)
    {
        $this->_latitude = $latitude;
    }

    public function setLongitude($longitude)
    {
        $this->_longitude = $longitude;
    }

    public function setPropertyType($propertyType)
    {
        $this->_propertyTypeId = $propertyType;
    }

    public function setTransactionType($transactionType)
    {
        $this->_transactionTypeId = $transactionType;
    }

    public function setCityId($cityId)
    {
        $this->_cityId = $cityId;
    }

    public function setZoneId($zoneId)
    {
        $this->_zoneId = $zoneId;
    }

    public function setAddress($address)
    {
        $this->_address = $address;
    }

    public function setUv($uv)
    {
        $this->_uv = $uv;
    }

    public function setBlock($block)
    {
        $this->_block = $block;
    }

    public function setSurfaceType($surfaceType)
    {
        $this->_surfaceType = $surfaceType;
    }

    public function setCurrencyType($currency)
    {
        $this->_currencyId = $currency;
    }

    public function setSurface($surface)
    {
        $this->_surface = $surface;
    }

    public function setPrice($price)
    {
        $this->_price = $price;
    }

    public function setOrder($order)
    {
        $this->_order = $order;
    }

    public function setIsPublished($isPublished)
    {
        $this->_isPublished = $isPublished;
    }

    public function setStatus($status)
    {
        $this->_status = $status;
    }

    public function setAmenities($amenities)
    {
        $this->_amenities = $amenities;
    }
    ################################################################################################# END - SETTERS

    public function __toString()
    {
        $jsonEncode = json_encode($this->toArray());
        return $jsonEncode;
    }

    /************************************************************************************** BEGIN - DATATABLE AJAX METHODS */
    /**
     * @return mixed
     */
    public static function countAll()
    {
        $ci = &get_instance();
        $ci->load->database();

        $sql = '
                select count(' . self::TABLE_ID. ') as total
                from ' . self::TABLE_NAME.'
                left join publicacion on pub_inm_id = inm_id
                left join usuario on usu_id = pub_usu_id where '.static::EXCLUDE_PROJECTS." and pub_estado != 'Eliminado' ";

        $query = $ci->db->query($sql);
        $totalCount = $query->row()->total;
        return $totalCount;
    }

    /**
     * @param $limit
     * @param $offset
     * @param null $orderBy
     * @param string $orderType
     * @return mixed
     */
    public static function getAll($limit, $offset, $orderBy = null, $orderType = 'asc')
    {
        if ($orderBy === null)
        {
            $orderBy = self::TABLE_ID;
        }
        $ci = &get_instance();
        $ci->load->database();

        $sql = 'select '.self::_dataTableColumns().', usu_id, CONCAT(usu_nombre," ",usu_apellido) as user_full_name, datediff(pub_vig_fin, pub_vig_ini) days_remain, IFNULL(total_pending_payments, 0) total_pending_payments from ' . self::TABLE_NAME . '
                left join publicacion on pub_inm_id = inm_id
                left join usuario on usu_id = pub_usu_id
                left join forma on inm_for_id = for_id
                left join categoria on inm_cat_id = cat_id
                left join ciudad on inm_ciu_id = ciu_id
                left join departamento on ciu_dep_id = dep_id
                left join (
                    SELECT
                        pag_pub_id,
                        count(pag_id) total_pending_payments
                    FROM
                        pagos
                    WHERE
                    pag_estado = "Pendiente"
                    and CURDATE() <= pag_fecha_ven
                    GROUP BY pag_pub_id
                ) pending_payments on pending_payments.pag_pub_id = pub_id
                where '.static::EXCLUDE_PROJECTS.' and pub_estado != "Eliminado" 
                order by ' . $orderBy . ' ' . $orderType . ' limit ' . $limit . ' offset ' . $offset;

        $query = $ci->db->query($sql);
        $result = $query->result();
        return $result;
    }

    public static function search($text, $limit, $offset, $orderBy = null, $orderType = 'asc', $colsArray = null, $additionalParams = array())
    {
        if ($orderBy === null)
        {
            $orderBy = self::TABLE_ID;
        }
        $ci = &get_instance();
        $ci->load->database();

        $sql = 'select '.self::_dataTableColumns().', usu_id,CONCAT(usu_nombre," ",usu_apellido) as user_full_name, datediff(pub_vig_fin, pub_vig_ini) days_remain, IFNULL(total_pending_payments, 0) total_pending_payments from ' . self::TABLE_NAME.'
                left join publicacion on pub_inm_id = inm_id
                left join usuario on usu_id = pub_usu_id
                left join forma on inm_for_id = for_id
                left join categoria on inm_cat_id = cat_id
                left join ciudad on inm_ciu_id = ciu_id
                left join departamento on ciu_dep_id = dep_id
                left join (
                    SELECT
                        pag_pub_id,
                        count(pag_id) total_pending_payments
                    FROM
                        pagos
                    WHERE
                    pag_estado = "Pendiente"
                    and CURDATE() <= pag_fecha_ven
                    GROUP BY pag_pub_id
                ) pending_payments on pending_payments.pag_pub_id = pub_id
                ';
        $sql .= ' where '.static::EXCLUDE_PROJECTS.' '.static::_additionalParameters($additionalParams).' and pub_estado != "Eliminado" and (';
        foreach ($colsArray as $var)
        {
            if($var == "user_full_name")
            {
                $var = ' CONCAT(usu_nombre," ",usu_apellido) ';
            }
            $sql .= ' ' . $var . ' like \'%' . $text . '%\' or ';
        }

        $sql = substr($sql, 0, -3);
        $sql .= ') order by ' . $orderBy . ' ' . $orderType . ' limit ' . $limit . ' offset ' . $offset;
//        echo "<pre>";var_dump($sql);exit;
        $query = $ci->db->query($sql);
        return $query->result();
    }

    public static function searchTotalCount($text, $colsArray = null, $additionalParams = array())
    {
        $ci = &get_instance();
        $ci->load->database();

        $sql = 'select count(' . self::TABLE_ID . ') as total from ' . self::TABLE_NAME.'
        left join publicacion on pub_inm_id = inm_id
        left join usuario on usu_id = pub_usu_id
        left join forma on inm_for_id = for_id
        left join categoria on inm_cat_id = cat_id
        left join ciudad on inm_ciu_id = ciu_id
        left join departamento on ciu_dep_id = dep_id';
        $sql .= ' where '.static::EXCLUDE_PROJECTS.' '.static::_additionalParameters($additionalParams).' and pub_estado != "Eliminado" and (';

        foreach ($colsArray as $var)
        {
            if($var == "user_full_name")
            {
                $var = ' CONCAT(usu_nombre," ",usu_apellido) ';
            }
            $sql .= ' ' . $var . ' like \'%' . $text . '%\' or ';
        }

        $sql = substr($sql, 0, -3);
        $sql .= ')';

        $query = $ci->db->query($sql);
        $totalCount = $query->row()->total;
        return $totalCount;
    }

    private static function _dataTableColumns()
    {
        $columns = " pub_id, pub_vig_ini, pub_vig_fin, inm_id, inm_nombre, inm_publicado, inm_seo, inm_visitas, usu_email, usu_tipo, for_descripcion, cat_nombre, dep_nombre";
        return $columns;
    }

    private static function _additionalParameters($list = array())
    {
        $ci=&get_instance();
        $ci->load->database();
        $sql = "";
        $dateRange = "";
        $arrayDateToFilter = array("start"=>"pub_vig_ini","end"=>"pub_vig_fin");
        if(is_array($list) && count($list) >= 1)
        {
            foreach($list as $parameter => $value)
            {
                    switch ($parameter)
                {
                    case "show-expired":
                        $sql .= " and now() > concat(pub_vig_fin,' 23:59:59') ";
                        break;
                    case "start-date":
                        $value = date( "Y-m-d", strtotime($value));
                        $dateRange .= " and replace_date >= ".$ci->db->escape($value);
                        break;
                    case "end-date":
                        $value = date( "Y-m-d", strtotime($value));
                        $dateRange .= " and replace_date <= ".$ci->db->escape($value);
                        break;
                    case "date-to-filter":
                        if($dateRange != "")
                        {
                            $dateRange = str_replace("replace_date",$arrayDateToFilter[$value],$dateRange);
                            $sql .= $dateRange;
                        }
                        break;
                    case "user":
                        $sql .= " and usu_id = ".$ci->db->escape($value)." ";
                        break;
                }
            }
        }

        return $sql;
    }
    /*********************************************************************************** END - DATATABLE AJAX METHODS */

    public static function getFeaturesFullDetailByPropertyIdComposedList($propertyId)
    {
        $ci=&get_instance();
        $ci->load->database();
        $sql = "
            SELECT
                car_id,
                car_descripcion,
                eca_valor
            FROM
                inmueble_caracteristica ic
            LEFT JOIN caracteristica c on ic.eca_car_id = c.car_id
            where eca_inm_id = ".$ci->db->escape($propertyId)."
        ";
        $query = $ci->db->query($sql);
        $response = $query->result();
        return $response;
    }

    public static function deleteFeatures($propertyId)
    {
        $ci=&get_instance();
        $ci->load->database();
        $ci->db->where('eca_inm_id', $propertyId);
        $ci->db->delete("inmueble_caracteristica");
    }

    public function saveFeatures($arrayFeatures)
    {
        $ci=&get_instance();
        $ci->load->database();
        static::deleteFeatures($this->_id);
        $arrayToSave = array();

        foreach($arrayFeatures as $featureId => $featureValue)
        {
            if($featureId == 11)
            {
                $featureValue = str_replace(",","",$featureValue);
            }
            $arrayToSave[] = array(
                                "eca_inm_id" => $this->_id,
                                "eca_car_id" => $featureId,
                                "eca_valor" => $featureValue
                                );
        }
        if(count($arrayToSave) > 0)
            $ci->db->insert_batch("inmueble_caracteristica", $arrayToSave);
    }

    public function createPublication($userId)
    {
        $startValidity = date('Y-m-d');
        $finishValidity = strtotime ('+180 day', strtotime($startValidity));
        $finishValidity = date('Y-m-d', $finishValidity);
        $publication = new model_publication(
            date('Y-m-d H:i:s'),
            $startValidity,
            $finishValidity,
            0,
            $this->_id,
            $userId,
            "Aprobado",
            "",
            0,
            0,
            ""
        );
        $publication->save();
    }

    public static function getPropertyOffersBasedOnUsers($startDate = "", $endDate = "", $propertyTypeId = "", $transactionTypeId = "", $getDemand = FALSE)
    {
        if($startDate == "" || $endDate == "")
        {
            $startDate = date("Y-m")."-01";
            $endDate = date("Y-m-t", strtotime($startDate));
        }
        $propertyTypeCriteriaSearch = "";
        if($propertyTypeId != "")
        {
            $propertyTypeCriteriaSearch = " and inm_cat_id = ".$propertyTypeId;
        }

        $transactionTypeCriteriaSearch = "";
        if($transactionTypeId != "")
        {
            $transactionTypeCriteriaSearch = " and inm_for_id = ".$transactionTypeId;
        }

        $fieldTotal = " COUNT(inm_id) ";
        if($getDemand)
        {
            $fieldTotal = " SUM(inm_visitas) ";
        }

        $ci=&get_instance();
        $ci->load->database();
        $setTotalProjects = "set @totalProjects = (select count(proy_id) total from proyecto where proy_estado = 'Aprobado');";
        $sql = "
            SELECT
                usu_tipo type,
                ".$fieldTotal." total
            FROM
              inmueble
            LEFT JOIN publicacion on pub_inm_id = inm_id
            LEFT JOIN usuario on pub_usu_id = usu_id
            LEFT JOIN forma on inm_for_id = for_id
            LEFT JOIN categoria on inm_cat_id = cat_id
            where
                    usu_tipo in ('Particular', 'Inmobiliaria', 'Constructora')
                    and	pub_estado != \"Eliminado\"
                    and (".$ci->db->escape($startDate)."<= pub_vig_fin) and (".$ci->db->escape($endDate)." >= pub_vig_ini)
                    ".$propertyTypeCriteriaSearch."
                    ".$transactionTypeCriteriaSearch."
            GROUP BY usu_tipo
        ";
        $ci->db->query($setTotalProjects);
        $query = $ci->db->query($sql);
        $response = $query->result();
        return $response;
    }
    public static function getPropertyOffersBasedOnUsersComposedList($startDate = "", $endDate = "", $propertyTypeId = "", $transactionTypeId = "", $getDemand = FALSE)
    {
        if($startDate == "" || $endDate == "")
        {
            $startDate = date("Y-m")."-01";
            $endDate = date("Y-m-t", strtotime($startDate));
        }
        $propertyTypeCriteriaSearch = "";
        if($propertyTypeId != "")
        {
            $propertyTypeCriteriaSearch = " and inm_cat_id = ".$propertyTypeId;
        }

        $transactionTypeCriteriaSearch = "";
        if($transactionTypeId != "")
        {
            $transactionTypeCriteriaSearch = " and inm_for_id = ".$transactionTypeId;
        }

        $fieldTotal = " COUNT(inm_id) ";
        if($getDemand)
        {
            $fieldTotal = " SUM(inm_visitas) ";
        }

        $ci=&get_instance();
        $ci->load->database();
        $setTotalProjects = "set @totalProjects = (select count(proy_id) total from proyecto where proy_estado = 'Aprobado');";
        $sql = "
            SELECT
                cat_nombre 'Tipo de inmueble',
                for_descripcion 'Tipo de transaccion',
                inm_nombre 'Nombre del inmueble',
                inm_direccion 'Direccion',
                inm_superficie 'Superficie',
                inm_tipo_superficie 'Tipo de superficie',
                inm_precio 'Precio',
                mon_abreviado 'Moneda',
                FORMAT((inm_precio/inm_superficie),2) 'Precio x Mt2',
                eca_valor 'Superficie construida',
                FORMAT((inm_precio/eca_valor),2) 'Superficie const. precio x Mt2',
                inm_publicado 'Publicado',
                pub_estado 'Estado de la publicacion',
                -- if(CURDATE() < pub_vig_fin,0,1) expired,
                count(men_id) 'Mensajes',
	            inm_visitas 'Visitas',
                LOWER(CONCAT('https://toqueeltimbre.com/buscar/',REPLACE(cat_nombre, ' ', '-'),'-en-',for_descripcion,'-en-',REPLACE(dep_nombre, ' ', '-'),'/',inm_seo)) link
            FROM
              inmueble
            LEFT JOIN publicacion on pub_inm_id = inm_id
            LEFT JOIN moneda on inm_mon_id = mon_id
            LEFT JOIN categoria on cat_id = inm_cat_id
            LEFT JOIN forma on for_id = inm_for_id
            LEFT JOIN inmueble_caracteristica on inm_id = eca_inm_id and eca_car_id = 11
            LEFT JOIN ciudad on inm_ciu_id = ciu_id
            LEFT JOIN departamento on ciu_dep_id = dep_id
            LEFT JOIN mensaje on men_inm_id = inm_id
            where
                    usu_tipo in ('Particular', 'Inmobiliaria', 'Constructora')
                    and	pub_estado != \"Eliminado\"
                    and (".$ci->db->escape($startDate)."<= pub_vig_fin) and (".$ci->db->escape($endDate)." >= pub_vig_ini)
                    ".$propertyTypeCriteriaSearch."
                    ".$transactionTypeCriteriaSearch."
            GROUP BY usu_tipo
        ";
        $ci->db->query($setTotalProjects);
        $query = $ci->db->query($sql);
        $response = $query->result();
        return $response;
    }
    public static function getPropertyOffersBasedOnTransactionsType($startDate = "", $endDate = "", $userType = "", $propertyTypeId = "", $getDemand = FALSE)
    {
        $ci=&get_instance();
        $ci->load->database();

        if($startDate == "" || $endDate == "")
        {
            $startDate = date("Y-m")."-01";
            $endDate = date("Y-m-t", strtotime($startDate));
        }

        $userTypeCriteriaSearch = "";
        if($userType != "")
        {
            $userTypeCriteriaSearch = " and usu_tipo = ".$ci->db->escape($userType);
        }

        $propertyTypeCriteriaSearch = "";
        if($propertyTypeId != "")
        {
            $propertyTypeCriteriaSearch = " and inm_cat_id = ".$ci->db->escape($propertyTypeId);
        }

        $fieldTotal = " COUNT(inm_id) ";
        if($getDemand)
        {
            $fieldTotal = " SUM(inm_visitas) ";
        }

        $setTotalProjects = "set @totalProjects = (select count(proy_id) total from proyecto where proy_estado = 'Aprobado');";
        $sql = "
            SELECT
                for_descripcion type,
                ".$fieldTotal." total
            FROM
                inmueble
            LEFT JOIN publicacion on pub_inm_id = inm_id
            LEFT JOIN usuario on pub_usu_id = usu_id
            LEFT JOIN forma on inm_for_id = for_id
            LEFT JOIN categoria on inm_cat_id = cat_id
            WHERE
                pub_estado != \"Eliminado\"
                and (".$ci->db->escape($startDate)."<= pub_vig_fin) and (".$ci->db->escape($endDate)." >= pub_vig_ini)
                ".$userTypeCriteriaSearch."
                ".$propertyTypeCriteriaSearch."
            GROUP BY for_id
        ";
        $query = $ci->db->query($sql);
        $response = $query->result();
        return $response;
    }

    public static function getPropertyOffersBasedOnPropertyTypes($startDate = "", $endDate = "", $userType = "", $transactionTypeId = "", $getDemand = FALSE)
    {
        $ci=&get_instance();
        $ci->load->database();

        if($startDate == "" || $endDate == "")
        {
            $startDate = date("Y-m")."-01";
            $endDate = date("Y-m-t", strtotime($startDate));
        }

        $userTypeCriteriaSearch = "";
        if($userType != "")
        {
            $userTypeCriteriaSearch = " and usu_tipo = ".$ci->db->escape($userType);
        }

        $transactionTypeCriteriaSearch = "";
        if($transactionTypeId != "")
        {
            $transactionTypeCriteriaSearch = " and inm_for_id = ".$ci->db->escape($transactionTypeId);
        }

        $fieldTotal = " COUNT(inm_id) ";
        if($getDemand)
        {
            $fieldTotal = " SUM(inm_visitas) ";
        }

        $sql = "
            SELECT
                cat_nombre type,
                ".$fieldTotal." total
            FROM
                inmueble
            LEFT JOIN publicacion on pub_inm_id = inm_id
            LEFT JOIN usuario on pub_usu_id = usu_id
            LEFT JOIN categoria on inm_cat_id = cat_id
            LEFT JOIN forma on inm_for_id = for_id
            WHERE
                pub_estado != \"Eliminado\"
                and (".$ci->db->escape($startDate)."<= pub_vig_fin) and (".$ci->db->escape($endDate)." >= pub_vig_ini)
                ".$userTypeCriteriaSearch."
                ".$transactionTypeCriteriaSearch."
            GROUP BY cat_id
        ";
        $query = $ci->db->query($sql);
        $response = $query->result();
        return $response;
    }

    /**
     * @param $messageId
     * @return object
     */
    public static function getByMessageId($messageId)
    {
        $ci =&get_instance();
        $ci->load->database();

        $sql = "
            SELECT
                ".static::TABLE_NAME.".*
            FROM
                ".static::TABLE_NAME."        
            LEFT JOIN mensaje on men_inm_id = ".static::TABLE_ID."
            WHERE
            men_id = ".$ci->db->escape($messageId)."
        ";

        $query = $ci->db->query($sql);
        $response = static::recast(get_called_class(), $query->row());
        return $response;
    }

    public static function emailNewImageNotification(model_publication $publication,model_property $property, array $propertyImageArray = array())
    {
        $ci =& get_instance();
        $ci->load->model("model_parametros");
        $ci->load->model("model_usuario");
        $ci->load->model("model_property");

        require_once(BASE_DIRECTORY . DS . 'admin' . DS . 'clases' . DS. 'phpmailer' . DS . 'class.phpmailer.php');
        $parametros = model_parametros::getAll();
        $publication = $publication->toArray();
        $userInfo = model_user::getById($publication["pub_usu_id"]);
        $cue_nombre = $parametros->par_salida;
        $cue_pass = $parametros->par_pas_salida;
        $property = $property->toArray();
        $data["publication"] = $publication;
        $data["property"] = $property;
        $data["propertyCharacteristics"] = model_property::getCharacteristicsByPropertyId($property["inm_id"],11);
        $data["userInfo"] = $userInfo->toArray();
        $data["propertyImageArray"] = $propertyImageArray;
        $mail = new PHPMailer(true);
        $mail->SetLanguage('es');
        $mail->IsSMTP();
        $mail->SMTPAuth = true;
        $mail->Host = $parametros->par_smtp;
        $mail->Timeout=30;
        $mail->CharSet = 'utf-8';
        $mail->Subject = "Nuevas imagenes cargadas";
        $body = $ci->load->view("../../assets/template_mail/new-image-notification.php",$data,TRUE);
        $mail->MsgHTML($body);
        $infoEmail = Private_Controller::getEmail();
        $mail->AddAddress($infoEmail);
        $mail->SetFrom($cue_nombre, model_parametros::SITE_DOMAIN);
        $mail->Username = $cue_nombre;
        $mail->Password = $cue_pass;
        $mail->Send();
    }

    public static function getPublishedPropertiesComposedList($userId)
    {
        $ci =& get_instance();
        $ci->load->database();

        $sql = "
        SELECT
            *
        FROM
            publicacion
        LEFT JOIN inmueble on pub_inm_id = inm_id
        LEFT JOIN usuario on pub_usu_id = usu_id
        WHERE
        inm_publicado = 'Si'
        and usu_id = ".$ci->db->escape($userId)."
        and pub_vig_fin > CURDATE()
        ";

        $query = $ci->db->query($sql);
        return $query->result();
    }

    /**
     * @param $seo
     * @param $propertyId
     * @return bool
     */
    public static function getSeoNotPropertyIdExists($seo, $propertyId = "")
    {
        $property = static::getBySeo($seo);

        if ($propertyId == "")
        {
            //new
            if (!$property instanceof model_property)
            {
                //there isn't occurrences
                $response = false;
            }
            else
            {
                //If there is a property with same seo
                $response = true;
            }
        }
        else
        {
            //If is edition
            if (!$property instanceof model_property)
            {
                //there isn't occurrences
                $response = false;
            }
            elseif ($property->getId() == $propertyId)
            {
                //If there is a property with same seo but is the same property
                $response = false;
            }
            else
            {
                $response = true;
            }
        }
        return $response;
    }

    /**
     * @param $seo
     * @return object
     */
    public static function getBySeo($seo)
    {
        $ci = &get_instance();
        $ci->load->database();

        $sql = "select ".static::TABLE_NAME.".* from ".static::TABLE_NAME." where inm_seo = " . $ci->db->escape($seo);
        $query = $ci->db->query($sql);
        return static::recast(get_called_class(), $query->row());
    }

    public static function getFullDetailById($propertyId)
    {
        $ci = &get_instance();
        $ci->load->database();

        $sql = "
        SELECT
            inmueble.*,
            cat_nombre,
            for_descripcion,
            ciu_nombre,
            mon_abreviado,
            mon_descripcion
        FROM
            inmueble
        LEFT JOIN categoria on cat_id = inm_cat_id
        LEFT JOIN forma on for_id = inm_for_id
        LEFT JOIN ciudad on ciu_id = inm_ciu_id
        LEFT JOIN moneda on mon_id = inm_mon_id
        LEFT JOIN publicacion on pub_inm_id = inm_id
        WHERE 
            inm_id = ".$ci->db->escape($propertyId)."
        and inm_publicado = 'Si'
        and pub_estado = 'Aprobado'
        and pub_vig_fin > CURDATE()
        ";
        $query = $ci->db->query($sql);
        return $query->row();
    }
}