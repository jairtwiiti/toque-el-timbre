<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_suscripcion extends CI_Model {

    private $table = 'suscripcion';
    private $id = 'id';

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    /**
     * Gets all results for this model but get a one result by id too.
     *
     * @param bool $id
     * @return []
     */
    function get_all($id = FALSE) {
        if ($id === FALSE) {
            $query = $this->db->get($this->table);
            return $query->result_array();
        }

        $query = $this->db->get_where($this->table, array($this->id => $id));
        return $query->row_array();
    }

    /**
     * Add new Model
     *
     * @param $data
     * @return mixed
     */
    function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    /**
     * Update Model
     *
     * @param $id
     * @param $data
     * @return mixed
     */
    function update($id, $data) {
        $this->db->where($this->id, $id);
        return $this->db->update($this->table, $data);
    }

	function update_all($data) {
		$this->db->where('fecha_fin IS NOT NULL');
		$this->db->where('fecha_fin <=', date('Y-m-d'));
		return $this->db->update($this->table, $data);
	}

    /**
     * Delete Model
     *
     * @param $id
     * @return mixed
     */
    function delete($id) {
        return $this->db->update($this->table, array($this->id => $id));
    }

    function get_num_subscription($user_id){
        $sql = "
        SELECT * FROM suscripcion
        WHERE usuario_id = $user_id
        ";
        return $this->db->query($sql)->num_rows();
    }

    function get_subscription($user_id, $page, $limit){
        $page = ($page - 1) >= 0 ? ($page - 1) : 0;
        $page = $page * $limit;
        $sql = "
        SELECT su.*, pag.*, ps.*, ser.*  FROM suscripcion su
        INNER JOIN pagos pag ON (pag.`pag_id` = su.`pago_id`)
        INNER JOIN pago_servicio ps ON (ps.`pag_id` = pag.`pag_id`)
        INNER JOIN servicios ser ON (ser.`ser_id` = ps.`ser_id`)
        WHERE usuario_id = '$user_id' and estado <> 'Eliminado'
        LIMIT $page, $limit
        ";
        return $this->db->query($sql)->result();
    }

    function has_subscription($user_id){
        $sql = "
        SELECT COUNT(su.`id`) AS cantidad FROM suscripcion su
        WHERE CURDATE() >= su.`fecha_inicio`
        AND CURDATE() <= su.`fecha_fin`
        AND su.`usuario_id` = '$user_id'
        AND su.`estado` = 'Activo'
        ";
        $row = $this->db->query($sql)->row();
        return $row->cantidad > 0 ? true : false;
    }

    function get_suscription_by_userId($user_id) {
        $sql ="
            SELECT *
            FROM suscripcion su
            WHERE su.usuario_id = $user_id AND su.estado = 'Activo'
            ORDER BY su.fecha_fin desc
        ";
        return $this->db->query($sql)->row();
    }

    function update_by_pag_id($pag_id, $data) {
        $this->db->where('pago_id', $pag_id);
        return $this->db->update($this->table, $data);
    }

}