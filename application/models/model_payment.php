<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_payment extends MY_Model {

    const TABLE_ID = "pag_id";
    const TABLE_NAME = "pagos";
    //Payment Methods
    const OFFICE = "Oficina";
    const BANK = "Banco";
    const COURTESY = "Toqueeltimbre";
    const PAGOS_NET = "Pagosnet";
    //Payment Status
    const PAID_OUT = "Pagado";
    const PENDING = "Pendiente";
    const DELETED = "Eliminado";

    private $_paymentServiceList;
    private $_approvalTaskList;
    private $_omittedAmountByCourtesy;

    protected $_transactionCode;
    protected $_publicationId;
    protected $_couponUser;
    protected $_createdOn;
    protected $_paymentExpirationDate;
    protected $_invoiceName;
    protected $_invoiceDocumentId;
    protected $_amount;
    protected $_discountAmount;
    protected $_status;
    protected $_paymentMethod;
    protected $_paymentStatement;
    protected $_paymentDate;
    protected $_paymentModified;

    //Due to autoload is necessary to set the default data as empty string
    function __construct($paymentId = NULL, $transactionCode = "", $publicationId  = NULL, $couponUser = NULL, $createdOn = NULL, $paymentExpirationDate = NULL,
                        $invoiceName = "", $invoiceDocumentId = "", $amount = 0, $discountAmount = 0, $status = "Pendiente", $paymentMethod = NULL,
                        $paymentStatement = "", $paymentDate = NULL, $paymentModified = 0)
    {
        parent::__construct();
        $this->_id = $paymentId;
        $this->_paymentServiceList = NULL;
        $this->_approvalTaskList = array();
        $this->_transactionCode = $transactionCode;
        $this->_publicationId = $publicationId;
        $this->_couponUser = $couponUser;
        $this->_createdOn = $createdOn;
        $this->_paymentExpirationDate = $paymentExpirationDate;
        $this->_invoiceName = $invoiceName;
        $this->_invoiceDocumentId = $invoiceDocumentId;
        $this->_amount = $amount;
        $this->_discountAmount = $discountAmount;
        $this->_status = $status;
        $this->_paymentMethod = $paymentMethod;
        $this->_paymentStatement = $paymentStatement;
        $this->_paymentDate = $paymentDate;
        $this->_paymentModified = $paymentModified;

    }

    /**
     * Parse the current object to key => value array
     * @return array
     */
    public function toArray()
    {
        $tableAttributes = array(
                                "pag_id" => $this->_id,
                                "pag_cod_transaccion" => $this->_transactionCode,
                                "pag_pub_id" => $this->_publicationId,
                                "pag_cupon_user" => $this->_couponUser,
                                "pag_fecha" => $this->_createdOn,
                                "pag_fecha_ven" => $this->_paymentExpirationDate,
                                "pag_nombre" => $this->_invoiceName,
                                "pag_nit" => $this->_invoiceDocumentId,
                                "pag_monto" => $this->_amount,
                                "pag_monto_descuento" => $this->_discountAmount,
                                "pag_estado" => $this->_status,
                                "pag_entidad" =>$this->_paymentMethod,
                                "pag_concepto" => $this->_paymentStatement,
                                "pag_fecha_pagado" => $this->_paymentDate,
                                "pag_modificado" => $this->_paymentModified
                                );
        return $tableAttributes;
    }

    /**
     * @param $className
     * @param $object
     * @return model_payment|null
     */
    protected static function recast($className, $object)
    {
        $response =  null;
        if ($object instanceof stdClass)
        {
            if (!class_exists($className))
                throw new InvalidArgumentException(sprintf('Inexistant class %s.', $className));

            //Let's set the values to payment object using the data from stdObject
            $instance = new $className(
                NULL,
                $object->pag_cod_transaccion,
                $object->pag_pub_id,
                $object->pag_cupon_user,
                $object->pag_fecha,
                $object->pag_fecha_ven,
                $object->pag_nombre,
                $object->pag_nit,
                $object->pag_monto,
                $object->pag_monto_descuento,
                $object->pag_estado,
                $object->pag_entidad,
                $object->pag_concepto,
                $object->pag_fecha_pagado,
                $object->pag_modificado
            );
            $instance->_id = $object->pag_id;
            $response = $instance;
        }
        return $response;
    }
    ################################################################################################# BEGIN - GETTERS
    /**
     * Get the current payment status
     * @return string
     */
    public function getStatus()
    {
        return  $this->_status;
    }

    /**
     * Get the list of payment_services object that was bought on current payment
     * @return null|array
     */
    public function getPaymentServiceList()
    {
        if(is_null($this->_paymentServiceList))
        {
            $this->_paymentServiceList = model_paymentservice::getByPaymentId($this->_id);
        }
        return $this->_paymentServiceList;
    }

    public function getAmount()
    {
        return $this->_amount;
    }

    public function getInvoiceName()
    {
        return $this->_invoiceName;
    }

    public function getInvoiceDocumentId()
    {
        return $this->_invoiceDocumentId;
    }

    public function getUser()
    {
        $ci = &get_instance();
        $ci->load->database();

        $sql = '
                SELECT
                    pag_fecha_ven, usuario.*
                FROM
                    pagos
                LEFT JOIN publicacion on pub_id = pag_pub_id
                LEFT JOIN usuario on usu_id = pub_usu_id
                where pag_id = '.$ci->db->escape($this->_id).'
                ';

        $query = $ci->db->query($sql);
        $response = static::recast(get_called_class(), $query->row());
        return $response;
    }

    ################################################################################################# END - GETTERS

    ################################################################################################# BEGIN - SETTERS
    public function setId($paymentId)
    {
        $this->_id = $paymentId;
    }

    /**
     * Set payment status
     * @param $status
     */
    public function setStatus($status)
    {
        $this->_status = $status;
    }

    /**
     * Set the method to approve the payment
     * @param $paymentMethod
     */
    public function setPaymentMethod($paymentMethod)
    {
        $this->_paymentMethod = $paymentMethod;
    }

    /**
     * Set the date that the payment is settled
     * @param $paymentDate
     */
    public function setPaymentDate($paymentDate)
    {
        $this->_paymentDate = $paymentDate;
    }

    /**
     * Set the total amount of payment
     * @param $amount
     */
    public function setAmount($amount)
    {
        $this->_amount = $amount;
    }

    public function setTransactionCode($transactionCode)
    {
        $this->_transactionCode = $transactionCode;
    }
    ################################################################################################# END - SETTERS

    ################################################################################################# BEGIN - DATATABLE AJAX METHODS
    /**
     * @return mixed
     */
    public static function countAll()
    {
        $ci = &get_instance();
        $ci->load->database();

        $sql = '
                select count(' . self::TABLE_ID. ') as total
                from ' . self::TABLE_NAME.'
                left join publicacion on pub_id = pag_pub_id
                left join usuario on usu_id = pub_usu_id
                left join inmueble on inm_id = pub_inm_id';

        $query = $ci->db->query($sql);
        $totalCount = $query->row()->total;
        return $totalCount;
    }

    /**
     * @param $limit
     * @param $offset
     * @param null $orderBy
     * @param string $orderType
     * @return mixed
     */
    public static function getAll($limit, $offset, $orderBy = null, $orderType = 'asc')
    {
        if ($orderBy === null)
        {
            $orderBy = self::TABLE_ID;
        }
        $ci = &get_instance();
        $ci->load->database();

        $sql = 'select '.self::_dataTableColumns().', CONCAT(usu_nombre," ",usu_apellido) as user_full_name from ' . self::TABLE_NAME . '
                left join publicacion on pub_id = pag_pub_id
                left join usuario on usu_id = pub_usu_id
                left join inmueble on inm_id = pub_inm_id
                order by ' . $orderBy . ' ' . $orderType . ' limit ' . $limit . ' offset ' . $offset;

        $query = $ci->db->query($sql);
        $result = $query->result();
        return $result;
    }

    public static function search($text, $limit, $offset, $orderBy = null, $orderType = 'asc', $colsArray = null, $additionalParameters)
    {
        if ($orderBy === null)
        {
            $orderBy = self::TABLE_ID;
        }
        $ci = &get_instance();
        $ci->load->database();

        $sql = 'select '.self::_dataTableColumns().', CONCAT(usu_nombre," ",usu_apellido) as user_full_name from ' . self::TABLE_NAME.'
                left join publicacion on pub_id = pag_pub_id
                left join usuario on usu_id = pub_usu_id
                left join inmueble on inm_id = pub_inm_id';
        $sql .= ' where (';
        foreach ($colsArray as $var)
        {
            if($var == "user_full_name")
            {
                $var = ' CONCAT(usu_nombre," ",usu_apellido) ';
            }
            $sql .= ' ' . $var . ' like \'%' . $text . '%\' or ';
        }

        $sql = substr($sql, 0, -3);
        $sql .= ') '.static::_additionalParameters($additionalParameters).' order by  '. $orderBy . ' ' . $orderType . ' limit ' . $limit . ' offset ' . $offset;
//        echo"<pre>";var_dump($sql);exit;
        $query = $ci->db->query($sql);
        return $query->result();
    }

    public static function searchTotalCount($text, $colsArray = null,$additionalParameters)
    {
        $ci = &get_instance();
        $ci->load->database();

        $sql = 'select count(' . self::TABLE_ID . ') as total from ' . self::TABLE_NAME.'
        left join publicacion on pub_id = pag_pub_id
        left join usuario on usu_id = pub_usu_id
        left join inmueble on inm_id = pub_inm_id';
        $sql .= ' where (';

        foreach ($colsArray as $var)
        {
            if($var == "user_full_name")
            {
                $var = ' CONCAT(usu_nombre," ",usu_apellido) ';
            }
            $sql .= ' ' . $var . ' like \'%' . $text . '%\' or ';
        }

        $sql = substr($sql, 0, -3);
        $sql .= ') '.static::_additionalParameters($additionalParameters);

        $query = $ci->db->query($sql);
        $totalCount = $query->row()->total;
        return $totalCount;
    }

    private static function _dataTableColumns()
    {
        $columns = " pag_id, pag_cod_transaccion, inm_nombre, pag_fecha, pag_fecha_pagado, usu_nombre, usu_apellido, usu_email, usu_tipo, pag_monto, pag_entidad, pag_concepto, pag_estado ";
        return $columns;
    }

    private static function _additionalParameters($list = array())
    {
        $ci=&get_instance();
        $ci->load->database();
        $sql = "";
        $dateRange = "";
        $arrayDateToFilter = array("payment-request"=>"pag_fecha","payment-settled"=>"pag_fecha_pagado");
        if(is_array($list) && count($list) >= 1)
        {
            foreach($list as $parameter => $value)
            {
                switch ($parameter)
                {
                    case "start-date":
                        $value = date( "Y-m-d", strtotime($value));
                        $dateRange .= " and replace_date >= ".$ci->db->escape($value);
                        break;
                    case "end-date":
                        $value = date( "Y-m-d", strtotime($value));
                        $dateRange .= " and replace_date <= ".$ci->db->escape($value);
                        break;
                    case "date-to-filter":
                        if($dateRange != "")
                        {
                            $dateRange = str_replace("replace_date",$arrayDateToFilter[$value], $dateRange);
                            $sql .= $dateRange;
                        }
                        break;
                    case 'payment-status':
                        $sql .= " and pag_estado = ".$ci->db->escape($value);
                        break;
                }
            }
        }

        return $sql;
    }
    ################################################################################################# END - DATATABLE AJAX METHODS

    public function approve($paymentMethod)
    {
        $this->_approvalTaskList = array();
        $ci = &get_instance();
        $ci->load->database();
        switch ($this->getStatus())
        {
            case self::PENDING:
                $this->_paymentApproval($paymentMethod);
                if(isset($this->_approvalTaskList["payment"]))
                {
                    $this->_activatePaymentServiceList();
                }
                if(isset($this->_approvalTaskList["service"]))
                {
                    $this->_updatePublicationExpirationDate();
                    $this->_publishProperty();
                }
                //Less than 2 elements means that the payment wasn't successful
                if(!isset($this->_approvalTaskList['payment']) || !isset($this->_approvalTaskList['service']))
                {
                    $this->_undoPaymentApproval();
                }
                else
                {
                    $paymentMasterDetail = model_payment::prepareArrayPaymentMasterDetail($this->_id);
                    //Notify through email when the payment was approved only if the payment method wasn't courtesy
                    if($paymentMethod != self::COURTESY)
                    {
                        model_payment::notifyPaymentSettled($paymentMasterDetail);
                    }
                }
                break;
            case self::PAID_OUT:
                $this->_approvalTaskList[] = "Este pago ya habia sido aprobado.";
                break;
            case self::DELETED:
                $this->_approvalTaskList[] = "El pago que intenta aprobar ha sido eliminado.";
                break;
        }

        return $this->_approvalTaskList;
    }

    private function _paymentApproval($paymentMethod)
    {
        $ci = &get_instance();
        $ci->load->database();
        $paymentDate = date("Y-m-d H:i:s");
        $this->setStatus(self::PAID_OUT);
        $this->setPaymentMethod($paymentMethod);
        $this->setPaymentDate($paymentDate);
        $this->_omittedAmountByCourtesy = $this->_amount;
        if($paymentMethod == self::COURTESY)
        {
            $this->_amount = 0;
        }
        $this->save();
        if($ci->db->affected_rows()>0)
        {
            $this->_approvalTaskList["payment"] = "Pago con ID ".$this->_id." fue aprobado.";
        }
    }

    private function _activatePaymentServiceList()
    {
        /** @var model_paymentservice $paymentService */
        $ci = &get_instance();
        $ci->load->database();
        $paymentServiceList = $this->getPaymentServiceList();
        $i = 0;

        foreach($paymentServiceList as $paymentService)
        {
            $paymentService->activateService($this->_publicationId);
            if($ci->db->affected_rows()>0)
            {
                $i++;
            }
        }

        if(count($paymentServiceList) == 1 && $i == count($paymentServiceList))
        {
            $this->_approvalTaskList["service"] = "Un servicio fue activado.";
        }
        elseif(count($paymentServiceList) > 1 && $i == count($paymentServiceList))
        {
            $this->_approvalTaskList["service"] = $i." servicios fueron activados.";
        }
        elseif(count($paymentServiceList) != $i)
        {
           $this->_deactivatePaymentServiceList();
        }
    }

    private function _updatePublicationExpirationDate()
    {
        /** @var model_publication $publication */
        $ci = &get_instance();
        $ci->load->database();
        $publication = model_publication::getById($this->_publicationId);
        $publication->updateValidityBasedOnMostLongPaymentService();
        if($ci->db->affected_rows()>0)
        {
            $this->_approvalTaskList["publication"] = "La publicación con ID ".$publication->getId()." fue actualizada.";
        }
    }

    private function _publishProperty()
    {
        /** @var model_property $property */
        $ci = &get_instance();
        $ci->load->database();
        $property = model_property::getByPublicationId($this->_publicationId);
        $property->setIsPublished(model_property::PUBLISHED);
        $property->save();
        if($ci->db->affected_rows()>0)
        {
            $this->_approvalTaskList["property"] = "El inmueble con ID ".$property->getId()." fue publicado.";
        }
    }

    private function _undoPaymentApproval()
    {
        $this->setStatus(self::PENDING);
        $this->_amount = $this->_omittedAmountByCourtesy;
        $this->setPaymentMethod("");
        $this->setPaymentDate("");
        $this->save();
    }

    private function _deactivatePaymentServiceList()
    {
        /** @var model_paymentservice $paymentService */
        $ci = &get_instance();
        $ci->load->database();
        $paymentServiceList = $this->getPaymentServiceList();
        foreach($paymentServiceList as $paymentService)
        {
            $paymentService->deactivateService();
        }
    }

    /**
     * @param $paymentId
     * @return array
     */
    public static function getPaymentDetailComposedData($paymentId)
    {
        $ci = &get_instance();
        $ci->load->database();

        $sql = "
        SELECT
            p.pag_id payment_id,
            CONCAT(usu_nombre,' ',usu_apellido) user_full_name,
            usu_id user_id,
            usu_tipo user_type,
            usu_ci user_ci,
            usu_email user_email,
            usu_telefono user_phone,
            usu_celular user_cellphone,
            pag_monto payment_amount,
            pag_fecha payment_created_on,
            pag_entidad payment_method,
            pag_nombre payment_name,
            pag_nit payment_nit,
            pag_concepto payment_statement,
            s.ser_descripcion payment_service,
            ps.fech_ini start_date,
            ps.fech_fin finish_date,
            s.ser_precio service_price,
            inm_nombre property_name
        FROM
            pago_servicio ps
        LEFT JOIN pagos p on ps.pag_id = p.pag_id
        LEFT JOIN publicacion on pub_id = p.pag_pub_id
        LEFT JOIN inmueble on pub_inm_id = inm_id
        LEFT JOIN usuario on usu_id = pub_usu_id
        LEFT JOIN servicios s on s.ser_id = ps.ser_id
        WHERE
            p.pag_id = ".$ci->db->escape($paymentId);

        $query = $ci->db->query($sql);
        $result = $query->result();
        return $result;
    }

    public static function getAllPaymentsSettlementByDateRange($startDate = "", $endDate = "")
    {
        if($startDate == "" || $endDate == "")
        {
            $startDate = date("Y-m")."-01";
            $endDate = date("Y-m-t", strtotime($startDate));
        }

        $ci=&get_instance();
        $ci->load->database();
        $sql = "
            SELECT
                DATE_FORMAT(pag_fecha_pagado,'%Y-%m-%d') date,
                count(".static::TABLE_ID.") 'value'
            FROM
                ".static::TABLE_NAME."
            where
              pag_fecha_pagado BETWEEN ".$ci->db->escape($startDate)." and ".$ci->db->escape($endDate)."
            GROUP BY date
        ";
        $query = $ci->db->query($sql);
        $response = $query->result();
        return $response;
    }
    public static function getAllPaymentsSettlementByDateRangeDetailed($startDate = "", $endDate = "")
    {
        $ci=&get_instance();
        $ci->load->database();
        $sql = "
            SELECT
                p.pag_id \"ID TRANSACCION\",
                inm_nombre \"Nombre Inmueble\",
                cat_nombre \"Categoria\",
                for_descripcion \"Forma\",
                ciu_nombre \"Ciudad\",
                p.pag_fecha_pagado \"Fecha Pago\",
                CONCAT(usu_nombre,\" \",usu_apellido) \"Nombre Usuario\",
                usu_email \"Email\",
                usu_tipo \"Tipo usuario\",
                -- p.pag_monto \"Monto\",
                s.ser_precio \"Monto\",
                s.ser_descripcion \"Tipo Servicio\",
                p.pag_entidad \"Metodo\",
                p.pag_concepto \"Concepto\",
                p.pag_estado \"Estado\"

            FROM
                    pagos p
            LEFT JOIN publicacion on pub_id = p.pag_pub_id
            LEFT JOIN inmueble on pub_inm_id = inm_id
            LEFT JOIN categoria on inm_cat_id = cat_id
            LEFT JOIN forma on inm_for_id = for_id
            LEFT JOIN usuario on usu_id = pub_usu_id
            LEFT JOIN ciudad on inm_ciu_id = ciu_id
            LEFT JOIN pago_servicio ps on ps.pag_id = p.pag_id
            LEFT JOIN servicios s on s.ser_id = ps.ser_id
            where
                pag_fecha_pagado BETWEEN ".$ci->db->escape($startDate)." and ".$ci->db->escape($endDate)."
            ORDER BY pag_fecha_pagado desc
        ";
        $query = $ci->db->query($sql);
        $response = $query->result();
        return $response;

    }

    public function addServices($serviceIds = array())
    {
        $arrayToInsert = array();
        $serviceList = model_service::getAllInIdsArray($serviceIds, 100, 0);
        foreach($serviceList as $service)
        {
            $service = $service->toArray();
            if($service["ser_id"] != 0)
            {
                $arrayToInsert[] = array(
                                    "pag_id" => $this->_id,
                                    "ser_id" => $service["ser_id"],
                                    "fech_ini" => NULL,
                                    "fech_fin" => NULL
                                );
            }
        }

        if(count($arrayToInsert) > 0)
        {
            model_paymentservice::insertBatch($arrayToInsert);
        }
    }

    public function sendToPagosNet()
    {
        $pagosNet = new PagosNetHandler($this);
        $gatewayResponse = $pagosNet->cmRegistroPlan();
        return $gatewayResponse;
    }

    public static function generatePaymentId()
    {
        $sw   = true;
        $paymentId = NULL;
        while ($sw)
        {
            $paymentId = Private_Controller::alphaNumericCode(7);
            if (!static::getById($paymentId) instanceof model_payment)
            {
                $sw = false;
            }
        }
        return $paymentId;
    }

    public function addServicesToPayment($servicesList = array())
    {

        foreach($servicesList as $service)
        {
            $arrayToInsert[] = array(
                "pag_id" => $this->_id,
                "ser_id" => $service["id"],
                "fech_ini" => "",
                "fech_fin" => ""
            );
        }

        model_service::insertBatch();
    }

    public static function prepareArrayPaymentMasterDetail($paymentId)
    {
        $paymentDetailAndArrayService = model_payment::getPaymentDetailComposedData($paymentId);
        $paymentDetail = array();
        $serviceList = array();
        if(count($paymentDetailAndArrayService)>0)
        {
            $i=0;
            foreach($paymentDetailAndArrayService as $detail)
            {
                if($i==0)
                {
                    $paymentDetail["paymentId"] = $detail->payment_id;
                    $paymentDetail["paymentStatement"] = $detail->payment_statement;
                    $paymentDetail["userId"] = $detail->user_id;
                    $paymentDetail["userFullName"] = $detail->user_full_name;
                    $paymentDetail["userCI"] = $detail->user_ci;
                    $paymentDetail["userPhone"] = $detail->user_phone;
                    $paymentDetail["userCellPhone"] = $detail->user_cellphone;
                    $paymentDetail["userType"] = $detail->user_type;
                    $paymentDetail["userEmail"] = $detail->user_email;
                    $paymentDetail["paymentAmount"] = $detail->payment_amount;
                    $paymentDetail["paymentCreatedOn"] = $detail->payment_created_on;
                    $paymentDetail["paymentMethod"] = $detail->payment_method;
                    $paymentDetail["paymentName"] = $detail->payment_name;
                    $paymentDetail["paymentNit"] = $detail->payment_nit;
                    $paymentDetail["propertyName"] = $detail->property_name;
                }
                $serviceList[] = array(
                    "index" => ($i+1),
                    "name" => $detail->payment_service,
                    "startDate" => $detail->start_date,
                    "finishDate" => $detail->finish_date,
                    "price" => $detail->service_price,
                );
                $i++;
            }
        }
        $paymentDetail["serviceList"] = $serviceList;

        return $paymentDetail;
    }

    public static function userPaymentNotify($paymentMasterDetail)
    {
        $ci = &get_instance();
        $sendMessageResponse = array("response" => 0,"message" => 'Su mensaje no pudo ser enviado.');
        $template = $ci->load->view("email_template/mail_pago_notificacion_admin", array(), TRUE);
        $contenido = str_replace('@@concepto', $paymentMasterDetail["paymentStatement"], $template);
        $contenido = str_replace('@@usuario', $paymentMasterDetail["userFullName"], $contenido);
        $contenido = str_replace('@@telefono', $paymentMasterDetail["userPhone"], $contenido);
        $contenido = str_replace('@@celular', $paymentMasterDetail["userCellPhone"], $contenido);
        $contenido = str_replace('@@email', $paymentMasterDetail["userEmail"], $contenido);
        $contenido = str_replace('@@pag_id', $paymentMasterDetail["paymentId"], $contenido);
        $contenido = str_replace('@@monto', $paymentMasterDetail["paymentAmount"], $contenido);
        $contenido = str_replace('@@tipo', $paymentMasterDetail["userType"], $contenido);
        $contenido = str_replace('@@fac_name', $paymentMasterDetail["paymentName"], $contenido);
        $contenido = str_replace('@@fac_nit', $paymentMasterDetail["paymentNit"], $contenido);
        $emailHandler = new EmailHandler();
        $email = $emailHandler->initialize();
        $email->from(EmailHandler::getSender(), 'ToqueElTimbre');
        $paymentEmail = Private_Controller::getEmail('pagos');
        $email->to(array($paymentMasterDetail["userEmail"], $paymentEmail));
        $email->subject("Nuevo Pago #" . $paymentMasterDetail["paymentId"]);
        $email->message($contenido);
        try
        {
            if($email->Send())
            {
                $sendMessageResponse['response'] = 1;
                $sendMessageResponse['message'] = 'Su mensaje fue enviado correctamente';
            }
        }
        catch (Exception $e)
        {
            $sendMessageResponse['response'] = 0;
            $sendMessageResponse['message'] = $e->getMessage();
        }
        return $sendMessageResponse;
    }

    public static function sendPaymentId($paymentMasterDetail)
    {
        $ci = &get_instance();
        $sendMessageResponse = array("response" => 0,"message" => 'Su mensaje no pudo ser enviado.');

        $template = $ci->load->view("email_template/codigo_pago_anuncios", array(), TRUE);
        $contenido = str_replace('@@inmueble', $paymentMasterDetail["paymentStatement"], $template);
        $contenido = str_replace('@@pag_id', $paymentMasterDetail["paymentId"], $contenido);
        $contenido = str_replace('@@monto', $paymentMasterDetail["paymentAmount"], $contenido);
        $emailHandler = new EmailHandler();
        $email = $emailHandler->initialize();
        $email->from(EmailHandler::getSender(), 'ToqueElTimbre');
        $email->to($paymentMasterDetail["userEmail"]);
        $email->subject("Nuevo Pago #" . $paymentMasterDetail["paymentId"]);
        $email->message($contenido);
        try
        {
            if($email->Send())
            {
                $sendMessageResponse['response'] = 1;
                $sendMessageResponse['message'] = 'Su mensaje fue enviado correctamente';
            }
        }
        catch (Exception $e)
        {
            $sendMessageResponse['response'] = 0;
            $sendMessageResponse['message'] = $e->getMessage();
        }
        return $sendMessageResponse;
    }

    public static function notifyPaymentSettled($paymentMasterDetail)
    {
        $ci = &get_instance();
        $sendMessageResponse = array("response" => 0,"message" => 'Su mensaje no pudo ser enviado.');

        $template = $ci->load->view("email_template/confirmacion_pago", array(), TRUE);
        $contenido = str_replace('@@monto', $paymentMasterDetail["paymentAmount"], $template);
        $contenido = str_replace('@@inmueble', $paymentMasterDetail["propertyName"], $contenido);
        $emailHandler = new EmailHandler();
        $email = $emailHandler->initialize();
        $email->from(EmailHandler::getSender(), 'ToqueElTimbre');
        $email->to($paymentMasterDetail["userEmail"]);
        $email->subject("Pago Confirmado");
        $email->message($contenido);
        try
        {
            if($email->Send())
            {
                $sendMessageResponse['response'] = 1;
                $sendMessageResponse['message'] = 'Su mensaje fue enviado correctamente';
            }
        }
        catch (Exception $e)
        {
            $sendMessageResponse['response'] = 0;
            $sendMessageResponse['message'] = $e->getMessage();
        }
        return $sendMessageResponse;
    }
}