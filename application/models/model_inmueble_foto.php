<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_inmueble_foto extends CI_Model {

    const TABLE_ID = "fot_id";
    const TABLE_NAME = "inmueble_foto";

    private $table = 'inmueble_foto';
    private $id = 'fot_id';

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    /**
     * Gets all results for this model but get a one result by id too.
     *
     * @param bool $id
     * @return []
     */
    function get_all($id = FALSE) {
        if ($id === FALSE) {
            $query = $this->db->get($this->table);
            return $query->result_array();
        }

        $query = $this->db->get_where($this->table, array($this->id => $id));
        return $query->row_array();
    }

    /**
     * Add new Model
     *
     * @param $data
     * @return mixed
     */
    function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    /**
     * Update Model
     *
     * @param $id
     * @param $data
     * @return mixed
     */
    function update($id, $data) {
        $this->db->where($this->id, $id);
        return $this->db->update($this->table, $data);
    }

    /**
     * Delete Model
     *
     * @param $id
     * @return mixed
     */
    function delete($id) {
        return $this->db->delete($this->table, array($this->id => $id));
    }

    function delete_plano($id) {
        return $this->db->delete('inmueble_planos', array('id' => $id));
    }

    function get_fotos($id) {
        $sql = "
            SELECT *
            FROM
                inmueble_foto inm_fot
            WHERE
                inm_fot.fot_inm_id = $id
            ORDER BY fot_order ASC
            ";

        return $this->db->query($sql)->result();
    }

    function get_order_max_foto(){
        $sql = "SELECT MAX(inm.`fot_order`) AS max_order FROM inmueble_foto inm";

        $row = $this->db->query($sql)->row();
        return $row->max_order;
    }

    function get_planos($id) {
        $sql = "
            SELECT *
            FROM
                inmueble_planos
            WHERE
                inmueble_id = $id
            ORDER BY id ASC
            ";

        return $this->db->query($sql)->result();
    }

    public static function getByNameAndPropertyId($imageName, $propertyId)
    {
        $ci=&get_instance();
        $ci->load->database();
        $sql = "select * from ".self::TABLE_NAME." where fot_archivo = ".$ci->db->escape($imageName)." and fot_inm_id = ".$ci->db->escape($propertyId);
        $query = $ci->db->query($sql);
        $response = $query->row(0, 'model_inmueble_foto');
        return $response;
    }
}