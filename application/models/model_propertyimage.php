<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_propertyimage extends MY_Model {

    const TABLE_ID = "fot_id";
    const TABLE_NAME = "inmueble_foto";

    protected $_imageName;
    protected $_propertyId;
    protected $_order;
    protected $_description;
    //Due to autoload is necessary to set the default data as empty string
    function __construct($imageName = "", $propertyId = "", $order = "", $description = "")
    {
        parent::__construct();
        $this->_imageName = $imageName;
        $this->_propertyId = $propertyId;
        $this->_order = $order;
        $this->_description = $description;
    }

    /**
     * Parse the current object to key => value array
     * @return array
     */
    public function toArray()
    {
        $tableAttributes = array(
                                "fot_id" => $this->_id,
                                "fot_archivo" => $this->_imageName,
                                "fot_inm_id" => $this->_propertyId,
                                "fot_order" => $this->_order,
                                "fot_descripcion" => $this->_description
                                );
        return $tableAttributes;
    }

    /**
     * @param $className
     * @param $object
     * @return object
     */
    protected static function recast($className, $object)
    {
        $response =  null;
        if ($object instanceof stdClass)
        {
            if (!class_exists($className))
                throw new InvalidArgumentException(sprintf('Inexistant class %s.', $className));

            //Let's set the values to payment object using the data from stdObject
            $instance = new $className(
                    $object->fot_archivo,
                    $object->fot_inm_id,
                    $object->fot_order,
                    $object->fot_descripcion
            );
            $instance->_id = $object->fot_id;
            $response = $instance;
        }
        return $response;
    }

    ################################################################################################# BEGIN - GETTERS

    public function getImageName()
    {
        return $this->_imageName;
    }

    public static function getByPropertyId($propertyId)
    {
        $ci=&get_instance();
        $ci->load->database();
        $sql = "
            select * from ".self::TABLE_NAME." where fot_inm_id = ".$ci->db->escape($propertyId)." order by fot_order
        ";
        $query = $ci->db->query($sql);
        $response = self::recastArray(get_called_class(),$query->result());
        return $response;
    }
    ################################################################################################# END - GETTERS



    ################################################################################################# BEGIN - SETTERS

    public function setImageName($imageName)
    {
        $file = new File_Handler($this->_imageName);
        $file->delete();
        $this->_imageName = $imageName;
    }

    public function setOrder($order)
    {
        $this->_order = $order;
    }

    public function setDescription($description)
    {
        $this->_description = $description;
    }
    ################################################################################################# END - SETTERS
    public static function search($stateId, $text, $limit, $offset, $orderBy = null, $orderType = 'asc', $colsArray = null)
    {
        if ($orderBy === null)
        {
            $orderBy = self::TABLE_ID;
        }
        $ci = &get_instance();
        $ci->load->database();

        $sql = 'select * from ' . self::TABLE_NAME;
        $sql .= ' where (';
        foreach ($colsArray as $var)
        {
            $sql .= ' ' . $var . ' like \'%' . $text . '%\' or ';
        }

        $sql = substr($sql, 0, -3);
        $sql .= ') and ciu_dep_id ='.$ci->db->escape($stateId).' order by ' . $orderBy . ' ' . $orderType . ' limit ' . $limit . ' offset ' . $offset;
        $query = $ci->db->query($sql);
        return self::recastArray(get_called_class(), $query->result());
    }

    public function delete($makePhysicalDelete = FALSE)
    {
        $file = new File_Handler($this->_imageName);
        $file->delete();
        parent::delete($makePhysicalDelete);
    }

    public static function getByArrayIds($arrayIds = array())
    {
        $ci = &get_instance();
        $ci->load->database();

        $escapedIds = "";
        foreach ($arrayIds as $id)
        {


            $escapedIds .= $ci->db->escape($id).", ";

        }

        $escapedIds = substr($escapedIds,0,-2);
        $sql = "
        select * from inmueble_foto WHERE fot_id in (".$escapedIds.");
        ";
        $query = $ci->db->query($sql);
        return self::recastArray(get_called_class(), $query->result());
    }

    public static function updateBatch($list = array())
    {
        $ci=&get_instance();
        $ci->load->database();
        $ci->db->update_batch(static::TABLE_NAME, $list, static::TABLE_ID);
    }
}