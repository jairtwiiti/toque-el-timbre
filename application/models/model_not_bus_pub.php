<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_not_bus_pub extends CI_Model {

    private $table = 'not_bus_pub';

    function __construct() {
        parent::__construct();
        $this->load->database();
    }
}