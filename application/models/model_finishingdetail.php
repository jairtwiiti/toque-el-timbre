<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_finishingdetail extends MY_Model {

    const TABLE_ID = "aca_id";
    const TABLE_NAME = "acabado";

    protected $_description;
    protected $_image;
    protected $_projectId;
    //Due to autoload is necessary to set the default data as empty string
    function __construct($description = "", $image = "", $projectId = "")
    {
        parent::__construct();
        $this->_description = $description;
        $this->_image = $image;
        $this->_projectId = $projectId;
    }

    /**
     * Parse the current object to key => value array
     * @return array
     */
    public function toArray()
    {
        $tableAttributes = array(
                                "aca_id" => $this->_id,
                                "aca_descripcion" => $this->_description,
                                "aca_imagen" => $this->_image,
                                "aca_proy_id" => $this->_projectId,
                                );
        return $tableAttributes;
    }

    /**
     * @param $className
     * @param $object
     * @return object
     */
    protected static function recast($className, $object)
    {
        $response =  null;
        if ($object instanceof stdClass)
        {
            if (!class_exists($className))
                throw new InvalidArgumentException(sprintf('Inexistant class %s.', $className));

            //Let's set the values to payment object using the data from stdObject
            $instance = new $className(
                    $object->aca_descripcion,
                    $object->aca_imagen,
                    $object->aca_proy_id
            );
            $instance->_id = $object->aca_id;
            $response = $instance;
        }
        return $response;
    }
    ################################################################################################# BEGIN - GETTERS

    public function getImageName()
    {
        return $this->_imageName;
    }

    public static function getByProjectId($projectId)
    {
        $ci=&get_instance();
        $ci->load->database();
        $sql = "
            select * from ".self::TABLE_NAME." where aca_proy_id = ".$ci->db->escape($projectId)."
        ";
        $query = $ci->db->query($sql);
        $response = self::recastArray(get_called_class(),$query->result());
        return $response;
    }
    ################################################################################################# END - GETTERS



    ################################################################################################# BEGIN - SETTERS

    public function setDescription($description)
    {
        $this->_description = $description;
    }

    public function setImage($image)
    {
        $file = new File_Handler($this->_image,"projectImage");
        $file->delete();
        $this->_image = $image;
    }

    ################################################################################################# END - SETTERS
    public static function search($stateId, $text, $limit, $offset, $orderBy = null, $orderType = 'asc', $colsArray = null)
    {
        if ($orderBy === null)
        {
            $orderBy = self::TABLE_ID;
        }
        $ci = &get_instance();
        $ci->load->database();

        $sql = 'select * from ' . self::TABLE_NAME;
        $sql .= ' where (';
        foreach ($colsArray as $var)
        {
            $sql .= ' ' . $var . ' like \'%' . $text . '%\' or ';
        }

        $sql = substr($sql, 0, -3);
        $sql .= ') and ciu_dep_id ='.$ci->db->escape($stateId).' order by ' . $orderBy . ' ' . $orderType . ' limit ' . $limit . ' offset ' . $offset;
        $query = $ci->db->query($sql);
        return self::recastArray(get_called_class(), $query->result());
    }

    public function delete($makePhysicalDelete = FALSE)
    {
        $file = new File_Handler($this->_image,"projectImage");
        $file->delete();
        parent::delete($makePhysicalDelete);
    }
}