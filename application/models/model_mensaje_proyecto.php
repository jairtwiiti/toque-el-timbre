<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_mensaje_proyecto extends CI_Model {

    private $table = 'mensaje_proyecto';
    private $id = 'men_id';

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    /**
     * Gets all results for this model but get a one result by id too.
     *
     * @param bool $id
     * @return []
     */
    function get_all($id = FALSE) {
        if ($id === FALSE) {
            $query = $this->db->get($this->table);
            return $query->result_array();
        }

        $query = $this->db->get_where($this->table, array($this->id => $id));
        return $query->row_array();
    }

    /**
     * Add new Model
     *
     * @param $data
     * @return mixed
     */
    function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    /**
     * Update Model
     *
     * @param $id
     * @param $data
     * @return mixed
     */
    function update($id, $data) {
        $this->db->where($this->id, $id);
        return $this->db->update($this->table, $data);
    }

    /**
     * Delete Model
     *
     * @param $id
     * @return mixed
     */
    function delete($id) {
        return $this->db->update($this->table, array($this->id => $id));
    }

    function get_message_by_id($men_id){
        $query = $this->db->select('mensaje_proyecto.*, proyecto.proy_nombre as inm_nombre')
            ->from('mensaje_proyecto')
            ->join('proyecto', 'proyecto.proy_id = mensaje_proyecto.men_proy_id', 'inner')
            ->where('mensaje_proyecto.men_id', $men_id)
            ->get();
        return $query->row();
    }
}