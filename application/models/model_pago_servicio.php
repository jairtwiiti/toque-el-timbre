<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_pago_servicio extends CI_Model {

    private $table = 'pago_servicio';

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

	/**
	 * Add new Model
	 *
	 * @param $data
	 * @return mixed
	 */
	function insert($data) {
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

    function get_name_services($pag_id){
        $sql = "
        SELECT ser.`ser_descripcion` FROM pago_servicio ps
        INNER JOIN servicios ser ON (ser.`ser_id` = ps.`ser_id`)
        WHERE ps.`pag_id` = '$pag_id'
        ";
        return $this->db->query($sql)->result();
    }

    function update($pag_id, $ser_id, $data) {
        $sql = "
        UPDATE pago_servicio SET
        fech_ini = '".$data['fech_ini']."',
        fech_fin = '".$data['fech_fin']."'
        WHERE pag_id = '$pag_id' AND ser_id = $ser_id
        ";
        /*$this->db->where('pag_id', $pag_id);
        $this->db->where('ser_id', $ser_id);
        return $this->db->update($this->table, $data);*/
        $this->db->query($sql);

    }
}