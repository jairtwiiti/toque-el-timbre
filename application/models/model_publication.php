<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_publication extends MY_Model {

    const TABLE_ID = "pub_id";
    const TABLE_NAME = "publicacion";

    const APPROVED = "Aprobado";
    const NOT_APPROVED = "No Aprobado";
    const CANCELED = "Cancelado";
    const DELETED = "Eliminado";

    protected $_created;
    protected $_startValidity;
    protected $_finishValidity;
    protected $_amount;
    protected $_propertyId;
    protected $_userId;
    protected $_status;
    protected $_observations;
    protected $_sent;
    protected $_sent2;
    protected $_renewed;



    //Due to autoload is necessary to set the default data as empty string
    function __construct($created = "", $startValidity = "", $finishValidity = "", $amount = "", $propertyId = "", $userId = "", $status = "", $observations = "", $sent = "",
                        $sent2 = "", $renewed = "")
    {
        parent::__construct();
        $this->_created = $created;
        $this->_startValidity = $startValidity;
        $this->_finishValidity = $finishValidity;
        $this->_amount = $amount;
        $this->_propertyId = $propertyId;
        $this->_userId = $userId;
        $this->_status = $status;
        $this->_observations = $observations;
        $this->_sent = $sent;
        $this->_sent2 = $sent2;
        $this->_renewed = $renewed;
    }

    public function toArray()
    {
        $tableAttributes = array(
                            "pub_id" => $this->_id,
                            "pub_creado" => $this->_created,
                            "pub_vig_ini" => $this->_startValidity,
                            "pub_vig_fin" => $this->_finishValidity,
                            "pub_monto" => $this->_amount,
                            "pub_inm_id" => $this->_propertyId,
                            "pub_usu_id" => $this->_userId,
                            "pub_estado" => $this->_status,
                            "pub_obs" => $this->_observations,
                            "pub_enviado" => $this->_sent,
                            "pub_enviado_1" => $this->_sent2,
                            "pub_renovado" => $this->_renewed
                                );
        return $tableAttributes;
    }

    /**
     * @param $className
     * @param $object
     * @return null
     */
    protected static function recast($className, $object)
    {
        $response =  null;
        if ($object instanceof stdClass)
        {
            if (!class_exists($className))
                throw new InvalidArgumentException(sprintf('Inexistant class %s.', $className));

            //Let's set the values to payment object using the data from stdObject
            $instance = new $className(
                        $object->pub_creado,
                        $object->pub_vig_ini,
                        $object->pub_vig_fin,
                        $object->pub_monto,
                        $object->pub_inm_id,
                        $object->pub_usu_id,
                        $object->pub_estado,
                        $object->pub_obs,
                        $object->pub_enviado,
                        $object->pub_enviado_1,
                        $object->pub_renovado
                        );
            $instance->_id = $object->pub_id;
            $response = $instance;
        }
        return $response;
    }

    ################################################################################################# BEGIN - GETTERS

    public function getUser()
    {
        return model_user::getById($this->_userId);
    }

    public static function getByPropertyId($id)
    {
        $ci=&get_instance();
        $ci->load->database();
        $sql = "
            select * from ".static::TABLE_NAME." where pub_inm_id = ".$ci->db->escape($id)."
        ";
        $query = $ci->db->query($sql);
        $response = static::recast(get_called_class(),$query->row());
        return $response;
    }

    public function getFinishValidity()
    {
        return $this->_finishValidity;
    }
    ################################################################################################# END - GETTERS

    ################################################################################################# BEGIN - SETTERS
    /**
     * Set a string that contains observations about the current instance of publication
     * @param $observations
     */
    public function setObservations($observations)
    {
        $this->_observations = $observations;
    }

    /**
     * @param $date
     */
    public function setFinishValidity($date)
    {
        $this->_finishValidity = $date;
    }

    public function setStatus($status)
    {
        $this->_status = $status;
    }
    ################################################################################################# END - SETTERS
    /**
     * Update the validity date from publication taking account the payment service that has the most long expiration dates
     */
    public function updateValidityBasedOnMostLongPaymentService()
    {
        /** @var model_paymentservice $paymentService */
        $paymentService = model_paymentservice::getByMostLongExpirationDateAndPublicationId($this->_id);
        if($paymentService instanceof model_paymentservice && $paymentService->getFinishDate() > $this->_finishValidity)
        {
            $this->_finishValidity = $paymentService->getFinishDate();
        }
        $this->_status = static::APPROVED;
        $this->save();
    }

    /************************************************************************************** BEGIN - DATATABLE AJAX METHODS */
    /**
     * @return mixed
     */
    public static function countAll()
    {
        $ci = &get_instance();
        $ci->load->database();

        $sql = '
                select count(' . static::TABLE_ID. ') as total
                from ' . static::TABLE_NAME.'
                left join publicacion on pub_id = pag_pub_id
                left join usuario on usu_id = pub_usu_id
                left join publicacion on pub_id = pub_pub_id';

        $query = $ci->db->query($sql);
        $totalCount = $query->row()->total;
        return $totalCount;
    }

    /**
     * @param $limit
     * @param $offset
     * @param null $orderBy
     * @param string $orderType
     * @return mixed
     */
    public static function getAll($limit = 1000, $offset = 0, $orderBy = null, $orderType = 'asc')
    {
        if ($orderBy === null)
        {
            $orderBy = static::TABLE_ID;
        }
        $ci = &get_instance();
        $ci->load->database();

        $sql = 'select '.static::_dataTableColumns().', CONCAT(usu_nombre," ",usu_apellido) as user_full_name from ' . static::TABLE_NAME . '
                left join publicacion on pub_id = pag_pub_id
                left join usuario on usu_id = pub_usu_id
                left join publicacion on pub_id = pub_pub_id
                order by ' . $orderBy . ' ' . $orderType . ' limit ' . $limit . ' offset ' . $offset;

        $query = $ci->db->query($sql);
        $result = $query->result();
        return $result;
    }

    public static function search($text, $limit, $offset, $orderBy = null, $orderType = 'asc', $colsArray = null)
    {
        if ($orderBy === null)
        {
            $orderBy = static::TABLE_ID;
        }
        $ci = &get_instance();
        $ci->load->database();

        $sql = 'select '.static::_dataTableColumns().', CONCAT(usu_nombre," ",usu_apellido) as user_full_name from ' . static::TABLE_NAME.'
                left join publicacion on pub_id = pag_pub_id
                left join usuario on usu_id = pub_usu_id
                left join publicacion on pub_id = pub_pub_id';
        $sql .= ' where (';
        foreach ($colsArray as $var)
        {
            if($var == "user_full_name")
            {
                $var = ' CONCAT(usu_nombre," ",usu_apellido) ';
            }
            $sql .= ' ' . $var . ' like \'%' . $text . '%\' or ';
        }

        $sql = substr($sql, 0, -3);
        $sql .= ') order by ' . $orderBy . ' ' . $orderType . ' limit ' . $limit . ' offset ' . $offset;

        $query = $ci->db->query($sql);
        return $query->result();
    }

    public static function searchTotalCount($text, $colsArray = null)
    {
        $ci = &get_instance();
        $ci->load->database();

        $sql = 'select count(' . static::TABLE_ID . ') as total from ' . static::TABLE_NAME.'
        left join publicacion on pub_id = pag_pub_id
        left join usuario on usu_id = pub_usu_id
        left join publicacion on pub_id = pub_pub_id';
        $sql .= ' where (';

        foreach ($colsArray as $var)
        {
            if($var == "user_full_name")
            {
                $var = ' CONCAT(usu_nombre," ",usu_apellido) ';
            }
            $sql .= ' ' . $var . ' like \'%' . $text . '%\' or ';
        }

        $sql = substr($sql, 0, -3);
        $sql .= ')';

        $query = $ci->db->query($sql);
        $totalCount = $query->row()->total;
        return $totalCount;
    }

    private static function _dataTableColumns()
    {
        $columns = " pag_id, pag_cod_transaccion, inm_nombre, pag_fecha_pagado, usu_email, usu_tipo, pag_monto, pag_entidad, pag_concepto, pag_estado ";
        return $columns;
    }
    /*********************************************************************************** END - DATATABLE AJAX METHODS */

    public static function getTotalCreatedByDate()
    {
        $ci = &get_instance();
        $ci->load->database();
        $sql = "
        SELECT
            DATE_FORMAT(pub_creado, '%Y/%m/%d') date,
            count(*) total_created
        FROM
            publicacion
        GROUP BY
            date
        ";
        $query = $ci->db->query($sql);
        return $query->result();
    }

    public static function getAllPublicationsRegisteredByDateRange($startDate = "", $endDate = "")
    {
        if($startDate == "" || $endDate == "")
        {
            $startDate = date("Y-m")."-01";
            $endDate = date("Y-m-t", strtotime($startDate));
        }

        $ci=&get_instance();
        $ci->load->database();
        $sql = "
            SELECT
                DATE_FORMAT(pub_creado,'%Y-%m-%d') date,
                count(".static::TABLE_ID.") 'value'
            FROM
                ".static::TABLE_NAME."
            where
              pub_creado BETWEEN ".$ci->db->escape($startDate)." and ".$ci->db->escape($endDate)."
            GROUP BY date
        ";
        $query = $ci->db->query($sql);
        $response = $query->result();
        return $response;
    }

    public static function getAllPublicationsActivityByDateRange($startDate = "", $endDate = "", $showOnlyVisiblePublications = TRUE)
    {
        if($startDate == "" || $endDate == "")
        {
            $startDate = date("Y-m")."-01";
            $endDate = date("Y-m-t", strtotime($startDate));
        }
        $visiblePublicationsSearchCriteria = "";
        if($showOnlyVisiblePublications)
        {
            $visiblePublicationsSearchCriteria = " and inm_publicado = 'Si' ";
        }

        $ci=&get_instance();
        $ci->load->database();
        $setTotalProjects = "set @totalProjects = (select count(proy_id) total from proyecto where proy_estado = 'Aprobado');";
        $setSqlVariable = "set @i = -1";
        $sql = "
        select
                dateRange.date,
                count(pubVigent.inm_id)+@totalProjects Actividad,
                IFNULL(start_list.start_validity,0) Creadas,
                IFNULL(end_list.end_validity,0) Vencidas
        from
        (
            SELECT
                DATE(ADDDATE(".$ci->db->escape($startDate).", INTERVAL @i:=@i+1 DAY)) AS date
            FROM
                publicacion
            HAVING
                @i < DATEDIFF(".$ci->db->escape($endDate).", ".$ci->db->escape($startDate).")
        ) dateRange
        left join (
            SELECT
                    DATE_FORMAT(pub_vig_ini,\"%Y-%m-%d\") date,
                    count(pub_vig_ini) 'start_validity'
                FROM
                    publicacion
                                where
                                    (".$ci->db->escape($startDate)."<= pub_vig_fin) and (".$ci->db->escape($endDate)." >= pub_vig_ini)
                GROUP BY date
        ) start_list on start_list.date = dateRange.date
        left join (
            SELECT
                    DATE_FORMAT(DATE_ADD(pub_vig_fin, INTERVAL 1 DAY),\"%Y-%m-%d\") date,
                    count(DATE_ADD(pub_vig_fin, INTERVAL 1 DAY)) 'end_validity'
                FROM
                    publicacion
                                where
                                    (".$ci->db->escape($startDate)."<= DATE_ADD(pub_vig_fin, INTERVAL 1 DAY)) and (".$ci->db->escape($endDate)." >= pub_vig_ini)
                GROUP BY date
        ) end_list on end_list.date = dateRange.date
        LEFT JOIN (
            select
                *
            from
                publicacion
            LEFT JOIN inmueble on pub_inm_id = inm_id
            WHERE
                (".$ci->db->escape($startDate)."<= pub_vig_fin) and (".$ci->db->escape($endDate)." >= pub_vig_ini)
                and	pub_estado != \"Eliminado\"
                ".$visiblePublicationsSearchCriteria."
            ORDER BY pub_vig_ini
        ) pubVigent on dateRange.date <= pubVigent.pub_vig_fin and dateRange.date >= pubVigent.pub_vig_ini
        GROUP BY dateRange.date
        order by dateRange.date
        ";
        $ci->db->query($setTotalProjects);
        $ci->db->query($setSqlVariable);
        $query = $ci->db->query($sql);
        $response = $query->result();
        return $response;
    }
    public static function getAllPublicationsActivityByDateRangeComposedList($startDate = "", $endDate = "", $showOnlyVisiblePublications = TRUE)
    {
        $ci=&get_instance();
        $ci->load->database();

        if($startDate == "" || $endDate == "")
        {
            $startDate = date("Y-m")."-01";
            $endDate = date("Y-m-t", strtotime($startDate));
        }
        $visiblePublicationsSearchCriteria = "";
        if($showOnlyVisiblePublications)
        {
            $visiblePublicationsSearchCriteria = " and inm_publicado = 'Si' ";
        }

        $sql = "
            #selecionar todos los inmuebles que esten en alquiler
            #seleccionar casas en venta
            SELECT
                CONCAT(usu_nombre,' ',usu_apellido) 'Nombre Usuario',
                usu_email 'Email',
                cat_nombre 'Tipo de inmueble',
                for_descripcion 'Tipo de transaccion',
                inm_nombre 'Nombre del inmueble',
                inm_direccion 'Direccion',
                inm_superficie 'Superficie',
                inm_tipo_superficie 'Tipo de superficie',
                inm_precio 'Precio',
                mon_abreviado 'Moneda',
                REPLACE(FORMAT((inm_precio/inm_superficie),2),',','') 'Precio x Mt2',
                eca_valor 'Superficie construida',
                REPLACE(FORMAT((inm_precio/eca_valor),2),',','') 'Superficie const. precio x Mt2',
                inm_publicado 'Publicado',
                pub_estado 'Estado de la publicacion',
                -- if(CURDATE() < pub_vig_fin,0,1) expired,
                count(men_id) 'Mensajes',
	            inm_visitas 'Visitas',
	            pub_vig_ini 'Fecha Inicio',
                pub_vig_fin 'Fecha Fin',
                LOWER(CONCAT('https://toqueeltimbre.com/buscar/',REPLACE(cat_nombre, ' ', '-'),'-en-',for_descripcion,'-en-',REPLACE(dep_nombre, ' ', '-'),'/',inm_seo)) link

            FROM
                inmueble
            LEFT JOIN publicacion on pub_inm_id = inm_id
            LEFT JOIN moneda on inm_mon_id = mon_id
            LEFT JOIN categoria on cat_id = inm_cat_id
            LEFT JOIN forma on for_id = inm_for_id
            LEFT JOIN inmueble_caracteristica on inm_id = eca_inm_id and eca_car_id = 11
            LEFT JOIN ciudad on inm_ciu_id = ciu_id
            LEFT JOIN departamento on ciu_dep_id = dep_id
            LEFT JOIN mensaje on men_inm_id = inm_id
            LEFT JOIN usuario on pub_usu_id = usu_id

            where
            (".$ci->db->escape($startDate)."<= pub_vig_fin) and (".$ci->db->escape($endDate)." >= pub_vig_ini)
            and	pub_estado != 'Eliminado'
            ".$visiblePublicationsSearchCriteria."
            GROUP BY inm_id
            ORDER BY pub_vig_ini
        ";
        $query = $ci->db->query($sql);
        $response = $query->result();
        return $response;
    }

    public static  function getCreatedAndExpiredPublicationsComposedList($startDate = "", $endDate = "")
    {
        $ci=&get_instance();
        $ci->load->database();

        if($startDate == "" || $endDate == "")
        {
            $startDate = date("Y-m")."-01";
            $endDate = date("Y-m-t", strtotime($startDate));
        }

        $sql = "
            SELECT
            CASE WHEN (pub_vig_ini BETWEEN ".$ci->db->escape($startDate)." and ".$ci->db->escape($endDate).") THEN 'Created' WHEN (pub_vig_fin BETWEEN ".$ci->db->escape($startDate)." and ".$ci->db->escape($endDate).") THEN 'Expired' END 'Activity',
            CONCAT(usu_nombre,' ' ,usu_apellido) 'Usuario nombre completo',
            usu_email 'Usuario email',
            usu_tipo 'Usuario tipo',
            pub_id 'ID publicacion',
            cat_nombre 'Tipo de inmueble',
            for_descripcion 'Tipo de transaccion',
            inm_nombre 'Nombre del inmueble',
            inm_direccion 'Direccion',
            inm_superficie 'Superficie',
            inm_tipo_superficie 'Tipo de superficie',
            inm_precio 'Precio',
            mon_abreviado 'Moneda',
            REPLACE(FORMAT((inm_precio/inm_superficie), 2), ',', '') 'Precio x Mt2',
            eca_valor 'Superficie construida',
            REPLACE(FORMAT((inm_precio/eca_valor), 2), ',', '') 'Superficie const. precio x Mt2',
            inm_publicado 'Publicado',
            pub_estado 'Estado de la publicacion',
            -- if(CURDATE() < pub_vig_fin,0,1) expired,
            count(men_id) 'Mensajes',
            inm_visitas 'Visitas',
            pub_vig_ini 'Fecha Inicio',
            pub_vig_fin 'Fecha Fin',
            LOWER(CONCAT('https://toqueeltimbre.com/buscar/',REPLACE(cat_nombre, ' ', '-'),'-en-',for_descripcion,'-en-',REPLACE(dep_nombre, ' ', '-'),'/',inm_seo)) link
        FROM
            inmueble
        LEFT JOIN publicacion on pub_inm_id = inm_id
        LEFT JOIN moneda on inm_mon_id = mon_id
        LEFT JOIN categoria on cat_id = inm_cat_id
        LEFT JOIN forma on for_id = inm_for_id
        LEFT JOIN inmueble_caracteristica on inm_id = eca_inm_id and eca_car_id = 11
        LEFT JOIN ciudad on inm_ciu_id = ciu_id
        LEFT JOIN departamento on ciu_dep_id = dep_id
        LEFT JOIN mensaje on men_inm_id = inm_id
        LEFT JOIN usuario on pub_usu_id = usu_id
        where
            (pub_vig_ini BETWEEN ".$ci->db->escape($startDate)." and ".$ci->db->escape($endDate).")
            or (pub_vig_fin BETWEEN ".$ci->db->escape($startDate)." and ".$ci->db->escape($endDate).")
        GROUP BY pub_id
        ";
        $query = $ci->db->query($sql);
        $response = $query->result();
        return $response;
    }

    public static function getAllPublicationsByDateRangeAndVigency($startDate = "", $endDate = "")
    {
        $ci=&get_instance();
        $ci->load->database();

        if($startDate == "" || $endDate == "")
        {
            $startDate = date("Y-m")."-01";
            $endDate = date("Y-m-t", strtotime($startDate));
        }

        $sql = "
        #selecionar todos los inmuebles que esten en alquiler
        #seleccionar casas en venta
        SELECT
            CONCAT(usu_nombre,' ',usu_apellido) 'Nombre Usuario',
            usu_email 'Email',
            usu_tipo 'Tipo usuario',
            cat_nombre 'Tipo de inmueble',
            for_descripcion 'Tipo de transaccion',
            inm_nombre 'Nombre del inmueble',
            inm_direccion 'Direccion',
            inm_superficie 'Superficie',
            inm_tipo_superficie 'Tipo de superficie',
            inm_precio 'Precio',
            mon_abreviado 'Moneda',
            REPLACE(FORMAT((inm_precio/inm_superficie),2),',','') 'Precio x Mt2',
            eca_valor 'Superficie construida',
            REPLACE(FORMAT((inm_precio/eca_valor),2),',','') 'Superficie const. precio x Mt2',
            inm_publicado 'Publicado',
            pub_estado 'Estado de la publicacion',
            -- if(CURDATE() < pub_vig_fin,0,1) expired,
            count(men_id) 'Mensajes',
            inm_visitas 'Visitas',
            pub_vig_ini 'Fecha Inicio',
            pub_vig_fin 'Fecha Fin',
            LOWER(CONCAT('https://toqueeltimbre.com/buscar/',REPLACE(cat_nombre, ' ', '-'),'-en-',for_descripcion,'-en-',REPLACE(dep_nombre, ' ', '-'),'/',inm_seo)) link

        FROM
            inmueble
        LEFT JOIN publicacion on pub_inm_id = inm_id
        LEFT JOIN moneda on inm_mon_id = mon_id
        LEFT JOIN categoria on cat_id = inm_cat_id
        LEFT JOIN forma on for_id = inm_for_id
        LEFT JOIN inmueble_caracteristica on inm_id = eca_inm_id and eca_car_id = 11
        LEFT JOIN ciudad on inm_ciu_id = ciu_id
        LEFT JOIN departamento on ciu_dep_id = dep_id
        LEFT JOIN mensaje on men_inm_id = inm_id
        LEFT JOIN usuario on pub_usu_id = usu_id
        where
        (".$ci->db->escape($startDate)."<= pub_vig_fin) and (".$ci->db->escape($endDate)." >= pub_vig_ini)
                and	pub_estado != 'Eliminado'
        GROUP BY inm_id
        ORDER BY pub_vig_ini
        ";
        $query = $ci->db->query($sql);
        $response = $query->result();
        return $response;
    }

    public static function getAllMessagesToPublicationsByDateRange($startDate = "", $endDate = "")
    {
        if($startDate == "" || $endDate == "")
        {
            $startDate = date("Y-m")."-01";
            $endDate = date("Y-m-t", strtotime($startDate));
        }

        $ci=&get_instance();
        $ci->load->database();
        $setSqlVariable = "set @i = -1";
        $sql = "
            select
                    dateRange.date,
                    IFNULL(particular.messages,0) Particular,
                    IFNULL(realState.messages,0) Inmobiliaria,
                    IFNULL(projects.messages,0) Constructora
            from
                    (
                        SELECT
                            DATE(ADDDATE(".$ci->db->escape($startDate).", INTERVAL @i:=@i+1 DAY)) AS date
                        FROM
                            publicacion
                        HAVING
                            @i < DATEDIFF(".$ci->db->escape($endDate).", ".$ci->db->escape($startDate).")
                    ) dateRange
            LEFT JOIN (
                    SELECT
                        DATE_FORMAT(men_fech, '%Y/%m/%d') date,
                        count(men_id) messages
                    FROM
                        mensaje
                    LEFT JOIN inmueble on inm_id = men_inm_id
                    LEFT JOIN publicacion on pub_inm_id = inm_id
                    LEFT JOIN usuario on pub_usu_id = usu_id
                    where
                    men_fech BETWEEN ".$ci->db->escape($startDate)." and ".$ci->db->escape($endDate)."
                    and usu_tipo = 'Particular'
                    GROUP BY date
                    ORDER BY messages desc
            ) particular on particular.date = dateRange.date
            LEFT JOIN (
                    SELECT
                        DATE_FORMAT(men_fech, '%Y/%m/%d') date,
                        count(men_id) messages
                    FROM
                        mensaje
                    LEFT JOIN inmueble on inm_id = men_inm_id
                    LEFT JOIN publicacion on pub_inm_id = inm_id
                    LEFT JOIN usuario on pub_usu_id = usu_id
                    where
                    men_fech BETWEEN ".$ci->db->escape($startDate)." and ".$ci->db->escape($endDate)."
                    and usu_tipo = 'Inmobiliaria'
                    GROUP BY date
                    ORDER BY messages desc
            ) realState on realState.date = dateRange.date
            left join (
                    SELECT
                        DATE_FORMAT(men_fech, '%Y/%m/%d') date,
                        count(men_id) messages
                    FROM
                        mensaje_proyecto
                    where
                    men_fech BETWEEN ".$ci->db->escape($startDate)." and ".$ci->db->escape($endDate)."
                    GROUP BY date
                    ORDER BY messages desc
            ) projects on projects.date = dateRange.date
            where
                (IFNULL(particular.messages,0) + IFNULL(projects.messages,0) + IFNULL(realState.messages,0)) > 0
            order by dateRange.date
        ";
        //echo"<pre>";var_dump($sql);exit;
        $ci->db->query($setSqlVariable);
        $query = $ci->db->query($sql);
        $response = $query->result();
        return $response;
    }

    public static function getAllMessagesToPublicationsByDateRangeDetailed($startDate = "", $endDate = "")
    {
        $ci=&get_instance();
        $ci->load->database();

        $sql = "
        SELECT
            inm_id,
            -- publicationMessages.men_id 'Id de mensaje',
            -- publicationMessages.messageType 'Tipo de mensaje',
            publicationMessages.men_fech,
            publicationMessages.men_nombre 'Nombre del remitente',
            publicationMessages.men_telefono 'Telefono del remitente',
            publicationMessages.men_email 'Email del remitente',
            publicationMessages.men_descripcion 'Mensaje del remitente',
            publicationMessages.men_leido 'Mensaje leido',
            CONCAT(publicationMessages.usu_nombre,' ',publicationMessages.usu_apellido) 'Nombre del usuario',
            publicationMessages.usu_email 'Email del usuario',
            publicationMessages.usu_tipo 'Tipo usuario',
            cat_nombre 'Tipo de inmueble',
            for_descripcion 'Tipo de transaccion',
            inm_nombre 'Nombre del inmueble',
            inm_direccion 'Direccion',
            inm_superficie 'Superficie',
            inm_tipo_superficie 'Tipo de superficie',
            inm_precio 'Precio',
            mon_abreviado 'Moneda',
            REPLACE(FORMAT((inm_precio/inm_superficie),2),',','') 'Precio x Mt2',
            eca_valor 'Superficie construida',
            REPLACE(FORMAT((inm_precio/eca_valor),2),',','') 'Superficie const. precio x Mt2',
            inm_publicado 'Publicado',
            pub_estado 'Estado de la publicacion',
            -- if(CURDATE() < pub_vig_fin,0,1) expired,
            inm_visitas 'Visitas',
            pub_vig_ini 'Fecha Inicio',
            pub_vig_fin 'Fecha Fin',
            LOWER(CONCAT('https://toqueeltimbre.com/buscar/',REPLACE(cat_nombre, ' ', '-'),'-en-',for_descripcion,'-en-',REPLACE(dep_nombre, ' ', '-'),'/',inm_seo)) link

        FROM
            inmueble
        LEFT JOIN publicacion on pub_inm_id = inm_id
        LEFT JOIN moneda on inm_mon_id = mon_id
        LEFT JOIN categoria on cat_id = inm_cat_id
        LEFT JOIN forma on for_id = inm_for_id
        LEFT JOIN inmueble_caracteristica on inm_id = eca_inm_id and eca_car_id = 11
        LEFT JOIN ciudad on inm_ciu_id = ciu_id
        LEFT JOIN departamento on ciu_dep_id = dep_id
        LEFT JOIN (
            SELECT
                'publication' messageType,
                men_id,
                men_inm_id,
                men_fech,
                men_nombre,
                men_telefono,
                men_email,
                men_descripcion,
                men_leido,
                usu_nombre,
                usu_apellido,
                usu_email,
                usu_tipo
            FROM
                mensaje
            LEFT JOIN inmueble on inm_id = men_inm_id
            LEFT JOIN publicacion on pub_inm_id = inm_id
            LEFT JOIN usuario on pub_usu_id = usu_id
            where
            men_fech BETWEEN ".$ci->db->escape($startDate)." and ".$ci->db->escape($endDate)."
            UNION
            SELECT
                'project' messageType,
                men_id,
                inm_id 'men_inm_id',
                men_fech,
                men_nombre,
                men_telefono,
                men_email,
                men_descripcion,
                men_leido,
                usu_nombre,
                usu_apellido,
                usu_email,
                usu_tipo
            FROM
                mensaje_proyecto
            LEFT JOIN (
                SELECT MAX(inm_id) 'inm_id', inm_proy_id
                from inmueble
                WHERE
                inm_proy_id != 0
                GROUP BY inm_proy_id
            )	inmuebleProject on inmuebleProject.inm_proy_id = men_proy_id
            LEFT JOIN proyecto on proy_id = men_proy_id
            LEFT JOIN usuario on proy_usu_id = usu_id
            where
            men_fech BETWEEN ".$ci->db->escape($startDate)." and ".$ci->db->escape($endDate)."
        )	publicationMessages on publicationMessages.men_inm_id = inm_id

        LEFT JOIN usuario on pub_usu_id = usu_id
        where
            publicationMessages.men_fech BETWEEN ".$ci->db->escape($startDate)." and ".$ci->db->escape($endDate)."
            and	pub_estado != 'Eliminado'
        GROUP BY men_id
        ORDER BY pub_vig_ini
        ";

        $query = $ci->db->query($sql);
        $response = $query->result();
        return $response;
    }

    /**
     * @param $publicationId
     * @return array
     */
    public static function getPublicationDetailByIdComposedData($publicationId)
    {
        $ci = &get_instance();
        $ci->load->database();

        $sql = "
        SELECT
            pub_id,
            inm_id,
            inm_nombre,
            mon_abreviado,
            pub_creado,
            pub_vig_ini,
            pub_vig_fin,
            inm_visitas,
            fot_archivo
        FROM
            publicacion
        LEFT JOIN inmueble on pub_inm_id = inm_id
        LEFT JOIN moneda on inm_mon_id = mon_id
        LEFT JOIN categoria on cat_id = inm_cat_id
        LEFT JOIN forma on for_id = inm_for_id
        LEFT JOIN ciudad on inm_ciu_id = ciu_id
        LEFT JOIN departamento on ciu_dep_id = dep_id
        LEFT JOIN inmueble_foto on fot_inm_id = inm_id
        where
        pub_id = ".$ci->db->escape($publicationId)."
        limit 1";
        $query = $ci->db->query($sql);
        $result = $query->result();
        return $result;
    }

    /************************************************************************************** BEGIN - PUBLIC AJAX METHODS */
    /**
     * @param URL_Decoder $urlDecoder
     * @param bool $showOnlyProjects
     * @return mixed
     */
    public static function countAllPublicResult(URL_Decoder $urlDecoder, $showOnlyProjects = FALSE)
    {
        $ci = &get_instance();
        $ci->load->database();

        $sql = "
              SELECT count(inm_id) as total FROM (
              ";
        $sql .= "
                    SELECT
                        inm_id
                    FROM
                        publicacion
                    LEFT JOIN inmueble ON pub_inm_id = inm_id
                    LEFT JOIN proyecto on inm_proy_id = proy_id
                    LEFT JOIN usuario on proy_usu_id = usu_id
                    LEFT JOIN ciudad on ciu_id = inm_ciu_id
                    LEFT JOIN departamento on dep_id = ciu_dep_id
                    LEFT JOIN moneda on mon_id = inm_mon_id
                    LEFT JOIN forma on inm_for_id = for_id
                    LEFT JOIN categoria on cat_id = inm_cat_id
                    WHERE
                        inm_proy_id != 0
                        and proy_estado = 'Aprobado'
                        and  " . $urlDecoder->getMainQueryFilter(TRUE) . "
                    group by proy_id";
        if(!$showOnlyProjects)
        {
            $sql .= "
                    UNION
                    SELECT
                        inm_id
                    FROM
                        inmueble_caracteristica_optimized
                    LEFT JOIN inmueble on inm_id = eca_inm_id
                    LEFT JOIN publicacion on pub_inm_id = inm_id
                    LEFT JOIN usuario on pub_usu_id = usu_id
                    LEFT JOIN ciudad on inm_ciu_id = ciu_id
                    LEFT JOIN departamento on dep_id = ciu_dep_id
                    LEFT JOIN moneda on mon_id = inm_mon_id
                    LEFT JOIN forma on inm_for_id = for_id
                    LEFT JOIN categoria on cat_id = inm_cat_id               
                    LEFT JOIN (
                                #Publications that has search service
                                SELECT
                                    p.pag_pub_id, s.ser_tipo
                                FROM
                                    pagos p
                                LEFT JOIN pago_servicio ps on p.pag_id = ps.pag_id AND CURDATE() BETWEEN ps.fech_ini AND ps.fech_fin
                                LEFT JOIN servicios s on s.ser_id = ps.ser_id
                                WHERE 
                                    p.pag_estado = 'Pagado'
                                    and s.ser_tipo = 'Busqueda'
                                    and pag_pub_id is not null
                                    and pag_concepto in('Mejora','Anuncio')
                            ) as searchService ON searchService.pag_pub_id = pub_id        
                    where 
                        pub_inm_id is not null
                        AND inm_precio >= 0	
                        and pub_estado = 'Aprobado'
                        and inm_publicado = 'Si'
                        and pub_vig_fin >= CURDATE()
                        and inm_proy_id = 0
                        and " . $urlDecoder->getMainQueryFilter();
        }

        $sql .= "               
                    ) as totalRows
            ";
        $query = $ci->db->query($sql);
        $totalCount = $query->row()->total;
        return $totalCount;
    }

    /**
     * @param URL_Decoder $urlDecoder get a sql from url
     * @param $limit
     * @param $offset
     * @param null $orderBy
     * @param string $orderType
     * @param bool $showOnlyProjects define if include the projects on query
     * @param bool $zoneStringArray list of zone names defined on search form
     * @return mixed
     */
    public static function getAllPublicResult(URL_Decoder $urlDecoder, $limit = 1000, $offset = 0, $orderBy = null, $orderType = 'asc', $showOnlyProjects = FALSE)
    {
        $ci = &get_instance();
        $ci->load->database();

        $sql = "
              SELECT
                    max(pub_id) 'pub_id', 
                    inm_id,
                    inm_direccion,
                    ciu_nombre,
                    dep_nombre,
                    inm_superficie,
                    inm_tipo_superficie,
                    mon_abreviado,
                    inm_precio,
                    cat_nombre,
                    for_descripcion,
                    pub_creado,                    
                    proyImage.proy_fot_imagen publicationImage,
                    proy_seo seo,
                    usu_nombre,
                    usu_tipo,
                    usu_certificado,
                    usu_foto,
                    usu_id,
                    usu_empresa,
                    inm_latitud,
                    inm_longitud,
                    inm_nombre,
                    proy_id,
                    proy_ubicacion,
                    '' as 'eca_inm_id',
                    '' as '4',
                    '' as '5',
                    '' as '6',
                    '' as '7',
                    '' as '8',
                    '' as '9',
                    '' as '10',
                    '' as '11',
                    '' as '13',
                    '' as '14',
                    '' as '15',
                    '' as '16',
                    '' as '19',
                    '' as '21',
                    '' as '22',
                    '' as '23',
                    '' as '24',
                    'Project' as 'Service',
                    '1' as 'group_order'
                FROM
                    publicacion
                LEFT JOIN inmueble ON pub_inm_id = inm_id
                LEFT JOIN proyecto on inm_proy_id = proy_id
                LEFT JOIN usuario on proy_usu_id = usu_id
                LEFT JOIN ciudad on ciu_id = inm_ciu_id
                LEFT JOIN departamento on dep_id = ciu_dep_id
                LEFT JOIN moneda on mon_id = inm_mon_id
                LEFT JOIN forma on inm_for_id = for_id
                LEFT JOIN categoria on cat_id = inm_cat_id
                LEFT JOIN (
                    SELECT
                        proy_id as proyId, proy_fot_imagen
                    FROM
                        proy_foto
                    GROUP BY proy_id
                ) proyImage on proyImage.proyId = proy_id
                WHERE
                    inm_proy_id != 0
                    and proy_estado = 'Aprobado'
                    and  " . $urlDecoder->getMainQueryFilter(TRUE) . "
                group by proy_id";
        if(!$showOnlyProjects)
        {
            $sql .= "
                UNION
                SELECT
                    pub_id,
                    inm_id,
                    inm_direccion,
                    ciu_nombre,
                    dep_nombre,
                    inm_superficie,
                    inm_tipo_superficie,
                    mon_abreviado,
                    inm_precio,
                    cat_nombre,
                    for_descripcion,
                    pub_creado,
                    propertyImage.fot_archivo publicationImage,
                    inm_seo seo,
                    usu_nombre,
                    usu_tipo,
                    usu_certificado,
                    usu_foto,
                    usu_id,
                    usu_empresa,
                    inm_latitud,
                    inm_longitud,
                    inm_nombre,
                    '' proy_id,
                    'none' as 'proy_ubicacion',
                    inmueble_caracteristica_optimized.*,
                    IF(searchService.ser_tipo = 'Busqueda',searchService.ser_tipo,IF(usu_certificado = 1,'Certificate agent','gratis')) service,
                    IF(searchService.ser_tipo = 'Busqueda',2,IF(usu_certificado = 1,3,4)) 'group_order'
                FROM
                    inmueble_caracteristica_optimized
                LEFT JOIN inmueble on inm_id = eca_inm_id
                LEFT JOIN publicacion on pub_inm_id = inm_id
                LEFT JOIN usuario on pub_usu_id = usu_id
                LEFT JOIN ciudad on inm_ciu_id = ciu_id
                LEFT JOIN departamento on dep_id = ciu_dep_id
                LEFT JOIN moneda on mon_id = inm_mon_id
                LEFT JOIN forma on inm_for_id = for_id
                LEFT JOIN categoria on cat_id = inm_cat_id               
                LEFT JOIN (
                            #Publications that has search service
                            SELECT
                                p.pag_pub_id, s.ser_tipo
                            FROM
                                pagos p
                            LEFT JOIN pago_servicio ps on p.pag_id = ps.pag_id AND CURDATE() BETWEEN ps.fech_ini AND ps.fech_fin
                            LEFT JOIN servicios s on s.ser_id = ps.ser_id
                            WHERE 
                                p.pag_estado = 'Pagado'
                                and s.ser_tipo = 'Busqueda'
                                and pag_pub_id is not null
                                and pag_concepto in('Mejora','Anuncio')
                        ) as searchService ON searchService.pag_pub_id = pub_id
                        LEFT JOIN (
                            SELECT
                                fot_inm_id inmId, fot_archivo
                            FROM
                                inmueble_foto
                            LEFT JOIN inmueble on fot_inm_id = inm_id
                            LEFT JOIN ciudad on inm_ciu_id = ciu_id
                            LEFT JOIN departamento on dep_id = ciu_dep_id
                            LEFT JOIN publicacion on pub_inm_id = inm_id
                            LEFT JOIN usuario on pub_usu_id = usu_id
                            LEFT JOIN inmueble_caracteristica_optimized on inmueble_caracteristica_optimized.eca_inm_id = inm_id
                            where fot_inm_id is not null
                            and pub_inm_id is not null
                            AND inm_precio >= 0	
                            and pub_estado = 'Aprobado'
                            and inm_publicado = 'Si'
                            and pub_vig_fin >= CURDATE()
                            and  " . $urlDecoder->getMainQueryFilter() . " 
                            GROUP BY fot_inm_id
                        ) propertyImage on propertyImage.inmId = inm_id
                where 
                    pub_inm_id is not null
                    AND inm_precio >= 0	
                    and pub_estado = 'Aprobado'
                    and inm_publicado = 'Si'
                    and pub_vig_fin >= CURDATE()
                    and inm_proy_id = 0
                    and " . $urlDecoder->getMainQueryFilter();
        }

        $sql .= "
                ORDER BY group_order, service" . $orderBy . " " . $orderType . " limit ".$limit." offset ".$offset."
        ";
//        echo"<pre>";var_dump($sql);exit;//debug line
        $query = $ci->db->query($sql);
        $result = $query->result_array();
        return $result;
    }

    public static function searchPublication($text, $limit, $offset, $orderBy = null, $orderType = 'asc', $colsArray = null)
    {
        if ($orderBy === null)
        {
            $orderBy = static::TABLE_ID;
        }
        $ci = &get_instance();
        $ci->load->database();

        $sql = 'select '.static::_dataTableColumns().', CONCAT(usu_nombre," ",usu_apellido) as user_full_name from ' . static::TABLE_NAME.'
                left join publicacion on pub_id = pag_pub_id
                left join usuario on usu_id = pub_usu_id
                left join publicacion on pub_id = pub_pub_id';
        $sql .= ' where (';
        foreach ($colsArray as $var)
        {
            if($var == "user_full_name")
            {
                $var = ' CONCAT(usu_nombre," ",usu_apellido) ';
            }
            $sql .= ' ' . $var . ' like \'%' . $text . '%\' or ';
        }

        $sql = substr($sql, 0, -3);
        $sql .= ') order by ' . $orderBy . ' ' . $orderType . ' limit ' . $limit . ' offset ' . $offset;

        $query = $ci->db->query($sql);
        return $query->result();
    }

    public static function searchPublicationTotalCount($text, $colsArray = null)
    {
        $ci = &get_instance();
        $ci->load->database();

        $sql = 'select count(' . static::TABLE_ID . ') as total from ' . static::TABLE_NAME.'
        left join publicacion on pub_id = pag_pub_id
        left join usuario on usu_id = pub_usu_id
        left join publicacion on pub_id = pub_pub_id';
        $sql .= ' where (';

        foreach ($colsArray as $var)
        {
            if($var == "user_full_name")
            {
                $var = ' CONCAT(usu_nombre," ",usu_apellido) ';
            }
            $sql .= ' ' . $var . ' like \'%' . $text . '%\' or ';
        }

        $sql = substr($sql, 0, -3);
        $sql .= ')';

        $query = $ci->db->query($sql);
        $totalCount = $query->row()->total;
        return $totalCount;
    }

    private static function _publicSearchColumns()
    {
        $columns = " pag_id, pag_cod_transaccion, inm_nombre, pag_fecha_pagado, usu_email, usu_tipo, pag_monto, pag_entidad, pag_concepto, pag_estado ";
        return $columns;
    }
    /*********************************************************************************** END - PUBLIC AJAX METHODS */
    /**
     * @param int $limit
     * @return mixed
     */
    public static function getPublicationsOnMainPage($limit = 8)
    {
        $ci = &get_instance();
        $ci->load->database();
        $sql = "
                SELECT
                    pub_id,
                    inm_id,
                    inm_direccion,
                    ciu_nombre,
                    dep_nombre,
                    inm_superficie,
                    inm_tipo_superficie,
                    mon_abreviado,
                    inm_precio,
                    cat_nombre,
                    for_descripcion,
                    pub_creado,
                    propertyImage.fot_archivo publicationImage,
                    inm_seo seo,
                    usu_nombre,
                    usu_tipo,
                    usu_certificado,
                    usu_foto,
                    usu_id,
                    usu_empresa,
                    inm_latitud,
                    inm_longitud,
                    inm_nombre,
                    'none' as 'proy_ubicacion',
                    inmueble_caracteristica_optimized.*,
                    IF(searchService.ser_tipo = 'Principal',searchService.ser_tipo,IF(usu_certificado = 1,'Certificate agent','gratis')) service,
                    IF(searchService.ser_tipo = 'Principal',2,IF(usu_certificado = 1,3,4)) 'group_order'
                FROM
                    inmueble_caracteristica_optimized
                LEFT JOIN inmueble on inm_id = eca_inm_id
                LEFT JOIN publicacion on pub_inm_id = inm_id
                LEFT JOIN usuario on pub_usu_id = usu_id
                LEFT JOIN ciudad on inm_ciu_id = ciu_id
                LEFT JOIN departamento on dep_id = ciu_dep_id
                LEFT JOIN moneda on mon_id = inm_mon_id
                LEFT JOIN forma on inm_for_id = for_id
                LEFT JOIN categoria on cat_id = inm_cat_id               
                LEFT JOIN (
                            #Publications that has search service
                            SELECT	
                                pag_pub_id,
                                ps.ser_id,
                                s.ser_tipo,
                                MAX(fech_fin) fech_fin
                            FROM
                                pago_servicio ps
                            LEFT JOIN pagos p on ps.pag_id = p.pag_id
                            LEFT JOIN servicios s on s.ser_id = ps.ser_id
                            where 	
                                pag_estado = 'Pagado'
                                and s.ser_tipo = 'Principal'
                                and ps.fech_fin > CURDATE()
                            and pag_pub_id is not null
                            GROUP BY pag_pub_id, ps.ser_id
                        ) as searchService ON searchService.pag_pub_id = pub_id
                        LEFT JOIN (
                            SELECT
                                fot_inm_id inmId, fot_archivo
                            FROM
                                inmueble_foto
                            LEFT JOIN inmueble on fot_inm_id = inm_id
                            LEFT JOIN ciudad on inm_ciu_id = ciu_id
                            LEFT JOIN departamento on dep_id = ciu_dep_id
                            LEFT JOIN publicacion on pub_inm_id = inm_id
                            LEFT JOIN usuario on pub_usu_id = usu_id
                            LEFT JOIN inmueble_caracteristica_optimized on inmueble_caracteristica_optimized.eca_inm_id = inm_id
                            where fot_inm_id is not null
                            and pub_inm_id is not null
                            AND inm_precio >= 0	
                            and pub_estado = 'Aprobado'
                            and inm_publicado = 'Si'
                            and pub_vig_fin >= CURDATE()                            
                            GROUP BY fot_inm_id
                        ) propertyImage on propertyImage.inmId = inm_id
                where 
                    pub_inm_id is not null
                    AND inm_precio >= 0	
                    and pub_estado = 'Aprobado'
                    and inm_publicado = 'Si'
                    and pub_vig_fin >= CURDATE()
                    and inm_proy_id = 0
                    
                ORDER BY group_order, pub_creado desc limit ".$limit." offset 0
        ";
        $query = $ci->db->query($sql);
        $response = $query->result_array();
        return $response;
    }

    public static function getRealStatePublications($userId, $limit = 100, $offset = 0)
    {
        $ci = &get_instance();
        $ci->load->database();

        $sql = "
        SELECT
            pub_id,
            inm_id,
            inm_direccion,
            ciu_nombre,
            dep_nombre,
            inm_superficie,
            inm_tipo_superficie,
            mon_abreviado,
            inm_precio,
            cat_nombre,
            for_descripcion,
            pub_creado,
            propertyImage.fot_archivo publicationImage,
            inm_seo seo,
            usu_nombre,
            usu_tipo,
            usu_certificado,
            usu_foto,
            usu_id,
            usu_empresa,
            inm_latitud,
            inm_longitud,
            inm_nombre,
            'none' as 'proy_ubicacion',
            inmueble_caracteristica_optimized.*,
            IF(searchService.ser_tipo = 'Principal',searchService.ser_tipo,IF(usu_certificado = 1,'Certificate agent','gratis')) service,
            IF(searchService.ser_tipo = 'Principal',2,IF(usu_certificado = 1,3,4)) 'group_order'
        FROM
            inmueble_caracteristica_optimized
        LEFT JOIN inmueble on inm_id = eca_inm_id
        LEFT JOIN publicacion on pub_inm_id = inm_id
        LEFT JOIN usuario on pub_usu_id = usu_id
        LEFT JOIN ciudad on inm_ciu_id = ciu_id
        LEFT JOIN departamento on dep_id = ciu_dep_id
        LEFT JOIN moneda on mon_id = inm_mon_id
        LEFT JOIN forma on inm_for_id = for_id
        LEFT JOIN categoria on cat_id = inm_cat_id               
        LEFT JOIN (
                    #Publications that has search service
                    SELECT
                        p.pag_pub_id, s.ser_tipo
                    FROM
                        pagos p
                    LEFT JOIN pago_servicio ps on p.pag_id = ps.pag_id AND CURDATE() BETWEEN ps.fech_ini AND ps.fech_fin
                    LEFT JOIN servicios s on s.ser_id = ps.ser_id
                    WHERE 
                        p.pag_estado = 'Pagado'
                        and s.ser_tipo = 'Principal'
                        and pag_pub_id is not null
                        and pag_concepto in('Mejora','Anuncio')
                ) as searchService ON searchService.pag_pub_id = pub_id
                LEFT JOIN (
                    SELECT
                        fot_inm_id inmId, fot_archivo
                    FROM
                        inmueble_foto
                    LEFT JOIN inmueble on fot_inm_id = inm_id
                    LEFT JOIN ciudad on inm_ciu_id = ciu_id
                    LEFT JOIN departamento on dep_id = ciu_dep_id
                    LEFT JOIN publicacion on pub_inm_id = inm_id
                    LEFT JOIN usuario on pub_usu_id = usu_id
                    LEFT JOIN inmueble_caracteristica_optimized on inmueble_caracteristica_optimized.eca_inm_id = inm_id
                    where fot_inm_id is not null
                    and pub_inm_id is not null
                    AND inm_precio >= 0	
                    and pub_estado = 'Aprobado'
                    and inm_publicado = 'Si'
                    and pub_vig_fin >= CURDATE()                            
                    GROUP BY fot_inm_id
                ) propertyImage on propertyImage.inmId = inm_id
        where 
            pub_inm_id is not null
            AND inm_precio >= 0	
            and pub_estado = 'Aprobado'
            and inm_publicado = 'Si'
            and pub_vig_fin >= CURDATE()
            and inm_proy_id = 0
            and pub_usu_id = ".$ci->db->escape($userId)."
        ORDER BY pub_creado desc limit ".$limit." offset ".$ci->db->escape($offset)."
        ";

        $query = $ci->db->query($sql);
        $result = $query->result_array();
        return $result;
    }

    public static function countAllRealStatePublications($userId)
    {
        $ci = &get_instance();
        $ci->load->database();

        $sql = "
        SELECT
            count(pub_id) total
        FROM
            inmueble_caracteristica_optimized
        LEFT JOIN inmueble on inm_id = eca_inm_id
        LEFT JOIN publicacion on pub_inm_id = inm_id
        LEFT JOIN usuario on pub_usu_id = usu_id
        LEFT JOIN ciudad on inm_ciu_id = ciu_id
        LEFT JOIN departamento on dep_id = ciu_dep_id
        LEFT JOIN moneda on mon_id = inm_mon_id
        LEFT JOIN forma on inm_for_id = for_id
        LEFT JOIN categoria on cat_id = inm_cat_id               
        LEFT JOIN (
                    #Publications that has search service
                    SELECT
                        p.pag_pub_id, s.ser_tipo
                    FROM
                        pagos p
                    LEFT JOIN pago_servicio ps on p.pag_id = ps.pag_id AND CURDATE() BETWEEN ps.fech_ini AND ps.fech_fin
                    LEFT JOIN servicios s on s.ser_id = ps.ser_id
                    WHERE 
                        p.pag_estado = 'Pagado'
                        and s.ser_tipo = 'Principal'
                        and pag_pub_id is not null
                        and pag_concepto in('Mejora','Anuncio')
                ) as searchService ON searchService.pag_pub_id = pub_id
                LEFT JOIN (
                    SELECT
                        fot_inm_id inmId, fot_archivo
                    FROM
                        inmueble_foto
                    LEFT JOIN inmueble on fot_inm_id = inm_id
                    LEFT JOIN ciudad on inm_ciu_id = ciu_id
                    LEFT JOIN departamento on dep_id = ciu_dep_id
                    LEFT JOIN publicacion on pub_inm_id = inm_id
                    LEFT JOIN usuario on pub_usu_id = usu_id
                    LEFT JOIN inmueble_caracteristica_optimized on inmueble_caracteristica_optimized.eca_inm_id = inm_id
                    where fot_inm_id is not null
                    and pub_inm_id is not null
                    AND inm_precio >= 0	
                    and pub_estado = 'Aprobado'
                    and inm_publicado = 'Si'
                    and pub_vig_fin >= CURDATE()                            
                    GROUP BY fot_inm_id
                ) propertyImage on propertyImage.inmId = inm_id
        where 
            pub_inm_id is not null
            AND inm_precio >= 0	
            and pub_estado = 'Aprobado'
            and inm_publicado = 'Si'
            and pub_vig_fin >= CURDATE()
            and inm_proy_id = 0
            and pub_usu_id = ".$ci->db->escape($userId)."        
        ";

        $query = $ci->db->query($sql);
        $totalCount = $query->row()->total;
        return $totalCount;
    }

    public static function getSimilarPublications($userId, $limit = 100, $offset = 0)
    {
        $ci = &get_instance();
        $ci->load->database();

        $sql = "
        SELECT
            pub_id,
            inm_id,
            inm_direccion,
            ciu_nombre,
            dep_nombre,
            inm_superficie,
            inm_tipo_superficie,
            mon_abreviado,
            inm_precio,
            cat_nombre,
            for_descripcion,
            pub_creado,
            propertyImage.fot_archivo publicationImage,
            inm_seo seo,
            usu_nombre,
            usu_tipo,
            usu_certificado,
            usu_foto,
            usu_id,
            usu_empresa,
            inm_latitud,
            inm_longitud,
            inm_nombre,
            'none' as 'proy_ubicacion',
            inmueble_caracteristica_optimized.*,
            IF(searchService.ser_tipo = 'Principal',searchService.ser_tipo,IF(usu_certificado = 1,'Certificate agent','gratis')) service,
            IF(searchService.ser_tipo = 'Principal',2,IF(usu_certificado = 1,3,4)) 'group_order'
        FROM
            inmueble_caracteristica_optimized
        LEFT JOIN inmueble on inm_id = eca_inm_id
        LEFT JOIN publicacion on pub_inm_id = inm_id
        LEFT JOIN usuario on pub_usu_id = usu_id
        LEFT JOIN ciudad on inm_ciu_id = ciu_id
        LEFT JOIN departamento on dep_id = ciu_dep_id
        LEFT JOIN moneda on mon_id = inm_mon_id
        LEFT JOIN forma on inm_for_id = for_id
        LEFT JOIN categoria on cat_id = inm_cat_id               
        LEFT JOIN (
                    #Publications that has search service
                    SELECT
                        p.pag_pub_id, s.ser_tipo
                    FROM
                        pagos p
                    LEFT JOIN pago_servicio ps on p.pag_id = ps.pag_id AND CURDATE() BETWEEN ps.fech_ini AND ps.fech_fin
                    LEFT JOIN servicios s on s.ser_id = ps.ser_id
                    WHERE 
                        p.pag_estado = 'Pagado'
                        and s.ser_tipo = 'Principal'
                        and pag_pub_id is not null
                        and pag_concepto in('Mejora','Anuncio')
                ) as searchService ON searchService.pag_pub_id = pub_id
                LEFT JOIN (
                    SELECT
                        fot_inm_id inmId, fot_archivo
                    FROM
                        inmueble_foto
                    LEFT JOIN inmueble on fot_inm_id = inm_id
                    LEFT JOIN ciudad on inm_ciu_id = ciu_id
                    LEFT JOIN departamento on dep_id = ciu_dep_id
                    LEFT JOIN publicacion on pub_inm_id = inm_id
                    LEFT JOIN usuario on pub_usu_id = usu_id
                    LEFT JOIN inmueble_caracteristica_optimized on inmueble_caracteristica_optimized.eca_inm_id = inm_id
                    where fot_inm_id is not null
                    and pub_inm_id is not null
                    AND inm_precio >= 0	
                    and pub_estado = 'Aprobado'
                    and inm_publicado = 'Si'
                    and pub_vig_fin >= CURDATE()                            
                    GROUP BY fot_inm_id
                ) propertyImage on propertyImage.inmId = inm_id
        where 
            pub_inm_id is not null
            AND inm_precio >= 0	
            and pub_estado = 'Aprobado'
            and inm_publicado = 'Si'
            and pub_vig_fin >= CURDATE()
            and inm_proy_id = 0
            and pub_usu_id = ".$ci->db->escape($userId)."
        ORDER BY pub_creado desc limit ".$limit." offset ".$ci->db->escape($offset)."
        ";

        $query = $ci->db->query($sql);
        $result = $query->result_array();
        return $result;
    }

    public static function getMasterDetailById($publicationId = NULL)
    {
        $ci = &get_instance();
        $ci->load->database();

        $sql = "
        SELECT
            *
        FROM
            publicacion
        LEFT JOIN inmueble on inm_id = pub_inm_id
        where pub_id = ".$ci->db->escape($publicationId)."
        ";

        $query = $ci->db->query($sql);
        $result = $query->row_array();
        return $result;
    }

}