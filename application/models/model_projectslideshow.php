<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_projectslideshow extends MY_Model {

    const TABLE_ID = "id";
    const TABLE_NAME = "proyecto_slideshow";

    const SLIDE_SHOW_IMAGE = "Imagen";
    const SLIDE_SHOW_VIDEO = "Video";

    protected $_image;
    protected $_youTubeCode;
    protected $_projectId;
    protected $_type;

    //Due to autoload is necessary to set the default data as empty string
    function __construct($image = "", $youTubeCode = "", $projectId = "")
    {
        parent::__construct();
        $this->_image = $image;
        $this->_youTubeCode = $youTubeCode;
        $this->_projectId = $projectId;
        $this->_type = static::SLIDE_SHOW_IMAGE;
    }

    /**
     * Parse the current object to key => value array
     * @return array
     */
    public function toArray()
    {
        $tableAttributes = array(
                                "id" => $this->_id,
                                "imagen" => $this->_image,
                                "youtube" => $this->_youTubeCode,
                                "proyecto_id" => $this->_projectId,
                                "tipo" => $this->_type,
                                );
        return $tableAttributes;
    }

    /**
     * @param $className
     * @param $object
     * @return object
     */
    protected static function recast($className, $object)
    {
        $response =  null;
        if ($object instanceof stdClass)
        {
            if (!class_exists($className))
                throw new InvalidArgumentException(sprintf('Inexistant class %s.', $className));

            //Let's set the values to payment object using the data from stdObject
            $instance = new $className(
                    $object->imagen,
                    $object->youtube,
                    $object->proyecto_id,
                    $object->tipo
            );
            $instance->_id = $object->id;
            $response = $instance;
        }
        return $response;
    }
    ################################################################################################# BEGIN - GETTERS

    public static function getByProjectId($projectId)
    {
        $ci=&get_instance();
        $ci->load->database();
        $sql = "
            select * from ".static::TABLE_NAME." where proyecto_id = ".$ci->db->escape($projectId)."
        ";
        $query = $ci->db->query($sql);
        $response = static::recastArray(get_called_class(),$query->result());
        return $response;
    }
    ################################################################################################# END - GETTERS



    ################################################################################################# BEGIN - SETTERS

    public function setYouTubeCode($youTubeCode)
    {
        $this->_youTubeCode = $youTubeCode;
        if($this->_youTubeCode != "")
        {
            $this->_type = static::SLIDE_SHOW_VIDEO;
        }
    }

    public function setImage($image)
    {
        $file = new File_Handler($this->_image,"projectSlideShowImage");
        $file->delete();
        $this->_image = $image;
    }

    ################################################################################################# END - SETTERS
    public static function search($stateId, $text, $limit, $offset, $orderBy = null, $orderType = 'asc', $colsArray = null)
    {
        if ($orderBy === null)
        {
            $orderBy = static::TABLE_ID;
        }
        $ci = &get_instance();
        $ci->load->database();

        $sql = 'select * from ' . static::TABLE_NAME;
        $sql .= ' where (';
        foreach ($colsArray as $var)
        {
            $sql .= ' ' . $var . ' like \'%' . $text . '%\' or ';
        }

        $sql = substr($sql, 0, -3);
        $sql .= ') and ciu_dep_id ='.$ci->db->escape($stateId).' order by ' . $orderBy . ' ' . $orderType . ' limit ' . $limit . ' offset ' . $offset;
        $query = $ci->db->query($sql);
        return static::recastArray(get_called_class(), $query->result());
    }

    public function delete($makePhysicalDelete = FALSE)
    {
        $file = new File_Handler($this->_image,"projectSlideShowImage");
        $file->delete();
        parent::delete($makePhysicalDelete);
    }

}