<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_socialarea extends MY_Model {

    const TABLE_ID = "are_soc_id";
    const TABLE_NAME = "area_social";

    protected $_title;
    protected $_description;
    protected $_image;
    protected $_projectId;
    //Due to autoload is necessary to set the default data as empty string
    function __construct($title = "", $description = "", $image = "", $projectId = "")
    {
        parent::__construct();
        $this->_title = $title;
        $this->_description = $description;
        $this->_image = $image;
        $this->_projectId = $projectId;
    }

    /**
     * Parse the current object to key => value array
     * @return array
     */
    public function toArray()
    {
        $tableAttributes = array(
                                "are_soc_id" => $this->_id,
                                "are_soc_titulo" => $this->_title,
                                "are_soc_descripcion" => $this->_description,
                                "are_soc_imagen" => $this->_image,
                                "are_soc_proy_id" => $this->_projectId,
                                );
        return $tableAttributes;
    }

    /**
     * @param $className
     * @param $object
     * @return object
     */
    protected static function recast($className, $object)
    {
        $response =  null;
        if ($object instanceof stdClass)
        {
            if (!class_exists($className))
                throw new InvalidArgumentException(sprintf('Inexistant class %s.', $className));

            //Let's set the values to payment object using the data from stdObject
            $instance = new $className(
                    $object->are_soc_titulo,
                    $object->are_soc_descripcion,
                    $object->are_soc_imagen,
                    $object->are_soc_proy_id
            );
            $instance->_id = $object->are_soc_id;
            $response = $instance;
        }
        return $response;
    }
    ################################################################################################# BEGIN - GETTERS

    public function getImageName()
    {
        return $this->_imageName;
    }

    public static function getByProjectId($projectId)
    {
        $ci=&get_instance();
        $ci->load->database();
        $sql = "
            select * from ".static::TABLE_NAME." where are_soc_proy_id = ".$ci->db->escape($projectId)."
        ";
        $query = $ci->db->query($sql);
        $response = static::recastArray(get_called_class(),$query->result());
        return $response;
    }
    ################################################################################################# END - GETTERS



    ################################################################################################# BEGIN - SETTERS

    public function setTitle($title)
    {
        $this->_title = $title;
    }

    public function setDescription($description)
    {
        $this->_description = $description;
    }

    public function setImage($image)
    {
        $file = new File_Handler($this->_image,"projectImage");
        $file->delete();
        $this->_image = $image;
    }

    ################################################################################################# END - SETTERS
    public static function search($stateId, $text, $limit, $offset, $orderBy = null, $orderType = 'asc', $colsArray = null)
    {
        if ($orderBy === null)
        {
            $orderBy = static::TABLE_ID;
        }
        $ci = &get_instance();
        $ci->load->database();

        $sql = 'select * from ' . static::TABLE_NAME;
        $sql .= ' where (';
        foreach ($colsArray as $var)
        {
            $sql .= ' ' . $var . ' like \'%' . $text . '%\' or ';
        }

        $sql = substr($sql, 0, -3);
        $sql .= ') and ciu_dep_id ='.$ci->db->escape($stateId).' order by ' . $orderBy . ' ' . $orderType . ' limit ' . $limit . ' offset ' . $offset;
        $query = $ci->db->query($sql);
        return static::recastArray(get_called_class(), $query->result());
    }

    public function delete($makePhysicalDelete = FALSE)
    {
        $file = new File_Handler($this->_image,"projectImage");
        $file->delete();
        parent::delete($makePhysicalDelete);
    }
}