<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_survey extends CI_Model {

    const TABLE_ID = "sur_id";
    const TABLE_NAME = "surveys";
    //MEDIOS DE VENTA
    const TOQUE_EL_TIMBRE = 0;
    const PRINT_MEANS = 1;
    const DIGITAL_MEANS = 2;
    const REFERRED = 3;
    const SING_BOARDS = 4;
    const OTHERS = 5;

    private $sur_id;
    private $sur_publicationid;
    private $sur_propertysold;
    private $sur_meansofsale;
    private $sur_salesprice;
    private $sur_nosolddescription;
    private $sur_deleted;
    private $sur_createdon;
    private $sur_createdby;
    private $sur_editedon;
    private $sur_editedby;

    public function __construct($publicationId = "", $propertySold = "", $meansOfSale = "", $salesPrice = "", $noSoldDescription = "")
    {
        parent::__construct();
        $ci=&get_instance();
        $ci->load->database();

        $this->sur_publicationid = $publicationId;
        $this->sur_propertysold = $propertySold;
        $this->sur_meansofsale = $meansOfSale;
        $this->sur_salesprice = $salesPrice;
        $this->sur_nosolddescription = $noSoldDescription;

        $this->sur_deleted = 0;
        $this->sur_createdon = date("Y-m-d H:i:s");
        $this->sur_createdby = NULL;
        $this->sur_editedon = NULL;
        $this->sur_editedby = NULL;
    }

    /**
     * Gets all results for this model but get a one result by id too.
     *
     * @param bool $id
     * @return []
     */
    function get_all($id = FALSE) {
        if ($id === FALSE) {
            $query = $this->db->get($this->table);
            return $query->result_array();
        }

        $query = $this->db->get_where($this->table, array($this->id => $id));
        return $query->row_array();
    }

    public function getId()
    {
        return $this->sur_id;
    }

    /**
     * Add new Model
     *
     * @param $data
     * @return mixed
     */
    function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    /**
     * Update Model
     *
     * @param $id
     * @param $data
     * @return mixed
     */
    function update($id, $data) {
        $this->db->where($this->id, $id);
        return $this->db->update($this->table, $data);
    }

    /**
     * Delete Model
     *
     * @param $id
     * @return mixed
     */
    function delete($id) {
        return $this->db->update($this->table, array($this->id => $id));
    }

    function get_cities_by_state($dep_id){
        $sql = "
        SELECT * FROM ciudad WHERE ciu_dep_id = '$dep_id'
        ";
        return $this->db->query($sql)->result();
    }

    public function saveNew()
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
        $ci=&get_instance();


    }
    public function save()
    {
        $ci=&get_instance();
        $ci->load->library("session");
        //static::validateTableDefinition();
        //$userData = $ci->session->all_userdata();
        //echo"<pre>";var_dump($userData);exit;
        //$loggedUserId = $this->session->userdata('id_user');
        date_default_timezone_set('America/La_Paz');
        $now = new DateTime();
        if ( $this->getId() !== null && $this->getId() !== "" )
        {
            $editedBy = "editedby" . self::COLUMN_SUFIX;
            $editedOn = "editedon" . self::COLUMN_SUFIX;
            $this->$editedBy = $loggedUserId;
            $this->$editedOn = $now->format( "Y-m-d H:i:s" );
            return $ci->db->update( self::TABLE_NAME, get_object_vars( $this ), array( self::TABLE_ID => $this->getId()));
        }
        else
        {
            //$createdBy = "createdby" . static::COLUMN_SUFIX;
            //$createdOn = "createdon" . static::COLUMN_SUFIX;
            //$this->$createdBy = $loggedUserId;
            //$this->$createdOn = $now->format( "Y-m-d H:i:s" );
            $result = $ci->db->insert( self::TABLE_NAME, get_object_vars( $this ) );
            if ( $result === true )
            {
                $this->sur_id = $ci->db->insert_id();
            }
            return $this->sur_id;
        }
    }

}