<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_city extends MY_Model {

    const TABLE_ID = "ciu_id";
    const TABLE_NAME = "ciudad";

    protected $_name;
    protected $_stateId;
    protected $_latitude;
    protected $_longitude;
    //Due to autoload is necessary to set the default data as empty string
    function __construct($name = "", $stateId = "", $latitude = "", $longitude = "")
    {
        parent::__construct();
        $this->_name = $name;
        $this->_stateId = $stateId;
        $this->_latitude = $latitude;
        $this->_longitude = $longitude;
    }

    /**
     * Parse the current object to key => value array
     * @return array
     */
    public function toArray()
    {
        $tableAttributes = array(
                                "ciu_id" => $this->_id,
                                "ciu_nombre" => $this->_name,
                                "ciu_dep_id" => $this->_stateId,
                                "ciu_latitud" => $this->_latitude,
                                "ciu_longitud" => $this->_longitude
                                );
        return $tableAttributes;
    }

    /**
     * @param $className
     * @param $object
     * @return object
     */
    protected static function recast($className, $object)
    {
        $response =  null;
        if ($object instanceof stdClass)
        {
            if (!class_exists($className))
                throw new InvalidArgumentException(sprintf('Inexistant class %s.', $className));

            //Let's set the values to payment object using the data from stdObject
            $instance = new $className(
                    $object->ciu_nombre,
                    $object->ciu_dep_id,
                    $object->ciu_latitud,
                    $object->ciu_longitud
            );
            $instance->_id = $object->ciu_id;
            $response = $instance;
        }
        return $response;
    }

    ################################################################################################# BEGIN - GETTERS

    public function getName()
    {
        return $this->_name;
    }

    ################################################################################################# END - GETTERS



    ################################################################################################# BEGIN - SETTERS

    ################################################################################################# END - SETTERS
    public static function search($stateId, $text, $limit, $offset, $orderBy = null, $orderType = 'asc', $colsArray = null)
    {
        if ($orderBy === null)
        {
            $orderBy = static::TABLE_ID;
        }
        $ci = &get_instance();
        $ci->load->database();

        $sql = 'select * from ' . static::TABLE_NAME;
        $sql .= ' where (';
        foreach ($colsArray as $var)
        {
            $sql .= ' ' . $var . ' like \'%' . $text . '%\' or ';
        }

        $sql = substr($sql, 0, -3);
        $sql .= ') and ciu_dep_id ='.$ci->db->escape($stateId).' order by ' . $orderBy . ' ' . $orderType . ' limit ' . $limit . ' offset ' . $offset;
        $query = $ci->db->query($sql);
        return static::recastArray(get_called_class(), $query->result());
    }
}