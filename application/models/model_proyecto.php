<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_proyecto extends CI_Model {

    private $table = 'proyecto';
    private $id = 'proy_id';

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    /**
     * Gets all results for this model but get a one result by id too.
     *
     * @param bool $id
     * @return []
     */
    function get_all($id = FALSE) {
        if ($id === FALSE) {
            $query = $this->db->get($this->table);
            return $query->result_array();
        }

        $query = $this->db->get_where($this->table, array($this->id => $id));
        return $query->row_array();
    }

    /**
     * Add new Model
     *
     * @param $data
     * @return mixed
     */
    function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    /**
     * Update Model
     *
     * @param $id
     * @param $data
     * @return mixed
     */
    function update($id, $data) {
        $this->db->where($this->id, $id);
        return $this->db->update($this->table, $data);
    }

    /**
     * Delete Model
     *
     * @param $id
     * @return mixed
     */
    function delete($id) {
        return $this->db->update($this->table, array($this->id => $id));
    }

    function get_project_by_url($id) {
        $sw_project = $_GET["q"];
        if($sw_project == "true"){
            $sql = "
            SELECT p.*, u.`usu_email`, u.`usu_telefono`, u.usu_celular FROM proyecto p
            LEFT JOIN usuario u ON (u.`usu_id` = p.`proy_usu_id`)
            WHERE p.`proy_seo` = '$id'
            ";
        }else{
            $sql = "
            SELECT p.*, u.`usu_email`, u.`usu_telefono` FROM proyecto p
            LEFT JOIN usuario u ON (u.`usu_id` = p.`proy_usu_id`)
            WHERE p.`proy_seo` = '$id' AND p.proy_estado = 'Aprobado'
            ";
        }

        $project = $this->db->query($sql)->row();
        if(!empty($project)){
            $data["project"] = $project;
            $data["tipologia"] = $this->get_tipologia_price($project->proy_id);
            $data["lower_price"] = $this->get_lower_price($project->proy_id);
            $data["slideshow"] = $this->get_project_slideshow($project->proy_id);
            $data["sw_area_social"] = $this->sw_area_social($project->proy_id);
        }else{
            $data["project"] = array();
        }
        return $data;
    }

    function get_tipologia_price($id){
        $sql = "
        SELECT * FROM tipologia
        WHERE tip_proy_id = '$id'
        ";
        return $this->db->query($sql)->result();
    }

    function get_acabado($id){
        $sql = "
        SELECT * FROM acabado
        WHERE aca_proy_id = '$id'
        ";
        $result = $this->db->query($sql)->result();
        $i = $j = 0;
        foreach($result as $item){
            if($i % 3 == 0 && $i != 0){
                $data_result[] = $data;
                $data = array();
                $j++;
            }
            $data[] = $item;
            $i++;
        }
        if(($j * 3) != count($result)){
            $data_result[] = $data;
        }
        return $data_result;

    }

    function getAllFinishingDetails($id)
    {
        $sql = "
        SELECT * FROM acabado
        WHERE aca_proy_id = '$id'
        ";
        $result = $this->db->query($sql)->result();
        return $result;

    }

    function get_gallery($id){
        $sql = "
        SELECT * FROM proy_foto
        WHERE proy_id = '$id' ORDER BY proy_fot_order
        ";
        return $this->db->query($sql)->result();
    }

    function get_area_social($id){
        $sql = "
        SELECT * FROM area_social
        WHERE are_soc_proy_id = '$id'
        ";
        $result = $this->db->query($sql)->result();
        $i = $j = 0;
        foreach($result as $item){
            if($i % 3 == 0 && $i != 0){
                $data_result[] = $data;
                $data = array();
            }
            $data[] = $item;
            $i++;
        }
        if(($j * 3) != count($result)){
            $data_result[] = $data;
        }
        return $data_result;
    }

    function sw_area_social($id){
        $sql = "
        SELECT are_soc_id FROM area_social
        WHERE are_soc_proy_id = '$id'
        ";
        $num = $this->db->query($sql)->num_rows();
        return $num > 0 ? true : false;
    }

    function get_lower_price($id){
        $sql = "
        SELECT MIN(tip_precio) AS menor_precio
        FROM tipologia
        WHERE tip_proy_id = '$id'
        ";
        return $this->db->query($sql)->row();
    }

    function get_cover_image($id){
        $sql = "
        SELECT proy_fot_imagen FROM proy_foto
        WHERE proy_fot_portada='true' AND proy_id = '$id'
        ";
        $image = $this->db->query($sql)->row();

        if(!empty($image)){
            return $image;
        }else{
            $sql = "
            SELECT proy_fot_imagen FROM proy_foto WHERE proy_id = '$id'
            ";
            return $this->db->query($sql)->row();
        }
    }

    function get_project_slideshow($id){
        $sql = "
        SELECT  * FROM proyecto_slideshow
        WHERE proyecto_id = '$id'
        ";
        return $this->db->query($sql)->result();
    }

    function get_list_projects_by_user($user_id, $page, $limit){

        $page = ($page -1 >= 0) ? ($page - 1) : 0;
        $page = $page * $limit;
        $sql = "
        SELECT * FROM proyecto
        WHERE proy_usu_id = $user_id
        LIMIT $page, $limit
        ";

        return $this->db->query($sql)->result();
    }

    function get_num_projects_by_user($user_id){

        $sql = "
        SELECT * FROM proyecto
        WHERE proy_usu_id = $user_id
        ";
        return $this->db->query($sql)->num_rows();
    }

    function get_project_by_userId($user_id) {
        $sql = "
          SELECT MAX(proy_vig_fin) AS proy_vig_fin FROM proyecto
          WHERE proy_usu_id = $user_id
        ";
        return $this->db->query($sql)->result();
    }

    function get_config_proyecto($id){
        $sql = "
        SELECT * FROM proyecto_config
        WHERE proy_id = '$id'
        ";
        return $this->db->query($sql)->row();
    }

    function get_info_project($id){
        $sql = "
        SELECT * FROM proyecto p
        INNER JOIN proy_foto pf ON (pf.`proy_id` = p.`proy_id` )
        WHERE p.`proy_id` = '$id'
        ORDER BY pf.`proy_fot_id` ASC
        LIMIT 1
        ";
        return $this->db->query($sql)->row();
    }
}