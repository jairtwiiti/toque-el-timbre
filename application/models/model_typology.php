<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_typology extends MY_Model {

    const TABLE_ID = "tip_id";
    const TABLE_NAME = "tipologia";

    protected $_description;
    protected $_image;
    protected $_price;
    protected $_projectId;
    protected $_title;
    protected $_titlePrice;
    protected $_availability;
    protected $_builtSurface;
    protected $_surface;
    protected $_imagePrice;
    protected $_showPrice;
    protected $_showBuiltSurface;
    protected $_showSurface;
    //Due to autoload is necessary to set the default data as empty string
    function __construct($description = "", $image = "", $price = "", $projectId = "", $title = "", $titlePrice = "", $availability = "",
                        $builtSurface = "", $surface = "", $imagePrice = "", $showPrice = "", $showBuiltSurface = "", $showSurface = "")
    {
        parent::__construct();
        $this->_description = $description;
        $this->_image = $image;
        $this->_price = $price;
        $this->_projectId = $projectId;
        $this->_title = $title;
        $this->_titlePrice = $titlePrice;
        $this->_availability = $availability;
        $this->_builtSurface = $builtSurface;
        $this->_surface = $surface;
        $this->_imagePrice = $imagePrice;
        $this->_showPrice = $showPrice;
        $this->_showBuiltSurface = $showBuiltSurface;
        $this->_showSurface = $showSurface;
    }

    /**
     * Parse the current object to key => value array
     * @return array
     */
    public function toArray()
    {
        $tableAttributes = array(
                                "tip_id" => $this->_id,
                                "tip_descripcion" => $this->_description,
                                "tip_imagen" => $this->_image,
                                "tip_precio" => $this->_price,
                                "tip_proy_id" => $this->_projectId,
                                "tip_titulo" => $this->_title,
                                "tip_titulo_precio" => $this->_titlePrice,
                                "tip_disponibilidad" => $this->_availability,
                                "tip_mts" => $this->_builtSurface,
                                "tip_terreno_mts" => $this->_surface,
                                "tip_imagen_precio" => $this->_imagePrice,
                                "tip_ver_precio" => $this->_showPrice,
                                "tip_ver_supcon" => $this->_showBuiltSurface,
                                "tip_ver_suptot" => $this->_showSurface
                                );
        return $tableAttributes;
    }

    /**
     * @param $className
     * @param $object
     * @return object
     */
    protected static function recast($className, $object)
    {
        $response =  null;
        if ($object instanceof stdClass)
        {
            if (!class_exists($className))
                throw new InvalidArgumentException(sprintf('Inexistant class %s.', $className));

            //Let's set the values to payment object using the data from stdObject
            $instance = new $className(
                    $object->tip_descripcion,
                    $object->tip_imagen,
                    $object->tip_precio,
                    $object->tip_proy_id,
                    $object->tip_titulo,
                    $object->tip_titulo_precio,
                    $object->tip_disponibilidad,
                    $object->tip_mts,
                    $object->tip_terreno_mts,
                    $object->tip_imagen_precio,
                    $object->tip_ver_precio,
                    $object->tip_ver_supcon,
                    $object->tip_ver_suptot
            );
            $instance->_id = $object->tip_id;
            $response = $instance;
        }
        return $response;
    }

    ################################################################################################# BEGIN - GETTERS

    public function getImageName()
    {
        return $this->_image;
    }

    public static function getByProjectId($projectId)
    {
        $ci=&get_instance();
        $ci->load->database();
        $sql = "
            select * from ".self::TABLE_NAME." where tip_proy_id = ".$ci->db->escape($projectId)."
        ";
        $query = $ci->db->query($sql);
        $response = self::recastArray(get_called_class(),$query->result());
        return $response;
    }
    ################################################################################################# END - GETTERS



    ################################################################################################# BEGIN - SETTERS

    public function setTitle($title)
    {
        $this->_title = $title;
    }

    public function setTitlePrice($titlePrice)
    {
        $this->_titlePrice = $titlePrice;
    }

    public function setPrice($price)
    {
        $this->_price = $price;
    }

    public function setBuiltSurface($builtSurface)
    {
        $this->_builtSurface = $builtSurface;
    }
    public function setSurface($surface)
    {
        $this->_surface = $surface;
    }
    public function setAvailability($availability)
    {
        $this->_availability = $availability;
    }
    public function setShowPrice($showPrice)
    {
        $this->_showPrice = $showPrice;
    }
    public function setShowBuiltSurface($showBuiltSurface)
    {
        $this->_showBuiltSurface = $showBuiltSurface;
    }
    public function setShowSurface($showSurface)
    {
        $this->_showSurface = $showSurface;
    }
    public function setDescription($description)
    {
        $this->_description = $description;
    }
    public function setImage($image)
    {
        $file = new File_Handler($this->_image,"projectImage");
        $file->delete();
        $this->_image = $image;
    }
    public function setImagePrice($imagePrice)
    {
        $file = new File_Handler($this->_imagePrice,"projectImage");
        $file->delete();
        $this->_imagePrice = $imagePrice;
    }
    ################################################################################################# END - SETTERS
    public static function search($stateId, $text, $limit, $offset, $orderBy = null, $orderType = 'asc', $colsArray = null)
    {
        if ($orderBy === null)
        {
            $orderBy = self::TABLE_ID;
        }
        $ci = &get_instance();
        $ci->load->database();

        $sql = 'select * from ' . self::TABLE_NAME;
        $sql .= ' where (';
        foreach ($colsArray as $var)
        {
            $sql .= ' ' . $var . ' like \'%' . $text . '%\' or ';
        }

        $sql = substr($sql, 0, -3);
        $sql .= ') and ciu_dep_id ='.$ci->db->escape($stateId).' order by ' . $orderBy . ' ' . $orderType . ' limit ' . $limit . ' offset ' . $offset;
        $query = $ci->db->query($sql);
        return self::recastArray(get_called_class(), $query->result());
    }

    public function delete($makePhysicalDelete = FALSE)
    {
        $file = new File_Handler($this->_image,"projectImage");
        $file->delete();
        $file = new File_Handler($this->_imagePrice,"projectImage");
        $file->delete();
        parent::delete($makePhysicalDelete);
    }
}