<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class visitor extends CI_Model {

    private $table = 'visitor';
    private $id = 'id';

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    /**
     * Gets all results for this model but get a one result by id too.
     *
     * @param bool $id
     * @return []
     */
    function get_all($id = FALSE) {
        if ($id === FALSE) {
            $query = $this->db->get($this->table);
            return $query->result_array();
        }

        $query = $this->db->get_where($this->table, array($this->id => $id));
        return $query->row_array();
    }

    /**
     * Add new Model
     *
     * @param $data
     * @return mixed
     */
    function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    /**
     * Update Model
     *
     * @param $id
     * @param $data
     * @return mixed
     */
    function update($id, $data) {
        $this->db->where($this->id, $id);
        return $this->db->update($this->table, $data);
    }

    /**
     * Delete Model
     *
     * @param $id
     * @return mixed
     */
    function delete($id) {
        return $this->db->update($this->table, array($this->id => $id));
    }

    function get_visitor_by_type_and_id($type, $typeId){
        $sql = "
            SELECT *
            FROM visitor v
            WHERE v.type = '$type' AND v.type_id = $typeId
            ORDER v.visitor_date ASC
        ";
        return $this->db->query($sql)->result();
    }

    function get_current($type, $typeId) {
        $sql = "
            SELECT *
            FROM visitor v
            WHERE v.visitor_date = CURDATE()
            AND v.type = '$type'
            AND v.type_id = $typeId
        ";
        return $this->db->query($sql)->result();
    }

    function exist_current_date_visitor($type, $typeId) {
        return count($this->get_current($type, $typeId)) > 0;
    }

    // get all visitors('anuncio', 7883)
    function get_all_visitor_by_user_id($type, $user_id, $type_id = null) {
        $str = !is_null($type_id) ? " AND v.type_id = $type_id" : "";
        $sql = "
            SELECT v.*, p.pub_id, p.pub_usu_id, p.pub_estado
            FROM visitor v
            INNER JOIN publicacion p ON p.pub_inm_id = v.type_id AND p.pub_estado = 'Aprobado'
            WHERE v.type = '$type'
            AND p.pub_usu_id = $user_id
            $str
            AND v.count >= 5
            ORDER BY v.visitor_date ASC
        ";
        return $this->db->query($sql)->result();
    }
}