<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_notify_search extends MY_Model {

    const TABLE_ID = "not_bus_id";
    const TABLE_NAME = "notificaciones_busqueda";

    protected $_name;
    protected $_email;
    protected $_phone;
    protected $_description;
    protected $_status;
    protected $_propertyTypeId;
    protected $_transactionTypeId;
    protected $_stateId;
    protected $_price;
    protected $_startValidation;
    protected $_endValidation;
    //Due to autoload is necessary to set the default data as empty string
    function __construct($name = "", $email = "", $phone = "", $description = "", $status = "", $propertyTypeId = "", $transactionTypeId = "", $stateId = "", $priceId = "", $startValidation = "", $endValidation = "")

    {
        parent::__construct();
        $this->_name = $name;
        $this->_email = $email;
        $this->_phone = $phone;
        $this->_description = $description;
        $this->_status = $status;
        $this->_propertyTypeId = $propertyTypeId;
        $this->_transactionTypeId = $transactionTypeId;
        $this->_stateId = $stateId;
        $this->_price = $priceId;
        $this->_startValidation = $startValidation;
        $this->_endValidation = $endValidation;
    }

    /**
     * Parse the current object to key => value array
     * @return array
     */
    public function toArray()
    {
        $tableAttributes = array(
                                "not_bus_id" => $this->_id,
                                "not_bus_nombre" => $this->_name,
                                "not_bus_email" => $this->_email,
                                "not_bus_telefono" => $this->_phone,
                                "not_bus_descripcion" => $this->_description,
                                "not_bus_estado" => $this->_status,
                                "not_bus_cat_id" => $this->_propertyTypeId,
                                "not_bus_for_id" => $this->_transactionTypeId,
                                "not_bus_dep_id" => $this->_stateId,
                                "not_bus_precio" => $this->_price,
                                "not_bus_vig_ini" => $this->_startValidation,
                                "not_bus_vig_fin" => $this->_endValidation
                                );
        return $tableAttributes;
    }

    /**
     * @param $className
     * @param $object
     * @return object
     */
    protected static function recast($className, $object)
    {
        $response =  null;
        if ($object instanceof stdClass)
        {
            if (!class_exists($className))
                throw new InvalidArgumentException(sprintf('Inexistant class %s.', $className));

            //Let's set the values to payment object using the data from stdObject
            $instance = new $className(
                    $object->not_bus_nombre,
                    $object->not_bus_email,
                    $object->not_bus_telefono,
                    $object->not_bus_descripcion,
                    $object->not_bus_estado,
                    $object->not_bus_cat_id,
                    $object->not_bus_for_id,
                    $object->not_bus_dep_id,
                    $object->not_bus_precio,
                    $object->not_bus_vig_ini,
                    $object->not_bus_vig_fin
            );
            $instance->_id = $object->not_bus_id;
            $response = $instance;
        }
        return $response;
    }

    ################################################################################################# BEGIN - GETTERS

    public function getName()
    {
        return $this->_name;
    }

    ################################################################################################# END - GETTERS



    ################################################################################################# BEGIN - SETTERS

    ################################################################################################# END - SETTERS

    ################################################################################################# BEGIN - DATATABLE AJAX METHODS
    /**
     * @return mixed
     */
    public static function countAll()
    {
        $ci = &get_instance();
        $ci->load->database();

        $sql = '
                select count(' . self::TABLE_ID. ') as total                
                from ' . self::TABLE_NAME.'
                left join categoria on cat_id = not_bus_cat_id
                left join forma on for_id = not_bus_for_id
                left join departamento on dep_id = not_bus_dep_id
                ';

        $query = $ci->db->query($sql);
        $totalCount = $query->row()->total;
        return $totalCount;
    }

    /**
     * @param $limit
     * @param $offset
     * @param null $orderBy
     * @param string $orderType
     * @return mixed
     */
    public static function getAll($limit, $offset, $orderBy = null, $orderType = 'asc')
    {
        if ($orderBy === null)
        {
            $orderBy = static::TABLE_ID;
        }
        $ci = &get_instance();
        $ci->load->database();

        $sql = 'select '.self::_dataTableColumns().' from ' . static::TABLE_NAME . '
                left join categoria on cat_id = not_bus_cat_id
                left join forma on for_id = not_bus_for_id
                left join departamento on dep_id = not_bus_dep_id                 
                group by '.static::TABLE_ID.' order by ' . $orderBy . ' ' . $orderType . ' limit ' . $limit . ' offset ' . $offset;
        $query = $ci->db->query($sql);
        $result = $query->result();
        return $result;
    }

    public static function search($text, $limit, $offset, $orderBy = null, $orderType = 'asc', $colsArray = null, $additionalParameters)
    {
        if ($orderBy === null)
        {
            $orderBy = self::TABLE_ID;
        }
        $ci = &get_instance();
        $ci->load->database();

        $sql = 'select '.self::_dataTableColumns().' from ' . self::TABLE_NAME;
        $sql .= '
                left join categoria on cat_id = not_bus_cat_id
                left join forma on for_id = not_bus_for_id
                left join departamento on dep_id = not_bus_dep_id
        ';
        $sql .= ' where (';
        foreach ($colsArray as $var)
        {
            $sql .= ' ' . $var . ' like \'%' . $text . '%\' or ';
        }

        $sql = substr($sql, 0, -3);
        $sql .= ') '.static::_additionalParameters($additionalParameters).' group by '.static::TABLE_ID.' order by ' . $orderBy . ' ' . $orderType . ' limit ' . $limit . ' offset ' . $offset;

        $query = $ci->db->query($sql);
        return $query->result();
    }

    public static function searchTotalCount($text, $colsArray = null, $additionalParameters)
    {
        $ci = &get_instance();
        $ci->load->database();

        $sql = 'select count(' . self::TABLE_ID . ') as total from ' . self::TABLE_NAME;
        $sql .= '
                left join categoria on cat_id = not_bus_cat_id
                left join forma on for_id = not_bus_for_id
                left join departamento on dep_id = not_bus_dep_id
        ';
        $sql .= ' where (';

        foreach ($colsArray as $var)
        {
            $sql .= ' ' . $var . ' like \'%' . $text . '%\' or ';
        }

        $sql = substr($sql, 0, -3);
        $sql .= ') '.static::_additionalParameters($additionalParameters);

        $query = $ci->db->query($sql);
        $totalCount = $query->row()->total;
        return $totalCount;
    }

    private static function _dataTableColumns()
    {
        $columns = self::TABLE_NAME.".*, cat_nombre, for_descripcion, dep_nombre";
        return $columns;
    }

    private static function _additionalParameters($list = array())
    {
        $ci=&get_instance();
        $ci->load->database();
        $sql = "";
        if(is_array($list) && count($list) >= 1)
        {
            foreach($list as $parameter => $value)
            {
                switch ($parameter)
                {
                    case "start-date":
                        $value = date( "Y-m-d", strtotime($value));
                        $sql .= " and not_bus_vig_ini >= ".$ci->db->escape($value);
                        break;
                    case "end-date":
                        $value = date( "Y-m-d", strtotime($value));
                        $sql .= " and not_bus_vig_ini <= ".$ci->db->escape($value);
                        break;
                }
            }
        }

        return $sql;
    }
    ################################################################################################# END - DATATABLE AJAX METHODS
}