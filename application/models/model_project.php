<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_project extends MY_Model {

    const TABLE_ID = "proy_id";
    const TABLE_NAME = "proyecto";

    const APPROVED = "Aprobado";
    const NOT_APPROVED = "No Aprobado";
    const CANCELED = "Cancelado";

    protected $_name;
    protected $_logo;
    protected $_video;
    protected $_description;
    protected $_latitude;
    protected $_longitude;
    protected $_address;
    protected $_addressImage;
    protected $_status;
    protected $_created;
    protected $_startValidityDate;
    protected $_finishValidityDate;
    protected $_cityId;
    protected $_zoneId;
    protected $_seo;
    protected $_shortSeo;
    protected $_contact;
    protected $_userId;
    protected $_imageDis;
    protected $_visits;
    //Due to autoload is necessary to set the default data as empty string
    function __construct($name = "", $logo = "", $video = "", $description = "", $latitude = "", $longitude = "", $address = "",
                        $addressImage = "", $status = "", $created = "", $startValidityDate = "", $finishValidityDate = "", $cityId = "",
                        $zoneId = "", $seo = "", $shortSeo = "", $contact = "", $userId = "", $imageDist = "", $visits = "0")
    {
        parent::__construct();
        $this->load->model('model_project_message');
        $this->_name = $name;
        $this->_logo = $logo;
        $this->_video = $video;
        $this->_description = $description;
        $this->_latitude = $latitude;
        $this->_longitude = $longitude;
        $this->_address = $address;
        $this->_addressImage = $addressImage;
        $this->_status = $status;
        $this->_created = $created;
        $this->_startValidityDate = $startValidityDate;
        $this->_finishValidityDate = $finishValidityDate;
        $this->_cityId = $cityId;
        $this->_zoneId = $zoneId;
        $this->_seo = $seo;
        $this->_shortSeo = $shortSeo;
        $this->_contact = $contact;
        $this->_userId = $userId;
        $this->_imageDis = $logo;
        $this->_visits = $visits;
    }

    /**
     * Parse the current object to key => value array
     * @return array
     */
    public function toArray()
    {
        $tableAttributes = array(
                                "proy_id" => $this->_id,
                                "proy_nombre" => $this->_name,
                                "proy_logo" => $this->_logo,
                                "proy_video" => $this->_video,
                                "proy_descripcion" => $this->_description,
                                "proy_latitud" => $this->_latitude,
                                "proy_longitud" => $this->_longitude,
                                "proy_ubicacion" => $this->_address,
                                "proy_ubicacion_img" => $this->_addressImage,
                                "proy_estado" => $this->_status,
                                "proy_creado" => $this->_created,
                                "proy_vig_ini" =>$this->_startValidityDate,
                                "proy_vig_fin" => $this->_finishValidityDate,
                                "proy_ciu_id" => $this->_cityId,
                                "proy_zon_id" => $this->_zoneId,
                                "proy_seo" => $this->_seo,
                                "proy_short_seo" => $this->_shortSeo,
                                "proy_contacto" => $this->_contact,
                                "proy_usu_id" => $this->_userId,
                                "proy_imagen_dis" => $this->_imageDis,
                                "proy_visitas" => $this->_visits
                                );
        return $tableAttributes;
    }

    /**
     * @param $className
     * @param $object
     * @return object
     */
    protected static function recast($className, $object)
    {
        $response =  null;
        if ($object instanceof stdClass)
        {
            if (!class_exists($className))
                throw new InvalidArgumentException(sprintf('Inexistant class %s.', $className));

            //Let's set the values to payment object using the data from stdObject
            $instance = new $className(
                $object->proy_nombre,
                $object->proy_logo,
                $object->proy_video,
                $object->proy_descripcion,
                $object->proy_latitud,
                $object->proy_longitud,
                $object->proy_ubicacion,
                $object->proy_ubicacion_img,
                $object->proy_estado,
                $object->proy_creado,
                $object->proy_vig_ini,
                $object->proy_vig_fin,
                $object->proy_ciu_id,
                $object->proy_zon_id,
                $object->proy_seo,
                $object->proy_short_seo,
                $object->proy_contacto,
                $object->proy_usu_id,
                $object->proy_imagen_dis,
                $object->proy_visitas
            );
            $instance->_id = $object->proy_id;
            $response = $instance;
        }
        return $response;
    }
    ################################################################################################# BEGIN - GETTERS
    /**
     * Get the current status
     * @return string
     */
    public function getStatus()
    {
        return  $this->_status;
    }

    public function  getName()
    {
        return  $this->_name;
    }

    public function getSeo($fullUrl = FALSE)
    {
        $url = $this->_seo;
        if($fullUrl)
        {
            $url = base_url($this->_seo);
        }
        return $url;
    }

    public function getUserId()
    {
        return $this->_userId;
    }
    ################################################################################################# END - GETTERS

    ################################################################################################# BEGIN - SETTERS
    /**
     * @param $name
     */
    public function setName($name)
    {
        $this->_name = $name;
    }

    /**
     * @param $logo
     */
    public function setLogo($logo)
    {
        if($logo != "")
        {
            $this->_logo = $logo;
        }
    }

    /**
     * @param $video
     */
    public function setVideo($video)
    {
        $this->_video = $video;
    }
    public function setDescription($description)
    {
        $this->_description = $description;
    }
    /**
     * @param $latitude
     */
    public function setLatitude($latitude)
    {
        $this->_latitude = $latitude;
    }

    /**
     * @param $longitude
     */
    public function setLongitude($longitude)
    {
        $this->_longitude = $longitude;
    }

    /**
     * @param $address
     */
    public function setAddress($address)
    {
        $this->_address = $address;
    }

    /**
     * @param $addressImage
     */
    public function setAddressImage($addressImage)
    {
        if($addressImage != "")
        {
            $this->_addressImage = $addressImage;
        }
    }

    /**
     * @param $status
     */
    public function setStatus($status)
    {
        $this->_status = $status;
    }

    /**
     * @param $startValidityDate
     */
    public function setStartValidityDate($startValidityDate)
    {
        $this->_startValidityDate = $startValidityDate;
    }

    /**
     * @param $finishValidityDate
     */
    public function setFinishValidityDate($finishValidityDate)
    {
        $this->_finishValidityDate = $finishValidityDate;
    }

    /**
     * @param $cityId
     */
    public function setCityId($cityId)
    {
        $this->_cityId = $cityId;
    }

    /**
     * @param $zoneId
     */
    public function setZoneId($zoneId)
    {
        $this->_zoneId = $zoneId;
    }

    /**
     * @param $contact
     */
    public function setContact($contact)
    {
        $this->_contact = $contact;
    }

    /**
     * @param $userId
     */
    public function setUser($userId)
    {
        $this->_userId = $userId;
    }
    ################################################################################################# END - SETTERS

    ################################################################################################# BEGIN - DATATABLE AJAX METHODS
    /**
     * @return mixed
     */
    public static function countAll()
    {
        $ci = &get_instance();
        $ci->load->database();

        $sql = '
                select count(' . static::TABLE_ID. ') as total
                from ' . static::TABLE_NAME;

        $query = $ci->db->query($sql);
        $totalCount = $query->row()->total;
        return $totalCount;
    }

    /**
     * @param $limit
     * @param $offset
     * @param null $orderBy
     * @param string $orderType
     * @return mixed
     */
    public static function getAll($limit, $offset, $orderBy = null, $orderType = 'asc')
    {
        if ($orderBy === null)
        {
            $orderBy = static::TABLE_ID;
        }
        $ci = &get_instance();
        $ci->load->database();

        $sql = 'select '.static::_dataTableColumns().' from ' . static::TABLE_NAME . '
                order by ' . $orderBy . ' ' . $orderType . ' limit ' . $limit . ' offset ' . $offset;

        $query = $ci->db->query($sql);
        $result = $query->result();
        return $result;
    }

    public static function search($text, $limit, $offset, $orderBy = null, $orderType = 'asc', $colsArray = null)
    {
        if ($orderBy === null)
        {
            $orderBy = static::TABLE_ID;
        }
        $ci = &get_instance();
        $ci->load->database();

        $sql = 'select '.static::_dataTableColumns().' from ' . static::TABLE_NAME;
        $sql .= ' where (';
        foreach ($colsArray as $var)
        {
            $sql .= ' ' . $var . ' like \'%' . $text . '%\' or ';
        }

        $sql = substr($sql, 0, -3);
        $sql .= ') order by ' . $orderBy . ' ' . $orderType . ' limit ' . $limit . ' offset ' . $offset;

        $query = $ci->db->query($sql);
        return $query->result();
    }

    public static function searchTotalCount($text, $colsArray = null)
    {
        $ci = &get_instance();
        $ci->load->database();

        $sql = 'select count(' . static::TABLE_ID . ') as total from ' . static::TABLE_NAME;
        $sql .= ' where (';

        foreach ($colsArray as $var)
        {
            $sql .= ' ' . $var . ' like \'%' . $text . '%\' or ';
        }

        $sql = substr($sql, 0, -3);
        $sql .= ')';

        $query = $ci->db->query($sql);
        $totalCount = $query->row()->total;
        return $totalCount;
    }

    private static function _dataTableColumns()
    {
        $columns = " proy_id, proy_nombre, proy_logo, proy_estado, proy_creado, proy_visitas ";
        return $columns;
    }
    ################################################################################################# END - DATATABLE AJAX METHODS

    /**
     * @param $seo
     * @param $projectId
     * @return bool
     */
    public static function getSeoNotProjectIdExists($seo, $projectId = "")
    {
        $project = static::getBySeo($seo);

        if ($projectId == "")
        {
            //new
            if (!$project instanceof model_project)
            {
                //there isn't occurrences
                $response = false;
            }
            else
            {
                //If there is a project with same seo
                $response = true;
            }
        }
        else
        {
            //If is edition
            if (!$project instanceof model_project)
            {
                //there isn't occurrences
                $response = false;
            }
            elseif ($project->getId() == $projectId)
            {
                //If there is a project with same seo but is the same project
                $response = false;
            }
            else
            {
                $response = true;
            }
        }
        return $response;
    }

    /**
     * @param $seo
     * @return object
     */
    public static function getBySeo($seo)
    {
        $ci = &get_instance();
        $ci->load->database();

        $sql = "select ".static::TABLE_NAME.".* from ".static::TABLE_NAME." where proy_seo = " . $ci->db->escape($seo);
        $query = $ci->db->query($sql);
        return static::recast(get_called_class(), $query->row());
    }

    public function createDependencies()
    {
        $property = new model_property(
            $this->_name,
            $this->_logo,
            $this->_video,
            $this->_cityId,
            0,
            NULL,
            $this->_address,
            "",
            "",
            NULL,
            $this->_zoneId,
            NULL,
            "",
            "",
            "",
            "",
            "",
            "",
            "0",
            "0",
            "No",
            "",
            "",
            "Si",
            $this->_id
        );
        $property->save();

        $publication = new model_publication(
            date("Y-m-d H:i:s"),
            "",
            "",
            "0",
            $property->getId(),
            NULL,
            "No Aprobado",
            "",
            "0",
            "0",
            NULL
        );
        $publication->save();
        $this->createSettings();
    }
    public function createSettings()
    {
        $settings = new model_projectsettings($this->_id);
        $settings->save();
    }

    public function getEnabledProjects()
    {
        $ci = &get_instance();
        $ci->load->database();
        $sql = "
        SELECT
        proy_nombre 'name', ciu_nombre 'city', for_descripcion 'transaction_type', mon_abreviado 'currency', inm_precio 'price', proy_seo 'seo_url', inm_proy_id, proy_fot_imagen, proy_ubicacion
        FROM
            inmueble
            left JOIN categoria ON cat_id = inm_cat_id
            left JOIN forma ON for_id = inm_for_id
            left JOIN moneda ON mon_id = inm_mon_id
            left JOIN proy_foto ON proy_id = inm_proy_id
            left JOIN proyecto p ON p.proy_id = inm_proy_id
            left JOIN ciudad ON ciu_id = p.`proy_ciu_id`
        WHERE
            inm_proy_id <> 0
         AND p.proy_estado = 'Aprobado'
        GROUP BY inm_proy_id
        ORDER BY RAND()
        ";
        $query = $ci->db->query($sql);
        $response = $query->result_array();
        return $response;
    }
}