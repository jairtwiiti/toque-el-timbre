<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_projectsettings extends MY_Model {

    const TABLE_ID = "id";
    const TABLE_NAME = "proyecto_config";

    protected $_projectId;
    protected $_homeLabel;
    protected $_typologyLabel;
    protected $_locationLabel;
    protected $_formLabel;
    protected $_contactLabel;
    protected $_priceLabel;
    protected $_finishingDetailLabel;
    protected $_socialAreaLabel;
    protected $_galleryLabel;
    protected $_showDescription;
    protected $_showTypology;
    protected $_showLocation;
    protected $_showForm;
    protected $_showContact;
    protected $_showPrice;
    protected $_showFinishingDetail;
    protected $_showSocialArea;
    protected $_showGallery;
    protected $_showTypologyDetail;
    protected $_status;

    //Due to autoload is necessary to set the default data as empty string
    function __construct($projectId = "", $homeLabel = "Descripcion Proyecto", $typologyLabel = "Tipologia", $locationLabel = "Mapa de Ubicación", $formLabel = "Formulario de Contacto",
                            $contactLabel = "Vendedor", $priceLabel = "Precio", $finishingDetailLabel = "Acabado", $socialAreaLabel = "Area Social", $galleryLabel = "Foto Galeria",
                            $showDescription = "Si", $showTypology = "Si", $showLocation = "Si", $showForm = "Si", $showContact = "Si", $showPrice = "Si", $showFinishingDetail = "Si",
                            $showSocialArea = "Si", $showGallery = "Si", $showTypologyDetail = "No", $status = "Activo")
    {
        parent::__construct();
        $this->_projectId = $projectId;
        $this->_homeLabel = $homeLabel;
        $this->_typologyLabel = $typologyLabel;
        $this->_locationLabel = $locationLabel;
        $this->_formLabel = $formLabel;
        $this->_contactLabel = $contactLabel;
        $this->_priceLabel = $priceLabel;
        $this->_finishingDetailLabel = $finishingDetailLabel;
        $this->_socialAreaLabel = $socialAreaLabel;
        $this->_galleryLabel = $galleryLabel;
        $this->_showDescription = $showDescription;
        $this->_showTypology = $showTypology;
        $this->_showLocation = $showLocation;
        $this->_showForm = $showForm;
        $this->_showContact = $showContact;
        $this->_showPrice = $showPrice;
        $this->_showFinishingDetail = $showFinishingDetail;
        $this->_showSocialArea = $showSocialArea;
        $this->_showGallery = $showGallery;
        $this->_showTypologyDetail = $showTypologyDetail;
        $this->_status = $status;
    }

    /**
     * Parse the current object to key => value array
     * @return array
     */
    public function toArray()
    {
        $tableAttributes = array(
                                "id" => $this->_id,
                                "proy_id" => $this->_projectId,
                                "menu_inicio" => $this->_homeLabel,
                                "menu_tipologia" => $this->_typologyLabel,
                                "menu_ubicacion" => $this->_locationLabel,
                                "menu_formulario" => $this->_formLabel,
                                "menu_contacto" => $this->_contactLabel,
                                "menu_precio" => $this->_priceLabel,
                                "menu_acabado" => $this->_finishingDetailLabel,
                                "menu_social" => $this->_socialAreaLabel,
                                "menu_galeria" => $this->_galleryLabel,
                                "check_descripcion" => $this->_showDescription,
                                "check_tipologia" => $this->_showTypology,
                                "check_ubicacion" => $this->_showLocation,
                                "check_formulario" => $this->_showForm,
                                "check_contacto" => $this->_showContact,
                                "check_precio" => $this->_showPrice,
                                "check_acabado" => $this->_showFinishingDetail,
                                "check_social" => $this->_showSocialArea,
                                "check_galeria" => $this->_showGallery,
                                "check_tipologia_det" => $this->_showTypologyDetail,
                                "estado" => $this->_status,
                                );
        return $tableAttributes;
    }

    /**
     * @param $className
     * @param $object
     * @return object
     */
    protected static function recast($className, $object)
    {
        $response =  null;
        if ($object instanceof stdClass)
        {
            if (!class_exists($className))
                throw new InvalidArgumentException(sprintf('Inexistant class %s.', $className));

            //Let's set the values to payment object using the data from stdObject
            $instance = new $className(
                    $object->proy_id,
                    $object->menu_inicio,
                    $object->menu_tipologia,
                    $object->menu_ubicacion,
                    $object->menu_formulario,
                    $object->menu_contacto,
                    $object->menu_precio,
                    $object->menu_acabado,
                    $object->menu_social,
                    $object->menu_galeria,
                    $object->check_descripcion,
                    $object->check_tipologia,
                    $object->check_ubicacion,
                    $object->check_formulario,
                    $object->check_contacto,
                    $object->check_precio,
                    $object->check_acabado,
                    $object->check_social,
                    $object->check_galeria,
                    $object->check_tipologia_det,
                    $object->estado
            );
            $instance->_id = $object->id;
            $response = $instance;
        }
        return $response;
    }

    ################################################################################################# BEGIN - GETTERS

    public static function getByProjectId($projectId)
    {
        $ci=&get_instance();
        $ci->load->database();
        $sql = "
            select * from ".static::TABLE_NAME." where proy_id = ".$ci->db->escape($projectId)."
        ";
        $query = $ci->db->query($sql);
        $response = static::recast(get_called_class(),$query->row());
        return $response;
    }
    ################################################################################################# END - GETTERS



    ################################################################################################# BEGIN - SETTERS

    public function setHomeLabel($homeLabel)
    {
        $this->_homeLabel = $homeLabel;
    }

    public function setTypologyLabel($typologyLabel)
    {
        $this->_typologyLabel = $typologyLabel;
    }

    public function setLocationLabel($locationLabel)
    {
        $this->_locationLabel = $locationLabel;
    }

    public function setFormLabel($formLabel)
    {
        $this->_formLabel = $formLabel;
    }
    public function setContactLabel($contactLabel)
    {
        $this->_contactLabel = $contactLabel;
    }
    public function setPriceLabel($priceLabel)
    {
        $this->_priceLabel = $priceLabel;
    }
    public function setFinishingDetailLabel($finishingDetailLabel)
    {
        $this->_finishingDetailLabel = $finishingDetailLabel;
    }
    public function setSocialAreaLabel($socialAreaLabel)
    {
        $this->_socialAreaLabel = $socialAreaLabel;
    }

    public function setGalleryLabel($galleryLabel)
    {
        $this->_galleryLabel = $galleryLabel;
    }

    public function setShowDescription($showDescription)
    {
        $this->_showDescription = $showDescription;
    }

    public function setShowTypology($showTypology)
    {
        $this->_showTypology = $showTypology;
    }

    public function setShowLocation($showLocation)
    {
        $this->_showLocation = $showLocation;
    }

    public function setShowForm($showForm)
    {
        $this->_showForm = $showForm;
    }

    public function setShowContact($showContact)
    {
        $this->_showContact = $showContact;
    }

    public function setShowPrice($showPrice)
    {
        $this->_showPrice = $showPrice;
    }

    public function setShowFinishingDetail($showFinishingDetail)
    {
        $this->_showFinishingDetail = $showFinishingDetail;
    }

    public function setShowSocialArea($showSocialArea)
    {
        $this->_showSocialArea = $showSocialArea;
    }

    public function setShowGallery($showGallery)
    {
        $this->_showGallery = $showGallery;
    }

    public function setShowTypologyDetail($showTypologyDetail)
    {
        $this->_showTypologyDetail = $showTypologyDetail;
    }


    ################################################################################################# END - SETTERS
    public static function search($stateId, $text, $limit, $offset, $orderBy = null, $orderType = 'asc', $colsArray = null)
    {
        if ($orderBy === null)
        {
            $orderBy = static::TABLE_ID;
        }
        $ci = &get_instance();
        $ci->load->database();

        $sql = 'select * from ' . static::TABLE_NAME;
        $sql .= ' where (';
        foreach ($colsArray as $var)
        {
            $sql .= ' ' . $var . ' like \'%' . $text . '%\' or ';
        }

        $sql = substr($sql, 0, -3);
        $sql .= ') and ciu_dep_id ='.$ci->db->escape($stateId).' order by ' . $orderBy . ' ' . $orderType . ' limit ' . $limit . ' offset ' . $offset;
        $query = $ci->db->query($sql);
        return static::recastArray(get_called_class(), $query->result());
    }
}