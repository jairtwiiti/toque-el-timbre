<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_facebook_lead extends MY_Model {

    const TABLE_ID = "fle_id";
    const TABLE_NAME = "facebook_leads";

    protected $_formId;
    protected $_fbLeadgenId;
    protected $_query;
    protected $_email;
    protected $_fullName;
    protected $_firstName;
    protected $_lastName;
    protected $_phoneNumber;
    protected $_city;
    protected $_province;
    protected $_postCode;
    protected $_streetAddress;
    protected $_country;
    protected $_dateOfBirth;
    protected $_maritalStatus;
    protected $_militaryStatus;
    protected $_gender;
    protected $_relationshipStatus;
    protected $_jobTitle;
    protected $_workEmail;
    protected $_workPhoneNumber;
    protected $_companyName;
    protected $_debugInfo;

    //Due to autoload is necessary to set the default data as empty string
    function __construct($formId = "",$fbLeadgenId =  "", $query = "", $email = "", $fullName = "", $firstName = "", $lastName = "", $phoneNumber = "",
                        $city = "", $province = "", $postCode = "", $streetAddress = "", $country = "", $dateOfBirth = "", $maritalStatus = "",
                        $militaryStatus = "", $gender = "", $relationshipStatus = "", $jobTitle = "", $workEmail = "", $workPhoneNumber = "",
                        $companyName = "", $debugInfo = "")
    {
        parent::__construct();
        $this->_formId = $formId;
        $this->_fbLeadgenId = $fbLeadgenId;
        $this->_query = $query;
        $this->_email = $email;
        $this->_fullName = $fullName;
        $this->_firstName = $firstName;
        $this->_lastName = $lastName;
        $this->_phoneNumber = $phoneNumber;
        $this->_city = $city;
        $this->_province = $province;
        $this->_postCode = $postCode;
        $this->_streetAddress = $streetAddress;
        $this->_country = $country;
        $this->_dateOfBirth = $dateOfBirth;
        $this->_maritalStatus = $maritalStatus;
        $this->_militaryStatus = $militaryStatus;
        $this->_gender = $gender;
        $this->_relationshipStatus = $relationshipStatus;
        $this->_jobTitle = $jobTitle;
        $this->_workEmail = $workEmail;
        $this->_workPhoneNumber = $workPhoneNumber;
        $this->_companyName = $companyName;
        $this->_debugInfo = $debugInfo;
    }

    /**
     * Parse the current object to key => value array
     * @return array
     */
    public function toArray()
    {
        $tableAttributes = array(
                "fle_id" => $this->_id,
                "fle_form_id" =>  $this->_formId,
                "fle_fb_leadgen_id" =>  $this->_fbLeadgenId,
                "fle_consulta" => $this->_query,
                "fle_email" => $this->_email,
                "fle_full_name" => $this->_fullName,
                "fle_first_name" => $this->_firstName,
                "fle_last_name" => $this->_lastName,
                "fle_phone_number" => $this->_phoneNumber,
                "fle_city" => $this->_city,
                "fle_province" => $this->_province,
                "fle_post_code" => $this->_postCode,
                "fle_street_address" => $this->_streetAddress,
                "fle_country" => $this->_country,
                "fle_date_of_birth" => $this->_dateOfBirth,
                "fle_marital_status" => $this->_maritalStatus,
                "fle_military_status" => $this->_militaryStatus,
                "fle_gender" => $this->_gender,
                "fle_relationship_status" => $this->_relationshipStatus,
                "fle_job_title" => $this->_jobTitle,
                "fle_work_email" => $this->_workEmail,
                "fle_work_phone_number" => $this->_workPhoneNumber,
                "fle_company_name" => $this->_companyName,
                "fle_debug_info" => $this->_debugInfo,
                "fle_deleted" => $this->_deleted,
                "fle_createdon" => $this->_createdOn,
                "fle_createdby" => $this->_createdBy,
                "fle_editedon" => $this->_editedOn,
                "fle_editedby" => $this->_editedBy

                                );
        return $tableAttributes;
    }

    /**
     * @param $className
     * @param $object
     * @return object
     */
    protected static function recast($className, $object)
    {
        $response =  null;
        if ($object instanceof stdClass)
        {
            if (!class_exists($className))
                throw new InvalidArgumentException(sprintf('Inexistant class %s.', $className));

            //Let's set the values to payment object using the data from stdObject
            $instance = new $className(
                    $object->fle_form_id,
                    $object->fle_fb_leadgen_id,
                    $object->fle_consulta,
                    $object->fle_email,
                    $object->fle_full_name,
                    $object->fle_first_name,
                    $object->fle_last_name,
                    $object->fle_phone_number,
                    $object->fle_city,
                    $object->fle_province,
                    $object->fle_post_code,
                    $object->fle_street_address,
                    $object->fle_country,
                    $object->fle_date_of_birth,
                    $object->fle_marital_status,
                    $object->fle_military_status,
                    $object->fle_gender,
                    $object->fle_relationship_status,
                    $object->fle_job_title,
                    $object->fle_work_email,
                    $object->fle_work_phone_number,
                    $object->fle_company_name,
                    $object->fle_debug_info

            );
            $instance->_id = $object->fle_id;
            $instance->_deleted = $object->fle_deleted;
            $instance->_createdOn = $object->fle_createdon;
            $instance->_createdBy = $object->fle_createdby;
            $instance->_editedOn = $object->fle_editedon;
            $instance->_editedBy = $object->fle_editedby;
            $response = $instance;
        }
        return $response;
    }

    ################################################################################################# BEGIN - GETTERS

    ################################################################################################# END - GETTERS


    ################################################################################################# BEGIN - SETTERS

    ################################################################################################# END - SETTERS

    public function notifyToUser()
    {
        $ci = &get_instance();
        $sendMessageResponse = array("response" => static::MESSAGE_RESP_ERROR,"message" => 'Su mensaje no pudo ser enviado.');
        $data = array();
        $leadForm = model_facebook_lead_form::getById($this->_formId);
        $userReceiver = NULL;

        //If exist the leadgen form then let's search to user receiver
        if($leadForm instanceof model_facebook_lead_form)
        {
            //To search an user first let's eval if we have a project or property defined
            $publication = NULL;
            if($leadForm->getProjectId() != "")
            {
                $userReceiver = model_user::getByProjectId($leadForm->getProjectId());
                $publication = model_project::getById($leadForm->getProjectId());
            }
            elseif($leadForm->getPropertyId() != "")
            {
                $userReceiver = model_user::getByPropertyId($leadForm->getPropertyId());
                $publication = model_property::getById($leadForm->getPropertyId());
            }
            //if the user receiver exist then let's send the email
            if($userReceiver instanceof model_user)
            {

                $userReceiver = $userReceiver->toArray();
                $view = "email_template/contact-message-facebook-lead";
                $data['leadgen'] = $this->toArray();
                $data['publication'] = $publication;
                $emailHandler = new EmailHandler();
                $email = $emailHandler->initialize();
                $email->from(EmailHandler::getSender(), 'ToqueElTimbre');
                $email->to($userReceiver['usu_email']);
                if($publication instanceof model_project && $publication->getId() == 73)
                {
                    $email->cc(array("karinarocabado@forza.com.bo", "alejandranavia@forza.com.bo", "sergioestivariz@forza.com.bo"));
                }
                $email->bcc("paola@toqueeltimbre.com");

                $email->subject("Nuevo cliente potencial");
                $email->message($ci->load->view($view,$data, true));
                try
                {
                    if($email->Send())
                    {
                        $sendMessageResponse['response'] = static::MESSAGE_RESP_SUCCESS;
                        $sendMessageResponse['message'] = 'Su mensaje fue enviado correctamente';
                    }
                }
                catch (Exception $e)
                {
                    $sendMessageResponse['response'] = static::MESSAGE_RESP_ERROR;
                    $sendMessageResponse['message'] = $e->getMessage();
                }
            }
        }

        return $sendMessageResponse;
    }

    public static function getByProjectId($projectId, $formId = NULL)
    {
        $ci = &get_instance();
        $ci->load->database();

        $includeFormFilter = "";
        if(!is_null($formId) || $formId != "")
        {
            $includeFormFilter = " and fle_form_id = ".$ci->db->escape($formId)." ";
        }

        $sql = "
            SELECT
                facebook_leads.*
            FROM
                facebook_leads
            LEFT JOIN facebook_lead_forms on fle_form_id = flf_id
            where flf_project_id = ".$ci->db->escape($projectId)." ".$includeFormFilter."
        ";
        $query = $ci->db->query($sql);
        $result = static::recastArray(get_called_class(), $query->result());
        return $result;
    }

    public static function getByPropertyId($propertyId, $formId = NULL)
    {
        $ci = &get_instance();
        $ci->load->database();

        $includeFormFilter = "";
        if(!is_null($formId) || $formId != "")
        {
            $includeFormFilter = " and fle_form_id = ".$ci->db->escape($formId)." ";
        }

        $sql = "
            SELECT
                facebook_leads.*
            FROM
                facebook_leads
            LEFT JOIN facebook_lead_forms on fle_form_id = flf_id
            where flf_property_id = ".$ci->db->escape($propertyId)." ".$includeFormFilter."
        ";
        $query = $ci->db->query($sql);
        $result = static::recastArray(get_called_class(), $query->result());
        return $result;
    }

    public static function getByFormId($formId = NULL)
    {
        $ci = &get_instance();
        $ci->load->database();

        $includeFormFilter = "";
        if(!is_null($formId) || $formId != "")
        {
            $includeFormFilter = " where fle_form_id = ".$ci->db->escape($formId)." ";
        }

        $sql = "
            SELECT
                facebook_leads.*
            FROM
                facebook_leads
            LEFT JOIN facebook_lead_forms on fle_form_id = flf_id 
            ".$includeFormFilter;
        $query = $ci->db->query($sql);
        $result = static::recastArray(get_called_class(), $query->result());
        return $result;
    }
    ################################################################################################# BEGIN - DATATABLE AJAX METHODS
    /**
     * @return mixed
     */
    public static function countAll()
    {
        $ci = &get_instance();
        $ci->load->database();

        $sql = '
                select count(' . self::TABLE_ID. ') as total
                from ' . self::TABLE_NAME;

        $query = $ci->db->query($sql);
        $totalCount = $query->row()->total;
        return $totalCount;
    }

    /**
     * @param $limit
     * @param $offset
     * @param null $orderBy
     * @param string $orderType
     * @return mixed
     */
    public static function getAll($limit, $offset, $orderBy = null, $orderType = 'asc')
    {
        if ($orderBy === null)
        {
            $orderBy = static::TABLE_ID;
        }
        $ci = &get_instance();
        $ci->load->database();

        $sql = 'select '.self::_dataTableColumns().' 
                from ' . static::TABLE_NAME;
        $sql .= '
                LEFT JOIN facebook_lead_forms on fle_form_id = flf_id
                LEFT JOIN inmueble on inm_id = flf_property_id
                LEFT JOIN proyecto on proy_id = flf_project_id
        ';
        $sql .=' group by '.static::TABLE_ID.' order by ' . $orderBy . ' ' . $orderType . ' limit ' . $limit . ' offset ' . $offset;
        $query = $ci->db->query($sql);
        $result = $query->result();
        return $result;
    }

    public static function search($text, $limit, $offset, $orderBy = null, $orderType = 'asc', $colsArray = null, $additionalParams = array())
    {
        if ($orderBy === null)
        {
            $orderBy = self::TABLE_ID;
        }
        $ci = &get_instance();
        $ci->load->database();

        $sql = 'select '.self::_dataTableColumns().' from ' . self::TABLE_NAME;
        $sql .= '
                LEFT JOIN facebook_lead_forms on fle_form_id = flf_id
                LEFT JOIN inmueble on inm_id = flf_property_id
                LEFT JOIN proyecto on proy_id = flf_project_id
        ';
        $sql .= ' where (';
        foreach ($colsArray as $var)
        {
            $sql .= ' ' . $var . ' like \'%' . $text . '%\' or ';
        }

        $sql = substr($sql, 0, -3);
        $sql .= ') '.static::_additionalParameters($additionalParams).' group by '.static::TABLE_ID.' order by ' . $orderBy . ' ' . $orderType . ' limit ' . $limit . ' offset ' . $offset;

        $query = $ci->db->query($sql);
        return $query->result();
    }

    public static function searchTotalCount($text, $colsArray = null, $additionalParams = array())
    {
        $ci = &get_instance();
        $ci->load->database();

        $sql = 'select count(' . self::TABLE_ID . ') as total from ' . self::TABLE_NAME;
        $sql .= '
                LEFT JOIN facebook_lead_forms on fle_form_id = flf_id
                LEFT JOIN inmueble on inm_id = flf_property_id
                LEFT JOIN proyecto on proy_id = flf_project_id
        ';
        $sql .= ' where (';

        foreach ($colsArray as $var)
        {
            $sql .= ' ' . $var . ' like \'%' . $text . '%\' or ';
        }

        $sql = substr($sql, 0, -3);
        $sql .= ') '.static::_additionalParameters($additionalParams);

        $query = $ci->db->query($sql);
        $totalCount = $query->row()->total;
        return $totalCount;
    }

    private static function _dataTableColumns()
    {
        $columns = self::TABLE_NAME.".*, proy_nombre, proy_seo, inm_nombre, inm_seo";
        return $columns;
    }

    private static function _additionalParameters($list = array())
    {
        $ci=&get_instance();
        $ci->load->database();
        $sql = "";
        if(is_array($list) && count($list) >= 1)
        {
            foreach($list as $parameter => $value)
            {
                switch ($parameter)
                {
                    case "facebook-form-id":
                        $formIds = explode(",",$value);
                        $idEscapedList = "";
                        foreach ($formIds as $id)
                        {
                            $idEscapedList .= $ci->db->escape($id).",";
                        }
                        $idEscapedList = substr($idEscapedList,0,-1);
                        $sql .= " and fle_form_id in(".$idEscapedList.")";
                    case "start-date":
                        $value = date( "Y-m-d", strtotime($value));
                        $sql .= " and fle_createdon >= ".$ci->db->escape($value);
                        break;
                    case "end-date":
                        $value = date( "Y-m-d", strtotime($value));
                        $sql .= " and fle_createdon <= ".$ci->db->escape($value);
                        break;
                }
            }
        }

        return $sql;
    }
    ################################################################################################# END - DATATABLE AJAX METHODS
}