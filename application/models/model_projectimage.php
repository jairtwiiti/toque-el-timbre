<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_projectimage extends MY_Model {

    const TABLE_ID = "proy_fot_id";
    const TABLE_NAME = "proy_foto";

    protected $_imageName;
    protected $_projectId;
    protected $_isAdImage;
    protected $_isCoverImage;
    protected $_description;
    protected $_order;
    //Due to autoload is necessary to set the default data as empty string
    function __construct($imageName = "", $projectId = "", $isAdImage = "", $isCoverImage = "", $description = "", $order = "")
    {
        parent::__construct();
        $this->_imageName = $imageName;
        $this->_projectId = $projectId;
        $this->_isAdImage = $isAdImage;
        $this->_isCoverImage = $isCoverImage;
        $this->_description = $description;
        $this->_order = $order;
    }

    /**
     * Parse the current object to key => value array
     * @return array
     */
    public function toArray()
    {
        $tableAttributes = array(
                                "proy_fot_id" => $this->_id,
                                "proy_fot_imagen" => $this->_imageName,
                                "proy_id" => $this->_projectId,
                                "proy_fot_anuncio" => $this->_isAdImage,
                                "proy_fot_portada" => $this->_isCoverImage,
                                "proy_fot_descripcion" => $this->_description,
                                "proy_fot_order" => $this->_order
                                );
        return $tableAttributes;
    }

    /**
     * @param $className
     * @param $object
     * @return object
     */
    protected static function recast($className, $object)
    {
        $response =  null;
        if ($object instanceof stdClass)
        {
            if (!class_exists($className))
                throw new InvalidArgumentException(sprintf('Inexistant class %s.', $className));

            //Let's set the values to payment object using the data from stdObject
            $instance = new $className(
                    $object->proy_fot_imagen,
                    $object->proy_id,
                    $object->proy_fot_anuncio,
                    $object->proy_fot_portada,
                    $object->proy_fot_descripcion,
                    $object->proy_fot_order
            );
            $instance->_id = $object->proy_fot_id;
            $response = $instance;
        }
        return $response;
    }
    ################################################################################################# BEGIN - GETTERS

    public function getImageName()
    {
        return $this->_imageName;
    }

    public static function getByProjectId($projectId)
    {
        $ci=&get_instance();
        $ci->load->database();
        $sql = "
            select * from ".static::TABLE_NAME." where proy_id = ".$ci->db->escape($projectId)." order by proy_fot_order
        ";
        $query = $ci->db->query($sql);
        $response = static::recastArray(get_called_class(),$query->result());
        return $response;
    }
    ################################################################################################# END - GETTERS



    ################################################################################################# BEGIN - SETTERS

    public function setImageName($imageName)
    {
        $file = new File_Handler($this->_imageName,"projectImage");
        $file->delete();
        $this->_imageName = $imageName;
    }

    public function setIsAdImage($isAdImage)
    {
        $this->_isAdImage = $isAdImage;
    }

    public function setIsCoverImage($isCoverImage)
    {
        $this->_isCoverImage = $isCoverImage;
    }

    public function setDescription($description)
    {
        $this->_description = $description;
    }
    ################################################################################################# END - SETTERS
    public static function search($stateId, $text, $limit, $offset, $orderBy = null, $orderType = 'asc', $colsArray = null)
    {
        if ($orderBy === null)
        {
            $orderBy = static::TABLE_ID;
        }
        $ci = &get_instance();
        $ci->load->database();

        $sql = 'select * from ' . static::TABLE_NAME;
        $sql .= ' where (';
        foreach ($colsArray as $var)
        {
            $sql .= ' ' . $var . ' like \'%' . $text . '%\' or ';
        }

        $sql = substr($sql, 0, -3);
        $sql .= ') and ciu_dep_id ='.$ci->db->escape($stateId).' order by ' . $orderBy . ' ' . $orderType . ' limit ' . $limit . ' offset ' . $offset;
        $query = $ci->db->query($sql);
        return static::recastArray(get_called_class(), $query->result());
    }

    public static function updateBatch($list = array())
    {
        $ci=&get_instance();
        $ci->load->database();
        $ci->db->update_batch(static::TABLE_NAME, $list, static::TABLE_ID);
    }

    public function delete($makePhysicalDelete = FALSE)
    {
        $file = new File_Handler($this->_imageName,"projectImage");
        $file->delete();
        parent::delete($makePhysicalDelete);
    }
}