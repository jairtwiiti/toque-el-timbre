<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_mensaje extends CI_Model {

    private $table = 'mensaje';
    private $id = 'men_id';

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    /**
     * Gets all results for this model but get a one result by id too.
     *
     * @param bool $id
     * @return []
     */
    function get_all($id = FALSE) {
        if ($id === FALSE) {
            $query = $this->db->get($this->table);
            return $query->result_array();
        }

        $query = $this->db->get_where($this->table, array($this->id => $id));
        return $query->row_array();
    }

    /**
     * Add new Model
     *
     * @param $data
     * @return mixed
     */
    function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    /**
     * Update Model
     *
     * @param $id
     * @param $data
     * @return mixed
     */
    function update($id, $data) {
        $this->db->where($this->id, $id);
        return $this->db->update($this->table, $data);
    }

    /**
     * Delete Model
     *
     * @param $id
     * @return mixed
     */
    function delete($id) {
        return $this->db->update($this->table, array($this->id => $id));
    }

    function get_num_messages_unread($usu_id){
        $sql = "
        SELECT COUNT(men.`men_id`) AS cantidad FROM publicacion pub
        INNER JOIN inmueble inm ON (inm.`inm_id` = pub.`pub_inm_id`)
        INNER JOIN mensaje men ON (men.`men_inm_id` = inm.`inm_id` AND men.`men_leido` = 'No' AND men.`men_estado` = 'Visible')
        WHERE pub.`pub_usu_id` = '$usu_id'
        ";
        $num = $this->db->query($sql)->row();
        return $num->cantidad;
    }

    function get_num_messages_unread_project($usu_id){
        $sql = "
        SELECT COUNT(mp.men_id) AS cantidad FROM proyecto p
        INNER JOIN mensaje_proyecto mp ON (mp.`men_proy_id` = p.`proy_id` AND mp.`men_leido` = 'No' AND mp.`men_estado` = 'Visible')
        WHERE p.`proy_usu_id` = '$usu_id'
        ";
        $num = $this->db->query($sql)->row();
        return $num->cantidad;
    }

    function get_num_messages_inbox($usu_id, $search = ""){
        $sql = "
        select sum(cantidad) as cantidad from (
            SELECT COUNT(rsmen.`men_id`) AS cantidad FROM message_real_state rsmen
            WHERE 
            rsmen.men_usu_id = '$usu_id'
            and rsmen.men_nombre LIKE '%$search%'
            and rsmen.men_estado = 'Visible'
            union
            SELECT COUNT(men.`men_id`) AS cantidad FROM publicacion pub
            INNER JOIN inmueble inm ON (inm.`inm_id` = pub.`pub_inm_id` AND (inm.inm_nombre LIKE '%$search%'))
            INNER JOIN mensaje men ON (men.`men_inm_id` = inm.`inm_id` AND men.`men_estado` = 'Visible')
            WHERE pub.`pub_usu_id` = '$usu_id'
            and (men.men_user_sender != '$usu_id' or men.men_user_sender is NULL)
        ) as total
        ";
        $num = $this->db->query($sql)->row();
        return $num->cantidad;
    }

    function get_num_messages_inbox_project($usu_id, $search = ""){
        $sql = "
        SELECT COUNT(mp.men_id) AS cantidad FROM proyecto p
        INNER JOIN mensaje_proyecto mp ON (mp.`men_proy_id` = p.`proy_id` AND mp.`men_estado` = 'Visible')
        WHERE p.`proy_usu_id` = '$usu_id'
        AND p.proy_nombre LIKE '%$search%'
        ";
        $num = $this->db->query($sql)->row();
        return $num->cantidad;
    }

    function get_messages_top_menu($usu_id){
        $sql = "
        SELECT men.* FROM publicacion pub
        INNER JOIN inmueble inm ON (inm.`inm_id` = pub.`pub_inm_id`)
        INNER JOIN mensaje men ON (men.`men_inm_id` = inm.`inm_id` AND men.`men_estado` = 'Visible')
        WHERE pub.`pub_usu_id` = '$usu_id'
        GROUP BY men.`men_id`
        ORDER BY men.`men_fech` DESC
        LIMIT 10
        ";
        return $this->db->query($sql)->result();
    }

    function get_messages_top_menu_project($usu_id){
        $sql = "
        SELECT mp.* FROM proyecto p
        INNER JOIN mensaje_proyecto mp ON (mp.`men_proy_id` = p.`proy_id` AND mp.`men_estado` = 'Visible')
        WHERE p.`proy_usu_id` = '$usu_id'
        ORDER BY mp.`men_fech` DESC
        LIMIT 10
        ";
        return $this->db->query($sql)->result();
    }

    function get_messages_by_user($usu_id, $page, $limit, $search = ""){
        $page = $page - 1;
        $page = $page * $limit;
        $sql = "
        #property message
        SELECT men.*, inm.inm_nombre, 'Mensaje' as tipo FROM publicacion pub
        INNER JOIN inmueble inm ON (inm.`inm_id` = pub.`pub_inm_id`)
        INNER JOIN mensaje men ON (men.`men_inm_id` = inm.`inm_id` AND men.`men_estado` = 'Visible' AND (men.`men_nombre` LIKE '%$search%' OR inm.inm_nombre LIKE '%$search%'))
        WHERE pub.`pub_usu_id` = '$usu_id'
        and (men.men_user_sender != '$usu_id' or men.men_user_sender is NULL)
        UNION 
        SELECT men.*, inm.inm_nombre, 'Mensaje' as tipo FROM publicacion pub
        INNER JOIN inmueble inm ON (inm.`inm_id` = pub.`pub_inm_id`)
        INNER JOIN mensaje men ON (men.`men_inm_id` = inm.`inm_id` AND men.`men_estado` = 'Visible' AND (men.`men_nombre` LIKE '%%' OR inm.inm_nombre LIKE '%%'))
        WHERE  men.men_user_receiver = '$usu_id'
        UNION 
        SELECT men.*, inm.inm_nombre, 'Mensaje' as tipo FROM publicacion pub
        INNER JOIN inmueble inm ON (inm.`inm_id` = pub.`pub_inm_id`)
        INNER JOIN mensaje men ON (men.`men_inm_id` = inm.`inm_id` AND men.`men_estado` = 'Visible' AND (men.`men_nombre` LIKE '%$search%' OR inm.inm_nombre LIKE '%$search%'))
        WHERE pub.`pub_usu_id` = '$usu_id'
        and men.men_user_sender != '$usu_id'        
        union
        #real state message
        SELECT men_id, men_nombre, men_empresa, men_telefono, men_email, men_descripcion, men_leido, men_fech, men_usu_id as men_inm_id, men_ciudad, men_estado, men_user_sender, men_user_receiver, 'Contacto Inmobiliaria', 'Mensaje inmobiliaria' as tipo 
        FROM  message_real_state rsmen
        WHERE men_usu_id = '$usu_id'				
        and rsmen.`men_estado` = 'Visible' 
        AND (rsmen.`men_nombre` LIKE '%$search%')
        and (rsmen.men_user_sender != '$usu_id' or rsmen.men_user_sender is NULL)
         UNION 
         SELECT men_id, men_nombre, men_empresa, men_telefono, men_email, men_descripcion, men_leido, men_fech, men_usu_id as men_inm_id, men_ciudad, men_estado, men_user_sender, men_user_receiver, 'Contacto Inmobiliaria', 'Mensaje inmobiliaria' as tipo 
        FROM  message_real_state rsmen
        WHERE rsmen.men_user_receiver = '$usu_id'				
        and rsmen.`men_estado` = 'Visible' 
        AND (rsmen.`men_nombre` LIKE '%$search%')
        UNION 
        SELECT men_id, men_nombre, men_empresa, men_telefono, men_email, men_descripcion, men_leido, men_fech, men_usu_id as men_inm_id, men_ciudad, men_estado, men_user_sender, men_user_receiver, 'Contacto Inmobiliaria', 'Mensaje inmobiliaria' as tipo 
        FROM  message_real_state rsmen
        WHERE men_usu_id = '$usu_id'				
        and rsmen.`men_estado` = 'Visible' 
        AND (rsmen.`men_nombre` LIKE '%$search%')
        and rsmen.men_user_sender != '$usu_id'
        union
        SELECT mp.*, p.`proy_nombre` AS inm_nombre, 'Mensaje_Proyecto' AS tipo FROM proyecto p
        INNER JOIN mensaje_proyecto mp ON (mp.`men_proy_id` = p.`proy_id` AND mp.`men_estado` = 'Visible' AND (mp.`men_nombre` LIKE '%%' OR p.proy_nombre LIKE '%%'))
        WHERE mp.men_user_receiver = '$usu_id' 
        -- GROUP BY men.`men_id`
        ORDER BY men_fech DESC
        LIMIT $page, $limit
        ";

        return $this->db->query($sql)->result();
    }

    function get_messages_projects_by_user($usu_id, $page, $limit, $search = ""){
        $page = $page - 1;
        $page = $page * $limit;
        $sql = "
        SELECT mp.*, p.`proy_nombre` AS inm_nombre, 'Mensaje_Proyecto' AS tipo FROM proyecto p
        INNER JOIN mensaje_proyecto mp ON (mp.`men_proy_id` = p.`proy_id` AND mp.`men_estado` = 'Visible' AND (mp.`men_nombre` LIKE '%$search%' OR p.proy_nombre LIKE '%$search%'))
        WHERE p.`proy_usu_id` = '$usu_id'
        and (mp.men_user_sender != '$usu_id' or mp.men_user_sender is NULL)
        ORDER BY mp.`men_fech` DESC
        LIMIT $page, $limit
        ";

        return $this->db->query($sql)->result();
    }

    function get_message_by_id($men_id){
        $query = $this->db->select('mensaje.*, inmueble.inm_nombre')
            ->from('mensaje')
            ->join('inmueble', 'inmueble.inm_id = mensaje.men_inm_id', 'inner')
            ->where('mensaje.men_id', $men_id)
            ->get();
        return $query->row();
    }

    function get_num_messages_trash($usu_id){
        $sql = "
        SELECT COUNT(men.`men_id`) AS cantidad FROM publicacion pub
        INNER JOIN inmueble inm ON (inm.`inm_id` = pub.`pub_inm_id`)
        INNER JOIN mensaje men ON (men.`men_inm_id` = inm.`inm_id` AND men.`men_estado` = 'Novisible')
        WHERE pub.`pub_usu_id` = '$usu_id'
        ";
        $num = $this->db->query($sql)->row();
        return $num->cantidad;
    }

    function get_num_messages_trash_project($usu_id){
        $sql = "
        SELECT COUNT(mp.men_id) AS cantidad FROM proyecto p
        INNER JOIN mensaje_proyecto mp ON (mp.`men_proy_id` = p.`proy_id` AND mp.`men_estado` = 'Novisible')
        WHERE p.`proy_usu_id` = '$usu_id'
        ";
        $num = $this->db->query($sql)->row();
        return $num->cantidad;
    }

    function get_messages_trash_by_user($usu_id, $page, $limit){
        $page = $page - 1;
        $page = $page * $limit;
        $sql = "
        SELECT men.*, inm.inm_nombre FROM publicacion pub
        INNER JOIN inmueble inm ON (inm.`inm_id` = pub.`pub_inm_id`)
        INNER JOIN mensaje men ON (men.`men_inm_id` = inm.`inm_id` AND men.`men_estado` = 'Novisible')
        WHERE pub.`pub_usu_id` = '$usu_id'
        GROUP BY men.`men_id`
        ORDER BY men.`men_fech` DESC
        LIMIT $page, $limit
        ";

        return $this->db->query($sql)->result();
    }

    function get_messages_projects_trash_by_user($usu_id, $page, $limit, $search = ""){
        $page = $page - 1;
        $page = $page * $limit;
        $sql = "
        SELECT mp.*, p.`proy_nombre` AS inm_nombre, 'Mensaje_Proyecto' AS tipo FROM proyecto p
        INNER JOIN mensaje_proyecto mp ON (mp.`men_proy_id` = p.`proy_id` AND mp.`men_estado` = 'Novisible' AND (mp.`men_nombre` LIKE '%$search%' OR p.proy_nombre LIKE '%$search%'))
        WHERE p.`proy_usu_id` = '$usu_id'
        ORDER BY mp.`men_fech` DESC
        LIMIT $page, $limit
        ";

        return $this->db->query($sql)->result();
    }

    function get_num_messages_by_inmueble($inm_id){
        $sql = "
        SELECT COUNT(men.`men_id`) AS cantidad  FROM mensaje men
        WHERE men.`men_inm_id` = '$inm_id'
        ";
        $num = $this->db->query($sql)->row();
        return $num->cantidad;
    }
}