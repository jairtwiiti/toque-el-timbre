<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_cupon extends CI_Model {

    private $table = 'cupon';
    private $id = 'id';

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    /**
     * Gets all results for this model but get a one result by id too.
     *
     * @param bool $id
     * @return []
     */
    function get_all($id = FALSE) {
        if ($id === FALSE) {
            $query = $this->db->get($this->table);
            return $query->result_array();
        }

        $query = $this->db->get_where($this->table, array($this->id => $id));
        return $query->row_array();
    }

    /**
     * Add new Model
     *
     * @param $data
     * @return mixed
     */
    function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    /**
     * Update Model
     *
     * @param $id
     * @param $data
     * @return mixed
     */
    function update($id, $data) {
        $this->db->where($this->id, $id);
        return $this->db->update($this->table, $data);
    }

    /**
     * Delete Model
     *
     * @param $id
     * @return mixed
     */
    function delete($id) {
        return $this->db->update($this->table, array($this->id => $id));
    }

    function is_expired($cupon){
        $sql = "
        SELECT c.`id` FROM cupon c
        WHERE c.cupon =  '$cupon'
        AND (CURDATE() >= c.`fecha_ini` AND CURDATE() <= c.`fecha_fin`)
        AND c.`estado` =  'Activo'
        ";
        $row = $this->db->query($sql)->row();
        return !empty($row->id) ? $row->id : 0;
    }

    function is_valid($cupon){
        $this->db->where("cupon", $cupon);
        $this->db->where("estado", 'Activo');
        $row = $this->db->get($this->table)->row();
        return $row->id;
    }

}