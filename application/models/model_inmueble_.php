<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_inmueble extends CI_Model {

    private $table = 'inmueble';
    private $id = 'inm_id';

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    /**
     * Gets all results for this model but get a one result by id too.
     *
     * @param bool $id
     * @return []
     */
    function get_all($id = FALSE) {
        if ($id === FALSE) {
            $query = $this->db->get($this->table);
            return $query->result_array();
        }

        $query = $this->db->get_where($this->table, array($this->id => $id));
        return $query->row_array();
    }

    function get_inmueble_by_id($id) {
        $sql = "
        SELECT inm.*, dep.`dep_id`, zon.`zon_id` FROM inmueble inm
        INNER JOIN ciudad ciu ON (ciu.`ciu_id` = inm.`inm_ciu_id`)
        INNER JOIN departamento dep ON (dep.`dep_id` = ciu.`ciu_dep_id`)
        INNER JOIN zona zon ON (zon.`zon_dep_id` = ciu.`ciu_dep_id`)
        WHERE inm.`inm_id` = '$id'
        GROUP BY inm.`inm_id`
        ";
        return $this->db->query($sql)->row();
    }

    /**
     * Add new Model
     *
     * @param $data
     * @return mixed
     */
    function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    /**
     * Update Model
     *
     * @param $id
     * @param $data
     * @return mixed
     */
    function update($id, $data) {
        $this->db->where($this->id, $id);
        return $this->db->update($this->table, $data);
    }

    function get_by_id($id) {
       $sql = "
          SELECT
                 inm.`inm_id` AS `id`,
                 inm.`inm_nombre` AS `nombre`,
                 inm.`inm_foto` AS `foto`,
                 ci.ciu_nombre AS `ciudad`,
                 inm.`inm_precio` AS `precio`,
                 mo.mon_abreviado AS `moneda`,
                 inm.`inm_direccion` AS `direccion`,
                 inm.`inm_superficie` AS `superficie`,
                 inm.`inm_detalle` AS `detalle`,
                 ca.cat_nombre AS `categoria`,
                 zo.zon_nombre AS `zona`,
                 fo.for_descripcion AS `forma`,
                 inm.`inm_latitud` AS `latitud`,
                 inm.`inm_longitud` AS `longitud`,
                 inm.`inm_tipo_superficie` AS `tipo_superficie`,
                 inm.`inm_orden` AS `orden`,
                 inm.`inm_visitas` AS `visitas`,
                 inm.`inm_publicado` AS `publicado`,
                 inm.`inm_seo` AS `seo`,
                 inm.`inm_visible` AS `visible`,
                 inm.`inm_proy_id` AS `proy_id`,
                 u.`usu_nombre`,
                 u.`usu_apellido`,
                 u.`usu_tipo`,
                 u.`usu_telefono`,
                 u.`usu_email`
          FROM
                `inmueble` inm
                INNER JOIN ciudad ci ON (ci.ciu_id = inm.inm_ciu_id)
                INNER JOIN categoria ca ON (ca.cat_id = inm.inm_cat_id)
                INNER JOIN zona zo ON (zo.zon_id = inm.inm_zon_id)
                INNER JOIN forma fo ON (fo.for_id = inm.inm_for_id)
                INNER JOIN moneda mo ON (mo.mon_id = inm.inm_mon_id)
                INNER JOIN publicacion p ON (p.`pub_inm_id` = inm.`inm_id`)
                INNER JOIN usuario u ON (u.`usu_id` = p.`pub_usu_id`)
                AND inm.`inm_publicado` = 'SI'
                AND inm.inm_id = $id";

        return $this->db->query($sql)->result();
    }

    function get_by_url($seo) {
        $sql = "
          SELECT
                 inm.`inm_id` AS `id`,
                 inm.`inm_nombre` AS `nombre`,
                 inm.`inm_foto` AS `foto`,
                 ci.ciu_nombre AS `ciudad`,
                 inm.`inm_precio` AS `precio`,
                 mo.mon_abreviado AS `moneda`,
                 inm.`inm_direccion` AS `direccion`,
                 inm.`inm_superficie` AS `superficie`,
                 inm.`inm_detalle` AS `detalle`,
                 inm.`inm_video` AS `video`,
                 ca.cat_nombre AS `categoria`,
                 zo.zon_nombre AS `zona`,
                 fo.for_descripcion AS `forma`,
                 inm.`inm_latitud` AS `latitud`,
                 inm.`inm_longitud` AS `longitud`,
                 inm.`inm_tipo_superficie` AS `tipo_superficie`,
                 inm.`inm_orden` AS `orden`,
                 inm.`inm_visitas` AS `visitas`,
                 inm.`inm_publicado` AS `publicado`,
                 inm.`inm_seo` AS `seo`,
                 inm.`inm_visible` AS `visible`,
                 inm.`inm_proy_id` AS `proy_id`,
                 u.`usu_id`,
                 u.`usu_nombre`,
                 u.`usu_apellido`,
                 u.`usu_empresa`,
                 u.`usu_logo`,
                 u.`usu_foto`,
                 u.`usu_descripcion`,
                 u.`usu_tipo`,
                 u.`usu_telefono`,
                 u.`usu_celular`,
                 u.`usu_email`,
                 u.`usu_certificado`,
                 p.pub_vig_fin,
                 inm.`inm_ciu_id` AS `id_ciudad`,
                 inm.`inm_zon_id` AS `id_zona`,
                 inm.`inm_cat_id` AS `id_categoria`
          FROM
                `inmueble` inm
                INNER JOIN ciudad ci ON (ci.ciu_id = inm.inm_ciu_id)
                INNER JOIN categoria ca ON (ca.cat_id = inm.inm_cat_id)
                INNER JOIN zona zo ON (zo.zon_id = inm.inm_zon_id)
                INNER JOIN forma fo ON (fo.for_id = inm.inm_for_id)
                INNER JOIN moneda mo ON (mo.mon_id = inm.inm_mon_id)
                INNER JOIN publicacion p ON (p.`pub_inm_id` = inm.`inm_id`)
                INNER JOIN usuario u ON (u.`usu_id` = p.`pub_usu_id`)
                AND inm.`inm_publicado` = 'Si'
                AND inm.inm_seo = '$seo'";

        return $this->db->query($sql)->row();
    }

    function get_by_id_detail($id) {
        $sql = "
          SELECT
                 inm.`inm_id` AS `id`,
                 inm.`inm_nombre` AS `nombre`,
                 inm.`inm_foto` AS `foto`,
                 ci.ciu_nombre AS `ciudad`,
                 inm.`inm_precio` AS `precio`,
                 mo.mon_abreviado AS `moneda`,
                 inm.`inm_direccion` AS `direccion`,
                 inm.`inm_superficie` AS `superficie`,
                 inm.`inm_detalle` AS `detalle`,
                 ca.cat_nombre AS `categoria`,
                 zo.zon_nombre AS `zona`,
                 fo.for_descripcion AS `forma`,
                 inm.`inm_latitud` AS `latitud`,
                 inm.`inm_longitud` AS `longitud`,
                 inm.`inm_tipo_superficie` AS `tipo_superficie`,
                 inm.`inm_video` AS `video`,
                 inm.`inm_orden` AS `orden`,
                 inm.`inm_visitas` AS `visitas`,
                 inm.`inm_publicado` AS `publicado`,
                 inm.`inm_seo` AS `seo`,
                 inm.`inm_visible` AS `visible`,
                 inm.`inm_proy_id` AS `proy_id`,
                 u.`usu_id`,
                 u.`usu_nombre`,
                 u.`usu_apellido`,
                 u.`usu_empresa`,
                 u.`usu_logo`,
                 u.`usu_foto`,
                 u.`usu_descripcion`,
                 u.`usu_tipo`,
                 u.`usu_telefono`,
                 u.`usu_celular`,
                 u.`usu_email`,
                 u.`usu_certificado`,
                 inm.`inm_ciu_id` AS `id_ciudad`,
                 inm.`inm_zon_id` AS `id_zona`,
                 inm.`inm_cat_id` AS `id_categoria`
          FROM
                `inmueble` inm
                INNER JOIN ciudad ci ON (ci.ciu_id = inm.inm_ciu_id)
                INNER JOIN categoria ca ON (ca.cat_id = inm.inm_cat_id)
                INNER JOIN zona zo ON (zo.zon_id = inm.inm_zon_id)
                INNER JOIN forma fo ON (fo.for_id = inm.inm_for_id)
                INNER JOIN moneda mo ON (mo.mon_id = inm.inm_mon_id)
                INNER JOIN publicacion p ON (p.`pub_inm_id` = inm.`inm_id`)
                INNER JOIN usuario u ON (u.`usu_id` = p.`pub_usu_id`)
                AND inm.`inm_publicado` = 'Si'
                AND inm.inm_id = '$id'";

        return $this->db->query($sql)->row();
    }

    function get_caracteristicas($id) {
        $sql = "
            SELECT
                inm_car.`eca_inm_id` AS inmueble_id,
                car.`car_nombre` AS `caracteristica`,
                inm_car.`eca_valor` AS valor
            FROM
                `inmueble_caracteristica` inm_car
                INNER JOIN caracteristica car ON (car.car_id = inm_car.eca_car_id)
            WHERE
                inm_car.eca_inm_id = $id";

        return $this->db->query($sql)->result();
    }

    function get_main_pay_projects($limit = 0) {
        if($limit != 0){
            $filter = "LIMIT " . $limit;
        }
        $sql = "
            SELECT
                inm_nombre, cat_nombre, for_descripcion, inm_precio, pub_vig_ini, mon_abreviado, proy_fot_imagen, proy_estado,proy_seo,ciu_nombre, inm_proy_id
            FROM
                inmueble
                INNER JOIN categoria ON cat_id = inm_cat_id
                INNER JOIN forma ON for_id = inm_for_id
                INNER JOIN ciudad ON ciu_id = inm_ciu_id
                INNER JOIN moneda ON mon_id = inm_mon_id
                INNER JOIN proy_foto ON proy_id = inm_proy_id
                INNER JOIN proyecto p ON (p.proy_id = inm_proy_id AND p.proy_estado = 'Aprobado')
                INNER JOIN publicacion ON pub_inm_id = inm_id
            WHERE
              inm_proy_id <> 0
            GROUP BY inm_proy_id
            ORDER BY RAND()
            ".$filter."
        ";
        return $this->db->query($sql)->result();
    }

    function get_departmens_from_view() {
        $sql = "
            SELECT dep_id AS id, dep_nombre AS nombre, COUNT(DISTINCT inm_id) AS cantidad
            FROM bus_inmueble
            INNER JOIN inmueble_caracteristica ON (eca_inm_id = inm_id)
            LEFT JOIN caracteristica ON (eca_car_id = car_id)
            WHERE pub_estado = 'Aprobado' AND pub_vig_fin>= CURRENT_DATE() AND inm_publicado = 'Si'
            GROUP BY dep_nombre;
        ";

        return $this->db->query($sql)->result();
    }

    function get_categories_from_view() {
        $sql = "
            SELECT inm_cat_id, cat_nombre, COUNT(DISTINCT inm_id) AS cantidad
            FROM bus_inmueble
            INNER JOIN inmueble_caracteristica ON (eca_inm_id = inm_id)
            LEFT JOIN caracteristica ON (eca_car_id = car_id)
            WHERE pub_estado = 'Aprobado' AND pub_vig_fin>=CURRENT_DATE() AND inm_publicado = 'Si'
            GROUP BY cat_nombre
        ";

        return $this->db->query($sql)->result();
    }

    function get_info_property_mail($id) {
        $sql = "
            SELECT inm_id,usu_email,inm_nombre
            FROM inmueble
            INNER JOIN publicacion ON (pub_inm_id = inm_id)
		    INNER JOIN usuario ON (pub_usu_id = usu_id)
			WHERE inm_id = '$id'
        ";

        return $this->db->query($sql)->row();
    }

    function update_visitor_llamadas($id) {
        $sql = "UPDATE inmueble SET inm_llamadas = inm_llamadas + 1 WHERE inm_id = '$id'";
        $this->db->query($sql);
    }


}