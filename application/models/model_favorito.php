<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_favorito extends CI_Model {

    private $table = 'favorito';
    private $id = 'id';

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    /**
     * Gets all results for this model but get a one result by id too.
     *
     * @param bool $id
     * @return []
     */
    function get_all($id = FALSE) {
        if ($id === FALSE) {
            $query = $this->db->get($this->table);
            return $query->result();
        }

        $query = $this->db->get_where($this->table, array($this->id => $id));
        return $query->row();
    }

    /**
     * Add new Model
     *
     * @param $data
     * @return mixed
     */
    function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    /**
     * Update Model
     *
     * @param $id
     * @param $data
     * @return mixed
     */
    function update($id, $data) {
        $this->db->where($this->id, $id);
        return $this->db->update($this->table, $data);
    }

    /**
     * Delete Model
     *
     * @param $id
     * @return mixed
     */
    function delete($id) {
        return $this->db->delete($this->table, array($this->id => $id));
    }

    function delete_favorite($inm_id, $user_id) {
        $this->db->where("inmueble_id", $inm_id);
        $this->db->where("usuario_id", $user_id);
        $this->db->delete($this->table);
    }

    function get_num_favorite($id_user, $id_inmueble){
        $sql = "
        SELECT id FROM favorito
        WHERE usuario_id = $id_user
        AND inmueble_id = $id_inmueble
        ";
        return $this->db->query($sql)->num_rows();
    }

    function get_id_favorite($id_user, $id_inmueble){
        $sql = "
        SELECT * FROM favorito
        WHERE usuario_id = $id_user
        AND inmueble_id = $id_inmueble
        ";
        return $this->db->query($sql)->row();
    }
}