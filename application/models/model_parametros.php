<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_parametros extends CI_Model {

    const TABLE_NAME = "ad_parametro";
    private $table = 'ad_parametro';

    const SITE_DOMAIN = "ToqueElTimbre";

    //private $id = 'aca_id';

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    /**
     * Gets all results for this model but get a one result by id too.
     *
     * @param bool $id
     * @return []
     */
    function get_all($id = FALSE) {
        $query = $this->db->get($this->table);
        return $query->row();
    }

    public static function getAll()
    {
        $ci=&get_instance();
        $query = $ci->db->get(self::TABLE_NAME);
        return $query->row();
    }
}