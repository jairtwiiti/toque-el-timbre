<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_categoria extends CI_Model {

    private $table = 'categoria';
    private $id = 'cat_id';

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    /**
     * Gets all results for this model but get a one result by id too.
     *
     * @param bool $id
     * @return []
     */
    function get_all($id = FALSE) {
        if ($id === FALSE) {
            $query = $this->db->get($this->table);
            return $query->result_array();
        }

        $query = $this->db->get_where($this->table, array($this->id => $id));
        return $query->row_array();
    }
    
    function get_all_categories() {
        $query = $this->db->get($this->table);
        return $query->result();
    }


    /**
     * Add new Model
     *
     * @param $data
     * @return mixed
     */
    function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    /**
     * Update Model
     *
     * @param $id
     * @param $data
     * @return mixed
     */
    function update($id, $data) {
        $this->db->where($this->id, $id);
        return $this->db->update($this->table, $data);
    }

    /**
     * Delete Model
     *
     * @param $id
     * @return mixed
     */
    function delete($id) {
        return $this->db->update($this->table, array($this->id => $id));
    }

    /**
     * Get Categories having at last one publications
     *
     * @return []
     */
    function get_categories(){
        $sql = "
            SELECT
                cat.`cat_id`, cat.`cat_nombre`, COUNT(inm.inm_id) AS cantidad
            FROM
                categoria cat
                INNER JOIN inmueble inm ON(inm.`inm_cat_id` = cat.`cat_id` AND inm.`inm_publicado` = 'Si')
                INNER JOIN publicacion pub ON (pub.`pub_inm_id` = inm.`inm_id` AND pub.`pub_estado` = 'Aprobado' AND CURDATE() <= pub.`pub_vig_fin`)
            GROUP BY
                cat.`cat_id`
        ";
        return $this->db->query($sql)->result();
    }
}