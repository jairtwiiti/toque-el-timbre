<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_real_state_message extends MY_Model {

    const TABLE_ID = "men_id";
    const TABLE_NAME = "message_real_state";

    protected $_name;
    protected $_businessName;
    protected $_phone;
    protected $_email;
    protected $_description;
    protected $_read;
    protected $_date;
    protected $_state;
    protected $_userId;
    protected $_status;
    protected $_userSender;
    protected $_userReceiver;

    //Due to autoload is necessary to set the default data as empty string
    function __construct($name = "", $businessName = "", $phone = "", $email = "", $description = "", $read = "", $date = "", $state = "", $userId = "", $status = "Visible", $userSender = NULL, $userReceiver = NULL)
    {
        parent::__construct();
        $this->_name = $name;
        $this->_businessName = $businessName;
        $this->_phone = $phone;
        $this->_email = $email;
        $this->_description = $description;
        $this->_read = $read;
        $this->_date = $date;
        $this->_userId = $userId;
        $this->_state = $state;
        $this->_status = $status;
        $this->_userSender = $userSender;
        $this->_userReceiver = $userReceiver;
    }

    /**
     * Parse the current object to key => value array
     * @return array
     */
    public function toArray()
    {
        $tableAttributes = array(
                                "men_id" => $this->_id,
                                "men_nombre" => $this->_name,
                                "men_empresa" => $this->_businessName,
                                "men_telefono" => $this->_phone,
                                "men_email" => $this->_email,
                                "men_descripcion" => $this->_description,
                                "men_leido" => $this->_read,
                                "men_fech" => $this->_date,
                                "men_ciudad" => $this->_state,
                                "men_usu_id" => $this->_userId,
                                "men_estado" => $this->_status,
                                "men_user_sender" => $this->_userSender,
                                "men_user_receiver" => $this->_userReceiver
                                );
        return $tableAttributes;
    }

    /**
     * @param $className
     * @param $object
     * @return object
     */
    protected static function recast($className, $object)
    {
        $response =  null;
        if ($object instanceof stdClass)
        {
            if (!class_exists($className))
                throw new InvalidArgumentException(sprintf('Inexistant class %s.', $className));

            //Let's set the values to payment object using the data from stdObject
            $instance = new $className(
                    $object->men_nombre,
                    $object->men_empresa,
                    $object->men_telefono,
                    $object->men_email,
                    $object->men_descripcion,
                    $object->men_leido,
                    $object->men_fech,
                    $object->men_ciudad,
                    $object->men_usu_id,
                    $object->men_estado,
                    $object->men_user_sender,
                    $object->men_user_receiver
            );
            $instance->_id = $object->men_id;
            $response = $instance;
        }
        return $response;
    }

    ################################################################################################# BEGIN - GETTERS

    public function getName()
    {
        return $this->_name;
    }

    public function getPhone()
    {
        return $this->_phone;
    }

    public function getEmail()
    {
        return $this->_email;
    }

    ################################################################################################# END - GETTERS



    ################################################################################################# BEGIN - SETTERS

    public function setRead($read)
    {
        $this->_read = $read;
    }

    ################################################################################################# END - SETTERS

    public static function createMessage(model_user $userSender, $userReceiverId)
    {
        $ci=&get_instance();
        $formData = $ci->input->post();
        $company = !is_null($userSender->getCompany())?$userSender->getCompany():"";
        $phone = !is_null($userSender->getPhone())?$userSender->getPhone():"";
        $email = !is_null($userSender->getEmail())?$userSender->getEmail():"";
        $description = isset($formData['mensaje'])?$formData['mensaje']:$formData['replyMessage'];
        $state = isset($formData['ciudad'])?$formData['ciudad']:"";
        $userId = isset($formData['user-id'])?$formData['user-id']:"";
        $message = new model_real_state_message(
            $userSender->getFullName(),
            $company,
            $phone,
            $email,
            $description,
            'No',
            date('Y-m-d'),
            $state,
            $userSender->getId(),//Reference id that start the conversation
            "Visible",
            $userSender->getId(),
            $userReceiverId
        );
        $message->save();
    }
}