<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_caracteristica extends CI_Model {

    private $table = 'caracteristica';
    private $id = 'car_id';

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    /**
     * Gets all results for this model but get a one result by id too.
     *
     * @param bool $id
     * @return []
     */
    function get_all($id = FALSE) {
        if ($id === FALSE) {
            $query = $this->db->get($this->table);
            return $query->result_array();
        }

        $query = $this->db->get_where($this->table, array($this->id => $id));
        return $query->row_array();
    }

    /**
     * Add new Model
     *
     * @param $data
     * @return mixed
     */
    function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    /**
     * Update Model
     *
     * @param $id
     * @param $data
     * @return mixed
     */
    function update($id, $data) {
        $this->db->where($this->id, $id);
        return $this->db->update($this->table, $data);
    }

    /**
     * Delete Model
     *
     * @param $id
     * @return mixed
     */
    function delete($id) {
        return $this->db->update($this->table, array($this->id => $id));
    }

    function get_all_features($cat_id) {
        /*$sql = "
        SELECT caracteristica.* FROM categoria_caracteristica AS catcar INNER JOIN caracteristica ON (catcar.car_id = caracteristica.car_id)
        WHERE catcar.cat_id='$cat_id'
        ORDER BY catcar.orden";*/
        $sql = "
        SELECT caracteristica.* FROM categoria_caracteristica AS catcar
        INNER JOIN caracteristica ON (catcar.car_id = caracteristica.car_id)
        WHERE catcar.cat_id = '$cat_id'
        GROUP BY catcar.car_id
        ORDER BY catcar.orden";

        $result = $this->db->query($sql)->result();
        foreach($result as $feature):
            $sql = "
            SELECT det_key AS id,det_valor AS nombre
            FROM caracteristica_detalle
            WHERE det_car_id = ".$feature->car_id."
            ORDER BY det_orden
            ";
            $result_detail = $this->db->query($sql)->result();

            $values[$feature->car_id] = array(
                $feature->car_tipo => array(
                    'descripcion' => $feature->car_descripcion,
                    'input_name' => $feature->car_nombre,
                    'input_values' => $result_detail
                )
            );
        endforeach;

        return $values;
    }

    function get_features_by_category($cat_id) {
        $sql = "
        SELECT caracteristica.* FROM categoria_caracteristica AS catcar
        INNER JOIN caracteristica ON (catcar.car_id = caracteristica.car_id)
        WHERE catcar.cat_id = '$cat_id'
        GROUP BY catcar.car_id
        ORDER BY catcar.orden";

        return $this->db->query($sql)->result();
    }
}