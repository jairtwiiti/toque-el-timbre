<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_service extends MY_Model {

    const TABLE_ID = "ser_id";
    const TABLE_NAME = "servicios";

    //Services status
    const ACTIVE = "Active";
    const INACTIVE = "Inactive";
    //Services type
    const PRINCIPAL = "Principal";
    const SEARCH = "Busqueda";
    const SIMILAR = "Similares";

    protected $_description;
    protected $_price;
    protected $_subscriberPrice;
    protected $_days;
    protected $_status;
    protected $_type;

    //Due to autoload is necessary to set the default data as empty string
    function __construct($description = "", $price = "", $subscriberPrice = "", $days = "", $status = "", $type = "")
    {
        parent::__construct();
        $this->_description = $description;
        $this->_price = $price;
        $this->_subscriberPrice = $subscriberPrice;
        $this->_days = $days;
        $this->_status = $status;
        $this->_type = $type;
    }

    public function toArray()
    {
        $tableAttributes = array(
                                "ser_id" => $this->_id,
                                "ser_descripcion" => $this->_description,
                                "ser_precio" => $this->_price,
                                "ser_precio_suscriptor" => $this->_subscriberPrice,
                                "ser_dias" => $this->_days,
                                "ser_estado" => $this->_status,
                                "ser_tipo" => $this->_type
                                );
        return $tableAttributes;
    }
################################################################################################# BEGIN - RECAST METHODS
    /**
     * @param $className
     * @param $object
     * @return model_payment|null
     */
    protected static function recast($className, $object)
    {
        $response =  null;
        if ($object instanceof stdClass)
        {
            if (!class_exists($className))
                throw new InvalidArgumentException(sprintf('Inexistant class %s.', $className));

            //Let's set the values to payment object using the data from stdObject
            $instance = new $className(
                $object->ser_descripcion,
                $object->ser_precio,
                $object->ser_precio_suscriptor,
                $object->ser_dias,
                $object->ser_estado,
                $object->ser_tipo
            );
            $instance->_id = $object->ser_id;
            $response = $instance;
        }
        return $response;
    }

    ################################################################################################# END - RECAST METHODS
    ################################################################################################# BEGIN - GETTERS

    public function getDays()
    {
        return $this->_days;
    }

    public function getPrice()
    {
        return $this->_price;
    }

    ################################################################################################# END - GETTERS

    ################################################################################################# BEGIN - SETTERS

    public function setStatus($status)
    {
        $this->_status = $status;
    }

    ################################################################################################# END - SETTERS



    /************************************************************************************** BEGIN - DATATABLE AJAX METHODS */
    /**
     * @return mixed
     */
    public static function countAll()
    {
        $ci = &get_instance();
        $ci->load->database();

        $sql = '
                select count(' . static::TABLE_ID. ') as total
                from ' . static::TABLE_NAME.'
                left join publicacion on pub_id = pag_pub_id
                left join usuario on usu_id = pub_usu_id
                left join inmueble on inm_id = pub_inm_id';

        $query = $ci->db->query($sql);
        $totalCount = $query->row()->total;
        return $totalCount;
    }

    /**
     * @param $limit
     * @param $offset
     * @param null $orderBy
     * @param string $orderType
     * @return mixed
     */
    public static function getAll($limit, $offset, $orderBy = null, $orderType = 'asc')
    {
        if ($orderBy === null)
        {
            $orderBy = static::TABLE_ID;
        }
        $ci = &get_instance();
        $ci->load->database();

        $sql = 'select '.static::_dataTableColumns().', CONCAT(usu_nombre," ",usu_apellido) as user_full_name from ' . static::TABLE_NAME . '
                left join publicacion on pub_id = pag_pub_id
                left join usuario on usu_id = pub_usu_id
                left join inmueble on inm_id = pub_inm_id
                order by ' . $orderBy . ' ' . $orderType . ' limit ' . $limit . ' offset ' . $offset;

        $query = $ci->db->query($sql);
        $result = $query->result();
        return $result;
    }

    public static function search($text, $limit, $offset, $orderBy = null, $orderType = 'asc', $colsArray = null, $additionalParameters = array())
    {
        if ($orderBy === null)
        {
            $orderBy = static::TABLE_ID;
        }
        $ci = &get_instance();
        $ci->load->database();

        $sql = 'select '.static::_dataTableColumns().', CONCAT(usu_nombre," ",usu_apellido) as user_full_name from ' . static::TABLE_NAME.'
                left join publicacion on pub_id = pag_pub_id
                left join usuario on usu_id = pub_usu_id
                left join inmueble on inm_id = pub_inm_id';
        $sql .= ' where (';
        foreach ($colsArray as $var)
        {
            if($var == "user_full_name")
            {
                $var = ' CONCAT(usu_nombre," ",usu_apellido) ';
            }
            $sql .= ' ' . $var . ' like \'%' . $text . '%\' or ';
        }

        $sql = substr($sql, 0, -3);
        $sql .= ') order by ' . $orderBy . ' ' . $orderType . ' limit ' . $limit . ' offset ' . $offset;

        $query = $ci->db->query($sql);
        return $query->result();
    }

    public static function searchTotalCount($text, $colsArray = null, $additionalParameters = array())
    {
        $ci = &get_instance();
        $ci->load->database();

        $sql = 'select count(' . static::TABLE_ID . ') as total from ' . static::TABLE_NAME.'
        left join publicacion on pub_id = pag_pub_id
        left join usuario on usu_id = pub_usu_id
        left join inmueble on inm_id = pub_inm_id';
        $sql .= ' where (';

        foreach ($colsArray as $var)
        {
            if($var == "user_full_name")
            {
                $var = ' CONCAT(usu_nombre," ",usu_apellido) ';
            }
            $sql .= ' ' . $var . ' like \'%' . $text . '%\' or ';
        }

        $sql = substr($sql, 0, -3);
        $sql .= ')';

        $query = $ci->db->query($sql);
        $totalCount = $query->row()->total;
        return $totalCount;
    }

    private static function _dataTableColumns()
    {
        $columns = " pag_id, pag_cod_transaccion, inm_nombre, pag_fecha_pagado, usu_email, usu_tipo, pag_monto, pag_entidad, pag_concepto, pag_estado ";
        return $columns;
    }
    /*********************************************************************************** END - DATATABLE AJAX METHODS */

    /**
     * @param array $serviceArray
     * @return array
     */
    public static function getByArrayServiceType($serviceArray = array())
    {
        $ci = &get_instance();
        $ci->load->database();

        $serviceList = "";
        foreach ($serviceArray as $service)
        {
            $serviceList .= $ci->db->escape($service);
            if ($service !== end($serviceArray))
            {
                $serviceList .= ", ";
            }
        }

        $serviceTypeFilter = "";
        if(count($serviceArray) >= 1)
        {
            $serviceTypeFilter = " and ser_tipo in(".$serviceList.") ";
        }

        $sql = "
            select ".static::TABLE_NAME.".* from ".static::TABLE_NAME." where ser_estado = 'Activo' ".$serviceTypeFilter." order by ser_tipo
        ";

        $query = $ci->db->query($sql);
        $result = static::recastArray(get_called_class(), $query->result());
        return $result;
    }

    public static function getTotalPaymentServiceByArrayIds($arrayIds = array())
    {
        $serviceList = model_service::getAllInIdsArray($arrayIds, 100, 0);
        $totalAmount = 0;

        foreach ($serviceList as $service)
        {
            $totalAmount += $service->getPrice();
        }

        return $totalAmount;
    }
}