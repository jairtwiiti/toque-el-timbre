<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_transactiontype extends MY_Model {

    const TABLE_ID = "for_id";
    const TABLE_NAME = "forma";

    protected $_type;
    protected $_order;
    //Due to autoload is necessary to set the default data as empty string
    function __construct($type = "", $order = "")
    {
        parent::__construct();
        $this->_type = $type;
        $this->_order = $order;
    }

    /**
     * Parse the current object to key => value array
     * @return array
     */
    public function toArray()
    {
        $tableAttributes = array(
                                "for_id" => $this->_id,
                                "for_descripcion" => $this->_type
                                );
        return $tableAttributes;
    }

    /**
     * @param $className
     * @param $object
     * @return object
     */
    protected static function recast($className, $object)
    {
        $response =  null;
        if ($object instanceof stdClass)
        {
            if (!class_exists($className))
                throw new InvalidArgumentException(sprintf('Inexistant class %s.', $className));

            //Let's set the values to payment object using the data from stdObject
            $instance = new $className(
                    $object->for_descripcion
            );
            $instance->_id = $object->for_id;
            $response = $instance;
        }
        return $response;
    }

    ################################################################################################# BEGIN - GETTERS

    public function getType()
    {
        return $this->_type;
    }

    public static function getAll($limit = 1000, $offset = 0, $orderBy = null, $orderType = 'asc')
    {
        $ci=&get_instance();
        $ci->load->database();
        $sql = "select * from ".static::TABLE_NAME;
        $query = $ci->db->query($sql);
        $response = static::recastArray(get_called_class(),$query->result());
        return $response;
    }
    ################################################################################################# END - GETTERS

    ################################################################################################# BEGIN - SETTERS

    ################################################################################################# END - SETTERS
    public static function search($text, $limit, $offset, $orderBy = null, $orderType = 'asc', $colsArray = null)
    {
        if ($orderBy === null)
        {
            $orderBy = static::TABLE_ID;
        }
        $ci = &get_instance();
        $ci->load->database();

        $sql = 'select * from ' . static::TABLE_NAME;
        $sql .= ' where (';
        foreach ($colsArray as $var)
        {
            $sql .= ' ' . $var . ' like \'%' . $text . '%\' or ';
        }

        $sql = substr($sql, 0, -3);
        $sql .= ') order by ' . $orderBy . ' ' . $orderType . ' limit ' . $limit . ' offset ' . $offset;
        $query = $ci->db->query($sql);
        return static::recastArray(get_called_class(), $query->result());
    }
}