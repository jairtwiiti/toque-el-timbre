<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_message extends MY_Model {

    const TABLE_ID = "men_id";
    const TABLE_NAME = "mensaje";

    protected $_name;
    protected $_businessName;
    protected $_phone;
    protected $_email;
    protected $_description;
    protected $_read;
    protected $_date;
    protected $_propertyId;
    protected $_cityId;
    protected $_status;
    protected $_userSender;
    protected $_userReceiver;

    //Due to autoload is necessary to set the default data as empty string
    function __construct($name = "", $businessName = "", $phone = "", $email = "", $description = "", $read = "", $date = "", $propertyId = "", $cityId = "", $status = "Visible", $userSender = NULL, $userReceiver = NULL)
    {
        parent::__construct();
        $this->_name = $name;
        $this->_businessName = $businessName;
        $this->_phone = $phone;
        $this->_email = $email;
        $this->_description = $description;
        $this->_read = $read;
        $this->_date = $date;
        $this->_propertyId = $propertyId;
        $this->_cityId = $cityId;
        $this->_status = $status;
        $this->_userSender = $userSender;
        $this->_userReceiver = $userReceiver;
    }

    /**
     * Parse the current object to key => value array
     * @return array
     */
    public function toArray()
    {
        $tableAttributes = array(
                                "men_id" => $this->_id,
                                "men_nombre" => $this->_name,
                                "men_empresa" => $this->_businessName,
                                "men_telefono" => $this->_phone,
                                "men_email" => $this->_email,
                                "men_descripcion" => $this->_description,
                                "men_leido" => $this->_read,
                                "men_fech" => $this->_date,
                                "men_inm_id" => $this->_propertyId,
                                "men_ciudad" => $this->_cityId,
                                "men_estado" => $this->_status,
                                "men_user_sender" => $this->_userSender,
                                "men_user_receiver" => $this->_userReceiver
                                );
        return $tableAttributes;
    }

    /**
     * @param $className
     * @param $object
     * @return object
     */
    protected static function recast($className, $object)
    {
        $response =  null;
        if ($object instanceof stdClass)
        {
            if (!class_exists($className))
                throw new InvalidArgumentException(sprintf('Inexistant class %s.', $className));

            //Let's set the values to payment object using the data from stdObject
            $instance = new $className(
                    $object->men_nombre,
                    $object->men_empresa,
                    $object->men_telefono,
                    $object->men_email,
                    $object->men_descripcion,
                    $object->men_leido,
                    $object->men_fech,
                    $object->men_inm_id,
                    $object->men_ciudad,
                    $object->men_estado,
                    $object->men_user_sender,
                    $object->men_user_receiver
            );
            $instance->_id = $object->men_id;
            $response = $instance;
        }
        return $response;
    }

    ################################################################################################# BEGIN - GETTERS

    public function getName()
    {
        return $this->_name;
    }

    public function getPhone()
    {
        return $this->_phone;
    }

    public function getEmail()
    {
        return $this->_email;
    }
    ################################################################################################# END - GETTERS



    ################################################################################################# BEGIN - SETTERS

    ################################################################################################# END - SETTERS
    public function propertyContactResponse($subject, $replyMessage)
    {
        $ci=&get_instance();
        $ci->load->database();
        require_once(BASE_DIRECTORY . DS . 'admin' . DS . 'clases' . DS. 'phpmailer' . DS . 'class.phpmailer.php');
        $parametros = $this->model_parametros->get_all();
        $user = model_user::getByMessageId($this->_id);
        $property = model_property::getByMessageId($this->_id);
        $response = FALSE;
        if($user instanceof model_user && $property instanceof model_property && $this->_email!= "")
        {
            $cue_nombre = $parametros->par_salida;
            $cue_pass = $parametros->par_pas_salida;
            $user = $user->toArray();

            $data["message"] = $replyMessage;
            $data["property"] = $property->toArray();

            $mail = new PHPMailer(true);
            $mail->SetLanguage('es');
            $mail->IsSMTP();
            $mail->SMTPAuth = true;
            $mail->Host = $parametros->par_smtp;
            $mail->Timeout = 30;
            $mail->CharSet = 'utf-8';
            $mail->Subject = $subject;
            $body = $ci->load->view("email_template/mensaje_contacto_respuesta",$data,TRUE);
            $mail->MsgHTML($body);
            $mail->AddAddress($this->_email);
            $mail->AddReplyTo($user["usu_email"], $user["usu_nombre"]);
            $mail->SetFrom($cue_nombre, model_parametros::SITE_DOMAIN);
            $mail->Username = $cue_nombre;
            $mail->Password = $cue_pass;
            $response = $mail->Send();
        }
        return $response;
    }
    ################################################################################################# BEGIN - DATATABLE AJAX METHODS
    /**
     * @return mixed
     */
    public static function countAll()
    {
        $ci = &get_instance();
        $ci->load->database();

        $sql = '
                select sum(messages.total) total from (
                    select count(*) total from mensaje
                    LEFT JOIN inmueble on men_inm_id = inm_id
                    LEFT JOIN categoria on inm_cat_id = cat_id
                    LEFT JOIN forma on inm_for_id = for_id
                    LEFT JOIN ciudad on inm_ciu_id = ciu_id
                    LEFT JOIN departamento on ciu_dep_id = dep_id
                    union
                    select count(*) total from mensaje_proyecto
                    LEFT JOIN proyecto on men_proy_id = proy_id                    
                    union
                    select count(*) total from message_real_state
                    LEFT JOIN usuario on men_usu_id = usu_id
                ) messages
            ';

        $query = $ci->db->query($sql);
        $totalCount = $query->row()->total;
        return $totalCount;
    }

    /**
     * @param $limit
     * @param $offset
     * @param null $orderBy
     * @param string $orderType
     * @param string $additionalParameters
     * @return mixed
     */
    public static function getAll($limit, $offset, $orderBy = null, $orderType = 'asc')
    {
        if ($orderBy === null)
        {
            $orderBy = static::TABLE_ID;
        }
        $ci = &get_instance();
        $ci->load->database();

        $sql = 'select '.self::_dataTableColumns().', "0" messageType, cat_nombre, for_descripcion, men_ciudad, dep_nombre, "" usu_id, inm_seo seo,  inm_nombre addresses, usu_tipo from ' . static::TABLE_NAME.'
                left join inmueble on men_inm_id = inm_id                
                LEFT JOIN categoria on inm_cat_id = cat_id
                LEFT JOIN forma on inm_for_id = for_id
                LEFT JOIN ciudad on inm_ciu_id = ciu_id
                LEFT JOIN departamento on ciu_dep_id = dep_id
                left join publicacion on pub_inm_id = inm_id
                left join usuario on pub_usu_id = usu_id
                ' .
            ' 
                ';
        $sql .= ' union ';
        $sql .= 'select '.self::_dataTableColumns().',"1" messageType, "" cat_nombre, "" for_descripcion, men_ciudad, "" dep_nombre, "" usu_id, proy_seo seo, proy_nombre addresses, usu_tipo from mensaje_proyecto
                left join proyecto on men_proy_id = proy_id
                left join usuario on proy_usu_id = usu_id 
                ';
        $sql .= ' union ';
        $sql .= 'select '.self::_dataTableColumns().',"2" messageType, "" cat_nombre, "" for_descripcion, men_ciudad, dep_nombre, usu_id, "" seo, case when usu_empresa = "" or usu_empresa is null then usu_nombre else usu_empresa end addresses, usu_tipo from message_real_state
                left join usuario on men_usu_id = usu_id
                LEFT JOIN ciudad on usu_ciu_id = ciu_id
                LEFT JOIN departamento on ciu_dep_id = dep_id
                ';
        $sql .= 'group by '.static::TABLE_ID.' order by ' . $orderBy . ' ' . $orderType . ' limit ' . $limit . ' offset ' . $offset;
        $query = $ci->db->query($sql);
        $result = $query->result();
        return $result;
    }

    public static function search($text, $limit, $offset, $orderBy = null, $orderType = 'asc', $colsArray = null, $additionalParameters)
    {
        if ($orderBy === null)
        {
            $orderBy = self::TABLE_ID;
        }
        $ci = &get_instance();
        $ci->load->database();

        $where = '';
        foreach ($colsArray as $var)
        {
            if($var == "addresses")
            {
                $where .= ' replace_addresses like \'%' . $text . '%\' or ';
            }
            else
            {
                $where .= ' ' . $var . ' like \'%' . $text . '%\' or ';
            }

        }

        $where = substr($where, 0, -3);

        $sql = '
                select * from(
                select '.self::_dataTableColumns().',"0" messageType, cat_nombre, for_descripcion, dep_nombre, "" usu_id, inm_seo seo, inm_nombre addresses from ' . self::TABLE_NAME.'
                left join inmueble on inm_id = men_inm_id
                LEFT JOIN categoria on inm_cat_id = cat_id
                LEFT JOIN forma on inm_for_id = for_id
                LEFT JOIN ciudad on inm_ciu_id = ciu_id
                LEFT JOIN departamento on ciu_dep_id = dep_id
                LEFT JOIN publicacion on pub_inm_id = inm_id
                left join usuario on pub_usu_id = usu_id
                where ('.str_replace("replace_addresses","inm_nombre",$where).') '.static::_additionalParameters($additionalParameters).'  
                ';
        $sql .= ' union ';
        $sql .= 'select '.self::_dataTableColumns().',"1" messageType, "" cat_nombre, "" for_descripcion, "" dep_nombre, "" usu_id, proy_seo seo, proy_nombre addresses from mensaje_proyecto 
                left join proyecto on proy_id = men_proy_id
                left join usuario on proy_usu_id = usu_id
                where ('.str_replace("replace_addresses","proy_nombre",$where).') '.static::_additionalParameters($additionalParameters).'
                ';
        $sql .= ' union ';
        $sql .= 'select '.self::_dataTableColumns().',"2" messageType, "" cat_nombre, "" for_descripcion, dep_nombre, usu_id, "" seo, case when usu_empresa = "" or usu_empresa is null then usu_nombre else usu_empresa end addresses from message_real_state
                left join usuario on men_usu_id = usu_id
                LEFT JOIN ciudad on usu_ciu_id = ciu_id
                LEFT JOIN departamento on ciu_dep_id = dep_id
                where ('.str_replace("replace_addresses","usu_empresa",$where).') '.static::_additionalParameters($additionalParameters).' 
                ) as searchQuery';
        $sql .= ' group by '.static::TABLE_ID.' order by ' . $orderBy . ' ' . $orderType . ' limit ' . $limit . ' offset ' . $offset;

//        var_dump($sql);exit;
        $query = $ci->db->query($sql);
        return $query->result();
    }

    public static function searchTotalCount($text, $colsArray = null, $additionalParameters)
    {
        $ci = &get_instance();
        $ci->load->database();
        $where = '';
        foreach ($colsArray as $var)
        {
            if($var == "addresses")
            {
                $where .= ' replace_addresses like \'%' . $text . '%\' or ';
            }
            else
            {
                $where .= ' ' . $var . ' like \'%' . $text . '%\' or ';
            }
        }

        $where = substr($where, 0, -3);
        $sql = '
                select IFNULL(sum(messages.total),0) total, '.self::_dataTableColumns().', messageType from (
                    select count(men_id) total, '.self::_dataTableColumns().', "0" messageType from mensaje
                    LEFT JOIN inmueble on men_inm_id = inm_id 
                    left join publicacion on pub_inm_id = inm_id
                    left join usuario on pub_usu_id = usu_Id
                    where ('.str_replace("replace_addresses","inm_nombre",$where).') and men_fech != "" '.static::_additionalParameters($additionalParameters).'
                    union
                    select count(men_id) total, '.self::_dataTableColumns().', "1" messageType from mensaje_proyecto
                    LEFT JOIN proyecto on men_proy_id = proy_id
                    left join usuario on proy_usu_id = usu_Id
                    where ('.str_replace("replace_addresses","proy_nombre",$where).') and men_fech != "" '.static::_additionalParameters($additionalParameters).'
                    union
                    select count(men_id) total, '.self::_dataTableColumns().', "2" messageType from message_real_state
                    LEFT JOIN usuario on men_usu_id = usu_id
                    where ('.str_replace("replace_addresses","usu_empresa",$where).') and men_fech != "" '.static::_additionalParameters($additionalParameters).'
                ) messages
            ';
//        $sql .= ' where ('.$where.') ';
//        var_dump($sql);exit;
        $query = $ci->db->query($sql);
        $totalCount = $query->row()->total;
        return $totalCount;
    }

    private static function _dataTableColumns()
    {
        $columns = "
                    men_id,
                    men_nombre,
                    men_empresa,
                    men_telefono,
                    men_email,
                    men_descripcion,
                    men_leido,
                    men_fech,                    
                    men_ciudad,
                    men_estado,
                    usu_tipo
                    ";
        return $columns;
    }
    private static function _additionalParameters($list = array())
    {
        $ci=&get_instance();
        $ci->load->database();
        $sql = "";
        $havingFilter = "";
        if(is_array($list) && count($list) >= 1)
        {
            foreach($list as $parameter => $value)
            {
                switch ($parameter)
                {

                    case "start-date":
                        $value = date( "Y-m-d", strtotime($value));
                        $sql .= " and men_fech >= ".$ci->db->escape($value);
                        break;
                    case "end-date":
                        $value = date( "Y-m-d", strtotime($value));
                        $sql .= " and men_fech <= ".$ci->db->escape($value);
                        break;
                    case "addresses":
                        if($value != 3)
                        {
                            $havingFilter = " HAVING messageType = ".$ci->db->escape($value);
                        }
                        break;
                }
            }
        }
        return $sql.$havingFilter;
    }
    ################################################################################################# END - DATATABLE AJAX METHODS

    public static function createMessage(model_user $userSender, $userReceiverId)
    {
        $ci=&get_instance();
        $formData = $ci->input->post();
        $company = !is_null($userSender->getCompany())?$userSender->getCompany():"";
        $phone = !is_null($userSender->getPhone())?$userSender->getPhone():"";
        $email = !is_null($userSender->getEmail())?$userSender->getEmail():"";
        $description = isset($formData['mensaje'])?$formData['mensaje']:$formData['replyMessage'];
        $propertyId = isset($formData['id_inmueble'])?$formData['id_inmueble']:$formData['propertyId'];
        $message = new model_message(
            $userSender->getFullName(),
            $company,
            $phone,
            $email,
            $description,
            'No',
            date('Y-m-d'),
            $propertyId,
            NULL,
            "Visible",
            $userSender->getId(),
            $userReceiverId
        );

        $message->save();
    }
}