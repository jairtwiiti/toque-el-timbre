<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_departamento extends CI_Model {

    const TABLE_ID = "dep_id";
    const TABLE_NAME = "departamento";

    private $table = 'departamento';
    private $id = 'dep_id';

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    /**
     * Get a object
     *
     * @param int $departmentId
     * @return object
     */

    public static function getById($departmentId)
    {
        $ci=&get_instance();
        $ci->load->database();
        $sql = "
            select * from ".self::TABLE_NAME." where ".self::TABLE_ID." = ".$ci->db->escape($departmentId)."
        ";
        $query = $ci->db->query($sql);
        $response = $query->row(0,'model_departamento');
        return $response;
    }

    /**
     * Gets all results for this model but get a one result by id too.
     *
     * @param bool $id
     * @return []
     */
    function get_all($id = FALSE) {
        if ($id === FALSE) {
            $query = $this->db->get($this->table);
            return $query->result_array();
        }

        $query = $this->db->get_where($this->table, array($this->id => $id));
        return $query->row_array();
    }

    /**
     * Add new Model
     *
     * @param $data
     * @return mixed
     */
    function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    /**
     * Update Model
     *
     * @param $id
     * @param $data
     * @return mixed
     */
    function update($id, $data) {
        $this->db->where($this->id, $id);
        return $this->db->update($this->table, $data);
    }

    /**
     * Delete Model
     *
     * @param $id
     * @return mixed
     */
    function delete($id) {
        return $this->db->update($this->table, array($this->id => $id));
    }

    /**
     * Get cities
     *
     * @return []
     */
    function get_states(){
        $sql = "
            SELECT dep.`dep_id`, dep.`dep_nombre`, COUNT(inm.inm_id) as cantidad FROM ciudad ciu
            INNER JOIN inmueble inm ON(inm.`inm_ciu_id` = ciu.`ciu_id` AND inm.`inm_publicado` = 'Si')
            INNER JOIN publicacion pub ON (pub.`pub_inm_id` = inm.`inm_id` AND pub.`pub_estado` = 'Aprobado' AND CURDATE() <= pub.`pub_vig_fin`)
            INNER JOIN departamento dep ON (dep.`dep_id` = ciu.`ciu_dep_id`)
            GROUP BY dep.`dep_id`
        ";
        return $this->db->query($sql)->result();
    }

    function get_all_states() {
        $query = $this->db->get($this->table);
        return $query->result();
    }
}