<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_paymentservice extends MY_Model {

    const TABLE_ID = "pas_id";
    const TABLE_NAME = "pago_servicio";

    protected $_paymentId;
    protected $_serviceId;
    protected $_startDate;
    protected $_finishDate;

    //Due to autoload is necessary to set the default data as empty string
    function __construct($paymentId = "", $serviceId = "", $startDate = "", $finishDate = "")
    {
        parent::__construct();
        $this->_paymentId = $paymentId;
        $this->_serviceId = $serviceId;
        $this->_startDate = $startDate;
        $this->_finishDate = $finishDate;
    }

    public function toArray()
    {
        $tableAttributes = array(
                                "pas_id" => $this->_id,
                                "pag_id" => $this->_paymentId,
                                "ser_id" => $this->_serviceId,
                                "fech_ini" => $this->_startDate,
                                "fech_fin" => $this->_finishDate
                                );
        return $tableAttributes;
    }

    /**
     * @param $className
     * @param $object
     * @return null
     */
    protected static function recast($className, $object)
    {
        $response =  null;
        if ($object instanceof stdClass)
        {
            if (!class_exists($className))
                throw new InvalidArgumentException(sprintf('Inexistant class %s.', $className));

            //Let's set the values to payment object using the data from stdObject
            $instance = new $className(
                $object->pag_id,
                $object->ser_id,
                $object->fech_ini,
                $object->fech_fin
            );
            $instance->_id = $object->pas_id;
            $response = $instance;
        }
        return $response;
    }
    ################################################################################################# BEGIN - GETTERS

    /**
     * get the date when finish the payment service
     * @return string
     */
    public function getFinishDate()
    {
        return $this->_finishDate;
    }

    /**
     * Get the last payment service object that still is valid taking account the publicationId, serviceId and not the paymentId passed
     * @param $publication
     * @param $serviceId
     * @param $paymentId
     * @return null
     */
    public static function getLastActiveByServiceIdPublicationIdAndNotPaymentId($publication, $serviceId, $paymentId)
    {
        $ci=&get_instance();
        $ci->load->database();

        $sql = "
        SELECT
            ps.*
        FROM
            pagos p
        INNER JOIN pago_servicio ps ON (ps.pag_id = p.pag_id AND ps.ser_id = ".$ci->db->escape($serviceId)."	AND (/*CURDATE() >= ps.`fech_ini` AND*/ CURDATE() <= ps.`fech_fin`))
        WHERE
            p.pag_pub_id = ".$ci->db->escape($publication)."
        AND p.`pag_id` <> ".$ci->db->escape($paymentId)."
        AND p.`pag_entidad` <> 'Suscripcion'
        ORDER BY
            p.pag_fecha_pagado DESC,
            p.pag_fecha DESC
            LIMIT 1
        ";
//        echo"<pre>";var_dump($sql);exit;//debug line
        $query = $ci->db->query($sql);
        $response = static::recast(get_called_class(),$query->row());
        return $response;
    }

    /**
     * @param $paymentId
     * @return array
     */
    public static function getByPaymentId($paymentId)
    {
        $ci=&get_instance();
        $ci->load->database();
        $sql = "
            select * from ".static::TABLE_NAME." where pag_id = ".$ci->db->escape($paymentId)."
        ";
        $query = $ci->db->query($sql);
        $response = static::recastArray(get_called_class(),$query->result());
        return $response;
    }

    /**
     * @param $paymentId
     * @return array
     */
    public static function getMasterDetailByPaymentId($paymentId)
    {
        $ci=&get_instance();
        $ci->load->database();
        $sql = "
            SELECT
                ps.pag_id payment_id,
                ps.ser_id service_id,
                s.ser_descripcion service_description,
                s.ser_precio service_price
            FROM
                pago_servicio ps 
            left join servicios s on ps.ser_id = s.ser_id
            WHERE
                ps.pag_id = ".$ci->db->escape($paymentId)."    
        ";
        $query = $ci->db->query($sql);
        return $query->result_array();
    }


    /**
     * Get the payment service object that has the most long expirationDate
     * @param $pub_id
     * @return null
     */
    public static function getByMostLongExpirationDateAndPublicationId($pub_id)
    {
        $ci=&get_instance();
        $ci->load->database();
        $sql = "
        SELECT ps.* FROM pagos p
        INNER JOIN pago_servicio ps ON (ps.`pag_id` = p.`pag_id` AND CURDATE() <= ps.`fech_fin`)
        WHERE p.`pag_pub_id` = '$pub_id' AND p.`pag_estado` = 'Pagado' ORDER BY ps.fech_fin DESC limit 1
        ";
        $query = $ci->db->query($sql);
        $response = static::recast(get_called_class(),$query->row());
        return $response;
    }

    ################################################################################################# END - GETTERS

    ################################################################################################# BEGIN - SETTERS

    public function setStartDate($startDate)
    {
        $this->_startDate = $startDate;
    }

    public function setSetFinishDate($finishDate)
    {
        $this->_finishDate = $finishDate;
    }

    ################################################################################################# END - SETTERS
    /**
     * activate the service taking into account the finish date from already existing active service
     * @param $publicationId
     */
    public function activateService($publicationId)
    {
        /** @var model_service $service */
        $this->_startDate = date("Y-m-d");
        $lastActivePaymentService = static::getLastActiveByServiceIdPublicationIdAndNotPaymentId($publicationId, $this->_serviceId, $this->_paymentId);
        $service = model_service::getById($this->_serviceId);
//        echo"<pre>";var_dump($publicationId, $this->_id, $this->_paymentId, $lastActivePaymentService, $lastActivePaymentService instanceof model_paymentservice, $lastActivePaymentService->_finishDate);exit;
        if($lastActivePaymentService instanceof model_paymentservice)
        {
            $this->_startDate = date("Y-m-d", strtotime($lastActivePaymentService->_finishDate. " + 1 days"));
        }
        $this->_finishDate = date("Y-m-d", strtotime($this->_startDate. " + " . $service->getDays() . " days"));
        $this->save();
    }

    /**
     * Remove the start and finish date from service
     */
    public function deactivateService()
    {
        $this->_startDate = $this->_finishDate = "";
        $this->save();
    }

    public function __toString()
    {
        $jsonEncode = json_encode($this->toArray());
        return $jsonEncode;
    }

    /************************************************************************************** BEGIN - DATATABLE AJAX METHODS */
    /**
     * @return mixed
     */
    public static function countAll()
    {
        $ci = &get_instance();
        $ci->load->database();

        $sql = '
                select count(' . static::TABLE_ID. ') as total
                from ' . static::TABLE_NAME.'
                left join publicacion on pub_id = pag_pub_id
                left join usuario on usu_id = pub_usu_id
                left join inmueble on inm_id = pub_inm_id';

        $query = $ci->db->query($sql);
        $totalCount = $query->row()->total;
        return $totalCount;
    }

    /**
     * @param $limit
     * @param $offset
     * @param null $orderBy
     * @param string $orderType
     * @return mixed
     */
    public static function getAll($limit, $offset, $orderBy = null, $orderType = 'asc')
    {
        if ($orderBy === null)
        {
            $orderBy = static::TABLE_ID;
        }
        $ci = &get_instance();
        $ci->load->database();

        $sql = 'select '.static::_dataTableColumns().', CONCAT(usu_nombre," ",usu_apellido) as user_full_name from ' . static::TABLE_NAME . '
                left join publicacion on pub_id = pag_pub_id
                left join usuario on usu_id = pub_usu_id
                left join inmueble on inm_id = pub_inm_id
                order by ' . $orderBy . ' ' . $orderType . ' limit ' . $limit . ' offset ' . $offset;

        $query = $ci->db->query($sql);
        $result = $query->result();
        return $result;
    }

    public static function search($text, $limit, $offset, $orderBy = null, $orderType = 'asc', $colsArray = null)
    {
        if ($orderBy === null)
        {
            $orderBy = static::TABLE_ID;
        }
        $ci = &get_instance();
        $ci->load->database();

        $sql = 'select '.static::_dataTableColumns().', CONCAT(usu_nombre," ",usu_apellido) as user_full_name from ' . static::TABLE_NAME.'
                left join publicacion on pub_id = pag_pub_id
                left join usuario on usu_id = pub_usu_id
                left join inmueble on inm_id = pub_inm_id';
        $sql .= ' where (';
        foreach ($colsArray as $var)
        {
            if($var == "user_full_name")
            {
                $var = ' CONCAT(usu_nombre," ",usu_apellido) ';
            }
            $sql .= ' ' . $var . ' like \'%' . $text . '%\' or ';
        }

        $sql = substr($sql, 0, -3);
        $sql .= ') order by ' . $orderBy . ' ' . $orderType . ' limit ' . $limit . ' offset ' . $offset;

        $query = $ci->db->query($sql);
        return $query->result();
    }

    public static function searchTotalCount($text, $colsArray = null)
    {
        $ci = &get_instance();
        $ci->load->database();

        $sql = 'select count(' . static::TABLE_ID . ') as total from ' . static::TABLE_NAME.'
        left join publicacion on pub_id = pag_pub_id
        left join usuario on usu_id = pub_usu_id
        left join inmueble on inm_id = pub_inm_id';
        $sql .= ' where (';

        foreach ($colsArray as $var)
        {
            if($var == "user_full_name")
            {
                $var = ' CONCAT(usu_nombre," ",usu_apellido) ';
            }
            $sql .= ' ' . $var . ' like \'%' . $text . '%\' or ';
        }

        $sql = substr($sql, 0, -3);
        $sql .= ')';

        $query = $ci->db->query($sql);
        $totalCount = $query->row()->total;
        return $totalCount;
    }

    private static function _dataTableColumns()
    {
        $columns = " pag_id, pag_cod_transaccion, inm_nombre, pag_fecha_pagado, usu_email, usu_tipo, pag_monto, pag_entidad, pag_concepto, pag_estado ";
        return $columns;
    }
    /*********************************************************************************** END - DATATABLE AJAX METHODS */
}