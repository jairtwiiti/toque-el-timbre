<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_pagos extends CI_Model {

    const TABLE_ID = "pag_id";
    const TABLE_NAME = "pagos";

    //Payment Methods
    const OFFICE = "Oficina";
    const BANK = "Banco";
    const COURTESY = "Toqueeltimbre";

    private $table = 'pagos';
    private $id = 'pag_id';

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    /**
     * Gets all results for this model but get a one result by id too.
     *
     * @param bool $id
     * @return []
     */
    function get_all($id = FALSE) {
        if ($id === FALSE) {
            $query = $this->db->get($this->table);
            return $query->result_array();
        }

        $query = $this->db->get_where($this->table, array($this->id => $id));
        return $query->row_array();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->sur_id;
    }
    /**
     * Add new Model
     *
     * @param $data
     * @return mixed
     */
    function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    /**
     * Update Model
     *
     * @param $id
     * @param $data
     * @return mixed
     */
    function update($id, $data) {
        $this->db->where($this->id, $id);
        return $this->db->update($this->table, $data);
    }

    /**
     * Delete Model
     *
     * @param $id
     * @return mixed
     */
    function delete($id) {
        return $this->db->update($this->table, array($this->id => $id));
    }

    function get_payment_by_user($user_id, $page, $limit, $status, $pub_id){
        $page = ($page - 1) >= 0 ?  (($page-1)*$limit) : 0;
        $sql_pub = !empty($pub_id) ? ' AND pag.`pag_pub_id` = '.$pub_id : '';

        if($status == "Pendiente"){
            $sql = "
            SELECT pag.*, inm.`inm_nombre` FROM publicacion pub
            INNER JOIN pagos pag ON (pag.`pag_pub_id` = pub.`pub_id` AND pag.`pag_estado` = 'Pendiente' $sql_pub)
            INNER JOIN inmueble inm ON (inm.`inm_id` = pub.`pub_inm_id`)
            WHERE pub.`pub_usu_id` = '$user_id' AND CURDATE() <= pag.`pag_fecha_ven`
            ORDER BY pag.pag_fecha DESC
            LIMIT $page, $limit
            ";
        }else{
            $sql = "
            SELECT pag.*, inm.`inm_nombre` FROM publicacion pub
            INNER JOIN pagos pag ON (pag.`pag_pub_id` = pub.`pub_id` AND pag.pag_monto > 0 AND pag.`pag_estado` = '$status' $sql_pub)
            INNER JOIN inmueble inm ON (inm.`inm_id` = pub.`pub_inm_id`)
            WHERE pub.`pub_usu_id` = '$user_id'
            ORDER BY pag.pag_fecha DESC
            LIMIT $page, $limit
            ";
        }

        $result = $this->db->query($sql)->result();
        return $result;
    }

    function get_num_payment_by_user($user_id, $status){
        if($status == "Pendiente"){
            $sql = "
            SELECT COUNT(pag.`pag_id`) AS cantidad FROM publicacion pub
            INNER JOIN pagos pag ON (pag.`pag_pub_id` = pub.`pub_id` AND pag.`pag_estado` = '$status')
            WHERE pub.`pub_usu_id` = '$user_id' AND CURDATE() <= pag.`pag_fecha_ven`
            ";
        }else{
            $sql = "
            SELECT COUNT(pag.`pag_id`) AS cantidad FROM publicacion pub
            INNER JOIN pagos pag ON (pag.`pag_pub_id` = pub.`pub_id` AND pag.pag_monto > 0 AND pag.`pag_estado` = '$status')
            WHERE pub.`pub_usu_id` = '$user_id'
            ";
        }
        $result = $this->db->query($sql)->row();
        return $result->cantidad;
    }

    function get_payment_detail($pag_id){
        $sql = "
        SELECT * FROM pagos pag
        WHERE pag.`pag_id` = '$pag_id'
        ";
        $row = $this->db->query($sql)->row();
        $values = array(
            'payment' => $row,
            'services' => $this->get_services_payment($row->pag_id),
            'inmueble' => $this->get_detail_inmueble($row->pag_id)
        );
        return $values;
    }

    function get_services_payment($pag_id){
        $sql = "
        SELECT ps.*, ser.`ser_descripcion`, ser.`ser_precio`, ser.`ser_precio_suscriptor` FROM pago_servicio ps
        INNER JOIN servicios ser ON (ser.`ser_id` = ps.`ser_id`)
        WHERE ps.`pag_id` = '$pag_id'
        ";
        return $this->db->query($sql)->result();
    }

    function get_detail_inmueble($pag_id){
        $sql = "
        SELECT inm.*, zon.`zon_nombre`, ciu.`ciu_nombre` FROM pagos pag
        INNER JOIN publicacion pub ON (pub.`pub_id` = pag.`pag_pub_id`)
        INNER JOIN inmueble inm ON (inm.`inm_id` = pub.`pub_inm_id`)
        INNER JOIN zona zon ON (zon.`zon_id` = inm.`inm_zon_id`)
        INNER JOIN ciudad ciu ON (ciu.`ciu_id` = inm.`inm_ciu_id`)
        WHERE pag.`pag_id` = '$pag_id'
        GROUP BY inm.`inm_id`
        ";
        return $this->db->query($sql)->row();
    }

	function exist($pago_id) {
		$sql = "
			SELECT pag_id
			FROM pagos
			WHERE pag_id = '$pago_id';
		";

		return $this->db->query($sql)->num_rows() > 0;
	}

	function update_transaccion_id($payment_id, $data) {
		$this->db->where($this->id, $payment_id);
		return $this->db->update($this->table, $data);;

		//mysql_query($sql);
	}

    function get_last_payment_by_publication($publicationId) {
        $sql = "
            SELECT *
            FROM pagos
            WHERE pagos.pag_pub_id = $publicationId
            ORDER BY pagos.pag_fecha DESC
        ";

        $result = $this->db->query($sql)->result();
        return $result[0];
    }

    function get_payment_pending_by_user($user_id, $pub_id){
        $sql = "
        SELECT COUNT(pag.`pag_id`) AS cantidad FROM publicacion pub
        INNER JOIN pagos pag ON (pag.`pag_pub_id` = pub.`pub_id` AND pag.`pag_estado` = 'Pendiente')
        WHERE pub.`pub_usu_id` = '$user_id' AND pag.`pag_pub_id` = '$pub_id' AND CURDATE() <= pag.`pag_fecha_ven`
        ";
        $obj = $this->db->query($sql)->row();
        return $obj->cantidad;
    }

    function get_payment_pending_subscription_by_user($user_id){
        $sql = "
        SELECT pag.pag_id, pag.`pag_pub_id` FROM publicacion pub
        INNER JOIN pagos pag ON (pag.`pag_pub_id` = pub.`pub_id` AND pag.`pag_estado` = 'Pendiente' AND pag.`pag_concepto` = 'Anuncio')
        WHERE pub.`pub_usu_id` = '$user_id' AND CURDATE() <= pag.`pag_fecha_ven`
        GROUP BY pag.`pag_id`
        ";
        return $this->db->query($sql)->result();
    }

    function desactivate_mejora_publication($pub_id){
        $value = array(
            'pag_estado' => 'Eliminado'
        );
        $this->db->where("pag_pub_id", $pub_id);
        $this->db->where("pag_estado", 'Pagado');
        $this->db->where("pag_concepto", 'Mejora');
        $this->db->update($this->table, $value);
    }

    function get_payment_detail_tigomoney($pag_id){
        $sql = "
        SELECT pag.*, u.`usu_id`, CONCAT(u.`usu_nombre`, ' ',u.`usu_apellido`) AS nombre_completo, u.`usu_ci` FROM pagos pag
        LEFT JOIN publicacion p ON (p.`pub_id` = pag.`pag_pub_id`)
        LEFT JOIN suscripcion s ON (s.`pago_id` = pag.`pag_id`)
        INNER JOIN usuario u ON (u.`usu_id` = p.`pub_usu_id` OR u.`usu_id` = s.`usuario_id`)
        WHERE pag.`pag_id` = '$pag_id'
        ";
        $row = $this->db->query($sql)->row();
        return $row;
    }

    function verify_id_paid($pag_id){
        $sql = "
        SELECT COUNT(p.`pag_id`) AS cantidad FROM pagos p
        WHERE p.`pag_id` = '$pag_id' AND p.`pag_estado` = 'Pagado'
        ";

        $row = $this->db->query($sql)->row();
        return $row->cantidad > 0 ? true : false;
    }

    function get_services($pag_id){
        $sql = "
		SELECT ser_id FROM pago_servicio
		WHERE pag_id = '".$pag_id."'
		";
        $result = $this->db->query($sql)->result();
        return $result;
    }

    function get_email_idpag($pag_id){
        $sql = "
		SELECT pag_pub_id FROM pagos
		WHERE pag_id =  '".$pag_id."'
		";
        $row = $this->db->query($sql)->row();
        if($row->pag_pub_id == 0){
            $sql = "
            SELECT `usu_email` FROM pagos
            INNER JOIN suscripcion s ON (s.`pago_id` = pag_id)
            INNER JOIN usuario ON usu_id = s.`usuario_id`
            WHERE pag_id =  '".$pag_id."'
            ";
        }else{
            $sql = "
            SELECT `usu_email` FROM pagos
            INNER JOIN publicacion ON pub_id = pag_pub_id
            INNER JOIN usuario ON usu_id = pub_usu_id
            WHERE pag_id =  '".$pag_id."'
            ";
        }
        $row2 = $this->db->query($sql)->row();
        return $row2->usu_email;
    }

    function get_id_publication($pag_id){
        $sql = "
		SELECT pag_pub_id FROM pagos
		WHERE pag_id = '".$pag_id."'
		LIMIT 1
		";
        $row = $this->db->query($sql)->row();
        return $row->pag_pub_id;
    }

    function verify_exist_other_paids($pag_id, $pub_id, $ser_id){
        $sql = "
        SELECT p.pag_entidad, ps.fech_ini, ps.fech_fin FROM pagos p
        INNER JOIN pago_servicio ps ON (ps.pag_id = p.pag_id AND ps.ser_id = ".$ser_id." AND (CURDATE() >= ps.`fech_ini` AND CURDATE() <= ps.`fech_fin`) )
        WHERE p.pag_pub_id = ".$pub_id." AND p.`pag_id` <> '$pag_id'
        ORDER BY p.pag_fecha_pagado DESC, p.pag_fecha DESC
        LIMIT 1
        ";
        $row = $this->db->query($sql)->row();
        return $row->fech_fin;
    }

    function get_publication($pag_id){
        $sql = "
        SELECT pub.`pub_id`, pub.`pub_vig_fin` FROM pagos p
        INNER JOIN publicacion pub ON (pub.`pub_id` = p.`pag_pub_id`)
        WHERE p.`pag_id` = '$pag_id'
        GROUP BY pub.`pub_id`
        ";
        return $this->db->query($sql)->row();
    }

    function get_max_date_service($pub_id){
        $sql = "
        SELECT MAX(ps.`fech_fin`) AS mayor_fecha FROM pagos p
        INNER JOIN pago_servicio ps ON (ps.`pag_id` = p.`pag_id` AND CURDATE() <= ps.`fech_fin`)
        WHERE p.`pag_pub_id` = '$pub_id' AND p.`pag_estado` = 'Pagado'
        ";
        $row = $this->db->query($sql)->row();
        return $row->mayor_fecha;
    }

    function get_id_inmueble($pub_id){
        $sql = "
		SELECT inm_id, inm_nombre FROM publicacion
		INNER JOIN inmueble on (inm_id = pub_inm_id)
		WHERE pub_id = '".$pub_id."'
		LIMIT 1
		";
        return $this->db->query($sql)->row();
    }

    function get_total_paid($pag_id){
        $sql = "
        SELECT p.pag_monto FROM pagos p
        WHERE p.`pag_id` = '$pag_id'
        ";
        $row = $this->db->query($sql)->row();
        return $row->pag_monto;
    }

    function get_id_paid($pag_id){
        $sql = "
        SELECT * FROM pagos p
        WHERE p.`pag_id` = '$pag_id'
        ";
        return $this->db->query($sql)->row();
    }

    function get_user_by_paid($pag_id){
        $sql = "
        SELECT u.`usu_id`, p.`pag_concepto` FROM pagos p
        INNER JOIN publicacion pu ON (pu.`pub_id` = p.`pag_pub_id`)
        INNER JOIN usuario u ON (u.`usu_id` = pu.`pub_usu_id`)
        WHERE p.`pag_id` = '$pag_id'
        ";
        $num = $this->db->query($sql)->num_rows();
        if($num > 0){
            return $this->db->query($sql)->row();
        }else{
            $sql = "
            SELECT u.`usu_id`, p.`pag_concepto` FROM pagos p
            INNER JOIN suscripcion s ON (s.`pago_id` = p.pag_id)
            INNER JOIN usuario u ON u.usu_id = s.`usuario_id`
            WHERE p.pag_id =  '".$pag_id."'
            ";
            return $this->db->query($sql)->row();
        }
    }

    public function __toString()
    {
        $jsonEncode = json_encode((array)$this);
        return $jsonEncode;
    }

    /************************************************************************************** BEGIN - DATATABLE AJAX METHODS */
    /**
     * @return mixed
     */
    public static function countAll()
    {
        $ci = &get_instance();
        $ci->load->database();

        $sql = '
                select count(' . self::TABLE_ID. ') as total
                from ' . self::TABLE_NAME.'
                left join publicacion on pub_id = pag_pub_id
                left join usuario on usu_id = pub_usu_id
                left join inmueble on inm_id = pub_inm_id';

        $query = $ci->db->query($sql);
        $totalCount = $query->row()->total;
        return $totalCount;
    }

    /**
     * @param $limit
     * @param $offset
     * @param null $orderBy
     * @param string $orderType
     * @return mixed
     */
    public static function getAll($limit, $offset, $orderBy = null, $orderType = 'asc')
    {
        if ($orderBy === null)
        {
            $orderBy = self::TABLE_ID;
        }
        $ci = &get_instance();
        $ci->load->database();

        $sql = 'select '.self::_dataTableColumns().', CONCAT(usu_nombre," ",usu_apellido) as user_full_name from ' . self::TABLE_NAME . '
                left join publicacion on pub_id = pag_pub_id
                left join usuario on usu_id = pub_usu_id
                left join inmueble on inm_id = pub_inm_id
                order by ' . $orderBy . ' ' . $orderType . ' limit ' . $limit . ' offset ' . $offset;

        $query = $ci->db->query($sql);
        $result = $query->result();
        return $result;
    }

    public static function search($text, $limit, $offset, $orderBy = null, $orderType = 'asc', $colsArray = null)
    {
        if ($orderBy === null)
        {
            $orderBy = self::TABLE_ID;
        }
        $ci = &get_instance();
        $ci->load->database();

        $sql = 'select '.self::_dataTableColumns().', CONCAT(usu_nombre," ",usu_apellido) as user_full_name from ' . self::TABLE_NAME.'
                left join publicacion on pub_id = pag_pub_id
                left join usuario on usu_id = pub_usu_id
                left join inmueble on inm_id = pub_inm_id';
        $sql .= ' where (';
        foreach ($colsArray as $var)
        {
            if($var == "user_full_name")
            {
                $var = ' CONCAT(usu_nombre," ",usu_apellido) ';
            }
            $sql .= ' ' . $var . ' like \'%' . $text . '%\' or ';
        }

        $sql = substr($sql, 0, -3);
        $sql .= ') order by ' . $orderBy . ' ' . $orderType . ' limit ' . $limit . ' offset ' . $offset;

        $query = $ci->db->query($sql);
        return $query->result("model_pagos");
    }

    public static function searchTotalCount($text, $colsArray = null)
    {
        $ci = &get_instance();
        $ci->load->database();

        $sql = 'select count(' . self::TABLE_ID . ') as total from ' . self::TABLE_NAME.'
        left join publicacion on pub_id = pag_pub_id
        left join usuario on usu_id = pub_usu_id
        left join inmueble on inm_id = pub_inm_id';
        $sql .= ' where (';

        foreach ($colsArray as $var)
        {
            if($var == "user_full_name")
            {
                $var = ' CONCAT(usu_nombre," ",usu_apellido) ';
            }
            $sql .= ' ' . $var . ' like \'%' . $text . '%\' or ';
        }

        $sql = substr($sql, 0, -3);
        $sql .= ')';

        $query = $ci->db->query($sql);
        $totalCount = $query->row()->total;
        return $totalCount;
    }

    private static function _dataTableColumns()
    {
        $columns = " pag_id, pag_cod_transaccion, inm_nombre, pag_fecha_pagado, usu_email, usu_tipo, pag_monto, pag_entidad, pag_concepto, pag_estado ";
        return $columns;
    }
    /*********************************************************************************** END - DATATABLE AJAX METHODS */

    public function save()
    {
        $ci=&get_instance();
        $ci->load->library("session");
        //static::validateTableDefinition();
        //$userData = $ci->session->all_userdata();
        //echo"<pre>";var_dump($userData);exit;
        //$loggedUserId = $this->session->userdata('id_user');
        date_default_timezone_set('America/La_Paz');
        $now = new DateTime();
        if ( $this->getId() !== null && $this->getId() !== "" )
        {
            $editedBy = "editedby" . self::COLUMN_SUFIX;
            $editedOn = "editedon" . self::COLUMN_SUFIX;
            $this->$editedBy = $loggedUserId;
            $this->$editedOn = $now->format( "Y-m-d H:i:s" );
            return $ci->db->update( self::TABLE_NAME, get_object_vars( $this ), array( self::TABLE_ID => $this->getId()));
        }
        else
        {
            //$createdBy = "createdby" . static::COLUMN_SUFIX;
            //$createdOn = "createdon" . static::COLUMN_SUFIX;
            //$this->$createdBy = $loggedUserId;
            //$this->$createdOn = $now->format( "Y-m-d H:i:s" );
            $result = $ci->db->insert( self::TABLE_NAME, get_object_vars( $this ) );
            if ( $result === true )
            {
                $this->sur_id = $ci->db->insert_id();
            }
            return $this->sur_id;
        }
    }

}