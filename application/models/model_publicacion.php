<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_publicacion extends CI_Model {

	const TABLE_ID = "pub_id";
	const TABLE_NAME = "publicacion";

	static $tableId = "pub_id";
	static $tableName = "publicacion";

	private $table = 'publicacion';
	private $id = 'pub_id';

	function __construct() {
		parent::__construct();
		$this->load->database();
	}
	/**
	 * Get a object
	 *
	 * @param int $publicationId
	 * @return object
	 */

	public static function getById($publicationId)
	{
		$ci=&get_instance();
		$ci->load->database();

		$sql = "
            select * from ".self::TABLE_NAME." where ".self::TABLE_ID." = ".$ci->db->escape($publicationId)."
        ";
		$query = $ci->db->query($sql);
		$response = $query->row(0,'model_publicacion');
		return $response;
	}
	/**
	 * Gets all results for this model but get a one result by id too.
	 *
	 * @param bool $id
	 * @return []
	 */
	function get_all($id = FALSE) {
		if ($id === FALSE) {
			$query = $this->db->get($this->table);
			return $query->result_array();
		}

		$query = $this->db->get_where($this->table, array($this->id => $id));
		return $query->row_array();
	}

	/**
	 * Add new Model
	 *
	 * @param $data
	 * @return mixed
	 */
	function insert($data) {
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	/**
	 * Update Model
	 *
	 * @param $id
	 * @param $data
	 * @return mixed
	 */
	function update($id, $data) {
		$this->db->where($this->id, $id);
		return $this->db->update($this->table, $data);
	}

	/**
	 * Delete Model
	 *
	 * @param $id
	 * @return mixed
	 */
	function delete($id) {
		return $this->db->update($this->table, array($this->id => $id));
	}

	/**
	 * Get all pay projects publishied
	 *
	 * @return []
	 */
	function get_pay_projects() {
		$sql = "
			SELECT DISTINCT
				pub_id, inm_id, inm_nombre, inm_direccion, cat_nombre, for_descripcion, inm_precio, pub_vig_ini, for_descripcion, zon_nombre, mon_abreviado, fot_archivo, inm_seo
			FROM
				publicacion
				INNER JOIN inmueble ON inm_id = pub_inm_id
				INNER JOIN categoria ON cat_id = inm_cat_id
				INNER JOIN forma ON for_id = inm_for_id
				INNER JOIN zona ON zon_id = inm_zon_id
				INNER JOIN moneda ON mon_id = inm_mon_id
				LEFT JOIN inmueble_foto ON fot_inm_id = inm_id
				INNER JOIN pagos p ON (p.`pag_pub_id`= pub_id AND p.`pag_estado` = 'Pagado')
				INNER JOIN servicios s ON s.ser_tipo = 'Banner'
				INNER JOIN pago_servicio ps ON (ps.pag_id = p.pag_id AND ps.ser_id = s.ser_id AND CURDATE() BETWEEN ps.`fech_ini` AND ps.`fech_fin`)
			WHERE
			  pub_estado = 'Aprobado'
			GROUP BY
			  pub_id
			ORDER BY
			  RAND()
			  LIMIT 8
		";
		return $this->db->query($sql)->result();
	}

	/**
	 * Gets all features from property.
	 *
	 * @param bool $id
	 * @return []
	 */
	function get_features_property($id = FALSE) {
		$this->db->where("eca_inm_id", $id);
		$query = $this->db->get("inmueble_caracteristica");
		return $query->result_array();
	}

	function get_pay_publications($forma = "", $posicion = "Principal", $ciudad = "") {
		if(!empty($forma)){
			$forma = " AND for_descripcion = '$forma'";
		}
		/*if(!empty($ciudad) && $ciudad != "Bolivia"){
			$ciudad = " AND dep_nombre = '$ciudad'";
		}else{
			$ciudad = "";
		}*/

		$sql = "
			SELECT DISTINCT
				pub_id,
				inm_nombre,
				cat_nombre,
				for_descripcion,
				inm_precio,
				pub_vig_ini,
				mon_abreviado,
				fot_archivo,
				inm_seo,
				ciu_nombre,
				inm_id,
				inm_superficie,
				inm_tipo_superficie,
				inm_latitud,
				inm_longitud,
				dep_nombre,
				usu_id,
				usu_tipo,
				usu_foto,
				usu_nombre,
				usu_empresa
			FROM
				publicacion
				INNER JOIN inmueble ON (inm_id = pub_inm_id AND inm_publicado = 'Si')
				INNER JOIN categoria ON cat_id = inm_cat_id
				INNER JOIN forma ON for_id = inm_for_id
				INNER JOIN moneda ON mon_id = inm_mon_id
				LEFT JOIN inmueble_foto ON fot_inm_id = inm_id
				INNER JOIN pagos p ON (p.`pag_pub_id`= pub_id AND p.`pag_estado` = 'Pagado')
				INNER JOIN servicios s ON s.ser_tipo = '".$posicion."'
				INNER JOIN ciudad ON ciu_id = inm_ciu_id
				INNER JOIN departamento ON dep_id = ciu_dep_id $ciudad
				INNER JOIN usuario ON (usu_id = pub_usu_id)
				INNER JOIN pago_servicio ps ON (ps.pag_id = p.pag_id AND ps.ser_id = s.ser_id AND CURDATE() BETWEEN ps.`fech_ini` AND ps.`fech_fin`)
			WHERE
				pub_estado = 'Aprobado'
				$forma
			GROUP BY
				pub_id
			ORDER BY
				RAND()
				LIMIT 8
			";echo"<pre>";

		$publications = $this->db->query($sql)->result();
		foreach($publications as $publication){
			$publication->features = $this->get_features_property($publication->inm_id);
            $publication->fot_archivo = $this->get_image_publication($publication->inm_id);
			$result[] = $publication;
		}
		return $result;
	}

    function get_pay_publications_home($forma = "", $posicion = "Principal", $ciudad = "") {
        if(!empty($forma)){
            $forma = " AND for_descripcion = '$forma'";
        }

        $sql = "
			SELECT DISTINCT
				pub_id
			FROM
				publicacion
				INNER JOIN inmueble ON (inm_id = pub_inm_id AND inm_publicado = 'Si')
				INNER JOIN categoria ON cat_id = inm_cat_id
				INNER JOIN forma ON for_id = inm_for_id
				INNER JOIN moneda ON mon_id = inm_mon_id
				LEFT JOIN inmueble_foto ON fot_inm_id = inm_id
				INNER JOIN pagos p ON (p.`pag_pub_id`= pub_id AND p.`pag_estado` = 'Pagado')
				INNER JOIN servicios s ON (s.ser_tipo = '".$posicion."' or s.ser_tipo = 'Anuncios')
				INNER JOIN ciudad ON ciu_id = inm_ciu_id
				INNER JOIN departamento ON dep_id = ciu_dep_id $ciudad
				INNER JOIN usuario ON (usu_id = pub_usu_id)
				INNER JOIN pago_servicio ps ON (ps.pag_id = p.pag_id AND ps.ser_id = s.ser_id AND CURDATE() BETWEEN ps.`fech_ini` AND ps.`fech_fin`)
			WHERE
				pub_estado = 'Aprobado'
				$forma
			GROUP BY
				pub_id
			";
        $num = $this->db->query($sql)->num_rows();
        if($num > 6){
            $limit = 8;
        }else{
            $limit = 4;
        }

        $sql = "
			(SELECT DISTINCT
				pub_id,
				ser_tipo,
				inm_nombre,
				cat_nombre,
				for_descripcion,
				inm_precio,
				pub_vig_ini,
				mon_abreviado,
				fot_archivo,
				inm_seo,
				dep_nombre,
				ciu_nombre,
				inm_id,
				inm_superficie,
				inm_tipo_superficie,
				inm_latitud,
				inm_longitud,
				dep_nombre,
				usu_id,
				usu_tipo,
				usu_foto,
				usu_nombre,
				usu_empresa,
				inm_direccion,
				pub_creado
			FROM
				publicacion
				INNER JOIN inmueble ON (inm_id = pub_inm_id AND inm_publicado = 'Si')
				INNER JOIN categoria ON cat_id = inm_cat_id
				INNER JOIN forma ON for_id = inm_for_id
				INNER JOIN moneda ON mon_id = inm_mon_id
				LEFT JOIN inmueble_foto ON fot_inm_id = inm_id
				INNER JOIN pagos p ON (p.`pag_pub_id`= pub_id AND p.`pag_estado` = 'Pagado')
				INNER JOIN servicios s ON (s.ser_tipo = '".$posicion."' or s.ser_tipo = 'Anuncios')
				INNER JOIN ciudad ON ciu_id = inm_ciu_id
				INNER JOIN departamento ON dep_id = ciu_dep_id $ciudad
				INNER JOIN usuario ON (usu_id = pub_usu_id)
				INNER JOIN pago_servicio ps ON (ps.pag_id = p.pag_id AND ps.ser_id = s.ser_id AND CURDATE() BETWEEN ps.`fech_ini` AND ps.`fech_fin`)
			WHERE
				pub_estado = 'Aprobado'
				$forma
			GROUP BY
				pub_id)
				UNION
				(
				SELECT DISTINCT
				pub_id,
				'gratis' ser_tipo,
				inm_nombre,
				cat_nombre,
				for_descripcion,
				inm_precio,
				pub_vig_ini,
				mon_abreviado,
				fot_archivo,
				inm_seo,
				dep_nombre,
				ciu_nombre,
				inm_id,
				inm_superficie,
				inm_tipo_superficie,
				inm_latitud,
				inm_longitud,
				dep_nombre,
				usu_id,
				usu_tipo,
				usu_foto,
				usu_nombre,
				usu_empresa,
				inm_direccion,
				pub_creado
			FROM
				publicacion
				INNER JOIN inmueble ON (inm_id = pub_inm_id AND inm_publicado = 'Si')
				INNER JOIN categoria ON cat_id = inm_cat_id
				INNER JOIN forma ON for_id = inm_for_id
				INNER JOIN moneda ON mon_id = inm_mon_id
				LEFT JOIN inmueble_foto ON fot_inm_id = inm_id
				INNER JOIN ciudad ON ciu_id = inm_ciu_id
				INNER JOIN departamento ON dep_id = ciu_dep_id
				INNER JOIN usuario ON (usu_id = pub_usu_id)
			WHERE
				pub_estado = 'Aprobado'

			GROUP BY
				pub_id
				)
			ORDER BY
				ser_tipo DESC, pub_creado desc,
				RAND()
				LIMIT 8
			";
		//echo"<pre>";var_dump($sql);exit;
        $publications = $this->db->query($sql)->result();
        foreach($publications as $publication){
            $publication->features = $this->get_features_property($publication->inm_id);
            $publication->fot_archivo = $this->get_image_publication($publication->inm_id);
            $result[] = $publication;
        }
        return $result;
    }

	function get_all_type_publications_map($forma, $ciudad = "") {
		if(!empty($forma)){
			$forma = " AND for_descripcion = '$forma'";
		}
		/*if(!empty($ciudad) && $ciudad != "Bolivia"){
			$ciudad = " AND dep_nombre = '$ciudad'";
		}else{
			$ciudad = "";
		}*/

		$sql = "
			SELECT DISTINCT
				pub_id,
				inm_nombre,
				cat_nombre,
				for_descripcion,
				inm_precio,
				pub_vig_ini,
				mon_abreviado,
				fot_archivo,
				inm_seo,
				ciu_nombre,
				inm_id,
				inm_superficie,
				inm_latitud,
				inm_longitud,
				dep_nombre,
				usu_id,
				usu_tipo,
				usu_foto,
				usu_nombre,
				usu_empresa
			FROM
				publicacion
				INNER JOIN inmueble ON (inm_id = pub_inm_id AND inm_publicado = 'Si')
				INNER JOIN categoria ON cat_id = inm_cat_id
				INNER JOIN forma ON for_id = inm_for_id
				INNER JOIN moneda ON mon_id = inm_mon_id
				LEFT JOIN inmueble_foto ON fot_inm_id = inm_id
				INNER JOIN pagos p ON (p.`pag_pub_id`= pub_id AND p.`pag_estado` = 'Pagado')
				INNER JOIN servicios s ON s.ser_tipo = 'Anuncios'
				INNER JOIN ciudad ON ciu_id = inm_ciu_id
				INNER JOIN departamento ON dep_id = ciu_dep_id $ciudad
				INNER JOIN usuario ON (usu_id = pub_usu_id)
				INNER JOIN pago_servicio ps ON (ps.pag_id = p.pag_id AND ps.ser_id = s.ser_id)
			WHERE
				pub_estado = 'Aprobado'
				AND CURDATE() <= pub_vig_fin
				$forma
			GROUP BY
				pub_id
			";

		return  $this->db->query($sql)->result();
	}

	function get_all_publications_query($limit,$page,$config)
	{
		$filters = $this->convert_filter_sql($config);

		$page = $page * $limit;

		$sql = "
		(
		SELECT DISTINCT bus_inmueble_proyecto.*, 'Proyecto' AS patrocinado FROM bus_inmueble_proyecto
        INNER JOIN proyecto p ON (p.proy_id = inm_proy_id)
        WHERE p.proy_estado = 'Aprobado'
        ".$filters["search"]."
		".$filters["type"]."
		".$filters["in"]."
		".$filters["city"]."
		".$filters["currency"]."
		".$filters["price"]."
		GROUP BY p.proy_id
        ORDER BY inm_precio ASC
		)
		UNION
		(
		SELECT DISTINCT bus_inmueble_3.*, 'Patrocinado' AS patrocinado FROM bus_inmueble_3
		INNER JOIN pagos p ON (p.`pag_pub_id`= pub_id AND p.`pag_estado` = 'Pagado')
		INNER JOIN servicios s ON s.ser_tipo = 'Busqueda'
		INNER JOIN pago_servicio ps ON (ps.pag_id = p.pag_id AND ps.ser_id = s.ser_id AND CURDATE() BETWEEN ps.`fech_ini` AND ps.`fech_fin`)
		".$filters["filter"]."
		WHERE pub_estado = 'Aprobado'
		AND inm_publicado = 'Si'
		".$filters["search"]."
		".$filters["type"]."
		".$filters["in"]."
		".$filters["city"]."
		".$filters["currency"]."
		".$filters["price"]."
		)
		UNION
		(
		SELECT DISTINCT bus_inmueble_3.*, '0' AS patrocinado FROM bus_inmueble_3
		".$filters["filter"]."
		WHERE pub_estado = 'Aprobado' AND CURDATE() <= pub_vig_fin
		AND inm_publicado = 'Si'
		AND inm_proy_id = 0
		".$filters["search"]."
		".$filters["type"]."
		".$filters["in"]."
		".$filters["city"]."
		".$filters["currency"]."
		".$filters["price"]."
		AND inm_id not in(
						SELECT DISTINCT bus_inmueble_3.inm_id FROM bus_inmueble_3
						INNER JOIN pagos p ON (p.`pag_pub_id`= pub_id AND p.`pag_estado` = 'Pagado')
						INNER JOIN servicios s ON s.ser_tipo = 'Busqueda'
						INNER JOIN pago_servicio ps ON (ps.pag_id = p.pag_id AND ps.ser_id = s.ser_id AND CURDATE() BETWEEN ps.`fech_ini` AND ps.`fech_fin`)
						".$filters["filter"]."
						WHERE pub_estado = 'Aprobado'
						AND inm_publicado = 'Si'
						".$filters["search"]."
						".$filters["type"]."
						".$filters["in"]."
						".$filters["city"]."
						".$filters["currency"]."
						".$filters["price"]."
						)
		)
		".$filters["sort"]."

		LIMIT $page, $limit
		";//echo"<pre>";var_dump($sql);exit;

		$result = $this->db->query($sql)->result();
		$num = $this->db->query("SELECT FOUND_ROWS() as total")->row();
		if($num->total > 0) {
			foreach ($result as $publication) {
				$publication->features = $this->get_features_property($publication->inm_id);
				$publication->fot_archivo = $this->get_image_publication($publication->inm_id);
				$publication->images = $this->get_all_images_publication($publication->inm_id);
				$data_search[] = $publication;
			}
		}else{
			$data_search = array();
		}

		$data_result = array(
			"num_total" => $num->total,
			"results" => $data_search
		);
		return $data_result;
	}

    function get_all_publications_query_map($config){
        $filters = $this->convert_filter_sql($config);

        $sql = "
		(
		SELECT DISTINCT bus_inmueble_3.*, 'Patrocinado' AS patrocinado FROM bus_inmueble_3
		INNER JOIN pagos p ON (p.`pag_pub_id`= pub_id AND p.`pag_estado` = 'Pagado')
		INNER JOIN servicios s ON s.ser_tipo = 'Busqueda'
		INNER JOIN pago_servicio ps ON (ps.pag_id = p.pag_id AND ps.ser_id = s.ser_id AND CURDATE() BETWEEN ps.`fech_ini` AND ps.`fech_fin`)
		".$filters["filter"]."
		WHERE pub_estado = 'Aprobado'
		AND inm_publicado = 'Si'
		".$filters["search"]."
		".$filters["type"]."
		".$filters["in"]."
		".$filters["city"]."
		".$filters["currency"]."
		".$filters["price"]."
		)
		UNION
		(
		SELECT DISTINCT bus_inmueble_3.*, 'Anuncio' AS patrocinado FROM bus_inmueble_3
		".$filters["filter"]."
		INNER JOIN pagos p ON (p.`pag_pub_id`= pub_id AND p.`pag_estado` = 'Pagado')
		INNER JOIN servicios s ON (s.ser_tipo = 'Anuncios')
		INNER JOIN pago_servicio ps ON (ps.pag_id = p.pag_id AND ps.ser_id = s.ser_id)
		WHERE pub_estado = 'Aprobado' AND CURDATE() <= pub_vig_fin
		AND inm_publicado = 'Si'
		AND inm_proy_id = 0
		".$filters["search"]."
		".$filters["type"]."
		".$filters["in"]."
		".$filters["city"]."
		".$filters["currency"]."
		".$filters["price"]."
		)
		";
        // ".$filters["sort"]."

        $result = $this->db->query($sql)->result();
        $num = $this->db->query("SELECT FOUND_ROWS() as total")->row();
        if($num->total > 0) {
            foreach ($result as $publication) {
                $publication->features = $this->get_features_property($publication->inm_id);
                $publication->fot_archivo = $this->get_image_publication($publication->inm_id);
                $publication->fot_archivo = "https://toqueeltimbre.com/librerias/timthumb.php?src=http://toqueeltimbre.com/admin/imagenes/inmueble/".$publication->fot_archivo."&w=130&h=100";
                $data_search[] = $publication;
            }
        }else{
            $data_search = array();
        }

        $data_result = array(
            "num_total" => $num->total,
            "results" => $data_search
        );
        return $data_result;
    }

	function convert_filter_sql($config)
	{
        $config->room = urldecode($config->room);
        $config->bathroom = urldecode($config->bathroom);
        $config->parking = urldecode($config->parking);
		$search = !empty($config->search) ? "AND (general LIKE '%" . $config->search . "%')" : '';
		$type = !empty($config->type) && $config->type != "Inmuebles" ? "AND cat_nombre = '" . strtoupper($config->type) . "'" : '';
		$in = !empty($config->in) ? "AND for_descripcion = '" . strtoupper($config->in) . "'" : '';
		$city = !empty($config->city) && $config->city != "Bolivia" ? "AND dep_nombre = '" . $config->city . "'" : '';
		$config->currency = empty($config->currency) ? "Dolares" : $config->currency;
		$currency = !empty($config->currency) ? "AND mon_descripcion = '" . $config->currency . "'" : '';

		if(!empty($config->sort)){
			$aux = explode("-",$config->sort);
			if($aux[1]){
				$sort = $aux[1] == "asc" ? "ASC" : "DESC";
			}

			if($aux[0] == "price"){
				$campo = "inm_precio";
			}
			if($aux[0] == "relevance"){
				$campo = "inm_proy_id";
				$sort = "DESC";
			}
			if($aux[0] == "date"){
				$campo = "pub_vig_ini";
			}
			$sort_cad = "ORDER BY $campo $sort";
		}else{
            //$sort_cad = "ORDER BY pub_renovado, pub_creado DESC";
            //$sort_cad = "ORDER BY patrocinado DESC, usu_certificado DESC, pub_renovado DESC, pub_creado DESC, inm_id DESC";
			$sort_cad = "ORDER BY patrocinado DESC, pub_renovado DESC, pub_creado DESC, inm_id DESC";
		}

		if(($config->price_max == "2000" && $config->in == "Alquiler") || ($config->price_max == "500000" && $config->in == "Venta") || ($config->price_max == "150000" && $config->in == "Anticretico") ) {
			$price = ($config->price_min != null && $config->price_max != null) ? "AND inm_precio >= " . $config->price_min : '';
		}else{
			$price = ($config->price_min != null && $config->price_max != null) ? "AND (inm_precio >= " . $config->price_min . " AND inm_precio <= " . $config->price_max . ")" : '';
		}

		if (!empty($config->room) || !empty($config->bathroom) || !empty($config->parking)) {
			$filter = "INNER JOIN inmueble_caracteristica ic ON ic.`eca_inm_id` = inm_id  ";
		}


		if(!empty($config->room)){
			$filter_array[] = $config->room;
            $array_ids[] = 4;
            if($config->room != "5+"){
                $room_value = " = '".$config->room."'";
            }else{
                $room_value = " >= '5'";
            }
            $room_ope = (!empty($config->bathroom) || !empty($config->parking)) ? " OR " : "";
			//$filter_room = " (ca.eca_car_id = 4 AND ca.`eca_valor` ".$room_value.") " . $room_ope;
            $filter_room = " (ic.eca_car_id = 4 AND ic.`eca_valor` ".$room_value.") " . $room_ope;
		}

		if(!empty($config->bathroom)){
			$filter_array[] = $config->bathroom;
            $array_ids[] = 5;
            if($config->bathroom != "5+"){
                $bathroom_value = " = '".$config->bathroom."'";
            }else{
                $bathroom_value = " >= '5'";
            }
            $bathroom_ope = !empty($config->parking) ? " OR " : "";
            //$filter_bathroom = " (ca.eca_car_id = 5 AND ca.`eca_valor` ".$bathroom_value.") " . $bathroom_ope;
            $filter_bathroom = " (ic.eca_car_id = 5 AND ic.`eca_valor` ".$bathroom_value.") " . $bathroom_ope;
		}

		if(!empty($config->parking)){
			$filter_array[] = $config->parking;
            $array_ids[] = 9;
            if($config->parking != "5+"){
                $parking_value = " = '".$config->parking."'";
            }else{
                $parking_value = " >= '5'";
            }
            //$filter_parking = " (ca.eca_car_id = 9 AND ca.`eca_valor` ".$parking_value.") ";
            $filter_parking = " (ic.eca_car_id = 9 AND ic.`eca_valor` ".$parking_value.") ";
		}

		if(!empty($filter_array)){
			$values = implode(',', $filter_array);
            $values_ids = implode(',', $array_ids);
            //$filter .= " AND ic.`eca_car_id` IN ( SELECT ca.`eca_car_id` FROM `inmueble_caracteristica` ca WHERE ca.`eca_inm_id` = inm_id AND ( $filter_room $filter_bathroom $filter_parking ) )";
            $filter .= " AND ( $filter_room $filter_bathroom $filter_parking ) ";
		}

		return array(
			"search" => $search,
			"type" => $type,
			"in" => $in,
			"city" => $city,
			"currency" => $currency,
			"price" => $price,
			"sort" => $sort_cad,
			"filter" => $filter
		);
	}

	function get_image_publication($inm_id) {
		$sql = "SELECT fot_archivo FROM inmueble_foto WHERE fot_inm_id = " . $inm_id . " ORDER BY fot_order ASC, fot_id ASC LIMIT 1";
		$obj = $this->db->query($sql)->row_array();
		return $obj["fot_archivo"];
	}

    function get_name_inmueble_by_pub_id($pub_id) {
        $sql = "
        SELECT inm.`inm_id`, inm.`inm_nombre` FROM publicacion pub
        INNER JOIN inmueble inm ON (inm.`inm_id` = pub.`pub_inm_id`)
        WHERE pub.`pub_id` = $pub_id
        ";
        $obj = $this->db->query($sql)->row();
        return $obj;
    }

    function get_inmueble_by_pub_id($pub_id) {
        $sql = "
        SELECT inm.`inm_id` FROM publicacion pub
        INNER JOIN inmueble inm ON (inm.`inm_id` = pub.`pub_inm_id`)
        WHERE pub.`pub_id` = $pub_id
        ";
        $obj = $this->db->query($sql)->row();
        return $obj->inm_id;
    }

	function get_all_images_publication($inm_id) {
		$sql = "SELECT fot_archivo FROM inmueble_foto WHERE fot_inm_id = " . $inm_id . " ORDER BY fot_order ASC, fot_id ASC";
		return $this->db->query($sql)->result_array();
	}

	function get_similar_properties($for_id, $ciu_id, $cat_id, $zon_id, $inm_id, $limit = 3){
		$sql = "
			SELECT DISTINCT bus_inmueble_3.*, 'Patrocinado' AS patrocinado FROM bus_inmueble_3
		INNER JOIN pagos p ON (p.`pag_pub_id`= pub_id AND p.`pag_estado` = 'Pagado')
		INNER JOIN servicios s ON s.ser_tipo = 'Similares'
		INNER JOIN pago_servicio ps ON (ps.pag_id = p.pag_id AND ps.ser_id = s.ser_id AND CURDATE() BETWEEN ps.`fech_ini` AND ps.`fech_fin`)
		WHERE pub_estado = 'Aprobado'
		AND inm_publicado = 'Si'
		AND inm_for_id = '$for_id'
		AND inm_ciu_id = '$ciu_id'
		AND inm_cat_id = '$cat_id'
		AND inm_zon_id = '$zon_id'
		AND inm_id <> '$inm_id'
		UNION
			SELECT DISTINCT bus_inmueble_3.*, 'No' AS patrocinado FROM bus_inmueble_3
			WHERE pub_estado = 'Aprobado' AND pub_vig_fin >=  CURDATE()
			AND inm_publicado = 'Si'
			AND inm_proy_id = 0
			AND inm_ciu_id = '$ciu_id'
			AND inm_cat_id = '$cat_id'
			AND inm_zon_id = '$zon_id'
			AND inm_id <> '$inm_id'
			ORDER BY patrocinado DESC, RAND()
			LIMIT ".$limit;
//		echo"<pre>";var_dump($sql);exit;
		$result = $this->db->query($sql)->result();
		$num = $this->db->query("SELECT FOUND_ROWS() as total")->row();
		if($num->total > 0) {
			foreach ($result as $publication) {
				$publication->features = $this->get_features_property($publication->inm_id);
				$publication->fot_archivo = $this->get_image_publication($publication->inm_id);
				$data_search[] = $publication;
			}
		}else{
			$data_search = array();
		}
		$data_result = array(
			"results" => $data_search
		);
		return $data_result;
	}

    function get_relacionadas_empty_properties($ciudad, $categoria){
        $sql = "
			SELECT DISTINCT bus_inmueble_3.* FROM bus_inmueble_3
			WHERE pub_estado = 'Aprobado' AND pub_vig_fin >=  CURDATE()
			AND inm_publicado = 'Si'
			AND inm_proy_id = 0
			AND ciu_nombre LIKE '%$ciudad%'
			AND cat_nombre LIKE '%$categoria%'
			ORDER BY RAND()
			LIMIT 12
		";

        $result = $this->db->query($sql)->result();
        $num = $this->db->query("SELECT FOUND_ROWS() as total")->row();
        if($num->total > 0) {
            foreach ($result as $publication) {
                $publication->features = $this->get_features_property($publication->inm_id);
                $publication->fot_archivo = $this->get_image_publication($publication->inm_id);
                $data_search[] = $publication;
            }
        }else{
            $data_search = array();
        }
        $data_result = array(
            "results" => $data_search
        );
        return $data_result;
    }

	function get_properties_inmobiliarias_id($limit, $page, $id){
		$page = $page * $limit;
		$sql = "
			SELECT DISTINCT bus_inmueble_3.* FROM bus_inmueble_3
			WHERE pub_estado = 'Aprobado' AND pub_vig_fin >=  CURDATE()
			AND inm_publicado = 'Si'
			AND inm_proy_id = 0
			AND pub_usu_id = '$id'
			ORDER BY pub_id DESC
			LIMIT $page, $limit
		";

		$result = $this->db->query($sql)->result();
		$num = $this->db->query("
		SELECT DISTINCT COUNT(pub_id) AS total FROM bus_inmueble_3
		WHERE pub_estado = 'Aprobado' AND pub_vig_fin >=  CURDATE()
		AND inm_publicado = 'Si'
		AND inm_proy_id = 0
		AND pub_usu_id = '$id'
		")->row();
		if($num->total > 0) {
			foreach ($result as $publication) {
				$publication->features = $this->get_features_property($publication->inm_id);
				$publication->fot_archivo = $this->get_image_publication($publication->inm_id);
				$data_search[] = $publication;
			}
		}else{
			$data_search = array();
		}
		$data_result = array(
			"results" => $data_search,
			"num_total" => $num->total
		);
		return $data_result;
	}


	function get_favorites_by_ids($favorite_array){

		foreach($favorite_array as $fav){
			$sql = "
			SELECT bus_inmueble_3.*, 'No' AS patrocinado FROM bus_inmueble_3
			WHERE inm_id = '$fav'
			";

			$publication = $this->db->query($sql)->row();
			$publication->features = $this->get_caracteristicas($publication->inm_id);
			$publication->fot_archivo = $this->get_image_publication($publication->inm_id);
			$data_search[] = $publication;
		}

		return $data_search;
	}

	function get_favorites_by_user_id($usu_id){
		$sql = "
		SELECT * FROM favorito
		WHERE usuario_id = '$usu_id'
		";
		$results = $this->db->query($sql)->result();
		foreach($results as $favorite):
			$sql = "
			SELECT bus_inmueble_3.*, 'No' AS patrocinado FROM bus_inmueble_3
			WHERE inm_id = '".$favorite->inmueble_id."'
			";
			$publication = $this->db->query($sql)->row();
			$publication->features = $this->get_caracteristicas($publication->inm_id);
			$publication->fot_archivo = $this->get_image_publication($publication->inm_id);
			$data_search[] = $publication;
		endforeach;

		return $data_search;
	}

	function get_caracteristicas($id) {
		$sql = "
			SELECT
				inm_car.`eca_inm_id` AS inmueble_id,
				car.`car_nombre` AS `caracteristica`,
				inm_car.`eca_valor` AS valor
			FROM
				`inmueble_caracteristica` inm_car
				INNER JOIN caracteristica car ON (car.car_id = inm_car.eca_car_id)
			WHERE
				inm_car.eca_inm_id = $id";

		return $this->db->query($sql)->result();
	}

	function get_num_publications_by_user($user_id, $searchText=""){
		$sql = "
			SELECT COUNT(inm.`inm_id`) AS cantidad FROM publicacion pub
			INNER JOIN inmueble inm ON (inm.`inm_id` = pub.`pub_inm_id` AND inm.inm_nombre LIKE '%$searchText%')
			WHERE pub.`pub_usu_id` = '$user_id' AND pub.`pub_estado` <> 'Eliminado'
			";

		$num = $this->db->query($sql)->row();
		return $num->cantidad;
	}

	function get_publications_by_user($user_id, $page, $limit, $searchText = ""){
		$page = ($page - 1) >= 0 ?  (($page-1)*$limit) : 0;
        // ORDER BY pub.pub_estado ASC, inm.`inm_publicado` ASC, pub.pub_vig_fin DESC
		$sql = "
			SELECT inm.*, MAX(pub.`pub_vig_fin`) AS finish, pub.`pub_estado`, ciu.`ciu_nombre`, dep.`dep_nombre`, cat.`cat_nombre`, fm.`for_descripcion`, pub.`pub_id`, COUNT(m.`men_id`) AS cantidad_mensajes
			FROM publicacion pub
			INNER JOIN inmueble inm ON (inm.`inm_id` = pub.`pub_inm_id` AND (inm.inm_nombre LIKE '%$searchText%' or pub_id LIKE '%$searchText%'))
			INNER JOIN ciudad ciu ON (ciu.`ciu_id` = inm.`inm_ciu_id`)
			INNER JOIN departamento dep ON (dep.`dep_id` = ciu.`ciu_dep_id`)
			INNER JOIN categoria cat ON (cat.`cat_id` = inm.`inm_cat_id`)
			INNER JOIN forma fm ON (fm.`for_id` = inm.`inm_for_id`)
			LEFT JOIN mensaje m ON (m.`men_inm_id` = inm.inm_id)
			WHERE pub.`pub_usu_id` = '$user_id' AND pub.`pub_estado` <> 'Eliminado'
			GROUP BY inm.`inm_id`
            ORDER BY pub.pub_creado DESC, inm.`inm_publicado` ASC
			LIMIT $page, $limit
			";

		return $this->db->query($sql)->result();
	}

	function has_payment_pending_renovation($publicationId){
		$sql = "
		SELECT COUNT(p.`pag_id`) AS cantidad FROM publicacion pub
		INNER JOIN pagos p ON (p.`pag_pub_id` = pub.`pub_id` AND p.`pag_estado` = 'Pendiente' AND p.`pag_concepto` = 'Renovacion')
		WHERE pub.`pub_id` = $publicationId AND CURDATE() <= p.`pag_fecha_ven`
		GROUP BY p.`pag_id`
		";

		$obj = $this->db->query($sql)->row();
		return $obj->cantidad > 0 ? true : false;
	}

	function exist_publication_payment($publicationId){
		$sql = "
		SELECT COUNT(p.`pag_id`) AS cantidad FROM publicacion pub
		INNER JOIN pagos p ON (p.`pag_pub_id` = pub.`pub_id` AND (p.`pag_estado` = 'Pagado' ) AND p.`pag_concepto` = 'Anuncio')
		WHERE pub.`pub_id` = $publicationId
		GROUP BY p.`pag_id`
		";

		$obj = $this->db->query($sql)->row();
		return $obj->cantidad > 0 ? true : false;
	}

	function has_payment_pending($publicationId){
		$sql = "
		SELECT COUNT(p.`pag_id`) AS cantidad FROM publicacion pub
		INNER JOIN pagos p ON (p.`pag_pub_id` = pub.`pub_id` AND p.`pag_estado` = 'Pendiente' AND p.`pag_concepto` = 'Anuncio')
		WHERE pub.`pub_id` = $publicationId AND CURDATE() <= p.`pag_fecha_ven`
		GROUP BY p.`pag_id`
		";

		$obj = $this->db->query($sql)->row();
		return $obj->cantidad > 0 ? true : false;
	}


	function is_payment_publication($publicationId){
		/*$sql = "
			SELECT *
			FROM pagos
			WHERE pagos.pag_pub_id =  $publicationId
			AND pagos.pag_entidad IS NOT NULL
		";*/
		$sql = "
		SELECT COUNT(p.`pag_id`) AS cantidad FROM publicacion pub
		INNER JOIN pagos p ON (p.`pag_pub_id` = pub.`pub_id` AND p.`pag_estado` = 'Pagado' AND p.`pag_concepto` = 'Anuncio')
		WHERE pub.`pub_id` = $publicationId AND CURDATE() <= pub.`pub_vig_fin`
		GROUP BY p.`pag_id`
		";

		$obj = $this->db->query($sql)->row();
		return $obj->cantidad > 0 ? true : false;
	}

	function has_payment_improve($publicationId){
		/*$sql = "
			SELECT *
			FROM pagos
			WHERE pagos.pag_pub_id =  $publicationId
			AND pagos.pag_entidad IS NOT NULL
		";*/
		$sql = "
		SELECT COUNT(p.`pag_id`) AS cantidad FROM publicacion pub
		INNER JOIN pagos p ON (p.`pag_pub_id` = pub.`pub_id` AND p.`pag_estado` = 'Pendiente' AND p.`pag_concepto` = 'Mejora')
		WHERE pub.`pub_id` = $publicationId AND CURDATE() <= pub.`pub_vig_fin` AND CURDATE() <= p.`pag_fecha_ven`
		GROUP BY p.`pag_id`
		";

		$obj = $this->db->query($sql)->row();
		return $obj->cantidad > 0 ? true : false;
	}

    function get_first_image_publication($inm_id){
        $sql = "
        SELECT f.`fot_archivo` FROM inmueble_foto f
        WHERE f.`fot_inm_id` = $inm_id
        ORDER BY f.`fot_id` ASC
        LIMIT 1
		";
        $obj = $this->db->query($sql)->row();
        return $obj->fot_archivo;
    }

	function exist_url_seo($url_seo){
		$sql = "
		  SELECT COUNT(inm.`inm_id`) AS cantidad FROM inmueble inm
		  WHERE inm.`inm_seo` = '$url_seo'
		";
		$num = $this->db->query($sql)->row();
		return $num->cantidad > 0 ? true : false;
	}

	public static function insertImages(array $propertyImageList = array())
	{
		$ci =&get_instance();
		$ci->db->insert_batch('inmueble_foto', $propertyImageList);
	}

	function insert_images($data) {
		$this->db->insert("inmueble_foto", $data);
		return $this->db->insert_id();
	}

	function insert_planos($data) {
		$this->db->insert("inmueble_planos", $data);
		return $this->db->insert_id();
	}

	function delete_publication($id){
		$value = array(
			'pub_estado' => 'Eliminado'
		);
		$this->db->where('pub_id', $id);
		$this->db->update($this->table, $value);
	}

	function get_publication($pub_id){
		$this->db->where('pub_id', $pub_id);
		$this->db->where('pub_estado', 'Aprobado');
		return $this->db->get($this->table)->row();
	}

	function get_created_publication($pId) {
		$this->db->where('pub_id', $pId);
		$this->db->where('pub_estado', 'No Aprobado');
		return $this->db->get($this->table)->row();
	}

	function is_expired($pub_id){
		/*$sql = "
		SELECT COUNT(pub.`pub_id`) AS cantidad FROM publicacion pub
		WHERE CURDATE() <= pub.`pub_vig_fin` AND pub.`pub_id` = $pub_id
		";*/
		$sql = "
		SELECT COUNT(pub.`pub_id`) AS cantidad FROM publicacion pub
		INNER JOIN pagos pa ON (pa.`pag_pub_id` = pub.`pub_id` AND pa.`pag_estado` = 'Pagado')
		INNER JOIN pago_servicio ps ON (ps.`pag_id` = pa.`pag_id` AND (ps.`ser_id` = 15 OR ps.`ser_id` = 20 ))
		WHERE pub.`pub_id` = $pub_id AND CURDATE() > pub.`pub_vig_fin`
		";
		$row = $this->db->query($sql)->row();
		return $row->cantidad > 0 ? true : false;
	}

	function insert_log_renew($data) {
		$this->db->insert('log_renovacion', $data);
	}

	function get_publications_visible_in_frontend($user_id){
		/*$sql = "
			SELECT pub.`pub_id` FROM publicacion pub
			INNER JOIN inmueble inm ON (inm.`inm_id` = pub.`pub_inm_id` AND inm.`inm_publicado` = 'Si')
			INNER JOIN pagos p ON (p.`pag_pub_id` = pub.`pub_id` AND p.`pag_estado` = 'Pagado' AND p.`pag_entidad` = 'Suscripcion')
			WHERE pub.`pub_usu_id` = $user_id AND pub.pub_estado = 'Aprobado'
			GROUP BY pub.`pub_id`
		";*/
        // Cuando se controla por el visible "Si" pueden crear todos los anuncios gratis, luego desactivan unos cuantos y activan otros mas como si contaran gratis, ese es el problema con esta variable
        $sql = "
			SELECT pub.`pub_id` FROM publicacion pub
			INNER JOIN inmueble inm ON (inm.`inm_id` = pub.`pub_inm_id` AND inm.`inm_publicado` = 'Si')
			INNER JOIN pagos p ON (p.`pag_pub_id` = pub.`pub_id` AND p.`pag_estado` = 'Pagado' AND p.`pag_entidad` = 'Suscripcion')
			WHERE pub.`pub_usu_id` = $user_id AND pub.pub_estado = 'Aprobado' AND pub.`pub_vig_fin` > CURDATE()
			GROUP BY pub.`pub_id`
		";
		return $this->db->query($sql)->num_rows();
	}

    function ya_esta_publicado($pub_id){
        $sql = "
			SELECT pub.`pub_id` FROM publicacion pub
            INNER JOIN inmueble inm ON (inm.`inm_id` = pub.`pub_inm_id` AND inm.`inm_publicado` = 'Si')
            INNER JOIN pagos p ON (p.`pag_pub_id` = pub.`pub_id` AND p.`pag_estado` = 'Pagado' AND p.`pag_entidad` = 'Suscripcion')
            WHERE pub.`pub_id` = $pub_id AND pub.pub_estado = 'Aprobado' AND pub.`pub_vig_fin` > CURDATE()
            GROUP BY pub.`pub_id`
		";
        return $this->db->query($sql)->num_rows();
    }

    function ya_esta_publicado_normal($pub_id){
        $sql = "
			SELECT pub.`pub_id` FROM publicacion pub
            INNER JOIN inmueble inm ON (inm.`inm_id` = pub.`pub_inm_id`)
            INNER JOIN pagos p ON (p.`pag_pub_id` = pub.`pub_id` AND p.`pag_estado` = 'Pagado' AND p.`pag_entidad` <> 'Suscripcion' AND (p.`pag_concepto` = 'Anuncio' OR p.`pag_concepto` = 'Renovacion'))
            WHERE pub.`pub_id` = $pub_id AND pub.pub_estado = 'Aprobado' AND pub.`pub_vig_fin` > CURDATE()
            GROUP BY pub.`pub_id`
		";
        return $this->db->query($sql)->num_rows();
    }

    function no_tiene_pagos($pub_id){
        $sql = "
			SELECT pub.`pub_id` FROM publicacion pub
            INNER JOIN pagos p ON (p.`pag_pub_id` = pub.`pub_id` AND p.`pag_estado` = 'Pagado')
            INNER JOIN inmueble im on (im.inm_id = pub.pub_inm_id AND im.inm_publicado = 'Si')
            WHERE pub.`pub_id` = $pub_id AND pub.pub_estado = 'Aprobado'
		";
        return $this->db->query($sql)->num_rows();
    }

    function has_paid_publication($pub_id){
        $sql = "
            SELECT pub.`pub_id` FROM publicacion pub
            INNER JOIN pagos p ON (p.`pag_pub_id` = pub.`pub_id` AND p.`pag_estado` = 'Pagado' AND (p.`pag_concepto` = 'Anuncio' OR p.`pag_concepto` = 'Renovacion'))
            INNER JOIN pago_servicio ps ON (ps.`pag_id` = p.`pag_id` AND ps.`fech_fin` > CURDATE())
            WHERE pub.`pub_id` = $pub_id AND pub.`pub_estado` = 'Aprobado'
		";
        return $this->db->query($sql)->num_rows();
    }

	function get_active_publications($user_id) {
		$sql = "
			SELECT pub.*, inm.inm_nombre, inm.inm_id, inm.inm_visitas
			FROM publicacion pub
			INNER JOIN inmueble inm ON (inm.inm_id = pub.pub_inm_id AND inm.inm_publicado = 'Si')
			-- INNER JOIN pagos p ON (p.pag_pub_id = pub.pub_id AND p.pag_estado = 'Pagado')
			WHERE pub.pub_usu_id = $user_id
			AND pub.pub_estado = 'Aprobado'
			AND CURDATE() <= pub.pub_vig_fin
			GROUP BY inm.inm_id
			ORDER BY pub.pub_vig_fin desc
		";

		return $this->db->query($sql)->result();
	}

	function get_publications_ended_this_week($user_id, $was_sent = false) {
		$str = "";
		if(!empty($was_sent)) {
			$str = "AND pub.pub_enviado_1 = $was_sent";
		}

		$sql = "
			SELECT pub.*, inm.inm_nombre
			FROM publicacion pub
			INNER JOIN inmueble inm ON (inm.`inm_id` = pub.`pub_inm_id` AND inm.`inm_publicado` = 'Si')
			-- INNER JOIN pagos p ON (p.`pag_pub_id` = pub.`pub_id` AND p.`pag_estado` = 'Pagado')
			WHERE pub.`pub_usu_id` = $user_id AND pub.pub_estado = 'Aprobado'
			$str
			AND pub.pub_vig_fin BETWEEN DATE_SUB(CURDATE(), INTERVAL WEEKDAY(CURDATE()) + 0 DAY) AND DATE_SUB(CURDATE(), INTERVAL WEEKDAY(CURDATE()) - 6 DAY) AND pub.pub_vig_fin >= CURDATE()
			GROUP BY inm.inm_id
			ORDER BY pub.pub_vig_fin DESC
		";

		return $this->db->query($sql)->result();
	}

	function get_publications_created_by_Category($category="", $createdDate = null){

		if (!empty($category)) {
			$category = " AND ca.cat_nombre = '$category'";
		}

		if (!is_null($createdDate)) {
			$createdDate = " AND pub_vig_ini = '$createdDate'";
		}

		$sql = "
			SELECT pub.*, inm.inm_nombre, inm.inm_id, inm.inm_cat_id, ca.cat_nombre
			FROM publicacion pub
			INNER JOIN inmueble inm ON (inm.inm_id = pub.pub_inm_id AND inm.inm_publicado = 'Si')
			INNER JOIN pagos p ON (p.pag_pub_id = pub.pub_id)
			INNER JOIN categoria ca on (ca.cat_id = inm.inm_cat_id)
			WHERE (pub.pub_estado = 'Aprobado' or pub.pub_estado = 'No Aprobado')
			$category
			$createdDate
			-- AND CURDATE() <= pub.pub_vig_fin
			GROUP BY inm.inm_id
			ORDER BY pub.pub_vig_fin DESC
		";

		return $this->db->query($sql)->result();
	}

	function get_publications_by_type($type = "") {
		if (!empty($type)) {
			$type = " AND ca.cat_nombre = '$type'";
		}
		$sql = "
			SELECT pub.*, inm.inm_nombre, inm.inm_id, inm.inm_visitas, inm.inm_cat_id, ca.cat_nombre
			FROM publicacion pub
			INNER JOIN inmueble inm ON (inm.inm_id = pub.pub_inm_id AND inm.inm_publicado = 'Si')
			INNER JOIN pagos p ON (p.pag_pub_id = pub.pub_id AND p.pag_estado = 'Pagado')
			INNER JOIN categoria ca on (ca.cat_id = inm.inm_cat_id)
			WHERE pub.pub_estado = 'Aprobado'
			$type
			AND CURDATE() <= pub.pub_vig_fin
			GROUP BY inm.inm_id
			ORDER BY pub.pub_vig_fin DESC
		";

		return $this->db->query($sql)->result();
	}

	function get_sum_total_by_type($type = "") {
		if (!empty($type)) {
			$type = " AND ca.cat_nombre = '$type'";
		}
		$sql = "
			SELECT pub.*, inm.inm_nombre, inm.inm_id, inm.inm_visitas, inm.inm_cat_id, ca.cat_nombre
			FROM publicacion pub
			INNER JOIN inmueble inm ON (inm.inm_id = pub.pub_inm_id AND inm.inm_publicado = 'Si')
			INNER JOIN pagos p ON (p.pag_pub_id = pub.pub_id AND p.pag_estado = 'Pagado')
			INNER JOIN categoria ca on (ca.cat_id = inm.inm_cat_id)
			WHERE pub.pub_estado = 'Aprobado'
			$type
			-- AND CURDATE() <= pub.pub_vig_fin
			GROUP BY inm.inm_id
			ORDER BY pub.pub_vig_fin DESC
		";

		return $this->db->query($sql)->result();
	}

	function get_publications_ended_this_week_users() {
		$sql = "
			SELECT pub.*, inm.inm_nombre
			FROM publicacion pub
			INNER JOIN inmueble inm ON (inm.`inm_id` = pub.`pub_inm_id` AND inm.`inm_publicado` = 'Si')
			INNER JOIN pagos p ON (p.`pag_pub_id` = pub.`pub_id` AND p.`pag_estado` = 'Pagado')
			WHERE pub.pub_estado = 'Aprobado'
			AND pub.pub_vig_fin BETWEEN CURDATE() AND DATE_ADD(CURDATE(), INTERVAL 7 DAY)
			GROUP BY pub.`pub_usu_id`
			ORDER BY pub.pub_vig_fin DESC
		";

		return $this->db->query($sql)->result();
	}

	function get_publications_ended_this_week_was_sent($user_id, $was_sent = false) {
		$str = "";
		if(!empty($was_sent)) {
			$str = "AND pub.pub_enviado_1 = 1";
		} else {
			$str = "AND pub.pub_enviado_1 <> 1";
		}

		$sql = "
			SELECT pub.*, inm.inm_nombre
			FROM publicacion pub
			INNER JOIN inmueble inm ON (inm.`inm_id` = pub.`pub_inm_id` AND inm.`inm_publicado` = 'Si')
			INNER JOIN pagos p ON (p.`pag_pub_id` = pub.`pub_id` AND p.`pag_estado` = 'Pagado')
			WHERE pub.`pub_usu_id` = $user_id AND pub.pub_estado = 'Aprobado'
			$str
			AND pub.pub_vig_fin BETWEEN CURDATE() AND DATE_ADD(CURDATE(), INTERVAL 7 DAY)
			GROUP BY inm.inm_id
			ORDER BY pub.pub_vig_fin DESC
		";

		return $this->db->query($sql)->result();
	}
}