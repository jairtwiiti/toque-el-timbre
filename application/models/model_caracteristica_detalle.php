<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_caracteristica_detalle extends CI_Model {

    private $table = 'caracteristica_detalle';

    function __construct() {
        parent::__construct();
        $this->load->database();
    }
}