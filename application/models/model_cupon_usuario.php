<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_cupon_usuario extends CI_Model {

    private $table = 'cupon_usuario';
    private $id = 'id';

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    /**
     * Gets all results for this model but get a one result by id too.
     *
     * @param bool $id
     * @return []
     */
    function get_all($id = FALSE) {
        if ($id === FALSE) {
            $query = $this->db->get($this->table);
            return $query->result_array();
        }

        $query = $this->db->get_where($this->table, array($this->id => $id));
        return $query->row_array();
    }

    /**
     * Add new Model
     *
     * @param $data
     * @return mixed
     */
    function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    /**
     * Update Model
     *
     * @param $id
     * @param $data
     * @return mixed
     */
    function update($id, $data) {
        $this->db->where($this->id, $id);
        return $this->db->update($this->table, $data);
    }

    /**
     * Delete Model
     *
     * @param $id
     * @return mixed
     */
    function delete($id) {
        return $this->db->update($this->table, array($this->id => $id));
    }

    function is_assigned($id_cupon, $id_user){
        $sql = "
        SELECT cu.id, c.`descuento` FROM cupon_usuario cu
        INNER JOIN cupon c ON (c.`id` = cu.`id_cupon`)
        WHERE cu.`id_cupon` = $id_cupon
        AND cu.`id_usuario` = $id_user
        ";
        $row = $this->db->query($sql)->row();
        return $row;
    }

    function is_cobrado($id_cupon, $id_user){
        $sql = "
        SELECT cu.id, c.`descuento` FROM cupon_usuario cu
        INNER JOIN cupon c ON (c.`id` = cu.`id_cupon`)
        WHERE cu.`id_cupon` = $id_cupon
        AND cu.`id_usuario` = $id_user
        AND cu.`cobrado` = 'Si'
        ";
        $num = $this->db->query($sql)->num_rows();
        return $num > 0 ? true : false;
    }

}