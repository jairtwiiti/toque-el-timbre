<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_inmueble_caracteristica extends CI_Model {

    private $table = 'inmueble_caracteristica';

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    /**
     * Add new Model
     *
     * @param $data
     * @return mixed
     */
    function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    /**
     * Update Model
     *
     * @param $id
     * @param $data
     * @return mixed
     */
    function update_features($inm_id, $car_id, $data) {
        $this->db->where('eca_inm_id', $inm_id);
        $this->db->where('eca_car_id', $car_id);
        return $this->db->update($this->table, $data);
    }

    function delete_features($inm_id) {
        $this->db->where('eca_inm_id', $inm_id);
        return $this->db->delete($this->table);
    }

    /**
     * Gets all features from property.
     *
     * @param bool $id
     * @return []
     */
    function get_features_property($id = FALSE) {
        $query = $this->db->get_where($this->table, array("eca_inm_id" => $id));
        return $query->row_array();
    }

    function get_features_by_inmueble($id = FALSE) {
        $sql = "
        SELECT * FROM inmueble_caracteristica car
        WHERE car.`eca_inm_id` = '$id'
        ";
        $result = $this->db->query($sql)->result();
        foreach($result as $value){
            $valores[$value->eca_car_id] = $value->eca_valor;
        }
        return $valores;
    }
}