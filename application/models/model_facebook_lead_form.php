<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_facebook_lead_form extends MY_Model {

    const TABLE_ID = "flf_id";
    const TABLE_NAME = "facebook_lead_forms";

    protected $_projectId;
    protected $_propertyId;
    protected $_fbFormId;
    protected $_leadgenExportCsvUrl;
    protected $_name;
    protected $_status;
    protected $_debugInfo;

    //Due to autoload is necessary to set the default data as empty string
    function __construct($projectId = "", $propertyId = "", $fbFormId =  "", $leadgenExportCsvUrl = "", $name = "", $status = "", $debugInfo = "")
    {
        parent::__construct();
        $this->_projectId = $projectId;
        $this->_propertyId = $propertyId;
        $this->_fbFormId = $fbFormId;
        $this->_leadgenExportCsvUrl = $leadgenExportCsvUrl;
        $this->_name = $name;
        $this->_status = $status;
        $this->_debugInfo = $debugInfo;
    }

    /**
     * Parse the current object to key => value array
     * @return array
     */
    public function toArray()
    {
        $tableAttributes = array(
            "flf_id" => $this->_id,
            "flf_project_id" =>  $this->_projectId,
            "flf_property_id" =>  $this->_propertyId,
            "flf_fb_form_id" =>  $this->_fbFormId,
            "flf_leadgen_export_csv_url" => $this->_leadgenExportCsvUrl,
            "flf_name" => $this->_name,
            "flf_status" => $this->_status,
            "flf_debug_info" => $this->_debugInfo,
            "flf_deleted" => $this->_deleted,
            "flf_createdon" => $this->_createdOn,
            "flf_createdby" => $this->_createdBy,
            "flf_editedon" => $this->_editedOn,
            "flf_editedby" => $this->_editedBy
        );
        return $tableAttributes;
    }

    /**
     * @param $className
     * @param $object
     * @return object
     */
    protected static function recast($className, $object)
    {
        $response =  null;
        if ($object instanceof stdClass)
        {
            if (!class_exists($className))
                throw new InvalidArgumentException(sprintf('Inexistant class %s.', $className));

            //Let's set the values to payment object using the data from stdObject
            $instance = new $className(
                $object->flf_project_id,
                $object->flf_property_id,
                $object->flf_fb_form_id,
                $object->flf_leadgen_export_csv_url,
                $object->flf_name,
                $object->flf_status,
                $object->flf_debug_info
            );
            $instance->_id = $object->flf_id;
            $instance->_deleted = $object->flf_deleted;
            $instance->_createdOn = $object->flf_createdon;
            $instance->_createdBy = $object->flf_createdby;
            $instance->_editedOn = $object->flf_editedon;
            $instance->_editedBy = $object->flf_editedby;
            $response = $instance;
        }
        return $response;
    }

    ################################################################################################# BEGIN - GETTERS

    public function getProjectId()
    {
        return $this->_projectId;
    }

    public function getPropertyId()
    {
        return $this->_propertyId;
    }

    public function getName()
    {
        return $this->_name;
    }

    public function getFbFormId()
    {
        return $this->_fbFormId;
    }

    /**
     * @param $projectId
     * @return array
     *
     */
    public static function getAllByProjectId($projectId)
    {
        $ci = &get_instance();
        $ci->load->database();
        $sql = "
        select * from ".static::TABLE_NAME." where flf_project_id = ".$ci->db->escape($projectId)."
        ";
        $query = $ci->db->query($sql);
        $result = static::recastArray(get_called_class(), $query->result());
        return $result;
    }

    public static function getByFbFormId($fbFormId)
    {
        $ci = &get_instance();
        $ci->load->database();
        $sql = "
        select * from ".static::TABLE_NAME." where flf_fb_form_id = ".$ci->db->escape($fbFormId)."
        ";
        $query = $ci->db->query($sql);
        $result = static::recast(get_called_class(), $query->row());
        return $result;
    }

    ################################################################################################# END - GETTERS


    ################################################################################################# BEGIN - SETTERS

    public function setLeadgenExportCsvUrl($leadgenExportUrl)
    {
        $this->_leadgenExportCsvUrl = $leadgenExportUrl;
    }

    public function setName($name)
    {
        $this->_name = $name;
    }

    public function setStatus($status)
    {
        $this->_status = $status;
    }

    public function setDebugInfo($debugInfo)
    {
        $this->_debugInfo = $debugInfo;
    }
    ################################################################################################# END - SETTERS

    public static function getByProjectId($projectId)
    {
        $ci = &get_instance();
        $ci->load->database();

        $sql = "
        SELECT * FROM ".static::TABLE_NAME."
        where
        flf_project_id = ".$ci->db->escape($projectId)." and
        flf_deleted != 1
        ";

        $query = $ci->db->query($sql);
        $result = static::recastArray(get_called_class(), $query->result());
        return $result;
    }

    public static function getByPropertyId($propertyId)
    {
        $ci = &get_instance();
        $ci->load->database();

        $sql = "
        SELECT * FROM ".static::TABLE_NAME."
        where
        flf_property_id = ".$ci->db->escape($propertyId)." and
        flf_deleted != 1
        ";

        $query = $ci->db->query($sql);
        $result = static::recastArray(get_called_class(), $query->result());
        return $result;
    }

    public static function search($text, $limit, $offset, $orderBy = null, $orderType = 'asc', $colsArray = null)
    {
        if ($orderBy === null)
        {
            $orderBy = static::TABLE_ID;
        }
        $ci = &get_instance();
        $ci->load->database();

        $sql = 'select '.static::_dataTableColumns().' from ' . static::TABLE_NAME;
        $sql .= ' where flf_deleted != 1 and (';
        foreach ($colsArray as $var)
        {
            $sql .= ' ' . $var . ' like \'%' . $text . '%\' or ';
        }

        $sql = substr($sql, 0, -3);
        $sql .= ') group by '.static::TABLE_ID.' order by ' . $orderBy . ' ' . $orderType . ' limit ' . $limit . ' offset ' . $offset;

        $query = $ci->db->query($sql);
        return $query->result();
    }

    private static function _dataTableColumns()
    {
        $columns = static::TABLE_NAME.".*";
        return $columns;
    }
}