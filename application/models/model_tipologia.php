<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_tipologia extends CI_Model {

    private $table = 'tipologia';
    private $id = 'tip_id';

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    /**
     * Gets all results for this model but get a one result by id too.
     *
     * @param bool $id
     * @return []
     */
    function get_all($id = FALSE) {
        if ($id === FALSE) {
            $query = $this->db->get($this->table);
            return $query->result_array();
        }

        $query = $this->db->get_where($this->table, array($this->id => $id));
        return $query->row_array();
    }

    /**
     * Add new Model
     *
     * @param $data
     * @return mixed
     */
    function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    /**
     * Update Model
     *
     * @param $id
     * @param $data
     * @return mixed
     */
    function update($id, $data) {
        $this->db->where($this->id, $id);
        return $this->db->update($this->table, $data);
    }

    /**
     * Delete Model
     *
     * @param $id
     * @return mixed
     */
    function delete($id) {
        return $this->db->update($this->table, array($this->id => $id));
    }


    function get_tipologia_list($project_id){
        $sql = "
        SELECT * FROM tipologia
        WHERE tip_proy_id = $project_id
        GROUP BY tip_id
        ";
        return $this->db->query($sql)->result();
    }

    function get_tipologia_id($id){
        $sql = "
        SELECT * FROM tipologia
        WHERE tip_id = $id
        ";
        return $this->db->query($sql)->row();
    }
}