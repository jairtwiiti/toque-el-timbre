<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_propertyfeature extends MY_Model {

    const TABLE_ID = "eca_id";
    const TABLE_NAME = "inmueble_caracteristica";

    protected $_propertyId;
    protected $_featureId;
    protected $_value;
    //Due to autoload is necessary to set the default data as empty string
    function __construct($propertyId = "", $featureId = "", $value = "")
    {
        parent::__construct();
        $this->_propertyId = $propertyId;
        $this->_featureId = $featureId;
        $this->_value = $value;
    }

    /**
     * Parse the current object to key => value array
     * @return array
     */
    public function toArray()
    {
        $tableAttributes = array(
                                "eca_id" => $this->_id,
                                "eca_inm_id" => $this->_propertyId,
                                "eca_car_id" => $this->_featureId,
                                "eca_valor" => $this->_value
                                );
        return $tableAttributes;
    }

    /**
     * @param $className
     * @param $object
     * @return object
     */
    protected static function recast($className, $object)
    {
        $response =  null;
        if ($object instanceof stdClass)
        {
            if (!class_exists($className))
                throw new InvalidArgumentException(sprintf('Inexistant class %s.', $className));

            //Let's set the values to payment object using the data from stdObject
            $instance = new $className(
                    $object->eca_inm_id,
                    $object->eca_car_id,
                    $object->eca_valor
            );
            $instance->_id = $object->eca_id;
            $response = $instance;
        }
        return $response;
    }
    ################################################################################################# BEGIN - GETTERS

    ################################################################################################# END - GETTERS

    ################################################################################################# BEGIN - SETTERS

    ################################################################################################# END - SETTERS
    public static function search($text, $limit, $offset, $orderBy = null, $orderType = 'asc', $colsArray = null)
    {
        if ($orderBy === null)
        {
            $orderBy = static::TABLE_ID;
        }
        $ci = &get_instance();
        $ci->load->database();

        $sql = 'select * from ' . static::TABLE_NAME;
        $sql .= ' where (';
        foreach ($colsArray as $var)
        {
            $sql .= ' ' . $var . ' like \'%' . $text . '%\' or ';
        }

        $sql = substr($sql, 0, -3);
        $sql .= ') order by ' . $orderBy . ' ' . $orderType . ' limit ' . $limit . ' offset ' . $offset;
        $query = $ci->db->query($sql);
        return static::recastArray(get_called_class(), $query->result());
    }
}