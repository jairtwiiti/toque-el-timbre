<html>
<head></head>
<body style="background-color:#f3f3f3; color: #4a4a4a; font-family: Helvetica,Arial,sans-serif;">
<table width="100%" cellpadding="0" cellspacing="0" style="border-spacing: 0; line-height: 20px; margin: 0 auto;">
    <tr>
        <td style="padding-left: 10px; padding-right: 10px;">
            <table width="100%" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" style="margin: 0 auto; max-width: 600px; width: 100%; margin-top: 3%;">
                <tr>
                    <td style="padding-top:2%; border-top: 4px #19bdf4 solid; text-align: center">
                        <a href="http://toqueeltimbre.com" target="_blank">
                            <img src="http://toqueeltimbre.com/assets/img/logo.png" style="width: 100%; max-width: 300px;"/>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #ffffff; padding: 6% 9%;">
                        <table width="100%" cellpadding="0" cellspacing="0" style="padding-bottom: 20px; width: 100%;">
                            <tr>
                                <td style="font-size: 14px; line-height: 22px;">
                                    <h3 style="color:#484848; font-size: 16px;">Recibiste un Mensaje de ToqueelTimbre.com</h3>
                                    <p style="color:#474747">Referente al proyecto <b><?=$project['proy_nombre']?></b> el mensaje es el siguiente:</p>
                                    <p><b>Datos del contacto:</b></p>
                                    <p>
                                        <b>Nombre:</b> <?=$userSender["usu_nombre"]." ".$userSender["usu_apellido"]?> <br />
                                        <b>Mensaje</b><br />
                                        <?=$comment?><br>
                                        <br>
                                    </p>
                                    <a href="<?=base_url("dashboard/messages")?>" style="position: relative ; top: 14px ; text-align: center ; background-color: #00aeef; min-width: 160px ; display: inline-block ; margin: 0 auto ; color: #fff ; padding: 10px ; cursor: pointer;">
                                        Responder este mensaje
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #555759; border-top:5px solid #00aeef; padding: 2% 9%;">
                        &nbsp;
                        <!--<table width="100%" cellpadding="0" cellspacing="0" style="padding-bottom: 20px; width: 100%;">
                            <tr>
                                <td style="color: #fff; text-align: center; line-height: 20px;">
                                    Avenida Beni Calle Murur&eacute; 2010<br />
                                    Telf. (591) 3-3437530<br />
                                    info@toqueeltimbre.com<br />
                                    Santa Cruz - Bolivia
                                </td>
                            </tr>
                        </table>-->
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td width="100%" style="padding:20px 0" cellpadding="0" cellspacing="0" border="0">
            <table width="100%" cellspacing="0" cellpadding="0" border="0" style="border-collapse:collapse">
                <tr>
                    <td width="100%" style="padding:20px">
                        <div style="margin:0 0 6px 0;text-align:center;font-size:14px; color:#a8a8a8">
                            Si no quieres recibir notificaciones, se puede dar de baja enviando un mail a
                            <a target="_blank" style="color:#a8a8a8;font-weight:bold;text-decoration:none;" href="mailto:suscripciones@toqueeltimbre.com">suscripciones@toqueeltimbre.com</a>
                        </div>
                        <div style="margin:0;font-size:13px;text-align:center;color:#a8a8a8">&copy; 2015 ToqueElTimbre.com</div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
<html>

					