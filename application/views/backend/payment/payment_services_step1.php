﻿<?php
if(@validation_errors()){ echo '<div class="alert alert-danger">'.@validation_errors().'</div>'; }
if(@$cupon_error == "-1"){ echo '<div class="alert alert-danger"><button class="close" aria-hidden="true" data-dismiss="alert" type="button"></button>El cupon es invalido.</div>'; }
if(@$cupon_error == "-2"){ echo '<div class="alert alert-danger"><button class="close" aria-hidden="true" data-dismiss="alert" type="button"></button>El cupon ha expirado.</div>'; }
if(@$cupon_error == "-3"){ echo '<div class="alert alert-danger"><button class="close" aria-hidden="true" data-dismiss="alert" type="button"></button>Este cupon no ha sido asignado a tu cuenta.</div>'; }
?>
<!-- BEGIN PAGE CONTENT-->
<div class="row">
	<div class="col-md-2"></div>
    <div class="col-md-8">
    <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i> Pagos
                </div>
                <div class="tools"></div>
            </div>
	        <div class="portlet-body form">
                <form id="form_payment_step1" role="form" method="post" enctype="multipart/form-data" action="<?= base_url(); ?>dashboard/payment/publication/<?= $publicationId?>">
<!--                <form id="form_payment_step1" role="form" method="post">-->
                    <div class="form-body">
                        <div class="form-group">
<!--                            <label>Paraa que tu anuncio sea publico debes cancelar la suma de :</label>-->
	                        <div class="row">
		                        <div class="col-md-3"></div>
		                        <input type="hidden" value="" name="anuncio" value="">
								<div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
									<div class="dashboard-stat green" style="height: 122px; cursor: pointer" rel="<?php echo $service->ser_id; ?>" data-price="<?php echo $service->ser_precio; ?>">
										<div class="visual">
											<!-- <i class="fa fa-home"></i> -->
										</div>
										<div class="details2">
											<div class="published">
												Tu anuncio ya esta publicado
											</div>
										</div>
									</div>
								</div>
		                        <?php
								//by now prevent loop the particular services list
								$particularServices = array();
								foreach($particularServices as $service) { ?>
                                    <?php
                                    if($sw_subscription){
                                        $service->ser_precio = $service->ser_precio_suscriptor;
                                    }
                                    ?>
			                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
				                        <div class="dashboard-stat green" style="height: 122px; cursor: pointer" rel="<?php echo $service->ser_id; ?>" data-price="<?php echo $service->ser_precio; ?>">
					                        <div class="visual">
						                        <!-- <i class="fa fa-home"></i> -->
					                        </div>
					                        <div class="details">
						                        <div class="number">
							                        <?php echo $service->ser_precio; ?> Bs
						                        </div>
						                        <div class="desc">

							                        <input type="radio" name="groupradio" value="<?= $service->ser_precio; ?>">

							                        Por <?php echo $service->ser_dias; ?> dias
						                        </div>
					                        </div>
				                        </div>
			                        </div>
		                        <?php } ?>
		                        <div class="col-md-2"></div>
	                        </div>
	                        <br/>
	                        <div class="input-group">
		                        <div>
		                        </div>
	                        </div>
                        </div>
						<div class="form-group">
							<label><b>Puedes destacar tu anuncio publicando en nuestros espacios especiales:</b></label>
							<div class="portlet box white checkout">
								<div class="portlet-body form">

									<div class="form-body">
										<div class="panel-group accordion" id="accordion" role="tablist" aria-multiselectable="true">
											<div class="panel panel-default">
												<div class="panel-heading" role="tab" id="headingOne">
													<h4 class="panel-title">
														<a class="accordion-toggle accordion-toggle-styled " data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-controls="collapseOne">
															<i class="fa fa-arrow-right"></i> DESTACAR ANUNCIO
														</a>
													</h4>
												</div>
												<div id="collapseOne" class="panel-collapse" role="tabpanel" aria-labelledby="headingOne">
													<div class="panel-body">
														<div id="html_pagos">
															<div class="form-body">
																<div class="row">
																	<div class="col-md-7">
																		<div class="form-group">
																			<label>Aparecer en pagina principal</label>&nbsp;&nbsp;<span class="form-group-clear hide" data-radio-name="principal">Limpiar</span>
																			<div class="input-group">
																				<div class="icheck-list" id="select-radio-principal">
																					<?php foreach($mainServices as $service) {
																						$serviceCost = $sw_subscription ? $service->ser_precio_suscriptor : $service->ser_precio;
																						?>
																						<label days="<?= $service->ser_dias?>" cost="<?= $serviceCost ?>">
																							<input type="checkbox" name="services[]" class="icheck" data-radio="iradio_square-grey" value="<?= $service->ser_id?>" >
																							<?= $service->ser_dias?> días (<?php if($sw_subscription) {?><span style="text-decoration: line-through">Bs. <?= $service->ser_precio ?> </span>, <?php } ?> Bs. <?= $serviceCost ?>.-)
																						</label>
																					<?php }?>
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="col-md-2">
																		<a href="#" class="thumbnail" data-toggle="modal" data-target=".service-example" data-service-title="Aparecer en pagina principal" data-service-type="principal"> <img alt="100%x180" data-src="holder.js/100%x180" style="height: auto; width: 110px; display: block;" src="<?=base_url("admin/imagenes/principalServiceExample.PNG")?>" data-holder-rendered="true"> </a>
																	</div>
																</div>
																<div class="row">
																	<div class="col-md-7">
																		<div class="form-group">
																			<label>Primeros lugares en resultados de búsqueda</label>&nbsp;&nbsp;<span class="form-group-clear hide" data-radio-name="busqueda">Limpiar</span>
																			<div class="input-group">
																				<div class="icheck-list" id="select-radio-busqueda">
																					<?php foreach($searchServices as $service) {
																						$serviceCost = $sw_subscription ? $service->ser_precio_suscriptor : $service->ser_precio;
																						?>
																						<label days="<?= $service->ser_dias?>" cost="<?= $serviceCost ?>">
																							<input type="checkbox" name="services[]" class="icheck" data-radio="iradio_square-grey" value="<?= $service->ser_id ?>">
																							<?= $service->ser_dias?> días (<?php if($sw_subscription) {?><span style="text-decoration: line-through">Bs. <?= $service->ser_precio ?> </span>, <?php } ?> Bs. <?= $serviceCost ?>.-)
																						</label>
																					<?php }?>
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="col-md-2">
																		<a href="#" class="thumbnail" data-toggle="modal" data-target=".service-example" data-service-title="Primeros lugares en resultados de búsqueda" data-service-type="search"> <img alt="100%x180" data-src="holder.js/100%x180" style="height: auto; width: 110px; display: block;" src="<?=base_url("admin/imagenes/searchServiceExample.PNG")?>" data-holder-rendered="true"> </a>
																	</div>
																</div>
																<div class="row">
																	<div class="col-md-7">
																		<div class="form-group">
																			<label>Primeros lugares en anuncios similares</label>&nbsp;&nbsp;<span class="form-group-clear hide" data-radio-name="similares">Limpiar</span>
																			<div class="input-group">
																				<div class="icheck-list" id="select-radio-similares">
																					<?php foreach($similarServices as $service) {
																						$serviceCost = $sw_subscription ? $service->ser_precio_suscriptor : $service->ser_precio;
																						?>
																						<label days="<?= $service->ser_dias?>" cost="<?= $serviceCost ?>">
																							<input type="checkbox" name="services[]" class="icheck" data-radio="iradio_square-grey" value="<?= $service->ser_id ?>">
																							<?= $service->ser_dias?> días (<?php if($sw_subscription) {?><span style="text-decoration: line-through">Bs. <?= $service->ser_precio ?> </span>, <?php } ?> Bs. <?= $serviceCost ?>.-)
																						</label>
																					<?php }?>
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="col-md-2">
																		<a href="#" class="thumbnail" data-toggle="modal" data-target=".service-example" data-service-title="Primeros lugares en anuncios similares" data-service-type="similars"> <img alt="100%x180" data-src="holder.js/100%x180" style="height: auto; width: 110px; display: block;" src="<?=base_url("admin/imagenes/similarsServiceExample.PNG")?>" data-holder-rendered="true"> </a>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row" id="customer-information">
										<div class="col-md-12 hide">
											<div class="row">
												<div class="col-md-6 pull-right">
													<div class="row">
														<div class="col-md-12 mensaje_cupon"></div>
													</div>
													<div class="row">
														<div class="col-md-6">
															<div class="input-group pull-right">
																<a class="btn btn-info blue" href="javascript:;" id="canjear_cupon" rel="<?php echo base_url(); ?>dashboard/payment/validar_cupon" style="max-width: 300px;">Carjear Cupon</a>
															</div>
														</div>
														<div class="col-md-6">
															<div class="input-group pull-right">
																<input type="text" name="coupon" class="form-control" >
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div style="text-align: right;padding-right: 15px;">
											<input id="paymentTotal" type="hidden" name="paymentTotal" value="0">
											<input id="paymentTotalNew" type="hidden" name="paymentTotalNew" value="0">
											<input id="descuentoAjax" type="hidden" name="descuentoAjax" value="0">
											<input id="descuento" type="hidden" name="descuento" value="0">
											<input id="descuentoId" type="hidden" name="descuentoId" value="0">
											<input type="hidden" name="concepto" value="<?php echo $concepto; ?>">
											<h4 class="hide">Descuento: &nbsp;-<span id="total_discount">0</span> Bs.</h4>
											<h4>Total: &nbsp;<span id="total_payment">0</span> Bs.</h4>
										</div>
										<div class="col-md-12">
											<div class="form-group">
												<label><b>DATOS DE FACTURACIÓN</b></label>
												<br>
												<div class="row">
													<div class="col-md-6">
														<div class="input-group">
															<input type="text" name="userName" class="form-control" placeholder="Nombre Completo" id="userName" required>
															<span class="input-group-addon">
																<i class="fa fa-user"></i>
															</span>
														</div>
													</div>
													<div class="col-md-6">
														<div class="input-group">
															<input type="text" name="userNit" class="form-control" id="userNit" placeholder="Nro. de NIT/C.I:" required>
															<span class="input-group-addon">
																<i class="fa fa-slack"></i>
															</span>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="form-actions">
							<div class="pull-right" id="buttons-action">
								<a class="btn btn-info blue see-my-publishments" href="<?php echo base_url(); ?>dashboard/publications" title="Aceptar">Ver mis publicaciones</a>
								<input class="btn btn-info blue" type="submit" value="Pagar" name="btnPagar">
								<div class="progress no-margin hide" id="payment-progress-bar">
									<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width:0%"></div>
								</div>
							</div>
						</div>
                    </div>
                </form>
			</div>
		</div>
        <!-- END SAMPLE FORM PORTLET-->
    </div>
	<div class="col-md-2">

	</div>

</div>
<!-- END PAGE CONTENT-->

<script>
</script>
<!-- Small modal -->
<div class="modal fade service-example" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h4 class="modal-title" id="service-title-example"></h4>
			</div>
			<div class="modal-body">
				<img class="principal-service hide" alt="100%x180" data-src="holder.js/100%x180" style="height: auto; width: 100%; display: block;" src="<?=base_url("admin/imagenes/principalServiceExample.PNG")?>" data-holder-rendered="true">
				<img class="search-service hide" alt="100%x180" data-src="holder.js/100%x180" style="height: auto; width: 100%; display: block;" src="<?=base_url("admin/imagenes/searchServiceExample.PNG")?>" data-holder-rendered="true">
				<img class="similars-service hide" alt="100%x180" data-src="holder.js/100%x180" style="height: auto; width: 100%; display: block;" src="<?=base_url("admin/imagenes/similarsServiceExample.PNG")?>" data-holder-rendered="true">
			</div>

		</div>
	</div>
</div>
