<form action="<?php echo base_url(); ?>dashboard/payment/tigomoney" method="post" target="_top">
    <div class="row">
        <div class="col-md-4">
            <p><input type="text" name="linea" placeholder="Linea Tigo Money" style="padding: 7px 10px; font-size: 14px;" required /></p>
        </div>
        <div class="col-md-4">
            <input type="hidden" name="pag_id" value="<?php echo $_GET['pag_id']; ?>" />
            <p><input type="submit" value="Pagar con Tigo Money" style="background-color: #24a5d9; color: white; border: 0; padding: 7px 14px; font-size: 14px;" /></p>
        </div>
    </div>
</form>