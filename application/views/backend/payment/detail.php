<?php
$payment = $detail['payment'];
$services = $detail['services'];
$inmueble = $detail['inmueble'];
$pag_id = str_replace('toque','', $payment->pag_id);
?>
<!-- BEGIN PAGE CONTENT-->
<div class="row">
<div class="col-md-12">
<!-- Begin: life time stats -->
    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-basket font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">Pago #<?php echo $pag_id; ?> </span>
                <span class="caption-helper"><?php echo date('M d, Y H:i:s', strtotime($payment->pag_fecha)); ?></span>
            </div>
            <div class="actions">

            </div>
        </div>
        <div class="portlet-body">
            <div class="tabbable">

                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <div class="portlet yellow-crusta box">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-cogs"></i>Detalle de Pago
                                        </div>
                                        <div class="actions">

                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="row static-info">
                                            <div class="col-md-5 name">
                                                Cod. Pago #:
                                            </div>
                                            <div class="col-md-7 value">
                                                <?php echo $pag_id; ?>
                                            </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-5 name">
                                                Fecha:
                                            </div>
                                            <div class="col-md-7 value">
                                                <?php echo date('M d, Y H:i:s A', strtotime($payment->pag_fecha)); ?>
                                            </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-5 name">
                                                Estado:
                                            </div>
                                            <div class="col-md-7 value">
                                                <?php if($payment->pag_estado == 'Pendiente'): ?>
                                                    <span class="label label-danger">Por Pagar </span>
                                                <?php endif; ?>
                                                <?php if($payment->pag_estado == 'Pagado'): ?>
                                                    <span class="label label-success">Pagado </span>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-5 name">
                                                Total:
                                            </div>
                                            <div class="col-md-7 value">
                                                <?php echo number_format($payment->pag_monto,2); ?> Bs
                                            </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-5 name">
                                                Entidad:
                                            </div>
                                            <div class="col-md-7 value">
                                                <?php echo $payment->pag_entidad; ?>
                                            </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-5 name">
                                                Nombre/Razon Social:
                                            </div>
                                            <div class="col-md-7 value">
                                                <?php echo $payment->pag_nombre; ?>
                                            </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-5 name">
                                                NIT:
                                            </div>
                                            <div class="col-md-7 value">
                                                <?php echo $payment->pag_nit; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="portlet green-meadow box">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-cogs"></i>Datos de Inmueble
                                        </div>
                                        <div class="actions">

                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="row static-info">
                                            <div class="col-md-12 value">
                                                <?php echo $inmueble->inm_nombre; ?>
                                            </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-12 value">
                                                <?php echo $inmueble->zon_nombre; ?>, <?php echo $inmueble->inm_direccion; ?>
                                            </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-12 value">
                                                Precio: <?php echo number_format($inmueble->inm_precio, 2); ?> <?php echo $inmueble->inm_mon_id == 1 ? 'Bs':'USD'; ?>
                                            </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-12 value">
                                                <?php echo $inmueble->ciu_nombre; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-cogs"></i>Servicios
                                        </div>
                                        <div class="actions">

                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-responsive">
                                            <table class="table table-hover table-bordered table-striped">
                                                <thead>
                                                <tr>
                                                    <th>
                                                        Servicio
                                                    </th>
                                                    <th>
                                                        Precio (Bs)
                                                    </th>
                                                    <th>
                                                        Cantidad
                                                    </th>
                                                    <th>
                                                        Fecha Inicio
                                                    </th>
                                                    <th>
                                                        Fecha Fin
                                                    </th>
                                                    <th>
                                                        Total
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php if(!empty($services)): ?>
                                                    <?php foreach($services as $service): ?>
                                                        <tr>
                                                            <td>
                                                                <?php echo $service->ser_descripcion; ?>
                                                            </td>
                                                            <td>
                                                                <?php
                                                                $servicePrice = $service->ser_precio;
                                                                if($hasSubscription)
                                                                {
                                                                    $servicePrice = $service->ser_precio_suscriptor;
                                                                }
                                                                echo number_format($servicePrice,2);
                                                                ?>
                                                            </td>
                                                            <td>
                                                                1
                                                            </td>
                                                            <td>
                                                                <?php echo !empty($service->fech_ini) ? date('d/m/Y', strtotime($service->fech_ini)) : 'No Definido'; ?>
                                                            </td>
                                                            <td>
                                                                <?php echo !empty($service->fech_fin) ? date('d/m/Y', strtotime($service->fech_fin)) : 'No Definido'; ?>
                                                            </td>
                                                            <td>
                                                                <?php echo number_format(($servicePrice * 1),2); ?>
                                                            </td>
                                                        </tr>
                                                        <?php $total = $total + ($servicePrice * 1); ?>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                            </div>
                            <div class="col-md-6">
                                <div class="well">
                                    <div class="row static-info align-reverse">
                                        <div class="col-md-8 name">
                                            Sub Total:
                                        </div>
                                        <div class="col-md-3 value">
                                            <?php echo number_format($total, 2); ?> Bs
                                        </div>
                                    </div>
                                    <div class="row static-info align-reverse">
                                        <div class="col-md-8 name">
                                            Total Pago:
                                        </div>
                                        <div class="col-md-3 value">
                                            <?php echo number_format($total, 2); ?> Bs
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
<!-- End: life time stats -->
</div>
</div>
<!-- END PAGE CONTENT-->