<?php
$action = $_GET['action'];
?>
<!-- BEGIN PAGE CONTENT-->
<div class="row">
<div class="col-md-12">

    <?php if($action == 'delete'): ?>
        <div class="alert alert-danger">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
            <strong>Eliminado correctamente!</strong> se ha eliminado su pago.
        </div>
    <?php endif; ?>

    <!-- BEGIN EXAMPLE TABLE PORTLET-->
    <div class="portlet box grey-cascade">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-globe"></i>Mis Pagos
        </div>
        <div class="tools">
        </div>
    </div>

    <div class="portlet-body">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">

                </div>
                <div class="col-md-6">

                </div>
            </div>
        </div>

        <?php
        $status = !empty($_GET['status']) ? $_GET['status'] : 'Pagado';
        $uid = $_GET['uid'];
        $userid = $_GET['userid'];
        ?>
        <div class=" portlet-tabs">
            <ul class="nav nav-tabs">
                <li class="<?=$status == 'Pagado' ? 'active' : ''; ?>">
                    <a href="<?=base_url('dashboard/payments?status=Pagado&uid='.$uid.'&userid='.$userid); ?>">
                        Pagados </a>
                </li>
                <li class="<?=$status == 'Pendiente' ? 'active' : ''; ?>">
                    <a href="<?=base_url('dashboard/payments?status=Pendiente&uid='.$uid.'&userid='.$userid); ?>">
                        Pendientes </a>
                </li>
            </ul>
        </div>

        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover" id="table-payment">
            <thead>
            <tr>
                <th>Cod. Pago</th>
                <th>Anuncio</th>
                <th>Fecha Compra</th>
                <th>Servicios</th>
                <th>Estado</th>
                <th>Monto (Bs)</th>
                <th>Opciones</th>
            </tr>
            </thead>
            <tbody>
            <?php if(count($payments) > 0){ ?>
                <?php foreach($payments as $payment): ?>
                    <tr class="odd gradeX">
                        <td>
                            <?php echo $payment->pag_id; ?>
                        </td>
                        <td>
                            <?php echo $payment->inm_nombre; ?>
                        </td>
                        <td>
                            <?php echo date('d/m/Y', strtotime($payment->pag_fecha)); ?>
                        </td>
                        <td>
                            <?php $services = $this->model_pago_servicio->get_name_services($payment->pag_id); ?>
                            <?php foreach($services as $service): ?>
                                - <?php echo $service->ser_descripcion; ?><br />
                            <?php endforeach; ?>
                        </td>
                        <td>
                            <?php if($payment->pag_estado == 'Pagado'): ?>
                                <span class="label label-sm label-success">Pagado</span>
                            <?php endif; ?>
                            <?php if($payment->pag_estado == 'Pendiente'): ?>
                                <span class="label label-sm label-warning">Pendiente</span>
                            <?php endif; ?>
                        </td>
                        <td class="center">
                            <?php echo number_format($payment->pag_monto, 2); ?>
                        </td>
                        <td>
                            <?php if($payment->pag_estado == 'Pendiente'): ?>
                                <a class="btn btn-sm red" href="<?php echo base_url(); ?>dashboard/payment/delete/<?php echo str_replace('toque', '', $payment->pag_id); ?>" title="Eliminar"><i class="fa fa-trash-o"></i></a>
                            <?php endif; ?>
                            <a class="btn btn-sm green" href="<?php echo base_url(); ?>dashboard/payment/detail/<?php echo str_replace('toque', '', $payment->pag_id); ?>" title="Ver Detalle"><i class="fa fa-search"></i> Ver Detalle</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php }else{ ?>
                <tr class="odd gradeX">
                    <td colspan="6" align="center">
                        No se ha encontrado resultados.
                    </td>
                </tr>
            <?php } ?>
            </tbody>
            </table>

            <?php if($pagination != ""): ?>
            <ul class="pagination">
                <?php echo $pagination; ?>
            </ul>
            <?php endif; ?>

        </div>
    </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>
<!-- END PAGE CONTENT-->