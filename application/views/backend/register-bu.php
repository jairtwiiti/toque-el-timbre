<?php
$path_assets = base_url() . "assets/backend/";
?>
<!DOCTYPE html>
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 3.3.0
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title>Registrarse</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $path_assets; ?>global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $path_assets; ?>global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $path_assets; ?>global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $path_assets; ?>global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="<?php echo $path_assets; ?>admin/pages/css/login.css" rel="stylesheet" type="text/css"/>
    <!-- END PAGE LEVEL SCRIPTS -->
    <!-- BEGIN THEME STYLES -->
    <link href="<?php echo $path_assets; ?>global/css/components.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $path_assets; ?>global/css/plugins.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $path_assets; ?>admin/css/layout.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $path_assets; ?>admin/css/themes/grey.css" rel="stylesheet" type="text/css" id="style_color"/>
    <link href="<?php echo $path_assets; ?>admin/css/custom.css" rel="stylesheet" type="text/css"/>
    <!-- END THEME STYLES -->
    <link rel="shortcut icon" href="<?php echo $path_assets; ?>img/icons/favicon.ico"/>
    <style>
        .login .alert p{
            margin-bottom: 0 !important;
        }
    </style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login">
    <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
    <div class="menu-toggler sidebar-toggler">
    </div>
    <!-- END SIDEBAR TOGGLER BUTTON -->
    <!-- BEGIN LOGO -->
    <div class="logo">
        <a href="<?php echo base_url(); ?>">
            <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="Toqueeltimbre.com" title="Toqueeltimbre.com" />
        </a>
    </div>
    <!-- END LOGO -->
    <!-- BEGIN LOGIN -->
    <div class="content">


    <!-- BEGIN REGISTRATION FORM -->
    <form class="register-form" action="<?php echo base_url(); ?>registrarse" method="post">
        <h3 style="margin-bottom: 12px;">Registrarse</h3>

        <?php if(@$error_mail){ echo '<div class="alert alert-danger"><button class="close" data-close="alert"></button><span>'. $error_mail .'</span></div>'; } ?>
        <?php if(@$error_captcha){ echo '<div class="alert alert-danger"><button class="close" data-close="alert"></button><span>'. $error_captcha .'</span></div>'; } ?>
        <?php if($_GET['error'] == "facebook"){ echo '<div class="alert alert-danger"><button class="close" data-close="alert"></button><span>No hemos podido obtener su email de su cuenta facebook, esto puede deberse a una configuracion de seguridad en su cuenta. Por favor intente registrarse de la forma normal.</span></div>'; } ?>
        <?php if($_GET['error'] == "google"){ echo '<div class="alert alert-danger"><button class="close" data-close="alert"></button><span>No hemos podido obtener su email de su cuenta google, esto puede deberse a una configuracion de seguridad en su cuenta. Por favor intente registrarse de la forma normal.</span></div>'; } ?>
        <?php if(@$ok_registro){ echo '<div class="alert alert-success"><span>'. $ok_registro .'</span></div>'; }else{ ?>


        <div class="login-options row" style="margin-bottom: 10px;">
            <div class="col-md-12">
                <p class="hint">
                    Ingrese sus datos en el siguiente formulario:
                </p>
            </div>
        </div>

        <div class="form-group">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9">Email</label>
            <input class="form-control placeholder-no-fix" type="text" placeholder="Correo electrónico" name="email" value="<?php echo !empty($_GET['email']) ? $_GET['email'] : @$_POST['email']; ?>"/>
        </div>

        <div class="panel_group_password" style="<?php echo (!empty($_GET['uid']) || !empty($_POST['uid'])) ? 'display:none;' : ''; ?>">
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9">Contraseña</label>
                <input class="form-control placeholder-no-fix" type="password" autocomplete="off" id="register_password" placeholder="Contraseña" name="password"/>
            </div>
        </div>

        <div class="form-group margin-top-20 margin-bottom-10">
            <label class="check">
                <input type="checkbox" name="tnc"/> Acepto los <a href="<?= base_url()?>terminos-y-condiciones" target="_blank">Terminos y Condiciones</a>
            </label>
            <div id="register_tnc_error"></div>
        </div>

        <div class="login-options row" style="margin-bottom: 10px;">
            <div class="col-md-12">
                <p class="hint" style="margin-top: 0px;">
                    O Registrarse via redes sociales
                </p>
            </div>
        </div>

        <div class="row" style="margin-bottom: 20px;">
            <div class="col-xs-12 col-sm-12 col-md-6 margin-bottom-10">
                <a href="<?php echo base_url(); ?>auth/facebook" class="btn btn-success" style="border-radius: 5px !important; background-color: #47639e !important; font-size: 12px; width: 100%;"><i class="fa fa-facebook" aria-hidden="true"></i> Ingresar con Facebook</a>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 margin-bottom-10">
                <a href="<?php echo base_url(); ?>auth/google" class="btn btn-success" style="border-radius: 5px !important; background-color: #DD4C3B !important; font-size: 12px; width: 100%;"><i class="fa fa-google-plus" aria-hidden="true"></i> Ingresar con Google</a>
            </div>
        </div>

        <div class="form-group">
            <?php echo $widget;?>
            <?php echo $script;?>
        </div>

        <div class="form-group">
            <div class="">
                <input type="hidden" name="uid" value="<?php echo !empty($_GET['uid'])? $_GET['uid'] : @$_POST['uid']; ?>" />
                <input type="hidden" name="social" value="<?php echo !empty($_GET['social'])? $_GET['social'] : @$_POST['social']; ?>" />
                <button type="submit" id="register-submit-btn" class="btn btn-success uppercase" style="border-radius: 5px !important;"><i class="fa fa-user" aria-hidden="true"></i> Registrarme</button>
            </div>
        </div>
        <?php } ?>
        <div class="create-account">
            <p>
                <a href="<?php echo base_url(); ?>login" class="uppercase">Login</a>
            </p>
        </div>
    </form>


    <!-- END REGISTRATION FORM -->
    </div>

    <div class="copyright">
        <?= date('Y')?>  &copy; Toqueeltimbre.com. Todos los derechos reservados.
    </div>
    <!-- END LOGIN -->


<style>
@media screen and (max-height: 575px){
    #rc-imageselect, .g-recaptcha {transform:scale(0.77);-webkit-transform:scale(0.77);transform-origin:0 0;-webkit-transform-origin:0 0;
}
</style>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?php echo $path_assets; ?>global/plugins/respond.min.js"></script>
<script src="<?php echo $path_assets; ?>plugins/excanvas.min.js"></script>
<![endif]-->
<script src="<?php echo $path_assets; ?>global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo $path_assets; ?>global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<script src="<?php echo $path_assets; ?>global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo $path_assets; ?>global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo $path_assets; ?>global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo $path_assets; ?>global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo $path_assets; ?>global/scripts/metronic.js" type="text/javascript"></script>
<script src="<?php echo $path_assets; ?>admin/scripts/layout.js" type="text/javascript"></script>
<script src="<?php echo $path_assets; ?>admin/pages/scripts/login.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<script>
    jQuery(document).ready(function() {
        Metronic.init(); // init metronic core components
        Layout.init(); // init current layout

        jQuery.validator.addMethod('mypassword', function(value, element) {
                return this.optional(element) || (value.match(/[a-zA-Z]/) && value.match(/[0-9]/));
            },
            'Su contraseña debe contener almenos un numero y/o un caracter albatetico.');


        $("#type_user").change(function() {
            var valor = $(this).val();
            if(valor == "Inmobiliaria"){
                $('.panel_group_inmobiliaria').show();
            }else{
                $('.panel_group_inmobiliaria').hide();
            }
        });

          
            
        $('.register-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {

                /*first_name: {
                    required: true
                },
                last_name: {
                    required: true
                },*/
                email: {
                    required: true,
                    email: true,
                    remote: '<?php echo base_url(); ?>ajax/validate_email'
                },
                /*city: {
                    required: true
                },
                phone: {
                    required: true,
                    number: true
                },

                type: {
                    required: true
                },*/
                <?php if(empty($_GET['uid']) && empty($_POST['uid'])): ?>
                password: {
                    required: true,
                    minlength: 6,
                    maxlength: 16,
                    mypassword : true
                },
                /*repassword: {
                    equalTo: "#register_password",
                    minlength: 6,
                    maxlength: 16,
                    mypassword: true
                },*/
                <?php endif; ?>
                tnc: {
                    required: true
                }
            },

            messages: { // custom messages for radio buttons and checkboxes
                first_name: {
                    required: "Ingrese su nombre"
                },
                last_name: {
                    required: "Ingrese su apellido"
                },
                email: {
                    required: "Ingrese su email",
                    email: "Ingrese un email valido",
                    remote: "El email ya esta registrado"
                },
                city: {
                    required: "Seleccione una ciudad"
                },
                phone: {
                    required: "Ingrese un numero de telefono",
                    number: "Debe ingresar datos numericos"
                },

                type: {
                    required: "Seleccione el tipo de usuario"
                },
                <?php if(empty($_GET['uid']) && empty($_POST['uid'])): ?>
                password: {
                    required: "Ingrese una contraseña",
                    minlength: "Su contraseña debe contener numeros y/o letras, y un minimo de 6 caracteres",
                    maxlength: "Contraseña demaciado larga",
                    alphanumeric: "Solamente Letras, numeros o Lineas abajo"
                },
                repassword: {
                    equalTo: "Las contraseñas deben coincidir",
                    minlength: "Su contraseña debe contener numeros y/o letras, y un minimo de 6 caracteres",
                    maxlength: "Contraseña demaciado larga",
                    alphanumeric: "Solamente Letras, numeros o Lineas abajo"
                },
                <?php endif; ?>
                tnc: {
                    required: "Por favor acepte los terminos y condiciones."
                }
            },

            invalidHandler: function(event, validator) { //display error alert on form submit

            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function(error, element) {
                if (element.attr("name") == "tnc") { // insert checkbox errors after the container
                    error.insertAfter($('#register_tnc_error'));
                } else if (element.closest('.input-icon').size() === 1) {
                    error.insertAfter(element.closest('.input-icon'));
                } else {
                    error.insertAfter(element);
                }
            },

            submitHandler: function(form) {
                form.submit();
            }
        });



    });
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>