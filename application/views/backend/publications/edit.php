<?php if(@validation_errors()){ echo '<div class="alert alert-danger">'.@validation_errors().'</div>'; } ?>
<?php
$path_assets = base_url() . "assets/backend/";
$inm_id = $this->uri->segment(4);
$pub_id = $this->uri->segment(5);
?>
<!-- BEGIN PAGE CONTENT-->
<div class="row">
<div class="col-md-12">
    <form class="form-horizontal form-row-seperated" action="<?php echo base_url(); ?>dashboard/publication/edit/<?php echo $inm_id; ?>/<?php echo $pub_id; ?>" method="post" enctype="multipart/form-data">
    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-basket font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">Editar Anuncio </span>
            </div>
            <div class="actions btn-set">
            </div>
        </div>
        <div class="portlet-body">
            <div class="tabbable">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#tab_general" data-toggle="tab">Informacion </a>
                    </li>
                    <li>
                        <a href="#tab_images" data-toggle="tab">Images </a>
                    </li>
                </ul>
                <div class="tab-content no-space">
                    <div class="tab-pane active" id="tab_general">
                        <div class="form-body">
                            <div class="form-group">
                                <label class="col-md-2 control-label">Titulo: <span class="required">* </span>
                                </label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="title" value="<?php echo $inmueble->inm_nombre; ?>" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Categoria: <span class="required">* </span>
                                </label>
                                <div class="col-md-2">
                                    <select id="categories" class="table-group-action-input form-control input-medium" name="category" rel="<?php echo base_url(); ?>ajax/features" required>
                                        <option value="">Seleccionar...</option>
                                        <?php foreach($categories as $cat): ?>
                                            <option value="<?php echo $cat['cat_id']; ?>" <?php echo $cat['cat_id'] == $inmueble->inm_cat_id ? 'selected':''; ?>><?php echo $cat['cat_nombre']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Descripcion: </label>
                                <div class="col-md-10">
                                    <textarea class="form-control" name="description" rows="8"><?php echo $inmueble->inm_detalle; ?></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Departamento: <span class="required">* </span>
                                </label>
                                <div class="col-md-2">
                                    <select id="state" class="table-group-action-input form-control input-medium" name="state" rel="<?php echo base_url(); ?>ajax/" required>
                                        <option value="">Seleccionar...</option>
                                        <?php foreach($states as $state): ?>
                                            <option value="<?php echo $state['dep_id']; ?>" <?php echo $state['dep_id'] == $inmueble->dep_id ? 'selected':''; ?>><?php echo $state['dep_nombre']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Ciudad: <span class="required">* </span>
                                </label>
                                <div class="col-md-2">
                                    <select id="city" class="table-group-action-input form-control input-medium" name="city" required>
                                        <option value="">Seleccionar...</option>
                                        <?php foreach($cities as $city): ?>
                                            <option value="<?php echo $city->ciu_id; ?>" <?php echo $city->ciu_id == $inmueble->inm_ciu_id ? 'selected':''; ?>><?php echo $city->ciu_nombre; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Zona: <span class="required">* </span>
                                </label>
                                <div class="col-md-2">
                                    <select id="zone" class="table-group-action-input form-control input-medium" name="zone" required>
                                        <option value="">Seleccionar...</option>
                                        <?php foreach($zones as $zone): ?>
                                            <option value="<?php echo $zone->zon_id; ?>" <?php echo $zone->zon_id == $inmueble->inm_zon_id ? 'selected':''; ?>><?php echo $zone->zon_nombre; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-md-2 control-label">Direccion:<span class="required">* </span>
                                </label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="address" required value="<?php echo $inmueble->inm_direccion; ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label"></label>
                                <div class="col-md-10" style="color: #e02222;">
                                    <label id="check_mapa"><input type="checkbox" class="form-control" name="marcar_mapa" <?php echo empty($inmueble->inm_latitud) && empty($inmueble->inm_longitud) ? 'checked': ''; ?> /> Haga clic en el mapa para marcar la ubicación de su inmueble.</label>
                                    <strong><span class="map-error-message"></span></strong>
                                </div>
                            </div>

                            <div class="form-group" id="marcar_mapa_div" style="<?php echo empty($inmueble->inm_latitud) && empty($inmueble->inm_longitud) ? 'display: none;': ''; ?>">
                                <label class="col-md-2 control-label">Ubicacion:
                                </label>
                                <div class="col-md-10">
                                    <!-- BEGIN GEOCODING PORTLET-->
                                    <div class="light">
                                        <div class="portlet-body">
                                            <div class="input-group">
                                                <input type="text" class="form-control" id="gmap_geocoding_address" placeholder="Direccion...">
                                                        <span class="input-group-btn">
                                                            <button class="btn blue" id="gmap_geocoding_btn"><i class="fa fa-search"></i>
                                                        </span>
                                            </div>
                                            <input id="latitude" type="hidden" name="latitude" value="<?php echo !empty($inmueble->inm_latitud) ? $inmueble->inm_latitud : ''; ?>" />
                                            <input id="longitude" type="hidden" name="longitude" value="<?php echo !empty($inmueble->inm_longitud) ? $inmueble->inm_longitud : ''; ?>" />
                                            <div id="gmap_geocoding" class="gmaps" data-latitud="-17.784559" data-longitud="-63.1590514" data-city="Santa Cruz de la Sierra" data-marker-latitud="<?php echo $inmueble->inm_latitud; ?>" data-marker-longitud="<?php echo $inmueble->inm_longitud; ?>"></div>
                                        </div>
                                    </div>
                                    <!-- END GEOCODING PORTLET-->
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Tipo: <span class="required">* </span>
                                </label>
                                <div class="col-md-2">
                                    <select class="table-group-action-input form-control input-medium" name="type" required>
                                        <option value="">Seleccionar...</option>
                                        <?php foreach($types as $tip): ?>
                                            <option value="<?php echo $tip['for_id']; ?>" <?php echo $tip['for_id'] == $inmueble->inm_for_id ? 'selected':''; ?>><?php echo $tip['for_descripcion']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">UV:
                                </label>
                                <div class="col-md-2">
                                    <input type="text" class="form-control" name="uv" value="<?php echo $inmueble->inm_uv; ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Manzano:
                                </label>
                                <div class="col-md-2">
                                    <input type="text" class="form-control" name="manzano" value="<?php echo $inmueble->inm_manzano; ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Superficie: <span class="required">* </span>
                                </label>
                                <div class="col-md-2">
                                    <input type="number" class="form-control" name="area" required value="<?php echo $inmueble->inm_superficie; ?>">
                                </div>
                                <div class="col-md-3">
                                    <select class="table-group-action-input form-control input-medium" name="type_area">
                                        <option value="Metros Cuadrados" <?php echo $inmueble->inm_tipo_superficie == 'Metros Cuadrados' ? 'selected':''; ?>>Metros Cuadrados</option>
                                        <option value="Hectareas" <?php echo $inmueble->inm_tipo_superficie == 'Hectareas' ? 'selected':''; ?>>Hectareas</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Precio: <span class="required">* </span>
                                </label>
                                <div class="col-md-2">
                                    <input type="text" class="form-control" name="price" required value="<?php echo round($inmueble->inm_precio, 0); ?>">
                                </div>
                                <div class="col-md-3">
                                    <select class="table-group-action-input form-control input-medium" name="currency" required>
                                        <option value="">Seleccionar...</option>
                                        <?php foreach($currency as $cur): ?>
                                            <option value="<?php echo $cur['mon_id']; ?>" <?php echo ($cur['mon_id'] == $inmueble->inm_mon_id || $cur['mon_id'] == 2) ? 'selected':''; ?>><?php echo $cur['mon_descripcion']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <!-- /.modal -->
                            <div id="responsive" class="modal fade" tabindex="-1" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form method="post" action="<?php echo base_url(); ?>dashboard/publication/register_form/<?php echo $pub_id; ?>">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                <h4 class="modal-title">Inmueble Vendido?</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="scroller" style="height:300px" data-always-visible="1" data-rail-visible1="1">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <h4>Pregunta 1</h4>
                                                            <p style="height: 35px;">
                                                                <input type="text" class="col-md-12 form-control">
                                                            </p>
                                                            <h4>Pregunta 1</h4>
                                                            <p style="height: 35px;">
                                                                <input type="text" class="col-md-12 form-control">
                                                            </p>
                                                            <h4>Pregunta 1</h4>
                                                            <p style="height: 35px;">
                                                                <input type="text" class="col-md-12 form-control">
                                                            </p>
                                                            <h4>Pregunta 1</h4>
                                                            <p style="height: 35px;">
                                                                <input type="text" class="col-md-12 form-control">
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" data-dismiss="modal" class="btn default">Cerrar</button>
                                                <button type="button" class="btn green">Guardar</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade" id="ajax" role="basic" aria-hidden="true">
                                <div class="page-loading page-loading-boxed">
                                    <img src="<?=base_url("assets/backend/global/img/loading-spinner-grey.gif")?>" alt="" class="loading">
										<span>
										&nbsp;&nbsp;Loading... </span>
                                </div>
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                    </div>
                                </div>
                            </div>
                            <!-- /.modal -->






                            <div class="divider"></div>
                            <h2>Caracteristicas</h2>


                            <div id="caracteristicas">
                                <input type="hidden" name="sw_caracteristica" value="true" />
                                <?php
                                foreach($features as $key_car_id=>$feature):
                                    foreach($feature as $key=>$value):
                                        if($key == 'select'):
                                ?>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label"><?php echo $value['descripcion']; ?>:
                                            </label>
                                            <div class="col-md-2">
                                                <select class="table-group-action-input form-control input-medium" name="features[<?php echo $key_car_id; ?>]">
                                                    <?php $auxiliar = $value['input_values']; ?>
                                                    <?php foreach($auxiliar as $item): ?>
                                                        <option value="<?php echo $item->id; ?>" <?php echo strtolower($feature_values[$key_car_id]) == strtolower($item->nombre) ? 'selected':''; ?>><?php echo ucwords(strtolower($item->nombre)); ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>

                                <?php
                                        endif;
                                        if($key == 'input'):
                                ?>
                                            <div class="form-group">
                                                <label class="col-md-2 control-label"><?php echo $value['descripcion']; ?>:
                                                </label>
                                                <div class="col-md-2">
                                                    <input type="<?=$key_car_id=="11"?"number":"text"?>" class="form-control" name="features[<?php echo $key_car_id; ?>]" value="<?php echo $feature_values[$key_car_id]; ?>">
                                                </div>
                                            </div>
                                <?php
                                        endif;
                                    endforeach;
                                endforeach;
                                ?>
                            </div>

                            <br />
                            <div class="form-group margiv-top-10">
                                <div class="col-md-2"></div>
                                <div class="col-md-10">
                                    <button class="btn blue property-save">Guardar Cambios</button>
                                    <a class="btn default" href="<?php echo base_url(); ?>dashboard">Cancelar</a>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="tab-pane" id="tab_images">
                        <div id="tab_images_uploader_container" class="text-align-reverse margin-bottom-10">
                            <a id="tab_images_uploader_pickfiles" href="javascript:;" class="btn yellow">
                                <i class="fa fa-plus"></i> Seleccionar Imagen </a>
                            <!--<a id="tab_images_uploader_uploadfiles" href="javascript:;" class="btn green">
                                <i class="fa fa-share"></i> Upload Files </a>-->
                        </div>
                        <div class="row">
                            <div id="tab_images_uploader_filelist" class="col-md-6 col-sm-12"></div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover" width="100%">
                                <thead>
                                <tr role="row" class="heading">
                                    <th width="10%">
                                        Image
                                    </th>
                                    <th width="40%">
                                        Label
                                    </th>
                                    <th width="8%">
                                        Sort Order
                                    </th>
                                    <th width="10%">
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if(!empty($inmueble_fotos)): ?>
                                    <?php foreach($inmueble_fotos as $foto): ?>
                                        <tr>
                                            <td>
                                                <!--
                                                <a href="<?php echo base_url(); ?>admin/imagenes/inmueble/<?php echo $foto->fot_archivo; ?>" class="fancybox-button" data-rel="fancybox-button">
                                                    <img class="img-responsive" src="<?php echo base_url(); ?>admin/imagenes/inmueble/<?php echo $foto->fot_archivo; ?>" alt="">
                                                </a> -->
                                                <?php
                                                $imageX = $foto->fot_archivo;
                                                $fileimage = BASE_DIRECTORY . DS . "admin" . DS . "imagenes" . DS . "inmueble" . DS . $imageX;

                                                if (!empty($imageX) && !is_null($imageX) && file_exists($fileimage)) { ?>
                                                    <a href="<?php echo base_url(); ?>admin/imagenes/inmueble/<?php echo $foto->fot_archivo; ?>" class="fancybox-button" data-rel="fancybox-button">
                                                        <img class="img-responsive" src="<?php echo base_url(); ?>admin/imagenes/inmueble/<?php echo $foto->fot_archivo; ?>" alt="">
                                                    </a>
                                                <?php } else { ?>
                                                    <a href="<?php echo $path_assets . 'img/default.png'?>" class="fancybox-button" data-rel="fancybox-button">
                                                        <img class="img-responsive" src="<?php echo $path_assets.'img/default.png'?>" alt="foto archivo" title="default toqueeltimbre">
                                                    </a>
                                                <?php } ?>

                                            </td>
                                            <td>
                                                <input type="text" class="form-control" name="images[description][<?php echo $foto->fot_id ?>]" value="<?php echo $foto->fot_descripcion; ?>" placeholder="Descripcion de la Imagen">
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" name="images[sort_order][<?php echo $foto->fot_id ?>]" value="<?php echo $foto->fot_order; ?>">
                                            </td>
                                            <td>
                                                <div href="<?php echo base_url(); ?>ajax/delete_image" rel="<?php echo $foto->fot_id ?>" class="btn red btn-sm delete_image">
                                                    <i class="fa fa-times"></i> Eliminar
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                                </tbody>
                            </table>
                        </div>

                        <br />
                        <div class="form-group margiv-top-10">
                            <div class="col-md-10">
                                <button class="btn blue property-save">Guardar Cambios</button>
                                <a class="btn default" href="<?php echo base_url(); ?>dashboard/publications">Cancelar</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    </form>
</div>
</div>
<!-- END PAGE CONTENT-->