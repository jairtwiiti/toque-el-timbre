<?php
foreach($features as $feature):
    foreach($feature as $key=>$value):
        if($key == 'select'):
            ?>
            <div class="form-group">
                <label class="col-md-2 control-label"><?php echo $value['descripcion']; ?>:
                </label>
                <div class="col-md-2">
                    <select class="table-group-action-input form-control input-medium" name="<?php echo $value['input_name']; ?>">
                        <?php $auxiliar = $value['input_values']; ?>
                        <?php foreach($auxiliar as $item): ?>
                            <option value="<?php echo $item->id; ?>"><?php echo ucwords(strtolower($item->nombre)); ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>

        <?php
        endif;
        if($key == 'input'):
            ?>
            <div class="form-group">
                <label class="col-md-2 control-label"><?php echo $value['descripcion']; ?>:
                </label>
                <div class="col-md-2">
                    <input type="text" class="form-control" name="<?php echo $value['input_name']; ?>">
                </div>
                <?php if($value['descripcion'] == "Superficie Construida"): ?>
                    <label class="col-md-2 control-label" style="text-align: left;">Metros Cuadrados</label>
                <?php endif; ?>
            </div>
        <?php
        endif;
    endforeach;
endforeach;
?>