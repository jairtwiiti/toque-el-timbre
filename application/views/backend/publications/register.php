<?php if(@validation_errors()){ echo '<div class="alert alert-danger">'.@validation_errors().'</div>'; } ?>
<?php
$path_assets = base_url() . "assets/backend/";
?>
<!-- BEGIN PAGE CONTENT-->
<div class="row">
<div class="col-md-12">
    <form class="form-horizontal form-row-seperated" action="<?php echo base_url(); ?>dashboard/publication/register" method="post" enctype="multipart/form-data">
    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-basket font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">Nuevo Anuncio </span>
            </div>
        </div>
        <div class="portlet-body">
            <div class="tabbable">

                        <div class="form-body">
	                        <div class="row">
		                        <div class="col-xs-12 col-sm-8 col-md-9 col-lg-10">

		                            <div class="form-group">
		                                <label class="col-md-2 control-label">Titulo: <span class="required">* </span>
		                                </label>
		                                <div class="col-md-10">
		                                    <input id="input-title" type="text" class="form-control" name="title" required maxlength="90" value="<?php echo $_POST['title']; ?>">
		                                </div>
		                            </div>

		                            <div class="form-group">
		                                <label class="col-md-2 control-label">Categoria: <span class="required">* </span>
		                                </label>
		                                <div class="col-md-6">
		                                    <select id="categories" class="table-group-action-input form-control input-medium" name="category" rel="<?php echo base_url(); ?>ajax/features" required>
		                                        <option value="">Seleccionar...</option>
		                                        <?php foreach($categories as $cat): ?>
		                                        <option value="<?php echo $cat['cat_id']; ?>" <?php echo $_POST['category'] == $cat['cat_id'] ? 'selected' : ''; ?>><?php echo $cat['cat_nombre']; ?></option>
		                                        <?php endforeach; ?>
		                                    </select>
		                                </div>
		                            </div>

			                        <div class="form-group">
				                        <label class="col-md-2 control-label">Tipo: <span class="required">* </span>
				                        </label>
				                        <div class="col-md-6">
					                        <select id="type-select" class="table-group-action-input form-control input-medium" name="type" required>
						                        <option value="">Seleccionar...</option>
						                        <?php foreach($types as $tip): ?>
							                        <option value="<?php echo $tip['for_id']; ?>" <?php echo $_POST['type'] == $tip['for_id'] ? 'selected' : ''; ?>><?php echo $tip['for_descripcion']; ?></option>
						                        <?php endforeach; ?>
					                        </select>
				                        </div>
			                        </div>

		                            <div class="form-group">
		                                <label class="col-md-2 control-label">Descripcion: <span class="required">* </span></label>
		                                <div class="col-md-10">
		                                    <textarea class="form-control" name="description" rows="8" required><?php echo $_POST['description']; ?></textarea>
		                                </div>
		                            </div>

		                        </div>
		                        <div class="col-xs-12 col-sm-4 col-md-3 col-lg-2">
                                    <!--<div style="background: deepskyblue; width: 100%; height: 320px;" >

                                    </div>
			                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">-->
                                    <div class="thumbnail" style="padding:0; margin:0">
                                      <!-- <img src="<?php // echo base_url(); ?>librerias/timthumb.php?src=<?php // echo base_url(); ?>assets/img/default.png&w=250&h=170" alt="default" title="default toqueeltimbre" > -->
                                      <img src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo base_url(); ?>assets/img/default.png&h=180" alt="default" title="default toqueeltimbre" >
                                      <div class="caption">
                                        <h4 style="font-weight:bold; color: #626467;"> <span id="categoria-preview">CATEGORIA</span>&nbsp;<span id="tipo-preview"></span> </h4>
                                        <p id="description-preview"> Escribe en el titulo del anuncio...</p>                                        
                                      </div>
                                    </div>
			                        <!-- </div> -->
		                        </div>
	                        </div>
                            <div class="form-group">
                                <div>
                                    <label class="col-md-2 control-label"></label>
                                    <div class="col-md-10" style="margin-bottom: 8px; color: #e02222;">
                                        Las fotos deben ser horizontales para una mejor visualizacion en la pagina.
                                    </div>
                                </div>
                                <div>
                                    <label class="col-md-2 control-label">Images: <span class="required">* </span>
                                    </label>
                                    <div class="col-md-10">

                                        <div id="tab_images_uploader_container" class="text-align-reverse margin-bottom-10" style="text-align: left;">
                                            <a id="tab_images_uploader_pickfiles" href="javascript:;" class="btn yellow">
                                                <i class="fa fa-plus"></i> Subir Imagenes </a>
                                            <!--<a id="tab_images_uploader_uploadfiles" href="javascript:;" class="btn green">
                                                <i class="fa fa-share"></i> Subir Imagenes </a>-->
                                        </div>
                                        <div class="row">
                                            <div id="tab_images_uploader_filelist" class="col-md-6 col-sm-12"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Departamento: <span class="required">* </span>
                                </label>
                                <div class="col-md-2">
                                    <select id="state" class="table-group-action-input form-control input-medium" name="state" rel="<?php echo base_url(); ?>ajax/" required>
                                        <option value="">Seleccionar...</option>
                                        <?php foreach($states as $state): ?>
                                            <option value="<?php echo $state['dep_id']; ?>"><?php echo $state['dep_nombre']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Ciudad: <span class="required">* </span>
                                </label>
                                <div class="col-md-2">
                                    <select id="city" class="table-group-action-input form-control input-medium" name="city" required>
                                        <option value="">Seleccionar...</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Zona: <span class="required">* </span>
                                </label>
                                <div class="col-md-2">
                                    <select id="zone" class="table-group-action-input form-control input-medium" name="zone" required>
                                        <option value="">Seleccionar...</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Direccion: <span class="required">* </span>
                                </label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" required name="address" value="<?php echo $_POST['address']; ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label"></label>
                                <div class="col-md-10" style="color: #e02222;">
                                    <label id="check_mapa"><input type="checkbox" class="form-control" name="marcar_mapa" /> Haga clic en el mapa para marcar la ubicación de su inmueble.</label>
                                    <strong><span class="map-error-message"></span></strong>
                                </div>
                            </div>

                            <div class="form-group" id="marcar_mapa_div" style="display: none;">
                                <label class="col-md-2 control-label">Ubicacion:
                                </label>
                                <div class="col-md-10">
                                        <!-- BEGIN GEOCODING PORTLET-->
                                        <div class="light">
                                            <div class="portlet-body">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="gmap_geocoding_address" placeholder="Direccion...">
                                                        <span class="input-group-btn">
                                                            <button class="btn blue" id="gmap_geocoding_btn"><i class="fa fa-search"></i>
                                                        </span>
                                                </div>
                                                <input id="latitude" type="hidden" name="latitude" />
                                                <input id="longitude" type="hidden" name="longitude" />
                                                <div id="gmap_geocoding" class="gmaps" data-latitud="-17.784559" data-longitud="-63.1590514" data-city="Santa Cruz" data-marker-longitud="" data-marker-latitud=""></div>
                                            </div>
                                        </div>
                                        <!-- END GEOCODING PORTLET-->
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">UV:
                                </label>
                                <div class="col-md-2">
                                    <input type="text" class="form-control" name="uv" value="<?php echo $_POST['uv']; ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Manzano:
                                </label>
                                <div class="col-md-2">
                                    <input type="text" class="form-control" name="manzano" value="<?php echo $_POST['manzano']; ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Superficie: <span class="required">* </span>
                                </label>
                                <div class="col-md-2">
                                    <input type="number" class="form-control" name="area" required value="<?php echo $_POST['area']; ?>">
                                </div>
                                <div class="col-md-2">
                                    <select class="table-group-action-input form-control input-medium" name="type_area">
                                        <option value="Metros Cuadrados" <?php echo $_POST['type_area'] == "Metros Cuadrados" ? 'selected' : ''; ?>>Metros Cuadrados</option>
                                        <option value="Hectareas" <?php echo $_POST['type_area'] == "Hectareas" ? 'selected' : ''; ?>>Hectareas</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Precio: <span class="required">* </span>
                                </label>
                                <div class="col-md-2">
                                    <input type="text" class="form-control" name="price" required value="<?php echo $_POST['price']; ?>">
                                </div>
                                <div class="col-md-2">
                                    <select class="table-group-action-input form-control input-medium" name="currency" required>
                                        <option value="">Seleccionar...</option>
                                        <?php foreach($currency as $cur): ?>
                                            <option value="<?php echo $cur['mon_id']; ?>" <?php echo ($_POST['currency'] == $cur['mon_id'] || $cur['mon_id'] == 2) ? 'selected' : ''; ?>><?php echo $cur['mon_descripcion']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <span class="help-inline" style="color:red;">El precio debe ingresarse sin puntos (.) ni comas (,) Ej: 590500</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Mostrar Anuncio: <span class="required">* </span>
                                </label>
                                <div class="col-md-2">
                                    <select class="table-group-action-input form-control input-medium" name="status" required>
                                        <option value="Si" <?php echo $_POST['status'] == "Si" ? 'selected' : ''; ?>>SI</option>
                                        <option value="No" <?php echo $_POST['status'] == "No" ? 'selected' : ''; ?>>NO</option>
                                    </select>
                                </div>
                            </div>

                            <br />
                            <h2>Caracteristicas</h2>
                            <div id="caracteristicas"><?php echo $features; ?></div>

                            <br />
                            <div class="form-group margiv-top-10">
                                <div class="col-md-2"></div>
                                <div class="col-md-10">
                                    <button class="btn blue property-save">Guardar Cambios</button>
                                    <a class="btn default" href="<?php echo base_url(); ?>dashboard">Cancelar</a>
                                </div>
                            </div>

                        </div>
                    </div>

        </div>
    </div>
    </form>
</div>
</div>
<!-- END PAGE CONTENT-->
