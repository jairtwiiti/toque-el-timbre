<?php
$action = $_GET['action'];
$error = $_GET['error'];
$meansOfSalesArray = array(
    model_survey::TOQUE_EL_TIMBRE => "Toque el timbre",
    model_survey::PRINT_MEANS => "Medios impresos",
    model_survey::DIGITAL_MEANS => "Medios digitales",
    model_survey::REFERRED => "Referidos",
    model_survey::SING_BOARDS => "Letreros",
    model_survey::OTHERS => "Otros"
);
?>
<!-- BEGIN PAGE CONTENT-->
<div class="row">
<div class="col-md-12">
    <div class="tmp_message">
        <?php
        $this->load->view("flash-data-basic-messages")
        ?>
    </div>
    <?php if($action == 'register'): ?>
        <div class="alert alert-success">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
            <strong>Registrado correctamente!</strong> se ha registrado un nuevo anuncio.
        </div>
    <?php endif; ?>
    <?php if($action == 'update'): ?>
        <div class="alert alert-success">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
            <strong>Actualizado correctamente!</strong> se ha actualizado un anuncio.
        </div>
    <?php endif; ?>
    <?php if($action == 'delete'): ?>
        <div class="alert alert-danger">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
            <strong>Eliminado correctamente!</strong> se ha eliminado un anuncio.
        </div>
    <?php endif; ?>
    <?php if($error == 'limit'): ?>
        <div class="alert alert-warning">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
            <strong>Advertencia:</strong> solo puede tener <?php echo $limit_publications; ?> anuncios publicados en la pagina.
        </div>
    <?php endif; ?>


    <!-- BEGIN EXAMPLE TABLE PORTLET-->
    <div class="portlet box grey-cascade">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-globe"></i>Mis Anuncios
        </div>
        <div class="tools">
        </div>
    </div>

    <div class="portlet-body">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-3 col-ms-3 col-xs-4 col-mobile-1">
                    <div class="btn-group pull-right">
                        <a href="<?=base_url("admin/Property/add")?>" id="sample_editable_1_new" class="btn green">
                            Nuevo Anuncio <i class="fa fa-plus"></i>
                        </a>
                    </div>
                </div>
                <div class="col-md-6 col-ms-6 col-xs-4 col-mobile-1">
	                <form id="form-search-publications" class="form-inline pull-right" action="<?php echo base_url(); ?>dashboard/publications/" method="post">
		                <div class="input-group input-medium">
			                <input type="text" name="searchPublication" class="form-control" placeholder="Buscar Inmueble..." value="<?= $_POST['searchPublication'] ?>">
			                <input type="hidden" name="page" value="1" />
							<span class="input-group-btn">
							    <button type="submit" class="btn green"><i class="fa fa-search"></i></button>
                            </span>
		                </div>
	                </form>
                </div>
                <div class="col-md-3 col-ms-3 col-xs-4 col-mobile-1">
                    <div class="btn-group pull-right">
                        <button class="btn dropdown-toggle" data-toggle="dropdown">Herramientas <i class="fa fa-angle-down"></i></button>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a href="#" id="printPublicationList"> Imprimir </a>
                            </li>
                            <li>
                                <a href="<?= base_url(); ?>dashboard/publications/export_excel/?searchText=<?= $_POST['searchPublication']?>">Exportar a Excel </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover" id="tabla-publication">
            <thead>
            <tr>
                <th width="4%">ID</th>
                <th width="7%">Imagen</th>
                <th>Titulo</th>
                <th width="5%">Vistas</th>
                <th width="5%">Mensajes</th>
                <th>Fecha Fin</th>
                <th>Publicado</th>
                <th>Estado</th>
                <th>Opciones</th>
            </tr>
            </thead>
            <tbody>
            <?php if(count($publications) > 0){ ?>
                <?php foreach($publications as $inmueble): ?>
                    <tr class="odd gradeX">
                        <td>
                            <?php echo $inmueble->pub_id ?>
                        </td>
                        <td>
                            <?php
                            $fileHandler = new File_Handler($inmueble->imageInmueble);
                            $thumbnail = $fileHandler->getThumbnail(100,100);
                            ?>
                            <img src="<?=$thumbnail->getSource()?>"/>
                        </td>
                        <td>
                            <?php
                            $url_detalle = base_url("buscar/".seo_url($inmueble->cat_nombre . " EN ". $inmueble->for_descripcion." EN ".$inmueble->dep_nombre)."/".$inmueble->inm_seo);
                            ?>
                            <a href="<?php echo $url_detalle; ?>" target="_blank"><?php echo $inmueble->inm_nombre; ?></a>
                        </td>
                        <td>
                            <?php echo $inmueble->inm_visitas; ?>
                        </td>
                        <!--<td>
                            <?php echo $inmueble->inm_llamadas; ?>
                        </td>-->
                        <td>
                            <?php echo $inmueble->cantidad_mensajes; ?>
                        </td>
                        <td>
                            <?php echo date('d/m/Y', strtotime($inmueble->finish)); ?>
                        </td>
                        <td class="center">
                            <?php if(date('Y-m-d') > $inmueble->finish)
                                { ?>
                                <span class="label label-sm label-danger">Expirado</span>
                            <?php }else{ ?>
                                <div class="checkbox text-center">
                                    <label>
                                        <?php $checked = $inmueble->inm_publicado == "Si"?" checked":""?>
                                        <input class="deactivate-publication" <?=$checked?> data-publication-id="<?=$inmueble->pub_id?>" data-property-id="<?=$inmueble->inm_id?>" type="checkbox">
                                        <i class="fa fa-spinner fa-pulse fa-fw pull-left hide"style="margin-top: 3px;margin-right: 5px;"></i>
                                    </label>
                                </div>
                            <?php } ?>

                        </td>
                        <td class="center">
                            <?php
                                $expired = date('Y-m-d') > $inmueble->finish?TRUE:FALSE;
                                if($expired){ ?>
                                <?php if ($inmueble->hasPaymentPending == false && $inmueble->existPaymentPublication == false) { ?>
                                    <span class="label label-sm label-warning">Desactivado</span>
                                <?php }else{ ?>
                                    <span class="label label-sm label-danger">Expirado</span>
                                <?php } ?>
                            <?php }else{ ?>
                                <?php if($inmueble->pub_estado == 'Aprobado' && date('Y-m-d') <= $inmueble->finish): ?>
                                    <span class="label label-sm label-success">Aprobado</span>
                                <?php endif; ?>
                                <?php if($inmueble->pub_estado == 'No Aprobado'): ?>
                                    <span class="label label-sm label-info">Pendiente</span>
                                <?php endif; ?>
                                <?php if($inmueble->pub_estado == 'Cancelado'): ?>
                                    <span class="label label-sm label-danger">Cancelado</span>
                                <?php endif; ?>
                            <?php } ?>
                        </td>
                        <td style="text-align: center;">
                            <?php
                            $editUrl = base_url("admin/Property/edit/".$inmueble->inm_id);
                            ?>
                            <a class="btn btn-sm yellow" href="<?=$editUrl?>" title="Editar Anuncio"><i class="fa fa-edit"></i></a>
                            <?php if($expired): ?>
                                <a class="btn btn-sm blue" href="<?php echo base_url(); ?>dashboard/publication/renew/<?php echo $inmueble->pub_id; ?>" title="Renovar"><i class="fa fa-recycle"></i></a>
                            <?php endif; ?>
                            <a class="btn btn-sm red" href="<?php echo base_url(); ?>dashboard/publication/delete/<?php echo $inmueble->pub_id; ?>" title="Eliminar Anuncio"><i class="fa fa-trash-o"></i></a>

                            <?php if ($inmueble->isPaymentPublication == true && $inmueble->hasPaymentImprove == false) { ?>
                                <a class="btn btn-sm green" href="<?php echo base_url(); ?>dashboard/payment/<?php echo $inmueble->pub_id; ?>" title="Mejorar Anuncio">Mejorar Anuncio <i class="fa fa-bullhorn"></i></a>
                            <?php } ?>

                            <?php if ($inmueble->hasPaymentPending == false && $inmueble->existPaymentPublication == false) { ?>
                                <a class="btn btn-sm btn-circle btn-primary purchase" href="#" title="Pagar Anuncio" data-publication-id="<?=$inmueble->pub_id?>" >Mejorar anuncio</a>
<!--                                <a class="btn btn-sm btn-circle btn-primary" href="--><?php //echo base_url(); ?><!--dashboard/publications/pay/--><?php //echo $inmueble->pub_id; ?><!--" title="Pagar Anuncio">Mejorar anuncio</a>-->
                            <?php
                            }
                            $pendientes = $this->model_pagos->get_payment_pending_by_user($this->user_id, $inmueble->pub_id);
                            $pendientes = ($pendientes > 1) || $pendientes == 0 ? $pendientes . " pagos pendientes" : " 1 pago pendiente";
                            if($pendientes > 0)
                            {
                            ?>
                                <a class="btn btn-sm btn-circle btn-danger" href="<?=base_url("dashboard/payments?status=Pendiente&uid=".$inmueble->pub_id);?>" title="Pagar Anuncio">Pagos pendientes</a>
                            <?php
                            }
                            ?>


                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php }else{ ?>
                <tr class="odd gradeX">
                    <td colspan="10" style="text-align: center !important;">
                        No se ha encontrado resultados.
                    </td>
                </tr>
            <?php } ?>
            </tbody>
            </table>

            <?php if($pagination != ""): ?>
            <ul class="pagination">
                <?php echo $pagination; ?>
            </ul>
            <?php endif; ?>

        </div>
    </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>

<div id="print_publications_html" style="display:none;">
    <div style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 20px; padding:5px 10px;">
        <p style="margin-top:10px; text-align:center;">
            <a href="javascript:;" style="font-size:12px;" onclick="window.print();">Imprimir</a>
        </p>

        <h1>Mis Anuncios</h1>
        <style>
            .print-table-bordered {
                border: 1px solid #ddd;
            }

            .print-table-bordered>thead>tr>th {
                border: 1px solid #ddd;
                border-top-color: rgb(221, 221, 221);
                border-top-style: solid;
                border-top-width: 1px;
                border-right-color: rgb(221, 221, 221);
                border-right-style: solid;
                border-right-width: 1px;
                border-bottom-color: rgb(221, 221, 221);
                border-bottom-style: solid;
                border-bottom-width: 1px;
                border-left-color: rgb(221, 221, 221);
                border-left-style: solid;
                border-left-width: 1px;

                padding: 8px;
                line-height: 1.42857143;
            }

            .print-table {
                width: 100%;
                max-width: 100%;
                margin-bottom: 20px;

                background-color: transparent;
                border-spacing: 0;
                border-collapse: collapse;

                color: #333333;
                font-family: "Open Sans", sans-serif;
                padding: 0px !important;
                margin: 0px !important;
                font-size: 13px;
                direction: ltr;
            }
            thead {
                display: table-header-group;
                vertical-align: middle;
                border-color: inherit;
            }
            tbody {
                display: table-row-group;
                vertical-align: middle;
                border-color: inherit;
            }

            tr {
                display: table-row;
                vertical-align: inherit;
                border-color: inherit;
            }

            th {
                text-align: left;
            }

            .print-table thead tr th {
                font-size: 14px;
                font-weight: 600;
            }

            .print-table>tbody>tr>td {
                padding: 8px;
                line-height: 1.42857143;
                vertical-align: top;
                border-top: 1px solid #ddd;
            }

            tbody th, table.print-table-bordered tbody td {
                border-left-width: 0;
                border-bottom-width: 0;
            }

            .print-table-bordered>tbody>tr>td, .print-table-bordered>tfoot>tr>td {
                border: 1px solid #ddd;
            }

            .print-table-striped>tbody>tr:nth-child(odd)>td, .print-table-striped>tbody>tr:nth-child(odd)>th {
                background-color: #f9f9f9;
            }

            .print-title {
                color: #428bca;
                text-decoration: none;
                font-family: "Arial", sans-serif;
                font-size: 12px;
            }

            .label.label-sm {
                font-size: 12px;
                padding: 0px 4px 1px 4px;
            }

            .label-info {
                background-color: #89c4f4;
            }

            .label-danger {
                background-color: #f3565d;
            }

            .label-success {
                background-color: #45b6af;
            }

            .label {
                text-shadow: none !important;
                font-size: 13px;
                font-weight: 300;
                padding: 3px 6px 3px 6px;
                color: #fff;
                font-family: "Open Sans", sans-serif;
            }

        </style>

        <div class="table-responsive">
            <table class="print-table printa-table-striped print-table-bordered table-hover">
                <thead>
                <tr>
                    <th>Titulo</th>
                    <th>Vistas</th>
                    <th>Fecha Fin</th>
                    <th>Publicado</th>
                    <th>Estado</th>
                </tr>
                </thead>
                <tbody>
                <?php if(count($publications) > 0): ?>
                    <?php foreach($publications as $inmueble): ?>
                        <tr class="odd gradeX">
                            <td>
                                <?php
                                $url_detalle = base_url();
                                $url_detalle .= seo_url($inmueble->dep_nombre)."/";
                                $url_detalle .= seo_url($inmueble->cat_nombre . " EN ". $inmueble->for_descripcion)."/";
                                $url_detalle .= seo_url($inmueble->inm_seo).".html";
                                ?>
                                <!-- <a href="<?php echo $url_detalle; ?>" target="_blank"><?php echo $inmueble->inm_nombre; ?></a> -->
                                <span class="print-title" target="_blank"><?php echo $inmueble->inm_nombre; ?></span>
                            </td>
                            <td>
                                <?php echo $inmueble->inm_visitas; ?>
                            </td>
                            <td>
                                <?php echo date('d/m/Y', strtotime($inmueble->finish)); ?>
                            </td>
                            <td>
                                <?php echo $inmueble->inm_publicado; ?>
                            </td>
                            <td class="center">
                                <?php if($inmueble->pub_estado == 'Aprobado'): ?>
                                    <span class="label label-sm label-success">Aprobado</span>
                                <?php endif; ?>
                                <?php if($inmueble->pub_estado == 'No Aprobado'): ?>
                                    <span class="label label-sm label-info">Pendiente</span>
                                <?php endif; ?>
                                <?php if($inmueble->pub_estado == 'Cancelado'): ?>
                                    <span class="label label-sm label-danger">Cancelado</span>
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
        <p style="margin-top:10px; text-align:center;"><input type="button" onclick="window.close();" value="Cerrar"></p>
    </div>
</div>

<div class="modal fade poll-modal" data-publication-id tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Nos interesa su opinion</h4>
            </div>
            <div class="modal-body">
                <form name="poll-form" method="post" data-parsley-validate>
                    <input type="hidden" name="publication-id">
                    <div class="row">
                        <div class="col-md-12">
                            <p>
                                Consiguió vender su inmueble?
                                <label class="radio-inline">
                                    <input type="radio" name="property-sold" value="1" checked  > Si
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="property-sold" value="0"> No
                                </label>
                            <p>
                        </div>
                    </div>
                    <div class="row response-1">
                        <div class="col-md-12">
                            <p>Podria indicar por que medio consiguio venderlo?</p>
                            <?php
                            $i = 0;
                            foreach($meansOfSalesArray as $meansOfSale => $value)
                            {
                                $checked = $i==1?"":"";
                                ?>
                                <div class="radio-inline">
                                    <label>
                                        <input type="radio" data-parsley-group="block-1" required name="means-of-sale" value="<?=$meansOfSale?>" <?=$checked?> data-parsley-required-message="Por favor indique un medio de venta" data-parsley-errors-container="#means-of-sale-error-container">
                                        <?php
                                        if($meansOfSale == model_survey::TOQUE_EL_TIMBRE)
                                        {
                                            ?>
                                            <img style="height: 54px;width: auto;padding-bottom: 9px;"
                                                 src="<?= base_url("assets/backend/admin/img/logo-default.png") ?>">
                                            <?php
                                        }
                                        else
                                        {
                                            echo $value;
                                        }
                                        ?>

                                    </label>
                                </div>
                                <?php
                                $i++;
                                echo  $i%3==0?"<br>":"";
                            }
                            ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="means-of-sale-error-container"></div>
                                </div>
                            </div>
                            <div class="response-1-1">
                                <p>Podria compartirnos con nosotros el precio de venta?</p><div class="row">

                                    <div class="col-md-5">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="basic-addon1">$</span>
                                            <input type="number" min="1" class="form-control" name="sales-price" placeholder="Precio" aria-describedby="basic-addon1" required data-parsley-required-message="Por favor rellene este campo" data-parsley-errors-container="#sales-price" data-parsley-group="block-1">
                                            <span class="input-group-addon" id="basic-addon1">Dólares</span>
                                        </div>
                                        <div id="sales-price"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row response-2">
                        <div class="col-md-12">
                            <p>Podria compartir con nostros la razon por la cual dejo de publicar su inmueble?</p>
                            <textarea name="no-sold-description" required data-parsley-group="block-2" class="form-control" rows="3" data-parsley-required-message="Su opinion nos interesa, por favor agregue un detalle"></textarea>
                        </div>
                    </div>
                </form>
                <div class='progress modal-progress-bar' style="display: none">
                    <div class='progress-bar progress-bar-striped active' role='progressbar' aria-valuenow='100' aria-valuemin='0' aria-valuemax='100' style='width: 100%'></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary send-data">Listo!</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- END PAGE CONTENT-->