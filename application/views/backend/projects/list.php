<?php
$action = $_GET['action'];
?>
<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">

        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box grey-cascade">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-globe"></i>
                </div>
                <div class="tools">
                </div>
            </div>

            <div class="portlet-body">

                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>Imagen</th>
                            <th>Titulo</th>
                            <th>Vistas</th>
                            <th>Fecha Inicio</th>
                            <th>Fecha Fin</th>
                            <th>Estado</th>
                            <th>Opciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if(count($projects) > 0): ?>
                            <?php foreach($projects as $project): ?>
                                <tr class="odd gradeX">
                                    <td><img src="<?php echo base_url()."admin/imagenes/proyecto/".$project->proy_logo; ?>" width="100" /></td>
                                    <td><?php echo $project->proy_nombre; ?></td>
                                    <td><?php echo $project->proy_visitas; ?></td>
                                    <td><?php echo $project->proy_vig_ini != "0000-00-00" ? date("d/m/Y", strtotime($project->proy_vig_ini)) : '-'; ?></td>
                                    <td><?php echo $project->proy_vig_fin != "0000-00-00" ? date("d/m/Y", strtotime($project->proy_vig_fin)) : '-'; ?></td>
                                    <td>
                                        <?php
                                        if($project->proy_estado == "No Aprobado"){
                                            echo '<span class="label label-sm label-danger">No Publicado</span>';
                                        }else{
                                            echo '<span class="label label-sm label-success">Aprobado</span>';
                                        }
                                        ?>
                                    </td>
                                    <td>
                                        <a class="btn btn-sm yellow" href="<?php echo base_url(). $project->proy_seo; ?>" title="Ver Proyecto" target="_blank"><i class="fa fa-link"></i></a>
                                        <a class="btn btn-sm yellow" href="<?=base_url("admin/Project/leads/".$project->proy_id)?>" title="Ver Leads" target="_blank"><i class="fa fa-list"></i></a>
                                        <!--<a class="btn btn-sm green" href="<?php echo base_url(); ?>dashboard/project/available/<?php echo $project->proy_id; ?>" title="Disponibilidad"><i class="fa fa-lightbulb-o"></i></a>-->
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        </tbody>
                    </table>

                    <?php if($pagination != ""): ?>
                        <ul class="pagination">
                            <?php echo $pagination; ?>
                        </ul>
                    <?php endif; ?>

                </div>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
<!-- END PAGE CONTENT-->