<?php
$action = $_GET['action'];
?>
<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-success">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
            <strong>Registrado correctamente!</strong> se ha registrado un nuevo anuncio.
        </div>

        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box grey-cascade">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-globe"></i>
                </div>
                <div class="tools">
                </div>
            </div>

            <div class="portlet-body">

                <div class="table-responsive">
                    <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                        <thead>
                        <tr>
                            <th>
                                Tipologia
                            </th>
                            <th>
                                Precio/USD
                            </th>
                            <th>
                                Disponibilidad
                            </th>
                            <th>
                                Edit
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if(count($tipologias) > 0): ?>
                            <?php foreach($tipologias as $tip): ?>
                            <tr rel="<?php echo $tip->tip_id; ?>">
                                <td>
                                    <?php echo $tip->tip_titulo; ?>
                                </td>
                                <td>
                                    <?php echo number_format($tip->tip_precio, 0, ',', '.'); ?>
                                </td>
                                <td>
                                    <?php echo $tip->tip_disponibilidad; ?>
                                </td>
                                <td>
                                    <a class="edit" href="javascript:;">Modificar</a>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        </tbody>
                    </table>

                    <?php if($pagination != ""): ?>
                        <ul class="pagination">
                            <?php echo $pagination; ?>
                        </ul>
                    <?php endif; ?>

                </div>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
<!-- END PAGE CONTENT-->