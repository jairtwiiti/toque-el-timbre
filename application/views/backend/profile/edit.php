<?php
$path_assets = base_url() . "assets/backend/";
?>
<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PROFILE SIDEBAR -->
        <div class="profile-sidebar">
            <!-- PORTLET MAIN -->
            <div class="portlet light profile-sidebar-portlet">
                <!-- SIDEBAR USERPIC -->
                <div class="profile-userpic">
                    <?php if(!empty($user_info->usu_foto)){ ?>
                        <img alt="" class="img-circle img-responsive" src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo base_url(); ?>/assets/uploads/users/<?php echo strtolower($user_info->usu_foto); ?>&w=750&h=750"/>
                    <?php }else{ ?>
                        <img alt="" class="img-circle img-responsive" src="<?php echo $path_assets; ?>admin/img/avatar_empty.gif"/>
                    <?php } ?>
                </div>
                <!-- END SIDEBAR USERPIC -->
                <!-- SIDEBAR USER TITLE -->
                <div class="profile-usertitle">
                    <div class="profile-usertitle-name">
                        <?php
                        if($user_info->usu_tipo != "Particular"){
                            echo $user_info->usu_empresa;
                        }else{
                            echo $user_info->usu_nombre . " " . $user_info->usu_apellido;
                        }
                        ?>
                    </div>
                    <div class="profile-usertitle-job">
                        <?php echo $user_info->usu_tipo; ?>
                    </div>
                </div>
                <!-- END SIDEBAR USER TITLE -->

                <!-- SIDEBAR BUTTONS -->
                <!--<div class="profile-userbuttons">
                    <button type="button" class="btn btn-circle green-haze btn-sm">Follow</button>
                    <button type="button" class="btn btn-circle btn-danger btn-sm">Message</button>
                </div>-->
                <!-- END SIDEBAR BUTTONS -->

                <!-- SIDEBAR MENU -->
                <div class="profile-usermenu">
                    <ul class="nav">
                        <li class="active">
                            <a href="<?php echo base_url() ?>dashboard/edit-profile"><i class="icon-settings"></i> Configuracion de Cuenta </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url() ?>dashboard/messages"><i class="icon-check"></i> Mensajes </a>
                        </li>
                        <!--<li>
                            <a href="extra_profile_help.html"><i class="icon-info"></i> Help </a>
                        </li>-->
                    </ul>
                </div>
                <!-- END MENU -->
            </div>
            <!-- END PORTLET MAIN -->

        </div>
        <!-- END BEGIN PROFILE SIDEBAR -->
        <!-- BEGIN PROFILE CONTENT -->
        <div class="profile-content">
            <div class="row">
                <div class="col-md-12 col-mobile-1">
                    <div class="portlet light">
                        <div class="portlet-title tabbable-line col-md-12 col-mobile-1">
                            <div class="caption caption-md">
                                <i class="icon-globe theme-font hide"></i>
                                <span class="caption-subject font-blue-madison bold uppercase">Perfil de Usuario</span>
                            </div>
                            <ul class="nav nav-tabs">
                                <li class="<?php echo empty($position) || $position == 'user' ? 'active' : ''; ?>">
                                    <a href="#tab_edit_user" data-toggle="tab">Editar Usuario</a>
                                </li>
                                <?php if($user_info->usu_tipo != 'Particular' && $user_info->usu_tipo != 'Buscador'): ?>
                                <li class="<?php echo $position == 'company' ? 'active' : ''; ?>">
                                    <a href="#tab_edit_company" id="tab_company" data-toggle="tab">Datos de la Empresa</a>
                                </li>
                                <?php endif; ?>
                                <li class="<?php echo $position == 'change_image' ? 'active' : ''; ?>">
                                    <a href="#tab_change_image" data-toggle="tab">Cambiar Imagen</a>
                                </li>
                                <li class="<?php echo $position == 'change_password' ? 'active' : ''; ?>">
                                    <a href="#tab_change_password" data-toggle="tab">Cambiar Contraseña</a>
                                </li>
                                <li class="<?php echo $position == 'notifications' ? 'active' : ''; ?>">
                                    <a href="#tab_notifications" data-toggle="tab">Notificaciones</a>
                                </li>
                            </ul>
                        </div>
                        <div class="portlet-body">
                            <div class="tab-content">
                                <?php if(@$_POST['inmobiliaria'] == "Inmobiliaria"){ echo '<div class="alert alert-warning"><button class="close" data-close="alert"></button><span>Para disfrutar de los precios exclusivos y descuentos a empresas inmobiliarias debe certificarse en ToqueelTimbre.com. <br/>Llame al 3-3434253 para mas informacion o envie un mail a info@toqueeltimbre.com</span></div>'; } ?>
                                <?php if(@$ok_update){ echo '<div class="alert alert-success"><button class="close" data-close="alert"></button><span>'. $ok_update .'</span></div>'; } ?>
                                <?php if(@$error_update){ echo '<div class="alert alert-danger"><button class="close" data-close="alert"></button><span>'. $error_update.'</span></div>'; } ?>
                                <?php if(@validation_errors()){ echo '<div class="alert alert-danger"><button class="close" data-close="alert"></button><span>'. validation_errors().'</span></div>'; } ?>

                                <!-- PERSONAL INFO TAB -->
                                <div class="tab-pane <?php //echo empty($position) || $position == 'user' ? 'active' : ''; ?> active" id="tab_edit_user">

                                    <form role="form" action="<?php echo base_url(); ?>dashboard/edit-profile" method="post">
                                        <div class="form-group col-md-12 col-mobile-1">
                                            <label class="control-label">Nombre</label>
                                            <input type="text" name="first_name" class="form-control" value="<?php echo $user_info->usu_nombre; ?>" />
                                        </div>
                                        <div class="form-group col-md-12 col-mobile-1">
                                            <label class="control-label">Apellido</label>
                                            <input type="text" name="last_name" class="form-control" value="<?php echo $user_info->usu_apellido; ?>" />
                                        </div>
                                        <div class="form-group col-md-12 col-mobile-1">
                                            <label class="control-label">Email</label>
                                            <input type="text" name="email" class="form-control" value="<?php echo $user_info->usu_email; ?>" />
                                        </div>
                                        <div class="form-group col-md-3 col-mobile-1">
                                            <label class="control-label">Ciudad</label>
                                            <select name="city" class="bs-select form-control">
                                                <option value="">Seleccionar Ciudad</option>
                                                <option value="1" <?php echo $user_info->usu_ciu_id == 1 ? 'selected': ''; ?> >Santa Cruz</option>
                                                <option value="2" <?php echo $user_info->usu_ciu_id == 2 ? 'selected': ''; ?>>La Paz</option>
                                                <option value="3" <?php echo $user_info->usu_ciu_id == 3 ? 'selected': ''; ?>>Cochabamba</option>
                                                <option value="4" <?php echo $user_info->usu_ciu_id == 4 ? 'selected': ''; ?>>Tarija</option>
                                                <option value="5" <?php echo $user_info->usu_ciu_id == 5 ? 'selected': ''; ?>>Chuquisaca</option>
                                                <option value="6" <?php echo $user_info->usu_ciu_id == 6 ? 'selected': ''; ?>>Oruro</option>
                                                <option value="7" <?php echo $user_info->usu_ciu_id == 7 ? 'selected': ''; ?>>Potosi</option>
                                                <option value="8" <?php echo $user_info->usu_ciu_id == 8 ? 'selected': ''; ?>>Pando</option>
                                                <option value="9" <?php echo $user_info->usu_ciu_id == 9 ? 'selected': ''; ?>>Beni</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-12 col-mobile-1">
                                            <label class="control-label">Telefono</label>
                                            <input type="text" name="phone" class="form-control" value="<?php echo $user_info->usu_telefono; ?>" />
                                        </div>
                                        <div class="form-group col-md-12 col-mobile-1">
                                            <label class="control-label">Celular</label>
                                            <input type="text" name="cellphone" class="form-control" value="<?php echo $user_info->usu_celular; ?>" />
                                            <input type="hidden" name="tab_position" value="user" />
                                        </div>
                                        <?php if($user_info->usu_tipo == "Particular"): ?>
                                        <style>
                                            .btn-group .active{ background-color: #24a5d9; color:#fff; }
                                        </style>
                                        <div class="form-group">
                                            <label class="control-label">Cambiar tipo de Usuario</label>
                                            <div class="clearfix">
                                                <div class="btn-group btn-group-circle" data-toggle="buttons">
                                                    <label class="btn btn-default ">
                                                        <input type="radio" name="inmobiliaria" value="Inmobiliaria" class="toggle"> Inmobiliaria </label>
                                                    <label class="btn btn-default active">
                                                        <input type="radio" name="inmobiliaria" value="Particular" <?php echo $user_info->usu_tipo == "Particular" ? 'checked':''; ?> class="toggle"> Particular </label>
                                                </div>
                                            </div>
                                        </div>
                                        <?php endif; ?>
                                        <br />

                                        <div class="margin-top-10 col-md-12 col-mobile-1">
                                            <button class="btn blue">Guardar Cambios</button>
                                            <a href="<?php echo base_url(); ?>dashboard" class="btn default">Cancelar</a>
                                        </div>
                                    </form>
                                </div>
                                <!-- END PERSONAL INFO TAB -->

                                <!-- COMPANY INFO TAB -->
                                <div class="tab-pane <?php //echo $position == 'company' ? 'active' : ''; ?>" id="tab_edit_company">
                                    <form role="form" action="<?php echo base_url(); ?>dashboard/edit-profile" method="post">
                                        <div class="form-group col-md-12 col-mobile-1">
                                            <label class="control-label">Empresa</label>
                                            <input type="text" name="company" class="form-control" value="<?php echo $user_info->usu_empresa; ?>" />
                                        </div>
                                        <div class="form-group col-md-12 col-mobile-1">
                                            <label class="control-label">NIT</label>
                                            <input type="text" name="ci_nit" class="form-control" value="<?php echo $user_info->usu_ci; ?>" />
                                        </div>
                                        <div class="form-group col-md-12 col-mobile-1">
                                            <label class="control-label">Direccion</label>
                                            <input type="text" name="address" class="form-control" value="<?php echo $user_info->usu_direccion; ?>" />
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label class="control-label">Acerca de la Empresa</label>
                                            <textarea class="form-control" rows="3" name="description" placeholder=""><?php echo $user_info->usu_descripcion; ?></textarea>
                                        </div>
                                        <div class="form-group col-md-12 col-mobile-1">
                                            <label class="control-label">Skype</label>
                                            <input type="text" name="skype" class="form-control" value="<?php echo $user_info->usu_skype; ?>" />
                                        </div>
                                        <div class="form-group col-md-12 col-mobile-1">
                                            <label class="control-label">Facebook</label>
                                            <input type="text" name="facebook" placeholder="https://www.facebook.com/su_perfil" value="<?php echo $user_info->usu_facebook; ?>" class="form-control"/>
                                        </div>
                                        <div class="form-group col-md-12 col-mobile-1">
                                            <label class="control-label">Twitter</label>
                                            <input type="text" name="twitter" placeholder="https://www.twitter.com/su_perfil" value="<?php echo $user_info->usu_twitter; ?>" class="form-control"/>
                                            <input type="hidden" name="tab_position" value="company" />
                                        </div>


                                        <div class="form-group col-md-12 col-mobile-1">
                                            <label class="control-label">Ubicacion:
                                            </label>
                                            <br />
                                            <!-- BEGIN GEOCODING PORTLET-->
                                            <div class="light">
                                                <div class="portlet-body">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" id="gmap_geocoding_address" placeholder="Direccion...">
                                                            <span class="input-group-btn">
                                                                <button class="btn blue" id="gmap_geocoding_btn"><i class="fa fa-search"></i>
                                                            </span>
                                                        </div>
                                                    <input id="latitude" type="hidden" name="latitude"  value="<?php echo $user_info->usu_latitud; ?>" />
                                                    <input id="longitude" type="hidden" name="longitude" value="<?php echo $user_info->usu_longitud; ?>" />
                                                    <div id="gmap_geocoding" class="gmaps" data-latitud="-17.784559" data-longitud="-63.1590514" data-city="Santa Cruz de la Sierra" data-marker-longitud="<?php echo $user_info->usu_longitud; ?>" data-marker-latitud="<?php echo $user_info->usu_latitud; ?>"></div>
                                                </div>
                                            </div>
                                            <!-- END GEOCODING PORTLET-->
                                        </div>


                                        <br />

                                        <div class="margin-top-10 col-md-12">
                                            <button class="btn blue">Guardar Cambios</button>
                                            <a href="<?php echo base_url(); ?>dashboard" class="btn default">Cancelar</a>
                                        </div>
                                    </form>
                                </div>
                                <!-- END COMPANY INFO TAB -->

                                <!-- CHANGE AVATAR TAB -->
                                <div class="tab-pane <?php //echo $position == 'change_image' ? 'active' : ''; ?>" id="tab_change_image">
                                    <p class="col-md-12 col-mobile-1">
                                        Para modificar la imagen de su perfil, seleccione una imagen y presione el boton "Subir Imagen".
                                    </p>
                                    <form action="<?php echo base_url(); ?>dashboard/edit-profile" role="form" method="post" enctype="multipart/form-data">
                                        <div class="form-group col-md-12 col-mobile-1">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">

                                                    <?php if(!empty($user_info->usu_foto)){ ?>
                                                        <img src="<?php echo base_url(); ?>/assets/uploads/users/<?php echo strtolower($user_info->usu_foto); ?>"/>
                                                    <?php }else{ ?>
                                                        <img src="https://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/>
                                                    <?php } ?>


                                                </div>
                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                                                <div>
                                                    <span class="btn default btn-file">
                                                        <span class="fileinput-new">Seleccionar Imagen </span>
                                                        <span class="fileinput-exists">Cambiar </span>
                                                        <input type="file" name="userfile" accept="image/*">
                                                    </span>
                                                    <a href="#" class="btn default fileinput-exists" data-dismiss="fileinput">Quitar</a>
                                                </div>
                                            </div>
                                            <div class="clearfix margin-top-10">
                                                <span><b>Nota:</b> Solo se permiten subir imagenes (.jpg, .png, .gif) </span>
                                            </div>
                                        </div>
                                        <br />

                                        <div class="margin-top-10 col-md-12 col-mobile-1">
                                            <input type="hidden" name="tab_position" value="change_image" />
                                            <button class="btn blue">Guardar Cambios</button>
                                            <a href="<?php echo base_url(); ?>dashboard" class="btn default">Cancelar</a>
                                        </div>
                                    </form>
                                </div>
                                <!-- END CHANGE AVATAR TAB -->

                                <!-- CHANGE PASSWORD TAB -->
                                <div class="tab-pane <?php //echo $position == 'change_password' ? 'active' : ''; ?>" id="tab_change_password">
                                    <form action="<?php echo base_url(); ?>dashboard/edit-profile" method="post">
                                        <!--<div class="form-group">
                                            <label class="control-label">Current Password</label>
                                            <input type="password" class="form-control"/>
                                        </div>-->
                                        <div class="form-group col-md-12 col-mobile-1">
                                            <label class="control-label">Nueva Contraseña</label>
                                            <input type="password" name="password" class="form-control"/>
                                        </div>
                                        <div class="form-group col-md-12 col-mobile-1">
                                            <label class="control-label">Repetir Nueva Contraseña</label>
                                            <input type="password" name="repassword" class="form-control"/>
                                        </div>
                                        <br />

                                        <div class="margin-top-10 col-md-12 col-mobile-1">
                                            <input type="hidden" name="tab_position" value="change_password" />
                                            <button class="btn blue">Guardar Cambios</button>
                                            <a href="<?php echo base_url(); ?>dashboard" class="btn default">Cancelar</a>
                                        </div>
                                    </form>
                                </div>
                                <!-- END CHANGE PASSWORD TAB -->

                                <!-- PRIVACY SETTINGS TAB -->
                                <div class="tab-pane <?php //echo $position == 'notifications' ? 'active' : ''; ?>" id="tab_notifications">
                                    <form action="<?php echo base_url(); ?>dashboard/edit-profile" method="post" class="col-md-12 col-mobile-1">
                                        <table class="table table-light table-hover">
                                            <tr>
                                                <td>Desea mostrar su datos de contacto?</td>
                                                <td>
                                                    <label class="uniform-inline"><input type="radio" name="datos_contacto" value="Si" <?php echo $user_info->usu_dat_contacto == 'Si' ? 'checked':''; ?> /> Si </label>
                                                    <label class="uniform-inline"><input type="radio" name="datos_contacto" value="No" <?php echo $user_info->usu_dat_contacto == 'No' ? 'checked':''; ?> /> No </label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Recibir notificaciones por email sobre sus anuncios</td>
                                                <td>
                                                    <label class="uniform-inline"><input type="radio" name="email_reporte" value="1" <?php echo $user_info->usu_email_reporte == 1 ? 'checked':''; ?>/> Si </label>
                                                    <label class="uniform-inline"><input type="radio" name="email_reporte" value="0" <?php echo $user_info->usu_email_reporte == 0 ? 'checked':''; ?>/> No </label>
                                                </td>
                                            </tr>
                                        </table>
                                        <!--end profile-settings-->
                                        <br />

                                        <div class="margin-top-10">
                                            <input type="hidden" name="tab_position" value="notifications" />
                                            <button class="btn blue">Guardar Cambios</button>
                                            <a href="<?php echo base_url(); ?>dashboard" class="btn default">Cancelar</a>
                                        </div>
                                    </form>
                                </div>
                                <!-- END PRIVACY SETTINGS TAB -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PROFILE CONTENT -->
    </div>
</div>
<!-- END PAGE CONTENT-->