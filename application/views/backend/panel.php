<?php
$col_sp = "col-lg-4 col-md-4 col-sm-4";
if ($user->usu_tipo == 'Inmobiliaria' && $suscripcion == 1) {
    $col_sp = "col-lg-3 col-md-3 col-sm-3";
}
if($user->usu_certificado == "No" && $user->usu_tipo == 'Inmobiliaria'){
    echo '<div class="alert alert-warning"><button class="close" data-close="alert"></button><span>Para disfrutar de los precios exclusivos y descuentos a empresas inmobiliarias debe certificarse en ToqueelTimbre.com. <br/>Llame al 591 78526002 para mas informacion o envie un mail a info@toqueeltimbre.com</span></div>';
}
if(empty($user->usu_nombre) && empty($user->usu_apellido)){
    $url_perfil = base_url()."dashboard/edit-profile";
    echo '<div class="alert alert-danger"><button class="close" data-close="alert"></button><span>Debes actualizar los datos faltantes en tu perfil. <a href="'.$url_perfil.'">Actualizar Perfil</a></span></div>';
}
if($_GET['r'] == "new"){ echo '<div class="alert alert-success"><span style="font-weight:bold;"><i class="fa fa-envelope-o" aria-hidden="true"></i> Hemos enviado un mail de confirmacion a tu correo electronico, debes verificar el mismo para que tu cuenta este habilitada correctamente.</span></div>'; }
?>
<!-- BEGIN DASHBOARD STATS -->
<div class="row">
    <div class="<?= $col_sp ?> col-xs-12">
        <a class="dashboard-stat dashboard-stat-light green" href="<?=base_url("admin/Property/add")?>">
            <div class="visual">
                <!-- <i class="fa fa-home"></i> -->
            </div>
            <div class="details color-white">
                <div class="number">
                </div>
                <div class="desc" style="font-size: 28px; padding-top: 10px;">
                    Crear Anuncio +
                </div>
            </div>
        </a>
    </div>
    <div class="<?= $col_sp ?> col-xs-12">
        <a class="dashboard-stat dashboard-stat-light orange-gray" href="#">
            <div class="visual">
                <!-- <i class="fa fa-home"></i> -->
            </div>
            <div class="details color-white">
                <div class="number">
                    <?= $box1["info"] ?>
                </div>
                <div class="desc">
                    <?= $box1["text"]?>
                </div>
            </div>
        </a>
    </div>

    <div class="<?= $col_sp ?> col-xs-12">
        <a class="dashboard-stat dashboard-stat-light orange-light" href="#">
                <div class="visual">
                    <!-- <i class="fa fa-home"></i> -->
                </div>
                <div class="details color-white">
                    <div class="number">
                        <?= $box2["info"] ?>
                    </div>
                    <div class="desc">
                        <?= $box2["text"]?>
                    </div>
                </div>
            </a>
    </div>
    <?php if($user->usu_tipo == 'Inmobiliaria' && $suscripcion == 1) {?>
    <div class="<?= $col_sp ?> col-xs-12">
        <a class="dashboard-stat dashboard-stat-light orange-super-light" href="#">
            <div class="visual">
                <!-- <i class="fa fa-home"></i> -->
            </div>
            <div class="details color-white">
                <div class="number">
                    <?= $box3["info"] ?>
                </div>
                <div class="desc">
                    <?= $box3["text"]?>
                </div>
            </div>
        </a>
    </div>
    <?php }?>
</div>
<!-- END DASHBOARD STATS -->
<div class="clearfix">
</div>

<?php if(!empty($visitorsBar)) {?>
<div class="row">
    <div class="col-lg-12">
        <!-- BEGIN BASIC CHART PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>Cantidad de visitas en tus anuncios
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>
                    <a href="javascript:;" class="reload">
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div id="chart_1_1_legendPlaceholder">
                </div>
                <div id="chart_1_1" class="chart">
                </div>
            </div>
        </div>
        <!-- END BASIC CHART PORTLET-->
    </div>
</div>
<?php } else { ?>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div id="prefix_473970838607" class="Metronic-alerts alert alert-success fade in">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            <i class="icon-bar-chart fa-2x"></i> Para ver tus estadísticas, empieza a publicar ya.
        </div>
    </div>
</div>

<?php } ?>

<?php if(!empty($visitors)) {?>
<div class="row">
    <div class="col-md-6 col-sm-6">
        <!-- BEGIN PORTLET-->
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-bar-chart font-green-sharp hide"></i>
                    <span class="caption-subject font-green-sharp bold uppercase">publicaciones diarias en toque el timbre</span>
                    <!-- <span class="caption-helper">weekly stats...</span> -->
                </div>
                <!--<div class="actions">
                    <div class="btn-group btn-group-devided" data-toggle="buttons">
                        <label class="btn btn-transparent grey-salsa btn-circle btn-sm active">
                            <input type="radio" name="options" class="toggle" id="option1">New</label>
                        <label class="btn btn-transparent grey-salsa btn-circle btn-sm">
                            <input type="radio" name="options" class="toggle" id="option2">Returning</label>
                    </div>
                </div> -->
            </div>
            <div class="portlet-body">
                <div id="site_statistics_loading">
                    <img src="../../assets/admin/layout2/img/loading.gif" alt="loading"/>
                </div>
                <div id="site_statistics_content" class="display-none">
                    <div id="site_statistics" class="chart">
                    </div>
                </div>
            </div>
        </div>
        <!-- END PORTLET-->
    </div>
    <div class="col-md-6 col-sm-6">
        <!-- BEGIN PORTLET-->
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-share font-red-sunglo hide"></i>
                    <span class="caption-subject font-green-sharp bold uppercase">Anuncios mas Visitados</span>
                    <!-- <span class="caption-helper">Desde <?= date('M-d', strtotime ('-15 day', strtotime(date('Y-m-d'))))?> hasta hoy</span> -->
                </div>
                <div class="actions">
                    <div class="btn-group">
                        <!-- <select id="notific8_pos_hor" class="form-control input-sm input-small input-inline">
                            <option value="0">
                                <span>Todos</span>
                            </option>
                            <?php foreach($publications as $publication) {
                                $str = $publication->inm_nombre;
                                $colons = strlen($str) > 15 ? "..." : "";?>
                                <option value="<?= $publication->inm_id ?>">
                                    <span><?= substr($str, 0, 15) . $colons ?></span>
                                </option>
                            <?php }?>
                        </select> -->
                    </div>
                </div>
            </div>
            <div class="portlet-body">
                <div id="site_activities_loading">
                    <img src="../../assets/admin/layout2/img/loading.gif" alt="loading"/>
                </div>
                <div id="site_activities_content" class="display-none">
                    <!--<div id="site_activities" style="height: 228px;">
                    </div>-->
                    <div id="site_activities" style="height: 300px;">
                    </div>
                </div>
                <!--<div style="margin: 20px 0 10px 30px">
                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-6 text-stat">
                                                        <span class="label label-sm label-success">
                                                        Revenue: </span>
                            <h3>$13,234</h3>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 text-stat">
                                                        <span class="label label-sm label-danger">
                                                        Shipment: </span>
                            <h3>$1,134</h3>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 text-stat">
                                                        <span class="label label-sm label-primary">
                                                        Orders: </span>
                            <h3>235090</h3>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
        <!-- END PORTLET-->
    </div>
</div>
<?php } ?>

<?php if(!empty($categoriesPieOffer) || !empty($categoriesPieDemand)) {?>
<!-- BEGIN PIE CHART PORTLET-->
<div class="row">

    <?php if(!empty($categoriesPieOffer)) {?>

    <div class="col-md-6 col-sm-6">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>Oferta de publicaciones por categoria
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>
                    <a href="javascript:;" class="reload">
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <h4></h4>
                <div id="pie_chart_2" class="chart">
                </div>
            </div>
        </div>
    </div>
    <?php } if (!empty($categoriesPieDemand)) { ?>
    <div class="col-md-6 col-sm-6">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>Demanda de publicaciones por categoria
                </div>
                <div class="tools">
                    <!-- <a href="#portlet-config" data-toggle="modal" class="config">
                    </a>
                    <a href="javascript:;" class="reload">
                    </a> -->
                    <a href="javascript:;" class="collapse">
                    </a>
                    <a href="javascript:;" class="reload">
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <h4></h4>
                <div id="pie_chart_3" class="chart">
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
</div>
<!-- END PIE CHART PORTLET-->
<?php } ?>

<div class="clearfix">
</div>
