<style>
.dashboard-stat{
    cursor: pointer;
}

</style>
<?php if(@validation_errors()){ echo '<div class="alert alert-danger">'.@validation_errors().'</div>'; } ?>
<!-- BEGIN PAGE CONTENT-->
<div class="row">
<div class="col-md-2"></div>
<div class="col-md-8">
    <!-- BEGIN SAMPLE FORM PORTLET-->
    <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-gift"></i> Suscripciones
            </div>
            <div class="tools">

            </div>
        </div>
        <div class="portlet-body form">
            <form id="form_payment" role="form" method="post" enctype="multipart/form-data">
                <div class="form-body">

                    <?php foreach($subscription_services as $obj): ?>
                        <!--<div class="form-group">
                            <label><?php echo $obj->ser_descripcion; ?></label>
                            <div class="input-group">
                                <div class="icheck-list">
                                    <label>
                                        <input type="radio" name="subscription" class="icheck" data-radio="iradio_square-grey" value="<?php echo $obj->ser_id; ?>"> Por <?php echo $obj->ser_dias; ?> dias
                                    </label>
                                </div>
                            </div>
                        </div>-->
                    <?php endforeach; ?>

                    <p>Para crear anuncios con suscripciones debe seleccionar un paquete:</p>


                    <div class="row">
                        <?php foreach($subscription_services as $obj): ?>
                        <?php
                        if($certification){
                            $price = $obj->ser_precio_suscriptor;
                        }else{
                            $price = $obj->ser_precio;
                        }
                        ?>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat green" style="height: 122px;" rel="<?php echo $obj->ser_id; ?>" data-price="<?php echo $price; ?>">
                                <div class="visual">
                                    <!-- <i class="fa fa-home"></i> -->
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <?php echo $price; ?> Bs
                                    </div>
                                    <div class="desc">

	                                    <input type="radio" name="groupradio" value="<?= $price; ?>">

                                        Por <?php echo $obj->ser_dias; ?> dias
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                    <input type="hidden" name="subscription" id="select_subscription" value="" />

                    <div class="form-group" style="margin-top: 15px;">
                        <p>Para que tu suscripcion sea aprobada debes cancelar la suma de <span id="total_subscription">0</span> Bs. escogiendo cualquiera de nuestras opciones.</p>
                    </div>

                    <div class="form-group">
                        <label><b>BENEFICIOS</b></label>
                        <p>Los beneficios de esta es suscripción son:</p>
                        <ul>
                            <li>30 Anuncios gratis</li>
                            <li>Publicación Inmediata</li>
                            <li>Contacto Directo</li>
                            <li>35% de Descuento en Destacados</li>
                            <li>Mail de Requerimiento de anuncios</li>
                            <li>Url Propia (ej. tuinmobiliaria.toqueeltimbre.com)</li>
                            <li>Perfil Inmobiliario con logo, descripción, información gral.</li>
                            <li>Estadísticas semanales del comportamiento de la demanda de su ciudad.</li>
                        </ul>

                        <label><b>FORMAS DE PAGO</b></label>
                        <p>Usted puede realizar el pago nuestros servicios por cualquiera de nuestras modalidades de pago: PagosNet, Deposito Bancario o en nuestras oficinas, siempre mencionando o dando como referencia su numero de pago.</p>
                        <p>
                            Todos los precios incluyen IVA.<br />
                            Toqueeltimbre.com S.R.L.<br />
                            NIT: 222334022
                        </p>

                        <div class="portlet box white">
                            <div class="portlet-body form">
                                <div class="form-body">
                                    <div class="panel-group accordion" id="accordion" role="tablist" aria-multiselectable="true">
                                        <input type="hidden" name="paymentType" value="">
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" rel="PAGOSNET">
	                                                    <i class="fa fa-arrow-right"></i> PAGOSNET (RECOMENDADO)
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                                <div class="panel-body">
                                                    <div id="html_pagos">
                                                        <!-- <p>Temporalmente este servicio no se encuentra disponible, por favor intenta con PAGO EN OFICINA, o TRANSFERENCIA BANCARIA. <br> Mil disculpas por las molestias.</p> -->
                                                        <p>Pagosnet es un servicio de puntos de pago que facilita a los anunciantes de inmuebles realizar su pago en efectivo en mas de 350 puntos autorizados a nivel nacional.</p>
                                                        <p>Usted puede realizar su pago en efectivo en las siguientes entidades financieras:</p>

                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <img src="<?php echo base_url(); ?>assets/img/logos_pagosnet/farmacorp.png" />
                                                            </div>
                                                            <div class="col-md-3">
                                                                <img src="<?php echo base_url(); ?>assets/img/logos_pagosnet/banco_fassil.png" />
                                                            </div>
                                                            <div class="col-md-3">
                                                                <img src="<?php echo base_url(); ?>assets/img/logos_pagosnet/grigota.png" />
                                                            </div>
                                                            <div class="col-md-3">
                                                                <img src="<?php echo base_url(); ?>assets/img/logos_pagosnet/x_cobrar.png" />
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <img src="<?php echo base_url(); ?>assets/img/logos_pagosnet/eco_futuro.png" />
                                                            </div>
                                                            <div class="col-md-3">
                                                                <img src="<?php echo base_url(); ?>assets/img/logos_pagosnet/la_merced.png" />
                                                            </div>
                                                            <div class="col-md-3">
                                                                <img src="<?php echo base_url(); ?>assets/img/logos_pagosnet/mutual_la_primera.png" />
                                                            </div>
                                                            <div class="col-md-3">
                                                                <img src="<?php echo base_url(); ?>assets/img/logos_pagosnet/mutual_la_paz.png" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php if($this->user_id == 384): ?>
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingFour">
                                                <h4 class="panel-title">
                                                    <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree" rel="TIGOMONEY">
                                                        <i class="fa fa-arrow-right"></i> TIGO MONEY (RECOMENDADO)
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                                <div class="panel-body">
                                                    <div id="html_pagos">
                                                        <div class="row">
                                                            <div class="col-sm-12 col-md-6">
                                                                <img src="<?php echo base_url(); ?>assets/img/logo-tigo-money.jpg" style="max-width: 400px;" />
                                                            </div>
                                                        </div>
                                                        <p style="margin-top: 6px;">Usted puede pagar a travez de su cuenta tigo money, solo ingrese el numero de su cuenta para poder proceder al pago.</p>
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <p><input type="text" name="linea" placeholder="Linea Tigo Money" style="padding: 7px 10px; font-size: 14px; border: 1px #B5B5B5 solid;" /></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php endif; ?>
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingThree">
                                                <h4 class="panel-title">
                                                    <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree" rel="BANCO">
	                                                    <i class="fa fa-arrow-right"></i> DEPÓSITO BANCARIO O TRANSFERENCIA
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                <div class="panel-body">
                                                    <div id="html_pagos">
                                                        <p>Usted puede pagar mediante transferencia o depósito bancario. Tenemos nuestra cuenta en el Banco Ganadero.</p>
                                                        <p>Si hace una transferencia bancaria mediante banca por internet, por favor coloque el nombre de su empresa en el detalle del pago y envíenos una captura de pantalla a pagos@toqueeltimbre.com. Si realiza un depósito por ventanilla, por favor escanee el comprobante de depósito y envíelo a pagos@toqueeltimbre.com. Una vez verificado su pago, su anuncio será publicado en el plazo de 24 horas hábiles.</p>
                                                        <p>Le enviaremos una factura digital por correo electronico. Usted la debe imprimir y esta le servirá para crédito fiscal.</p>
                                                        <p>NOTA IMPORTANTE: Nos debe informar antes de fin de mes el nombre y NIT para la factura de cualquier depósito o transferencia que realice. Caso contrario, procederemos a emitir su factura sin nombre y sin NIT. El plazo para enviar esta informacion es hasta las 16:00 horas del último día hábil del mes.</p>

                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <p><img src="<?php echo base_url(); ?>assets/img/logos_pagosnet/logo_ganadero.png" /></p>
                                                                <p>Cuenta Nro 1041-157282<br /> A nombre de TOQUEELTIMBRE.COM S.R.L.<br/> NIT 222334022</p>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingTwo">
                                                <h4 class="panel-title">
                                                    <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" rel="OFICINA">
	                                                    <i class="fa fa-arrow-right"></i> EFECTIVO Y CHEQUE (SOLO SANTA CRUZ)
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                                <div class="panel-body">
                                                    <div id="html_pagos">
	                                                    <div class="row">
		                                                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-6">
			                                                    <p>Puedes hacer el pago de tu anuncio en nuestras oficinas (Servicio solo para Santa Cruz)</p>
			                                                    <p>
				                                                    Nuestra oficina esta ubicada en:<br>
				                                                    ToqueElTimbre.com<br>
				                                                    Calle Mururé 2010, AV. Beni <br>
				                                                    Santa Cruz de la Sierra<br>
                                                                    (591) 78526002<br>
				                                                    info@toqueeltimbre.com
			                                                    </p>			                                                    
		                                                    </div>
		                                                    <div class="col-xs-12 col-sm-6 col-md-8 col-lg-6">
			                                                    <img src="<?php echo base_url()?>assets/img/hubicacion.PNG" style="width: 100%; height: 280px">
		                                                    </div>
	                                                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label><b>DATOS DE FACTURACION</b></label>
                        <br>
                        <!-- <label> ¿Nombre para su factura? SI <input type="checkbox" name="invoiced" id="invoiced"></label> -->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" name="userName" class="form-control" placeholder="Nombre Completo" id="userName" required>
											<span class="input-group-addon">
												<i class="fa fa-user"></i>
											</span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" name="userNit" class="form-control" id="userNit" placeholder="Nro. de NIT/C.I:" required>
											<span class="input-group-addon">
												<i class="fa fa-slack"></i>
											</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <input id="paymentTotal" type="hidden" name="paymentTotal" value="">
                <div class="form-actions">
                    <button type="submit" class="btn blue pull-right">Procesar el pago</button>
                </div>

            </form>
        </div>
    </div>
    <!-- END SAMPLE FORM PORTLET-->
</div>
<div class="col-md-2"></div>
</div>
<!-- END PAGE CONTENT-->
