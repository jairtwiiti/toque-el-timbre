<?php if($errorSintesis == "Si"){ echo '<div class="alert alert-danger">El servidor de PagosNet actualmente se encuentra con dificultades técnicas, le recomendamos pagar por Oficina o Depósito Bancario para no afectar la publicación de su anuncio, pedimos disculpas por los inconvenientes y esperamos su comprensión.</div>'; } ?>
<div class="row">
<div class="col-md-2"></div>
<div class="col-md-8">
    <!-- BEGIN SAMPLE FORM PORTLET-->
    <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-gift"></i> Confirmacion de Pago
            </div>
            <div class="tools">
                <a href="javascript:;" style="color:#fff;" onclick="window.print();">Imprimir</a>
            </div>
        </div>
        <div class="portlet-body form">
            <form id="form_payment" role="form" method="post" enctype="multipart/form-data">
            <div class="form-body">
                <div class="form-group">
                <!-- BEGIN PAGE CONTENT-->
                <?php if($paymentType == 'PAGOSNET') {?>
                    <div>
                        <p>El total a pagar será: </p>
                        <h1 style="font-weight:bold; font-size:48px; text-align:center;">Bs. <?= $totalPayment ?>.-</h1>
                        <p>Pagosnet es un servicio de puntos de pago que facilita a los anunciantes de inmuebles realizar su pago en efectivo en mas de 350 puntos autorizados a nivel nacional. </p>
                        <p>Debes presentar el siguiente codigo con el cual realizara el pago de su suscripcion:</p>
                        <h1 style="font-weight: bold; text-align: center; line-height:36px; font-size: 28px; margin-top:18px;">Codigo de Pago: <br><?= $pagoId?></h1>
                    </div>
                <?php }elseif($paymentType == 'OFICINA') {?>
                    <div>
                        <p>El total a pagar será: </p>
                        <h1 style="font-weight:bold; font-size:48px; text-align:center;">Bs. <?= $totalPayment ?>.-</h1>
                        <p>Puedes hacer el pago de tu anuncio en nuestras oficinas (Servicio solo para Santa Cruz), nuestra oficina esta ubicada en:</p>
                        <p style="font-weight:bold; margin-top:18px;">
                            Av. Beni Entre 2do y 3er anillo<br>
                            Calle Murure #2010<br>
                            70838911 - 591 78526002
                        </p>
                    </div>
                <?php }elseif($paymentType == 'BANCO') {?>
                    <div>
                        <p>El total a pagar será: </p>
                        <h1 style="font-weight:bold; font-size:48px; text-align:center;">Bs. <?= $totalPayment ?>.-</h1>
                        <p>Para pagar mediante banco solo debes realizar el deposito en el Banco GANADERO en la cuenta bancaria 1041157282 TOQUEELTIMBRE.COM S.R.L. o puedes hacerlo mediante transferencia bancaria, en ambos casos requerimos que nos envíes una imagen o scanner del comprobante de pago a
                            <a href="mailto:info@toqueeltimbre.com">info@toqueeltimbre.com</a>.
                        </p>
                    </div>
                <?php } ?>
                <!-- END PAGE CONTENT-->
                </div>
            </div>
            </form>
        </div>
    </div>
    <!-- END SAMPLE FORM PORTLET-->
</div>
<div class="col-md-2"></div>
</div>