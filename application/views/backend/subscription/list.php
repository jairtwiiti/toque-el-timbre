<?php
$action = $_GET['action'];
?>
<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">

        <?php if($action == 'delete'): ?>
            <div class="alert alert-danger">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                <strong>Eliminado correctamente!</strong> se ha eliminado su suscripcion correctamente.
            </div>
        <?php endif; ?>

        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box grey-cascade">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-globe"></i> LISTA DE SUSCRIPCIONES
                </div>
                <div class="tools">
                </div>
            </div>

            <div class="portlet-body">

                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="table-subscription">
                        <thead>
                        <tr>
                            <th>Pago ID</th>
                            <th>Suscripcion</th>
                            <th>Fecha Inicio</th>
                            <th>Fecha Fin</th>
                            <th>Total Bs</th>
                            <th>Estado</th>
                            <th>Opciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if(count($subscriptions) > 0){ ?>
                            <?php foreach($subscriptions as $obj): ?>
                                <tr class="odd gradeX">
                                    <td><?php echo $obj->pag_id; ?></td>
                                    <td><?php echo $obj->ser_descripcion; ?></td>
                                    <td><?php echo $obj->fecha_inicio != "" ? date("d/m/Y", strtotime($obj->fecha_inicio)) : "Sin Definir"; ?></td>
                                    <td><?php echo $obj->fecha_fin != "" ? date("d/m/Y", strtotime($obj->fecha_fin)) : "Sin Definir"; ?></td>
                                    <td><?php echo $obj->pag_monto; ?></td>
                                    <td>
                                        <?php if($obj->estado == 'Activo'){ ?>
                                            <span class="label label-sm label-success">Habilitado</span>
                                        <?php }elseif($obj->estado == 'Inactivo'){ ?>
                                            <span class="label label-sm label-warning">Pendiente de Pago</span>
                                        <?php }elseif($obj->estado == 'Expirado'){ ?>
                                            <span class="label label-sm label-danger">Expirado</span>
                                        <?php } ?>
                                    </td>
                                    <td>
                                        <?php if($obj->estado == 'Inactivo'){ ?>
                                            <a class="btn btn-sm red" href="<?php echo base_url(); ?>dashboard/subscriptions/delete/<?php echo $obj->pag_id; ?>" title="Eliminar Suscripcion"><i class="fa fa-trash-o"></i></a>
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php }else{ ?>
                            <tr class="odd gradeX">
                                <td colspan="6" align="center">
                                    No se ha encontrado resultados.
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>

                    <?php if($pagination != ""): ?>
                        <ul class="pagination">
                            <?php echo $pagination; ?>
                        </ul>
                    <?php endif; ?>

                </div>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
<!-- END PAGE CONTENT-->