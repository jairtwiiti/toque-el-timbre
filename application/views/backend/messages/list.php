<div class="portlet light">
    <div class="portlet-body">
        <div class="row inbox">
            <div class="col-md-12 col-ms-12 col-xs-12">
                <div class="col-md-4 col-ms-4 col-xs-4 col-mobile-1"></div>
                <div class="col-md-4 col-ms-4 col-xs-4 col-mobile-1"></div>
                <div class="col-md-4 col-ms-4 col-xs-4 col-mobile-1">
                    <div class="inbox-header">
                        <!--<h1 class="pull-left">Inbox</h1>-->
                        <form id="form-search" class="form-inline pull-right" action="<?php echo base_url(); ?>dashboard/messages/ajax_inbox">
                            <div class="input-group input-medium">
                                <input type="text" name="search" class="form-control" placeholder="Bucar mensaje...">
                                <input type="hidden" name="page" value="1" />
                                <span class="input-group-btn">
                                    <button type="submit" class="btn green"><i class="fa fa-search"></i></button>
                                </span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-ms-12 col-xs-12">
                <div class="col-md-3 col-ms-3 col-xs-4 col-mobile-1">
                    <ul class="inbox-nav margin-bottom-10">

                        <li class="inbox active">
                            <a href="javascript:;" class="btn" data-title="Mensajes" rel="<?php echo base_url(); ?>dashboard/messages/ajax_inbox" data-page="1">
                                Entrada<?php if($num_unread > 0): ?>(<?php echo $num_unread; ?>) <?php endif; ?></a>
                            <b></b>
                        </li>
                        <li class="trash">
                            <a class="btn" href="javascript:;" data-title="Eliminados" rel="<?php echo base_url(); ?>dashboard/messages/ajax_trash">
                                Eliminados </a>
                            <b></b>
                        </li>
                    </ul>
                </div>
                <div class="col-md-9 col-ms-9 col-xs-8 col-mobile-1">
                    <div class="inbox-loading">
                        Cargando...
                    </div>
                    <div class="inbox-content">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>