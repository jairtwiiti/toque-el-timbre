<?php
$limit = $num_per_page;
$page_total = ceil($num_inbox / $limit);
?>
<form id="form-table-actions" action="<?php echo base_url(); ?>dashboard/messages/actions" method="post">
    <input type="hidden" name="action" id="action" value="" />
    <table class="table table-striped table-advance table-hover">
    <thead>
    <tr>
        <th colspan="2">
            <input type="checkbox" class="mail-checkbox mail-group-checkbox">
            <div class="btn-group">
                <a class="btn btn-sm blue" href="#" data-toggle="dropdown">
                    Acciones <i class="fa fa-angle-down"></i>
                </a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="#" class="read"><i class="fa fa-pencil"></i> Marcar como leidos </a>
                    </li>
                    <li>
                        <a href="#" class="delete"><i class="fa fa-trash-o"></i> Eliminar</a>
                    </li>
                </ul>
            </div>
        </th>
        <th class="pagination-control" colspan="2">
            <?php if($num_inbox > 0){ ?>
                <span class="pagination-info"><?php echo $page; ?>-<?php echo $page_total; ?> de <?php echo $num_inbox; ?> </span>
            <?php }else{ ?>
                <span class="pagination-info">1-1 de <?php echo $num_inbox; ?></span>
            <?php } ?>
            <a class="btn btn-sm blue prev" rel="<?php echo base_url(); ?>dashboard/messages/ajax_inbox" data-name="prev" data-page="<?php echo ($page - 1) > 0 ? ($page - 1) : 1; ?>"><i class="fa fa-angle-left"></i></a>
            <a class="btn btn-sm blue next" rel="<?php echo base_url(); ?>dashboard/messages/ajax_inbox" data-name="next" data-page="<?php echo ($page + 1) <= $page_total ? ($page + 1) : $page_total; ?>"><i class="fa fa-angle-right"></i></a>
        </th>
    </tr>
    </thead>
    <tbody>
    <?php if(count($inbox) > 0): ?>
        <?php foreach($inbox as $in): ?>
            <tr <?php echo $in->men_leido == "No" ? 'class="unread"': ''; ?> data-messageid="<?php echo $in->men_id; ?>" data-message-type="<?php echo $in->tipo; ?>">
                <td class="inbox-small-cells">
                    <input type="checkbox" class="mail-checkbox" name="messages[]" value="<?php echo $in->men_id; ?>">
                </td>
                <td class="view-message hidden-xs" width="12%">
                    <?php echo ucwords(strtolower($in->men_nombre)); ?>
                </td>
                <td class="view-message ">
                    <?php echo crop_string($in->inm_nombre, 80); ?>
                </td>

                <td class="view-message text-right">
                    <?php echo date("d M Y", strtotime($in->men_fech)); ?>
                </td>
            </tr>
        <?php endforeach; ?>
    <?php endif; ?>
    </tbody>
    </table>
<form>
