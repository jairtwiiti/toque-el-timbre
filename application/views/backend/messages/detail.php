<div class="portlet light">
    <div class="portlet-body">
        <div class="row inbox">
            <div class="col-md-2">
                <ul class="inbox-nav margin-bottom-10" style="margin-top: 40px;">

                    <li class="inbox active">
                        <a href="javascript:;" class="btn" data-title="Mensajes" rel="<?php echo base_url(); ?>dashboard/messages/ajax_inbox" data-page="1">
                            Entrada<?php if($num_unread > 0): ?>(<?php echo $num_unread; ?>) <?php endif; ?></a>
                        <b></b>
                    </li>
                    <li class="trash">
                        <a class="btn" href="javascript:;" data-title="Eliminados" rel="<?php echo base_url(); ?>dashboard/messages/ajax_trash">
                            Eliminados </a>
                        <b></b>
                    </li>
                </ul>
            </div>
            <div class="col-md-10">
                <div class="inbox-header">
                    <!--<h1 class="pull-left">Inbox</h1>-->
                    <form id="form-search" class="form-inline pull-right" action="<?php echo base_url(); ?>dashboard/messages/ajax_inbox">
                        <div class="input-group input-medium">
                            <input type="text" name="search" class="form-control" placeholder="Bucar mensaje...">
                            <input type="hidden" name="page" value="1" />
							<span class="input-group-btn">
							    <button type="submit" class="btn green"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                </div>
                <div class="inbox-loading">
                    Cargando...
                </div>
                <div class="inbox-content">
                    <div class="inbox-header inbox-view-header" style="border-bottom: 1px solid #eee; padding-bottom: 10px;">
                        <h1 class="pull-left"><?php echo $message->inm_nombre; ?></h1>
                        <!--<div class="pull-right">
                            <i class="fa fa-print"></i>
                        </div>-->
                    </div>
                    <div class="inbox-view">
                        <p><span class="bold">Fecha: </span> <?php echo date("d M Y", strtotime($message->men_fech)); ?></p>
                        <p><span class="bold">Nombre: </span> <?php echo ucwords(strtolower($message->men_nombre)); ?></p>
                        <p><span class="bold">Empresa: </span> <?php echo ucwords(strtolower($message->men_empresa)); ?></p>
                        <p><span class="bold">Telefono: </span> <?php echo $message->men_telefono; ?></p>
                        <p><span class="bold">Email: </span> <?php echo $message->men_email; ?></p>
                        <p><span class="bold">Mensaje: </span> </p>
                        <p><?php echo nl2br($message->men_descripcion); ?></p>
                    </div>
                    <hr>
                </div>
            </div>
        </div>
    </div>
</div>