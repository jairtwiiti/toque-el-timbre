<div class="inbox-header inbox-view-header" style="border-bottom: 1px solid #eee; padding-bottom: 10px;">
    <h1 class="pull-left"><span class="property-name"><?php echo $message->inm_nombre; ?></span></h1>
    <!--<div class="pull-right">
        <i class="fa fa-print"></i>
    </div>-->
</div>
<div class="inbox-view" data-message-id="<?=$message->men_id?>" data-message-type="<?=$messageType?>">
    <input type="hidden" value="<?=$message->men_email?>" name="message-email">
    <input type="hidden" value="<?=$message->men_proy_id?>" name="project-id">
    <input type="hidden" value="<?=$message->men_inm_id?>" name="property-id">
    <p><span class="bold">Fecha: </span> <?php echo date("d M Y", strtotime($message->men_fech)); ?></p>
    <p><span class="bold">Nombre: </span> <?php echo ucwords(strtolower($message->men_nombre)); ?></p>
    <p><span class="bold">Correo: </span> <?php echo ucwords(strtolower($message->men_email)); ?></p>
    <p><span class="bold">Telefono: </span> <?php echo ucwords(strtolower($message->men_telefono)); ?></p>
    <p><span class="bold">Empresa: </span> <?php echo ucwords(strtolower($message->men_empresa)); ?></p>
    <p><span class="bold">Mensaje: </span> </p>
    <p><?php echo nl2br($message->men_descripcion); ?></p>
    <?php
    if(true)
    {
    ?>
    <p><button class="btn btn-primary btn-response-message">Responder</button></p>
    <?php
    }
    ?>
</div>
<hr>
<script id="ht-response-message-form" type="text/x-handlebars-template">
    <form name="response-message" class="form-group">
        <input type="hidden" name="message-type" value="{{data.messageType}}">
        <input type="hidden" name="email" value="{{data.messageEmail}}">
        <input type="hidden" name="project-id" value="{{data.projectId}}">
        <input type="hidden" name="property-id" value="{{data.propertyId}}">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="message">Mensaje</label><br>
                    <textarea class="form-control" name="message"></textarea>
                </div>
            </div>
        </div>
    </form>
</script>
