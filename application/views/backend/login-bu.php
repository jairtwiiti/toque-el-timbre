<?php
$path_assets = base_url() . "assets/backend/";
?>
<!DOCTYPE html>
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 3.3.0
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title>Toque el timbre | Login</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $path_assets; ?>global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $path_assets; ?>global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $path_assets; ?>global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $path_assets; ?>global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="<?php echo $path_assets; ?>admin/pages/css/login.css" rel="stylesheet" type="text/css"/>
    <!-- END PAGE LEVEL SCRIPTS -->
    <!-- BEGIN THEME STYLES -->
    <link href="<?php echo $path_assets; ?>global/css/components.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $path_assets; ?>global/css/plugins.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $path_assets; ?>admin/css/layout.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $path_assets; ?>admin/css/themes/grey.css" rel="stylesheet" type="text/css" id="style_color"/>
    <link href="<?php echo $path_assets; ?>admin/css/custom.css" rel="stylesheet" type="text/css"/>

    <!-- END THEME STYLES -->
    <link rel="shortcut icon" href="<?php echo $path_assets; ?>img/icons/favicon.ico"/>
    <style>
        .login .alert p{
            margin-bottom: 0 !important;
        }
    </style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login">
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-18182487-11', 'auto');
        ga('send', 'pageview');
    </script>

    <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
    <div class="menu-toggler sidebar-toggler">
    </div>
    <!-- END SIDEBAR TOGGLER BUTTON -->
    <!-- BEGIN LOGO -->
    <div class="logo">
        <a href="<?php echo base_url(); ?>">
            <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="Toqueeltimbre.com" title="Toqueeltimbre.com" />
        </a>
    </div>
    <!-- END LOGO -->
    <!-- BEGIN LOGIN -->
    <div class="content">
        <!-- BEGIN LOGIN FORM -->
        <form class="login-form" action="<?php echo base_url(); ?>login" method="post">
            <h3 class="form-title">Autentificación</h3>
            <div class="alert alert-danger display-hide">
                <button class="close" data-close="alert"></button>
                <span>Ingrese su email y contraseña.</span>
            </div>

            <?php if(@$error_login || @$error_validation): ?>
                <div class="alert alert-danger">
                    <button class="close" data-close="alert"></button>
                    <span><?php echo @$error_login ? $error_login : $error_validation; ?></span>
                </div>
            <?php endif; ?>

            <div class="form-group">
                <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                <label class="control-label visible-ie8 visible-ie9">Correo electrónico</label>
                <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Correo electrónico" name="email"  value="<?php echo @$_POST['email']; ?>" />
            </div>
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9">Contraseña</label>
                <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Contraseña" name="password"/>
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <input type="hidden" name="url_redirect" value="<?php echo !empty($_GET['url_redirect']) ? $_GET['url_redirect'] : $_POST['url_redirect']; ?>" />
                        <button type="submit" class="btn btn-success uppercase" style="border-radius: 5px !important;"><i class="fa fa-sign-in"></i> Ingresar</button>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 forget">
                        <a href="<?php echo base_url(); ?>recuperar-contrasena" class="forget-password">Olvid&eacute; mi contraseña</a>
                    </div>
                </div>
            </div>
            <div class="login-options row">
                <div class="col-md-6">
                    <h4>O iniciar sesion con</h4>
                </div>
                <div class="col-md-6">
                    <ul class="social-icons">
                        <li>
                            <a class="social-icon-color facebook" data-original-title="facebook" href="<?php echo base_url(); ?>auth/facebook<?php echo !empty($_GET['url_redirect']) ? '?url_redirect='.$_GET['url_redirect'] : '?url_redirect='.$_POST['url_redirect']; ?>"></a>
                        </li>
                        <!--<li>
                            <a class="social-icon-color twitter" data-original-title="Twitter" href="<?php echo base_url(); ?>auth/twitter"></a>
                        </li>-->
                        <li>
                            <a class="social-icon-color googleplus" data-original-title="Goole Plus" href="<?php echo base_url(); ?>auth/google<?php echo !empty($_GET['url_redirect']) ? '?url_redirect='.$_GET['url_redirect'] : '?url_redirect='.$_POST['url_redirect']; ?>"></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="create-account">
                <p>
                    <a href="<?php echo base_url(); ?>registrarse" class="uppercase">Registrarse ya!</a>
                </p>
            </div>
        </form>
        <!-- END LOGIN FORM -->

    </div>

    <div class="copyright">
        <?= date('Y')?>  &copy; Toqueeltimbre.com. Todos los derechos reservados.
    </div>
    <!-- END LOGIN -->

<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?php echo $path_assets; ?>global/plugins/respond.min.js"></script>
<script src="<?php echo $path_assets; ?>plugins/excanvas.min.js"></script>
<![endif]-->
<script src="<?php echo $path_assets; ?>global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo $path_assets; ?>global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<script src="<?php echo $path_assets; ?>global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo $path_assets; ?>global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo $path_assets; ?>global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo $path_assets; ?>global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo $path_assets; ?>global/scripts/metronic.js" type="text/javascript"></script>
<script src="<?php echo $path_assets; ?>admin/scripts/layout.js" type="text/javascript"></script>
<script src="<?php echo $path_assets; ?>admin/pages/scripts/login.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
    jQuery(document).ready(function() {
        Metronic.init(); // init metronic core components
        Layout.init(); // init current layout
        Login.init();
    });
</script>
<!-- END JAVASCRIPTS -->fgh
</body>
<!-- END BODY -->
</html>