<?php
$path_assets = base_url() . "assets/backend/";
?>
<!DOCTYPE html>
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 3.3.0
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title>Recuperar Contraseña</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $path_assets; ?>global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $path_assets; ?>global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $path_assets; ?>global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $path_assets; ?>global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="<?php echo $path_assets; ?>admin/pages/css/login.css" rel="stylesheet" type="text/css"/>
    <!-- END PAGE LEVEL SCRIPTS -->
    <!-- BEGIN THEME STYLES -->
    <link href="<?php echo $path_assets; ?>global/css/components.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $path_assets; ?>global/css/plugins.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $path_assets; ?>admin/css/layout.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $path_assets; ?>admin/css/themes/grey.css" rel="stylesheet" type="text/css" id="style_color"/>
    <link href="<?php echo $path_assets; ?>admin/css/custom.css" rel="stylesheet" type="text/css"/>
    <!-- END THEME STYLES -->
    <link rel="shortcut icon" href="<?php echo $path_assets; ?>img/icons/favicon.ico"/>
    <style>
        .login .alert p{
            margin-bottom: 0 !important;
        }
    </style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login">
    <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
    <div class="menu-toggler sidebar-toggler">
    </div>
    <!-- END SIDEBAR TOGGLER BUTTON -->
    <!-- BEGIN LOGO -->
    <div class="logo">
        <a href="<?php echo base_url(); ?>">
            <img src="<?php echo $path_assets; ?>admin/img/logo-default.png" alt="logo toque el timbre"/>
        </a>
    </div>
    <!-- END LOGO -->
    <!-- BEGIN LOGIN -->
    <div class="content">
        <!-- BEGIN FORGOT PASSWORD FORM -->
        <form action="<?php echo base_url(); ?>recuperar-contrasena" method="post">
            <h3 style="margin-bottom: 15px;">Olvidaste tu contraseña?</h3>

            <?php if(@$error){ echo '<div class="alert alert-danger"><button class="close" data-close="alert"></button><span>'. $error .'</span></div>'; } ?>
            <?php if(@$error_validation){ echo '<div class="alert alert-danger"><button class="close" data-close="alert"></button><span>'. $error_validation .'</span></div>'; } ?>
            <?php if(@$ok_mail){ echo '<div class="alert alert-success"><button class="close" data-close="alert"></button><span>'. $ok_mail .'</span></div>'; } ?>

            <p>
                Ingresa tu direccion de correo electronico y te enviaremos instrucciones para restablecer tu contraseña.
            </p>
            <div class="form-group">
                <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email"/>
            </div>
            <div class="form-actions">
                <button type="submit" class="btn btn-success uppercase pull-left">Enviar</button>
            </div>
            <br style="clear: both; margin-top:6px;" />
            <div class="create-account">
                <p>
                    <a href="<?php echo base_url(); ?>login" class="uppercase">Login</a>
                </p>
            </div>
        </form>
        <!-- END FORGOT PASSWORD FORM -->
    </div>

    <div class="copyright">
        <?= date('Y')?>  &copy; Toqueeltimbre.com. Todos los derechos reservados.
    </div>
    <!-- END LOGIN -->

<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?php echo $path_assets; ?>global/plugins/respond.min.js"></script>
<script src="<?php echo $path_assets; ?>plugins/excanvas.min.js"></script>
<![endif]-->
<script src="<?php echo $path_assets; ?>global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo $path_assets; ?>global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<script src="<?php echo $path_assets; ?>global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo $path_assets; ?>global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo $path_assets; ?>global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo $path_assets; ?>global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo $path_assets; ?>global/scripts/metronic.js" type="text/javascript"></script>
<script src="<?php echo $path_assets; ?>admin/scripts/layout.js" type="text/javascript"></script>
<script src="<?php echo $path_assets; ?>admin/pages/scripts/login.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
    jQuery(document).ready(function() {
        Metronic.init(); // init metronic core components
        Layout.init(); // init current layout
        Login.init();
    });
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>