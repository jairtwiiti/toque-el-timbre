<!-- layout-->
<div id="layout" class="layout-wide">
<div class="link_direct">
    <?php if(!empty($user_logged))
    {
        $url = $user_logged->usu_tipo == "Admin"?"admin/Home":"dashboard";
    ?>
    <div class="jRibbon publicar desea_publicar my-account-button" style="">
        <a href="<?=base_url($url)?>" style="color: #fff;"><i class="fas fa-user"></i> <?=ucfirst($user_logged->usu_nombre)?></a>
    </div>
    <?php
    }
    else
    {
    ?>
        <a href="#" class="jRibbon jTrigger login my-account-button"><i class="fas fa-sign-in-alt"></i> Login / Registro</a>
    <?php
    }
    ?>
    <a href="<?=base_url("buscar/inmuebles-en-venta-en-bolivia?project-tab");?>" class="jRibbon jTrigger favoritos show-project-tab" target="_blank">PROYECTOS</a>
    <a href="<?=base_url("buscar/inmuebles-en-anticretico-en-bolivia");?>" class="jRibbon jTrigger hidden-xs favoritos" target="_blank">ANTICRETICO</a>
    <a href="<?=base_url("buscar/inmuebles-en-alquiler-en-bolivia");?>" class="jRibbon jTrigger hidden-xs favoritos" target="_blank">ALQUILER</a>
    <a href="<?=base_url("buscar/inmuebles-en-venta-en-bolivia");?>" class="jRibbon jTrigger favoritos" target="_blank">VENTA</a>
    <img src="<?=assets_url("img/logo.png")?>" class="top-responsive-logo hidden-md hidden-lg" alt="ToqueelTimbre.com" title="Toqueeltimbre.com el primer portal de anuncios inmobiliarios." style="width: 100%;" />
</div>
<!-- End Login Client -->

<!-- Info Head -->
<section class="info_head">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <ul class="list-option-header">
                    <li><i class="fa fa-headphones"></i><a href="tel:33434253">(591) + 3-3434253</a></li>
                    <li><i class="fa fa-envelope"></i><a href="mailto:info@toqueeltimbre.com">info@toqueeltimbre.com</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- Info Head -->

<!-- Nav-->
<nav class="navbar-home-page">
    <div class="container container-home-page">
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-3 logo hidden-xs hidden-sm">
            <h1 style="text-indent: 100%; white-space: nowrap; overflow: hidden; width: 100%">
                <a href="<?=base_url()?>">
                    <img src="<?=assets_url("img/logo.png")?>" class="logo_img" alt="ToqueelTimbre.com" title="Toqueeltimbre.com el primer portal de anuncios inmobiliarios." style="width: 100%;" />
                </a>
            </h1>
        </div>
    </div>
</nav>
<!-- End Nav-->