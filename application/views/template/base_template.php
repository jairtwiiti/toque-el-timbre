<?php 	
//Obtengo la referencia de la instancia de ci para poder hacer llamadas de los metodos del controlador
$CI =& get_instance();

$this->load->view ("template/cabezera");
?>
<div id="cuerpo">
<?php
$this->load->view ("template/header");
$this->load->view ("template/cuerpo");
$this->load->view ("template/footer");

?>
</div>
<?php $this->load->view ("template/endhtml"); ?>
