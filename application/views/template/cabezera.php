<?php
$path_assets = base_url() . "assets/";
$pagina = $this->uri->segment(1);
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <!-- Seo Parameters -->
    <meta charset="utf-8">
    <title><?=$title?></title>
    <meta name="keywords" content="<?=$metatag['keywords'] ?>" />
    <meta name="description" content="<?=$metatag['description'] ?>">
    <meta name="author" content="ToqueelTimbre.com" />
    <meta name="DC.Language" scheme="RFC1766" content="Spanish">
    <meta name="reply-to" content="info@toqueeltimbre.com">
    <?php
    $this->load->view("facebook-meta-tags");
    $url = "{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
    if(is_numeric(strpos(strtolower($title),"thank you page"))  ||
        is_numeric(strpos(strtolower($url),"q=true")) ||
        is_numeric(strpos(strtolower($url),"deactivateProperty")))
    {
        ?>
        <meta name="robots" content="noindex">
        <?php
    }
    ?>
    <!-- Browser Compatible -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width,  minimum-scale=1,  maximum-scale=1">
    <!-- Your styles -->
    <link href="<?=$path_assets?>css/style.css" rel="stylesheet" media="screen">
    <script defer src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>
    <link href="<?=$path_assets?>css/parsley.css" rel="stylesheet" media="screen">
    <link href="<?=$path_assets?>css/icon-fonts/av-icons.css" rel="stylesheet" media="screen">

    <!-- Skins Theme -->
    <link href="<?=$path_assets?>css/skins/blue/blue.css" rel="stylesheet" media="screen" class="skin">

    <!-- Favicons -->
    <link rel="shortcut icon" href="<?=$path_assets?>img/icons/favicon.ico">
    <link rel="apple-touch-icon" href="<?=$path_assets?>img/icons/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?=$path_assets?>img/icons/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?=$path_assets?>img/icons/apple-touch-icon-114x114.png">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- styles for IE -->
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="<?=$path_assets?>css/ie/ie.css" type="text/css" media="screen" />
    <![endif]-->

    <!-- Skins Changer-->
<!--    <script type="text/javascript" src="https://www.google.com/jsapi"></script>-->

    <link rel="stylesheet" href="<?=$path_assets?>css/jquery-ui.min.css">
    <link rel="stylesheet" href="<?=$path_assets?>css/jquery.switchButton.css">
    <link rel="stylesheet" href="<?=$path_assets?>css/idangerous.swiper.css">
    <link rel="stylesheet" href="<?=$path_assets?>css/colorbox/colorbox.css">
	<link rel="stylesheet" href="<?=$path_assets?>css/tet-tooltip.css">
    <script src="<?=$path_assets?>js/modernizr.min.js"></script>
    <script src="<?=$path_assets?>js/css3-mediaqueries.js"></script>
    <link rel="shortcut icon" href="<?=$path_assets?>img/icons/favicon.ico"/>

    <!-- External Scripts -->
    <?php if($pagina == "publicar-como-inmobiliaria"){ ?>
    <!-- Facebook Conversion Code for Registros Inmobiliarias -->
    <script>(function() {
            var _fbq = window._fbq || (window._fbq = []);
            if (!_fbq.loaded) {
                var fbds = document.createElement('script');
                fbds.async = true;
                fbds.src = '//connect.facebook.net/en_US/fbds.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(fbds, s);
                _fbq.loaded = true;
            }
        })();
        window._fbq = window._fbq || [];
        window._fbq.push(['track', '6028984950614', {'value':'0.00','currency':'USD'}]);
    </script>
    <noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6028984950614&amp;cd[value]=0.00&amp;cd[currency]=USD&amp;noscript=1" /></noscript>
    <?php }elseif($pagina == "publicar-como-particular"){ ?>
    <!-- Facebook Conversion Code for Registros Particulares -->
    <script>(function() {
            var _fbq = window._fbq || (window._fbq = []);
            if (!_fbq.loaded) {
                var fbds = document.createElement('script');
                fbds.async = true;
                fbds.src = '//connect.facebook.net/en_US/fbds.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(fbds, s);
                _fbq.loaded = true;
            }
        })();
        window._fbq = window._fbq || [];
        window._fbq.push(['track', '6028984999214', {'value':'0.00','currency':'USD'}]);
    </script>
    <noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6028984999214&amp;cd[value]=0.00&amp;cd[currency]=USD&amp;noscript=1" /></noscript>
    <?php }elseif($pagina == "publicar-proyecto-inmobiliario"){ ?>
    <!-- Facebook Conversion Code for Registro Proyectos Inmobiliarios -->
    <script>(function() {
            var _fbq = window._fbq || (window._fbq = []);
            if (!_fbq.loaded) {
                var fbds = document.createElement('script');
                fbds.async = true;
                fbds.src = '//connect.facebook.net/en_US/fbds.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(fbds, s);
                _fbq.loaded = true;
            }
        })();
        window._fbq = window._fbq || [];
        window._fbq.push(['track', '6028985031214', {'value':'0.00','currency':'USD'}]);
    </script>
    <noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6028985031214&amp;cd[value]=0.00&amp;cd[currency]=USD&amp;noscript=1" /></noscript>
    <?php } ?>
    <!-- External Scripts -->
    <script>
        var base_url ="<?=base_url()?>";
        var userRole ="<?=$user_logged->usu_tipo?>"
    </script>

    <link href="<?php echo $path_assets; ?>backend/global/plugins/select2/select2.css" rel="stylesheet" type="text/css"/>
    <?php $complementHandler->printViewCss()?>
</head>
<body>
<!-- facebook share button -->
<?php
$facebookAppIdAndSecretKey = Base_page::getFacebookAppIdAndSecretKey();
?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v2.10&appId=<?=$facebookAppIdAndSecretKey["appId"]?>";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<!-- Google Convertions -->
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-18182487-11', 'auto');
    ga('send', 'pageview');
</script>
<script type="text/javascript">
    /* <![CDATA[ */
    goog_snippet_vars = function() {
        var w = window;
        w.google_conversion_id = 1010835860;
        w.google_conversion_label = "rffJCPDuwWUQlMOA4gM";
        w.google_remarketing_only = false;
    }
    // DO NOT CHANGE THE CODE BELOW.
    goog_report_conversion = function(url) {
        goog_snippet_vars();
        window.google_conversion_format = "3";
        var opt = new Object();
        opt.onload_callback = function() {
            if (typeof(url) != 'undefined') {
                //window.location = url;
            }
        }
        var conv_handler = window['google_trackConversion'];
        if (typeof(conv_handler) == 'function') {
            conv_handler(opt);
        }
    }
    /* ]]> */
</script>
<script type="text/javascript"
        src="//www.googleadservices.com/pagead/conversion_async.js">
</script>
<!-- Google Convertions -->

<!-- Facebook Pixel Code -->
<script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window,document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '1481111128837486');
    fbq('track', 'PageView');
</script>
<noscript>
    <img height="1" width="1"
         src="https://www.facebook.com/tr?id=1481111128837486&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->
