<?php
$path_assets = base_url() . "assets/";
?>

</div>
<!-- End layout-->

<!-- ======================= JQuery libsss =========================== -->
<!-- Core JS Libraries -->
<script src="<?php echo $path_assets; ?>js/jquery.min.js"></script>
<!--<script src="--><?php //echo $path_assets; ?><!--js/URL_Builder.js"></script>-->
<script src="<?php echo $path_assets; ?>js/parsley.min.js"></script>
<script src="<?php echo $path_assets; ?>js/parsley.spanish.js"></script>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<!--Nav-->
<script type="text/javascript" src="<?php echo $path_assets; ?>js/nav/tinynav.js"></script>
<script type="text/javascript" src="<?php echo $path_assets; ?>js/nav/superfish.js"></script>
<script type="text/javascript" src="<?php echo $path_assets; ?>js/nav/hoverIntent.min.js"></script>
<!--Totop-->
<script type="text/javascript" src="<?php echo $path_assets; ?>js/totop/jquery.ui.totop.js" ></script>

<!--owlcarousel-->
<script type='text/javascript' src='<?php echo $path_assets; ?>js/owlcarousel/owl.carousel.js'></script>
<!--efect_switcher-->
<script type='text/javascript' src='<?php echo $path_assets; ?>js/efect_switcher/jquery.content-panel-switcher.js'></script>
<!--Theme Options-->
<!--<script type="text/javascript" src="--><?php //echo $path_assets; ?><!--js/theme-options/theme-options.js"></script>-->
<script type="text/javascript" src="<?php echo $path_assets; ?>js/theme-options/jquery.cookies.js"></script>
<!-- Bootstrap.js-->
<script src="<?php echo $path_assets; ?>js/bootstrap/bootstrap.min.js"></script>

<script type="text/javascript" src="<?php echo $path_assets; ?>js/animate.js" ></script>

<!--Slide-->
<script type="text/javascript" src="<?php echo $path_assets; ?>js/slide/camera.min.js" ></script>
<script type='text/javascript' src='<?php echo $path_assets; ?>js/slide/jquery.easing.1.3.min.js'></script>


<script src="<?php echo $path_assets; ?>js/jquery-ui.min.js"></script>
<script src="<?php echo $path_assets; ?>js/jquery.ui.touch-punch.min.js"></script>
<script src="<?php echo $path_assets; ?>js/jquery.switchButton.js"></script>
<script src="<?php echo $path_assets; ?>js/numeral.min.js"></script>
<script src="<?php echo $path_assets; ?>js/jquery.colorbox-min.js"></script>
<script src="<?php echo $path_assets; ?>js/jQuery.print.min.js"></script>
<?php
//if(!empty($_GET)) {
    //$filter = json_decode(urldecode($_GET["q"]));
    $filter_config = translate_decode_url($_GET);
    $filter = json_decode(json_encode($filter_config), FALSE);
    $filter = get_filters_segments($filter);
//}else{
    //$filter = $back_search;
//}
?>
<script>
    $(document).ready(function($) {
	    var var_navigable_home_page = <?= json_encode($var_navigable) ?>;;
        //=================================== Slider Range  ===================================//
        $( "#slider-range" ).slider({
            range: true,
            min: 0,
            <?php if(strtolower($filter->in) == "alquiler"){ ?>
                max: 5000,
                <?php
                if($filter->price_min != null && $filter->price_max != null){
                    echo 'values: [ '.$filter->price_min.', '.$filter->price_max.' ],';
                }else{
                    echo 'values: [ 0, 5000 ],';
                }
                ?>
                step: 100,
            <?php }elseif(strtolower($filter->in) == "anticretico"){ ?>
                max: 150000,
                <?php
                if($filter->price_min != null && $filter->price_max != null){
                    echo 'values: [ '.$filter->price_min.', '.$filter->price_max.' ],';
                }else{
                    echo 'values: [ 0, 150000 ],';
                }
                ?>
                step: 1000,
            <?php }elseif(strtolower($filter->in) == "comprar" || strtolower($filter->in) == "venta" || $filter->in == null){ ?>
                max: 500000,
                <?php
                if($filter->price_min != null && $filter->price_max != null){
                    echo 'values: [ '.$filter->price_min.', '.$filter->price_max.' ],';
                }else{
                    echo 'values: [ 0, 500000 ],';
                }
                ?>
                step: 10000,
            <?php } ?>
            slide: function( event, ui ) {
                var string1 = numeral(ui.values[ 0 ]).format('0,0');
                var string2 = numeral(ui.values[ 1 ]).format('0,0');
                if(string2 == "500,000"){
                    string2 = "mayor a $" + string2;
                }else{
                    var forma = "<?php echo strtolower($filter->in); ?>";
                    if(string2 == "150,000" && forma != "venta"){
                        string2 = "mayor a $" + string2;
                    }else{
                        if(string2 == "5,000" && forma != "anticretico"){
                            string2 = "mayor a $" + string2;
                        }else{
                            string2 = "$" + string2;
                        }
                    }
                }
                $("#amount").val( "$" + string1 + " - " + string2 );
                $("#price_min").val(ui.values[0]);
                $("#price_max").val(ui.values[1]);
            }
        });
        if($("#slider-range").length != 0) {
            var ini_1 = $("#slider-range").slider("values", 0);
            var string1 = numeral(ini_1).format('0,0');
            var ini_2 = $("#slider-range").slider("values", 1);
            var string2 = numeral(ini_2).format('0,0');
            if(string2 == "5,000" || string2 == "150,000" || string2 == "500,000"){
                string2 = "mayor a $" + string2;
            }
            $("#amount").val("$" + string1 + " - " + string2);
            $("#price_min").val(ini_1);
            $("#price_max").val(ini_2);
        }

	    // Tooltip only Text
	    $('.masterTooltip').hover(function(){
		    $(this).parent().find('div.tet-tooltip').css({display:'block'});
	    }, function() {
		    $(this).parent().find('div.tet-tooltip').css({display:'none'});
	    }).mousemove(function(e) {
		    /*var mousex = e.pageX + 20; //Get X coordinates
		    var mousey = e.pageY + 10; //Get Y coordinates
		    $('.tet-tooltip')
			    .css({ top: mousey, left: mousex })*/
	    });

    });
    <?php
    $publications  = !is_array($publications)?array():$publications;
    foreach($publications as $loc)
    {
        if(!empty($loc->inm_latitud) && !empty($loc->inm_longitud)){
            $loc->cat_nombre = ucwords(strtolower($loc->cat_nombre));
            //if($loc->cat_nombre == "Departamentos"){ $loc->cat_nombre = "Depto."; }elseif($loc->cat_nombre == "Casas"){ $loc->cat_nombre = "Casa"; }elseif($loc->cat_nombre == "OFICINAS"){ $loc->cat_nombre = "Oficina"; }elseif($loc->cat_nombre == "LOCALES COMERCIALES"){ $loc->cat_nombre = "Local"; }elseif($loc->cat_nombre == "TERRENOS"){ $loc->cat_nombre = "Terreno"; }elseif($loc->cat_nombre == "QUINTAS Y PROPIEDADES"){ $loc->cat_nombre = "Propiedad"; }
            $loc->for_descripcion = ucwords(strtolower($loc->for_descripcion));
            $loc->inm_precio = number_format($loc->inm_precio,0);

            $url_detalle = base_url();
            $url_detalle .= seo_url($loc->ciu_nombre)."/";
            $url_detalle .= seo_url($loc->cat_nombre . " EN ". $loc->for_descripcion)."/";
            $url_detalle .= seo_url($loc->inm_seo).".html";
            $url_detalle = base_url("buscar/".seo_url($loc->cat_nombre."-en-".$loc->for_descripcion."-en-".$loc->dep_nombre)."/".seo_url($loc->inm_seo));
            $loc->inm_seo = $url_detalle;

            $locations[] = array($loc->inm_nombre,$loc->inm_latitud,$loc->inm_longitud,$loc->fot_archivo,$loc->cat_nombre,$loc->for_descripcion,$loc->inm_direccion,$loc->inm_precio,$loc->mon_abreviado,$loc->inm_seo);
        }
    }
    $locations = json_encode($locations);
    switch($filter->city){
        case "Santa Cruz":
            $latitud = '-17.783477';
            $longitud = '-63.182030';
            $zoom = 13;
            break;
        case "La Paz":
            $latitud = '-16.495440';
            $longitud = '-68.133524';
            $zoom = 13;
            break;
        case "Cochabamba":
            $latitud = '-17.405031';
            $longitud = '-66.161816';
            $zoom = 13;
            break;
        case "Tarija":
            $latitud = '-21.522661';
            $longitud = '-64.727975';
            $zoom = 13;
            break;
        case "Chuquisaca":
            $latitud = '-19.020404';
            $longitud = '-65.262453';
            $zoom = 13;
            break;
        case "Oruro":
            $latitud = '-17.966148';
            $longitud = '-67.105995';
            $zoom = 13;
            break;
        case "Potosi":
            $latitud = '-19.573560';
            $longitud = '-65.754837';
            $zoom = 13;
            break;
        case "Pando":
            $latitud = '-11.023708';
            $longitud = '-68.766557';
            $zoom = 13;
            break;
        case "Beni":
            $latitud = '-14.827909';
            $longitud = '-64.900474';
            $zoom = 13;
            break;
        default:
            $latitud = '-17.783477';
            $longitud = '-63.182030';
            $zoom = 7;
            break;
    }
    ?>
    var locations = <?php echo $locations; ?>;
    var dep_latitud = <?php echo $latitud; ?>;
    var dep_longitud = <?php echo $longitud; ?>;
    var zoom = <?php echo $zoom; ?>;
</script>

<!--fUNCTIONS-->
<script type="text/javascript" src="<?php echo $path_assets; ?>js/main.js"></script>
<!-- ======================= End JQuery libs =========================== -->
<?php $complementHandler->printViewJs()?>
</body>
</html>