<form id="form_search_home" method="post" action="<?php echo base_url(); ?>buscar">
<!-- filter-horizontal -->
<div id="mobile" class="filter_horizontal">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 style="color: #fff; text-align: center; font-weight: bold; font-size: 27px; margin: 9px 0 14px 0; line-height: 40px">Busca tu inmueble de forma confiable, eficiente y amigable.</h3>
            </div>
        </div>
        <div class="row">
                <div class="col-md-2">
                    <p class="filter_title"></p>
                </div>
                <div class="col-md-2">
                    <div style="background-color: #fff; width: 100%">
                        <div class="select_background">
                            <select name="type">
                                <!--<option value="" selected>Seleccionar...</option>-->
                                <?php foreach($categories as $category): ?>
                                    <option value="<?php echo ucwords(strtolower($category['cat_nombre'])); ?>"><?php echo ucwords(strtolower($category['cat_nombre'])); ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div style="background-color: #fff; width: 100%">
                        <div class="select_background">
                            <select name="in">
                                <!--<option value="" <?php echo $page == "" ? 'selected="selected"' : '';  ?>>Seleccionar...</option>-->
                                <?php foreach($forma as $form): ?>
                                    <option value="<?php echo ucwords(strtolower($form['for_descripcion'])); ?>" <?php if(strtolower($form['for_descripcion']) == $page): ?> selected <?php endif; ?>><?php echo ucwords(strtolower($form['for_descripcion'])); ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div style="background-color: #fff; width: 100%">
                        <div class="select_background">
                            <select name="city">
                                <option value="Bolivia">Toda Bolivia...</option>
                                <?php foreach($states as $state): ?>
                                    <option value="<?php echo $state['dep_nombre']; ?>"><?php echo ucwords(strtolower($state['dep_nombre'])); ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>
                <!--<div class="col-md-2">
                    <input type="text" name="price_min" placeholder="Desde:" />
                </div>
                <div class="col-md-2">
                    <input type="text" name="price_max" placeholder="Hasta:" />
                </div>
                <div class="col-md-2">
                    <select name="currency">
                        <option value="Dolares">Dolares</option>
                        <option value="Bolivianos">Bolivianos</option>
                    </select>
                </div>-->
                <div class="col-md-2">
                    <div id="button_search_home" style="background-color: #0083b3 !important; width: 100%; color: #fff !important; font-size: 16px; cursor: pointer; padding: 9px 10px; font-weight: bold; text-align: center;">
                        <i class="fa fa-search"></i> Buscar
                    </div>
                </div>
                <div class="col-md-2">
                    <p class="filter_title"></p>
                </div>
        </div>
    </div>
</div>
<!-- End filter-horizontal -->
</form>