<?php
$path_assets = base_url() . "assets/";
?>

<!-- Footer area - footer_top -->
<footer class="section_area footer_top">
    <div class="container center">
        <h2>¿Deseas publicar tu inmueble?</h2>
        <!--<p>Si tienes alguna duda acerca de nuestros servicios, puedes acceder a nuestras “Preguntas Frecuentes” o puedes contactarte con alguno de nuestros ejecutivos.</p>-->
        <p>Selecciona la forma de anunciar que sea más conveniente para vos.</p>

        <a href="<?php echo base_url(); ?>publicar-como-inmobiliaria" class="button">Como inmobiliaria</a>
        <a href="<?php echo base_url(); ?>publicar-como-particular" class="button">Como particular</a>
        <a href="<?php echo base_url(); ?>publicar-proyecto-inmobiliario" class="button">Proyecto inmobiliario</a>

        <!--<a href="<?php echo base_url(); ?>preguntas-frecuentes" class="button">Preguntas Frecuentes</a>
        <a href="<?php echo base_url(); ?>videotutoriales" class="button">Videotutoriales</a>-->
        <br><br>
    </div>
</footer>
<!-- End Footer area - footer_top -->



<!-- Footer area - footer_medium -->
<footer class="section_area footer_medium">
    <div class="container">
        <div class="row">

            <!-- Contact Footer -->
            <div class="col-md-3 col-sm-6">
                <h3>Contactenos</h3>
                <ul class="contact_footer">
                    <li style="font-weight: normal;">
                        Avenida Beni Calle Mururé 2010
                    </li>
                    <li>
                        <a href="tel:33434253">Telf. (591) 78526002</a>
                    </li>
                    <li>
                        <a href="mailto:info@toqueeltimbre.com">info@toqueeltimbre.com</a>
                    </li>
                    <li style="font-weight: normal;">
                        Santa Cruz - Bolivia
                    </li>
                </ul>
            </div>
            <!-- End Contact Footer -->

            <!-- Recent Links -->
            <div class="col-md-3 col-sm-6 links">
                <h3>Menu</h3>
                <ul>
                    <li><a href="<?php echo base_url(); ?>acerca-de-toqueeltimbre">Quienes Somos</a></li>
                    <li><a href="http://blog.toqueeltimbre.com">Blog</a></li>
                    <li><a href="<?php echo base_url(); ?>preguntas-frecuentes">Preguntas Frecuentes</a></li>
                    <li><a href="<?php echo base_url(); ?>formas-de-pago">Formas de Pago</a></li>
                    <li><a href="<?php echo base_url(); ?>politicas-de-privacidad">Politicas de Privacidad</a></li>
                    <li><a href="<?php echo base_url(); ?>contactenos">Contactenos</a></li>
                </ul>
            </div>
            <!-- End Recent Links -->

            <!-- Tags -->
            <div class="col-md-3 padding_items hide">
                <h3>Mas mencionados</h3>
                <ul class="tags">
                    <?php
                    $user_name = "toqueelt_beta";
                    $password = "TzKe0MIxSoOT";
                    $database = "toqueelt_drupalblog";
                    $server = "localhost";
                    $db_handle = mysqli_connect($server, $user_name, $password,$database);
                    //$db_found = mysql_select_db($database, $db_handle);
                    if ($db_handle):
                        $sql = "select * from taxonomy_term_data";
                        $result = mysqli_query($db_handle, $sql);
                        while($row = mysqli_fetch_object($result)): ?>
                            <li><a href="http://blog.toqueeltimbre.com/tags/<?php echo $row->name; ?>" target="_blank"><?php echo $row->name; ?></a></li>
                    <?php
                        endwhile;
                        mysqli_close($db_handle);
                    endif; ?>
                </ul>
            </div>
            <!-- End Tags -->

            <!-- Testimonials -->
            <div class="col-md-3">
                <?php if(!empty($testimonials)): ?>
                <ul class="testimonial-carousel">
                    <?php foreach($testimonials as $obj): ?>
                    <!-- Item Testimonial -->
                    <li>
                        <div class="testimonials">
                            <p><?php echo $obj->tes_descripcion; ?></p>
                            <span class="arrow_testimonials"></span>
                        </div>
                        <h6 class="testimonial_autor"><?php echo $obj->tes_titulo; ?></h6>
                    </li>
                    <!-- Item Testimonial -->
                    <?php endforeach; ?>
                </ul>
                <?php endif; ?>
            </div>
            <!-- End Testimonials -->
        </div>
    </div>
</footer>
<!-- End Footer area - footer_medium -->

<!-- Footer area - footer_down -->
<footer class="section_area footer_down">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <p> <?= date('Y')?>  &copy; Toqueeltimbre.com. Todos los derechos reservados.</p>
            </div>
            <div class="col-md-6">
                <ul class="social tooltip-demo">
                    <li title="Facebook">
                        <a href="https://www.facebook.com/ToqueelTimbre/" target="_blank"><i class="fab fa-facebook-f"></i></a>
                    </li>
                    <li title="Twitter">
                        <a href="https://twitter.com/#!/ToqueElTimbreBO" target="_blank"><i class="fab fa-twitter"></i></a>
                    </li>
                    <li title="Youtube">
                        <a href="http://www.youtube.com/channel/UCTA7OVG82_pTTc9iEeHLCGg" target="_blank"><i class="fab fa-youtube"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<!-- End Footer area- footer_down -->

<?php if(!empty($inmueble)): ?>
    <div style="position:fixed; bottom:10px; left: 10px; z-index: 99999;">
        <a id="toMail" href="#Formulario"></a>
        <?php if(!empty($inmueble->usu_telefono) || !empty($inmueble->usu_celular)): ?>
            <a id="toPhone" class="btn_phone_call" href="tel:<?php echo !empty($inmueble->usu_celular) ? $inmueble->usu_celular : $inmueble->usu_telefono; ?>" rel="<?php echo $inmueble->id; ?>"></a>
        <?php endif; ?>
    </div>
<?php endif; ?>

<?php if(!empty($project_data["project"]->usu_telefono) || !empty($project_data["project"]->usu_celular)): ?>
    <div style="position:fixed; bottom:10px; left: 10px; z-index: 99999;">
        <a id="toMail" href="#Formulario"></a>
        <a id="toPhone" class="btn_phone_call" href="tel:<?php echo !empty($project_data["project"]->usu_telefono) ? $project_data["project"]->usu_telefono : $project_data["project"]->usu_celular; ?>"></a>
    </div>
<?php endif; ?>

<?php
if(!empty($project_data["tipologia"])) {
    ?>
    <div class="modal_detail tipologia_detail_modal" style="background: rgba(0,0,0,0.9) !important;">
        <div class="content_info content_info_search" style="background: none;">
            <div class="container" style="background-color: #fff; position: relative;">
                <div class="modal_close" style="position: absolute; right: 2%; top: 0; z-index: 999;">
                    <div style="margin: 9px 0;">
                        <img src="<?php echo $path_assets; ?>img/icons/close.png" width="35"/>
                    </div>
                </div>
                <div class="modal_content" style="padding-top: 15px; padding-bottom: 15px;"></div>
            </div>
        </div>
    </div>
<?php
}
?>