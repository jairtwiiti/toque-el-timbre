<?php
$path_assets = base_url() . "assets/";
?>

<!-- layout-->
<div id="layout" class="layout-wide">
<div class="link_direct">
    <?php if(!empty($user_logged))
    {
        $url = $user_logged->usu_tipo == "Admin"?"admin/Home":"dashboard";
    ?>
    <div class="jRibbon publicar desea_publicar" style="min-width:125px;text-align: center; background-color: #00adee !important;">
        <a href="javascript:;" style="color: #fff;"><?=ucfirst($user_logged->usu_nombre)?></a>
        <ul style="text-align: left;">
            <li><a href="<?=base_url($url)?>"><i class="fas fa-tachometer-alt"></i> Dashboard</a></li>
            <li><a href="<?=base_url('logout')?>"><i class="fas fa-power-off"></i> Cerrar sesión</a></li>
        </ul>
    </div>
        <?php
    }
    else
    {
    ?>
        <a href="<?=base_url("login")?>" class="jRibbon jTrigger login">Iniciar Sesión</a>
    <?php
    }
    ?>
        <div class="jRibbon publicar desea_publicar" style="background-color: #53585a !important;">
            <a href="<?=base_url("admin/Property/add")?>" style="color: #fff;">¡Publica Ya!</a>
        </div>
    <?php
    if(!empty($user_logged))
    {
    ?>
        <a href="<?=base_url("mis-favoritos");?>" class="jRibbon jTrigger favoritos" target="_blank">Mis Favoritos</a>
    <?php
    }
    ?>
<!--    <div class="jRibbon publicar desea_publicar" style="background-color: #4267b2 !important;">-->
<!--        <div class="fb-share-button" data-href="--><?//= base_url()?><!--" data-layout="button" data-size="large" data-mobile-iframe="true">-->
<!--            <a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse">-->
<!--                Compartir-->
<!--            </a>-->
<!--        </div>-->
<!--    </div>-->
</div>

<div class="line"></div>
<!-- End Login Client -->

<!-- Info Head -->
<section class="info_head">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <ul class="list-option-header">
                    <li><i class="fa fa-headphones"></i><a href="tel:33434253">(591) + 3-3434253</a></li>
                    <li><i class="fa fa-envelope"></i><a href="mailto:info@toqueeltimbre.com">info@toqueeltimbre.com</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- Info Head -->


<!-- Nav-->
<nav>
    <div class="container">
        <div class="row header-content">
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 logo">
                <h1 style="text-indent: 100%; white-space: nowrap; overflow: hidden; width: 100%">
                    Toqueeltimbre.com el primer portal de anuncios inmobiliarios
                    <a href="<?php echo base_url(); ?>">
                        <img src="<?php echo $path_assets; ?>img/logo.png" class="logo_img" alt="ToqueelTimbre.com" title="Toqueeltimbre.com el primer portal de anuncios inmobiliarios." style="width: 100%;" />
                    </a>
                </h1>
            </div>
            <?php if($search_template){ ?>
                <?php if(!$mobile){ ?>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
						&nbsp;
                    </div>
		            <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 breadcrumbs-content">
				            <?= $breacrumbs ?>
		            </div>
                <?php }else{ ?>
                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 filters_top" style="height:52px;">
                        <span class="button" style="margin-right: 6px; position: relative;">
                            Ordenar
                            <form id="form-sort" action="<?php echo base_url(); ?>search/sort" method="post" style="opacity: 0; height: 35px; left: 0; position: absolute; top: 0; width: 107px;">
                                <select id="sort-type" name="sort-type" style="width: 107px; padding-left: 0; height: 35px;">
                                    <option value="date-desc" <?php echo $_GET['ordenar'] == "date-desc" ? 'selected':''; ?>>Fecha (Descendentemente)</option>
                                    <option value="date-asc" <?php echo $_GET['ordenar'] == "date-asc" ? 'selected':''; ?>>Fecha (Ascendentemente)</option>
                                    <option value="price-desc" <?php echo $_GET['ordenar'] == "price-desc" ? 'selected':''; ?>>Precio (Mayor a Menor)</option>
                                    <option value="price-asc" <?php echo $_GET['ordenar'] == "price-asc" ? 'selected':''; ?>>Precio (Menor a Mayor)</option>
                                </select>
                            </form>
                        </span>
                        <a id="search_button" href="javascript:;" class="button">Filtro</a>
                        <?php
                        if((isset($_GET["tipo"]) && $_GET["tipo"] != "mapa") || !isset($_GET["tipo"]))
                        {
                            ?>
                            <a href="#" data-layout="" data-display="mapa" class="button mapa layout-type" >
                                Ver Mapa
                            </a>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 filters_top hidden-sm hidden-md hidden-lg" style="height:52px;">
                        <h5 style="margin-bottom: 0px; padding-bottom: 0px; float: left;"><?php echo $title_search; ?> <span>(<?php echo $total_rows > 0 ? $total_rows : 0; ?>)</span></h5>
                    </div>
                <?php } ?>
            <?php } else { ?>
	            <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 breadcrumbs-content">
		            <?= $breacrumbs ?>
	            </div>
            <?php }?>
        </div>
    </div>
</nav>
<!-- End Nav-->
