<?php
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 28/7/2017
 * Time: 9:51 AM
 */
    if($this->session->flashdata("errorMessage")) { ?>
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
            &times;
        </button>
        <i class="fa fa-times-circle sign"></i><strong>Error!</strong> <?php echo $this->session->flashdata('errorMessage'); ?>
    </div>
<?php } if($this->session->flashdata('successMessage')) { ?>
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
            &times;
        </button>
        <i class="fa fa-check sign"></i><strong>Success!</strong> <?php echo $this->session->flashdata('successMessage'); ?>
    </div>
<?php } if ($this->session->flashdata('alertMessage')) { ?>
    <div class="alert alert-warning">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
            &times;
        </button>
        <i class="fa fa-warning sign"></i><strong>Alert!</strong> <?php echo $this->session->flashdata('alertMessage'); ?>
    </div>
<?php } if ($this->session->flashdata('infoMessage')) { ?>
    <div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
            &times;
        </button>
        <i class="fa fa-info-circle sign"></i><strong>Info!</strong> <?php echo $this->session->flashdata('infoMessage'); ?>
    </div>
<?php } if(isset($errorMessage)) { ?>
    <div class="alert alert-warning">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <i class="fa fa-warning sign"></i><strong>Alert!</strong> <?php echo $errorMessage;?>
    </div>
<?php } if(validation_errors()) { ?>
    <div class="alert alert-warning">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <i class="fa fa-warning sign"></i><strong>Alert!</strong> <?php echo validation_errors();?>
    </div>
<?php } ?>