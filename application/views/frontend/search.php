<?php
list($propertyType, $transactionType, $state) = explode('-en-', $mainFilter);
//echo"<pre>";var_dump($propertyType, $transactionType, $state);exit;
$propertyTypes = array(
    "inmuebles" => "Inmuebles",
    "casas" => "Casas",
    "departamentos" => "Departamentos",
    "oficinas" => "Oficinas",
    "locales-comerciales" => "Locales comerciales",
    "terrenos" => "Terrenos",
    "quintas-y-propiedades" => "Quintas y propiedades",
);

$transactionTypes = array(
    "venta" => "Venta",
    "alquiler" => "Alquiler",
    "anticretico" => "Anticretico",
);

$states = array(
    "bolivia" => "Bolivia",
    "santa-cruz" => "Santa cruz",
    "la-paz" => "La Paz",
    "cochabamba" => "Cochabamba",
    "tarija" => "Tarija",
    "chuquisaca" => "Chuquisaca",
    "oruro" => "Oruro",
    "potosi" => "Potosi",
    "pando" => "Pando",
    "beni" => "Beni"
);

?>
<!-- End content info -->
<section class="content_info content_info_search mobile-full-width" style="padding-top: 0; border-top: 5px solid #00aeef;">
<div class="container">
    <div class="row">
        <!-- Aside -->
        <div class="col-md-3">
            <!-- Search Advance -->
            <aside>
                <div class="search_advance">
                    <ul class="tabs_services">
                        <li><a id="1" class="set2">Mejora tu búsqueda </a><a href="#" class="enhance-search pull-right"><i class="fas fa-sliders-h"></i></a></li>
                    </ul>
                    <!-- 1-content -->
                    <div id="1-content" class="set2 show">
                        <div class="search_box">
                            <form action="<?php echo base_url() . "buscar" ?>" method="post" data-advanced-search-web-form>
                                <div class="col-md-12">
                                    <label>Estoy buscando...</label>
                                    <hr class="search-label-underline">
                                    <?php
                                    $htmlPropertyTypes = "";
                                    $i = 0;
                                    foreach ($propertyTypes as $id => $value)
                                    {
                                        if($i == 3)
                                        {
                                            $htmlPropertyTypes .= '<div class="more-options" style="display: none;">';
                                        }
                                        $label = strtolower($value)== "inmuebles"?"Todos los tipos":$value;
                                        $checked = $id == strtolower($propertyType)?"checked":"";
                                        $htmlPropertyTypes .= '
                                            <div class="radio radio-primary">
                                                <input type="radio" name="type" id="'.$id.'" value="'.$value.'" '.$checked.'>
                                                <label for="'.$id.'">
                                                    '.$label.'
                                                </label>
                                            </div>
                                        ';
                                        $i++;
                                        if($i == count($propertyTypes))
                                        {
                                            $htmlPropertyTypes .= '</div>';
                                        }
                                    }
                                    echo $htmlPropertyTypes;
                                    ?>
                                    <a href="#" class="show-more-options"><i class="fa fa-plus-square" aria-hidden="true"></i> Más opciones..</a>
                                    <a href="#" class="show-more-options" style="display: none;"><i class="fa fa-minus-square" aria-hidden="true"></i> Menos opciones..</a>
                                </div>
                                <div class="col-md-12">
                                    <label>En...</label>
                                    <hr class="search-label-underline">
                                    <?php
                                    $htmlTransactionTypes = "";
                                    $i = 0;
                                    foreach ($transactionTypes as $id => $value)
                                    {
                                        $label = $value;
                                        $checked = $id == strtolower($transactionType)?"checked":"";
                                        $htmlTransactionTypes .= '
                                            <div class="radio radio-primary">
                                                <input type="radio" name="in" id="'.$id.'" value="'.$value.'" '.$checked.'>
                                                <label for="'.$id.'">
                                                    '.$label.'
                                                </label>
                                            </div>
                                        ';
                                        $i++;
                                    }
                                    echo $htmlTransactionTypes;
                                    ?>
                                </div>
                                <div class="col-md-12">
                                    <label>En la ciudad de...</label>
                                    <hr class="search-label-underline">
                                    <?php
                                    $htmlStates = "";
                                    $i = 0;
                                    foreach ($states as $id => $value)
                                    {
                                        if($i == 3)
                                        {
                                            $htmlStates .= '<div class="more-options" style="display: none;">';
                                        }
                                        $label = strtolower($value)== "bolivia"?"Todas las ciudades":$value;
                                        $checked = $id == strtolower($state)?"checked":"";
                                        $stateId = $i == 0?"":$i;
                                        $htmlStates .= '
                                            <div class="radio radio-primary">
                                                <input data-state-id="'.$stateId.'"  type="radio" name="city" id="'.$id.'" value="'.$value.'" '.$checked.'>
                                                <label for="'.$id.'">
                                                    '.$label.'
                                                </label>
                                            </div>
                                        ';
                                        $i++;
                                        if($i == count($states))
                                        {
                                            $htmlStates .= '</div>';
                                        }
                                    }
                                    echo $htmlStates;
                                    ?>
                                    <a href="#" class="show-more-options"><i class="fa fa-plus-square" aria-hidden="true"></i> Más opciones..</a>
                                    <a href="#" class="show-more-options" style="display: none;"><i class="fa fa-minus-square" aria-hidden="true"></i> Menos opciones..</a>
                                </div>
                                <div class="col-md-12">
                                    <label>En la zona..</label>
                                    <hr class="search-label-underline">
                                    <div class="form-group">
                                        <input id="ajax-get-zones" name="zone" parsley-trigger="change" value=''>
                                        <em class="min-max-label choose-state-label">Elige un departamento para seleccionar zonas</em>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <label>Moneda</label>
                                    <hr class="search-label-underline">
                                    <div class="form-group">
                                        <div class="new-group-buttons currency">
                                            <label class="btn btn-primary btn-circle active" for="dolares">
                                                <input type="radio" name="currency" value="Dolares" id="dolares" checked>USD
                                            </label>
                                            <label class="btn btn-primary btn-circle" for="bob">
                                                <input type="radio" name="currency" value="Bolivianos" id="bob">BOB
                                            </label>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <label>Precio</label>
                                    <hr class="search-label-underline">
                                    <div class="row">
                                        <?php
                                        $inputMasked = $this->agent->is_mobile()?"number":"text";
                                        ?>
                                        <div class="col-md-4" style="padding-right: 0px;">
                                            <div class="form-group">
                                                <input type="<?=$inputMasked?>" name="price_min" value="0" id="price_min" class="form-control input-masked" placeholder="Desde" data-inputmask="'alias': 'integer', 'groupSeparator': ',', 'autoGroup': true" style="text-align: right;">
                                                <em class="min-max-label">Min</em>
                                            </div>
                                        </div>
                                        <div class="col-md-4" style="padding-right: 0px;">
                                            <div class="form-group">
                                                <input type="<?=$inputMasked?>" name="price_max" value="500000" id="price_max" class="form-control input-masked" placeholder="Hasta" data-inputmask="'alias': 'integer', 'groupSeparator': ',', 'autoGroup': true" style="text-align: right;">
                                                <em class="min-max-label">Max</em>
                                            </div>
                                        </div>
                                        <div class="col-md-2" style="padding-right: 0px;">
                                            <div class="form-group">
                                                <button type="button" class="btn btn-primary accept-price-range" style="color: #fff;padding: 2px 9px 9px;border: 2px solid;">Ok</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <label>Dormitorios mínimos</label>
                                    <hr class="search-label-underline">
                                    <div class="form-group">
                                        <div class="new-group-buttons">
                                            <label class="btn btn-primary btn-circle" for="1-room">
                                                <input type="radio" name="room" value="1" id="1-room">1
                                            </label>
                                            <label class="btn btn-primary btn-circle" for="2-rooms">
                                                <input type="radio" name="room" value="2" id="2-rooms">2
                                            </label>
                                            <label class="btn btn-primary btn-circle" for="3-rooms">
                                                <input type="radio" name="room" value="3" id="3-rooms">3
                                            </label>
                                            <label class="btn btn-primary btn-circle" for="4-rooms">
                                                <input type="radio" name="room" value="4" id="4-rooms">4
                                            </label>
                                            <label class="btn btn-primary btn-circle" for="5-rooms">
                                                <input type="radio" name="room" value="5+" id="5-rooms">5+
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <label>Baños</label>
                                    <hr class="search-label-underline">
                                    <div class="form-group">
                                        <div class="new-group-buttons">
                                            <label class="btn btn-primary btn-circle" for="1-bathroom">
                                                <input type="radio" name="bathroom" value="1" id="1-bathroom">1
                                            </label>
                                            <label class="btn btn-primary btn-circle" for="2-bathroom">
                                                <input type="radio" name="bathroom" value="2" id="2-bathroom">2
                                            </label>
                                            <label class="btn btn-primary btn-circle" for="3-bathroom">
                                                <input type="radio" name="bathroom" value="3" id="3-bathroom">3
                                            </label>
                                            <label class="btn btn-primary btn-circle" for="4-bathroom">
                                                <input type="radio" name="bathroom" value="4" id="4-bathroom">4
                                            </label>
                                            <label class="btn btn-primary btn-circle" for="5-bathroom">
                                                <input type="radio" name="bathroom" value="5+" id="5-bathroom">5+
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <label>Parqueo</label>
                                    <hr class="search-label-underline">
                                    <div class="form-group">
                                        <div class="new-group-buttons">
                                            <label class="btn btn-primary btn-circle" for="1-parking">
                                                <input type="radio" name="parking" value="1" id="1-parking">1
                                            </label>
                                            <label class="btn btn-primary btn-circle" for="2-parking">
                                                <input type="radio" name="parking" value="2" id="2-parking">2
                                            </label>
                                            <label class="btn btn-primary btn-circle" for="3-parking">
                                                <input type="radio" name="parking" value="3" id="3-parking">3
                                            </label>
                                            <label class="btn btn-primary btn-circle" for="4-parking">
                                                <input type="radio" name="parking" value="4" id="4-parking">4
                                            </label>
                                            <label class="btn btn-primary btn-circle" for="5-parking">
                                                <input type="radio" name="parking" value="5+" id="5-parking">5+
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 hide">
                                    <label>Amenidades</label>
                                    <hr class="search-label-underline">
                                    <?php
                                    $html = "";
                                    $i = 0;
                                    foreach ($amenityList as $amenity)
                                    {
                                        $html .= '
                                            <div class="checkbox checkbox-primary">
                                                <input type="checkbox" name="amenities[]" id="amenity-'.$amenity['code'].'" value="'.$amenity['code'].'">
                                                <label for="amenity-'.$amenity['code'].'">
                                                    '.$amenity['name'].'
                                                </label>
                                            </div>
                                        ';
                                        $i++;
                                    }
                                    echo $html;
                                    ?>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="more-options" style="display: none;">
                                            <div class="col-md-12">
                                                <label>Estado...</label>
                                                <hr class="search-label-underline">
                                                <div class="radio radio-primary">
                                                    <input type="radio" name="status" id="sin-definir" value="" checked>
                                                    <label for="sin-definir">
                                                        Sin definir
                                                    </label>
                                                </div>
                                                <div class="radio radio-primary">
                                                    <input type="radio" name="status" id="pre-venta" value="pre-venta">
                                                    <label for="pre-venta">
                                                        Pre venta
                                                    </label>
                                                </div>
                                                <div class="radio radio-primary">
                                                    <input type="radio" name="status" id="a-estrenar" value="a-estrenar">
                                                    <label for="a-estrenar">
                                                        A estrenar
                                                    </label>
                                                </div>
                                                <div class="radio radio-primary">
                                                    <input type="radio" name="status" id="buen-estado" value="buen-estado">
                                                    <label for="buen-estado">
                                                        Buen estado
                                                    </label>
                                                </div>
                                                <div class="radio radio-primary">
                                                    <input type="radio" name="status" id="requiere-mantenimiento" value="requiere-mantenimiento">
                                                    <label for="requiere-mantenimiento">
                                                        Require mantenimiento
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <label>Publicado por...</label>
                                                <hr class="search-label-underline">
                                                <div class="radio radio-primary">
                                                    <input type="radio" name="published-by" id="todos" value="" checked>
                                                    <label for="todos">
                                                        Todos
                                                    </label>
                                                </div>
                                                <div class="radio radio-primary">
                                                    <input type="radio" name="published-by" id="inmobiliaria" value="inmobiliaria">
                                                    <label for="inmobiliaria">
                                                        Inmobiliaria
                                                    </label>
                                                </div>
                                                <div class="radio radio-primary">
                                                    <input type="radio" name="published-by" id="particular" value="particular">
                                                    <label for="particular">
                                                        Particular
                                                    </label>
                                                </div>
                                                <div class="radio radio-primary">
                                                    <input type="radio" name="published-by" id="constructora" value="constructora">
                                                    <label for="constructora">
                                                        Constructora
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <label>Condominio...</label>
                                                <hr class="search-label-underline">
                                                <div class="radio radio-primary">
                                                    <input type="radio" name="condominium" id="condominio-sin-definir" value="" checked>
                                                    <label for="condominio-sin-definir">
                                                        Sin definir
                                                    </label>
                                                </div>
                                                <div class="radio radio-primary">
                                                    <input type="radio" name="condominium" id="condominio-si" value="si">
                                                    <label for="condominio-si">
                                                        Si
                                                    </label>
                                                </div>
                                                <div class="radio radio-primary">
                                                    <input type="radio" name="condominium" id="condominio-no" value="no">
                                                    <label for="condominio-no">
                                                        No
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <label>Vivienda social...</label>
                                                <hr class="search-label-underline">
                                                <div class="radio radio-primary">
                                                    <input type="radio" name="social-housing" id="vivienda-social-sin-definir" value="" checked>
                                                    <label for="vivienda-social-sin-definir">
                                                        Sin definir
                                                    </label>
                                                </div>
                                                <div class="radio radio-primary">
                                                    <input type="radio" name="social-housing" id="vivienda-social-si" value="si">
                                                    <label for="vivienda-social-si">
                                                        Si
                                                    </label>
                                                </div>
                                                <div class="radio radio-primary">
                                                    <input type="radio" name="social-housing" id="vivienda-social-no" value="no">
                                                    <label for="vivienda-social-no">
                                                        No
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="#" class="show-more-options"><i class="fa fa-plus-square" aria-hidden="true"></i> Más filtros..</a>
                                        <a href="#" class="show-more-options" style="display: none;"><i class="fa fa-minus-square" aria-hidden="true"></i> Menos filtros..</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- End 1-content -->
                </div>
            </aside>
            <!-- End Search Advance -->
            <!-- Accordion -->
            <aside class="hidden-xs">
                <h2>Proyectos</h2>
                <?php foreach($inmueble_projects as $project): ?>
                    <div class="project_item">
                        <div class="row">
                            <div class="col-md-12">
                                <a href="<?php echo base_url() . $project->proy_seo; ?>" target="_blank">
                                    <?php
                                    $image = new File_Handler($project->proy_fot_imagen,"projectImage");
                                    $thumbnail = $image->getThumbnail("263","148",$project->inm_nombre);
                                    ?>
                                    <img src="<?=$thumbnail->getSource()?>" alt="<?=$thumbnail->getAlternateText()?>">
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h4><?php echo $project->inm_nombre; ?></h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <small><?php echo $project->ciu_nombre; ?></small>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <small><strong><?php echo ucwords(strtolower($project->for_descripcion)); ?></strong></small>
                            </div>
                            <div class="col-md-6 text_right">
                                <small><strong><?php echo $project->mon_abreviado; ?><?php echo number_format($project->inm_precio, 0, ",", "."); ?></strong></small>
                            </div>
                        </div>
                    </div>
                    <div class="divisor"></div>
                <?php endforeach; ?>
            </aside>
            <!-- End Accordion -->
        </div>
        <!-- End Aside -->
    <!-- property List-->
    <div class="col-md-9 properties_two">

    <!-- Row Propertys-->
    <div class="row">
        <div class="col-md-12" style="margin-top: 8px; margin-bottom: 15px;">
            <h1 style="margin-bottom: 0px; padding-bottom: 0px; float: left;" id="search-title"></h1><span style="font-size: 24px; font-family: 'Roboto',sans-serif; line-height: 44px; padding-left: 10px;" class="hide">(<span id="total-result"></span>)</span>
            <section class="sort" style="visibility: visible; margin-top: 12px;">
                <span class="button" style="margin-right: 6px; position: relative;">
                    <nobr>Ordenar</nobr>
                    <form id="form-sort" action="<?php echo base_url(); ?>search/sort" method="post" style="opacity: 0; height: 35px; left: 0; position: absolute; top: 0; width: 107px;">
                        <select id="sort-type" name="sort" style="width: 107px; padding-left: 0; height: 35px;">
                            <option></option>
                            <option value="date-desc" <?php echo $filter->sort == "date-desc" ? 'selected':''; ?>>Fecha (Descendentemente)</option>
                            <option value="date-asc" <?php echo $filter->sort == "date-asc" ? 'selected':''; ?>>Fecha (Ascendentemente)</option>
                            <option value="price-asc" <?php echo $filter->sort == "price-asc" ? 'selected':''; ?>>Precio (Menor a Mayor)</option>
                            <option value="price-desc" <?php echo $filter->sort == "price-desc" ? 'selected':''; ?>>Precio (Mayor a Menor)</option>
                        </select>
                    </form>
                </span>
            </section>
        </div>
        <div class="col-md-12">
            <div class="portlet light">
                <div id="search-result-content" class="portlet-title tabbable-line col-md-12 col-mobile-1">
                    <ul class="nav nav-tabs">
                        <?php
                        $activeProperties = "active";
                        $activeProjects = $activeMap = "";
                        //Right now the defaultTab is only used to enable the projects tab
                        if(isset($_GET["project-tab"]))
                        {
                            $activeProjects = "active";
                            $activeProperties = $activeMap = "";
                        }
                        ?>
                        <li class="<?=$activeProperties?>">
                            <a href="#tab-properties" data-toggle="tab">Inmuebles</a>
                        </li>
                        <li class="<?=$activeProjects?>">
                            <a href="#tab-projects" data-toggle="tab">Proyectos</a>
                        </li>
                        <li class="hidden-xs <?=$activeMap?>">
                            <a href="#tab-map" data-toggle="tab">Mapa</a>
                        </li>
                    </ul>
                </div>
                <div class="portlet-body">
                    <div class="tab-content">
                        <div class="tab-pane <?=$activeProperties?>" id="tab-properties">
                            <div id="overlay-publications" class="search-overlay"></div>
                            <!-- Content -->
                            <div class="row">
                                <div class="col-md-12" id="publications-content"></div>
                            </div>
                            <!-- Pagination -->
                            <div class="col-md-12">
                                <div class="col-md-12 paginations publication-pagination"></div>
                            </div>
                        </div>
                        <div class="tab-pane <?=$activeProjects?>" id="tab-projects">
                            <div id="overlay-projects" class="search-overlay"></div>
                            <!-- Content -->
                            <div class="row">
                                <div class="col-md-12" id="projects-content"></div>
                            </div>
                            <!-- Pagination -->
                            <div class="row">
                                <div class="col-md-12 col-xs-12 paginations project-pagination"></div>
                            </div>
                        </div>
                        <div class="tab-pane <?=$activeMap?>" id="tab-map">
                            <div id="overlay-map" class="search-overlay"></div>
                            <div class="col-xs-12">
                                <input name="latitude" value="" type="hidden" required>
                                <input name="longitude" value="" type="hidden" required>
                                <div class="map-fancy-framework">
                                    <div id="maps">
                                    </div>
                                </div>
                            </div>
                            <!-- Pagination -->
                            <div class="col-md-12 col-xs-12 paginations map-pagination hide"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row Propertys-->
    <!-- End Pagination -->
    <div class="col-md-12" style="background-color: #DDDDDD; margin-top: 55px;">
        <center><a href="<?=base_url("ajax/form_busqueda_user")?>" class="busqueda_usuario"><img class="formulario_busqueda" src="<?=assets_url("img/icons/boton-info2.png")?>" style="margin-top: -55px;" /></a></center>
    </div>
    </div>
    <!-- End property List-->
</div>
</div>
</section>
<!-- End content info-->
<?php
//$this->load->view("handlebar-template/publication-quick-view");
$this->load->view("handlebar-template/publication-quick-view-2018");
$this->load->view("handlebar-template/publication-map-quick-view");
$this->load->view("handlebar-template/public-pagination");
?>