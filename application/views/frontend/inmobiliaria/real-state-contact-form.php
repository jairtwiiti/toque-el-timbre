<?php
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 20/11/2017
 * Time: 11:43 AM
 */
?>
<aside class="project-contact-form">
    <div class="project-contact-box">
        <!-- Datos del Venderos -->
        <div class="project-seller-detail">
            <div class="row item_agent" style="margin-bottom: 0;">
                <div class="col-md-12 info_agent">
                </div>
            </div>
            <p class="description_agency"></p>
        </div>
        <!-- Fin Datos del Venderor -->
        <div id="box_contact_agent" style="margin-top: 10px;">
            <a name="Formulario"></a>
            <h2>Necestias ayuda para vender tu inmueble..<br>Contáctame!</h2>
            <form id="form-real-state-contact" class="form_contact_real_state" action="#" data-parsley-validate>
                <div class="row">
                    <?php
                    if(!$user_logged instanceof stdClass) {
                        ?>
                        <div class="col-md-12">
                            <input type="text" placeholder="Nombre" name="nombre" required>
                        </div>
                        <div class="col-md-12">
                            <input type="text" placeholder="Telefono" name="telefono" required data-parsley-type="number">
                        </div>
                        <div class="col-md-12">
                            <input type="email" placeholder="Email"  name="email" required>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="col-md-12">
                        <select name="ciudad">
                            <option value="Santa Cruz">Santa Cruz</option>
                            <option value="La Paz">La Paz</option>
                            <option value="Cochabamba">Cochabamba</option>
                            <option value="Tarija">Tarija</option>
                            <option value="Chuquisaca">Chuquisaca</option>
                            <option value="Oruro">Oruro</option>
                            <option value="Potosi">Potosi</option>
                            <option value="Pando">Pando</option>
                            <option value="Beni">Beni</option>
                        </select>
                    </div>
                </div>
                <textarea placeholder="Tu consulta..." name="mensaje" required></textarea>
                <input type="hidden" name="email_send" value="<?=$inmobiliaria->usu_email?>" />
                <input type="hidden" name="user-id" value="<?=$inmobiliaria->usu_id?>" />
                <div id='recaptcha' class="g-recaptcha"
                     data-sitekey="<?=$reCaptchaKeys["publicKey"]?>"
                     data-callback="sendRequestRealEstate"
                     data-size="invisible"></div>
                <input type="submit" id="project-form-contact-button" name="Submit" value="Enviar" class="button">
            </form>
            <div class="result"></div>
        </div>
    </div>
</aside>