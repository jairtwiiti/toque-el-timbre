<?php
/**
 * Created by PhpStorm.
 * User: Christian
 * Date: 05/11/2014
 * Time: 10:04
 */
$path_assets = base_url() . "assets/";
?>
<div id="layout-detail-agent">

<!-- End content info -->
<section class="content_info" style="padding-top: 0 !important; border-top: 5px solid #00aeef;">
<div class="container">

<div class="row padding_top">
<!-- Blog Post-->
<div id="col-detail" class="col-md-8 col-lg-9">

<?php if(!empty($inmobiliaria) && $inmobiliaria->usu_certificado == "Si"): ?>
<!--<div style="margin: 8px 0;">
    <img src="<?php echo base_url(); ?>assets/img/agente_certificados.png" style="width: 100%;" />
</div>-->
<?php endif; ?>

<!-- Post-->
<div id="agent-detail" class="post single">

<?php if(!empty($inmobiliaria)){
        $empresa = !empty($inmobiliaria->usu_empresa) ? $inmobiliaria->usu_empresa : $inmobiliaria->usu_nombre;
?>
    <h4><?php echo $empresa; ?></h4>
    <input type="hidden" value="<?=$inmobiliaria->usu_id?>" id="real-state-id">
    <div class="row">
        <div class="col-md-3 col-sm-3">
            <?php
            if(!empty($inmobiliaria->usu_foto)){
                $url_imagen = base_url().'librerias/timthumb.php?src='.base_url().'assets/uploads/users/'.strtolower($inmobiliaria->usu_foto).'&w=250';
            }else{
                $url_imagen = $path_assets.'img/team/1.jpg';
            }
            ?>
            <figure class="agency-image"><img src="<?php echo $url_imagen; ?>" alt="<?php echo $inmobiliaria->usu_empresa; ?>" title="<?php echo $inmobiliaria->usu_empresa; ?>"></figure>
        </div><!-- /.col-md-3 -->
        <div class="col-md-5 col-sm-5">
            <h5>Informacion de Contacto</h5>
            <address>
                <?php
                if($inmobiliaria->usu_latitud != "" && $inmobiliaria->usu_longitud != ""){
                    $url_mapa = base_url(). "inmueble/mapa?latitud=".$inmobiliaria->usu_latitud."&longitud=".$inmobiliaria->usu_longitud;
                }
                ?>
                <a class="show-on-map iframe_mapa" href="<?php echo $url_mapa; ?>"><i class="fa fa-map-marker"></i><figure>Mapa</figure></a>
                <strong><?php echo $empresa; ?></strong><br>
                <?php echo !empty($inmobiliaria->usu_direccion) ? $inmobiliaria->usu_direccion : '-'; ?>
            </address>
            <dl>
                <?php if(!empty($inmobiliaria->usu_telefono)): ?>
                    <dt>Telefono:</dt>
                    <dd><a class="no_link" href="tel:<?php echo $inmobiliaria->usu_telefono; ?>"><?php echo $inmobiliaria->usu_telefono; ?></a></dd>
                <?php endif; ?>
                <?php if(!empty($inmobiliaria->usu_celular)): ?>
                    <dt>Celular:</dt>
                    <dd><a class="no_link" href="tel:<?php echo $inmobiliaria->usu_celular; ?>"><?php echo $inmobiliaria->usu_celular; ?></a></dd>
                <?php endif; ?>
                <?php if(!empty($inmobiliaria->usu_email)): ?>
                    <dt>Email:</dt>
                    <dd><a href="mailto:<?php echo $inmobiliaria->usu_email; ?>"><?php echo $inmobiliaria->usu_email; ?></a></dd>
                <?php endif; ?>
                <?php if(!empty($inmobiliaria->usu_skype)): ?>
                    <dt>Skype:</dt>
                    <dd><a class="no_link" href="skype:<?php echo $inmobiliaria->usu_skype; ?>?call"><?php echo $inmobiliaria->usu_skype; ?></a></dd>
                <?php endif; ?>
            </dl>
        </div><!-- /.col-md-5 -->
        <div class="col-md-4 col-sm-4">
            <h5>Sobre Nosotros</h5>
            <p style="line-height: 20px; margin-bottom: 6px;"><?php echo !empty($inmobiliaria->usu_descripcion) ? nl2br($inmobiliaria->usu_descripcion) : '-'; ?></p>
            <div id="social">
                <h5>Redes Sociales</h5>
                <div class="agent-social">
                    <?php if(!empty($inmobiliaria->usu_facebook)): ?>
                        <a class="fa fa-facebook btn btn-grey-dark" href="<?php echo $inmobiliaria->usu_facebook; ?>"></a>
                    <?php endif; ?>
                    <?php if(!empty($inmobiliaria->usu_twitter)): ?>
                        <a class="fa fa-twitter btn btn-grey-dark" href="<?php echo $inmobiliaria->usu_twitter; ?>"></a>
                    <?php endif; ?>
                </div>
            </div><!-- /.block -->
        </div><!-- /.col-md-4 -->
    </div><!-- /.row -->

    <div class="row">
        <div class="col-md-12">
            <h4>Nuestros Inmuebles (<span class="real-state-total-properties">0</span>)</h4>
            <input type="hidden" value="<?=$inmobiliaria->usu_id?>" name="user-id">
            <div class="row" id="real-state-publications" style="padding-top: 15px;">
                <div class="progress active">
                    <div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                        <span class="sr-only"></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <!-- Pagination -->
                    <div class="paginations">
                    </div>
                    <!-- End Pagination -->
                </div>
            </div>
        </div>
    </div>

    <!-- No se encontro inmueble -->
<?php }else{ ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-warning" style="text-transform: uppercase;">
                <?php $url_seo = str_replace("-", " ", $this->uri->segment(3)); ?>
                <?php $url_seo = str_replace(".html", "", $url_seo); ?>
                El usuario <strong><?php echo $url_seo; ?></strong> que esta buscando no esta activo o no existe.
            </div>
        </div>
    </div>
<?php } ?>

</div>
<!-- End Post-->


</div>
<!-- End Blog Post-->

<!-- Sidebars-->
<div class="col-md-4 col-lg-3">

    <!-- Search Advance -->
    <!-- End Search Advance -->
    <!-- Accordion -->
    <?php
    $data["inmobiliaria"] = $inmobiliaria;
    $this->load->view("frontend/inmobiliaria/real-state-contact-form",$data);
    ?>
    <!-- End Accordion -->
</div>
<!-- Sidebars-->
</div>
</div>
</section>
<!-- End content info-->

</div>
<?php
$this->load->view("handlebar-template/publication-quick-view-2018");
$this->load->view("handlebar-template/quick-login-form");
$this->load->view('handlebar-template/welcome-to-tet-login-facebook');
?>