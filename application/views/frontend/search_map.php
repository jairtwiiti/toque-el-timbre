<?php
$path_assets = base_url() . "assets/";
//$filter_config = translate_decode_url($_GET);
//$filter_config = json_decode(json_encode($filter_config), FALSE);
$filter_config = $config_filter;
$aux_search = $filter_config->search;
$aux_in = $filter_config->in;
$aux_type = $filter_config->type;
$aux_city = $filter_config->city;
$aux_room = $filter_config->room;
$aux_bathroom = $filter_config->bathroom;
$aux_parking = $filter_config->parking;
$aux_currency = $filter_config->currency;
$url_search = base_url() . "buscar/?";
$back_search = $this->session->userdata('back_search_map');
?>

<div style="position: relative;">
    <div id="map" style="width: auto; height: 800px; border-top: 5px solid #00aeef;"></div>

    <div style="position: absolute; left: 6%; top: 2%; background: #fff; padding: 2px 22px;" class="hidden-xs">
        <h1 style="margin-bottom: 0px; padding-bottom: 0px; float: left;"><?php echo $title_search; ?></h1><span style="font-size: 24px; font-family: 'Roboto',sans-serif; line-height: 44px; padding-left: 10px;">(<?php echo $total_rows > 0 ? $total_rows : 0; ?>)</span>
    </div>

    <div class="search_advance hidden-xs" style="width: 290px; position: absolute; top: 2%; right: 0; padding: 10px 0px;" id="search_container">
        <div style="margin-bottom: 15px;">

            <a href="javascript:;" class="button">Vista Mapa</a>
<!--            <a href="--><?php //echo base_url().$back_search; ?><!--" class="button">Vista Lista</a>-->
            <a href="#" data-layout="" data-display="list" class="button layout-type">Vista Lista</a>
        </div>

        <ul class="tabs_services">
            <li><a id="1" class="set2">Mejora tu búsqueda</a></li>
        </ul>
        <!-- 1-content -->
        <div id="1-content" class="set2 show">
            <div class="search_box">
                <form action="<?php echo base_url() . "buscar" ?>" method="post">
                    <!--<div class="">
                            <input type="text" name="search" value="<?php echo $aux_search; ?>" placeholder="Buscar...">
                        </div>-->
                    <div>
                        <label>Estoy buscando...</label>
                        <select name="type">
                            <option value="Inmuebles">Todas los tipos</option>
                            <?php foreach($categories as $cat): ?>
                                <?php $valor = ucwords(strtolower($cat->cat_nombre)); ?>
                                <option value="<?php echo $valor; ?>" <?php echo $aux_type == $valor ? 'selected="selected"' : ''; ?>><?php echo $valor; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div>
                        <label>En la ciudad de...</label>
                        <select name="city">
                            <option value="Bolivia">Todas las ciudades</option>
                            <?php foreach($cities as $city): ?>
                                <?php $valor = ucwords(strtolower($city->dep_nombre)); ?>
                                <option value="<?php echo $valor; ?>" <?php echo $aux_city == $valor ? 'selected' : ''; ?>><?php echo $valor; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div >
                        <label>En...</label>
                        <select name="in">
                            <option value="Venta" <?php echo $aux_in == "Venta" ? 'selected' : ''; ?>>Venta</option>
                            <option value="Alquiler" <?php echo $aux_in == "Alquiler" ? 'selected' : ''; ?>>Alquiler</option>
                            <option value="Anticretico" <?php echo $aux_in == "Anticretico" ? 'selected' : ''; ?>>Anticretico</option>
                        </select>
                    </div>
                    <div >
                        <label>Habitaciones</label>
                        <select name="room">
                            <option value="">No Definido</option>
                            <option value="1" <?php echo $aux_room == "1" ? 'selected' : ''; ?>>1</option>
                            <option value="2" <?php echo $aux_room == "2" ? 'selected' : ''; ?>>2</option>
                            <option value="3" <?php echo $aux_room == "3" ? 'selected' : ''; ?>>3</option>
                            <option value="4" <?php echo $aux_room == "4" ? 'selected' : ''; ?>>4</option>
                            <option value="5+" <?php echo $aux_room == "5+" ? 'selected' : ''; ?>>5+</option>
                        </select>
                    </div>
                    <div >
                        <label>Baños</label>
                        <select name="bathroom">
                            <option value="">No Definido</option>
                            <option value="1" <?php echo $aux_bathroom == "1" ? 'selected' : ''; ?>>1</option>
                            <option value="2" <?php echo $aux_bathroom == "2" ? 'selected' : ''; ?>>2</option>
                            <option value="3" <?php echo $aux_bathroom == "3" ? 'selected' : ''; ?>>3</option>
                            <option value="4" <?php echo $aux_bathroom == "4" ? 'selected' : ''; ?>>4</option>
                            <option value="5+" <?php echo $aux_bathroom == "5+" ? 'selected' : ''; ?>>5+</option>
                        </select>
                    </div>
                    <div >
                        <label>Parqueo</label>
                        <select name="parking">
                            <option value="">No Definido</option>
                            <option value="1" <?php echo $aux_parking == "1" ? 'selected' : ''; ?>>1</option>
                            <option value="2" <?php echo $aux_parking == "2" ? 'selected' : ''; ?>>2</option>
                            <option value="3" <?php echo $aux_parking == "3" ? 'selected' : ''; ?>>3</option>
                            <option value="4" <?php echo $aux_parking == "4" ? 'selected' : ''; ?>>4</option>
                            <option value="5+" <?php echo $aux_parking == "5+" ? 'selected' : ''; ?>>5+</option>
                        </select>
                    </div>
                    <div >
                        <label>Moneda</label>
                        <div class="switch-wrapper">
                            <input type="checkbox" name="currency" value="1" <?php echo ($aux_currency == "Dolares" || empty($aux_currency)) ? 'checked' : ''; ?>>
                        </div>
                    </div>
                    <div>
                        <p>
                            <label for="amount">Precios:</label>
                            <input type="text" id="amount" readonly style="border:0; color:#f6931f; font-weight:bold;">
                        </p>
                        <div id="slider-range"></div>
                    </div>
                    <div>
                        <input type="hidden" name="price_min" id="price_min" value="" />
                        <input type="hidden" name="price_max" id="price_max" value="" />
                        <input type="hidden" name="layout" id="layout" value="mapa" />
                        <input type="hidden" name="search" id="search" value="<?php echo $aux_search; ?>" />
                        <input type="hidden" name="currency" id="currency" value="Dolares" />
                        <!--<input type="hidden" name="in" value="<?php echo empty($aux_in) ? null : $aux_in; ?>" />-->
                        <input type="button" id="advanced-search-form" class="button" style="background-color: rgba(0, 174, 239, 1) !important; margin-top: 25px; font-weight: 600;" value="Mostrar Resultados">
                    </div>
                </form>
            </div>
        </div>
        <!-- End 1-content -->
    </div>
</div>