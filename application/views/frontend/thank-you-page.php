<?php
$primaryFilter = $primaryFilterArrayData['primaryFilter'];
$lastPropertyViewed = $this->session->userdata("lastPropertyViewed");
$backToTheSearch = $this->session->set_userdata(array("backToTheSearchMobile"=>"1"));
$path_assets = base_url() . "assets/";
$similar_properties = $relacionadas;
$url_new_publication = base_url(). "dashboard/publications/register";
?>
<div id="layout-home">

<!-- Header-->
<header>
    <?php
    if(!$mobile || $ipad)
    {
    ?>
    <!-- Slide -->
    <!--<div class="camera_wrap camera_white_skin" id="slide">-->
    <div class="camera_wrap camera_white_skin">
        <!-- Item Slide -->
        <!--<div  data-src="<?php echo base_url(); ?>admin/imagenes/slideshow/<?php echo $slide["imagen"]; ?>">-->
        <div >
            <div class="fadeFromTop">
                <div class="container">
<!--                    <img src="--><?php //echo base_url(); ?><!--assets/img/slide/cover_imagen.png" style="width: 100%;" alt="Bienvenido sentite en casa" />-->
                    <h2 style="color: #00adee; text-align: center; font-weight: bold; font-size: 27px; margin: 9px 0 14px 0; line-height: 40px">¡Tu mensaje ha sido enviado!</h2>
                    <p style="text-align:center">El encargado del inmueble, te responderá o entrara en contacto para brindarte la información requerida.</p>
                </div>
            </div>
        </div>
        <!-- End Item Slide -->
    </div>
    <!-- End Slide -->
    <?php
    }
    else
    {
    ?>
        <div style="position: relative;">
            <img src="<?php echo base_url(); ?>assets/img/slide/persona_movil.png" style="width: 100%;"  />
            <div style="position: absolute; top: 0; left: 0; width: 100%; background: rgba(255, 255, 255, 0.5) ; padding: 8px 0; text-align: center;">
                <div style="margin: 0 15px;">
                    <img src="<?php echo base_url(); ?>assets/img/slide/cover_movil.png" style="width: 100%;" />
                </div>
            </div>
        </div>
    <?php } ?>

     <!-- Facebook Pixel Code -->
     <script>
     !function(f,b,e,v,n,t,s)
     {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
     n.callMethod.apply(n,arguments):n.queue.push(arguments)};
     if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
     n.queue=[];t=b.createElement(e);t.async=!0;
     t.src=v;s=b.getElementsByTagName(e)[0];
     s.parentNode.insertBefore(t,s)}(window,document,'script',
     'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '1481111128837486'); 
     fbq('track', 'PageView');
     </script>
     <noscript>
      <img height="1" width="1" src="https://www.facebook.com/tr?id=1481111128837486&ev=PageView&noscript=1"/>
     </noscript>
     <!-- End Facebook Pixel Code -->   


</header>
<!-- End Header-->
        <!-- filter-horizontal -->
        <div id="mobile" class="filter_horizontal">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h3 style="color: #fff; text-align: center; font-weight: bold; font-size: 27px; margin: 9px 0 14px 0; line-height: 40px">Te invitamos a: </h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div id="button-back-to-the-publisment" onclick="window.location ='<?="http://".$this->session->userdata("lastPropertyViewed")?>'" style="background-color: #0083b3 !important; width: 100%; color: #fff !important; font-size: 16px; cursor: pointer; padding: 9px 10px; font-weight: bold; text-align: center;">
                            Volver al anuncio
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div id="button-back-to-the-primary-filter-result" onclick="window.location = '<?="http://".$this->session->userdata("urlLastSearch")?>'" style="background-color: #0083b3 !important; width: 100%; color: #fff !important; font-size: 16px; cursor: pointer; padding: 9px 10px; font-weight: bold; text-align: center;">
                            Volver a la busqueda
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End filter-horizontal -->


<!-- content info -->
<div class="content_info">
<div class="container">

<!-- Row Properties-->
<div class="row">
    <?php
    if(count($similar_properties["results"])>0)
    {
    ?>
        <div class="row">
            <div class="col-md-12">
                <h4>Publicaciones similares</h4>
                <div class="row" style="padding-top: 15px;">
                    <?php
                    foreach($similar_properties["results"] as $publication)
                    {
                    ?>
                        <!-- Item Property-->
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                            <div class="item_property animateIn" data-animate="flipInX" style="border-bottom: 0; position:relative;">
                                <div class="head_property">
                                    <?php
                                    $url_detalle = base_url("buscar/".$primaryFilterArrayData["primaryFilter"]."/".seo_url($publication->inm_seo));
                                    //                                                        $url_detalle = base_url();
                                    //                                                        $url_detalle .= seo_url($publication->ciu_nombre)."/";
                                    //                                                        $url_detalle .= seo_url($publication->cat_nombre . " EN ". $publication->for_descripcion)."/";
                                    //                                                        $url_detalle .= seo_url($publication->inm_seo).".html";
                                    ?>
                                    <a href="<?php echo $url_detalle; ?>">
                                        <?php
                                        $imageX = $publication->fot_archivo;
                                        $fileimage = BASE_DIRECTORY . DS . "admin" . DS . "imagenes" . DS . "inmueble" . DS . $imageX;

                                        if (!empty($imageX) && !is_null($imageX) && file_exists($fileimage)) { ?>
                                            <img src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo base_url(); ?>admin/imagenes/inmueble/<?php echo $publication->fot_archivo; ?>&w=271&h=183" alt="<?php echo $publication->inm_nombre; ?>" title="<?php echo $publication->inm_nombre; ?>">
                                        <?php } else { ?>
                                            <img src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo $path_assets.'img/default.png'?>&w=271&h=183" alt="default" title="default toqueeltimbre">
                                        <?php } ?>

                                        <h5><?php echo $publication->ciu_nombre; ?></h5>
                                    </a>

                                    <div class="av-portfolio-overlay">
                                        <a href="<?php echo $url_detalle; ?>"><i class="av-icon fa fa-search"></i> Ver Detalle</a>
                                        <a href="javascript:;" class="btn_favorite" rel="<?php echo $publication->inm_id; ?>"><i class="av-icon fa fa-star"></i> Agregar a Favoritos</a>
                                    </div>

                                    <?php if($publication->usu_tipo != "Particular" && !empty($publication->usu_foto)): ?>
                                        <div class="av-portfolio-info">
                                            <?php $empresa = !empty($publication->usu_empresa) ? $publication->usu_empresa : $publication->usu_nombre; ?>
                                            <a href="<?php echo base_url() . 'perfil/'.$publication->usu_id.'/'.urlencode($empresa); ?>">
                                                <?php if(!empty($publication->usu_foto)){ ?>
                                                    <img src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo base_url(); ?>assets/uploads/users/<?php echo strtolower($publication->usu_foto); ?>&h=36" alt="<?php echo $publication->usu_nombre; ?>" title="<?php echo $publication->usu_nombre; ?>" />
                                                <?php }else{ ?>
                                                    <?php echo $empresa; ?>
                                                <?php } ?>
                                            </a>
                                        </div>
                                    <?php endif; ?>

                                </div>

                                <div class="info_property" style="padding-bottom: 0;">
                                    <ul>
                                        <li style="line-height: 18px; padding-bottom: 10px; height: 8em">
                                            <div style="font-weight: bold; margin-bottom:8px;"><?php echo $publication->cat_nombre . " EN ". $publication->for_descripcion; ?></div>
                                            <span style="float:none;"><?php echo crop_string($publication->inm_nombre, 60); ?></span>
                                        </li>
                                        <li>
                                            <div class="line_separator"></div>
                                            <strong>Precio</strong><span><?php echo $publication->mon_abreviado . " "; ?><?php echo number_format($publication->inm_precio,0,",",".") ?></span>
                                        </li>
                                    </ul>
                                    <?php if($publication->usu_certificado == "Si"): ?>
                                        <div class="icon_certificado">
                                            <img src="<?php echo $path_assets; ?>img/icon_agente_certificados.png" alt="Agente Certificado" class="masterTooltip"/>
                                            <div class="tet-tooltip">Este Inmueble pertenece a un Agente Certificado</div>
                                        </div>
                                    <?php endif; ?>
                                    <div class="footer_property">
                                        <div class="icon m2" title="Metros Cuadrados"><img src="<?php echo $path_assets; ?>img/icons/metros_cuadrados.png" /> <?php echo empty($publication->inm_superficie) ? 0 : $publication->inm_superficie; ?>m2</div>
                                        <?php
                                        foreach($publication->features as $feature)
                                        {
                                        ?>
                                            <?php
                                            if($feature["eca_car_id"] == "4" && !empty($feature["eca_valor"]) && is_numeric($feature["eca_valor"]))
                                            {
                                            ?>
                                                <div class="icon bedroom" title="Dormitorios"><img src="<?php echo $path_assets; ?>img/icons/dormitorios.png" /> <?php echo $feature["eca_valor"]; ?></div>
                                            <?php
                                            }
                                            if($feature["eca_car_id"] == "5" && !empty($feature["eca_valor"]) && is_numeric($feature["eca_valor"]))
                                            {
                                            ?>
                                                <div class="icon bathroom" title="Baños"><img src="<?php echo $path_assets; ?>img/icons/banos.png" /> <?php echo $feature["eca_valor"]; ?></div>
                                            <?php
                                            }
                                            if($feature["eca_car_id"] == "9" && !empty($feature["eca_valor"]) && is_numeric($feature["eca_valor"]))
                                            {
                                            ?>
                                                <div class="icon parking" title="Parqueo"><img src="<?php echo $path_assets; ?>img/icons/parqueo.png" /> <?php echo $feature["eca_valor"]; ?></div>
                                            <?php
                                            }
                                            ?>
                                        <?php
                                        }
                                        ?>
                                        <br style="clear:both" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Item Property-->
                    <?php
                    }
                    ?>


                </div>
            </div>
        </div>
    <?php }
    ?>
</div>
<!-- End Row Properties-->


<!-- Row Publicidad-->

<!-- End Row Publicidad-->


</div>
<!-- End Container -->
</div>
<!-- End content info-->
</div>

<?php
if($this->agent->is_mobile())
{
?>
    <script>
    location.hash = "#mobile";
    </script>
<?php
}
?>