<?php
$path_assets = base_url() . "assets/";
$num = count($favorites);
?>

<!-- End content info -->
<section id="layout-list-favorites" class="content_info content_info_search" style="padding-top: 0 !important; border-top: 5px solid #00aeef;">
    <div class="container">

        <div class="row padding_top">
            <!-- Blog Post-->
            <div class="col-md-8 col-lg-8">
                <!-- Post-->
                <div class="post single">
                    <div>
                        <h2>Lista de anuncios (<?php echo $num == 1 ? '1 seleccionado' : $num.' seleccionados'; ?>)</h2>
                        <p>
                            <a class="send" data-toggle="modal" data-target="#myModal" href="javascript:void(0);"><i class="fa fa-envelope" style="margin-right: 3px;"></i> Enviar por Correo</a> &nbsp;&nbsp;
                            <a class="print" href="javascript:void(0);"><i class="fa fa-print" style="margin-right: 3px;"></i> Imprimir</a>
                        </p>
                    </div>

                    <div><?php echo $message; ?></div>

                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none; z-index: 1060">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <form id="form-favorite-email-2" action="<?php echo base_url(); ?>mis-favoritos" method="POST">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h4 class="modal-title" id="myModalLabel">Ingrese su direccion de correo de destino</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>Si el mensaje no aparece en tu bandeja de entrada, no olvides revisar tu carpeta de correo no deseado. Te contactaremos tan pronto tengamos mas resultados relacionados con tu búsqueda.</p>
                                        <p><input type="text" placeholder="Correo Electronico" name="email" value="" /></p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                        <button type="button" class="btn btn-default btn_submit_email">Enviar</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <?php if(!empty($favorites)){ ?>
                    <div id="print-content">
                        <?php foreach($favorites as $fav): ?>
                            <div class="row item-lista">
                                <div class="col-md-4">
                                    <p>
                                        <!-- <img src="http://www.toqueeltimbre.com/librerias/timthumb.php?src=http://www.toqueeltimbre.com/admin/imagenes/inmueble/<?php echo $fav->fot_archivo; ?>&w=261&h=173" /> -->

                                        <?php
                                        $imageX = $fav->fot_archivo;
                                        //$imageX = strtolower($fav->fot_archivo);
                                        $fileimage = BASE_DIRECTORY . DS . "admin" . DS . "imagenes" . DS . "inmueble" . DS . $imageX;

                                        if (!empty($imageX) && !is_null($imageX) && file_exists($fileimage)) { ?>
                                            <img src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo base_url(); ?>admin/imagenes/inmueble/<?php echo ($fav->fot_archivo); ?>&w=261&h=173" />
                                        <?php } else { ?>
                                            <img src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo $path_assets.'img/default.png'?>&w=261&h=173" alt="default" title="default toqueeltimbre">
                                        <?php } ?>

                                    </p>
                                </div>
                                <div class="col-md-8">
                                    <?php
                                    $url_detalle = base_url();
                                    $url_detalle .= seo_url($fav->ciu_nombre)."/";
                                    $url_detalle .= seo_url($fav->cat_nombre . " EN ". $fav->for_descripcion)."/";
                                    $url_detalle .= seo_url($fav->inm_seo).".html";
                                    ?>
                                    <?php echo $fav->inm_direccion; ?><br>
                                    <a href="<?php echo $url_detalle; ?>"><?php echo $fav->inm_nombre; ?></a><br>
                                    <?php echo $fav->zon_nombre; ?><br>
                                    <b>Precio:</b> <?php echo $fav->mon_abreviado; ?> <?php echo number_format($fav->inm_precio, 0, ",", "."); ?>
                                        <?php foreach($fav->features as $feature): ?>
                                            <?php
                                            if($feature->caracteristica == 'habitaciones'){
                                                echo '/ ' .$feature->valor . ' Habitaciones';
                                            }
                                            if($feature->caracteristica == 'banos'){
                                                echo '/ ' .$feature->valor . ' Baños';
                                            }
                                            ?>
                                        <?php endforeach; ?>
                                    <br>
                                    <b>Referencia:</b> <?php echo $fav->usu_nombre . " " . $fav->usu_apellido; ?> - <?php echo $fav->usu_telefono; ?><br>
                                    <a class="delete_favorite btn_delete_favorite" href="javascript:void(0);" rel="<?php echo $fav->inm_id; ?>">Eliminar de favoritos</a>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <?php }else{ ?>
                    <div>
                        <p>No tienes inmuebles seleccionados, empieza a buscar y podrás guardar los anuncios la lista de favoritos para poder volver a verlos sin necesidad de buscarlos.</p>
                    </div>
                    <?php } ?>
                </div>
                <!-- End Post-->

            </div>
            <!-- End Blog Post-->

            <!-- Sidebars-->
            <div class="col-md-4 col-lg-4">

                <!-- Search Advance -->
                <aside>
                    <div class="search_advance">
                        <?php
                        if(!empty($back_search)){
                            $aux_search = $back_search->search;
                            $aux_type = $back_search->type;
                            $aux_city = $back_search->city;
                            $aux_in = $back_search->in;
                            $aux_room = $back_search->room;
                            $aux_bathroom = $back_search->bathroom;
                            $aux_parking = $back_search->parking;
                            $aux_currency = $back_search->currency;
                        }
                        ?>

                        <ul class="tabs_services">
                            <li><a id="1" class="set2">Mejora tu búsqueda</a></li>
                        </ul>

                        <!-- 1-content -->
                        <div id="1-content" class="set2 show">
                            <div class="search_box">
                                <form action="<?php echo base_url() . "buscar" ?>" method="post">
                                    <div>
                                        <label>Estoy buscando...</label>
                                        <select name="type">
                                            <option value="Inmuebles">Todas los tipos</option>
                                            <?php foreach($categories as $cat): ?>
                                                <?php $valor = ucwords(strtolower($cat->cat_nombre)); ?>
                                                <option value="<?php echo $valor; ?>" <?php echo $aux_type == $valor ? 'selected' : ''; ?>><?php echo $valor; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div>
                                        <label>En la ciudad de...</label>
                                        <select name="city">
                                            <option value="Bolivia">Todas las ciudades</option>
                                            <?php foreach($cities as $city): ?>
                                                <?php $valor = ucwords(strtolower($city->dep_nombre)); ?>
                                                <option value="<?php echo $valor; ?>" <?php echo $aux_city == $valor ? 'selected' : ''; ?>><?php echo $valor; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div>
                                        <label>En...</label>
                                        <select name="in">
                                            <?php foreach($forma as $form): ?>
                                                <?php $valor = ucwords(strtolower($form["for_descripcion"])); ?>
                                                <option value="<?php echo $valor; ?>" <?php echo $aux_in == $valor ? 'selected' : ''; ?>><?php echo $valor; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div >
                                        <label>Habitaciones</label>
                                        <select name="room">
                                            <option value="">No Definido</option>
                                            <option value="1" <?php echo $aux_room == "1" ? 'selected' : ''; ?>>1</option>
                                            <option value="2" <?php echo $aux_room == "2" ? 'selected' : ''; ?>>2</option>
                                            <option value="3" <?php echo $aux_room == "3" ? 'selected' : ''; ?>>3</option>
                                            <option value="4" <?php echo $aux_room == "4" ? 'selected' : ''; ?>>4</option>
                                            <option value="5+" <?php echo $aux_room == "5+" ? 'selected' : ''; ?>>5+</option>
                                        </select>
                                    </div>
                                    <div >
                                        <label>Baños</label>
                                        <select name="bathroom">
                                            <option value="">No Definido</option>
                                            <option value="1" <?php echo $aux_bathroom == "1" ? 'selected' : ''; ?>>1</option>
                                            <option value="2" <?php echo $aux_bathroom == "2" ? 'selected' : ''; ?>>2</option>
                                            <option value="3" <?php echo $aux_bathroom == "3" ? 'selected' : ''; ?>>3</option>
                                            <option value="4" <?php echo $aux_bathroom == "4" ? 'selected' : ''; ?>>4</option>
                                            <option value="5+" <?php echo $aux_bathroom == "5+" ? 'selected' : ''; ?>>5+</option>
                                        </select>
                                    </div>
                                    <div >
                                        <label>Parqueo</label>
                                        <select name="parking">
                                            <option value="">No Definido</option>
                                            <option value="1" <?php echo $aux_parking == "1" ? 'selected' : ''; ?>>1</option>
                                            <option value="2" <?php echo $aux_parking == "2" ? 'selected' : ''; ?>>2</option>
                                            <option value="3" <?php echo $aux_parking == "3" ? 'selected' : ''; ?>>3</option>
                                            <option value="4" <?php echo $aux_parking == "4" ? 'selected' : ''; ?>>4</option>
                                            <option value="5+" <?php echo $aux_parking == "5+" ? 'selected' : ''; ?>>5+</option>
                                        </select>
                                    </div>
                                    <div >
                                        <label>Moneda</label>
                                        <div class="switch-wrapper">
                                            <input type="checkbox" name="currency" value="1" <?php echo ($aux_currency == "Dolares" || empty($aux_currency)) ? 'checked' : ''; ?>>
                                        </div>
                                    </div>
                                    <div>
                                        <p>
                                            <label for="amount">Precios:</label>
                                            <input type="text" id="amount" readonly style="border:0; color:#f6931f; font-weight:bold;">
                                        </p>
                                        <div id="slider-range"></div>
                                    </div>
                                    <div>
                                        <input type="hidden" name="price_min" id="price_min" value="" />
                                        <input type="hidden" name="price_max" id="price_max" value="" />
                                        <input type="hidden" name="search" id="search" value="<?php echo $aux_search; ?>" />
                                        <input type="hidden" name="currency" id="currency" value="Dolares" />
<!--                                        <input type="submit" class="button" style="background-color: rgba(0, 174, 239, 1) !important; margin-top: 25px; font-weight: 600;" value="Mostrar Resultados">-->
                                        <input type="button" class="button" id="advanced-search-form" style="background-color: rgba(0, 174, 239, 1) !important; margin-top: 25px; font-weight: 600;" value="Mostrar Resultados">
                                        <?php
                                        if(!empty($back_search)){
                                            $values = convert_values_url($back_search);
                                        }
                                        ?>
                                        <a href="javascript:void(0);" rel="<?php echo base_url() . $values; ?>" class="button volver_atras" style="background-color: rgba(0, 174, 239, 1) !important;">Volver a la Busqueda</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- End 1-content -->

                    </div>
                </aside>
                <!-- End Search Advance -->


            </div>
            <!-- Sidebars-->
        </div>
    </div>
</section>
<!-- End content info-->