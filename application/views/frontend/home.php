<?php
session_start();
$path_assets = base_url() . "assets/";
$url_new_publication = base_url(). "dashboard/publications/register";
?>
<div id="layout-home">
<!-- Header-->
<header>
    <?php
    if(!$mobile || $ipad)
    {
    ?>
    <!-- Slide -->
    <!--<div class="camera_wrap camera_white_skin" id="slide">-->
    <div class="camera_wrap camera_white_skin">
        <!-- Item Slide -->
        <!--<div  data-src="<?=base_url("admin/imagenes/slideshow/".$slide["imagen"])?>">-->
        <div >
            <div class="camera_caption fadeFromTop">
                <div class="container">
                    <img src="<?=base_url("assets/img/slide/cover_imagen.png")?>" style="width: 100%;" alt="Bienvenido sentite en casa" />
                </div>
            </div>
        </div>
        <!-- End Item Slide -->
    </div>
    <!-- End Slide -->
    <?php
    }
    else
    {
    ?>
        <div style="position: relative;">
            <img src="<?=base_url("assets/img/slide/persona_movil.png")?>" style="width: 100%;"  />
            <div style="position: absolute; top: 0; left: 0; width: 100%; background: rgba(255, 255, 255, 0.5) ; padding: 8px 0; text-align: center;">
                <div style="margin: 0 15px;">
                    <img src="<?=base_url("assets/img/slide/cover_movil.png")?>" style="width: 100%;" />
                </div>
            </div>
        </div>
    <?php } ?>

     <!-- Facebook Pixel Code -->
     <script>
     !function(f,b,e,v,n,t,s)
     {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
     n.callMethod.apply(n,arguments):n.queue.push(arguments)};
     if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
     n.queue=[];t=b.createElement(e);t.async=!0;
     t.src=v;s=b.getElementsByTagName(e)[0];
     s.parentNode.insertBefore(t,s)}(window,document,'script',
     'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '1481111128837486'); 
     fbq('track', 'PageView');
     </script>
     <noscript>
      <img height="1" width="1" src="https://www.facebook.com/tr?id=1481111128837486&ev=PageView&noscript=1"/>
     </noscript>
     <!-- End Facebook Pixel Code -->   


</header>
<!-- End Header-->
<?=$box_search?>
<!-- content info -->
<div class="content_info">
<div class="container">

<!-- Content Carousel Properties -->
<div class="content-carousel">

    <div class="row">
        <div class="col-md-12">
            <!-- Title-->
            <div class="titles">
                <h2 style="display: inline; font-size: 30px; line-height: 40px; margin: 20px 0; padding-bottom: 7px;">Inmuebles a Estrenar</h2>
            </div>
            <!-- End Title-->
        </div>
    </div>

    <!-- Carousel Properties -->
    <div id="properties-carousel" class="on-carousel properties-carousel" style="margin-bottom: 15px;">
        <?php
        if(!empty($inmueble_projects))
        {
	        $var_navigable = count($inmueble_projects);
	        foreach($inmueble_projects as $project)
            {
            ?>
        <!-- Item Property-->
        <div class="item_property">
            <div class="head_property">
                <a href="<?php echo base_url($project->proy_seo) ; ?>" target="_blank">
                    <?php
                    $fileHandler = new File_Handler($project->proy_fot_imagen,"projectImage");
                    $thumbnail = $fileHandler->getThumbnail(271,183, $project->inm_nombre);
                    ?>
                    <img src="<?=$thumbnail->getSource()?>" alt="<?=$thumbnail->getAlternateText()?>" title="<?=$thumbnail->getTitle()?>">
                    <h5><?php echo $project->ciu_nombre; ?></h5>
                </a>
            </div>
            <div class="info_property">
                <ul>
                    <li><div class="bold_text" style="font-size: 14px;"><?php echo $project->inm_nombre; ?></div></li>
                    <li>
                        <div class="line_separator"></div>
                        <span class="bold_text" style="float: none; font-size: 14px;"><?php echo ucwords(strtolower($project->for_descripcion)); ?></span>
                        <span>
                            <?php echo $project->mon_abreviado; ?>
                            <?php echo number_format($project->inm_precio, 0, ",", "."); ?>
                        </span>
                    </li>
                </ul>
            </div>
        </div>
        <!-- Item Property-->
        <?php
            }
        }
        ?>
    </div>
    <!-- End Carousel Properties -->
</div>
<!-- End Content Carousel Properties -->
<!-- Title-->
<div class="row">
    <div class="col-md-12">
        <div class="titles">
            <h2 style="display: inline; font-size: 30px; line-height: 40px; margin: 20px 0; padding-bottom: 7px;">Ultimos Anuncios</h2>
        </div>
    </div>
</div>
<!-- End Title-->
<!-- Row Properties-->
<div class="row">
    <?php
    if(!empty($pay_publications))
    {
        $count_pays = count($pay_publications);
        foreach($pay_publications as $publication)
        {
        ?>
        <!-- Item Property-->
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <div class="item_property animateIn" data-animate="flipInX" style="border-bottom: 0;">
                <div class="head_property">
                    <?php
                    $url_detalle = base_url();
                    $url_detalle .= "buscar/".seo_url($publication->cat_nombre . " EN ". $publication->for_descripcion." EN ".$publication->dep_nombre)."/";
                    $url_detalle .= $publication->inm_seo;
                    ?>
                    <a href="<?=$url_detalle?>" target="_blank">
                        <?php
                        $fileHandler = new File_Handler($publication->fot_archivo);
                        $thumbnail = $fileHandler->getThumbnail(271,183,$publication->inm_nombre);
                        ?>
                        <img src="<?=$thumbnail->getSource()?>" alt="<?=$thumbnail->getAlternateText()?>" title="<?=$thumbnail->getTitle()?>">
                        <h5><?php echo $publication->ciu_nombre; ?></h5>
                    </a>

                    <div class="av-portfolio-overlay">
                        <a href="<?php echo $url_detalle; ?>" target="_blank"><i class="av-icon fa fa-search"></i> <span>Ver Detalle</span></a>
                        <a href="javascript:;" class="btn_favorite" rel="<?php echo $publication->inm_id; ?>"><i class="av-icon fa fa-star"></i> <span>Agregar a Favoritos</span></a>
                    </div>

                    <?php
                    if($publication->usu_tipo != "Particular" && !empty($publication->usu_foto))
                    {
                    ?>
                        <div class="av-portfolio-info">
                            <?php $empresa = !empty($publication->usu_empresa) ? $publication->usu_empresa : $publication->usu_nombre; ?>
                            <a href="<?php echo base_url() . 'perfil/'.$publication->usu_id.'/'.urlencode($empresa); ?>" target="_blank">
                                <?php
                                $fileHandler = new File_Handler($publication->usu_foto);
                                $thumbnail = $fileHandler->getThumbnail(36,36,$empresa);
                                if($thumbnail->fileExist())
                                {
                                ?>
                                    <img src="<?=$thumbnail->getSource()?>" alt="<?=$thumbnail->getAlternateText()?>" title="<?=$thumbnail->getTitle()?>" />
                                <?php
                                }
                                else
                                {
                                    echo $empresa;
                                }
                                ?>
                            </a>
                        </div>
                    <?php
                    }
                    ?>

                </div>
                <div class="info_property <?=$user_logged->usu_tipo == "Admin" && $publication->ser_tipo == "Principal"?"stand_out":""?>" style="padding-bottom: 0;">
                    <ul>
                        <li style="line-height: 18px; padding-bottom: 10px; height: 5em">
                            <div style="font-weight: bold; margin-bottom:7px; font-size: 15px;"><?php echo $publication->cat_nombre . " EN ". $publication->for_descripcion; ?></div>
                            <span style="float:none;"><strong style="font-weight: normal;"><?php echo crop_string(strip_tags(html_entity_decode($publication->inm_direccion)), 60); ?></strong></span>
                        </li>
                        <li>
                            <div class="line_separator"></div>
                            <span style="float: none; font-size: 14px;" class="bold_text">Precio</span><span><?php echo $publication->mon_abreviado . " "; ?><?php echo number_format($publication->inm_precio,0,",",".") ?></span>
                        </li>
                    </ul>
                    <div class="footer_property">
                        <?php $tipo_sup = $publication->inm_tipo_superficie == "Metros Cuadrados" ? "m2" : "ha"; ?>
                        <div class="icon m2" title="Metros Cuadrados"><img src="<?php echo $path_assets; ?>img/icons/metros_cuadrados.png" alt="Metros Cuadrados" /> <?php echo empty($publication->inm_superficie) ? 0 : $publication->inm_superficie; ?><?php echo $tipo_sup; ?></div>
                        <?php
                        foreach($publication->features as $feature)
                        {
                            if($feature["eca_car_id"] == "4" && !empty($feature["eca_valor"]))
                            {
                            ?>
                                <div class="icon bedroom" title="Dormitorios"><img src="<?php echo $path_assets; ?>img/icons/dormitorios.png" alt="Dormitorio" /> <?php echo $feature["eca_valor"]; ?></div>
                            <?php
                            }
                            if($feature["eca_car_id"] == "5" && !empty($feature["eca_valor"]))
                            {
                            ?>
                                <div class="icon bathroom" title="Baños"><img src="<?php echo $path_assets; ?>img/icons/banos.png" alt="Baño" /> <?php echo $feature["eca_valor"]; ?></div>
                            <?php
                            }
                            if($feature["eca_car_id"] == "9" && !empty($feature["eca_valor"]))
                            {
                            ?>
                                <div class="icon parking" title="Parqueo"><img src="<?php echo $path_assets; ?>img/icons/parqueo.png" alt="Parqueo" /> <?php echo $feature["eca_valor"]; ?></div>
                            <?php
                            }
                        }
                        ?>
                        <br style="clear:both" />
                    </div>
                </div>

            </div>
        </div>
        <!-- Item Property-->
        <?php
        }
        if($count_pays > 6 )
        {
            for($i = 0; $i < (8 - $count_pays); $i++)
            {
        ?>
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                    <div class="item_property" style="border-bottom: 0;">
                        <div class="head_property">
                            <a href="<?php echo base_url(); ?>tabla-de-precios">
                                <img src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo $path_assets.'img/default.png'?>&w=271&h=183" alt="destacar anuncio" title="destacar anuncio" style="border-color: #eee;">
                            </a>
                        </div>
                        <div class="info_property stand_out" style="padding: 15px 0px 36px 0px; background: #EEEEEE; border: 0px;">
                            <ul>
                                <li><div style="font-size: 13px; text-align: center">Puedes Mejorar la visibilidad de tus anuncios accediendo a tu cuenta.</div></li>
                            </ul>
                            <a href="<?php echo base_url(); ?>tabla-de-precios">
                            <div class="footer_property" style="margin-top: 41px; color: white;text-align: center; background: #00AEEF; font-size: 13px">
                                MAS INFORMACIÓN
                                <br style="clear:both" />
                            </div>
                            </a>
                        </div>
                    </div>
                </div>
            <?php
            }
        }
    }
    ?>
</div>
<!-- End Row Properties-->


<!-- Row Publicidad-->
<div class="row">
    <div class="col-md-12">
        <!--<img src="<?php echo base_url() ?>assets/img/publicidad.jpg" style="width: 100%" alt="Publicidad" />-->
        <center class="banner_home">
            <a href="<?php echo base_url() ?>ajax/form_busqueda_user" class="busqueda_usuario" style="text-align: center">
                <img src="<?php echo base_url() ?>assets/img/banner_home/torre_publicidad_large.png" style="width: 100%; max-width: 940px;"/>
            </a>
        </center>

        <center class="banner_home_mobile">
            <a href="<?php echo base_url() ?>ajax/form_busqueda_user" class="busqueda_usuario" style="text-align: center">
                <img src="<?php echo base_url() ?>assets/img/banner_home/torre_publicidad.png" style="width: 100%;" />
            </a>
        </center>
    </div>
</div>
<!-- End Row Publicidad-->


</div>
<!-- End Container -->
</div>
<!-- End content info-->
</div>

<?php
if($this->agent->is_mobile())
{
?>
    <script>
    location.hash = "#mobile";
    </script>
<?php
}
?>