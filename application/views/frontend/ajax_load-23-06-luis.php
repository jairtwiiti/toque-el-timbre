<?php
$path_assets = base_url() . "assets/";
if(!empty($publications)):
?>
<?php foreach($publications as $publication): ?>
    <!-- Item Property-->
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="width: 100% !important; margin-bottom: 20px;">
        <div class=" animateIn" data-animate="flipInX" style="border-bottom: 0; position:relative;">
            <?php
            if($publication->patrocinado == "Proyecto"){
                $obj_project = $this->model_proyecto->get_info_project($publication->inm_proy_id);
                $url_detalle = base_url();
                $url_detalle .= $obj_project->proy_seo;
            }else{
                $url_detalle = base_url();
                $url_detalle .= seo_url($publication->ciu_nombre)."/";
                $url_detalle .= seo_url($publication->cat_nombre . " EN ". $publication->for_descripcion)."/";
                $url_detalle .= seo_url($publication->inm_seo).".html";
            }
            ?>
            <div class="head_property">
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        <?php if($publication->patrocinado == "Proyecto"){ ?>
                            <a href="<?php echo base_url().$obj_project->proy_seo; ?>">
                                <img src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo base_url(); ?>admin/imagenes/proyecto/<?php echo $obj_project->proy_fot_imagen; ?>&w=1024&h=700" alt="<?php echo $obj_project->proy_nombre; ?>" title="<?php echo $project->proy_nombre; ?>">
                            </a>
                        <?php }else{ ?>
                            <?php foreach($publication->images as $image): ?>
                                <div class="swiper-slide">
                                    <a href="javascript:void(0);" rel="<?php echo $publication->inm_id; ?>">
                                        <?php
                                        $imageX = $image["fot_archivo"];
                                        $fileimage = BASE_DIRECTORY . DS . "admin" . DS . "imagenes" . DS . "inmueble" . DS . $imageX;
                                        ?>
                                        <?php if(!empty($imageX) && !is_null($imageX) && file_exists($fileimage)){ ?>
                                            <img src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo base_url(); ?>admin/imagenes/inmueble/<?php echo $imageX; ?>&w=1024&h=700" alt="<?php echo $publication->inm_nombre; ?>" title="<?php echo $publication->inm_nombre; ?>">
                                        <?php }else{ ?>
                                            <img src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo $path_assets.'img/default.png'?>&w=1024&h=700" alt="default" title="Toqueeltimbre.com">
                                        <?php } ?>

                                    </a>
                                </div>
                            <?php endforeach; ?>
                        <?php } ?>
                    </div>
                </div>
                <h5><?php echo $publication->ciu_nombre; ?></h5>

            </div>

            <div class="info_property
            <?php
            if($publication->patrocinado == "Patrocinado"){
                echo 'stand_out';
            }elseif($publication->patrocinado == "Proyecto"){
                echo 'stand_out_project';
            }
            ?>
            " style="padding-bottom: 0;">
                <ul>
                    <li style="line-height: 18px; padding-bottom: 10px; height: 8em">
                        <div style="font-weight: bold; margin-bottom:8px;"><?php echo $publication->cat_nombre . " ". $publication->for_descripcion; ?></div>
                        <span style="float:none;"><?php echo crop_string($publication->inm_nombre, 60); ?></span>
                    </li>
                    <li>
                        <div class="line_separator"></div>
                        <strong>Precio</strong><span><?php echo $publication->mon_abreviado . " "; ?><?php echo number_format($publication->inm_precio,0,",",".") ?></span>
                    </li>
                </ul>
                <?php if($publication->usu_certificado == "Si"): ?>
                    <div class="icon_certificado">
                        <img src="<?php echo $path_assets; ?>img/icon_agente_certificados.png"  />
                    </div>
                <?php endif; ?>
                <div class="footer_property">
                    <?php $tipo_sup = $publication->inm_tipo_superficie == "Metros Cuadrados" ? "m2" : "ha"; ?>
                    <div class="icon m2" title="Metros Cuadrados"><i class="fa" style="margin-right: 2px;"><img src="<?php echo $path_assets; ?>img/icons/metros_cuadrados.png" /></i> <?php echo empty($publication->inm_superficie) ? 0 : $publication->inm_superficie; ?><?php echo $tipo_sup; ?></div>
                    <?php foreach($publication->features as $feature): ?>
                        <?php if($feature["eca_car_id"] == "4" && !empty($feature["eca_valor"]) && is_numeric($feature["eca_valor"])): ?>
                            <div class="icon bedroom" title="Dormitorios"><i class="fa"><img src="<?php echo $path_assets; ?>img/icons/dormitorios.png" /></i> <?php echo $feature["eca_valor"]; ?></div>
                        <?php  endif; ?>

                        <?php if($feature["eca_car_id"] == "5" && !empty($feature["eca_valor"]) && is_numeric($feature["eca_valor"])): ?>
                            <div class="icon bathroom" title="Baños"><i class="fa"><img src="<?php echo $path_assets; ?>img/icons/banos.png" /></i> <?php echo $feature["eca_valor"]; ?></div>
                        <?php endif; ?>

                        <?php if($feature["eca_car_id"] == "9" && !empty($feature["eca_valor"]) && is_numeric($feature["eca_valor"])): ?>
                            <div class="icon parking" title="Parqueo"><i class="fa"><img src="<?php echo $path_assets; ?>img/icons/parqueo.png" /></i> <?php echo $feature["eca_valor"]; ?></div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <br style="clear:both" />
                </div>
            </div>
        </div>
    </div>
    <!-- Item Property-->
<?php endforeach; ?>
<?php endif; ?>