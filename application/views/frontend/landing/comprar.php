<?php
$path_assets = base_url() . "assets/";
?>


<?php echo $buy_map['js']; ?>
<!-- Header-->
<header class="search_contact">
    <!-- Slide -->
    <div class="map_area">
        <?php echo $buy_map['html']; ?>
    </div>
</header>
<!-- End Header-->

<?php echo $box_search; ?>

<!-- content info -->
<div class="content_info">
    <div class="container">

        <!-- Content Carousel Properties -->
        <div class="content-carousel">
            <div class="row">
                <div class="col-md-12">
                    <!-- Title-->
                    <div class="titles">
                        <h1>MAS BUSCADOS</h1>
                    </div>
                    <!-- End Title-->
                </div>
            </div>

            <!-- Carousel Properties -->
            <div id="properties-carousel" class="properties-carousel">

                <?php
                $i= 1;
                foreach($landing as $seo): ?>
                    <?php if($i <= 4): ?>
                        <!-- Item Property-->
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                            <div class="item_property">
                                <div class="head_property">
                                    <a href="<?php echo base_url(); ?>busqueda/<?php echo $seo["seo"]; ?>">
                                        <!-- <img src="<?php echo base_url(); ?>admin/imagenes/landing_seo/<?php echo $seo["imagen"]; ?>" alt="<?php echo $seo["titulo"]; ?>" title="<?php echo $seo["titulo"]; ?>"> -->

                                        <?php
                                        $imageX = $seo["imagen"];
                                        $fileimage = BASE_DIRECTORY . DS . "admin" . DS . "imagenes" . DS . "landing_seo" . DS . $imageX;
                                        if (!empty($imageX) && !is_null($imageX) && file_exists($fileimage)) { ?>
                                            <img src="<?php echo base_url(); ?>admin/imagenes/landing_seo/<?php echo $seo["imagen"]; ?>" alt="<?php echo $seo["titulo"]; ?>" title="<?php echo $seo["titulo"]; ?>">
                                        <?php } else { ?>
                                            <img src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo $path_assets.'img/default.png'?>&w=271&h=183" alt="default" title="default toqueeltimbre">
                                        <?php } ?>

                                        <h5><?php echo $seo["titulo"]; ?></h5>
                                    </a>
                                </div>
                                <div class="info_property">
                                    <ul>
                                        <li>
                                            <strong>Precio desde</strong>
                                            <span><?php echo $seo["precio"]; ?></span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- Item Property-->
                    <?php endif; ?>
                    <?php
                    $i++;
                endforeach; ?>
                <br style="clear: both;" />
            </div>
            <!-- End Carousel Properties -->
        </div>
        <!-- End Content Carousel Properties -->



        <!-- Title-->
        <div class="row">
            <div class="col-md-12">
                <div class="titles">
                    <h1>ANUNCIOS DESTACADOS</h1>
                </div>
            </div>
        </div>
        <!-- End Title-->


        <!-- Row Properties-->
        <div class="row">
            <?php if(!empty($pay_publications)): ?>
                <?php foreach($pay_publications as $publication): ?>

                    <!-- Item Property-->
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                        <div class="item_property animateIn" data-animate="flipInX" style="border-bottom: 0;">
                            <div class="head_property">
                                <?php
                                $url_detalle = base_url();
                                $url_detalle .= seo_url($publication->ciu_nombre)."/";
                                $url_detalle .= seo_url($publication->cat_nombre . " EN ". $publication->for_descripcion)."/";
                                $url_detalle .= seo_url($publication->inm_seo).".html";
                                ?>
                                <a href="<?php echo $url_detalle; ?>">
                                    <!-- <img src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo base_url(); ?>admin/imagenes/inmueble/<?php echo $publication->fot_archivo; ?>&w=271&h=183" alt="<?php echo $publication->inm_nombre; ?>" title="<?php echo $publication->inm_nombre; ?>"> -->

                                    <?php
                                    $imageX = $publication->fot_archivo;
                                    $fileimage = BASE_DIRECTORY . DS . "admin" . DS . "imagenes" . DS . "inmueble" . DS . $imageX;

                                    if (!empty($imageX) && !is_null($imageX) && file_exists($fileimage)) { ?>
                                        <img src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo base_url(); ?>admin/imagenes/inmueble/<?php echo $publication->fot_archivo; ?>&w=271&h=183" alt="<?php echo $publication->inm_nombre; ?>" title="<?php echo $publication->inm_nombre; ?>">
                                    <?php } else { ?>
                                        <img src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo $path_assets.'img/default.png'?>&w=271&h=183" alt="default" title="default toqueeltimbre">
                                    <?php } ?>

                                    <h5><?php echo $publication->ciu_nombre; ?></h5>
                                </a>

                                <div class="av-portfolio-overlay">
                                    <a href="<?php echo $url_detalle; ?>"><i class="av-icon fa fa-search"></i> Ver Detalle</a>
                                    <?php $favorite = check_favorite_view($usuario_id, $publication->inm_id); ?>
                                    <a href="javascript:;" class="btn_favorite <?php echo $favorite ? 'check_favorite' : ''; ?>" rel="<?php echo $publication->inm_id; ?>">
                                        <i class="av-icon fa fa-star"></i> Agregar a Favoritos
                                    </a>
                                </div>

                                <?php if($publication->usu_tipo != "Particular" && !empty($publication->usu_foto)): ?>
                                    <div class="av-portfolio-info">
                                        <?php $empresa = !empty($publication->usu_empresa) ? $publication->usu_empresa : $publication->usu_nombre; ?>
                                        <a href="<?php echo base_url() . 'perfil/'.$publication->usu_id.'/'.urlencode($empresa); ?>">
                                            <?php if(!empty($publication->usu_foto)){ ?>
                                                <!-- <img src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo base_url(); ?>admin/imagenes/inmueble/<?php echo strtolower($publication->usu_foto); ?>&h=36" alt="<?php echo $publication->usu_nombre; ?>" title="<?php echo $publication->usu_nombre; ?>" /> -->

                                                <?php $empresa = !empty($publication->usu_empresa) ? $publication->usu_empresa : $publication->usu_nombre; ?>
                                                <a href="<?php echo base_url() . 'perfil/'.$publication->usu_id.'/'.urlencode($empresa); ?>">
                                                    <?php if(!empty($publication->usu_foto)){ ?>
                                                        <img src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo base_url(); ?>assets/uploads/users/<?php echo strtolower($publication->usu_foto); ?>&h=36" alt="<?php echo $empresa; ?>" title="<?php echo $empresa; ?>" />
                                                    <?php }else{ ?>
                                                        <?php echo $empresa; ?>
                                                    <?php } ?>
                                                </a>
                                            <?php }else{ ?>
                                                <?php echo $empresa; ?>
                                            <?php } ?>
                                        </a>
                                    </div>
                                <?php endif; ?>

                            </div>
                            <div class="info_property stand_out" style="padding-bottom: 0;">
                                <ul>
                                    <li style="line-height: 18px; padding-bottom: 10px; height: 5em">
                                        <div style="font-weight: bold; margin-bottom:8px;"><?php echo $publication->cat_nombre . " ". $publication->for_descripcion; ?></div>
                                        <span style="float:none;"><?php echo crop_string($publication->inm_nombre, 60); ?></span>
                                    </li>
                                    <li>
                                        <div class="line_separator"></div>
                                        <strong>Precio</strong><span><?php echo $publication->mon_abreviado . " "; ?><?php echo number_format($publication->inm_precio,0,",",".") ?></span>
                                    </li>
                                </ul>
                                <div class="footer_property">
                                    <div class="icon m2" title="Metros Cuadrados"><i class="fa"><img src="<?php echo $path_assets; ?>img/icons/metros_cuadrados.png" /></i> <?php echo empty($publication->inm_superficie) ? 0 : $publication->inm_superficie; ?>m2</div>
                                    <?php foreach($publication->features as $feature): ?>
                                        <?php if($feature["eca_car_id"] == "4" && !empty($feature["eca_valor"])): ?>
                                            <div class="icon bedroom" title="Dormitorios"><i class="fa"><img src="<?php echo $path_assets; ?>img/icons/dormitorios.png" /></i> <?php echo $feature["eca_valor"]; ?></div>
                                        <?php  endif; ?>

                                        <?php if($feature["eca_car_id"] == "5" && !empty($feature["eca_valor"])): ?>
                                            <div class="icon bathroom" title="Baños"><i class="fa"><img src="<?php echo $path_assets; ?>img/icons/banos.png" /></i> <?php echo $feature["eca_valor"]; ?></div>
                                        <?php endif; ?>

                                        <?php if($feature["eca_car_id"] == "9" && !empty($feature["eca_valor"])): ?>
                                            <div class="icon parking" title="Parqueo"><i class="fa"><img src="<?php echo $path_assets; ?>img/icons/parqueo.png" /></i> <?php echo $feature["eca_valor"]; ?></div>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                    <br style="clear:both" />
                                </div>

                            </div>

                        </div>
                    </div>
                    <!-- Item Property-->
                <?php endforeach; ?>
            <?php endif; ?>


        </div>
        <!-- End Row Properties-->

    </div>
</div>
<!-- End content info -->

