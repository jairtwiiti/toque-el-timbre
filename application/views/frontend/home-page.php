<?php
$path_assets = base_url() . "assets/";
?>
<div id="layout-home">
<!-- Header-->
<header>
    <!-- Slide -->
    <div class="camera_wrap camera_white_skin">
        <!-- Item Slide -->
        <div >
            <div class="camera_caption fadeFromTop">
                <div class="cover-image">
                    <img class="big-cover-image" src="<?=base_url("assets/img/slide/cover-home-page.png")?>" alt="Bienvenido, sentite en casa"/>
                    <img class="small-cover-image"src="<?=base_url("assets/img/slide/cover-home-page-mobile.png")?>" alt="Bienvenido, sentite en casa"/>
                </div>
                <div class="gradient-image">
                    <img src="<?=base_url("assets/img/slide/gradient.png")?>">
                </div>
                <div class="search-form-section">
                    <?php
                    $this->load->view ("template-home-page/navbar");
                    ?>
                    <div class="container">
                        <div class="jumbotron">
                            <div class="col-md-12 text-center">
                                <h2 class="find-ideal-home-text">ENCUENTRA EL HOGAR IDEAL<h2>
                            </div>
                            <div class="col-md-12 text-center">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="btn-group filter-transaction-type" role="group" aria-label="...">
                                            <button type="button" class="btn btn-default active" data-transaction-type="venta">VENTA</button>
                                            <button type="button" class="btn btn-default" data-transaction-type="alquiler">ALQUILER</button>
                                            <button type="button" class="btn btn-default" data-transaction-type="anticretico">ANTICRETICO</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="row row-content-form">
                                    <div class="col-md-8 col-centered home-search-form">
                                        <form class="form-inline" name="search-form">
                                            <div class="form-group hidden-xs">
                                                <div class="dropdown property-type">
                                                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <span data-property-type="inmuebles">INMUEBLES</span>
                                                        <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                                        <li><a href="#" data-property-type="inmuebles">INMUEBLES</a></li>
                                                        <li><a href="#" data-property-type="casas">CASAS</a></li>
                                                        <li><a href="#" data-property-type="departamentos">DEPARTAMENTOS</a></li>
                                                        <li><a href="#" data-property-type="oficinas">OFICINAS</a></li>
                                                        <li><a href="#" data-property-type="locales-comerciales">LOCALES COMERCIALES</a></li>
                                                        <li><a href="#" data-property-type="terrenos">TERRENOS</a></li>
                                                        <li><a href="#" data-property-type="quintas-y-propiedades">QUINTAS Y PROPIEDADES</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="form-group visible-xs">
                                                <select class="form-control" data-property-type>
                                                    <option value="inmuebles">INMUEBLES</option>
                                                    <option value="casas">CASAS</option>
                                                    <option value="departamentos">DEPARTAMENTOS</option>
                                                    <option value="oficinas">OFICINAS</option>
                                                    <option value="locales-comerciales">LOCALES COMERCIALES</option>
                                                    <option value="terrenos">TERRENOS</option>
                                                    <option value="quintas-y-propiedades">QUINTAS Y PROPIEDADES</option>
                                                </select>
                                            </div>
                                            <div class="form-group hidden-xs">
                                                <div class="dropdown state">
                                                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <span data-state="santa-cruz">SANTA CRUZ</span>
                                                        <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                                        <li><a href="#" data-state="santa-cruz">SANTA CRUZ</a></li>
                                                        <li><a href="#" data-state="la-paz">LA PAZ</a></li>
                                                        <li><a href="#" data-state="cochabamba">COCHABAMBA</a></li>
                                                        <li><a href="#" data-state="tarija">TARIJA</a></li>
                                                        <li><a href="#" data-state="chuquisaca">CHUQUISACA</a></li>
                                                        <li><a href="#" data-state="oruro">ORURO</a></li>
                                                        <li><a href="#" data-state="potosi">POTOSI</a></li>
                                                        <li><a href="#" data-state="pando">PANDO</a></li>
                                                        <li><a href="#" data-state="beni">BENI</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="form-group visible-xs">
                                                <select class="form-control" data-state>
                                                    <option value="santa-cruz">SANTA CRUZ</option>
                                                    <option value="la-paz">LA PAZ</option>
                                                    <option value="cochabamba">COCHABAMBA</option>
                                                    <option value="tarija">TARIJA</option>
                                                    <option value="chuquisaca">CHUQUISACA</option>
                                                    <option value="oruro">ORURO</option>
                                                    <option value="potosi">POTOSI</option>
                                                    <option value="pando">PANDO</option>
                                                    <option value="beni">BENI</option>
                                                </select>
                                            </div>
                                            <button type="submit" class="btn btn-default search-button">BUSCAR</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- End Item Slide -->
    </div>
    <!-- End Slide -->

     <!-- Facebook Pixel Code -->
<!--     <script>-->
<!--     !function(f,b,e,v,n,t,s)-->
<!--     {if(f.fbq)return;n=f.fbq=function(){n.callMethod?-->
<!--     n.callMethod.apply(n,arguments):n.queue.push(arguments)};-->
<!--     if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';-->
<!--     n.queue=[];t=b.createElement(e);t.async=!0;-->
<!--     t.src=v;s=b.getElementsByTagName(e)[0];-->
<!--     s.parentNode.insertBefore(t,s)}(window,document,'script',-->
<!--     'https://connect.facebook.net/en_US/fbevents.js');-->
<!--      fbq('init', '1481111128837486');-->
<!--     fbq('track', 'PageView');-->
<!--     </script>-->
<!--     <noscript>-->
<!--      <img height="1" width="1" src="https://www.facebook.com/tr?id=1481111128837486&ev=PageView&noscript=1"/>-->
<!--     </noscript>-->
     <!-- End Facebook Pixel Code -->


</header>
<!-- End Header-->
<!-- Begin - Invitation to create an ads -->
<div class="filter_horizontal">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="box-sale-property">
                    SI DESEAS VENDER TU INMUEBLE, <span class="publish-it-now-text">PUBLICALO YA!.</span>
                    <button type="button" class="btn btn-info publish-it-now-button">CREAR ANUNCIO</button>
                </h2>
            </div>
        </div>
    </div>
</div>
<!-- End - Invitation to create an ads -->
<!-- content info -->
    <div class="content_info mobile-full-width">
        <div class="container">
        <!-- Content Carousel Properties -->
            <div class="content-carousel">
                <div class="row">
                    <div class="col-md-12">
                        <!-- Title-->
                        <div class="titles">
                            <h2 class="subtitle-home">Inmuebles a Estrenar</h2>
                            <hr class="subtitle-line">
                        </div>
                        <!-- End Title-->
                    </div>
                </div>
                <!-- Carousel Properties -->
                <div id="properties-carousel" class="on-carousel properties-carousel" style="margin-bottom: 15px;">
                    <div class="progress active">
                        <div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                            <span class="sr-only"></span>
                        </div>
                    </div>
                </div>
                <!-- End Carousel Properties -->
            </div>
        </div>
    </div>
    <div class="content_info mobile-full-width">
        <div class="container">
            <!-- End Content Carousel Properties -->
            <!-- Title-->
            <div class="row">
                <div class="col-md-12">
                    <div class="titles">
                        <h2 class="subtitle-home">Ultimos Anuncios</h2>
                        <hr class="subtitle-line">
                    </div>
                </div>
            </div>
            <!-- End Title-->
            <!-- Row Properties-->
            <div class="row" id="paid-publications">
                <div class="progress active">
                    <div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                        <span class="sr-only"></span>
                    </div>
                </div>
            </div>
            <!-- End Row Properties-->

            <!-- Row Publicidad-->
            <div class="row">
                <div class="col-md-12">
                    <center class="banner_home">
                        <a href="<?=base_url("ajax/form_busqueda_user") ?>" class="busqueda_usuario" style="text-align: center">
                            <img src="<?=base_url("assets/img/banner_home/torre_publicidad_large.png") ?>" style="width: 100%; max-width: 940px;"/>
                        </a>
                    </center>
                    <center class="banner_home_mobile">
                        <a href="<?=base_url("ajax/form_busqueda_user")?>" class="busqueda_usuario" style="text-align: center">
                            <img src="<?=base_url("assets/img/banner_home/torre_publicidad.png") ?>" style="width: 100%;" />
                        </a>
                    </center>
                </div>
            </div>
        <!-- End Row Publicidad-->

        </div>
    <!-- End Container -->
    </div>
    <!-- End content info-->
</div>

<?php
if($this->agent->is_mobile())
{
?>
    <script>
    location.hash = "#mobile";
    </script>
<?php
}
$this->load->view("handlebar-template/carousel-item-home-page");
//$this->load->view("handlebar-template/publication-quick-view");
$this->load->view("handlebar-template/publication-quick-view-2018");
$this->load->view("handlebar-template/quick-login-form");
$this->load->view('handlebar-template/welcome-to-tet-login-facebook');
?>