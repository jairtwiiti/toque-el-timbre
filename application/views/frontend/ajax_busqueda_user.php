<?php
$path_assets = base_url() . "assets/";
?>
<link href="<?php echo $path_assets; ?>css/style.css" rel="stylesheet" media="screen">

<div style="margin: 18px 12px;">

<?php if(!empty($sw_insert)){ ?>
    <div class="col-md-12 col-sm-12 col-xs-12" id="box_contact_agent">
        <h4>INFORMACION ENVIADA </h4>
        <p>Enviaremos tu solicitud a todas las agencias inmobiliarias registradas en ToqueelTimbre.com, ellos se contactaran al siguiente correo electronico:</p>
        <p style="font-size: 30px; font-weight: bold; padding: 8px 0;"><?php echo $_POST["email"]; ?></p>
        <p>Si el mensaje no aparece en tu bandeja de entrada, no olvides revisar tu carpeta de correo no deseado. Te contactaremos tan pronto tengamos mas resultados relacionados con tu búsqueda.</p>
    </div>
<?php }else{ ?>
<div class="col-md-12 col-sm-12 col-xs-12" id="box_contact_agent">
    <h4>RESULTADOS DE TU BUSQUEDA INTELIGENTE</h4>
    <form action="#" class="form_contact" id="form" method="post">
        <div class="row">
            <div class="col-md-12">
                <input type="text" name="nombre" required placeholder="Escribe su nombre y apellido" />
            </div>
            <div class="col-md-12">
                <input type="text" name="telefono" placeholder="Teléfono" />
            </div>
            <div class="col-md-12">
                <input type="email" name="email" required placeholder="Correo Electrónico" />
            </div>
            <div class="col-md-12">
                <h4>ESTOY BUSCANDO...</h4>
            </div>
            <div class="col-md-12">
                <select name="categoria">
                    <?php foreach($categories as $cat): ?>
                        <option value="<?php echo $cat['cat_id']; ?>"><?php echo $cat['cat_nombre']; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="col-md-12">
                <select name="in">
                    <?php foreach($forma as $for): ?>
                        <option value="<?php echo $for['for_id']; ?>"><?php echo $for['for_descripcion']; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="col-md-12">
                <select name="ciudad">
                    <?php foreach($cities as $city): ?>
                        <option value="<?php echo $city['dep_id']; ?>"><?php echo $city['dep_nombre']; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <!--<div class="col-md-6">
                <input type="checkbox" name="terminos" />
                Deseo recibir una notificación relacionada con mi búsqueda.
            </div>-->
        </div>
        <h4 class="modal-title" id="myModalLabel" style="padding: 6px 0;">INFORMACION ADICIONAL...</h4>
        <div class="row">
            <div class="col-md-12">
                <input type="text" name="descripcion" required placeholder="Escribe alguna descripción adicional (Ej.: Zona Norte hasta 50000$)..." />
            </div>
        </div>
        <input type="submit" class="button" style="background: rgba(0, 174, 239, 0.8) !important;" value="Enviar" name="Submit">
    </form>
    <div class="result"></div>
</div>
<?php } ?>
</div>