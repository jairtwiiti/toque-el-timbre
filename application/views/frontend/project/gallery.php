<style>
.thumbnail{ margin-bottom: 8px; }
.detail{ margin-bottom: 20px; padding: 0 2%; }
.detail p{
    margin-bottom: 4px;
}
</style>
<?php
$path_assets = base_url() . "assets/";
$gallery = $project_data["gallery"];
?>

<?php if(!empty($gallery) && $project_config->check_galeria == "Si"): ?>
    <!-- Tipologia -->
    <div class="row">
        <div class="col-md-12">
            <h3 style="margin-bottom: 20px;"><?php echo $project_config->menu_galeria; ?></h3>
            <div class="row">
                <?php foreach($gallery as $image): ?>
                    <div class="col-md-3">
                        <?php
                            $fileHandler = new File_Handler($image->proy_fot_imagen,"projectImage");
                            $thumbnailImage = $fileHandler->getThumbnail(180,180);
                        if($image->proy_fot_imagen != "")
                        {
                        ?>
                        <a href="<?=base_url("admin/imagenes/proyecto/".strtolower($image->proy_fot_imagen))?>" rel="group1" class="thumbnail precio_zoom" title="<?php echo $image->proy_fot_descripcion; ?>">
                            <img alt="" src="<?=$thumbnailImage->getSource()?>">
                        </a>
                        <?php
                        }
                        ?>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <!-- End Tipologia-->
<?php endif; ?>