<div class="row">
    <?php if(!empty($tipologia)){ ?>
        <div class="col-sm-12 col-md-12 col-lg-6" style="text-align: center;">
            <img src="<?php echo base_url(); ?>admin/imagenes/proyecto/<?php echo $tipologia->tip_imagen; ?>" style="max-width: 100%; max-height: 800px !important;" />
        </div>
        <div class="col-sm-12 col-md-12 col-lg-6">
            <h2 style="margin-top: 6px; font-weight: bold;"><?php echo $tipologia->tip_titulo; ?></h2>
            <div><?php echo $tipologia->tip_descripcion; ?></div>
        </div>
    <?php }else{ ?>
        <div class="col-sm-12 col-md-12 col-lg-6" style="text-align: center;">No se encontro resultados, intente nuevamente</div>
    <?php } ?>
</div>