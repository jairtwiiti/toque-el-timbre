<?php
$path_assets = base_url() . "assets/";
$project = $project_data["project"];
$tipologia = $project_data["tipologia"];
?>
<!-- Post-->
<?php if($project_config->check_descripcion == "Si"): ?>
<div class="row">
    <div class="col-md-12">
        <h3><?php echo $project_config->menu_inicio; ?></h3>
        <div>
            <?php echo $project->proy_descripcion; ?>
        </div>
    </div>
</div>
<?php endif; ?>
<!-- End Post-->

<?php if(!empty($tipologia) && $project_config->check_tipologia == "Si"): ?>
<!-- Tipologia -->
<div class="row">
    <div class="col-md-12">
        <h3><?php echo $project_config->menu_tipologia; ?></h3>
        <div class="row">
            <?php foreach($tipologia as $tip): ?>
                <div class="col-md-4">
                    <?php
                    $fileimage = BASE_DIRECTORY . DS . "admin" . DS . "imagenes" . DS . "proyecto" . DS . $tip->tip_imagen;
                    if (!empty($tip->tip_imagen) && !is_null($tip->tip_imagen) && file_exists($fileimage)) { ?>
                        <a href="javascript:;" rel="<?php echo $tip->tip_id; ?>" class="thumbnail tipologia_ajax" title="<?php echo $tip->tip_titulo; ?>">
                            <img alt="" src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo base_url(); ?>admin/imagenes/proyecto/<?php echo strtolower($tip->tip_imagen); ?>&w=400&h=400">
                        </a>
                    <?php } else { ?>
                        <a href="<?php echo $path_assets.'img/default.png' ?>" rel="<?php echo $tip->tip_id; ?>" class="thumbnail tipologia_ajax">
                            <img src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo $path_assets.'img/default.png'?>&w=500&h=281" alt="toqueeltimbre" title="toqueeltimbre">
                        </a>
                    <?php } ?>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<!-- End Tipologia-->
<?php endif; ?>

<!-- Map-->
<?php if($project_config->check_ubicacion == "Si"): ?>
<div class="row">
    <div class="col-md-12">
        <h3><?php echo $project_config->menu_ubicacion; ?></h3>
            <?php if($project->proy_ubicacion_img != ""){ ?>
                <center><img src="<?php echo base_url(); ?>admin/imagenes/proyecto/<?php echo $project->proy_ubicacion_img; ?>" style="width: 100%; max-height: auto; margin-bottom: 10px;" /></center>
            <?php }else{ ?>
                <div class="map_area">
                    <input name="latitude" value="<?=$project->proy_latitud?>" type="hidden" required>
                    <input name="longitude" value="<?=$project->proy_longitud?>" type="hidden" required data-parsley-errors-container="#map-location-error-message-parsley" data-parsley-error-message="Debe marcar un punto en el mapa">
                    <div class="map-fancy-framework">
                        <div id="maps" style="height: 300px;width: auto"></div>
                    </div>
                </div>
            <?php } ?>
            <div>
                <br>
                <?php echo $project->proy_ubicacion; ?>
            </div>
    </div>
</div>
<?php endif; ?>
<!--End  Map-->

