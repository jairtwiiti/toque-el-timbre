<?php
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 20/11/2017
 * Time: 11:43 AM
 */
?>
<aside class="project-contact-form">
    <div class="project-contact-box">
        <!-- Datos del Venderos -->
        <div class="project-seller-detail hide">
            <div class="row item_agent" style="margin-bottom: 0;">
                <div class="col-md-12 info_agent">
<!--                    <h5 style="padding-top:0; float: left;">Jazmin Angus Bienes y raíces</h5>-->
                    <br style="clear: both;">
<!--                    <ul>-->
<!--                        <li style="font-size: 14px;"><i class="fa fa-envelope"></i><a href="mailto:jazminangusnieto@gmail.com">jazminangusnieto@gmail.com</a></li>-->
<!--                        <li style="font-size: 14px;"><i class="fa fa-phone"></i><a class="no_link" href="tel:78484127">78484127</a></li>-->
<!--                        <li style="font-size: 14px;"><i class="fa fa-mobile-phone"></i><a class="no_link" href="tel:3266044">3266044</a>-->
<!--                        </li>-->
<!--                    </ul>-->
                    <div style="font-size: 8px!important">
                    <?php
//                    echo $project->proy_contacto;
                    ?>
                    </div>
                </div>
            </div>
            <p class="description_agency"></p>
<!--            <p><a href="http://localhost/toque-el-timbre/perfil/8638/jazmin-angus-bienes-y-raices" style="font-size: 14px; text-decoration: underline;">Ver Perfil</a></p>-->
        </div>
        <!-- Fin Datos del Venderor -->
        <div id="box_contact_agent" style="margin-top: 10px;">
            <a name="Formulario"></a>
            <h2>Contacta al anuncinate</h2>
            <form id="form" class="form_contact_project" action="#">
                <div class="row">
                    <div class="col-md-12">
                        <input type="text" placeholder="Nombre" name="nombre" required>
                    </div>
                    <div class="col-md-12">
                        <select name="ciudad">
                            <?php foreach($cities as $city): ?>
                                <?php $valor = ucwords(strtolower($city->dep_nombre)); ?>
                                <option value="<?php echo $valor; ?>"><?php echo $valor; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-md-12">
                        <input type="text" placeholder="Telefono" name="telefono" required>
                    </div>
                    <div class="col-md-12">
                        <input type="email" placeholder="Email"  name="email" required>
                    </div>
                </div>
                <textarea placeholder="Tu comentario..." name="mensaje" required></textarea>
                <input type="hidden" name="proy_nombre" value="<?php echo $project->proy_nombre; ?>" />
                <input type="hidden" name="proy_id" value="<?php echo $project->proy_id; ?>" />
                <input type="hidden" name="email_send" value="<?php echo $project->usu_email; ?>" />
<!--                <input type="hidden" name="empresa" value="">-->
<!--                <input type="hidden" name="id_inmueble" value="16598">-->
<!--                 <div id="recaptcha" class="g-recaptcha" data-sitekey="6Lf7ICkUAAAAAFsX-ONUz1lSefiUGIAIsBgN39jD" data-callback="recaptchaSucess" data-size="invisible"><div class="grecaptcha-badge" style="width: 256px; height: 60px; transition: right 0.3s ease; position: fixed; bottom: 14px; right: -186px; box-shadow: gray 0px 0px 5px;"><div class="grecaptcha-logo"><iframe src="https://www.google.com/recaptcha/api2/anchor?k=6Lf7ICkUAAAAAFsX-ONUz1lSefiUGIAIsBgN39jD&amp;co=aHR0cDovL2xvY2FsaG9zdDo4MA..&amp;hl=es&amp;v=r20171115120512&amp;size=invisible&amp;cb=fx09o620ng0d" width="256" height="60" role="presentation" frameborder="0" scrolling="no" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-top-navigation allow-modals allow-popups-to-escape-sandbox"></iframe></div><div class="grecaptcha-error"></div><textarea id="g-recaptcha-response" name="g-recaptcha-response" class="g-recaptcha-response" style="width: 250px; height: 40px; border: 1px solid #c1c1c1; margin: 10px 25px; padding: 0px; resize: none;  display: none; "></textarea></div></div>-->
                <input type="submit" id="project-form-contact-button" name="Submit" value="Enviar" class="button">
            </form>
            <div class="result"></div>
        </div>
    </div>
</aside>