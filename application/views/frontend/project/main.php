<?php
$menu_active = $this->uri->segment(2);
$project_url = $this->uri->segment(1);
$path_assets = base_url() . "assets/";
$project = $project_data["project"];
$lower_price = $project_data["lower_price"];
$slideshow = $project_data["slideshow"];
$social = $project_data["sw_area_social"];
$data["cities"] = $cities;
$data["project_data"] = $project_data;
$data["project_config"] = $project_config;
?>

<!-- End content info -->
<section id="project_detail" class="content_info content_info_search" style="padding-top: 0 !important; border-top: 5px solid #00aeef;">
    <div class="container">

        <?php if(!empty($project)){ ?>
        <div class="row padding_top" style="padding-top: 16px;">
            <div class="col-md-12">
                <h1 style="padding-bottom: 5px;">
                    <?php
                    echo $project->proy_nombre;
                    $this->load->view("facebook-share-button-snippet");
                    ?>
                </h1>
                <p>Precios a partir de <?php echo number_format($lower_price->menor_precio, 0,",","."); ?> $US.</p>
            </div>
        </div>

        <div class="row project-banner">
            <div class="col-md-12">

                <div class="row">
                    <div class="imagen_video col-md-12" style="position: relative;">
                        <div class="proyecto_logo">
                            <?php
                            $fileimage = BASE_DIRECTORY . DS . "admin" . DS . "imagenes" . DS . "proyecto" . DS . $project->proy_logo;    
                            if (!empty($project->proy_logo) && !is_null($project->proy_logo) && file_exists($fileimage)) { ?>
                                <img src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo base_url(); ?>admin/imagenes/proyecto/<?php echo $project->proy_logo; ?>&w=200" />
                            <?php } else { ?>
                                <img src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo $path_assets.'img/default.png'?>&w=500&h=281" alt="toqueeltimbre" title="toqueeltimbre">
                            <?php }  ?>
                        </div>

                        <div class="proyecto_video">
                            <div class="camera_wrap" id="slide_projects">
                                   <?php
                                   if(!empty($slideshow)){
                                   foreach($slideshow as $image):
                                        $icon_video = $link = "";
                                        if(!empty($image->youtube)){
                                            $icon_video = '<div class="fadeIn icon_play"><div class="row"><a class="play_video" href="https://www.youtube.com/embed/'.$image->youtube.'?rel=0&amp;wmode=transparent"><img src="'.$path_assets.'img/project/icon-video.png" /></a></div></div>';
                                            $link = 'data-link="https://www.youtube.com/embed/'.$image->youtube.'?rel=0&amp;wmode=transparent"';
                                        }
                                   ?>
                                   <?php
                                        $fileimage = BASE_DIRECTORY . DS . "admin" . DS . "imagenes" . DS . "proyecto_slideshow" . DS . strtolower($image->imagen);
                                        if(!empty($image->imagen) && !is_null($image->imagen) && file_exists($fileimage)) { ?>
                                        <!--<div <?php echo $link; ?> data-src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo base_url(); ?>admin/imagenes/proyecto_slideshow/<?php echo strtolower($image->imagen); ?>&w=1144&h=458"><?php echo $icon_video; ?></div>-->
                                            <div <?php echo $link; ?> data-src="<?php echo base_url(); ?>admin/imagenes/proyecto_slideshow/<?php echo strtolower($image->imagen); ?>"><?php echo $icon_video; ?></div>
                                   <?php } else { ?>
                                        <div <?php echo $link; ?> data-src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo $path_assets.'img/default.png'?>&w=1144&h=458"><?php echo $icon_video; ?></div>
                                   <?php } ?>
                                   <?php
                                   endforeach; 
                                   }else{ ?>
                                        <div <?php echo $link; ?> data-src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo $path_assets.'img/default.png'?>&w=1144&h=458"><?php echo $icon_video; ?></div>
                                  <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="row project-menu">
            <div class="col-md-12">
                <!--NAV TABS-->
                <ul class="tabs" style="margin-bottom: 18px;">
                    <?php if($project_config->check_galeria == "Si"): ?>
                        <li <?php echo $menu_active == "fotogaleria" ? 'class="active"' : ''; ?>>
                            <a href="<?php echo base_url() . $project_url; ?>/fotogaleria<?php echo isset($_GET['q']) ? '?q=true' : ''; ?>"><?php echo $project_config->menu_galeria; ?></a>
                        </li>
                    <?php endif; ?>
                    <?php if($project_config->check_social == "Si"): ?>
                        <li <?php echo $menu_active == "area-social" ? 'class="active"' : ''; ?>>
                            <a href="<?php echo base_url() . $project_url; ?>/area-social<?php echo isset($_GET['q']) ? '?q=true' : ''; ?>"><?php echo $project_config->menu_social; ?></a>
                        </li>
                    <?php endif; ?>
                    <?php if($project_config->check_acabado == "Si"): ?>
                        <li <?php echo $menu_active == "acabado" ? 'class="active"' : ''; ?>>
                            <a href="<?php echo base_url() . $project_url; ?>/acabado<?php echo isset($_GET['q']) ? '?q=true' : ''; ?>"><?php echo $project_config->menu_acabado; ?></a>
                        </li>
                    <?php endif; ?>
                    <?php if($project_config->check_precio == "Si"): ?>
                        <li <?php echo $menu_active == "precio" ? 'class="active"' : ''; ?>>
                            <a href="<?php echo base_url() . $project_url; ?>/precio<?php echo isset($_GET['q']) ? '?q=true' : ''; ?>"><?php echo $project_config->menu_precio; ?></a>
                        </li>
                    <?php endif; ?>
                    <li <?php echo empty($menu_active) ? 'class="active"' : ''; ?>>
                        <a href="<?php echo base_url() . $project_url; ?><?php echo isset($_GET['q']) ? '?q=true' : ''; ?>"><?php echo $project_config->menu_inicio; ?></a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="row">

            <!-- Blog Post-->
            <div class="col-md-8 col-lg-9 col-detail">
                <?php echo $this->load->view($view_name, $data, TRUE); ?>
                <div class="row hide" style="margin-top:65px;">
                    <?php if($project_config->check_formulario == "Si"): ?>
                        <div id="box_contact_agent" class="col-md-6 col-project-form">
                            <a name="Formulario"></a>
                            <h3>Formulario de Contacto</h3>
                            <form id="form" class="form_contact_project_old" action="#">
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="text" placeholder="Nombre" name="nombre" required>
                                    </div>
                                    <div class="col-md-6">
                                        <select name="ciudad">
                                            <?php foreach($cities as $city): ?>
                                                <?php $valor = ucwords(strtolower($city->dep_nombre)); ?>
                                                <option value="<?php echo $valor; ?>"><?php echo $valor; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="text" placeholder="Telefono" name="telefono" required>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="email" placeholder="Email"  name="email" required>
                                    </div>
                                </div>
                                <textarea placeholder="Tu comentario..." name="mensaje" required></textarea>
                                <input type="hidden" name="proy_nombre" value="<?php echo $project->proy_nombre; ?>" />
                                <input type="hidden" name="proy_id" value="<?php echo $project->proy_id; ?>" />
                                <input type="hidden" name="email_send" value="<?php echo $project->usu_email; ?>" />
                                <input type="submit" name="Submit" value="Enviar" class="button">
                            </form>
                            <div class="result"></div>
                        </div>
                    <?php endif; ?>
                    <?php if($project_config->check_contacto == "Si"): ?>
                        <div class="col-md-6 col-project-info">
                            <h3>Vendedor</h3>
                            <div><?php echo $project->proy_contacto; ?></div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>

            <!-- End Blog Post-->
            <!-- Sidebars-->
            <div class="col-md-4 col-lg-3 proyecto_col_left">
                <!-- Search Advance -->
                <aside class="hide">
                    <div class="search_advance">
                        <ul class="tabs_services">
                            <li><a id="1" class="set2">Buscar otros Inmuebles</a></li>
                        </ul>
                        <!-- 1-content -->
                        <div id="1-content" class="set2 show">
                            <div class="search_box">
                                <form action="<?=base_url("buscar")?>" method="post">
                                    <div>
                                        <label>Estoy buscando...</label>
                                        <select name="type">
                                            <option value="Inmuebles">Todas los tipos</option>
                                            <?php foreach($categories as $cat): ?>
                                                <?php $valor = ucwords(strtolower($cat->cat_nombre)); ?>
                                                <option value="<?php echo $valor; ?>"><?php echo $valor; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div>
                                        <label>En la ciudad de...</label>
                                        <select name="city">
                                            <option value="Bolivia">Todas las ciudades</option>
                                            <?php foreach($cities as $city): ?>
                                                <?php $valor = ucwords(strtolower($city->dep_nombre)); ?>
                                                <option value="<?php echo $valor; ?>" ><?php echo $valor; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div >
                                        <label>En...</label>
                                        <select name="in">
                                            <option value="Venta">Venta</option>
                                            <option value="Alquiler" >Alquiler</option>
                                            <option value="Anticretico" >Anticretico</option>
                                        </select>
                                    </div>
                                    <div>
                                        <input type="hidden" name="currency" id="currency" value="Dolares" />
                                        <input type="submit" class="button" style="background-color: rgba(0, 174, 239, 1) !important; margin-top: 25px; font-weight: 600;" value="Mostrar Resultados">
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- End 1-content -->
                    </div>
                </aside>
                <!-- End Search Advance -->
                <!-- start - contact form -->
                <div class="sticky-div hide">This is a sticky div</div>
                <?php
                $data["project"] = $project;
                $this->load->view("frontend/project/project-contact-form",$data);
                ?>
                <!-- end - contact form -->
            </div>
            <!-- Sidebars-->
        </div>

        <?php }else{ ?>
            <div class="row padding_top margin_top">
                <div class="col-md-12">
                    <div class="alert alert-danger">
                        <strong>Oops!</strong> Este proyecto no existe o se encuentra deshabilitado.
                    </div>
                </div>
            </div>
        <?php } ?>

        <div class="row">
            <div class="col-md-12">
                <div class="line" style="margin-top: 15px; background: #F0F0F0 !important; height: 4px"></div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12">
                <!-- Title-->
                <div class="titles">
                    <h2 style="display: inline; font-size: 30px; line-height: 40px; margin: 20px 0; padding-bottom: 7px;">Otros Proyectos Inmobiliarios</h2>
                </div>
                <!-- End Title-->
            </div>
        </div>

        <!-- Carousel Properties -->
        <div id="properties-carousel" class="on-carousel properties-carousel" style="margin-bottom: 15px;">
            <?php if(!empty($inmueble_projects)): ?>
                <?php foreach($inmueble_projects as $project): ?>
                    <!-- Item Property-->
                    <div class="item_property">
                        <div class="head_property">
                            <a href="<?php echo base_url(). $project->proy_seo; ?>">
                                <!-- <img src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo base_url(); ?>admin/imagenes/proyecto/<?php echo $project->proy_fot_imagen; ?>&w=271&h=183" alt="<?php echo $project->inm_nombre; ?>" title="<?php echo $project->inm_nombre; ?>"> -->
                                <?php
                                $fileimage = BASE_DIRECTORY . DS . "admin" . DS . "imagenes" . DS . "proyecto" . DS . $project->proy_fot_imagen;
                                if (!empty($project->proy_fot_imagen) && !is_null($project->proy_fot_imagen) && file_exists($fileimage)) { ?>
                                    <img src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo base_url(); ?>admin/imagenes/proyecto/<?php echo $project->proy_fot_imagen?>&w=271&h=183" alt="<?php echo $project->inm_nombre; ?>" title="<?php echo $project->inm_nombre; ?>">
                                <?php } else { ?>
                                    <img src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo $path_assets.'img/default.png'?>&w=500&h=281" alt="toqueeltimbre" title="toqueeltimbre">
                                <?php } ?>
                                <h5><?php echo $project->ciu_nombre; ?></h5>
                            </a>
                        </div>
                        <div class="info_property">
                            <ul>
                                <li><div class="bold_text" style="font-size: 14px;"><?php echo $project->inm_nombre; ?></div></li>
                                <li>
                                    <div class="line_separator"></div>
                                    <span class="bold_text" style="float: none; font-size: 14px;"><?php echo ucwords(strtolower($project->for_descripcion)); ?></span>
                        <span>
                            <?php echo $project->mon_abreviado; ?>
                            <?php echo number_format($project->inm_precio, 0, ",", "."); ?>
                        </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- Item Property-->
                <?php endforeach; ?>
            <?php endif; ?>

        </div>
        <!-- End Carousel Properties -->



    </div>
</section>
<!-- End content info-->

