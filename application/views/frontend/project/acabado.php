<style>
.thumbnail{ margin-bottom: 8px; }
.detail{ margin-bottom: 20px; padding: 0 2%; }
.detail p{
    margin-bottom: 4px;
}
</style>
<?php
$path_assets = base_url() . "assets/";
$acabado = $project_data["acabado"];
?>

<?php if(!empty($acabado) && $project_config->check_acabado == "Si"): ?>
    <!-- Tipologia -->
    <div id="proyecto_det_acabado" class="row">
        <div class="col-md-12">
            <h3 style="margin-bottom: 20px;"><?php echo $project_config->menu_acabado; ?></h3>
            <?php foreach($acabado as $block): ?>
                <div class="row">
                    <?php foreach($block as $aux): ?>
                        <div class="col-md-4">
                            <?php if($aux->aca_imagen != ""): ?>
                                <!--
                                <a href="<?php echo base_url(); ?>admin/imagenes/proyecto/<?php echo strtolower($aux->aca_imagen); ?>" rel="group1" class="thumbnail precio_zoom">
                                    <img alt="" src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo base_url(); ?>admin/imagenes/proyecto/<?php echo strtolower($aux->aca_imagen); ?>&w=400&h=400">
                                </a>
                                -->
                                <?php
                                $fileimage = BASE_DIRECTORY . DS . "admin" . DS . "imagenes" . DS . "proyecto" . DS . $aux->aca_imagen;
                                if (!empty($aux->aca_imagen) && !is_null($aux->aca_imagen) && file_exists($fileimage)) { ?>
                                    <a href="<?php echo base_url(); ?>admin/imagenes/proyecto/<?php echo strtolower($aux->aca_imagen); ?>" rel="group1" class="thumbnail precio_zoom">
                                        <img alt="" src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo base_url(); ?>admin/imagenes/proyecto/<?php echo strtolower($aux->aca_imagen); ?>&w=400&h=400">
                                    </a>
                                <?php } else { ?>
                                    <a href="<?php echo $path_assets.'img/default.png' ?>" rel="group1" class="thumbnail precio_zoom">
                                        <img src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo $path_assets.'img/default.png'?>&w=500&h=281" alt="toqueeltimbre" title="toqueeltimbre">
                                    </a>
                                <?php } ?>

                            <?php endif; ?>
                            <div class="detail">
                                <?php echo $aux->aca_descripcion; ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    <!-- End Tipologia-->
<?php endif; ?>