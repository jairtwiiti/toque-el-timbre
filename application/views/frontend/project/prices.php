<style>
.thumbnail{ margin-bottom: 8px; }
.precio_detail{ margin-bottom: 20px; padding: 0 2%; }
.precio_detail p{
    margin-bottom: 4px;
}
</style>
<?php
$assetsPath = base_url() . "assets/";
$prices = $project_data["tipologia"];
$project = $project_data["project"];
?>

<?php if(!empty($prices) && $project_config->check_precio == "Si")
    { ?>
    <!-- Tipologia -->
    <div class="row check-this">
        <div class="col-md-12">
            <h3 style="margin-bottom: 20px;"><?=$project_config->menu_precio; ?></h3>
                <?php
                    $i=0;
                    $startRow = "<div class='row'>";
                    $endRow = "</div>";
                    foreach($prices as $price)
                    {

                        $title = !empty($price->tip_titulo_precio) ? $price->tip_titulo_precio : $price->tip_titulo;
                        $image = !empty($price->tip_imagen_precio) ? $price->tip_imagen_precio : $price->tip_imagen;

                        if($i == 0)
                        {
                            echo $startRow;
                        }
                        elseif($i%3 == 0)
                        {
                            echo $endRow.$startRow;
                        }
                    ?>
                    <div class="col-md-4">
                        <?php
                        $imageFile = BASE_DIRECTORY . DS . "admin" . DS . "imagenes" . DS . "proyecto" . DS . $image;
                        if (!empty($image) && !is_null($image) && file_exists($imageFile))
                        {
                        ?>
                            <a href="<?php echo base_url(); ?>admin/imagenes/proyecto/<?php echo strtolower($image); ?>" rel="group1" class="thumbnail precio_zoom" title="<?php echo $title; ?>">
                                <img alt="" src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo base_url(); ?>admin/imagenes/proyecto/<?php echo strtolower($image); ?>&w=400&h=400">
                            </a>
                        <?php
                        }
                        else
                        {
                        ?>
                            <a href="<?php echo $assetsPath.'img/default.png' ?>" rel="group1" class="thumbnail precio_zoom">
                                <img src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo $assetsPath.'img/default.png'?>&w=500&h=281" alt="toqueeltimbre" title="toqueeltimbre">
                            </a>
                        <?php
                        }
                        ?>
                        <div class="precio_detail">
                            <h4><?php echo $title; ?></h4>
                            <?php
                            if( $price->tip_terreno_mts != '' && $price->tip_ver_suptot == 1)
                            {
                            ?>
                                <p>Superficie Total: </p>
                                <p><?php echo $project->proy_id == 74?"Desde ":"".$price->tip_terreno_mts; ?> mts2</p>
                            <?php
                            }
                            ?>

                            <?php
                            if($price->tip_ver_supcon == 1)
                            {
                            ?>
                                <p>Superficie Construida:</p>
                                <p><?php echo $price->tip_mts; ?> mts2</p>
                            <?php
                            }
                            ?>

                            <?php
                            if($price->tip_ver_precio == 1)
                            {
                            ?>
                                <p>$us. <?php echo number_format($price->tip_precio, 0, ",", "."); ?></p>
                            <?php
                            }
                            ?>
                        </div>
                        <?php
                        if($project_config->check_tipologia_det == "Si")
                        {
                        ?>
                        <div class="precio_detalle_tip">
                            <a href="javascript:;" rel="<?php echo $price->tip_id; ?>" class="button tipologia_ajax" title="<?php echo $price->tip_titulo; ?>" style="background-color: rgba(0, 174, 239, 1) !important; width: 100%; display: block; text-align: center; font-weight: 600; padding-top: 6px; padding-bottom: 6px;">Ver Detalle</a>
                        </div>
                        <?php
                        }
                        ?>
                    </div>
                    <?php
                        $i++;
                        if($i == count($prices))
                        {
                            echo $endRow;
                        }
                    }
                    ?>
        </div>
    </div>
    <!-- End Tipologia-->
<?php } ?>