<style>
    .thumbnail{ margin-bottom: 8px; }
    .detail{ margin-bottom: 20px; padding: 0 2%; }
    .detail p{
        margin-bottom: 4px;
    }
</style>
<?php
$path_assets = base_url() . "assets/";
$social = $project_data["social"];
?>

<?php if(!empty($social) && $project_config->check_social == "Si"): ?>
    <!-- Tipologia -->
    <div class="row">
        <div class="col-md-12">
            <h3 style="margin-bottom: 20px;"><?php echo $project_config->menu_social; ?></h3>

            <?php foreach($social as $block): ?>
            <div class="row">
                <?php foreach($block as $aux): ?>
                    <div class="col-md-4">
                        <?php if($aux->are_soc_imagen != ""): ?>
                        <!--
                        <a href="<?php echo base_url(); ?>admin/imagenes/proyecto/<?php echo strtolower($aux->are_soc_imagen); ?>" rel="group1" class="thumbnail precio_zoom">
                                    <img alt="" src="<?php echo base_url(); ?>librerias/timthumb.php?src=http://www.toqueeltimbre.com/admin/imagenes/proyecto/<?php echo strtolower($aux->are_soc_imagen); ?>&w=400&h=400">
                                </a>
                                -->

                            <?php
                            $fileimage = BASE_DIRECTORY . DS . "admin" . DS . "imagenes" . DS . "proyecto" . DS . $aux->are_soc_imagen;

                            if (!empty($aux->are_soc_imagen) && !is_null($aux->are_soc_imagen) && file_exists($fileimage)) { ?>
                                <a href="<?php echo base_url(); ?>admin/imagenes/proyecto/<?php echo strtolower($aux->are_soc_imagen); ?>" rel="group1" class="thumbnail precio_zoom">
                                    <img alt="" src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo base_url(); ?>admin/imagenes/proyecto/<?php echo strtolower($aux->are_soc_imagen); ?>&w=400&h=400">
                                </a>
                            <?php } else { ?>
                                <a href="<?php echo $path_assets.'img/default.png' ?>" rel="group1" class="thumbnail precio_zoom">
                                    <img src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo $path_assets.'img/default.png'?>&w=400&h=400" alt="toqueeltimbre" title="toqueeltimbre">
                                </a>
                            <?php } ?>

                        <?php endif; ?>
                        <div class="detail">
                            <h4><?php echo $aux->are_soc_titulo; ?></h4>
                            <?php echo $aux->are_soc_descripcion; ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <?php endforeach; ?>

        </div>
    </div>
    <!-- End Tipologia-->
<?php endif; ?>