<!DOCTYPE html>
<html>
<head>
    <title>Google Maps</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <style>
        html, body, #map_canvas {
            margin: 0;
            padding: 0;
            height: 100%;
        }
    </style>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
    <script>
        var map;
        function initialize() {
            var myLatlng = new google.maps.LatLng(<?php echo $_GET["latitud"]; ?>,<?php echo $_GET["longitud"]; ?>);
            var mapOptions = {
                zoom: <?php echo !empty($_GET["zoom"]) ? $_GET["zoom"] : 16; ?>,
                center: myLatlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById('map_canvas'),mapOptions);

            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map
            });
        }

        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
</head>
<body>
<div id="map_canvas"></div>
</body>
</html>