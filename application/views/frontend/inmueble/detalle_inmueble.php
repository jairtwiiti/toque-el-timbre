<?php
$caracteristicas = $inmueble->caracteristicas;
$fotos = $inmueble->fotos;
$planos = $inmueble->planos;
$similar_properties = $inmueble->similar["results"];
$pubishied_proyects = $inmueble->proyectos_publicados;
$inmueble_detail = $inmueble;
$now = date("Y-m-d");
$sw_fecha = $inmueble->pub_vig_fin >= $now ? true : false;
?>
<!-- End content info -->
<section class="content_info content_info_search mobile-full-width" style="padding-top: 0 !important; border-top: 5px solid #00aeef;">
    <div class="container">

        <div class="row padding_top">
            <!-- Blog Post-->
            <div id="col-detail" class="col-md-8 col-lg-9">
                <!-- Post-->
                <div class="post single">

                    <?php
                    if(!empty($inmueble) && $sw_fecha && $inmueble->pub_estado == "Aprobado")
                    {
                    ?>
                            <div class="row property-detail-primary-info">
                                <input type="hidden" value="<?=$inmueble->id?>" id="property-id">
                                <div class="col-md-10">
                                    <h1>
                                        <?php
                                        echo $inmueble_detail->nombre;
                                        $this->load->view("facebook-share-button-snippet");
                                        ?>
                                    </h1>
                                    <p <?php echo $inmueble->usu_certificado == "Si" ? 'style="margin-bottom: 6px;"' : ''; ?>><?php echo !empty($inmueble_detail->direccion) ? $inmueble_detail->direccion .", ".$inmueble_detail->ciudad : $inmueble_detail->ciudad; ?></p>
                                    <?php
                                    if($inmueble->usu_certificado == "Si")
                                    {
                                    ?>
                                    <p>
                                        <img src="<?=assets_url("img/icon_agente_certificados.png")?>" style="margin-right:3px;" alt="Este Inmueble pertenece a un Agente Certificado" title="Este Inmueble pertenece a un Agente Certificado"  /> <span><i>Este Inmueble pertenece a un Agente Certificado</i></span>
                                    </p>
                                    <?php
                                    }
                                    ?>
                                </div>
                                <div class="col-md-2">
                                    <div class="favorito">
                                        <a href="javascript:void(0);" class="btn_favorite <?php echo $favorite ? 'check_favorite' : ''; ?>" rel="<?php echo $inmueble_detail->id; ?>">
                                            <i class="fa fa-star"></i> <span>Agregar a favoritos</span>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <!-- Image Postt-->
                            <div class="image_post">
                                <div class="image_detail">
                                    <!-- Slide News-->
                                    <?php if($inmueble->usu_certificado == "Si"): ?>
                                        <div style="position: absolute; right: 10px; top: 10px; z-index: 9999;">
                                            <img src="<?=base_url("assets/img/icon_agente_certificados.png")?>" style="margin-right:3px;" alt="Este Inmueble pertenece a un Agente Certificado" title="Este Inmueble pertenece a un Agente Certificado"  />
                                        </div>
                                    <?php endif; ?>

                                    <div class="camera_wrap" id="slide_details">
                                        <?php
                                        foreach($fotos as $img)
                                        {
                                            $fileHandler = new File_Handler($img->fot_archivo);
                                            $thumbnail = $fileHandler->getThumbnail(850,470);
                                            ?>
                                            <div data-src="<?=$thumbnail->getSource()?>"></div>
                                            <?php
                                        }
                                        ?>

                                    </div>
                                    <!-- End Slide-->
                                    <div class="price">
                                        <h2><?php echo number_format($inmueble_detail->precio,0,",",".") . " " . $inmueble_detail->moneda; ?></h2>
                                        <span class="property-detail-category-and-transaction"><?=$inmueble_detail->categoria." EN ".$inmueble_detail->forma?></span>
                                    </div>
                                </div>
                                <div class="icons_detail">
                                    <?php $tipo_sup = $inmueble_detail->tipo_superficie == "Metros Cuadrados" ? "m2" : "ha"; ?>
                                    <ul>
                                        <li><i class="fas fa-arrows-alt"></i> <?php echo empty($inmueble_detail->superficie) ? 0 : $inmueble_detail->superficie; ?><?php echo $tipo_sup; ?></li>
                                        <?php foreach($caracteristicas as $feature): ?>
                                            <?php if($feature->caracteristica == "habitaciones" && !empty($feature->valor) && is_numeric($feature->valor)): ?>
                                                <li><i class="fas fa-bed"></i> <?php echo $feature->valor; ?></li>
                                            <?php  endif; ?>
                                            <?php if($feature->caracteristica == "banos" && !empty($feature->valor) && is_numeric($feature->valor)): ?>
                                                <li><i class="fas fa-bath"></i> <?php echo $feature->valor; ?></li>
                                            <?php endif; ?>
                                            <?php if($feature->caracteristica == "parqueo" && !empty($feature->valor) && is_numeric($feature->valor)): ?>
                                                <li><i class="fas fa-car"></i> <?php echo $feature->valor; ?></li>
                                            <?php endif; ?>
                                        <?php endforeach; ?>

                                    </ul>
                                </div>
                            </div>
                            <!-- End Image Post-->

                            <div class="row gridalicious" id="content-gridalicious">
                                <div class="col-md-4 col-sm-4 col-xs-12 block-gridalicious">
                                    <h4>Caracteristicas <?=$offsetToStart = $this->session->userdata("offsetLastSearchMobile");?></h4>
                                    <ul class="unstyled" style="padding-left:15px;">
                                        <li><b>Tipo:</b> <?php echo ucwords(strtolower($inmueble_detail->categoria)) . " en " . ucwords(strtolower($inmueble_detail->forma)); ?></li>
                                        <li><b>Superficie Total:</b> <?php echo $inmueble_detail->superficie . " " . $inmueble_detail->tipo_superficie; ?></li>
                                        <?php foreach($caracteristicas as $car): ?>
                                            <?php if(!empty($car->valor)): ?>
                                                <?php if($car->caracteristica == "superficie"){ ?>
                                                    <?php if($car->valor != "0"): ?>
                                                        <li><?php echo "<b>".ucwords($car->caracteristica)." Construida:</b> " . $car->valor . " Metros Cuadrados"; ?></li>
                                                    <?php endif; ?>
                                                <?php }else{ ?>
                                                    <li><?php echo "<b>".ucwords($car->caracteristica).":</b> " . ucwords(strtolower($car->valor)); ?></li>
                                                <?php } ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>

                                    </ul>
                                    <div class="block-buy-house">
                                    <?php if(false):  //if($inmueble_detail->forma == "VENTA"): ?>
                                        <h4>¿Deseas comprar este inmueble?</h4>
                                        <div>
                                            <form id="form_2" class="form_calculator" action="<?php echo base_url();?>ajax/calculator">
                                                <!--<div class="row">
                                                    <div class="col-md-6">
                                                        Salario:
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input type="text" placeholder="Ejemplo: $800"  name="ingresos" required style="width: 100%; margin-bottom: 10px;">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        Años plazo:
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input type="number" name="plazo" required style="width: 100%; margin-bottom: 10px;">
                                                    </div>
                                                </div>-->
                                                <div class="row">
                                                    <div class="col-md-6">
                                                       Entidad Financiera:
                                                    </div>
                                                    <div class="col-md-6" style="text-align: right;">
                                                        <img src="<?=assets_url("img/sponsors/logo-facil.png")?>"  />
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                    <input type="hidden" name="precio" value="<?php echo $inmueble_detail->precio; ?>" />
                                                    <input type="submit" value="Calcular Interes" class="button button-calculator">
                                                    </div>
                                                </div>
                                            </form>
                                            <div id="result_calculator"></div>
                                        </div>
                                    <?php endif; ?>
                                    </div>                                    
                                </div>
                                <div class="col-md-8 col-sm-8 col-xs-12 block-gridalicious" style="min-height:140px;">
                                    <h4>Descripcion</h4>
                                    <p><?php echo  nl2br(trim($inmueble_detail->detalle)); ?></p>
                                </div>
                            <!--</div>

                            <div class="row">-->
                                <?php if(!empty($planos)): ?>
                                <div class="col-md-4 col-sm-4 col-xs-4 col-mobile-1 block-gridalicious"></div>
                                <div class="col-md-8 col-sm-8 col-xs-8 col-mobile-1 block-gridalicious">
                                    <h4>Planos</h4>
                                    <div class="row">
                                        <?php foreach($planos as $obj_plano): ?>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <p>
                                                    <a href="<?php echo base_url() . 'admin/imagenes/planos/' . $obj_plano->picture; ?>" class="planos_zoom" rel="group1"><img src="<?php echo base_url() . 'admin/imagenes/planos/' . $obj_plano->picture; ?>" style="width: 100%;" /></a>
                                                </p>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                                <?php endif; ?>
                            <!--</div>

                            <div class="row">-->
                                <?php if(!empty($inmueble_detail->latitud) && !empty($inmueble_detail->longitud)): ?>
                                <div class="col-md-12 col-sm-12 col-xs-12 col-mobile-1 map_static block-gridalicious">
                                    <h4>Mapa de Ubicación</h4>
                                    <div class="map-fancy-framework">
                                        <div id="maps" style="height: 300px;width: auto">
                                        </div>
                                        <input name="latitude" value="<?=$inmueble_detail->latitud?>" type="hidden" required>
                                        <input name="longitude" value="<?=$inmueble_detail->longitud?>" type="hidden" required data-parsley-errors-container="#map-location-error-message-parsley" data-parsley-error-message="Debe marcar un punto en el mapa">
                                    </div>
                                </div>
                                <?php endif; ?>
                                <?php if(!empty($inmueble_detail->video)): ?>
                                <div class="col-md-4 col-sm-4 col-xs-4 col-mobile-1 block-gridalicious"></div>
                                <div class="col-md-8 col-sm-8 col-xs-12 col-mobile-1 block-gridalicious">
                                    <h4>Video</h4>
                                    <div class="video-container">
                                        <?php $video = str_replace("&", "", $inmueble_detail->video); ?>
                                        <p><iframe width="560" height="315" src="http://www.youtube.com/embed/<?php echo $video; ?>" frameborder="0" allowfullscreen></iframe></p>
                                    </div>
                                </div>
                                <?php endif; ?>
                            <!--</div>-->
                            </div>

                    <!-- No se encontro inmueble -->
                    <?php
                    }
                    else
                    {
                    ?>
                        <div class="row main-content">
                            <div class="col-md-12">
                                <div class="alert alert-warning" style="text-transform: uppercase;">
                                    <?php
                                    $url_seo = str_replace("-", " ", $this->uri->segment(3));
                                    $url_seo = str_replace(".html", "", $url_seo);
                                    ?>
                                    El inmueble <strong><?php echo $url_seo; ?></strong> ya no esta disponible
                                </div>
                                <?php $relacionadas_aux = $relacionadas["results"]; ?>
                                <?php
                                if(!empty($relacionadas_aux))
                                {
                                ?>
                                    <div class="row ">
                                        <div class="col-md-12">
                                            <h4>A continuación te mostramos inmuebles que consideramos pueden ser de tu interés.</h4>
                                            <div class="row" style="padding-top: 15px;">
                                                <?php
                                                foreach($relacionadas_aux as $publication)
                                                {
                                                ?>
                                                    <!-- Item Property-->
                                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                                                        <div class="item_property animateIn" data-animate="flipInX" style="border-bottom: 0; position:relative;">
                                                            <div class="head_property">
                                                                <?php
                                                                $url_detalle = base_url("buscar/".$primaryFilterArrayData["primaryFilter"]."/".seo_url($publication->inm_seo));
                                                                ?>
                                                                 <a href="<?=$url_detalle;?>" target="_blank">
                                                                    <?php
                                                                    $fileHandler = new File_Handler($publication->fot_archivo);
                                                                    $thumbnail = $fileHandler->getThumbnail(271,183,$publication->inm_nombre);
                                                                    ?>
                                                                    <img src="<?=$thumbnail->getSource()?>" alt="<?=$thumbnail->getAlternateText()?>" title="<?=$thumbnail->getTitle()?>">

                                                                     <h5><?php echo $publication->ciu_nombre; ?></h5>
                                                                </a>

                                                                <div class="av-portfolio-overlay">
                                                                    <a href="<?php echo $url_detalle; ?>" target="_blank"><i class="av-icon fa fa-search"></i> Ver Detalle</a>
                                                                    <a href="javascript:;" class="btn_favorite" rel="<?php echo $publication->inm_id; ?>"><i class="av-icon fa fa-star"></i> Agregar a Favoritos</a>
                                                                </div>

                                                                <?php
                                                                if($publication->usu_tipo != "Particular" && !empty($publication->usu_foto))
                                                                {
                                                                ?>
                                                                    <div class="av-portfolio-info">
                                                                        <?php $empresa = !empty($publication->usu_empresa) ? $publication->usu_empresa : $publication->usu_nombre; ?>
                                                                        <a href="<?php echo base_url() . 'perfil/'.$publication->usu_id.'/'.urlencode($empresa); ?>">
                                                                            <?php
                                                                            $fileHandler = new File_Handler($publication->usu_foto);
                                                                            $thumbnail = $fileHandler->getThumbnail(271,183,$publication->usu_nombre);
                                                                            if($thumbnail->fileExist())
                                                                            {
                                                                                echo  "<img src='".$thumbnail->getSource()."' alt='".$thumbnail->getAlternateText()."' title='".$thumbnail->getTitle()."'";
                                                                            }
                                                                            else
                                                                            {
                                                                                echo $empresa;
                                                                            }
                                                                            ?>
                                                                        </a>
                                                                    </div>
                                                                <?php
                                                                }
                                                                ?>

                                                            </div>

                                                            <div class="info_property" style="padding-bottom: 0;">
                                                                <ul>
                                                                    <li style="line-height: 18px; padding-bottom: 10px; height: 8em">
                                                                        <div style="font-weight: bold; margin-bottom:8px;"><?php echo $publication->cat_nombre . " EN ". $publication->for_descripcion; ?></div>
                                                                        <span style="float:none;"><?php echo crop_string($publication->inm_nombre, 60); ?></span>
                                                                    </li>
                                                                    <li>
                                                                        <div class="line_separator"></div>
                                                                        <strong>Precio</strong><span><?php echo $publication->mon_abreviado . " "; ?><?php echo number_format($publication->inm_precio,0,",",".") ?></span>
                                                                    </li>
                                                                </ul>
                                                                <?php
                                                                if($publication->usu_certificado == "Si")
                                                                {
                                                                ?>
                                                                    <div class="icon_certificado">
                                                                        <img src="<?=base_url("assets/img/icon_agente_certificados.png")?>"/>
                                                                    </div>
                                                                <?php
                                                                }
                                                                ?>
                                                                <div class="footer_property">
                                                                    <div class="icon m2" title="Metros Cuadrados"><i class="fa" style="margin-right: 2px;"><img src="<?=assets_url("img/icons/metros_cuadrados.png")?>" /></i> <?php echo empty($publication->inm_superficie) ? 0 : $publication->inm_superficie; ?>m2</div>
                                                                    <?php
                                                                    foreach($publication->features as $feature)
                                                                    {
                                                                    ?>
                                                                        <?php
                                                                        if($feature["eca_car_id"] == "4" && !empty($feature["eca_valor"]) && is_numeric($feature["eca_valor"]))
                                                                        {
                                                                        ?>
                                                                            <div class="icon bedroom" title="Dormitorios"><i class="fa"><img src="<?=base_url("assets/img/icons/dormitorios.png")?>"/></i> <?=$feature["eca_valor"]; ?></div>
                                                                        <?php
                                                                        }
                                                                        if($feature["eca_car_id"] == "5" && !empty($feature["eca_valor"]) && is_numeric($feature["eca_valor"]))
                                                                        {
                                                                        ?>
                                                                            <div class="icon bathroom" title="Baños"><i class="fa"><img src="<?=base_url("assets/img/icons/banos.png")?>"/></i> <?=$feature["eca_valor"]; ?></div>
                                                                        <?php
                                                                        }
                                                                        if($feature["eca_car_id"] == "9" && !empty($feature["eca_valor"]) && is_numeric($feature["eca_valor"]))
                                                                        {
                                                                        ?>
                                                                            <div class="icon parking" title="Parqueo"><i class="fa"><img src="<?=base_url("assets/img/icons/parqueo.png")?>"/></i> <?=$feature["eca_valor"]; ?></div>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                    <?php
                                                                    }
                                                                    ?>
                                                                    <br style="clear:both" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Item Property-->
                                                <?php
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php
                                }
                                ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <!-- End Post-->
            </div>
            <!-- End Blog Post-->
            <!-- Sidebars-->
            <div class="col-md-4 col-lg-3">
                <!-- Search Advance -->
                <?php
                $hide = "hide";
                if(!empty($inmueble) && $sw_fecha && $inmueble->pub_estado == "Aprobado")
                {
                    $hide = "";
                }
                ?>
                <aside class="contact-form <?=$hide?>">
                    <div class="search_advance" style="background-color: #f4f4f4; padding: 15px 10px 5px 10px; border-radius: 4px;">
                        <!-- Datos del Venderos -->
                        <div style="clear: both; border-bottom: 1px solid #cdcdcd;" >
                            <div class="row item_agent" style="margin-bottom: 0;">
                                <?php
                                if($inmueble_detail->usu_tipo == "Particular"){
                                    $nombre = $inmueble_detail->usu_nombre . " " . $inmueble_detail->usu_apellido;
                                    $nombre = trim($nombre);
                                    $imagen = '<img src="'.assets_url("img/team/1.jpg").'" alt="No Image">';
                                    $url_perfil = "";
                                }else{
                                    if(!empty($inmueble_detail->usu_empresa)){
                                        $nombre = $inmueble_detail->usu_empresa;
                                        $nombre = trim($nombre);
                                        $nombre = trim($nombre);
                                        $url_perfil = base_url() . 'perfil/'.$inmueble_detail->usu_id.'/'.seo_url($nombre);
                                    }else{
                                        $nombre = $inmueble_detail->usu_nombre;
                                        $nombre = trim($nombre);
                                        $url_perfil = base_url() . 'perfil/'.$inmueble_detail->usu_id.'/'.seo_url($nombre);
                                    }
                                }
                                ?>
                                <div class="col-md-12 info_agent">
                                    <?php if($inmueble->usu_certificado == "Si"): ?>
                                    <div style="float: left; margin-right: 5px;">
                                        <img src="<?=assets_url("img/icon_agente_certificados.png")?>" style="margin-right:3px; width: 30px;" alt="Este Inmueble pertenece a un Agente Certificado" title="Este Inmueble pertenece a un Agente Certificado"  />
                                    </div>
                                    <?php endif; ?>
                                    <h5 style="padding-top:0; float: left;"><?php echo $nombre; ?></h5>
                                    <br style="clear: both;" />
                                    <ul>
                                        <?php if(!empty($inmueble_detail->usu_email) && $user_logged->usu_tipo == "Admin"): ?>
                                            <li style="font-size: 14px;"><i class="fas fa-envelope"></i> <a href="mailto:<?php echo $inmueble_detail->usu_email; ?>"><?php echo $inmueble_detail->usu_email; ?></a></li>
                                        <?php endif; ?>
                                        <?php if(!empty($inmueble_detail->usu_telefono)): ?>
                                            <li style="font-size: 14px;"><i class="fas fa-phone"></i> <a class="no_link" href="tel:<?php echo $inmueble_detail->usu_telefono; ?>"><?php echo $inmueble_detail->usu_telefono; ?></a></li>
                                        <?php endif; ?>
                                        <?php if(!empty($inmueble_detail->usu_celular)): ?>
                                            <li style="font-size: 14px;"><i class="fas fa-mobile"></i> <a class="no_link" href="tel:<?php echo $inmueble_detail->usu_celular; ?>"><?php echo $inmueble_detail->usu_celular; ?></a></li>
                                        <?php endif; ?>
                                    </ul>
                                </div>
                            </div>
                            <p class="description_agency"><?php echo !empty($inmueble_detail->usu_descripcion) ? crop_string2($inmueble_detail->usu_descripcion, 240) : ''; ?></p>
                            <?php if(!empty($url_perfil)): ?>
                                <p><a href="<?php echo $url_perfil; ?>" style="font-size: 14px; text-decoration: underline;" target="_blank">Ver Perfil</a></p>
                            <?php endif; ?>
                        </div>
                        <!-- Fin Datos del Venderor -->

                        <div id="box_contact_agent" style="margin-top: 10px;">
                            <a name="Formulario"></a>
                            <h2>Contacto</h2>
                            <form id="form" class="form_contact" action="#" data-parsley-validate>
                                <div class="row">
                                    <?php
                                    if(!$user_logged instanceof stdClass)
                                    {
                                        ?>
                                        <div class="col-md-12">
                                            <input type="text" placeholder="Nombre" name="nombre" required
                                                   style="margin-bottom: 7px;">
                                        </div>
                                        <div class="col-md-12">
                                            <input type="text" placeholder="Telefono" name="telefono" required data-parsley-type="number"
                                                   style="margin-bottom: 7px;">
                                        </div>
                                        <div class="col-md-12">
                                            <input type="email" placeholder="Correo Electronico" name="email" required
                                                   style="margin-bottom: 7px;">
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                                <textarea placeholder="Tu comentario..." name="mensaje" required></textarea>
                                <div id="content-send-to-whatsapp"><input type="checkbox" name="send-to-whatsapp" id="send-to-whatsapp"> Enviar tambien por whatsapp</div>
                                <div id="send-whatsapp" class="hide">
                                    <a href="#">send</a>
                                </div>
                                <input type="hidden" name="empresa" value="" />
                                <input type="hidden" name="id_inmueble" value="<?php echo $inmueble_detail->id; ?>" />
                                <?php
                                $url_detalle = base_url("buscar/".$primaryFilterArrayData["primaryFilter"]."/".seo_url($inmueble_detail->seo));

                                if($inmueble_detail->categoria == "CASAS" && $inmueble_detail->forma == "VENTA" && $inmueble_detail->dep_nombre == "Santa Cruz"){
                                    $onclick = 'onClick="goog_report_conversion(\''.$url_detalle.'/thank-you-page'.'\')"';
                                }
                                ?>
                                <input type="hidden" value="<?=$reCaptchaKeys["publicKey"]?>" name="site-key">
                                <div id='form-contact-recaptcha'></div>

                                <?php
                                if($user_logged->usu_tipo == "Admin") {
                                    ?>
                                    <a href="#" id="send-whatsapp">test</a>
                                    <?php
                                }
                                ?>
                                <input type="button" id="form-contact-button" name="Submit" value="Enviar" <?php echo $onclick; ?> class="button">

                            </form>
                            <div class="result"></div>
                        </div>
                    </div>
                </aside>
                <!-- End Search Advance -->
                <!-- Accordion -->
                <!-- End Accordion -->
            </div>
            <?php if(!empty($similar_properties)): ?>
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="similar-publishment">Publicaciones similares</h4>
                        <div class="row" style="padding-top: 15px;">
                            <?php
                            foreach($similar_properties as $publication)
                            {
                                ?>
                                <!-- Item Property-->
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                                    <div class="item_property animateIn" data-animate="flipInX" style="border-bottom: 0; position:relative;">
                                        <div class="head_property">
                                            <?php
                                            $propertyType = seo_url($publication->cat_nombre);
                                            $transactionType = seo_url($publication->for_descripcion);
                                            $state = seo_url($publication->dep_nombre);
                                            $primaryFilter = $propertyType."-en-".$transactionType."-en-".$state;

                                            $url_detalle = base_url("buscar/".$primaryFilter."/".$publication->inm_seo);
                                            ?>
                                            <a href="<?php echo $url_detalle; ?>" target="_blank">
                                                <?php
                                                $fileHandler = new File_Handler($publication->fot_archivo);
                                                $thumbnail = $fileHandler->getThumbnail(271,183,$publication->inm_nombre);
                                                ?>
                                                <img src="<?=$thumbnail->getSource()?>" alt="<?=$thumbnail->getAlternateText()?>" title="<?=$thumbnail->getTitle()?>">

                                                <h5><?php echo $publication->ciu_nombre; ?></h5>
                                            </a>

                                            <div class="av-portfolio-overlay">
                                                <a href="<?php echo $url_detalle; ?>" target="_blank"><i class="av-icon fa fa-search"></i> Ver Detalle</a>
                                                <a href="javascript:;" class="btn_favorite" rel="<?php echo $publication->inm_id; ?>"><i class="av-icon fa fa-star"></i> Agregar a Favoritos</a>
                                            </div>

                                            <?php if($publication->usu_tipo != "Particular" && !empty($publication->usu_foto)): ?>
                                                <div class="av-portfolio-info">
                                                    <?php $empresa = !empty($publication->usu_empresa) ? $publication->usu_empresa : $publication->usu_nombre; ?>
                                                    <a href="<?php echo base_url() . 'perfil/'.$publication->usu_id.'/'.urlencode($empresa); ?>" target="_blank">
                                                        <?php if(!empty($publication->usu_foto)){ ?>
                                                            <img src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo base_url(); ?>assets/uploads/users/<?php echo strtolower($publication->usu_foto); ?>&h=36" alt="<?php echo $publication->usu_nombre; ?>" title="<?php echo $publication->usu_nombre; ?>" />
                                                        <?php }else{ ?>
                                                            <?php echo $empresa; ?>
                                                        <?php } ?>
                                                    </a>
                                                </div>
                                            <?php endif; ?>

                                        </div>

                                        <div class="info_property" style="padding-bottom: 0;">
                                            <ul>
                                                <li style="line-height: 18px; padding-bottom: 10px; height: 8em">
                                                    <div style="font-weight: bold; margin-bottom:8px;"><?php echo $publication->cat_nombre . " EN ". $publication->for_descripcion; ?></div>
                                                    <span style="float:none;"><?php echo crop_string($publication->inm_direccion, 60); ?></span>
                                                </li>
                                                <li>
                                                    <div class="line_separator"></div>
                                                    <strong>Precio</strong><span><?php echo $publication->mon_abreviado . " "; ?><?php echo number_format($publication->inm_precio,0,",",".") ?></span>
                                                </li>
                                            </ul>
                                            <?php if($publication->usu_certificado == "Si"): ?>
                                                <div class="icon_certificado">
                                                    <img src="<?=assets_url("img/icon_agente_certificados.png")?>" alt="Agente Certificado" class="masterTooltip"/>
                                                    <div class="tet-tooltip">Este Inmueble pertenece a un Agente Certificado</div>
                                                </div>
                                            <?php endif; ?>
                                            <div class="footer_property">
                                                <div class="icon m2" title="Metros Cuadrados"><img src="<?=assets_url("img/icons/metros_cuadrados.png")?>" /> <?php echo empty($publication->inm_superficie) ? 0 : $publication->inm_superficie; ?>m2</div>
                                                <?php foreach($publication->features as $feature): ?>
                                                    <?php if($feature["eca_car_id"] == "4" && !empty($feature["eca_valor"]) && is_numeric($feature["eca_valor"])): ?>
                                                        <div class="icon bedroom" title="Dormitorios"><img src="<?=assets_url("img/icons/dormitorios.png")?>" /> <?php echo $feature["eca_valor"]; ?></div>
                                                    <?php  endif; ?>

                                                    <?php if($feature["eca_car_id"] == "5" && !empty($feature["eca_valor"]) && is_numeric($feature["eca_valor"])): ?>
                                                        <div class="icon bathroom" title="Baños"><img src="<?=assets_url("img/icons/banos.png")?>" /> <?php echo $feature["eca_valor"]; ?></div>
                                                    <?php endif; ?>

                                                    <?php if($feature["eca_car_id"] == "9" && !empty($feature["eca_valor"]) && is_numeric($feature["eca_valor"])): ?>
                                                        <div class="icon parking" title="Parqueo"><img src="<?=assets_url("img/icons/parqueo.png")?>" /> <?php echo $feature["eca_valor"]; ?></div>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                                <br style="clear:both" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Item Property-->
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <!-- Sidebars-->
        </div>
    </div>
</section>
<?php
$this->load->view("handlebar-template/quick-login-form");
$this->load->view('handlebar-template/welcome-to-tet-login-facebook');
?>
<!-- End content info-->
