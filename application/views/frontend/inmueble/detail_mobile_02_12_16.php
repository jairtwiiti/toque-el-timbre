<?php
$path_assets = base_url() . "assets/";
$caracteristicas = $inmueble->caracteristicas;
$fotos = $inmueble->fotos;
$planos = $inmueble->planos;
$similar_properties = $inmueble->similar["results"];
$pubishied_proyects = $inmueble->proyectos_publicados;

$inmueble_detail = $inmueble;
?>

<!-- End content info -->
<section class="content_info content_info_search" style="padding: 0 !important; border-top: 5px solid #00aeef;">
    <div class="container">

        <div class="row padding_top">
            <!-- Blog Post-->
            <div id="col-detail" class="">
                <!-- Post-->
                <div class="post single">

                    <?php if(!empty($inmueble)){ ?>
                            <div class="">
                                <div class="" style="padding-left: 15px; padding-right: 15px;">
                                    <h2><?php echo $inmueble_detail->nombre; ?></h2>
                                    <p <?php echo $inmueble->usu_certificado == "Si" ? 'style="margin-bottom: 6px;"' : ''; ?>><?php echo !empty($inmueble_detail->direccion) ? $inmueble_detail->direccion .", ".$inmueble_detail->ciudad : $inmueble_detail->ciudad; ?></p>
                                    <?php if($inmueble_detail->usu_certificado == "Si"): ?>
                                        <p>
                                            <img src="<?php echo $path_assets; ?>img/icon_agente_certificados.png" style="margin-right:3px;" alt="Este Inmueble pertenece a un Agente Certificado" title="Este Inmueble pertenece a un Agente Certificado"  /> <span><i>Este Inmueble pertenece a un Agente Certificado</i></span>
                                        </p>
                                    <?php endif; ?>
                                </div>
                                <!--<div class="col-md-2">
                                    <div class="favorito">
                                        <a href="javascript:void(0);" class="btn_favorite <?php echo $favorite ? 'check_favorite' : ''; ?>" rel="<?php echo $inmueble_detail->id; ?>">
                                            <i class="fa fa-star"></i> <span>Agregar a favoritos</span>
                                        </a>
                                    </div>
                                </div>-->
                            </div>

                            <!-- Image Post-->
                            <div class="image_post">
                                <div class="image_detail">
                                    <!-- Slide News-->

                                    <div class="swiper-container">
                                        <div class="swiper-wrapper">
                                            <?php foreach($fotos as $img): ?>
                                                <div class="swiper-slide">
                                                    <a href="javascript:void(0);" rel="<?php echo $publication->inm_id; ?>">
                                                        <?php
                                                        $imageX = $img->fot_archivo;
                                                        $fileimage = BASE_DIRECTORY . DS . "admin" . DS . "imagenes" . DS . "inmueble" . DS . $imageX;
                                                        ?>
                                                        <?php if(!empty($imageX) && !is_null($imageX) && file_exists($fileimage)){ ?>
                                                            <img src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo base_url(); ?>admin/imagenes/inmueble/<?php echo ($img->fot_archivo); ?>&h=600" >
                                                        <?php }else{ ?>
                                                            <img src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo $path_assets.'img/default.png'?>&h=600" alt="default" title="Toqueeltimbre.com">
                                                        <?php } ?>

                                                    </a>
                                                </div>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>


                                    <!-- End Slide-->
                                    <div class="price">
                                        <h2 style="color: #fff; padding: 0;">Precio: <?php echo number_format($inmueble_detail->precio,0,",",".") . " " . $inmueble_detail->moneda; ?></h2>
                                    </div>
                                </div>
                                <div class="icons_detail">
                                    <?php $tipo_sup = $inmueble_detail->tipo_superficie == "Metros Cuadrados" ? "m2" : "ha"; ?>
                                    <ul>
                                        <li><i class="fa"><img src="<?php echo $path_assets; ?>img/icons/metros_cuadrados.png" /></i> <?php echo empty($inmueble_detail->superficie) ? 0 : $inmueble_detail->superficie; ?><?php echo $tipo_sup; ?></li>
                                        <?php foreach($caracteristicas as $feature): ?>
                                            <?php if($feature->caracteristica == "habitaciones" && !empty($feature->valor) && is_numeric($feature->valor)): ?>
                                                <li><i class="fa"><img src="<?php echo $path_assets; ?>img/icons/dormitorios.png" /></i> <?php echo $feature->valor; ?></li>
                                            <?php  endif; ?>
                                            <?php if($feature->caracteristica == "banos" && !empty($feature->valor) && is_numeric($feature->valor)): ?>
                                                <li><i class="fa"><img src="<?php echo $path_assets; ?>img/icons/banos.png" /></i> <?php echo $feature->valor; ?></li>
                                            <?php endif; ?>
                                            <?php if($feature->caracteristica == "parqueo" && !empty($feature->valor) && is_numeric($feature->valor)): ?>
                                                <li><i class="fa"><img src="<?php echo $path_assets; ?>img/icons/parqueo.png" /></i> <?php echo $feature->valor; ?></li>
                                            <?php endif; ?>
                                        <?php endforeach; ?>

                                    </ul>
                                </div>
                            </div>
                            <!-- End Image Post-->

                        <div class="gridalicious" id="content-gridalicious">
                            <div class="block-gridalicious" style="padding-left: 15px; padding-right: 15px;">
                                <h4>Caracteristicas</h4>
                                <ul class="unstyled" style="padding-left:15px;">
                                    <li><b>Tipo:</b> <?php echo ucwords(strtolower($inmueble_detail->categoria)) . " en " . ucwords(strtolower($inmueble_detail->forma)); ?></li>
                                    <li><b>Superficie Total:</b> <?php echo $inmueble_detail->superficie . " " . $inmueble_detail->tipo_superficie; ?></li>
                                    <?php foreach($caracteristicas as $car): ?>
                                        <?php if(!empty($car->valor)): ?>
                                            <?php if($car->caracteristica == "superficie"){ ?>
                                                <?php if($car->valor != "0"): ?>
                                                    <li><?php echo "<b>".ucwords($car->caracteristica)." Construida:</b> " . $car->valor . " Metros Cuadrados"; ?></li>
                                                <?php endif; ?>
                                            <?php }else{ ?>
                                                <li><?php echo "<b>".ucwords($car->caracteristica).":</b> " . ucwords(strtolower($car->valor)); ?></li>
                                            <?php } ?>
                                        <?php endif; ?>
                                    <?php endforeach; ?>

                                </ul>
                                <div class="block-buy-house">
                                <?php if(false):  //if($inmueble_detail->forma == "VENTA"): ?>
                                    <h4>¿Deseas comprar esta casa?</h4>
                                    <div>
                                        <form id="form" class="form_calculator" action="<?php echo base_url();?>ajax/calculator">
                                            <!--<div class="row">
                                                <div class="col-md-6">
                                                    Salario:
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" placeholder="Ejemplo: $800"  name="ingresos" required style="width: 100%; margin-bottom: 10px;">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    Años plazo:
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="number" name="plazo" required style="width: 100%; margin-bottom: 10px;">
                                                </div>
                                            </div>-->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    Entidad Financiera:
                                                </div>
                                                <div class="col-md-6" style="text-align: right;">
                                                    <img src="<?php echo $path_assets; ?>img/sponsors/logo-facil.png"  />
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <input type="hidden" name="precio" value="<?php echo $inmueble_detail->precio; ?>" />
                                                    <input type="submit" value="Calcular Interes" class="button button-calculator">
                                                </div>
                                            </div>
                                        </form>
                                        <div id="result_calculator"></div>
                                    </div>
                                <?php endif; ?>
                                </div>
                            </div>
                            <div class="block-gridalicious" style="min-height:140px; padding-left: 15px; padding-right: 15px;">
                                <h4>Descripcion</h4>
                                <p><?php echo  nl2br(trim($inmueble_detail->detalle)); ?></p>
                            </div>
                            <!--</div>

                            <div class="row">-->
                            <?php if(!empty($planos)): ?>
                                <div class="block-gridalicious"></div>
                                <div class="block-gridalicious">
                                    <h4>Planos</h4>
                                    <div class="row">
                                        <?php foreach($planos as $obj_plano): ?>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <p>
                                                    <a href="<?php echo base_url() . 'admin/imagenes/planos/' . $obj_plano->picture; ?>" class="planos_zoom" rel="group1"><img src="<?php echo base_url() . 'admin/imagenes/planos/' . $obj_plano->picture; ?>" style="width: 100%;" /></a>
                                                </p>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <!--</div>

                            <div class="row">-->
                            <?php if(!empty($inmueble_detail->video)): ?>
                                <div class="block-gridalicious"></div>
                                <div class="block-gridalicious">
                                    <h4>Video</h4>
                                    <div class="video-container">
                                        <?php $video = str_replace("&", "", $inmueble_detail->video); ?>
                                        <p><iframe width="560" height="315" src="http://www.youtube.com/embed/<?php echo $video; ?>" frameborder="0" allowfullscreen></iframe></p>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <!--</div>

                            <div class="row">-->
                            <!-- Datos del Venderos -->
                            <div style="clear: both; padding-left: 15px; padding-right: 15px;" class="">
                                <h4>Vendedor</h4>
                                <div class="row item_agent">
                                    <?php
                                    if($inmueble_detail->usu_tipo == "Particular"){
                                        $nombre = $inmueble_detail->usu_nombre . " " . $inmueble_detail->usu_apellido;
                                        $imagen = '<img src="'.$path_assets.'img/team/1.jpg" alt="No Image">';
                                        $url_perfil = "";
                                    }else{
                                        if(!empty($inmueble_detail->usu_empresa)){
                                            $nombre = $inmueble_detail->usu_empresa;
                                            $url_perfil = base_url() . 'perfil/'.$inmueble_detail->usu_id.'/'.urlencode($nombre);
                                        }else{
                                            $nombre = $inmueble_detail->usu_nombre;
                                            $url_perfil = base_url() . 'perfil/'.$inmueble_detail->usu_id.'/'.urlencode($nombre);
                                        }
                                        if(!empty($inmueble_detail->usu_foto)){
                                            $imagen = '<img src="'.base_url().'librerias/timthumb.php?src='.base_url().'assets/uploads/users/'.strtolower($inmueble_detail->usu_foto).'&w=250" alt="'.$nombre.'" title="'.$nombre.'">';
                                        }else{
                                            $imagen = '<img src="'.$path_assets.'img/team/1.jpg" alt="No Image">';
                                        }
                                    }
                                    ?>
                                    <div class="image_agent">
                                        <?php echo $imagen; ?>
                                    </div>
                                    <div class="info_agent">
                                        <h5 style="padding-top:0;"><?php echo $nombre; ?></h5>
                                        <ul>
                                            <?php if(!empty($inmueble_detail->usu_email)): ?>
                                                <li><i class="fa fa-envelope"></i><a href="mailto:<?php echo $inmueble_detail->usu_email; ?>"><?php echo $inmueble_detail->usu_email; ?></a></li>
                                            <?php endif; ?>
                                            <?php if(!empty($inmueble_detail->usu_telefono)): ?>
                                                <li><i class="fa fa-phone"></i><a class="no_link" href="tel:<?php echo $inmueble_detail->usu_telefono; ?>"><?php echo $inmueble_detail->usu_telefono; ?></a></li>
                                            <?php endif; ?>
                                            <?php if(!empty($inmueble_detail->usu_celular)): ?>
                                                <li><i class="fa fa-mobile-phone"></i><a class="no_link" href="tel:<?php echo $inmueble_detail->usu_celular; ?>"><?php echo $inmueble_detail->usu_celular; ?></a></li>
                                            <?php endif; ?>
                                        </ul>
                                    </div>
                                </div>
                                <p class="description_agency"><?php echo !empty($inmueble_detail->usu_descripcion) ? nl2br(crop_string($inmueble_detail->usu_descripcion, 240)) : ''; ?></p>
                                <?php if(!empty($url_perfil)): ?>
                                    <p><a href="<?php echo $url_perfil; ?>">Ver Perfil</a></p>
                                <?php endif; ?>
                            </div>
                            <!-- Fin Datos del Venderor -->
                            <div id="box_contact_agent" class="block-gridalicious" style="padding-left: 15px; padding-right: 15px;">
                                <a name="Formulario"></a>
                                <h4>Formulario de Contacto</h4>
                                <form id="form" class="form_contact" action="<?php echo base_url(); ?>inmueble/send_mail">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <input type="text" placeholder="Nombre" name="nombre" required>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="text" placeholder="Telefono" name="telefono" required>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="email" placeholder="Email"  name="email" required>
                                        </div>
                                    </div>
                                    <textarea placeholder="Tu comentario..." name="comentario" required></textarea>
                                    <input type="hidden" name="empresa" value="" />
                                    <input type="hidden" name="id_inmueble" value="<?php echo $inmueble_detail->id; ?>" />
                                    <input type="submit" name="Submit" value="Enviar" class="button">
                                </form>
                                <div class="result"></div>
                            </div>

                            <?php if(!empty($inmueble_detail->latitud) && !empty($inmueble_detail->longitud)): ?>
                            <div class="block-gridalicious">
                                <a name="Mapa"></a>
                                <img src="https://maps.googleapis.com/maps/api/staticmap?center=<?php echo $inmueble_detail->latitud; ?>,<?php echo $inmueble_detail->longitud; ?>&zoom=13&size=500x500&maptype=roadmap&markers=color:red|label:S|<?php echo $inmueble_detail->latitud; ?>,<?php echo $inmueble_detail->longitud; ?>&key=AIzaSyDfRh4993szk5PnBIFHTQHGdGCrpgyqPhQ" />
                            </div>
                            <?php endif; ?>
                        </div>

                        <!-- No se encontro inmueble -->
                    <?php }else{ ?>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="alert alert-warning" style="text-transform: uppercase;">
                                    <?php $url_seo = str_replace("-", " ", $this->uri->segment(3)); ?>
                                    <?php $url_seo = str_replace(".html", "", $url_seo); ?>
                                    El inmueble <strong><?php echo $url_seo; ?></strong> ya no esta disponible
                                </div>
                                <p>A continuación te mostramos inmuebles que consideramos pueden ser de tu interés.</p>
                            </div>
                        </div>
                    <?php } ?>

                </div>
                <!-- End Post-->


            </div>
            <!-- End Blog Post-->

        <!-- Sidebars-->
        <div class="col-md-4 col-lg-3">

            <!-- Accordion -->
            <aside>
                <h2>Proyectos</h2>
                <?php foreach($projects as $project): ?>
                    <div class="project_item">
                        <div class="row">
                            <div class="col-md-12">
                                <a href="<?php echo base_url(). $project->proy_seo; ?>">
                                    <img src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo base_url(); ?>admin/imagenes/proyecto/<?php echo $project->proy_fot_imagen; ?>&w=500&h=281" alt="<?php echo $project->inm_nombre; ?>" title="<?php echo $project->inm_nombre; ?>">
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h4><?php echo $project->inm_nombre; ?></h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <small><?php echo $project->ciu_nombre; ?></small>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <small><strong><?php echo ucwords(strtolower($project->for_descripcion)); ?></strong></small>
                            </div>
                            <div class="col-md-6 text_right">
                                <small><strong><?php echo $project->mon_abreviado; ?><?php echo number_format($project->inm_precio, 0, ",", "."); ?></strong></small>
                            </div>
                        </div>

                    </div>
                    <div class="divisor"></div>
                <?php endforeach; ?>

            </aside>
            <!-- End Accordion -->
        </div>
        <!-- Sidebars-->
        </div>
    </div>
</section>
<!-- End content info-->


<?php if(!empty($inmueble_detail)): ?>
<?php
$url_detalle = base_url();
$url_detalle .= seo_url($inmueble_detail->ciudad)."/";
$url_detalle .= seo_url($inmueble_detail->categoria . " EN ". $inmueble_detail->forma)."/";
$url_detalle .= seo_url($inmueble_detail->seo).".html";

if($inmueble_detail->categoria == "CASAS" && $inmueble_detail->forma == "VENTA" && $inmueble_detail->dep_nombre == "Santa Cruz"){
    $onclick = 'onClick="goog_report_conversion(\''.$url_detalle.'\')"';
}
?>
<div style="position:fixed; bottom:10px; left: 10px; z-index: 99999;">
    <a id="toMail" href="#Formulario"></a>
    <?php if(!empty($inmueble_detail->usu_telefono)): ?><a id="toPhone" <?php echo $onclick; ?> href="tel:<?php echo $inmueble_detail->usu_telefono; ?>"></a><?php endif; ?>
    <?php if(!empty($inmueble_detail->latitud) && !empty($inmueble_detail->longitud)): ?><a id="toMarker" <?php echo $onclick; ?> href="#Mapa"></a><?php endif; ?>
</div>
<?php endif; ?>


<style>
    /* Demo Styles */
    .swiper-container {
        width: 100%;
        height: 243px !important;
        color: #fff;
        text-align: center;
    }

    .swiper-pagination-switch {
        display: inline-block;
        width: 8px;
        height: 8px;
        border-radius: 8px;
        background: #555;
        margin-right: 5px;
        opacity: 0.8;
        border: 1px solid #fff;
        cursor: pointer;
    }
    .swiper-active-switch {
        background: #fff;
    }
    .swiper-dynamic-links {
        text-align: center;
    }
    .swiper-dynamic-links a {
        display: inline-block;
        padding: 5px;
        border-radius: 3px;
        border: 1px solid #ccc;
        margin: 5px;
        font-size: 12px;
        text-decoration: none;
        color: #333;
        background: #eee;
    }
    .swiper-slide{
        width: 100%;
    }
</style>

<script>

    $('.swiper-container').each(function () {
        var mySwiper = new Swiper(this,{
            paginationClickable: false
        })
    });

    /*$('#slide_details').camera({
        height: '55.5%',
        navigation: true
    });

    $(".iframe_mapa").click(function() {
        $(this).attr("target", "_blank");
    });*/

    //=================================== Add favorite =================================//

    $(".btn_favorite").click(function() {
        var $btn = $(this);
        var inm_id = $(this).attr("rel");
        $.ajax({
            type: 'POST',
            url: "<?php echo base_url(); ?>" + "inmueble/favorito",
            data: { id_inmueble: inm_id },
            beforeSend: function() {
                // loading
            },
            success: function(data) {
                if (data == "true") {
                    $btn.addClass("check_favorite");
                }else{
                    $btn.removeClass("check_favorite");
                }
            }
        });
    });

    $(".form_contact").submit(function(e){
        var postData = $(this).serializeArray();
        var formURL = $(this).attr("action");
        $.ajax(
            {
                url : formURL,
                type: "POST",
                data : postData,
                beforeSend: function() {
                    $("#box_contact_agent .result").html('<img src="<?php echo base_url(); ?>assets/img/loading.gif" />');
                },
                success:function(data, textStatus, jqXHR)
                {
                    $("#box_contact_agent .result").html(data);
                }
            });
        e.preventDefault(); //STOP default action
    });

    $('.form_calculator').submit(function(event) {
        event.preventDefault();
        var url = $(this).attr('action');
        var datos = $(this).serialize();
        $.post(url, datos, function(resultado) {
            $('#result_calculator').html(resultado);
        });
    });
</script>


