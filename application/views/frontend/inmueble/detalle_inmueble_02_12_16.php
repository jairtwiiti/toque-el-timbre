<?php
$path_assets = base_url() . "assets/";
$caracteristicas = $inmueble->caracteristicas;
$fotos = $inmueble->fotos;
$planos = $inmueble->planos;
$similar_properties = $inmueble->similar["results"];
$pubishied_proyects = $inmueble->proyectos_publicados;
$inmueble_detail = $inmueble;
$now = date("Y-m-d");
$sw_fecha = $inmueble->pub_vig_fin >= $now ? true : false;
?>

<!-- End content info -->
<section class="content_info content_info_search" style="padding-top: 0 !important; border-top: 5px solid #00aeef;">
    <div class="container">

        <div class="row padding_top">
            <!-- Blog Post-->
            <div id="col-detail" class="col-md-8 col-lg-9">
                <!-- Post-->
                <div class="post single">

                    <?php if(!empty($inmueble) && $sw_fecha){ ?>
                            <div class="row">
                                <div class="col-md-10">
                                    <h2><?php echo $inmueble_detail->nombre; ?></h2>
                                    <p <?php echo $inmueble->usu_certificado == "Si" ? 'style="margin-bottom: 6px;"' : ''; ?>><?php echo !empty($inmueble_detail->direccion) ? $inmueble_detail->direccion .", ".$inmueble_detail->ciudad : $inmueble_detail->ciudad; ?></p>
                                    <?php if($inmueble->usu_certificado == "Si"): ?>
                                    <!--<p>
                                        <img src="<?php echo $path_assets; ?>img/icon_agente_certificados.png" style="margin-right:3px;" alt="Este Inmueble pertenece a un Agente Certificado" title="Este Inmueble pertenece a un Agente Certificado"  /> <span><i>Este Inmueble pertenece a un Agente Certificado</i></span>
                                    </p>-->
                                    <?php endif; ?>
                                </div>
                                <div class="col-md-2">
                                    <div class="favorito">
                                        <a href="javascript:void(0);" class="btn_favorite <?php echo $favorite ? 'check_favorite' : ''; ?>" rel="<?php echo $inmueble_detail->id; ?>">
                                            <i class="fa fa-star"></i> <span>Agregar a favoritos</span>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <!-- Image Post-->
                            <div class="image_post">
                                <div class="image_detail">
                                    <!-- Slide News-->
                                    <?php if($inmueble->usu_certificado == "Si"): ?>
                                        <div style="position: absolute; right: 10px; top: 10px; z-index: 9999;">
                                            <img src="<?php echo $path_assets; ?>img/icon_agente_certificados.png" style="margin-right:3px;" alt="Este Inmueble pertenece a un Agente Certificado" title="Este Inmueble pertenece a un Agente Certificado"  />
                                        </div>
                                    <?php endif; ?>

                                    <div class="camera_wrap" id="slide_details">
                                        <?php foreach($fotos as $img): ?>
                                            <!-- <div  data-src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo base_url(); ?>admin/imagenes/inmueble/<?php echo $img->fot_archivo; ?>&w=850&h=470"></div> -->

                                            <?php
                                            $imageX = $img->fot_archivo;
                                            //$imageX = strtolower($img->fot_archivo);
                                            $fileimage = BASE_DIRECTORY . DS . "admin" . DS . "imagenes" . DS . "inmueble" . DS . $imageX;

                                            if (!empty($imageX) && !is_null($imageX) && file_exists($fileimage)) { ?>
                                                <div data-src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo base_url(); ?>admin/imagenes/inmueble/<?php echo ($img->fot_archivo); ?>&w=850&h=470"></div>
                                            <?php } else { ?>
                                                <div data-src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo $path_assets.'img/default.png'?>&w=850&h=470"></div>
                                            <?php } ?>

                                        <?php endforeach; ?>
                                    </div>
                                    <!-- End Slide-->
                                    <div class="price">
                                        <h2 style="color: #fff; padding: 0;">Precio: <?php echo number_format($inmueble_detail->precio,0,",",".") . " " . $inmueble_detail->moneda; ?></h2>
                                    </div>
                                </div>
                                <div class="icons_detail">
                                    <?php $tipo_sup = $inmueble_detail->tipo_superficie == "Metros Cuadrados" ? "m2" : "ha"; ?>
                                    <ul>
                                        <li><i class="fa"><img src="<?php echo $path_assets; ?>img/icons/metros_cuadrados.png" /></i> <?php echo empty($inmueble_detail->superficie) ? 0 : $inmueble_detail->superficie; ?><?php echo $tipo_sup; ?></li>
                                        <?php foreach($caracteristicas as $feature): ?>
                                            <?php if($feature->caracteristica == "habitaciones" && !empty($feature->valor) && is_numeric($feature->valor)): ?>
                                                <li><i class="fa"><img src="<?php echo $path_assets; ?>img/icons/dormitorios.png" /></i> <?php echo $feature->valor; ?></li>
                                            <?php  endif; ?>
                                            <?php if($feature->caracteristica == "banos" && !empty($feature->valor) && is_numeric($feature->valor)): ?>
                                                <li><i class="fa"><img src="<?php echo $path_assets; ?>img/icons/banos.png" /></i> <?php echo $feature->valor; ?></li>
                                            <?php endif; ?>
                                            <?php if($feature->caracteristica == "parqueo" && !empty($feature->valor) && is_numeric($feature->valor)): ?>
                                                <li><i class="fa"><img src="<?php echo $path_assets; ?>img/icons/parqueo.png" /></i> <?php echo $feature->valor; ?></li>
                                            <?php endif; ?>
                                        <?php endforeach; ?>

                                    </ul>
                                </div>
                            </div>
                            <!-- End Image Post-->

                            <div class="row gridalicious" id="content-gridalicious">
                                <div class="col-md-4 col-sm-4 col-xs-12 block-gridalicious">
                                    <h4>Caracteristicas</h4>
                                    <ul class="unstyled" style="padding-left:15px;">
                                        <li><b>Tipo:</b> <?php echo ucwords(strtolower($inmueble_detail->categoria)) . " en " . ucwords(strtolower($inmueble_detail->forma)); ?></li>
                                        <li><b>Superficie Total:</b> <?php echo $inmueble_detail->superficie . " " . $inmueble_detail->tipo_superficie; ?></li>
                                        <?php foreach($caracteristicas as $car): ?>
                                            <?php if(!empty($car->valor)): ?>
                                                <?php if($car->caracteristica == "superficie"){ ?>
                                                    <?php if($car->valor != "0"): ?>
                                                        <li><?php echo "<b>".ucwords($car->caracteristica)." Construida:</b> " . $car->valor . " Metros Cuadrados"; ?></li>
                                                    <?php endif; ?>
                                                <?php }else{ ?>
                                                    <li><?php echo "<b>".ucwords($car->caracteristica).":</b> " . ucwords(strtolower($car->valor)); ?></li>
                                                <?php } ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>

                                    </ul>
                                    <div class="block-buy-house">
                                    <?php if(false):  //if($inmueble_detail->forma == "VENTA"): ?>
                                        <h4>¿Deseas comprar este inmueble?</h4>
                                        <div>
                                            <form id="form" class="form_calculator" action="<?php echo base_url();?>ajax/calculator">
                                                <!--<div class="row">
                                                    <div class="col-md-6">
                                                        Salario:
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input type="text" placeholder="Ejemplo: $800"  name="ingresos" required style="width: 100%; margin-bottom: 10px;">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        Años plazo:
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input type="number" name="plazo" required style="width: 100%; margin-bottom: 10px;">
                                                    </div>
                                                </div>-->
                                                <div class="row">
                                                    <div class="col-md-6">
                                                       Entidad Financiera:
                                                    </div>
                                                    <div class="col-md-6" style="text-align: right;">
                                                        <img src="<?php echo $path_assets; ?>img/sponsors/logo-facil.png"  />
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                    <input type="hidden" name="precio" value="<?php echo $inmueble_detail->precio; ?>" />
                                                    <input type="submit" value="Calcular Interes" class="button button-calculator">
                                                    </div>
                                                </div>
                                            </form>
                                            <div id="result_calculator"></div>
                                        </div>
                                    <?php endif; ?>
                                    </div>                                    
                                </div>
                                <div class="col-md-8 col-sm-8 col-xs-12 block-gridalicious" style="min-height:140px;">
                                    <h4>Descripcion</h4>
                                    <p><?php echo  nl2br(trim($inmueble_detail->detalle)); ?></p>
                                </div>
                            <!--</div>

                            <div class="row">-->
                                <?php if(!empty($planos)): ?>
                                <div class="col-md-4 col-sm-4 col-xs-4 col-mobile-1 block-gridalicious"></div>
                                <div class="col-md-8 col-sm-8 col-xs-8 col-mobile-1 block-gridalicious">
                                    <h4>Planos</h4>
                                    <div class="row">
                                        <?php foreach($planos as $obj_plano): ?>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <p>
                                                    <a href="<?php echo base_url() . 'admin/imagenes/planos/' . $obj_plano->picture; ?>" class="planos_zoom" rel="group1"><img src="<?php echo base_url() . 'admin/imagenes/planos/' . $obj_plano->picture; ?>" style="width: 100%;" /></a>
                                                </p>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                                <?php endif; ?>
                            <!--</div>

                            <div class="row">-->
                                <?php if(!empty($inmueble_detail->latitud) && !empty($inmueble_detail->longitud)): ?>
                                <div class="col-md-12 col-sm-12 col-xs-12 col-mobile-1 map_static block-gridalicious">
                                    <h4>Mapa de Ubicación</h4>

                                    <div class="map_area" style="height: 350px;">
                                        <iframe src="<?php echo base_url(); ?>inmueble/mapa?latitud=<?php echo $inmueble_detail->latitud; ?>&longitud=<?php echo $inmueble_detail->longitud; ?>"></iframe>
                                    </div>
                                </div>
                                <?php endif; ?>
                                <?php if(!empty($inmueble_detail->video)): ?>
                                <div class="col-md-4 col-sm-4 col-xs-4 col-mobile-1 block-gridalicious"></div>
                                <div class="col-md-8 col-sm-8 col-xs-12 col-mobile-1 block-gridalicious">
                                    <h4>Video</h4>
                                    <div class="video-container">
                                        <?php $video = str_replace("&", "", $inmueble_detail->video); ?>
                                        <p><iframe width="560" height="315" src="http://www.youtube.com/embed/<?php echo $video; ?>" frameborder="0" allowfullscreen></iframe></p>
                                    </div>
                                </div>
                                <?php endif; ?>
                            <!--</div>-->
                            </div>
                            <div class="row" style="background-color: #F4F4F4; margin-top: 15px;">
                                <!-- Datos del Venderos -->
                                 <div style="clear: both;" class="col-md-6 col-sm-6 col-xs-6 col-mobile-1">
                                        <h4>Vendedor</h4>
                                        <div class="row item_agent">
                                            <?php
                                                if($inmueble_detail->usu_tipo == "Particular"){
                                                    $nombre = $inmueble_detail->usu_nombre . " " . $inmueble_detail->usu_apellido;
                                                    $nombre = trim($nombre);
                                                    $imagen = '<img src="'.$path_assets.'img/team/1.jpg" alt="No Image">';
                                                    $url_perfil = "";
                                                }else{
                                                    if(!empty($inmueble_detail->usu_empresa)){
                                                        $nombre = $inmueble_detail->usu_empresa;
                                                        $nombre = trim($nombre);
                                                        $url_perfil = base_url() . 'perfil/'.$inmueble_detail->usu_id.'/'.urlencode($nombre);
                                                    }else{
                                                        $nombre = $inmueble_detail->usu_nombre;
                                                        $nombre = trim($nombre);
                                                        $url_perfil = base_url() . 'perfil/'.$inmueble_detail->usu_id.'/'.urlencode($nombre);
                                                    }
                                                    if(!empty($inmueble_detail->usu_foto)){
                                                        $imagen = '<img src="'.base_url().'librerias/timthumb.php?src='.base_url().'assets/uploads/users/'.strtolower($inmueble_detail->usu_foto).'&w=250" alt="'.$nombre.'" title="'.$nombre.'">';
                                                    }else{
                                                        $imagen = '<img src="'.$path_assets.'img/team/1.jpg" alt="No Image">';
                                                    }
                                                }
                                            ?>
                                            <div class="col-md-4 image_agent">
                                                <?php echo $imagen; ?>
                                            </div>
                                            <div class="col-md-8 info_agent">
                                                <h5 style="padding-top:0;"><?php echo $nombre; ?></h5>
                                                <ul>
                                                    <?php if(!empty($inmueble_detail->usu_email)): ?>
                                                    <li><i class="fa fa-envelope"></i><a href="mailto:<?php echo $inmueble_detail->usu_email; ?>"><?php echo $inmueble_detail->usu_email; ?></a></li>
                                                    <?php endif; ?>
                                                    <?php if(!empty($inmueble_detail->usu_telefono)): ?>
                                                    <li><i class="fa fa-phone"></i><a class="no_link" href="tel:<?php echo $inmueble_detail->usu_telefono; ?>"><?php echo $inmueble_detail->usu_telefono; ?></a></li>
                                                    <?php endif; ?>
                                                    <?php if(!empty($inmueble_detail->usu_celular)): ?>
                                                    <li><i class="fa fa-mobile-phone"></i><a class="no_link" href="tel:<?php echo $inmueble_detail->usu_celular; ?>"><?php echo $inmueble_detail->usu_celular; ?></a></li>
                                                    <?php endif; ?>
                                                </ul>
                                            </div>
                                        </div>
                                        <p class="description_agency"><?php echo !empty($inmueble_detail->usu_descripcion) ? crop_string2($inmueble_detail->usu_descripcion, 240) : ''; ?></p>
                                        <?php if(!empty($url_perfil)): ?>
                                        <p><a href="<?php echo $url_perfil; ?>">Ver Perfil</a></p>
                                        <?php endif; ?>
                                    </div>
                                    <!-- Fin Datos del Venderor -->

                                    <div id="box_contact_agent" class="col-md-6 col-sm-6 col-xs-6 col-mobile-1 block-gridalicious">
                                        <a name="Formulario"></a>
                                        <h4>Formulario de Contacto</h4>
                                        <form id="form" class="form_contact" action="#">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <input type="text" placeholder="Nombre" name="nombre" required>
                                                </div>
                                                <!--<div class="col-md-6">
                                                    <input type="text" placeholder="Empresa"  name="empresa" >
                                                </div>-->
                                                <div class="col-md-6">
                                                    <input type="text" placeholder="Telefono" name="telefono" required>
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="email" placeholder="Correo Electronico"  name="email" required>
                                                </div>
                                            </div>
                                            <textarea placeholder="Tu comentario..." name="comentario" required></textarea>
                                            <input type="hidden" name="empresa" value="" />
                                            <input type="hidden" name="id_inmueble" value="<?php echo $inmueble_detail->id; ?>" />
                                            <?php
                                            $url_detalle = base_url();
                                            $url_detalle .= seo_url($inmueble_detail->ciudad)."/";
                                            $url_detalle .= seo_url($inmueble_detail->categoria . " EN ". $inmueble_detail->forma)."/";
                                            $url_detalle .= seo_url($inmueble_detail->seo).".html";

                                            if($inmueble_detail->categoria == "CASAS" && $inmueble_detail->forma == "VENTA" && $inmueble_detail->dep_nombre == "Santa Cruz"){
                                                $onclick = 'onClick="goog_report_conversion(\''.$url_detalle.'\')"';
                                            }
                                            ?>
                                            <input type="submit" name="Submit" value="Enviar" <?php echo $onclick; ?> class="button">
                                        </form>
                                        <div class="result"></div>
                                    </div>
                            </div>
                            <?php if(!empty($similar_properties)): ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <h4>Publicaciones similares</h4>
                                    <div class="row" style="padding-top: 15px;">
                                        <?php foreach($similar_properties as $publication): ?>
                                            <!-- Item Property-->
                                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                                                <div class="item_property animateIn" data-animate="flipInX" style="border-bottom: 0; position:relative;">
                                                    <div class="head_property">
                                                        <?php
                                                        $url_detalle = base_url();
                                                        $url_detalle .= seo_url($publication->ciu_nombre)."/";
                                                        $url_detalle .= seo_url($publication->cat_nombre . " EN ". $publication->for_descripcion)."/";
                                                        $url_detalle .= seo_url($publication->inm_seo).".html";
                                                        ?>
                                                        <a href="<?php echo $url_detalle; ?>">
                                                            <?php
                                                            $imageX = $publication->fot_archivo;
                                                            $fileimage = BASE_DIRECTORY . DS . "admin" . DS . "imagenes" . DS . "inmueble" . DS . $imageX;

                                                            if (!empty($imageX) && !is_null($imageX) && file_exists($fileimage)) { ?>
                                                                <img src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo base_url(); ?>admin/imagenes/inmueble/<?php echo $publication->fot_archivo; ?>&w=271&h=183" alt="<?php echo $publication->inm_nombre; ?>" title="<?php echo $publication->inm_nombre; ?>">
                                                            <?php } else { ?>
                                                                <img src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo $path_assets.'img/default.png'?>&w=271&h=183" alt="default" title="default toqueeltimbre">
                                                            <?php } ?>

                                                            <h5><?php echo $publication->ciu_nombre; ?></h5>
                                                        </a>

                                                        <div class="av-portfolio-overlay">
                                                            <a href="<?php echo $url_detalle; ?>"><i class="av-icon fa fa-search"></i> Ver Detalle</a>
                                                            <a href="javascript:;" class="btn_favorite" rel="<?php echo $publication->inm_id; ?>"><i class="av-icon fa fa-star"></i> Agregar a Favoritos</a>
                                                        </div>

                                                        <?php if($publication->usu_tipo != "Particular" && !empty($publication->usu_foto)): ?>
                                                            <div class="av-portfolio-info">
                                                                <?php $empresa = !empty($publication->usu_empresa) ? $publication->usu_empresa : $publication->usu_nombre; ?>
                                                                <a href="<?php echo base_url() . 'perfil/'.$publication->usu_id.'/'.urlencode($empresa); ?>">
                                                                    <?php if(!empty($publication->usu_foto)){ ?>
                                                                        <img src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo base_url(); ?>assets/uploads/users/<?php echo strtolower($publication->usu_foto); ?>&h=36" alt="<?php echo $publication->usu_nombre; ?>" title="<?php echo $publication->usu_nombre; ?>" />
                                                                    <?php }else{ ?>
                                                                        <?php echo $empresa; ?>
                                                                    <?php } ?>
                                                                </a>
                                                            </div>
                                                        <?php endif; ?>

                                                    </div>

                                                    <div class="info_property" style="padding-bottom: 0;">
                                                        <ul>
                                                            <li style="line-height: 18px; padding-bottom: 10px; height: 8em">
                                                                <div style="font-weight: bold; margin-bottom:8px;"><?php echo $publication->cat_nombre . " EN ". $publication->for_descripcion; ?></div>
                                                                <span style="float:none;"><?php echo crop_string($publication->inm_nombre, 60); ?></span>
                                                            </li>
                                                            <li>
                                                                <div class="line_separator"></div>
                                                                <strong>Precio</strong><span><?php echo $publication->mon_abreviado . " "; ?><?php echo number_format($publication->inm_precio,0,",",".") ?></span>
                                                            </li>
                                                        </ul>
                                                        <?php if($publication->usu_certificado == "Si"): ?>
                                                            <div class="icon_certificado">
                                                                <img src="<?php echo $path_assets; ?>img/icon_agente_certificados.png" alt="Agente Certificado" class="masterTooltip"/>
	                                                            <div class="tet-tooltip">Este Inmueble pertenece a un Agente Certificado</div>
                                                            </div>
                                                        <?php endif; ?>
                                                        <div class="footer_property">
                                                            <div class="icon m2" title="Metros Cuadrados"><i class="fa" style="margin-right: 2px;"><img src="<?php echo $path_assets; ?>img/icons/metros_cuadrados.png" /></i> <?php echo empty($publication->inm_superficie) ? 0 : $publication->inm_superficie; ?>m2</div>
                                                            <?php foreach($publication->features as $feature): ?>
                                                                <?php if($feature["eca_car_id"] == "4" && !empty($feature["eca_valor"]) && is_numeric($feature["eca_valor"])): ?>
                                                                    <div class="icon bedroom" title="Dormitorios"><i class="fa"><img src="<?php echo $path_assets; ?>img/icons/dormitorios.png" /></i> <?php echo $feature["eca_valor"]; ?></div>
                                                                <?php  endif; ?>

                                                                <?php if($feature["eca_car_id"] == "5" && !empty($feature["eca_valor"]) && is_numeric($feature["eca_valor"])): ?>
                                                                    <div class="icon bathroom" title="Baños"><i class="fa"><img src="<?php echo $path_assets; ?>img/icons/banos.png" /></i> <?php echo $feature["eca_valor"]; ?></div>
                                                                <?php endif; ?>

                                                                <?php if($feature["eca_car_id"] == "9" && !empty($feature["eca_valor"]) && is_numeric($feature["eca_valor"])): ?>
                                                                    <div class="icon parking" title="Parqueo"><i class="fa"><img src="<?php echo $path_assets; ?>img/icons/parqueo.png" /></i> <?php echo $feature["eca_valor"]; ?></div>
                                                                <?php endif; ?>
                                                            <?php endforeach; ?>
                                                            <br style="clear:both" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Item Property-->
                                        <?php endforeach; ?>


                                    </div>
                                </div>
                            </div>
                            <?php endif; ?>

                    <!-- No se encontro inmueble -->
                    <?php }else{ ?>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="alert alert-warning" style="text-transform: uppercase;">
                                    <?php $url_seo = str_replace("-", " ", $this->uri->segment(3)); ?>
                                    <?php $url_seo = str_replace(".html", "", $url_seo); ?>
                                    El inmueble <strong><?php echo $url_seo; ?></strong> ya no esta disponible
                                </div>
                                <?php $relacionadas_aux = $relacionadas["results"]; ?>
                                <?php if(!empty($relacionadas_aux)): ?>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h4>A continuación te mostramos inmuebles que consideramos pueden ser de tu interés.</h4>
                                            <div class="row" style="padding-top: 15px;">
                                                <?php foreach($relacionadas_aux as $publication): ?>
                                                    <!-- Item Property-->
                                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                                                        <div class="item_property animateIn" data-animate="flipInX" style="border-bottom: 0; position:relative;">
                                                            <div class="head_property">
                                                                <?php
                                                                $url_detalle = base_url();
                                                                $url_detalle .= seo_url($publication->ciu_nombre)."/";
                                                                $url_detalle .= seo_url($publication->cat_nombre . " EN ". $publication->for_descripcion)."/";
                                                                $url_detalle .= seo_url($publication->inm_seo).".html";
                                                                ?>
                                                                <a href="<?php echo $url_detalle; ?>">
                                                                    <?php
                                                                    $imageX = $publication->fot_archivo;
                                                                    $fileimage = BASE_DIRECTORY . DS . "admin" . DS . "imagenes" . DS . "inmueble" . DS . $imageX;

                                                                    if (!empty($imageX) && !is_null($imageX) && file_exists($fileimage)) { ?>
                                                                        <img src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo base_url(); ?>admin/imagenes/inmueble/<?php echo $publication->fot_archivo; ?>&w=271&h=183" alt="<?php echo $publication->inm_nombre; ?>" title="<?php echo $publication->inm_nombre; ?>">
                                                                    <?php } else { ?>
                                                                        <img src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo $path_assets.'img/default.png'?>&w=271&h=183" alt="default" title="default toqueeltimbre">
                                                                    <?php } ?>

                                                                    <h5><?php echo $publication->ciu_nombre; ?></h5>
                                                                </a>

                                                                <div class="av-portfolio-overlay">
                                                                    <a href="<?php echo $url_detalle; ?>"><i class="av-icon fa fa-search"></i> Ver Detalle</a>
                                                                    <a href="javascript:;" class="btn_favorite" rel="<?php echo $publication->inm_id; ?>"><i class="av-icon fa fa-star"></i> Agregar a Favoritos</a>
                                                                </div>

                                                                <?php if($publication->usu_tipo != "Particular" && !empty($publication->usu_foto)): ?>
                                                                    <div class="av-portfolio-info">
                                                                        <?php $empresa = !empty($publication->usu_empresa) ? $publication->usu_empresa : $publication->usu_nombre; ?>
                                                                        <a href="<?php echo base_url() . 'perfil/'.$publication->usu_id.'/'.urlencode($empresa); ?>">
                                                                            <?php if(!empty($publication->usu_foto)){ ?>
                                                                                <img src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo base_url(); ?>assets/uploads/users/<?php echo strtolower($publication->usu_foto); ?>&h=36" alt="<?php echo $publication->usu_nombre; ?>" title="<?php echo $publication->usu_nombre; ?>" />
                                                                            <?php }else{ ?>
                                                                                <?php echo $empresa; ?>
                                                                            <?php } ?>
                                                                        </a>
                                                                    </div>
                                                                <?php endif; ?>

                                                            </div>

                                                            <div class="info_property" style="padding-bottom: 0;">
                                                                <ul>
                                                                    <li style="line-height: 18px; padding-bottom: 10px; height: 8em">
                                                                        <div style="font-weight: bold; margin-bottom:8px;"><?php echo $publication->cat_nombre . " EN ". $publication->for_descripcion; ?></div>
                                                                        <span style="float:none;"><?php echo crop_string($publication->inm_nombre, 60); ?></span>
                                                                    </li>
                                                                    <li>
                                                                        <div class="line_separator"></div>
                                                                        <strong>Precio</strong><span><?php echo $publication->mon_abreviado . " "; ?><?php echo number_format($publication->inm_precio,0,",",".") ?></span>
                                                                    </li>
                                                                </ul>
                                                                <?php if($publication->usu_certificado == "Si"): ?>
                                                                    <div class="icon_certificado">
                                                                        <img src="<?php echo $path_assets; ?>img/icon_agente_certificados.png"  />
                                                                    </div>
                                                                <?php endif; ?>
                                                                <div class="footer_property">
                                                                    <div class="icon m2" title="Metros Cuadrados"><i class="fa" style="margin-right: 2px;"><img src="<?php echo $path_assets; ?>img/icons/metros_cuadrados.png" /></i> <?php echo empty($publication->inm_superficie) ? 0 : $publication->inm_superficie; ?>m2</div>
                                                                    <?php foreach($publication->features as $feature): ?>
                                                                        <?php if($feature["eca_car_id"] == "4" && !empty($feature["eca_valor"]) && is_numeric($feature["eca_valor"])): ?>
                                                                            <div class="icon bedroom" title="Dormitorios"><i class="fa"><img src="<?php echo $path_assets; ?>img/icons/dormitorios.png" /></i> <?php echo $feature["eca_valor"]; ?></div>
                                                                        <?php  endif; ?>

                                                                        <?php if($feature["eca_car_id"] == "5" && !empty($feature["eca_valor"]) && is_numeric($feature["eca_valor"])): ?>
                                                                            <div class="icon bathroom" title="Baños"><i class="fa"><img src="<?php echo $path_assets; ?>img/icons/banos.png" /></i> <?php echo $feature["eca_valor"]; ?></div>
                                                                        <?php endif; ?>

                                                                        <?php if($feature["eca_car_id"] == "9" && !empty($feature["eca_valor"]) && is_numeric($feature["eca_valor"])): ?>
                                                                            <div class="icon parking" title="Parqueo"><i class="fa"><img src="<?php echo $path_assets; ?>img/icons/parqueo.png" /></i> <?php echo $feature["eca_valor"]; ?></div>
                                                                        <?php endif; ?>
                                                                    <?php endforeach; ?>
                                                                    <br style="clear:both" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Item Property-->
                                                <?php endforeach; ?>


                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>


                            </div>
                        </div>
                    <?php } ?>

                </div>
                <!-- End Post-->


            </div>
            <!-- End Blog Post-->

            <!-- Sidebars-->
            <div class="col-md-4 col-lg-3">

                <!-- Search Advance -->
                <aside style="padding-bottom: 10px;">
                    <div class="search_advance" style="background-color: #f4f4f4; padding: 15px 10px 5px 10px; border-radius: 4px;">





                        <!-- Datos del Venderos -->
                        <div style="clear: both; border-bottom: 1px solid #cdcdcd;" >
                            <div class="row item_agent" style="margin-bottom: 0;">
                                <?php
                                if($inmueble_detail->usu_tipo == "Particular"){
                                    $nombre = $inmueble_detail->usu_nombre . " " . $inmueble_detail->usu_apellido;
                                    $nombre = trim($nombre);
                                    $imagen = '<img src="'.$path_assets.'img/team/1.jpg" alt="No Image">';
                                    $url_perfil = "";
                                }else{
                                    if(!empty($inmueble_detail->usu_empresa)){
                                        $nombre = $inmueble_detail->usu_empresa;
                                        $nombre = trim($nombre);
                                        $url_perfil = base_url() . 'perfil/'.$inmueble_detail->usu_id.'/'.urlencode($nombre);
                                    }else{
                                        $nombre = $inmueble_detail->usu_nombre;
                                        $nombre = trim($nombre);
                                        $url_perfil = base_url() . 'perfil/'.$inmueble_detail->usu_id.'/'.urlencode($nombre);
                                    }
                                }
                                ?>
                                <div class="col-md-12 info_agent">
                                    <?php if($inmueble->usu_certificado == "Si"): ?>
                                    <div style="float: left; margin-right: 5px;">
                                        <img src="<?php echo $path_assets; ?>img/icon_agente_certificados.png" style="margin-right:3px; width: 30px;" alt="Este Inmueble pertenece a un Agente Certificado" title="Este Inmueble pertenece a un Agente Certificado"  />
                                    </div>
                                    <?php endif; ?>
                                    <h5 style="padding-top:0; float: left;"><?php echo $nombre; ?></h5>
                                    <br style="clear: both;" />
                                    <ul>
                                        <?php if(!empty($inmueble_detail->usu_email)): ?>
                                            <li style="font-size: 14px;"><i class="fa fa-envelope"></i><a href="mailto:<?php echo $inmueble_detail->usu_email; ?>"><?php echo $inmueble_detail->usu_email; ?></a></li>
                                        <?php endif; ?>
                                        <?php if(!empty($inmueble_detail->usu_telefono)): ?>
                                            <li style="font-size: 14px;"><i class="fa fa-phone"></i><a class="no_link" href="tel:<?php echo $inmueble_detail->usu_telefono; ?>"><?php echo $inmueble_detail->usu_telefono; ?></a></li>
                                        <?php endif; ?>
                                        <?php if(!empty($inmueble_detail->usu_celular)): ?>
                                            <li style="font-size: 14px;"><i class="fa fa-mobile-phone"></i><a class="no_link" href="tel:<?php echo $inmueble_detail->usu_celular; ?>"><?php echo $inmueble_detail->usu_celular; ?></a></li>
                                        <?php endif; ?>
                                    </ul>
                                </div>
                            </div>
                            <p class="description_agency"><?php echo !empty($inmueble_detail->usu_descripcion) ? crop_string2($inmueble_detail->usu_descripcion, 240) : ''; ?></p>
                            <?php //if(!empty($url_perfil)): ?>
                                <!--<p><a href="<?php echo $url_perfil; ?>" style="font-size: 14px; text-decoration: underline;">Ver Perfil</a></p>-->
                            <?php //endif; ?>
                        </div>
                        <!-- Fin Datos del Venderor -->

                        <div id="box_contact_agent" style="margin-top: 10px;">
                            <a name="Formulario"></a>
                            <h2>Contacto</h2>
                            <form id="form" class="form_contact" action="#">
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="text" placeholder="Nombre" name="nombre" required style="margin-bottom: 7px;">
                                    </div>
                                    <div class="col-md-12">
                                        <input type="text" placeholder="Telefono" name="telefono" required style="margin-bottom: 7px;">
                                    </div>
                                    <div class="col-md-12">
                                        <input type="email" placeholder="Correo Electronico"  name="email" required style="margin-bottom: 7px;">
                                    </div>
                                </div>
                                <textarea placeholder="Tu comentario..." name="comentario" required></textarea>
                                <input type="hidden" name="empresa" value="" />
                                <input type="hidden" name="id_inmueble" value="<?php echo $inmueble_detail->id; ?>" />
                                <?php
                                $url_detalle = base_url();
                                $url_detalle .= seo_url($inmueble_detail->ciudad)."/";
                                $url_detalle .= seo_url($inmueble_detail->categoria . " EN ". $inmueble_detail->forma)."/";
                                $url_detalle .= seo_url($inmueble_detail->seo).".html";

                                if($inmueble_detail->categoria == "CASAS" && $inmueble_detail->forma == "VENTA" && $inmueble_detail->dep_nombre == "Santa Cruz"){
                                    $onclick = 'onClick="goog_report_conversion(\''.$url_detalle.'\')"';
                                }
                                ?>
                                <input type="submit" name="Submit" value="Enviar" <?php echo $onclick; ?> class="button">
                            </form>
                            <div class="result"></div>
                        </div>




                    </div>
                </aside>
                <!-- End Search Advance -->

                <!-- Accordion -->
                <aside>
                    <h2>Proyectos</h2>
                    <?php foreach($projects as $project): ?>
                        <div class="project_item">
                            <div class="row">
                                <div class="col-md-12">
                                    <a href="<?php echo base_url(). $project->proy_seo; ?>">
                                        <img src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo base_url(); ?>admin/imagenes/proyecto/<?php echo $project->proy_fot_imagen; ?>&w=500&h=281" alt="<?php echo $project->inm_nombre; ?>" title="<?php echo $project->inm_nombre; ?>">
                                    </a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <h4><?php echo $project->inm_nombre; ?></h4>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <small><?php echo $project->ciu_nombre; ?></small>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <small><strong><?php echo ucwords(strtolower($project->for_descripcion)); ?></strong></small>
                                </div>
                                <div class="col-md-6 text_right">
                                    <small><strong><?php echo $project->mon_abreviado; ?><?php echo number_format($project->inm_precio, 0, ",", "."); ?></strong></small>
                                </div>
                            </div>

                        </div>
                        <div class="divisor"></div>
                    <?php endforeach; ?>

                </aside>
                <!-- End Accordion -->
            </div>
            <!-- Sidebars-->
        </div>
    </div>
</section>
<!-- End content info-->