<?php
$path_assets = base_url() . "assets/";
$inmueble_projects = $inmueble_projects;
$pay_publications = $pay_publications;
$url_new_publication = base_url(). "dashboard/publications/register";
?>
<div id="layout-home">

<!-- Header-->
<header>
    <?php if(!$mobile || $ipad){ ?>
    <!-- Slide -->
    <!--<div class="camera_wrap camera_white_skin" id="slide">-->
    <div class="camera_wrap camera_white_skin">
        <?php //foreach($slideshow as $slide): ?>
        <!-- Item Slide -->
        <!--<div  data-src="<?php echo base_url(); ?>admin/imagenes/slideshow/<?php echo $slide["imagen"]; ?>">-->
        <div >
            <div class="camera_caption fadeFromTop">
                <div class="container">
                    <img src="<?php echo base_url(); ?>assets/img/slide/cover_imagen.png" style="width: 100%;" alt="Bienvenido sentite en casa" />
                    <!--<div class="row">
                        <div class="col-md-7 col-md-offset-5">
                            <h3 class="animated fadeInRight delay1">Bienvenido
                                <span class="arrow_title_slide"></span>
                            </h3>
                            <p class="animated fadeInRight delay2">Sentite en Casa. ¡Busc&aacute; tu Casa! &nbsp;&nbsp;</p>
                            <div class="more animated fadeInRight delay4">
                                <?php if(!empty($user_logged)){ ?>
                                    <a href="<?php echo base_url(); ?>dashboard/publication/register" rel="canonical" class="button">Publicar</a>
                                    <a href="<?php echo base_url(); ?>registrarse" rel="canonical" class="button">Registrate</a>
                                <?php }else{ ?>
                                    <a href="<?php echo base_url(); ?>login?url_redirect=<?php echo urlencode($url_new_publication); ?>" rel="canonical" class="button">Publicar</a>
                                    <a href="<?php echo base_url(); ?>registrarse" rel="canonical" class="button">Registrate</a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>-->
                </div>
            </div>
        </div>
        <!-- End Item Slide -->
        <?php //endforeach; ?>
    </div>
    <!-- End Slide -->
    <?php }else{ ?>
        <div style="position: relative;">
            <img src="<?php echo base_url(); ?>assets/img/slide/persona_movil.png" style="width: 100%;"  />
            <div style="position: absolute; top: 0; left: 0; width: 100%; background: rgba(255, 255, 255, 0.5) ; padding: 8px 0; text-align: center;">
                <div style="margin: 0 15px;">
                    <img src="<?php echo base_url(); ?>assets/img/slide/cover_movil.png" style="width: 100%;" />
                </div>
            </div>
        </div>
    <?php } ?>
</header>
<!-- End Header-->


<?php echo $box_search; ?>


<!-- content info -->
<div class="content_info">
<div class="container">



<!--<div class="row">
    <div class="col-md-12">
        <div class="titles">
            <h2 style="display: inline; font-size: 30px; line-height: 40px; padding-bottom: 7px;">CATEGORIAS DESTACADAS</h2>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 15px;">
        <div class="dashboard-stat button_landing" style="text-align: center;">
            <a href="<?php echo base_url(); ?>comprar">
                <span class="title_landing">Venta</span>
                <span class="description_landing">Busca todas las propiedades en venta en tu área</span>
            </a>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 15px;">
        <div class="dashboard-stat button_landing" style="text-align: center;">
            <a href="<?php echo base_url(); ?>alquiler">
                <span class="title_landing">Alquiler</span>
                <span class="description_landing">Busca todas las propiedades en venta en tu área</span>
            </a>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 15px;">
        <div class="dashboard-stat button_landing" style="text-align: center;">
            <a href="<?php echo base_url(); ?>anticretico">
                <span class="title_landing">Anticretico</span>
                <span class="description_landing">Busca todas las propiedades en venta en tu área</span>
            </a>
        </div>
    </div>
</div>-->


<!-- Content Carousel Properties -->
<div class="content-carousel">

    <div class="row">
        <div class="col-md-12">
            <!-- Title-->
            <div class="titles">
                <h2 style="display: inline; font-size: 30px; line-height: 40px; margin: 20px 0; padding-bottom: 7px;">Inmuebles a Estrenar</h2>
            </div>
            <!-- End Title-->
        </div>
    </div>

    <!-- Carousel Properties -->
    <div id="properties-carousel" class="on-carousel properties-carousel" style="margin-bottom: 15px;">
        <?php if(!empty($inmueble_projects)):
	        $var_navigable = count($inmueble_projects);
	        foreach($inmueble_projects as $project): ?>
        <!-- Item Property-->
        <div class="item_property">
            <div class="head_property">
                <a href="<?php echo base_url(). $project->proy_seo; ?>">
                    <!-- <img src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo base_url(); ?>admin/imagenes/proyecto/<?php echo $project->proy_fot_imagen; ?>&w=271&h=183" alt="<?php echo $project->inm_nombre; ?>" title="<?php echo $project->inm_nombre; ?>"> -->
                    <?php
                    $fileimage = BASE_DIRECTORY . DS . "admin" . DS . "imagenes" . DS . "proyecto" . DS . $project->proy_fot_imagen;
                    if (!empty($project->proy_fot_imagen) && !is_null($project->proy_fot_imagen) && file_exists($fileimage)) { ?>
                        <img src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo base_url(); ?>admin/imagenes/proyecto/<?php echo $project->proy_fot_imagen?>&w=271&h=183" alt="<?php echo $project->inm_nombre; ?>" title="<?php echo $project->inm_nombre; ?>">
                    <?php } else { ?>
                        <img src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo $path_assets.'img/default.png'?>&w=500&h=281" alt="toqueeltimbre" title="toqueeltimbre">
                    <?php } ?>
                    <h5><?php echo $project->ciu_nombre; ?></h5>
                </a>
            </div>
            <div class="info_property">
                <ul>
                    <li><div class="bold_text" style="font-size: 14px;"><?php echo $project->inm_nombre; ?></div></li>
                    <li>
                        <div class="line_separator"></div>
                        <span class="bold_text" style="float: none; font-size: 14px;"><?php echo ucwords(strtolower($project->for_descripcion)); ?></span>
                        <span>
                            <?php echo $project->mon_abreviado; ?>
                            <?php echo number_format($project->inm_precio, 0, ",", "."); ?>
                        </span>
                    </li>
                </ul>
            </div>
        </div>
        <!-- Item Property-->
        <?php endforeach; ?>
        <?php endif; ?>

    </div>
    <!-- End Carousel Properties -->
</div>
<!-- End Content Carousel Properties -->



<!-- Title-->
<div class="row">
    <div class="col-md-12">
        <div class="titles">
            <h2 style="display: inline; font-size: 30px; line-height: 40px; margin: 20px 0; padding-bottom: 7px;">Anuncios Destacados</h2>
        </div>
    </div>
</div>
<!-- End Title-->


<!-- Row Properties-->
<div class="row">
    <?php if(!empty($pay_publications)): ?>
    <?php $count_pays = count($pay_publications); ?>
    <?php foreach($pay_publications as $publication): ?>
    <!-- Item Property-->
    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
        <div class="item_property animateIn" data-animate="flipInX" style="border-bottom: 0;">
            <div class="head_property">
                <?php
                $url_detalle = base_url();
                $url_detalle .= seo_url($publication->ciu_nombre)."/";
                $url_detalle .= seo_url($publication->cat_nombre . " EN ". $publication->for_descripcion)."/";
                $url_detalle .= seo_url($publication->inm_seo).".html";
                ?>
                <a href="<?php echo $url_detalle; ?>">

                    <!-- <img src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo base_url(); ?>admin/imagenes/inmueble/<?php echo $publication->fot_archivo; ?>&w=271&h=183" alt="<?php echo $publication->inm_nombre; ?>" title="<?php echo $publication->inm_nombre; ?>"> -->

                    <?php
                    $imageX = $publication->fot_archivo;
                    $fileimage = BASE_DIRECTORY . DS . "admin" . DS . "imagenes" . DS . "inmueble" . DS . $imageX;

                    if (!empty($imageX) && !is_null($imageX) && file_exists($fileimage)) { ?>
                        <img src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo base_url(); ?>admin/imagenes/inmueble/<?php echo $publication->fot_archivo; ?>&w=271&h=183" alt="<?php echo $publication->inm_nombre; ?>" title="<?php echo $publication->inm_nombre; ?>">
                    <?php } else { ?>
                        <img src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo $path_assets.'img/default.png'?>&w=271&h=183" alt="default" title="default toqueeltimbre">
                    <?php } ?>

                    <h5><?php echo $publication->ciu_nombre; ?></h5>
                </a>

                <div class="av-portfolio-overlay">
                    <a href="<?php echo $url_detalle; ?>"><i class="av-icon fa fa-search"></i> <span>Ver Detalle</span></a>
                    <a href="javascript:;" class="btn_favorite" rel="<?php echo $publication->inm_id; ?>"><i class="av-icon fa fa-star"></i> <span>Agregar a Favoritos</span></a>
                </div>

                <?php if($publication->usu_tipo != "Particular" && !empty($publication->usu_foto)): ?>
                    <div class="av-portfolio-info">
                        <?php $empresa = !empty($publication->usu_empresa) ? $publication->usu_empresa : $publication->usu_nombre; ?>
                        <a href="<?php echo base_url() . 'perfil/'.$publication->usu_id.'/'.urlencode($empresa); ?>">
                            <?php if(!empty($publication->usu_foto)){ ?>
                                <img src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo base_url(); ?>assets/uploads/users/<?php echo strtolower($publication->usu_foto); ?>&h=36" alt="<?php echo $empresa; ?>" title="<?php echo $empresa; ?>" />
                            <?php }else{ ?>
                                <?php echo $empresa; ?>
                            <?php } ?>
                        </a>
                    </div>
                <?php endif; ?>

            </div>
            <div class="info_property stand_out" style="padding-bottom: 0;">
                <ul>
                    <li style="line-height: 18px; padding-bottom: 10px; height: 5em">
                        <div style="font-weight: bold; margin-bottom:8px;"><?php echo $publication->cat_nombre . " ". $publication->for_descripcion; ?></div>
                        <span style="float:none;"><strong style="font-weight: normal;"><?php echo crop_string($publication->inm_nombre, 60); ?></strong></span>
                    </li>
                    <li>
                        <div class="line_separator"></div>
                        <span style="float: none; font-size: 14px;" class="bold_text">Precio</span><span><?php echo $publication->mon_abreviado . " "; ?><?php echo number_format($publication->inm_precio,0,",",".") ?></span>
                    </li>
                </ul>
                <div class="footer_property">
                    <?php $tipo_sup = $publication->inm_tipo_superficie == "Metros Cuadrados" ? "m2" : "ha"; ?>
                    <div class="icon m2" title="Metros Cuadrados"><i class="fa"><img src="<?php echo $path_assets; ?>img/icons/metros_cuadrados.png" alt="Metros Cuadrados" /></i> <?php echo empty($publication->inm_superficie) ? 0 : $publication->inm_superficie; ?><?php echo $tipo_sup; ?></div>
                    <?php foreach($publication->features as $feature): ?>
                        <?php if($feature["eca_car_id"] == "4" && !empty($feature["eca_valor"])): ?>
                            <div class="icon bedroom" title="Dormitorios"><i class="fa"><img src="<?php echo $path_assets; ?>img/icons/dormitorios.png" alt="Dormitorio" /></i> <?php echo $feature["eca_valor"]; ?></div>
                        <?php  endif; ?>

                        <?php if($feature["eca_car_id"] == "5" && !empty($feature["eca_valor"])): ?>
                            <div class="icon bathroom" title="Baños"><i class="fa"><img src="<?php echo $path_assets; ?>img/icons/banos.png" alt="Baño" /></i> <?php echo $feature["eca_valor"]; ?></div>
                        <?php endif; ?>

                        <?php if($feature["eca_car_id"] == "9" && !empty($feature["eca_valor"])): ?>
                            <div class="icon parking" title="Parqueo"><i class="fa"><img src="<?php echo $path_assets; ?>img/icons/parqueo.png" alt="Parqueo" /></i> <?php echo $feature["eca_valor"]; ?></div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <br style="clear:both" />
                </div>
            </div>

        </div>
    </div>
    <!-- Item Property-->
    <?php endforeach; ?>

    <?php if($count_pays > 6 ): ?>
        <?php for($i = 0; $i < (8 - $count_pays); $i++): ?>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                <div class="item_property" style="border-bottom: 0;">
                    <div class="head_property">
                        <a href="<?php echo base_url(); ?>tabla-de-precios">
                            <img src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo $path_assets.'img/default.png'?>&w=271&h=183" alt="destacar anuncio" title="destacar anuncio" style="border-color: #eee;">
                        </a>
                    </div>
                    <div class="info_property stand_out" style="padding: 15px 0px 36px 0px; background: #EEEEEE; border: 0px;">
                        <ul>
                            <li><div style="font-size: 13px; text-align: center">Puedes Mejorar la visibilidad de tus anuncios accediendo a tu cuenta.</div></li>
                        </ul>
                        <a href="<?php echo base_url(); ?>tabla-de-precios">
                        <div class="footer_property" style="margin-top: 41px; color: white;text-align: center; background: #00AEEF; font-size: 13px">
                            MAS INFORMACIÓN
                            <br style="clear:both" />
                        </div>
                        </a>
                    </div>

                </div>
            </div>
        <?php endfor; ?>
    <?php endif; ?>

    <?php endif; ?>

</div>
<!-- End Row Properties-->


<!-- Row Publicidad-->
<div class="row">
    <div class="col-md-12">
        <!--<img src="<?php echo base_url() ?>assets/img/publicidad.jpg" style="width: 100%" alt="Publicidad" />-->
        <center class="banner_home">
            <a href="<?php echo base_url() ?>torre-link" style="text-align: center">
                <img src="<?php echo base_url() ?>assets/img/banner_home/torre_publicidad_large.png" alt="Torre Link"  style="width: 100%; max-width: 940px;"/>
            </a>
        </center>

        <center class="banner_home_mobile">
            <a href="<?php echo base_url() ?>torre-link" style="text-align: center">
                <img src="<?php echo base_url() ?>assets/img/banner_home/torre_publicidad.png" alt="Torre Link" style="width: 100%;" />
            </a>
        </center>
    </div>
</div>
<!-- End Row Publicidad-->


</div>
<!-- End Container -->
</div>
<!-- End content info-->
</div>
