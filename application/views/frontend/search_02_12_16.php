<?php
$path_assets = base_url() . "assets/";
//$filter_config = translate_decode_url($_GET);
//$filter_config = json_decode(json_encode($filter_config), FALSE);
$filter_config = $config_filter;
$aux_search = $filter_config->search;
$aux_in = $filter_config->in;
$aux_type = $filter_config->type;
$aux_city = $filter_config->city;
$aux_room = $filter_config->room;
$aux_bathroom = $filter_config->bathroom;
$aux_parking = $filter_config->parking;
$aux_currency = $filter_config->currency;
$url_search = base_url() . "buscar/?";
?>

<!-- End content info -->
<section class="content_info content_info_search" style="padding-top: 0; border-top: 5px solid #00aeef;">
<div class="container">

    <div class="row">
    <!-- property List-->
    <div class="col-md-9 properties_two">

    <!-- Bar Filter properties-->
    <!--<div class="bar_properties">
        <div class="row">

            <div class="col-md-2 alignright">
                <?php
                /*$filter_config->in = "Anticretico";
                $url_seo_forma = base_url();
                $url_seo_forma .= convert_values_url($filter_config);*/
                ?>
                <a <?php echo $aux_in == "Anticretico" ? 'class="active"' : ''; ?> href="<?php echo $url_seo_forma; ?>">Anticretico</a>
            </div>
            <div class="col-md-2 alignright">
                <?php
                /*$filter_config->in = "Alquiler";
                $url_seo_forma = base_url();
                $url_seo_forma .= convert_values_url($filter_config);*/

                ?>
                <a <?php echo $aux_in == "Alquiler" ? 'class="active"' : ''; ?> href="<?php echo $url_seo_forma; ?>">Alquiler</a>
            </div>
            <div class="col-md-2 alignright">
                <?php
                /*$filter_config->in = "Comprar";
                $url_seo_forma = base_url();
                $url_seo_forma .= convert_values_url($filter_config);*/
                ?>
                <a <?php echo $aux_in == "Venta" ? 'class="active"' : ''; ?> href="<?php echo $url_seo_forma; ?>">Comprar</a>
            </div>
            <div class="col-md-2 alignright">
                <?php
                /*$filter_config->type = null;
                $filter_config->in = null;
                $filter_config->currency = "Dolares";
                $filter_config->price_min = null;
                $filter_config->price_max = null;
                $url_seo_forma = base_url();
                $url_seo_forma .= convert_values_url($filter_config);*/
                ?>
                <a <?php echo $aux_in == null ? 'class="active"' : ''; ?> href="<?php echo $url_seo_forma; ?>">Todos</a>
            </div>
        </div>
    </div>-->
    <!-- End Bar Filter properties-->

    <!-- Row Propertys-->
    <div class="row">

        <?php if(!empty($publications)){ ?>
            <div class="col-md-12" style="margin-top: 8px; margin-bottom: 15px;">
                <h1 style="margin-bottom: 0px; padding-bottom: 0px; float: left;">Resultados de la Busqueda (<?php echo $total_rows > 0 ? $total_rows : 0; ?>)</h1>
                <section class="sort" style="visibility: visible; margin-top: 12px;">

                    <span class="button" style="margin-right: 6px; position: relative;">
						<nobr>Ordenar</nobr>
						<form id="form-sort" action="<?php echo base_url(); ?>search/sort" method="post" style="opacity: 0; height: 35px; left: 0; position: absolute; top: 0; width: 107px;">
                            <select id="sort-type" name="sort-type" style="width: 107px; padding-left: 0; height: 35px;">
                                <option value="date-desc" <?php echo $filter->sort == "date-desc" ? 'selected':''; ?>>Fecha (Descendentemente)</option>
                                <option value="date-asc" <?php echo $filter->sort == "date-asc" ? 'selected':''; ?>>Fecha (Ascendentemente)</option>
                                <option value="price-asc" <?php echo $filter->sort == "price-asc" ? 'selected':''; ?>>Precio (Menor a Mayor)</option>
                                <option value="price-desc" <?php echo $filter->sort == "price-desc" ? 'selected':''; ?>>Precio (Mayor a Menor)</option>
                            </select>
                            <input type="hidden" name="in" value="<?php echo $aux_in; ?>" />
                            <input type="hidden" name="type" value="<?php echo $aux_type; ?>" />
                            <input type="hidden" name="city" value="<?php echo $aux_city; ?>" />
                        </form>
                    </span>



                   <!-- <form id="form-sort" action="<?php echo base_url(); ?>search/sort" method="post" style="width: 28px;">
                        <select id="sort-type" name="sort-type">
                            <option value="price-desc" <?php echo $filter_config->sort == "price-desc" ? 'selected':''; ?>>Precio (Mayor a Menor)</option>
                            <option value="price-asc" <?php echo $filter_config->sort == "price-asc" ? 'selected':''; ?>>Precio (Menor a Mayor)</option>
                            <option value="date-asc" <?php echo $filter_config->sort == "date-asc" ? 'selected':''; ?>>Fecha (Ascendentemente)</option>
                            <option value="date-desc" <?php echo $filter_config->sort == "date-desc" ? 'selected':''; ?>>Fecha (Descendentemente)</option>
                        </select>
                        <input type="hidden" name="in" value="<?php echo $aux_in; ?>" />
                        <input type="hidden" name="type" value="<?php echo $aux_type; ?>" />
                        <input type="hidden" name="city" value="<?php echo $aux_city; ?>" />
                    </form>-->
                </section>


            </div>

            <div class="col-md-12">
                <p style="margin-bottom:18px;"><a href="<?php echo base_url(); ?>mis-favoritos" style="color: #00aeef; font-weight: bold;">Mis Anuncios Favoritos</a></p>
            </div>

            <?php foreach($publications as $publication): ?>
            <!-- Item Property-->
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                <div class="item_property animateIn" data-animate="flipInX" style="border-bottom: 0; position: relative">
                    <div class="head_property">
                        <?php
                        if($publication->patrocinado == "Proyecto"){
                            $obj_project = $this->model_proyecto->get_info_project($publication->inm_proy_id);
                            $url_detalle = base_url();
                            $url_detalle .= $obj_project->proy_seo;
                        }else{
                            $url_detalle = base_url();
                            $url_detalle .= seo_url($publication->ciu_nombre)."/";
                            $url_detalle .= seo_url($publication->cat_nombre . " EN ". $publication->for_descripcion)."/";
                            $url_detalle .= seo_url($publication->inm_seo).".html";
                        }
                        if($aux_city == "Santa Cruz" && $aux_in == "Venta" && $aux_type == "Casas"){
                            //$onclick = 'onClick="goog_report_conversion(\''.$url_detalle.'\')"';
                        }
                        ?>
                        <a href="<?php echo $url_detalle; ?>" <?php echo $onclick; ?>>
                            <?php
                            $imageX =  $publication->fot_archivo;
                            $fileimage = BASE_DIRECTORY . DS . "admin" . DS . "imagenes" . DS . "inmueble" . DS . $imageX;
                            ?>
                            <?php if($publication->patrocinado == "Proyecto"){ ?>
                                <img src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo base_url(); ?>admin/imagenes/proyecto/<?php echo $obj_project->proy_fot_imagen?>&w=271&h=183" alt="<?php echo $obj_project->proy_nombre; ?>" title="<?php echo $project->proy_nombre; ?>">
                            <?php }elseif(!empty($imageX) && !is_null($imageX) && file_exists($fileimage)){ ?>
                                <img src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo base_url(); ?>admin/imagenes/inmueble/<?php echo ($publication->fot_archivo); ?>&w=271&h=183" alt="<?php echo $publication->inm_nombre; ?>" title="<?php echo $publication->inm_nombre; ?>">
                            <?php }else{ ?>
                                <img src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo $path_assets.'img/default.png'?>&w=271&h=183" alt="default" title="default toqueeltimbre">
                            <?php } ?>

                            <h5><?php echo $publication->ciu_nombre; ?></h5>
                        </a>

                        <div class="av-portfolio-overlay">
                            <a href="<?php echo $url_detalle; ?>"><i class="av-icon fa fa-search"></i> Ver Detalle</a>
                            <?php $favorite = check_favorite_view($usuario_id, $publication->inm_id); ?>

                            <?php if($publication->patrocinado != "Proyecto"){ ?>
                                <a href="javascript:;" class="btn_favorite <?php echo $favorite ? 'check_favorite' : ''; ?>" rel="<?php echo $publication->inm_id; ?>">
                                    <i class="av-icon fa fa-star"></i> Agregar a Favoritos
                                </a>
                            <?php }else{ ?>
                                <br style="clear:both;" />
                                <br style="clear:both;" />
                            <?php } ?>
                        </div>

                        <?php if($publication->usu_tipo != "Particular" && $publication->patrocinado != "Proyecto"): ?>
                        <div class="av-portfolio-info">
                            <?php $empresa = !empty($publication->usu_empresa) ? $publication->usu_empresa : $publication->usu_nombre; ?>
                            <a href="<?php echo base_url() . 'perfil/'.$publication->usu_id.'/'.urlencode($empresa); ?>">
                                <?php if(!empty($publication->usu_foto)){ ?>
                                    <img src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo base_url(); ?>assets/uploads/users/<?php echo strtolower($publication->usu_foto); ?>&h=36" alt="<?php echo $publication->usu_nombre; ?>" title="<?php echo $publication->usu_nombre; ?>" />
                                <?php }else{ ?>
                                    <?php echo $empresa; ?>
                                <?php } ?>
                            </a>
                        </div>
                        <?php endif; ?>

                    </div>

                    <div class="info_property
                    <?php
                    if($publication->patrocinado == "Patrocinado"){
                        echo 'stand_out';
                    }elseif($publication->patrocinado == "Proyecto"){
                        echo 'stand_out_project';
                    }
                    ?>" style="padding-bottom: 0;">
                        <ul>
                            <li style="line-height: 18px; padding-bottom: 10px; height: 8em">
                                <div style="font-weight: bold; margin-bottom:8px;"><?php echo $publication->cat_nombre . " EN ". $publication->for_descripcion; ?></div>
                                <span style="float:none;"><?php echo crop_string($publication->inm_nombre, 60); ?></span>
                            </li>
                            <li>
                                <div class="line_separator"></div>
                                <strong>Precio</strong><span><?php echo $publication->mon_abreviado . " "; ?><?php echo number_format($publication->inm_precio,0,",",".") ?></span>
                            </li>
                        </ul>
                        <?php if($publication->usu_certificado == "Si"): ?>
                        <div class="icon_certificado">
                            <img src="<?php echo $path_assets; ?>img/icon_agente_certificados.png" alt="Agente Certificado" class="masterTooltip"/>
	                        <div class="tet-tooltip">Este Inmueble pertenece a un Agente Certificado</div>
                        </div>
                        <?php endif; ?>

                        <div class="footer_property">
                            <?php $tipo_sup = $publication->inm_tipo_superficie == "Metros Cuadrados" ? "m2" : "ha"; ?>
                            <div class="icon m2" title="Metros Cuadrados"><i class="fa" style="margin-right: 2px;"><img src="<?php echo $path_assets; ?>img/icons/metros_cuadrados.png" /></i> <?php echo empty($publication->inm_superficie) ? 0 : $publication->inm_superficie; ?><?php echo $tipo_sup; ?></div>
                            <?php foreach($publication->features as $feature): ?>
                                <?php if($feature["eca_car_id"] == "4" && !empty($feature["eca_valor"]) && is_numeric($feature["eca_valor"])): ?>
                                    <div class="icon bedroom" title="Dormitorios"><i class="fa"><img src="<?php echo $path_assets; ?>img/icons/dormitorios.png" /></i> <?php echo $feature["eca_valor"]; ?></div>
                                <?php  endif; ?>

                                <?php if($feature["eca_car_id"] == "5" && !empty($feature["eca_valor"]) && is_numeric($feature["eca_valor"])): ?>
                                    <div class="icon bathroom" title="Baños"><i class="fa"><img src="<?php echo $path_assets; ?>img/icons/banos.png" /></i> <?php echo $feature["eca_valor"]; ?></div>
                                <?php endif; ?>

                                <?php if($feature["eca_car_id"] == "9" && !empty($feature["eca_valor"]) && is_numeric($feature["eca_valor"])): ?>
                                    <div class="icon parking" title="Parqueo"><i class="fa"><img src="<?php echo $path_assets; ?>img/icons/parqueo.png" /></i> <?php echo $feature["eca_valor"]; ?></div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            <br style="clear:both" />
                        </div>
                    </div>
                </div>
            </div>
            <!-- Item Property-->
            <?php endforeach; ?>
        <?php }else{ ?>
            <div class="col-md-12">
                <h1>Resultados de la Busqueda</h1>
                <div class="alert alert-danger">No se encontraron resultados.</div>
            </div>
        <?php } ?>

    </div>
    <!-- Row Propertys-->

    <!-- Pagination -->
    <div class="paginations">
        <?php echo $pagination; ?>
    </div>
    <!-- End Pagination -->

    <div class="col-md-12" style="background-color: #DDDDDD; margin-top: 55px;">
        <center><a href="<?php echo base_url(); ?>ajax/form_busqueda_user" class="busqueda_usuario"><img class="formulario_busqueda" src="<?php echo $path_assets; ?>img/icons/boton-info2.png" style="margin-top: -55px;" /></a></center>
    </div>

    </div>
    <!-- End property List-->



    <!-- Aside -->
    <div class="col-md-3">
    <!-- Search Advance -->
    <aside>
        <div class="search_advance">

            <ul class="tabs_services">
                <li><a id="1" class="set2">Mejora tu búsqueda</a></li>
            </ul>

            <!-- 1-content -->
            <div id="1-content" class="set2 show">
                <div class="search_box">
                    <form action="<?php echo base_url() . "buscar" ?>" method="post">
                        <!--<div class="">
                            <input type="text" name="search" value="<?php echo $aux_search; ?>" placeholder="Buscar...">
                        </div>-->
                        <div>
                            <label>Estoy buscando...</label>
                            <select name="type">
                                <option value="Inmuebles">Todas los tipos</option>
                                <?php foreach($categories as $cat): ?>
                                    <?php $valor = ucwords(strtolower($cat->cat_nombre)); ?>
                                    <option value="<?php echo $valor; ?>" <?php echo $aux_type == $valor ? 'selected="selected"' : ''; ?>><?php echo $valor; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div>
                            <label>En la ciudad de...</label>
                            <select name="city">
                                <option value="Bolivia">Todas las ciudades</option>
                                <?php foreach($cities as $city): ?>
                                    <?php $valor = ucwords(strtolower($city->dep_nombre)); ?>
                                    <option value="<?php echo $valor; ?>" <?php echo $aux_city == $valor ? 'selected' : ''; ?>><?php echo $valor; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div >
                            <label>En...</label>
                            <select name="in">
                                <option value="Venta" <?php echo $aux_in == "Venta" ? 'selected' : ''; ?>>Venta</option>
                                <option value="Alquiler" <?php echo $aux_in == "Alquiler" ? 'selected' : ''; ?>>Alquiler</option>
                                <option value="Anticretico" <?php echo $aux_in == "Anticretico" ? 'selected' : ''; ?>>Anticretico</option>
                            </select>
                        </div>
                        <div >
                            <label>Habitaciones</label>
                            <select name="room">
                                <option value="">No Definido</option>
                                <option value="1" <?php echo $aux_room == "1" ? 'selected' : ''; ?>>1</option>
                                <option value="2" <?php echo $aux_room == "2" ? 'selected' : ''; ?>>2</option>
                                <option value="3" <?php echo $aux_room == "3" ? 'selected' : ''; ?>>3</option>
                                <option value="4" <?php echo $aux_room == "4" ? 'selected' : ''; ?>>4</option>
                                <option value="5+" <?php echo $aux_room == "5+" ? 'selected' : ''; ?>>5+</option>
                            </select>
                        </div>
                        <div >
                            <label>Baños</label>
                            <select name="bathroom">
                                <option value="">No Definido</option>
                                <option value="1" <?php echo $aux_bathroom == "1" ? 'selected' : ''; ?>>1</option>
                                <option value="2" <?php echo $aux_bathroom == "2" ? 'selected' : ''; ?>>2</option>
                                <option value="3" <?php echo $aux_bathroom == "3" ? 'selected' : ''; ?>>3</option>
                                <option value="4" <?php echo $aux_bathroom == "4" ? 'selected' : ''; ?>>4</option>
                                <option value="5+" <?php echo $aux_bathroom == "5+" ? 'selected' : ''; ?>>5+</option>
                            </select>
                        </div>
                        <div >
                            <label>Parqueo</label>
                            <select name="parking">
                                <option value="">No Definido</option>
                                <option value="1" <?php echo $aux_parking == "1" ? 'selected' : ''; ?>>1</option>
                                <option value="2" <?php echo $aux_parking == "2" ? 'selected' : ''; ?>>2</option>
                                <option value="3" <?php echo $aux_parking == "3" ? 'selected' : ''; ?>>3</option>
                                <option value="4" <?php echo $aux_parking == "4" ? 'selected' : ''; ?>>4</option>
                                <option value="5+" <?php echo $aux_parking == "5+" ? 'selected' : ''; ?>>5+</option>
                            </select>
                        </div>
                        <div >
                            <label>Moneda</label>
                            <div class="switch-wrapper">
                                <input type="checkbox" name="currency" value="1" <?php echo ($aux_currency == "Dolares" || empty($aux_currency)) ? 'checked' : ''; ?>>
                            </div>
                        </div>
                        <div>
                            <p>
                                <label for="amount">Precios:</label>
                                <input type="text" id="amount" readonly style="border:0; color:#f6931f; font-weight:bold;">
                            </p>
                            <div id="slider-range"></div>
                        </div>
                        <div>
                            <input type="hidden" name="price_min" id="price_min" value="" />
                            <input type="hidden" name="price_max" id="price_max" value="" />
                            <input type="hidden" name="search" id="search" value="<?php echo $aux_search; ?>" />
                            <input type="hidden" name="currency" id="currency" value="Dolares" />
                            <!--<input type="hidden" name="in" value="<?php echo empty($aux_in) ? null : $aux_in; ?>" />-->
                            <input type="submit" class="button" style="background-color: rgba(0, 174, 239, 1) !important; margin-top: 25px; font-weight: 600;" value="Mostrar Resultados">
                        </div>
                    </form>
                </div>
            </div>
            <!-- End 1-content -->

        </div>
    </aside>
    <!-- End Search Advance -->

    <!-- Accordion -->
    <aside>
        <h2>Proyectos</h2>
        <?php foreach($inmueble_projects as $project): ?>
            <div class="project_item">
                <div class="row">
                    <div class="col-md-12">
                        <a href="<?php echo base_url() . $project->proy_seo; ?>">
                            <?php
                            $fileimage = BASE_DIRECTORY . DS . "admin" . DS . "imagenes" . DS . "proyecto" . DS . $project->proy_fot_imagen;
                            if (!empty($project->proy_fot_imagen) && !is_null($project->proy_fot_imagen) && file_exists($fileimage)) { ?>
                                <img src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo base_url(); ?>admin/imagenes/proyecto/<?php echo $project->proy_fot_imagen?>&w=500&h=281" alt="<?php echo $project->inm_nombre; ?>" title="<?php echo $project->inm_nombre; ?>">
                            <?php } else { ?>
                                <img src="<?php echo base_url(); ?>librerias/timthumb.php?src=<?php echo $path_assets.'img/default.png'?>&w=500&h=281" alt="toqueeltimbre" title="toqueeltimbre">
                            <?php } ?>
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h4><?php echo $project->inm_nombre; ?></h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <small><?php echo $project->ciu_nombre; ?></small>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <small><strong><?php echo ucwords(strtolower($project->for_descripcion)); ?></strong></small>
                    </div>
                    <div class="col-md-6 text_right">
                        <small><strong><?php echo $project->mon_abreviado; ?><?php echo number_format($project->inm_precio, 0, ",", "."); ?></strong></small>
                    </div>
                </div>

            </div>
            <div class="divisor"></div>
        <?php endforeach; ?>

    </aside>
    <!-- End Accordion -->


    </div>
    <!-- End Aside -->
</div>
</div>
</section>
<!-- End content info-->