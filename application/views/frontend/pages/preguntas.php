<?php
$path_assets = base_url() . "assets/";
?>

<div id="layout-preguntas">
<!-- Section Title -->
<div class="section_title about">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h1>PREGUNTAS
                    <span><a href="<?php echo base_url(); ?>">Home </a> / Preguntas Frecuentes</span>
                </h1>
            </div>
        </div>
    </div>
</div>
<!-- End Section Title -->

<!-- End content info -->
<section class="content_info" style="padding-top: 0 !important;">
<div class="container">


<div class="row">
    <div class="col-md-12">
        <!-- Title-->
        <div class="titles">
            <h1>Preguntas Frecuentes</h1>
        </div>
        <!-- End Title-->

        <p><?php echo $contenido->con_descripcion_en; ?></p>

        <div class="accordion-trigger">¿Qué es TOQUEELTIMBRE.COM?</div>
        <div class="accordion-container">
            <p>Toqueeltimbre es una herramienta digital que facilita la intermediación inmobiliaria. Eso significa menor tiempo, mayor eficiencia en el proceso y por ende costos mas bajos para el buscador y  para el anunciante.</p>
        </div>

        <div class="accordion-trigger">¿Cual es el costo por buscar mi inmueble en toqueeltimbre?</div>
        <div class="accordion-container">
            <p>Buscar un inmueble en Toqueeltimbre no tiene ningún costo, el contacto es directo con el anunciante.</p>
        </div>

        <div class="accordion-trigger">¿Como realizo mi búsqueda?</div>
        <div class="accordion-container">
            <p>Para buscar un inmueble en toqueeltimbre, lo único que debes hacer es entrar en la pagina principal, elegir el tipo de inmueble que estas buscando, la transacción que deseas realizar y la ciudad que quieres tu inmueble. Solo tienes que darle buscar y elige tu futuro inmueble contactando al anunciante.</p>
        </div>

        <div class="accordion-trigger">¿Son una inmobiliaria?</div>
        <div class="accordion-container">
            <p>No, somos un intermediario del anunciante y el demandante de inmueble. El contacto es directo con el propietario, agente inmobiliario o intermediarios.</p>
        </div>

        <div class="accordion-trigger">¿Cual es el costo por Publicar mi inmueble en toqueeltimbre?</div>
        <div class="accordion-container">
            <p>Se puede publicar en Toqueeltimbre como usuario particular con un costo de  1 Bs. Diario.</p>
            <p>También puedes publicar como Inmobiliaria certificada y publicar tus proyectos inmobiliarios. Contactarse con nuestros ejecutivos para mayor información.</p>
        </div>

        <div class="accordion-trigger">¿Cómo puedo crear un anuncio?</div>
        <div class="accordion-container">
            <p>Para crear su anuncio debe:</p>
            <ol>
                <li>Registrarse, mediante Facebook o de manera directa en nuestro portal (1 minuto)</li>
                <li>Crea tu nuevo anuncio, solo necesitas 2 cosas, imágenes e información básica en mano (dormitorios, mts2, etc.), si estas creando tu anuncio desde tu celular o cualquier otro dispositivo móvil, podrás cargar tus imágenes directamente.</li>
                <li>Luego debes pagar tu anuncio en cualquiera de los más de 350 <a href="<?php echo base_url(); ?>formas-de-pago">puntos de pago</a>, luego de haber sido realizado el pago, tu anuncio será activado de manera inmediata.</li>
            </ol>
        </div>

        <div class="accordion-trigger">¿Dónde debo registrarme?</div>
        <div class="accordion-container">
            <p>Para ser parte de la nueva era de buscadores  inteligentes contamos con 2 métodos para que puedas registrarte</p>
            <p>En la parte superior derecha encontrarás el botón ¨Iniciar Sesión¨, debes hacer clic  y haz clic en ¨Regístrate Ya¨</p>
            <ol>
                <li>Mediante Facebook, lo único que debes hacer es darle clic en el icono de Facebook, colocar que permites hacer login con Facebook y rellenar los datos restantes de la plantilla de los datos de solicitud.</li>
                <li>Mediante nuestro Portal, debes llenar la planilla con tus datos solicitados.</li>
            </ol>
            <p>Listo!!! Ya eres parte de Toqueeltimbre.com </p>
        </div>

        <div class="accordion-trigger">¿Cuánto es la capacidad permitida y que formatos de imagen debo subir?</div>
        <div class="accordion-container">
            <p>Las imagenes no deben pesar mas de 2 MB, los formatos permitidos son JPG, PNG Y GIF.</p>
        </div>

        <div class="accordion-trigger">¿Cuántas imágenes pueden ser cargadas por anuncio?</div>
        <div class="accordion-container">
            <p>El numero de imagenes o fotos es de 10 por cada anuncio.</p>
        </div>

        <div class="accordion-trigger">¿Dónde realizó el pago de mi anuncio?</div>
        <div class="accordion-container">
            <p>Sabemos que el tiempo es dinero, es por ello que te ofrecemos distintas formas para realizar tus pagos.</p>
            <p>Pagos net mas de 350 puntos de pago, Depósitos o Transferencia Bancaria y si estas en Santa Cruz en efectivo o cheque en nuestras oficinas.</p>
            <p>Ver <a href="<?php echo base_url(); ?>formas-de-pago">Formas de Pago</a></p>
        </div>

        <div class="accordion-trigger">¿Cómo puedo destacar mi anuncio?</div>
        <div class="accordion-container">
            <p>Para destacar en un anuncio nuevo: Debes elegir de que maneras quieres destacar tu anuncio (pagina principal y/o primero lugares de búsqueda)  y el tiempo el cual quieres destacar tu anuncio cuando termines de crear tu anuncio.</p>
            <p>Para destacar un anuncio creado anteriormente: Entrar a tu cuenta, elegir en la parte izquierda “Mis Anuncios” y darle clic en la opción “Mejorar Anuncio” el cual deseas destacar. Luego debes elegir de que maneras quieres destacar tu anuncio (pagina principal y/o primero lugares de búsqueda)  y el tiempo el cual quieres destacar tu anuncio. Landing anuncios destacados.</p>
            <p>Cualquiera sea el anuncio debes elegir la manera de pago (formas de Pagos) que desees realizar.</p>
        </div>

        <div class="accordion-trigger">Cuento con un proyecto Inmobiliario, ¿cómo puedo publicarlo?</div>
        <div class="accordion-container">
            <p>Toqueeltimbre.com cuenta con diversos servicios diseñados para cada una de las necesidades requerida. Nuestro servicio PERFIL PARA PROYECTO INMOBILIARIO te permite contar con un espacio personalizado y único dentro de nuestro portal donde puedes indicar a detalle todos los beneficios (con imágenes, videos, planos de distribución, acabados, inversión en redes sociales, etc.) que hacen de tu proyecto una de las mejores opciones para adquirir ya sea que proporcione una mejor calidad de vida o una interesante inversión. Cabe resaltar que el contacto es DIRECTO de las personas interesadas con la persona que se encuentra ofertando. Si deseas mayor información contáctanos al 591 78526002.</p>
        </div>


    </div>

</div>




<!-- Features -->
<div class="row">
    <!-- Title-->
    <div class="col-md-12">
        <!-- Divisor-->
        <div class="divisor">
            <div class="circle_left"></div>
            <div class="circle_right"></div>
        </div>
        <!-- End Divisor-->
    </div>
    <!-- End Title-->
</div>
<!-- End Features -->

</div>
</section>
<!-- End content info-->
</div>