<?php
$path_assets = base_url() . "assets/";
?>

<div id="layout-pagos">
<!-- Section Title -->
<div class="section_title about">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h1>PRECIOS
                    <span><a href="<?php echo base_url(); ?>">Home </a> / Tabla de Precios</span>
                </h1>
            </div>
        </div>
    </div>
</div>
<!-- End Section Title -->

<!-- End content info -->
<section class="content_info" style="padding-top: 0 !important;">
<div class="container">


<div class="row">
    <div class="col-md-12">

        <!-- Title-->
        <div class="titles">
            <h1>Tabla de Precios</h1>
        </div>
        <!-- End Title-->

        <div style="text-align: justify;">
            <p>Sabemos que en el rubro inmobiliario el tiempo es dinero, es por ello que implementamos nuestros nuevos servicios premium donde podrás mejorar el rendimiento de tus anuncios llegando en el momento oportuno a esa persona que esta en busca de un inmueble como el tuyo!</p>
            <p>En toqueeltimbre tenemos mas de 1,200 inmuebles, mas de 2,000 visitas diarias que se conectan todos los días con anunciastes de todo el país.</p>
            <p>Proba nuestros anuncios Premium y descubro una forma inteligente y dinámica de ofrecer tu inmueble!</p>
        </div>

        <style>
            .tabla_precios td{ border: 1px #ccc solid; text-align: center; font-size: 15px; padding: 8px 0; }
            .tabla_precios th{ background: rgba(0, 173, 238, 1) !important; color: #fff; font-size: 18px; font-weight: bold; text-align: center; padding: 15px 0; }
        </style>
        <div class="tabla_precios">
        <table width="100%">
            <tr>
                <th width="30%"></th>
                <th width="35%">Página Principal</th>
                <th width="35%">Primeros lugares en busquedas</th>
            </tr>
            <tr>
                <td>7 dias</td>
                <td>Bs. 60.-</td>
                <td>Bs. 15.-</td>
            </tr>
            <tr>
                <td>14 dias</td>
                <td>Bs. 115.-</td>
                <td>Bs. 28.-</td>
            </tr>
            <tr>
                <td>21 dias</td>
                <td>Bs. 160.-</td>
                <td>Bs. 40.-</td>
            </tr>
            <tr>
                <td>30 dias</td>
                <td>Bs. 210.-</td>
                <td>Bs. 53.-</td>
            </tr>
        </table>

        <div class="row" style="margin-top: 15px;">
            <div class="col-md-3"></div>
            <div class="col-md-3"><img src="<?php echo $path_assets; ?>img/precio_principal.jpg" style="width: 100%; border: 1px #ccc solid;" /></div>
            <div class="col-md-3"><img src="<?php echo $path_assets; ?>img/precio_busquedas.jpg" style="width: 100%; border: 1px #ccc solid;" /></div>
            <div class="col-md-3"></div>
        </div>

        </div>

    </div>

</div>




<!-- Features -->
<div class="row">
    <!-- Title-->
    <div class="col-md-12">
        <!-- Divisor-->
        <div class="divisor">
            <div class="circle_left"></div>
            <div class="circle_right"></div>
        </div>
        <!-- End Divisor-->
    </div>
    <!-- End Title-->
</div>
<!-- End Features -->

</div>
</section>
<!-- End content info-->
</div>