<?php
$path_assets = base_url() . "assets/";
?>

<div id="layout-pagos">
<!-- Section Title -->
<div class="section_title about">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h1>PAGOS
                    <span><a href="<?php echo base_url(); ?>">Home </a> / Formas de Pago</span>
                </h1>
            </div>
        </div>
    </div>
</div>
<!-- End Section Title -->

<!-- End content info -->
<section class="content_info" style="padding-top: 0 !important;">
<div class="container">


<div class="row">
    <div class="col-md-12">

        <!-- Title-->
        <div class="titles">
            <h1><?php echo $contenido->con_titulo; ?></h1>
        </div>
        <!-- End Title-->

        <div style="text-align: justify;">
            <?php
            $description = str_replace("http","https",$contenido->con_descripcion_en);
            echo $description;
            ?>
        </div>

        <!--<div class="col-md-8" style="margin:0 auto !important; position: relative; float: none; padding: 0 !important;"><center><img src="<?php echo $path_assets; ?>img/puntos_pagos.jpg" /></center></div>-->

    </div>

</div>




<!-- Features -->
<div class="row">
    <!-- Title-->
    <div class="col-md-12">
        <!-- Divisor-->
        <div class="divisor">
            <div class="circle_left"></div>
            <div class="circle_right"></div>
        </div>
        <!-- End Divisor-->
    </div>
    <!-- End Title-->
</div>
<!-- End Features -->

</div>
</section>
<!-- End content info-->
</div>