<?php
$path_assets = base_url() . "assets/";
?>

<div id="layout-pagos">
<!-- Section Title -->
<div class="section_title about" style="height: 0; padding: 0 !important;">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <!--<h1>PUBLICAR
                    <span><a href="<?php echo base_url(); ?>">Home </a> / Inmobiliarias</span>
                </h1>-->
            </div>
        </div>
    </div>
</div>
<!-- End Section Title -->

<!-- End content info -->
<section class="content_info" style="padding-top: 0 !important;">
<div class="container">


<div class="row">
    <div class="col-md-12">
        <div style="margin: 8px 0;">
            <img src="<?=base_url("assets/img/agente_certificados.png")?>" style="width: 100%; max-width:900px" />
        </div>

        <!-- Title-->
        <div class="titles">
            <h1><?php echo $contenido->con_titulo; ?></h1>
        </div>
        <!-- End Title-->

        <div style="text-align: justify;">
            <?php
                $description = str_replace("http","https",$contenido->con_descripcion_en);
            echo $description;
            ?>
        </div>

        <center>
            <a href="<?=base_url("contacto-agente")?>" class="button">Aplicar Ahora!</a>
        </center>

    </div>

</div>

<!-- Features -->
<div class="row">
    <!-- Title-->
    <div class="col-md-12">
        <!-- Divisor-->
        <div class="divisor">
            <div class="circle_left"></div>
            <div class="circle_right"></div>
        </div>
        <!-- End Divisor-->
    </div>
    <!-- End Title-->
</div>
<!-- End Features -->

	<div class="row padding_top_mini hide">
		<!-- Title-->
		<div class="col-md-12">
			<!-- Title-->
			<div class="titles">
				<h1>Inmobiliarias Certificadas</h1>
			</div>
			<!-- End Title-->
		</div>
		<!--End Title-->

        <?php
        if(count($inmobiliarias) > 0)
        {
            foreach($inmobiliarias as $inmobiliaria)
            {
			?>
				<!-- Item Team-->
				<div class="col-xs-6 col-sm-3 col-md-3 col-lg-2">
					<div class="item_team">
						<div class="image_team">
                            <?php
                            $fileHandler = new File_Handler($inmobiliaria->usu_foto);
                            $thumbnail = $fileHandler->getThumbnail(271,183,$publication->inm_nombre);
                            $realStateName = !is_null($inmobiliaria->usu_empresa) && $inmobiliaria->usu_empresa != ""?$inmobiliaria->usu_empresa:$inmobiliaria->usu_nombre;
                            $profileUrl = base_url("perfil/".$inmobiliaria->usu_id."/".seo_url($realStateName));
                            ?>
                            <a href="<?=$profileUrl?>">
                                <img src="<?=$thumbnail->getSource()?>" alt="<?=$thumbnail->getAlternateText()?>" title="<?=$thumbnail->getTitle()?>">
                            </a>
						</div>
					</div>
				</div>
				<!-- End Item Team-->
			<?php
            }
        }
        ?>
	</div>
</div>
</section>
<!-- End content info-->
</div>