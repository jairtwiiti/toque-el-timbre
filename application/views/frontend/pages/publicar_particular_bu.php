<?php
$path_assets = base_url() . "assets/";
?>

<div id="layout-pagos">
<!-- Section Title -->
<div class="section_title about" style="height: 0; padding: 0;">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <!--<h1>PUBLICAR
                    <span><a href="<?php echo base_url(); ?>">Home </a> / Particulares</span>
                </h1>-->
            </div>
        </div>
    </div>
</div>
<!-- End Section Title -->

<!-- End content info -->
<section class="content_info" style="padding-top: 0 !important;">
<div class="container">


<div class="row">
    <div class="col-md-12">
        <div style="margin: 8px 0;">
            <img src="http://toqueeltimbre.com/assets/img/banner_particular.png" style="width: 100%;" />
        </div>

        <!-- Title-->
        <div class="titles">
            <h1><?php echo $contenido->con_titulo; ?></h1>
        </div>
        <!-- End Title-->

        <div style="text-align: justify;">
            <?php echo $contenido->con_descripcion_en; ?>
        </div>

        <center>
            <a href="<?php echo base_url(); ?>login" class="button">¡Crear mi Anuncio!</a>
        </center>

    </div>

</div>

<!-- Features -->
<div class="row">
    <!-- Title-->
    <div class="col-md-12">
        <!-- Divisor-->
        <div class="divisor">
            <div class="circle_left"></div>
            <div class="circle_right"></div>
        </div>
        <!-- End Divisor-->
    </div>
    <!-- End Title-->
</div>
<!-- End Features -->

</div>
</section>
<!-- End content info-->
</div>