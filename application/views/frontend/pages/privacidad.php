<?php
$path_assets = base_url() . "assets/";
?>

<div id="layout-privacidad">
<!-- Section Title -->
<div class="section_title about">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h1>PRIVACIDAD
                    <span><a href="<?php echo base_url(); ?>">Home </a> / Politicas de Privacidad</span>
                </h1>
            </div>
        </div>
    </div>
</div>
<!-- End Section Title -->

<!-- End content info -->
<section class="content_info" style="padding-top: 0 !important;">
<div class="container">


<div class="row">
    <div class="col-md-12">

        <!-- Title-->
        <div class="titles">
            <h1><?php echo $contenido->con_titulo; ?></h1>
        </div>
        <!-- End Title-->

        <div style="text-align: justify;">
            <?php echo $contenido->con_descripcion_en; ?>
        </div>

    </div>

</div>




<!-- Features -->
<div class="row">
    <!-- Title-->
    <div class="col-md-12">
        <!-- Divisor-->
        <div class="divisor">
            <div class="circle_left"></div>
            <div class="circle_right"></div>
        </div>
        <!-- End Divisor-->
    </div>
    <!-- End Title-->
</div>
<!-- End Features -->

</div>
</section>
<!-- End content info-->
</div>