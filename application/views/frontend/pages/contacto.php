<?php
$path_assets = base_url() . "assets/";
?>
<div id="layout-contact">
<!-- Section Title -->
<div class="section_title about">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h1>CONTACTENOS
                    <span><a href="<?php echo base_url(); ?>">Home </a> / Contactenos</span>
                </h1>
            </div>
        </div>
    </div>
</div>
<!-- End Section Title -->

<!-- End content info -->
<section class="content_info" style="padding-top: 0 !important;">
    <div class="container">


        <!-- Contact-->
        <div class="row padding_top">
            <div class="col-md-5">
                <h2>FORMULARIO DE CONTACTO</h2>
                <p>Para cualquier duda o consulta, por favor rellene el formulario y le responderemos lo antes posible.</p>
                <div id="result">
                    <?php echo $mensaje; ?>
                    <?php echo validation_errors() ? '<div class="alert alert-danger">'.validation_errors().'</div>':''; ?>
                </div>
                <form id="form-contact" class="contact-us-form" action="<?php echo base_url(); ?>contactenos" method="post" data-parsley-validate>
                    <input type="text" placeholder="Nombre" name="name" required>
                    <input type="email" placeholder="Email"  name="email" required>
                    <input type="number" placeholder="Telefono"  name="phone" required>
                    <select name="city" required>
                        <option value="">Seleccionar Ciudad</option>
                        <option value="Santa Cruz">Santa Cruz</option>
                        <option value="Cochabamba">Cochabamba</option>
                        <option value="La Paz">La Paz</option>
                        <option value="Beni">Beni</option>
                        <option value="Pando">Pando</option>
                        <option value="Chuquisaca">Chuquisaca</option>
                        <option value="Tarija">Tarija</option>
                        <option value="Oruro">Oruro</option>
                        <option value="Potosi">Potosi</option>
                    </select>
                    <textarea placeholder="Tu mensaje..." name="message" required></textarea>
                    <div id='recaptcha' class="g-recaptcha"
                         data-sitekey="<?=$reCaptchaKeys["publicKey"]?>"
                         data-callback="sendRequestContactForm"
                         data-size="invisible"></div>
                    <input type="button" id="form-contact-us-button" name="Submit" value="Enviar" class="button">
                </form>
            </div>
            <div class="col-md-7">
                <h2>DATOS CONTACTO</h2>
                <p>
                    ToqueElTimbre.com<br/>
                    Calle Mururé 2010, AV. Beni <br />
                    Santa Cruz de la Sierra<br/>
                    (591) 78526002<br />
                    info@toqueeltimbre.com
                </p>

                <!-- Divisor-->
                <div class="divisor divisor_services margin_top">
                    <div class="circle_left"></div>
                    <div class="circle_right"></div>
                </div>
                <!-- End Divisor-->

                <div class="row" style="margin-top: 15px;">
                    <div class="col-sm-6 col-md-12">
<!--                        <iframe src="--><?php //echo base_url(); ?><!--pages/mapa?latitud=-17.765374&longitud=-63.176297&zoom=17" frameborder="0"></iframe>-->
                        <div class="map-fancy-framework">
                            <div id="maps" style="height: 300px;width: auto">
                            </div>
                            <input name="latitude" value="-17.765374" type="hidden" required>
                            <input name="longitude" value="-63.176297" type="hidden" required data-parsley-errors-container="#map-location-error-message-parsley" data-parsley-error-message="Debe marcar un punto en el mapa">
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- End Contact-->
    </div>
</section>
<!-- End content info-->
</div>
