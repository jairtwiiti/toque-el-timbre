<?php
$path_assets = base_url() . "assets/";
?>
<!-- Section Title -->
<div class="section_title about">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h1>NOSOTROS
                    <span><a href="<?php echo base_url(); ?>">Home </a> / Quienes Somos</span>
                </h1>
            </div>
        </div>
    </div>
</div>
<!-- End Section Title -->

<!-- Newsletter Box -->
<div class="newsletter_box">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>
                    SOMOS EL PRIMER BUSCADOR DE INMUEBLES DE BOLIVIA
                </h3>
            </div>
        </div>
    </div>
</div>
<!-- End Newsletter Box -->

<!-- End content info -->
<section class="content_info">
<div class="container">


<div class="row">
    <div class="col-md-3">
        <div class="titles padding_top_mini" style="text-align: center;">
            <img src="<?php echo $path_assets; ?>img/icons/icon-toque.png" />
        </div>
    </div>
    <div class="col-md-9">
        <!-- Title-->
        <div class="titles">
            <h1><?php echo $contenido->con_titulo; ?></h1>
        </div>
        <!-- End Title-->

        <div class="lead">
            <?php echo $contenido->con_descripcion_en; ?>
        </div>

    </div>
</div>

<!-- Features -->
<div class="row">
    <!-- Title-->
    <div class="col-md-12">
        <!-- Divisor-->
        <div class="divisor">
            <div class="circle_left"></div>
            <div class="circle_right"></div>
        </div>
        <!-- End Divisor-->
    </div>
    <!-- End Title-->
</div>
<!-- End Features -->

</div>
</section>
<!-- End content info-->