<?php
/**
 * Created by PhpStorm.
 * User: Christian
 * Date: 24/10/2014
 * Time: 10:31
 */

$path_assets = base_url() . "assets/";
//$filter_config = json_decode(urldecode($_GET["q"]));
$filter_config = $config_filter;
$aux_search = $filter_config->search;
$aux_in = $filter_config->in;
$aux_type = $filter_config->type;
$aux_city = $filter_config->city;
$aux_room = $filter_config->room;
$aux_bathroom = $filter_config->bathroom;
$aux_parking = $filter_config->parking;
$aux_currency = $filter_config->currency;
//$url_search = base_url() . "buscar/?q=";
$backToTheSearchMobile = $this->session->userdata("backToTheSearchMobile");
$offsetToStart = $this->session->userdata("offsetLastSearchMobile");
$lastPropertyIdViewed = $this->session->userdata("lastPropertyIdViewed");
?>
<div id="layout-search-mobile">

<!-- End content info -->
<section class="content_info content_info_search padding-0-mobile" style="border-top: 5px solid #00aeef;">
<div class="container">

<!--<div class="row">
    <div class="col-md-12">
        <div id="map" style="width: auto; height: 400px;"></div>
    </div>
</div>-->

<div class="row">

<!-- Aside -->
<div class="col-md-12" id="search_container" style="display: none;">
    <!-- Search Advance -->
    <aside>
        <div class="search_advance">

            <ul class="tabs_services">
                <li><a id="1" class="set2">Mejora tu búsqueda</a></li>
            </ul>

            <!-- 1-content -->
            <div id="1-content" class="set2 show">
                <div class="search_box">
                    <form action="<?php echo base_url() . "buscar" ?>" method="post">
                        <!--<div class="">
                            <input type="text" name="search" value="<?php echo $aux_search; ?>" placeholder="Buscar...">
                        </div>-->
                        <div>
                            <label>Estoy buscando...</label>
                            <select name="type">
                                <option value="Inmuebles">Todas los tipos</option>
                                <?php foreach($categories as $cat): ?>
                                    <?php $valor = ucwords(strtolower($cat->cat_nombre)); ?>
                                    <option value="<?php echo $valor; ?>" <?php echo $aux_type == $valor ? 'selected' : ''; ?>><?php echo $valor; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div>
                            <label>En la ciudad de...</label>
                            <select name="city">
                                <option value="Bolivia">Todas las ciudades</option>
                                <?php foreach($cities as $city): ?>
                                    <?php $valor = ucwords(strtolower($city->dep_nombre)); ?>
                                    <option value="<?php echo $valor; ?>" <?php echo $aux_city == $valor ? 'selected' : ''; ?>><?php echo $valor; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div>
                            <label>En...</label>
                            <select name="in">
                                <?php foreach($forma as $form): ?>
                                    <?php $valor = ucwords(strtolower($form->for_descripcion)); ?>
                                    <option value="<?php echo $valor; ?>" <?php echo $aux_in == $valor ? 'selected' : ''; ?>><?php echo $valor; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div >
                            <label>Habitaciones</label>
                            <select name="room">
                                <option value="">No Definido</option>
                                <option value="1" <?php echo $aux_room == "1" ? 'selected' : ''; ?>>1</option>
                                <option value="2" <?php echo $aux_room == "2" ? 'selected' : ''; ?>>2</option>
                                <option value="3" <?php echo $aux_room == "3" ? 'selected' : ''; ?>>3</option>
                                <option value="4" <?php echo $aux_room == "4" ? 'selected' : ''; ?>>4</option>
                                <option value="5+" <?php echo $aux_room == "5+" ? 'selected' : ''; ?>>5+</option>
                            </select>
                        </div>
                        <div >
                            <label>Baños</label>
                            <select name="bathroom">
                                <option value="">No Definido</option>
                                <option value="1" <?php echo $aux_bathroom == "1" ? 'selected' : ''; ?>>1</option>
                                <option value="2" <?php echo $aux_bathroom == "2" ? 'selected' : ''; ?>>2</option>
                                <option value="3" <?php echo $aux_bathroom == "3" ? 'selected' : ''; ?>>3</option>
                                <option value="4" <?php echo $aux_bathroom == "4" ? 'selected' : ''; ?>>4</option>
                                <option value="5+" <?php echo $aux_bathroom == "5+" ? 'selected' : ''; ?>>5+</option>
                            </select>
                        </div>
                        <div >
                            <label>Parqueo</label>
                            <select name="parking">
                                <option value="">No Definido</option>
                                <option value="1" <?php echo $aux_parking == "1" ? 'selected' : ''; ?>>1</option>
                                <option value="2" <?php echo $aux_parking == "2" ? 'selected' : ''; ?>>2</option>
                                <option value="3" <?php echo $aux_parking == "3" ? 'selected' : ''; ?>>3</option>
                                <option value="4" <?php echo $aux_parking == "4" ? 'selected' : ''; ?>>4</option>
                                <option value="5+" <?php echo $aux_parking == "5+" ? 'selected' : ''; ?>>5+</option>
                            </select>
                        </div>
                        <div >
                            <label>Moneda</label>
                            <div class="switch-wrapper">
                                <input type="checkbox" name="currency" value="1" <?php echo ($aux_currency == "Dolares" || empty($aux_currency)) ? 'checked' : ''; ?>>
                            </div>
                        </div>
                        <div>
                            <p>
                                <label for="amount">Precios:</label>
                                <input type="text" id="amount" readonly style="border:0; color:#f6931f; font-weight:bold;">
                            </p>
                            <div id="slider-range"></div>
                        </div>
                        <div>
                            <input type="hidden" name="price_min" id="price_min" value="" />
                            <input type="hidden" name="price_max" id="price_max" value="" />
                            <input type="hidden" name="search" id="search" value="<?php echo $aux_search; ?>" />
                            <input type="hidden" name="currency" id="currency" value="Dolares" />
                            <input type="button" id="advanced-search-form" class="button" style="background-color: rgba(0, 174, 239, 1) !important; margin-top: 25px; font-weight: 600;" value="Mostrar Resultados">
                            <a href="<?php echo base_url(); ?>buscar/limpiar" class="button" style="background-color: rgba(0, 174, 239, 1) !important; width: 100%; display:block; margin-top: 0px; font-weight: 600; text-align: center;">Limpiar Filtros <?=$lastPropertyIdViewed?></a>
                        </div>
                    </form>
                </div>
            </div>
            <!-- End 1-content -->

        </div>
    </aside>
    <!-- End Search Advance -->


</div>
<!-- End Aside -->



<!-- property List-->
<div class="properties_two">
    <!-- Row Propertys-->
    <div id="results_publications" class="row" style="padding-top: 15px;">
        <!--<div class="col-md-12">
            <h1 style="margin-bottom: 10px;">Resultados de la Busqueda (<?php echo $total_rows > 0 ? $total_rows : 0; ?>)</h1>
        </div>-->
    </div>
    <!-- Row Propertys-->
</div>
<!-- End property List-->



</div>
</div>
</section>
<!-- End content info-->



<div class="modal_detail">
    <div class="modal_close">
        <div style="margin: 8px 10px;"><img src="<?php echo $path_assets; ?>img/icons/close.png" width="25" /> Volver Atras</div>
    </div>
    <div class="modal_content"></div>
</div>

</div>
<!-- End layout-->

</div>

<!-- ======================= JQuery libs =========================== -->
<!-- Core JS Libraries -->
<script src="<?php echo $path_assets; ?>js/jquery.min.js"></script>
<script src="<?php echo $path_assets; ?>js/URL_Builder.js"></script>
<script src="<?php echo $path_assets; ?>js/parsley.min.js"></script>
<script src="<?php echo $path_assets; ?>js/parsley.spanish.js"></script>
<!--Nav-->
<script type="text/javascript" src="<?php echo $path_assets; ?>js/nav/tinynav.js"></script>
<script type="text/javascript" src="<?php echo $path_assets; ?>js/nav/superfish.js"></script>
<script type="text/javascript" src="<?php echo $path_assets; ?>js/nav/hoverIntent.js"></script>
<!--Totop-->
<script type="text/javascript" src="<?php echo $path_assets; ?>js/totop/jquery.ui.totop.js" ></script>
<!--Slide-->
<script type="text/javascript" src="<?php echo $path_assets; ?>js/slide/camera.js" ></script>
<script type='text/javascript' src='<?php echo $path_assets; ?>js/slide/jquery.easing.1.3.min.js'></script>
<!--owlcarousel-->
<script type='text/javascript' src='<?php echo $path_assets; ?>js/owlcarousel/owl.carousel.js'></script>
<!--efect_switcher-->
<script type='text/javascript' src='<?php echo $path_assets; ?>js/efect_switcher/jquery.content-panel-switcher.js'></script>
<!--Theme Options-->
<script type="text/javascript" src="<?php echo $path_assets; ?>js/theme-options/theme-options.js"></script>
<script type="text/javascript" src="<?php echo $path_assets; ?>js/theme-options/jquery.cookies.js"></script>
<!-- Bootstrap.js-->
<script src="<?php echo $path_assets; ?>js/bootstrap/bootstrap.js"></script>

<script type="text/javascript" src="<?php echo $path_assets; ?>js/animate.js" ></script>


<script src="<?php echo $path_assets; ?>js/jquery-ui.js"></script>
<script src="<?php echo $path_assets; ?>js/jquery.ui.touch-punch.min.js"></script>
<script src="<?php echo $path_assets; ?>js/jquery.switchButton.js"></script>
<script src="<?php echo $path_assets; ?>js/numeral.js"></script>
<script src="<?php echo $path_assets; ?>js/pagination_ajax.js"></script>
<script src="<?php echo $path_assets; ?>js/idangerous.swiper.min.js"></script>

<script src="<?php echo $path_assets; ?>js/jquery.colorbox-min.js"></script>
<script src="<?php echo $path_assets; ?>js/jQuery.print.js"></script>

<style>
/* Demo Styles */
.swiper-container {
    width: 100%;
    height: 233px;
    color: #fff;
    text-align: center;
}

.swiper-pagination-switch {
    display: inline-block;
    width: 8px;
    height: 8px;
    border-radius: 8px;
    background: #555;
    margin-right: 5px;
    opacity: 0.8;
    border: 1px solid #fff;
    cursor: pointer;
}
.swiper-active-switch {
    background: #fff;
}
.swiper-dynamic-links {
    text-align: center;
}
.swiper-dynamic-links a {
    display: inline-block;
    padding: 5px;
    border-radius: 3px;
    border: 1px solid #ccc;
    margin: 5px;
    font-size: 12px;
    text-decoration: none;
    color: #333;
    background: #eee;
}
.swiper-slide{
    width: 100%;
}
.head_property img{
    border: 0;
}
.info_property{
    border: 0;
}

</style>

<?php
//$filter = json_decode(urldecode($_GET["q"]));
$filter = $config_filter;
?>
<script>


$(document).on("ajaxComplete",function(){
    $('.swiper-container').each(function () {
        var mySwiper = new Swiper(this,{
            paginationClickable: false
        })
    });
});

$(document).ready(function($) {
    $("#search_button").click(function() {
        $("#search_container").toggle("fade");
    });

    $(".modal_close").click(function() {
        $(".modal_detail").hide();
        $(".modal_detail .modal_content").html("");
        $("body").css("overflow-y", "scroll");
        $(".modal_close").hide();
    });
    //On mobile the publishment is pull via ajax
    /*$('#results_publications').on('click', '.swiper-slide a', function(){
        var inm_id = $(this).attr("rel");
        var $content = $(".modal_detail .modal_content");
        $("body").css("overflow-y", "hidden");
        $.ajax({
            type: 'POST',
            url: "<?php echo base_url(); ?>" + "inmueble/detail_mobile",
            data: { id_inmueble: inm_id },
            beforeSend: function() {
                $(".modal_detail").show();
                $content.html("<center style='padding-top:20px;'><img src='<?php echo $path_assets;  ?>img/loading.gif' /></center>");
            },
            success: function(data) {
                $(".modal_close").show();
                $content.html(data);
            }
        });
    });*/
    //The publishment are showing using ajax scrolling pagination
    $('#results_publications').scrollPagination({
        nop     : 1, // The number of posts per scroll to be loaded
        offset  : 0, // Initial offset, begins at 0 in this case
        error   : 'No hay mas resultados!', // When the user reaches the end this is the message that is
        delay   : 500, // When you scroll down the posts will load after a delayed amount of time.
        scroll  : true, // The main bit, if set to false posts will not load as the user scrolls.
        url     : '<?php echo base_url(). "search/ajax_pagination"; ?>',
        loading : '<?php echo $path_assets. "img/loading.gif"; ?>',
        goToItem: '<?=!is_null($lastPropertyIdViewed) && $lastPropertyIdViewed != ""?"div[data-property-id=".$lastPropertyIdViewed."]":"";?>'
    });
});
</script>


<script>

$(document).ready(function($) {


    //=================================== Slider Range  ===================================//
    $( "#slider-range" ).slider({
        range: true,
        min: 0,
        <?php if($filter->in == "Alquiler"){ ?>
        max: 5000,
        <?php
        if($filter->price_min != null && $filter->price_max != null){
            echo 'values: [ '.$filter->price_min.', '.$filter->price_max.' ],';
        }else{
            echo 'values: [ 0, 5000 ],';
        }
        ?>
        step: 100,
        <?php }elseif($filter->in == "Anticretico"){ ?>
        max: 150000,
        <?php
        if($filter->price_min != null && $filter->price_max != null){
            echo 'values: [ '.$filter->price_min.', '.$filter->price_max.' ],';
        }else{
            echo 'values: [ 0, 150000 ],';
        }
        ?>
        step: 1000,
        <?php }elseif($filter->in == "Venta" || $filter->in == null){ ?>
        max: 500000,
        <?php
        if($filter->price_min != null && $filter->price_max != null){
            echo 'values: [ '.$filter->price_min.', '.$filter->price_max.' ],';
        }else{
            echo 'values: [ 0, 500000 ],';
        }
        ?>
        step: 10000,
        <?php } ?>
        slide: function( event, ui ) {
            var string1 = numeral(ui.values[ 0 ]).format('0,0');
            var string2 = numeral(ui.values[ 1 ]).format('0,0');
            if(string2 == "2,000" || string2 == "150,000" || string2 == "500,000"){
                string2 = string2 + "+";
            }
            $("#amount").val( "$" + string1 + " - $" + string2 );
            $("#price_min").val(ui.values[0]);
            $("#price_max").val(ui.values[1]);
        }
    });
    if($("#slider-range").length != 0) {
        var ini_1 = $("#slider-range").slider("values", 0);
        var string1 = numeral(ini_1).format('0,0');
        var ini_2 = $("#slider-range").slider("values", 1);
        var string2 = numeral(ini_2).format('0,0');
        if(string2 == "2,000" || string2 == "150,000" || string2 == "500,000"){
            string2 = string2 + "+";
        }
        $("#amount").val("$" + string1 + " - $" + string2);
        $("#price_min").val(ini_1);
        $("#price_max").val(ini_2);
    }
});
</script>

<!--fUNCTIONS-->
<script type="text/javascript" src="<?php echo $path_assets; ?>js/main.js"></script>

<!-- ======================= End JQuery libs =========================== -->

<?php exit; ?>