<?php
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 22/5/2017
 * Time: 4:27 PM
 */
?>
<aside class="hide">
    <h2>Proyectos</h2>
    <?php
    foreach($projects as $project)
    {
        $directoryFileImage = BASE_DIRECTORY . DS . "admin" . DS . "imagenes" . DS . "proyecto" . DS . $project->proy_fot_imagen;
        $projectImageSource = base_url("librerias/timthumb.php?src=".base_url("assets/img/default.png&w=500&h=281"));
        $title = $alt = "Default toqueeltimbre";
        if(file_exists($directoryFileImage))
        {
            $projectImageSource = base_url("librerias/timthumb.php?src=".base_url("admin/imagenes/proyecto/".$project->proy_fot_imagen."&w=500&h=281"));
            $title = $alt = $project->inm_nombre;
        }

    ?>
    <div class="project_item">
        <div class="row">
            <div class="col-md-12">
                <a href="<?php echo base_url(). $project->proy_seo; ?>">
                    <img src="<?=$projectImageSource?>" alt="<?=$alt?>" title="<?=$title?>">
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h4><?=$project->inm_nombre; ?></h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <small><?=$project->ciu_nombre; ?></small>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <small><strong><?=ucwords(strtolower($project->for_descripcion)); ?></strong></small>
            </div>
            <div class="col-md-6 text_right">
                <small><strong><?=$project->mon_abreviado; ?><?=number_format($project->inm_precio, 0, ",", "."); ?></strong></small>
            </div>
        </div>
    </div>
    <div class="divisor"></div>
    <?php
    }
    ?>
</aside>