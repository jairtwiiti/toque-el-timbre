<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i> Pagos
                </div>
                <div class="tools">
                </div>
            </div>
            <div class="portlet-body form">
                <form id="form_payment" role="form" method="post" enctype="multipart/form-data">
                    <div class="form-body">
                        <div class="form-group">
                            <label>Pago realizado correctamente</label>
                            <div class="row">
                                <div class="col-md-4"></div>
                                <div class="col-md-4" style="text-align: center;">
                                    <h1 class="page-title"><?php echo $pagoId; ?></h1>
                                </div>
                                <div class="col-md-4"></div>
                            </div>
                            <div class="input-group">
                                <div>
                                   Su pago fue efectuado correctamente, por la suma de <strong><?php echo $totalPayment; ?> Bs.</strong> sus anuncios se publicaran automaticamente.
                                </div>
                            </div>

                        </div>
                        <div class="form-group">

                            <div class="portlet box white">
                                <div class="portlet-body form">
                                    <div class="form-body">
                                        <div class="panel-group accordion" id="accordion" role="tablist" aria-multiselectable="true">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading" role="tab" id="headingThree">
                                                        <h4 class="panel-title">
                                                            <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                                TIGO MONEY
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseThree" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
                                                        <div class="panel-body">
                                                            <div id="html_pagos">
                                                                <div class="row">
                                                                    <div class="col-sm-12 col-md-6">
                                                                        <img src="<?php echo base_url(); ?>assets/img/logo-tigo-money.jpg" style="max-width: 450px;" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <input id="paymentTotal" type="hidden" name="paymentTotal" value="">

                    <div class="form-actions">
                        <div class="pull-right">
                            <a class="btn btn-info blue" href="<?php echo base_url(); ?>dashboard/publications" title="Aceptar">Aceptar</a>
                        </div>

                    </div>

	                <a href="<?php echo base_url(); ?>/publicar-proyectos" target="_blank" style="color: inherit; text-decoration: none;">
                    <div class="form-body">
                        <div class="form-group">
                            <label><b>Deseo anunciar un condominio, urbanización, etc.</b></b></label>
                            <br>
                            <div class="texto_condominios">

                                <p>Con este tipo de anuncio podras:</p>

                                <ul>
                                    <li>Tener tu propio timbre (mini sitio web dentro de toqueletimbre.com).</li>
                                    <li>Personalizarlo de acuerdo a tu proyecto.</li>
                                    <li>Publicidad en Facebook Ads (mas de 7 millones de impresiones). </li>
                                    <li>Anuncios en portadas.</li>
                                    <li>Banner lateral.</li>
                                    <li>Primeros lugares en búsqueda.</li>
                                    <li>Reportes semanalaes de visitas.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
	                </a>
                </form>
            </div>
        </div>
        <!-- END SAMPLE FORM PORTLET-->
    </div>
    <div class="col-md-2">

    </div>

</div>
<!-- END PAGE CONTENT-->

<script>
</script>