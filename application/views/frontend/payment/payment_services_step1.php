﻿<?php if(@validation_errors()){ echo '<div class="alert alert-danger">'.@validation_errors().'</div>'; } ?>
<!-- BEGIN PAGE CONTENT-->
<div class="row">
	<div class="col-md-2"></div>
    <div class="col-md-8">
    <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i> Pagos
                </div>
                <div class="tools"></div>
            </div>
	        <div class="portlet-body form">
                <form id="form_payment_step1" role="form" method="post" enctype="multipart/form-data" action="<?= base_url(); ?>publication/payment/<?= $publicationId?>">
                    <div class="form-body">
                        <div class="form-group">
                            <label>Para que tu anuncio sea publico debes cancelar la suma de :</label>
	                        <div class="row">
		                        <div class="col-md-3"></div>
		                        <input type="hidden" value="" name="anuncio" value="">
		                        <?php foreach($particularServices as $service) { ?>
                                    <?php
                                    if($sw_subscription){
                                        $service->ser_precio = $service->ser_precio_suscriptor;
                                    }
                                    ?>
			                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
				                        <div class="dashboard-stat green" style="height: 122px; cursor: pointer" rel="<?php echo $service->ser_id; ?>" data-price="<?php echo $service->ser_precio; ?>">
					                        <div class="visual">
						                        <!-- <i class="fa fa-home"></i> -->
					                        </div>
					                        <div class="details">
						                        <div class="number">
							                        <?php echo $service->ser_precio; ?> Bs
						                        </div>
						                        <div class="desc">

							                        <input type="radio" name="groupradio" value="<?= $service->ser_precio; ?>">

							                        Por <?php echo $service->ser_dias; ?> dias
						                        </div>
					                        </div>
				                        </div>
			                        </div>
		                        <?php } ?>
		                        <div class="col-md-2"></div>
	                        </div>
	                        <br/>
	                        <div class="input-group">
		                        <div>
		                        </div>
	                        </div>
                        </div>
						<div class="form-group">
							<label><b>Puedes destacar tu anuncio publicando en nuestros espacios especiales:</b></label>
							<div class="portlet box white">
								<div class="portlet-body form">

									<div class="form-body">
										<div class="panel-group accordion" id="accordion" role="tablist" aria-multiselectable="true">
											<div class="panel panel-default">
												<div class="panel-heading" role="tab" id="headingOne">
													<h4 class="panel-title">
														<a class="accordion-toggle accordion-toggle-styled " data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-controls="collapseOne">
															<i class="fa fa-arrow-right"></i> DESTACAR ANUNCIO
														</a>
													</h4>
												</div>
												<div id="collapseOne" class="panel-collapse" role="tabpanel" aria-labelledby="headingOne">
													<div class="panel-body">
														<div id="html_pagos">
															<div class="form-body">
																<div class="form-group">
																	<div class="row">
																		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
																			<label >Primeros lugares en resultados de búsqueda</label>
																		</div>
																		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
																			<div class="input-group">
																				<select class="table-group-action-input form-control input-medium select-service" name="busqueda">
																					<option value="0"  data-price="0">Seleccionar...</option>
																					<?php foreach($searchServices as $service) {
																						$serviceCost = $sw_subscription ? $service->ser_precio_suscriptor : $service->ser_precio;
																						?>?>
																						<option value="<?= $service->ser_id?>" data-price="<?= $serviceCost ?>">
																							<?= $service->ser_dias?> días ( Bs. <?= $serviceCost ?> .-)
																						</option>
																					<?php } ?>
																				</select>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="form-group">
																	<div class="row">
																		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
																			<label>Aparecer en pagina principal</label>
																		</div>
																		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
																			<div class="input-group">
																				<select class="table-group-action-input form-control input-medium select-service" name="principal">
																					<option value="0" data-price="0">Seleccionar...</option>
																					<?php foreach($mainServices as $service) {
																							$serviceCost = $sw_subscription ? $service->ser_precio_suscriptor : $service->ser_precio;
																						?>?>
																						<option value="<?= $service->ser_id?>" data-price="<?= $serviceCost ?>">
																							<?= $service->ser_dias?> días (Bs. <?= $serviceCost ?>.-)
																						</option>
																					<?php } ?>
																				</select>
																			</div>
																		</div>
																	</div>
																</div>
																<!--
																<div class="form-group">
																	<div class="row">
																		<div class="col-md-4">
																			<label>Landing page</label>
																		</div>
																		<div class="col-md-6">
																			<div class="input-group">
																				<select class="table-group-action-input form-control input-medium select-service" name="banner">
																					<option value="0" data-price="0">Seleccionar...</option>
																					<?php foreach($bannerServices as $service) {
																							$serviceCost = $sw_subscription ? $service->ser_precio_suscriptor : $service->ser_precio;
																						?>?>
																						<option value="<?= $service->ser_id?>" data-price="<?= $serviceCost ?>">
																							<?= $service->ser_dias?> días (Bs. <?= $serviceCost ?>.-)
																						</option>
																						<?php } ?>
																				</select>
																			</div>
																		</div>
																	</div>
																</div>
																-->
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div style="text-align: right">
										<input id="paymentTotal" type="hidden" name="paymentTotal" value="0">
                                        <input type="hidden" name="concepto" value="<?php echo $concepto; ?>">
										<h4>Total: &nbsp;<span id="total_payment"></span> Bs.</h4>
									</div>

								</div>
							</div>
						</div>

	                    <div class="form-group">
		                    <label><b>DATOS DE FACTURACIÓN</b></label>
		                    <br>
		                    <div class="row">
			                    <div class="col-md-6">
				                    <div class="input-group">
					                    <input type="text" name="userName" class="form-control" placeholder="Nombre Completo" id="userName" required>
																					<span class="input-group-addon">
																						<i class="fa fa-user"></i>
																					</span>
				                    </div>
			                    </div>
			                    <div class="col-md-6">
				                    <div class="input-group">
					                    <input type="text" name="userNit" class="form-control" id="userNit" placeholder="Nro. de NIT/C.I:" required>
																						<span class="input-group-addon">
																							<i class="fa fa-slack"></i>
																						</span>
				                    </div>
			                    </div>
		                    </div>
	                    </div>
                    </div>
	                <div class="form-actions">
		                <div class="pull-right">
			                <!-- <a class="btn btn-info blue" href="<?php echo base_url(); ?>dashboard/publications" title="Aceptar">Pagar</a> -->
			                <input class="btn btn-info blue" type="submit" value="Pagar" name="btnPagar">
		                </div>
	                </div>
                </form>
			</div>
		</div>
        <!-- END SAMPLE FORM PORTLET-->
    </div>
	<div class="col-md-2">

	</div>

</div>
<!-- END PAGE CONTENT-->

<script>
</script>