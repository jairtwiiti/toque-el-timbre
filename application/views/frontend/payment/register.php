﻿<?php if(@validation_errors()){ echo '<div class="alert alert-danger">'.@validation_errors().'</div>'; } ?>
<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-6 ">
    <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i> Pagos
                </div>
                <div class="tools">

                </div>
            </div>
            <div class="portlet-body form">
                <form id="form_payment" role="form" method="post" enctype="multipart/form-data">
                    <div class="form-body">
                        <div class="form-group">
                            <label>Aparecer en pagina principal</label>&nbsp;&nbsp;<span class="form-group-clear principal">Limpiar</span>
                            <div class="input-group">
                                <div class="icheck-list" id="select-radio-principal">
									<?php foreach($mainServices as $service) {
										$serviceCost = $sw_subscription ? $service->ser_precio_suscriptor : $service->ser_precio;
										?>
										<label days="<?= $service->ser_dias?>" cost="<?= $serviceCost ?>">
											<input type="radio" name="principal" class="icheck" data-radio="iradio_square-grey" value="<?= $service->ser_id?>" >
											<?= $service->ser_dias?> días (<?php if($sw_subscription) {?><span style="text-decoration: line-through">Bs. <?= $service->ser_precio ?> </span>, <?php } ?> Bs. <?= $serviceCost ?>.-)
										</label>
									<?php }?>
                                </div>
                            </div>
                        </div>
	                    <!--
                        <div class="form-group">
                            <label>Landing Page</label>&nbsp;&nbsp;<span class="form-group-clear banner">Limpiar</span>
                            <div class="input-group">
                                <div class="icheck-list" id="select-radio-banner">
									<?php foreach($bannerServices as $service) {
										$serviceCost = $sw_subscription ? $service->ser_precio_suscriptor : $service->ser_precio;
										?>
										<label days="<?= $service->ser_dias?>" cost="<?= $serviceCost ?>">
											<input type="radio" name="banner" class="icheck" data-radio="iradio_square-grey" value="<?= $service->ser_id?>">
											<?= $service->ser_dias?> días (Bs. <?= $serviceCost?>.-)
										</label>
									<?php }?>
                                </div>
                            </div>
                        </div>
	                    -->
                        <div class="form-group">
                            <label>Primeros lugares en resultados de búsqueda</label>&nbsp;&nbsp;<span class="form-group-clear busqueda">Limpiar</span>
                            <div class="input-group">
								<div class="icheck-list" id="select-radio-busqueda">
									<?php foreach($searchServices as $service) {
										$serviceCost = $sw_subscription ? $service->ser_precio_suscriptor : $service->ser_precio;
										?>
										<label days="<?= $service->ser_dias?>" cost="<?= $serviceCost ?>">
											<input type="radio" name="busqueda" class="icheck" data-radio="iradio_square-grey" value="<?= $service->ser_id ?>">
											<?= $service->ser_dias?> días (<?php if($sw_subscription) {?><span style="text-decoration: line-through">Bs. <?= $service->ser_precio ?> </span>, <?php } ?> Bs. <?= $serviceCost ?>.-)
										</label>
									<?php }?>
								</div>
                            </div>
                        </div>

						<div class="form-group">
                            <label><b>FORMAS DE PAGO</b></label>
                            <p>Usted puede realizar el pago nuestros servicios por cualquiera de nuestras modalidades de pago: PagosNet, Deposito Bancario o en nuestras oficinas, siempre mencionando o dando como referencia su numero de pago.</p>
                            <p>Todos los precios incluyen IVA. Toqueeltimbre.com S.R.L.</p>

							<div class="portlet box white">
								<div class="portlet-body form">
									<div class="form-body">
										<div class="panel-group accordion" id="accordion" role="tablist" aria-multiselectable="true">
											<input type="hidden" name="paymentType" value="">
											<div class="panel panel-default">
												<div class="panel-heading" role="tab" id="headingOne">
													<h4 class="panel-title">
														<a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" rel="PAGOSNET">
                                                            PAGOSNET (RECOMENDADO)
														</a>
													</h4>
												</div>
												<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
													<div class="panel-body">
                                                        <div id="html_pagos">
                                                            <!-- <p>Temporalmente este servicio no se encuentra disponible, por favor intenta con PAGO EN OFICINA, o TRANSFERENCIA BANCARIA. <br> Mil disculpas por las molestias.</p> -->
                                                            <p>Pagosnet es un servicio de puntos de pago que facilita a los anunciantes de inmuebles realizar su pago en efectivo en mas de 350 puntos autorizados a nivel nacional.</p>
                                                            <p>Usted puede realizar su pago en efectivo en las siguientes entidades financieras:</p>

                                                            <div class="row">
                                                                <div class="col-md-3">
                                                                    <img src="<?php echo base_url(); ?>assets/img/logos_pagosnet/farmacorp.png" />
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <img src="<?php echo base_url(); ?>assets/img/logos_pagosnet/banco_fassil.png" />
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <img src="<?php echo base_url(); ?>assets/img/logos_pagosnet/grigota.png" />
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <img src="<?php echo base_url(); ?>assets/img/logos_pagosnet/x_cobrar.png" />
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-3">
                                                                    <img src="<?php echo base_url(); ?>assets/img/logos_pagosnet/eco_futuro.png" />
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <img src="<?php echo base_url(); ?>assets/img/logos_pagosnet/la_merced.png" />
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <img src="<?php echo base_url(); ?>assets/img/logos_pagosnet/mutual_la_primera.png" />
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <img src="<?php echo base_url(); ?>assets/img/logos_pagosnet/mutual_la_paz.png" />
                                                                </div>
                                                            </div>
                                                        </div>
													</div>
												</div>
											</div>
                                            <!--<div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingFour">
                                                    <h4 class="panel-title">
                                                        <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree" rel="TIGOMONEY">
                                                            TIGO MONEY (RECOMENDADO)
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                                    <div class="panel-body">
                                                        <div id="html_pagos">
                                                            <div class="row">
                                                                <div class="col-sm-12 col-md-6">
                                                                    <img src="<?php echo base_url(); ?>assets/img/logo-tigo-money.jpg" style="max-width: 400px;" />
                                                                </div>
                                                            </div>
                                                            <p style="margin-top: 6px;">Usted puede pagar a travez de su cuenta tigo money, solo ingrese el numero de su cuenta para poder proceder al pago.</p>
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <p><input type="text" name="linea" placeholder="Linea Tigo Money" style="padding: 7px 10px; font-size: 14px; border: 1px #B5B5B5 solid;" /></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>-->
                                            <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingThree">
                                                    <h4 class="panel-title">
                                                        <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree" rel="BANCO">
                                                            DEPÓSITO BANCARIO O TRANSFERENCIA
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                    <div class="panel-body">
                                                        <div id="html_pagos">
                                                            <p>Usted puede pagar mediante transferencia o depósito bancario. Tenemos nuestra cuenta en el Banco Ganadero.</p>
                                                            <p>Si hace una transferencia bancaria mediante banca por internet, por favor coloque el nombre de su empresa en el detalle del pago y envíenos una captura de pantalla a pagos@toqueeltimbre.com. Si realiza un depósito por ventanilla, por favor escanee el comprobante de depósito y envíelo a pagos@toqueeltimbre.com. Una vez verificado su pago, su anuncio será publicado en el plazo de 24 horas hábiles.</p>
                                                            <p>Le enviaremos una factura digital por correo electronico. Usted la debe imprimir y esta le servirá para crédito fiscal.</p>
                                                            <p>NOTA IMPORTANTE: Nos debe informar antes de fin de mes el nombre y NIT para la factura de cualquier depósito o transferencia que realice. Caso contrario, procederemos a emitir su factura sin nombre y sin NIT. El plazo para enviar esta informacion es hasta las 16:00 horas del último día hábil del mes.</p>

                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <p><img src="<?php echo base_url(); ?>assets/img/logos_pagosnet/logo_ganadero.png" /></p>
                                                                    <p>Cuenta Nro 1041157282<br /> A nombre de TOQUEELTIMBRE.COM S.R.L. <br />NIT 222334022</p>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingTwo">
                                                    <h4 class="panel-title">
                                                        <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" rel="OFICINA">
                                                            EFECTIVO Y CHEQUE (SOLO SANTA CRUZ)
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                                    <div class="panel-body">
                                                        <div id="html_pagos">
	                                                        <div class="row">
		                                                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-6">
			                                                        <p>Puedes hacer el pago de tu anuncio en nuestras oficinas (Servicio solo para Santa Cruz)</p>
			                                                        <p>
				                                                        Nuestra oficina esta ubicada en:<br>
				                                                        ToqueElTimbre.com<br>
				                                                        Calle Mururé 2010, AV. Beni <br>
				                                                        Santa Cruz de la Sierra<br>
                                                                        (591) 78526002<br>
				                                                        info@toqueeltimbre.com
			                                                        </p>
		                                                        </div>
		                                                        <div class="col-xs-12 col-sm-6 col-md-8 col-lg-6">
			                                                        <img src="<?php echo base_url()?>assets/img/hubicacion.PNG" style="width: 100%; height: 280px">
		                                                        </div>
	                                                        </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label><b>DATOS DE FACTURACION</b></label>
							<br>
							<!--<label> ¿Nombre para su factura? SI <input type="checkbox" name="invoiced" id="invoiced"></label>-->
							<div class="row">
								<div class="col-md-6">
										<div class="input-group">
											<input type="text" name="userName" class="form-control" placeholder="Nombre Completo" id="userName" required>
											<span class="input-group-addon">
												<i class="fa fa-user"></i>
											</span>
										</div>
								</div>
								<div class="col-md-6">
										<div class="input-group">
											<input type="text" name="userNit" class="form-control" id="userNit" placeholder="Nro. de NIT/C.I:" required>
											<span class="input-group-addon">
												<i class="fa fa-slack"></i>
											</span>
										</div>
								</div>
							</div>
							</div>
					</div>
					<input id="paymentTotal" type="hidden" name="paymentTotal" value="">
					<div class="form-actions">
						<button type="submit" class="btn blue pull-right">Procesar el pago</button>
					</div>
	                <!--
					<div class="form-body">
						<div class="form-group">
							<label><b>Deseo anunciar un condominio, urbanización, etc.</b></b></label>
							<br>
							<div class="texto_condominios">

								<p>Con este tipo de anuncio podras:</p>

								<ul>
									<li>Tener tu propio timbre (mini sitio web dentro de toqueletimbre.com).</li>
									<li>Personalizarlo de acuerdo a tu proyecto.</li>
									<li>Publicidad en Facebook Ads (mas de 7 millones de impresiones). </li>
									<li>Anuncios en portadas.</li>
									<li>Banner lateral.</li>
									<li>Primeros lugares en búsqueda.</li>
									<li>Reportes semanalaes de visitas.</li>
								</ul>
							</div>
						</div>
					</div>
					-->
				</form>
			</div>
		</div>
        <!-- END SAMPLE FORM PORTLET-->
    </div>
	<div class="col-md-6 ">
		<!-- BEGIN SAMPLE FORM PORTLET-->
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-calculator"></i> CARACTERISTICAS
				</div>
				<div class="tools">
				</div>
			</div>
			<div class="portlet-body form">
				<form role="form">
					<div class="form-body">

						<div class="form-group">
							<label class="show-payment-principal">
								- <span class="payment-days">0</span> días en la página principal.
								(Costo: Bs. <span class="payment-cost">0</span>)
							</label>
						</div>

						<div class="form-group">
							<label class="show-payment-banner">
								- <span class="payment-days">0</span> días en banner lateral.
								(Costo: Bs. <span class="payment-cost">0</span>)
							</label>
						</div>

						<div class="form-group">
							<label class="show-payment-busqueda">
								- <span class="payment-days">0</span> días en primeros lugares de resultados de búsqueda.
								(Costo: Bs. <span class="payment-cost">0</span>)
							</label>
						</div>

					</div>
				</form>
			</div>
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-money"></i> Total <span>Bs.&nbsp;<span class="total-payment">0</span></span>
				</div>
				<div class="tools">
				</div>
			</div>
		</div>
		<!-- END SAMPLE FORM PORTLET-->
	</div>
</div>
<!-- END PAGE CONTENT-->

<script>
</script>