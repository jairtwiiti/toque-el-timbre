<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i> Pagos
                </div>
                <div class="tools">
                </div>
            </div>
            <div class="portlet-body form">
                <form id="form_payment" role="form" method="post" enctype="multipart/form-data">
                    <div class="form-body">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4"></div>
                                <div class="col-md-4" style="text-align: center;">
                                    <h1 class="page-title"><?= $pagoId ?></h1>
                                </div>
                                <div class="col-md-4"></div>
                            </div>
                            <div class="input-group">
								<?php 
								switch($result){
									case 4: 
										echo '<div>El comercio no se encuentra registrado en Tigo Money, por favor contactese con nosotros al 591 78526002 o a nuestro mail <a href="mailto:info@toqueeltimbre.com">info@toqueeltimbre.com</a> para mayor informacion.</div>';
									break;
									case 7: 
										echo '<div>Acceso Denegado por favor intente nuevamente y verifique los datos ingresados.</div>';
									break;
									case 8: 
										echo '<div>El PIN ingresado no es valido, por favor vuelva a intentar nuevamente.</div>';
									break;
									case 11: 
										echo '<div>El tiempo de respuesta ha excedido el limite, por favor vuelva a intentar la transaccion nuevamente.</div>';
									break;
									case 14: 
										echo '<div>La billetera movil destino no se encuentra registrada, por favor ingrese un numero Tigo Money valido.</div>';
									break;
									case 17: 
										echo '<div>El monto ingresado no es valido, por favor verifique los datos proporcionados.</div>';
									break;
									case 19: 
										echo '<div>El comercio no se encuentra habilitado para realizar pagos, por favor contactese con nosotros al 591 78526002 o a nuestro mail <a href="mailto:info@toqueeltimbre.com">info@toqueeltimbre.com</a> para mayor informacion</div>';
									break;
									case 23: 
										echo '<div>El monto introducido es menor al requerido, por favor verifique los datos.</div>';
									break;
									case 24: 
										echo '<div>El monto introducido es mayor al requerido, por favor verifique los datos.</div>';
									break;
									case 1001: 
										echo '<div>Los fondos en su Billetera movil son insuficientes, para cargar su billetera vaya al Punto Tigo Money mas cercano, marque *555# para mas información.</div>';
									break;
									case 1002: 
										echo '<div>No ingresaste tu PIN o ingresaste un PIN incorrecto, tu transaccion no pudo ser completada, intentar realizar el pago nuevamente.</div>';
									break;
									case 1004: 
										echo '<div>Estimado Cliente llego a su limite de monto transaccionado, si tiene alguna consulta comuniquese con el *555 para mas información.</div>';
									break;
									case 1012: 
										echo '<div>Estimado Cliente excedio su limite de intentos de introducir su PIN, por favor comuniquese con el *555 para solicitar su nuevo PIN.</div>';
									break;
									case 560: 
										echo '<div>Su transaccion no fue completada, favor intente nuevamente dentro de unos minutos.</div>';
									break;
									default:
										echo '<div>La transaccion no puedo efectuarse correctamente, por favor contactese con nosotros al 591 78526002 o a nuestro mail <a href="mailto:info@toqueeltimbre.com">info@toqueeltimbre.com</a> para mayor informacion.</div>';
									break;
								}
								?>
                                
                            </div>

                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END SAMPLE FORM PORTLET-->
    </div>
    <div class="col-md-2">

    </div>

</div>
<!-- END PAGE CONTENT-->

<script>
</script>