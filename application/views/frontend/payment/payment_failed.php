<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i> Pagos
                </div>
                <div class="tools">
                </div>
            </div>
            <div class="portlet-body form">
                <form id="form_payment" role="form" method="post" enctype="multipart/form-data">
                    <div class="form-body">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4"></div>
                                <div class="col-md-4" style="text-align: center;">
                                    <h1 class="page-title"><?= $pagoId ?></h1>
                                </div>
                                <div class="col-md-4"></div>
                            </div>
                            <div class="input-group">
                                <div>
                                    La transaccion no puedo efectuarse correctamente, por favor contactese con nosotros al 591 78526002 o a nuestro mail <a href="mailto:info@toqueeltimbre.com">info@toqueeltimbre.com</a> para mayor informacion.
                                </div>
                            </div>

                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END SAMPLE FORM PORTLET-->
    </div>
    <div class="col-md-2">

    </div>

</div>
<!-- END PAGE CONTENT-->

<script>
</script>