<?php
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 13/3/2018
 * Time: 12:23 PM
 */
?>
<div id="fb-root"></div>
<button onclick="myFacebookLogin()">Login with facebook</button>
<ul id="list"></ul>
<script>

    window.fbAsyncInit = function() {
        FB.init({
            appId      : <?=$appId?>,
            cookie     : true,
            xfbml      : true,
            version    : "v2.12"
        });

        FB.AppEvents.logPageView();

    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    function subscribeApp(page_id, page_access_token)
    {
        console.log("Subscribing page to app!" + page_id);
        FB.api("/"+page_id+"/subscribed_apps","post",{access_token : page_access_token},
            function(response){
                console.log("Successfully subcribed page", response);
            });
    }

    function myFacebookLogin()
    {
        FB.login(function(response){
            console.log("successfully log in",response);
            FB.api('/me/accounts/',function(response)
            {
                console.log("successfully retrieve pages", response);
                var pages = response.data;
                var ul = document.getElementById('list');
                for(var i = 0; i < pages.length; i++)
                {
                    var page = pages[i];
                    var li = document.createElement('li');
                    var a = document.createElement('a');
                    a.href = "#";
                    a.onclick = subscribeApp.bind(this, page.id, page.access_token);
                    a.innerHTML = page.name;
                    li.appendChild(a);
                    ul.appendChild(li);
                }
            })
        },{scope:"manage_pages"});
    }
</script>
