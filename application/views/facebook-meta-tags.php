<?php
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 25/8/2017
 * Time: 12:09 PM
 */
if(!isset($facebookMetaTags))
{
    $facebookMetaTags["url"] = "";
    $facebookMetaTags["title"] = "";
    $facebookMetaTags["description"] = "";
    $facebookMetaTags["image"] = "";
    $facebookMetaTags["site_name"] = "";
    $facebookMetaTags["app_id"] = "";
}
?>
<meta property="og:url" content="<?=$facebookMetaTags["url"]?>" >
<meta property="og:type" content="website" />
<meta property="og:title" content="<?=$facebookMetaTags["title"]?>">
<meta property="og:description" content="<?=$facebookMetaTags['description'] ?>" >
<meta property="og:image" content="<?=$facebookMetaTags["image"]?>" >
<!--    <meta property="og:image:width" content="1200">-->
<!--    <meta property="og:image:height" content="596">-->
<meta property="og:site_name" content="<?=$facebookMetaTags["site_name"]?>"/>
<meta property="fb:app_id" content="<?=$facebookMetaTags["app_id"]?>"/>
