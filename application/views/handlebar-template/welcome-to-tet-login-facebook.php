<?php
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 5/03/2018
 * Time: 12:11 PM
 */
?>
<!-- END JAVASCRIPTS -->
<script id="ht-welcome-to-tet-login-facebook" type="text/x-handlebars-template">
    <p>Tu cuenta esta casi lista!<br>Ingresa un correo eletronico para poder contactarte cuando sea necesario.</p>
    <div class="input-group">
        <span class="input-group-addon" id="basic-addon1"><i class="fa fa-envelope" aria-hidden="true"></i></span>
        <input type="text" class="form-control" placeholder="myemail@example.com" value="{{userEmail}}" aria-describedby="basic-addon1">
    </div>
    <strong style="color: red;"><em><em class="invalid-email"></em></strong>
    <p>Si ya posees una cuenta en ToqueElTimbre, accede a ella y has click en "Conectame con facebook"</p>
</script>