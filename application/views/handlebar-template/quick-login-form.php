<?php
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 8/9/2017
 * Time: 2:37 PM
 */
?>
<!-- BEGIN - FEATURE TYPE RADIO -->
<script id="ht-quick-login-form" type="text/x-handlebars-template">
    <div class="row">
        <div class="col-md-12">
            <div class="account-box">
                <div class="logo ">
                    <img src="<?=base_url("assets/img/logo.png")?>" alt=""/>
                </div>
                <form class="form-signin-signup" action="#" data-parsley-validate data-parsley-excluded="input[disabled]">
                    <span class="login-error-message">{{defaultMessage}}</span>
                    <div class="register-info" style="display: none;">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1"><i class="fas fa-user"></i></span>
                            <input type="text" class="form-control" name="first-name" placeholder="* Nombre" required data-parsley-errors-container="#first-name-error" disabled/>
                        </div>
                        <span id="first-name-error"></span>
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1"><i class="fas fa-user"></i></span>
                            <input type="text" class="form-control" name="last-name" placeholder="* Apellido" required data-parsley-errors-container="#last-name-error" disabled/>
                        </div>
                        <span id="last-name-error"></span>
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1"><i class="fas fa-phone"></i></span>
                            <input type="text" class="form-control" name="phone" placeholder="* Telefono" required data-parsley-errors-container="#phone-error" data-parsley-type="number" disabled/>
                        </div>
                        <span id="phone-error"></span>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1"><i class="fas fa-envelope"></i></span>
                        <input type="email" class="form-control" name="email" value="{{emailToAttemptLogin}}" placeholder="* Email" required autofocus data-parsley-errors-container="#email-error"/>
                    </div>
                    <span id="email-error"></span>
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1"><i class="fas fa-key"></i></span>
                        <input type="password" class="form-control" name="password" id="password" placeholder="* Password" required data-parsley-errors-container="#password-error"/>
                    </div>
                    <span id="password-error"></span>
                    <div class="register-info" style="display: none;">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1"><i class="fas fa-lock"></i></span>
                            <input type="password" class="form-control" name="confirm-password" placeholder="* Confirmar password" required data-parsley-errors-container="#confirm-password-error" data-parsley-equalto="#password" data-parsley-equalto-message="Las contraseñas son diferentes" disabled/>
                        </div>
                        <span id="confirm-password-error"></span>
                    </div>
                    <input type="hidden" value="<?=$reCaptchaKeys["publicKey"]?>" name="site-key">
                    <div id='signin-signup-recaptcha'></div>
                    <button class="btn btn-lg btn-block purple-bg signin-signup-button" type="button">
                        <i class="fas fa-sign-in-alt"></i> Ingresar
                    </button>
                </form>
                <a class="forgotLnk" href="<?=base_url("recuperar-contrasena")?>">Olvidé mi contraseña</a>
                <div class="or-box">
                    <span class="or">O</span>
                    <div class="row">
                        <div class="col-md-12 row-block">
                            <input type="hidden" value="<?=$facebookAppIdAndSecretKey["appId"]?>" name="app-id">
<!--                            <div class="fb-login-button" data-onlogin="findUser" data-scope="email" data-max-rows="1" data-size="large" data-button-type="login_with" data-show-faces="true" data-auto-logout-link="false" data-use-continue-as="true"></div>-->
                            <a href="#" class="btn btn-facebook btn-block">Facebook</a>
                        </div>
                    </div>
                </div>
                <div class="or-box row-block">
                    <div class="row">
                        <div class="col-md-12 row-block">
                            <span class="signin-signup-switch-message">
                                <span>No tienes una cuenta?</span>
                                <span style="display: none">Ya tienes una cuenta?</span>
                            </span>
                            <button class="btn btn-lg btn-block signin-signup-switch-button" type="button">
                                REGISTRATE YA!
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>