<?php
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 11/12/2017
 * Time: 11:19 AM
 */
?>
<script id="ht-publication-quick-view" type="text/x-handlebars-template">
    <div class="{{colDefinition}}">
        <div class="item_property animateIn" data-animate="flipInX" style="border-bottom: 0; position: relative">
            <div class="head_property">
                <a href="{{publication.seo}}" target="_blank">
                    <img src="{{publication.imageUrl}}" alt="default" title="default toqueeltimbre">
                    <h5 style="right: 0em;">{{publication.ciu_nombre}}</h5>
                </a>
                <div class="av-portfolio-overlay">
                    <a href="{{publication.seo}}" target="_blank"><i class="av-icon fa fa-search"></i> Ver Detalle</a>
                    {{#ifCond publication.Service "!=" "Project"}}
                    <a href="javascript:;" class="btn_favorite " rel="16885">
                        <i class="av-icon fa fa-star"></i> Agregar a Favoritos
                    </a>
                    {{else}}
                    <br style="clear:both;" />
                    <br style="clear:both;" />
                    {{/ifCond}}
                </div>
                {{#ifCond publication.usu_tipo "==" "Inmobiliaria"}}
                <div class="av-portfolio-info">
                    <a href="{{publication.profile}}" target="_blank">
                        <img src="{{publication.userImageUrl}}" alt="{{publication.usu_nombre}}" title="{{publication.usu_nombre}}">
                    </a>
                </div>
                {{/ifCond}}
            </div>
            <div data-imn-id="{{publication.inm_id}}" class="info_property {{publication.backgroundPropertyInfo}}" style="padding-bottom: 0;">
                <ul>
                    <li style="line-height: 18px; padding-bottom: 10px; height: 8em">
                        <div style="font-weight: bold; margin-bottom:8px;">
                            {{publication.cat_nombre}} EN {{publication.for_descripcion}}
                        </div>
                        <span style="float:none;">{{striptags publication.address}}</span>
                    </li>
                    <li>
                        <div class="line_separator"></div>
                        <strong>Precio</strong><span>{{publication.mon_abreviado}} {{numberFormat publication.inm_precio decimalLength="0"}}</span>
                    </li>
                </ul>
                {{#ifCond publication.usu_certificado "==" 1}}
                <div class="icon_certificado">
                    <img src="<?=base_url("assets/img/icon_agente_certificados.png")?>" />
                </div>
                {{/ifCond}}
                <div class="footer_property">
                    <div class="icon m2" title="Metros Cuadrados"><img src="<?=base_url("assets/img/icons/metros_cuadrados.png")?>">
                        {{publication.inm_superficie}}
                        {{#ifCond publication.inm_tipo_superficie "==" "Metros Cuadrados"}}
                            m2
                        {{/ifCond}}
                        {{#ifCond publication.inm_tipo_superficie "==" 'Hectareas'}}
                            ha
                        {{/ifCond}}
                    </div>
                    {{#ifCond publication.[4] "!=" ""}}
                        <div class="icon bedroom" title="Dormitorios"><img src="<?=base_url("assets/img/icons/dormitorios.png")?>"> {{publication.[4]}}</div>
                    {{/ifCond}}
                    {{#ifCond publication.[5] "!=" ""}}
                        <div class="icon bathroom" title="Baños"><img src="<?=base_url("assets/img/icons/banos.png")?>"> {{publication.[5]}}</div>
                    {{/ifCond}}
                    {{#ifCond publication.[9] "!=" ""}}
                    <div class="icon bathroom" title="Baños"><img src="<?=base_url("assets/img/icons/parqueo.png")?>"> {{publication.[9]}}</div>
                    {{/ifCond}}
                    <br style="clear:both">
                </div>
            </div>
        </div>
    </div>
</script>