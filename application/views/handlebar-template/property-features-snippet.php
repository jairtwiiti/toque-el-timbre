<?php
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 8/9/2017
 * Time: 2:37 PM
 */
?>
<!-- BEGIN - FEATURE TYPE RADIO -->
<script id="ht-feature-type-radio" type="text/x-handlebars-template">
    <div class="col-md-4 col-lg-4">
        <div class="form-group">
            <label>{{feature.car_descripcion}}</label>
            <div class="mt-radio-inline">
                <label class="mt-radio">
                    <input type="radio" name="status" value="1"> Metros cuadrados
                    <span></span>
                </label>
                <label class="mt-radio">
                    <input type="radio" name="status" value="0"> Hectareas
                    <span></span>
                </label>
            </div>
        </div>
    </div>
</script>
<script id="ht-feature-radio-items" type="text/x-handlebars-template">
    <label class="mt-radio">
        <input type="radio" name="{{option.name}}" value="{{option.value}}"> {{option.label}}
        <span></span>
    </label>
</script>
<!-- END - FEATURE TYPE RADIO -->

<!-- BEGIN - FEATURE TYPE INPUT -->
<script id="ht-feature-type-input" type="text/x-handlebars-template">
    <div class="col-md-4 col-lg-4">
        <div class="form-group">
            <label class="required" data-field="name">{{feature.car_descripcion}}</label>
            <input  name="features[{{feature.car_id}}]" parsley-trigger="change"
                    {{#ifCond feature.car_id "==" 11}}
                        required type="{{feature.inputMasked}}" data-inputmask="'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true"
                    {{/ifCond}}
                    {{#ifCond feature.car_id "==" 13}}
                        min='1' data-parsley-type='integer'
                    {{/ifCond}}
               class="form-control input-masked" value="{{feature.default}}">
        </div>
    </div>
</script>
<!-- END - FEATURE TYPE INPUT -->

<!-- BEGIN - FEATURE TYPE SELECT2 -->
<script id="ht-feature-type-select2" type="text/x-handlebars-template">
    <div class="col-md-4 col-lg-4">
        <div class="form-group">
            {{var "default" feature.default}}
            <label class="required" data-field="property-type">{{feature.car_descripcion}}</label>
            <select class="select2" name="features[{{feature.car_id}}]">
                {{#listSelectOptions feature.valores feature.default}}{{/listSelectOptions}}
            </select>
        </div>
    </div>
</script>
<script id="ht-feature-select2-item" type="text/x-handlebars-template">
    <option value="option"></option>
</script>
<!-- END - FEATURE TYPE SELECT2 -->


