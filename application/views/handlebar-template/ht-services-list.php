<?php
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 04/12/2018
 * Time: 11:19 AM
 */
?>
<script id="ht-services-list" type="text/x-handlebars-template">
    {{#each servicesList}}
        {{> ht-service-type}}
    {{/each}}
</script>
<script id="ht-service-type" type="text/x-handlebars-template">
    <div class="row">
        <div class="col-md-7">
            <div class="form-group">
                {{var "groupDescription" type}}
                {{#ifCond type "==" "Principal"}}
                    {{var "groupDescription" "Aparecer en pagina principal"}}
                {{/ifCond}}
                {{#ifCond type "==" "Busqueda"}}
                    {{var "groupDescription" "Primeros lugares en resultados de búsqueda"}}
                {{/ifCond}}
                {{#ifCond type "==" "Similares"}}
                    {{var "groupDescription" "Primeros lugares en anuncios similares"}}
                {{/ifCond}}

                <label>{{groupDescription}}</label> <span class="form-group-clear hide" data-radio-name="principal">Limpiar</span>
                <div class="input-group">
                    <div class="icheck-list">
                        {{#each list}}
                            {{> ht-service-item}}
                        {{/each}}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            {{var "imageSrc" "assets/backend/admin/img/logo-default.png"}}
            {{#ifCond type "==" "Principal"}}
                {{var "imageSrc" "admin/imagenes/principalServiceExample.PNG"}}
            {{/ifCond}}
            {{#ifCond type "==" "Busqueda"}}
                {{var "imageSrc" "admin/imagenes/searchServiceExample.PNG"}}
            {{/ifCond}}
            {{#ifCond type "==" "Similares"}}
                {{var "imageSrc" "admin/imagenes/similarsServiceExample.PNG"}}
            {{/ifCond}}
            <a href="#" class="thumbnail" data-toggle="modal" data-target=".service-example" data-service-title="Aparecer en pagina principal" data-service-type="principal"> <img alt="100%x180" data-src="holder.js/100%x180" style="height: auto; width: 110px; display: block;" src="{{base_url}}{{imageSrc}}" data-holder-rendered="true"> </a>
        </div>
    </div>
</script>
<script id="ht-service-item" type="text/x-handlebars-template">
    <label days="{{ser_dias}}" cost="{{ser_precio}}">
        <input type="radio" name="services[{{ser_tipo}}][]" class="icheck" data-radio="iradio_square-grey" value="{{ser_id}}">
        {{ser_dias}} días ( Bs. {{ser_precio}} Bs.- )
    </label>
</script>