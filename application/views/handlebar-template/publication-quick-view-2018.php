<?php
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 11/12/2017
 * Time: 11:19 AM
 */
?>
<script id="ht-publication-quick-view" type="text/x-handlebars-template">

    <div class="{{thumbnailSettings.colDefinition}} publication-thumbnail">
        <div class="thumbnail">
            <a href="{{seoFormat publication.seo groupOrder=publication.group_order propertyType=publication.cat_nombre transactionType=publication.for_descripcion stateName=publication.dep_nombre}}" target="_blank" class="thumbnail">
                {{#ifCond publication.group_order "==" 1}}
                    <img src="{{base_url}}librerias/timthumb.php?src={{base_url}}admin/imagenes/proyecto/{{publication.publicationImage}}&w=360&h=250" data-default-width="360" data-default-height="250" alt="default" title="{{publication.inm_nombre}}">
                {{/ifCond}}
                {{#ifCond publication.group_order "!=" 1}}
                    <img src="{{base_url}}librerias/timthumb.php?src={{base_url}}admin/imagenes/inmueble/{{publication.publicationImage}}&w=360&h=250" data-default-width="360" data-default-height="250" alt="default" title="{{publication.inm_nombre}}">
                {{/ifCond}}
            </a>
            <div class="caption {{publicationQuickViewBgInfo publication.group_order}}">
                <h1 class="publication-price">
                    {{numberFormat publication.inm_precio decimalLength="0"}} {{publication.mon_abreviado}}
                    {{#ifCond userRole "==" "Admin"}}
                        {{#ifCond publication.group_order "==" 1}}
                            <a href="{{base_url}}admin/Project/edit/{{publication.proy_id}}" target="_blank" class="thumbnail-edit-publication"><i class="fas fa-pencil-alt"></a></i>
                        {{/ifCond}}
                        {{#ifCond publication.group_order "!=" 1}}
                            <a href="{{base_url}}admin/Property/edit/{{publication.inm_id}}" target="_blank" class="thumbnail-edit-publication"><i class="fas fa-pencil-alt"></a></i>
                        {{/ifCond}}
                    {{/ifCond}}
                </h1>
                <p class="publication-property-and-transaction-type">{{publication.cat_nombre}} EN {{publication.for_descripcion}}</p>
                <p class="publication-name {{thumbnailSettings.hidePublicationName}}">
                    <a href="{{seoFormat publication.seo groupOrder=publication.group_order propertyType=publication.cat_nombre transactionType=publication.for_descripcion stateName=publication.dep_nombre}}" class="cut-text" target="_blank">{{publication.inm_nombre}}</a>
                </p>
                <p class="publication-address cut-text" data-ct-lines="2">
                    {{#ifCond publication.group_order "==" 1}}
                        {{striptags publication.proy_ubicacion}}
                    {{/ifCond}}
                    {{#ifCond publication.group_order "!=" 1}}
                        {{striptags publication.inm_direccion}}
                    {{/ifCond}}
                </p>

            </div>
            <div class="caption-amenities {{publicationQuickViewBgInfo publication.group_order}}">
                <p class="publication-amenities">
                    <span class="publication-amenity-item">
                        <i class="fas fa-arrows-alt"></i>
                        <span class="publication-amenity-value">
                            {{publication.inm_superficie}}
                            {{#ifCond publication.inm_tipo_superficie "==" "Metros Cuadrados"}}
                                m2
                            {{/ifCond}}
                            {{#ifCond publication.inm_tipo_superficie "==" 'Hectareas'}}
                                ha
                            {{/ifCond}}
                        </span>
                    </span>
                    {{#ifCond publication.[4] "!=" ""}}
                    <span class="publication-amenity-item">
                        <i class="fas fa-bed"></i>
                        <span class="publication-amenity-value">
                            {{publication.[4]}}
                        </span>
                    </span>
                    {{/ifCond}}
                    {{#ifCond publication.[5] "!=" ""}}
                    <span class="publication-amenity-item">
                        <i class="fas fa-bath"></i>
                        <span class="publication-amenity-value">
                            {{publication.[5]}}
                        </span>
                    </span>
                    {{/ifCond}}
                    {{#ifCond publication.[9] "!=" ""}}
                    <span class="publication-amenity-item">
                        <i class="fas fa-car"></i>
                        <span class="publication-amenity-value">
                            {{publication.[9]}}
                        </span>
                    </span>
                    {{/ifCond}}
                </p>
            </div>
        </div>
    </div>
</script>