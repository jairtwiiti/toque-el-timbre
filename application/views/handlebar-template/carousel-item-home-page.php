<?php
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 12/4/2018
 * Time: 12:05 PM
 */
?>
<script id="ht-carousel-item-home-page" type="text/x-handlebars-template">
    {{#each projectList}}
        {{#ifCond proy_fot_imagen "!=" null}}
            <div class="item_property">
                <div class="head_property">
                    <a href="{{base_url}}{{seo_url}}" target="_blank">
                        <img src="{{base_url}}/librerias/timthumb.php?src={{base_url}}/admin/imagenes/proyecto/{{proy_fot_imagen}}&w=271&h=183" alt="{{name}}" title="{{name}}">
                        <h5 style="right: 0em;">{{city}}</h5>
                    </a>
                </div>
                <div class="info_property">
                    <ul>
                        <li><div class="bold_text" style="font-size: 14px;">{{name}}.</div></li>
                        <li><div class="text-justify" style="font-size: 12px;">{{proy_ubicacion}}</div></li>
                        <li>
                            <div class="line_separator"></div>
                            <span class="bold_text" style="float: none; font-size: 14px;">{{transaction_type}}</span>
                            <span> {{currency}} {{numberFormat price decimalLength="0"}}</span>
                        </li>
                    </ul>
                </div>
            </div>
        {{/ifCond}}
    {{/each}}
</script>
