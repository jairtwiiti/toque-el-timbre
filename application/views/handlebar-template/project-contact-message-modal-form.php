<?php
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 23/4/2018
 * Time: 09:43 AM
 */
?>
<script id="ht-project-contact-message-modal-form" type="text/x-handlebars-template">
    <div class="agent-contact-form fix project-contact-form">
        <form name="new-form-contact" action="#" class="project-contact-message-modal-form" data-parsley-validate>
            <input type="hidden" value="<?=$reCaptchaKeys["publicKey"]?>" name="site-key">
            <?php
            if(!$user_logged instanceof stdClass)
            {
                ?>
                <div class="input-box">
                    <input type="text" placeholder="Nombre completo" name="nombre" required>
                </div>
                <div class="input-box">
                    <input type="text" placeholder="Telefono" name="telefono" required
                           data-parsley-type="number">
                </div>
                <?php
            }
            ?>
            <?php
            if(!$user_logged instanceof stdClass)
            {
                ?>
                <div class="input-box">
                    <input type="email" placeholder="Email" name="email" required>
                </div>
                <?php
            }
            ?>
            <div class="input-box">
                <select name="ciudad" required="">
                    <option value="Santa Cruz">Santa Cruz</option>
                    <option value="La Paz">La Paz</option>
                    <option value="Cochabamba">Cochabamba</option>
                    <option value="Tarija">Tarija</option>
                    <option value="Chuquisaca">Chuquisaca</option>
                    <option value="Oruro">Oruro</option>
                    <option value="Potosi">Potosi</option>
                    <option value="Pando">Pando</option>
                    <option value="Beni">Beni</option>
                </select>
            </div>
            <div class="input-box"><textarea placeholder="Consulta" required name="mensaje"></textarea></div>
                <input type="checkbox" name="send-to-whatsapp" id="send-to-whatsapp"> Enviar tambien por whatsapp
            <div id='project-contact-message-modal-form-recaptcha'></div>
        </form>
    </div>
</script>