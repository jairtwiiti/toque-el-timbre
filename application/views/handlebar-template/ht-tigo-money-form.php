<?php
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 28/11/2018
 * Time: 12:11 PM
 */
?>
<!-- END JAVASCRIPTS -->
<script id="ht-tigo-money-form" type="text/x-handlebars-template">
    <div class="col-md-12">
        <form method="post" name="tigo-money-form">
            <input type="hidden" name="payment-id" value="{{paymentId}}" />
            <div class="row">
                <div class="col-md-4">
                    <p><input type="text" name="linea" placeholder="Linea Tigo Money" style="padding: 7px 10px; font-size: 14px;" required /></p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <p>
                        <input type="button" id="send-payment-request" value="Pagar con Tigo Money" style="background-color: #24a5d9; color: white; border: 0; padding: 7px 14px; font-size: 14px;" />
                    </p>
                </div>
            </div>
        </form>
    </div>
</script>