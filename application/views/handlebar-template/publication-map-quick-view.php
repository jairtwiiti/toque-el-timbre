<?php
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 11/12/2017
 * Time: 11:19 AM
 */
?>
<script id="ht-publication-map-quick-view" type="text/x-handlebars-template">
<div id="iw-container">
    <div class="iw-title">{{publication.inm_nombre}}</div>
    <div class="iw-content">
        <div class="iw-subTitle">Direccion</div>
        <img src="{{publication.imageUrl}}" alt="Porcelain Factory of Vista Alegre" height="90" width="150">
        <p class="text-justify" style="width: 350px;">{{striptags publication.inm_direccion}}</p>
        <div class="iw-subTitle">Precio</div>
        <p>{{publication.inm_precio}}</p>
        <p>
            <a href="{{publication.seo}}" target="_blank" class="btn btn-primary btn-xs" role="button">Ver</a>
        </p>
        <div class="iw-bottom-gradient"></div>
    </div>
    </div>

</script>