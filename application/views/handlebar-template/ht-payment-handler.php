<?php
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 23/04/2019
 * Time: 11:19 AM
 */
?>
<script id="ht-purchase-option-list" type="text/x-handlebars-template">
    <div class="portlet box purple">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-buy"></i> Formulario de compra
            </div>
            <div class="actions">
                <a class="btn btn-circle btn-danger" href="javascript:swal.close()">Cerrar</a></a>
            </div>
        </div>
        <div class="portlet-body form">
            <form role="form" name="purchase-form" class="form-horizontal" data-parsley-validate="">
                <div class="form-body">
                    <h3 class="form-section text-left">Elije uno o mas de nuestros paquetes</h3>
                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="pricing-content-1">
                                <div class="row">
                                    {{#each data.servicesList}}
                                        {{> ht-service-list-item service=this}}
                                    {{/each}}
                                </div>
                                <div class="form-group">
                                    <div id="purchase-option-error-content" class="text-left"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h3 class="form-section text-left">Datos de facturacion</h3>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="col-md-12 text-left">
                                <div class="form-group form-md-line-input">
                                    <div class="input-icon">
                                        <input type="text" class="form-control" required placeholder="Nombre completo" name="full-name" data-parsley-pattern="^[a-zA-Z ]*$" value="SN" data-parsley-error-message="Por favor ingrese un nombre sin caracteres especiales">
                                        <span class="help-block">Nombre a facturar</span>
                                        <i class="fa fa-user"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 text-left">
                                <div class="form-group form-md-line-input">
                                    <div class="input-icon">
                                        <input type="text" class="form-control" required placeholder="Nit" name="nit" value="0" data-parsley-error-message="Ingrese un nit por favor">
                                        <span class="help-block">Nit/CI</span>
                                        <i class="fa fa-slack"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group" style="margin-bottom: 0px;">
                                <div class="col-md-12">
                                    <div class="price-table-pricing">
                                        <h1 style="font-size: 40">Total<br>
                                            <sup class="price-sign">Bs</sup><span id="total-to-purchase">0.00</span>
                                        </h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-actions hide">
                    <div class="row">
                        <div class="col-md-offset-2 col-md-8">
                            <button type="button" class="btn default">Cancel</button>
                            <button type="submit" class="btn blue">Submit</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

</script>
<script id="ht-service-list-item" type="text/x-handlebars-template">
    <div class="col-md-4">
        <div class="price-column-container border-active">
            <div class="price-table-head bg-toque-el-timbre">
                <h2 class="no-margin">{{type}}</h2>
                {{#ifCond id "==" 21}}
                    <div class="price-ribbon">Popular</div>
                {{/ifCond}}
            </div>
            <div class="arrow-down border-top-toque-el-timbre"></div>
            <div class="price-table-pricing">
                <h3>
                    <sup class="price-sign">Bs</sup>{{price}}
                </h3>
                <p style="margin-bottom: 0px">{{description}}</p>
            </div>
            <div style="margin-bottom: 20px">
                <div class="form-group form-md-checkboxes">
                    <div class="md-checkbox-inline">
                        <div class="md-checkbox">
                            <input type="checkbox" id="checkbox-{{id}}" name="purchase-option[]" required="" class="md-check checkbox-purchase-option" value="{{id}}" data-price="{{price}}" data-parsley-errors-container="#purchase-option-error-content" data-parsley-error-message="Tienes que elegir al menos una opcion">
                            <label for="checkbox-{{id}}">
                                <span></span>
                                <span class="check"></span>
                                <span class="box"></span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>
<script id="ht-payment-option-list" type="text/x-handlebars-template">
    <div class="portlet box purple">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-money"></i>Elige tu metodo de pago </div>
            <div class="actions">
                <a class="btn btn-circle btn-danger" href="javascript:swal.close()">Cerrar</a></a>
            </div>
        </div>
        <div class="portlet-body">
            <div class="tabbable-custom ">
                <ul class="nav nav-tabs ">
                    <li class="active">
                        <a href="#tab_5_1" data-toggle="tab"> PagosNet </a>
                    </li>
                    <li class="hide">
                        <a href="#tab_5_2" data-toggle="tab"> Tigo Money </a>
                    </li>
                    <li>
                        <a href="#tab_5_3" data-toggle="tab"> Dep&oacute;sito/Transferencia </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_5_1">
                        {{#ifCond data.payment.pagosNetTransactionId "==" null}}
                        <div class="alert alert-danger" role="alert">
                            No se ha podido conectar con <strong>Pagosnet</strong>, por favor realice el pago a trav&eacute;s de la opci&oacute;n <strong>"Dep&oacute;sito/Transferencia"</strong>.
                        </div>
                        {{/ifCond}}
                        <p class="text-justify">
                            Pagosnet es un servicio de puntos de pago que facilita a los anunciantes de inmuebles realizar su pago en efectivo en mas de 350 puntos autorizados a nivel nacional.
                        </p>
                        <p class="text-justify">
                            Use este codigo <strong>{{data.payment.id}}</strong> para realizar su pago en efectivo de <strong>{{data.payment.amount}} Bs</strong> en las siguientes entidades financieras:
                        </p>
                        <?php
                            $arrayImage = array('farmacorp.png', 'banco_fassil.png', 'grigota.png','x_cobrar.png','eco_futuro.png','la_merced.png','mutual_la_primera.png','mutual_la_paz.png');
                            $html = '';
                            foreach($arrayImage as $image)
                            {
                                $imageUrl = assets_url("img/logos_pagosnet/".$image);
                                $timthumbImage = base_url("librerias/timthumb.php?src=".$imageUrl."&w=100");
                                $html .= '
                                    <div class="thumbnail" style="margin-bottom: 0px;display: inline-block;">
                                        <img src="'.$timthumbImage.'" alt="Farmacorp">
                                    </div>
                                ';
                            }
                            echo $html;
                        ?>
                    </div>
                    <div class="tab-pane" id="tab_5_2">
                        <?php
                        $imageUrl = assets_url("img/logo-tigo-money.jpg");
                        $timthumbImage = base_url("librerias/timthumb.php?src=".$imageUrl."&w=200");
                        ?>
                        <div class="thumbnail" style="margin-bottom: 0px;display: inline-block;">
                            <img src="<?=$timthumbImage?>" alt="Farmacorp">
                        </div>
                        <p class="text-justify">
                            Usted puede pagar a traves de su cuenta tigo money, solo ingrese el numero de su cuenta para poder proceder al pago.
                        </p>
                        <div class="row">
                            <form name="tigo-money-request-payment-form">
                                <div class="col-md-8" style="margin-left: auto;margin-right: auto;float: none;">
                                    <input type="hidden" name="payment-id" value="{{data.payment.id}}">
                                    <div class="form-group form-md-line-input has-info">
                                        <div class="input-group input-group-lg">
                                            <div class="input-group-control">
                                                <input type="text" name="cellphone" class="form-control input-lg" placeholder="Telefono">
                                            </div>
                                            <span class="input-group-btn btn-right">
                                            <button class="btn blue-madison tigo-money-request-payment" type="button">Procesar pago!</button>
                                        </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12" id="tigo-money-response-message">
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab_5_3">
                        <p class="text-justify">
                            Usted puede pagar mediante transferencia o depósito bancario. Tenemos nuestra cuenta en el <strong>Banco Ganadero</strong>.
                        </p>
                        <p class="text-justify">
                            Si hace una transferencia bancaria mediante banca por internet, por favor coloque el nombre de su empresa en el detalle del pago y envíenos una captura de pantalla a pagos@toqueeltimbre.com. Si realiza un depósito por ventanilla, por favor escanee el comprobante de depósito y envíelo a <strong>pagos@toqueeltimbre.com</strong>. Una vez verificado su pago, su anuncio será publicado en el plazo de 24 horas hábiles.
                        </p>
                        <p class="text-justify">
                            Le enviaremos una factura digital por correo electronico. Usted la debe imprimir y esta le servirá para crédito fiscal.
                        </p>
                        <div class="note note-warning">
                            <h5 class="block text-left">NOTA IMPORTANTE </h5>
                            <p class="text-justify">
                                Nos debe informar antes de fin de mes el nombre y NIT para la factura de cualquier depósito o transferencia que realice. Caso contrario, procederemos a emitir su factura sin nombre y sin NIT. El plazo para enviar esta informacion es hasta las 16:00 horas del último día hábil del mes.
                            </p>
                        </div>
                        <dl class="text-left">
                            <dt>Cuenta Nro</dt>
                            <dd>1041157282</dd>
                            <dt>A nombre de</dt>
                            <dd>TOQUEELTIMBRE.COM S.R.L.</dd>
                            <dt>NIT</dt>
                            <dd>222334022</dd>
                        </dl>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>

<script id="ht-tigo-money-danger-message" type="text/x-handlebars-template">
    <div class="alert alert-block alert-danger">
        <h4 class="alert-heading text-left"><strong>{{data.status}}!</strong></h4>
        <p class="text-justify">
            {{data.message}}
        </p>
    </div>
</script>
<script id="ht-tigo-money-success-message" type="text/x-handlebars-template">
    <div class="alert alert-block alert-success">
        <h4 class="alert-heading text-left"><strong>{{data.status}}!</strong></h4>
        <p class="text-justify">
            {{data.message}}
        </p>
        <p class="text-justify">
            Verificaremos el estado del pago en <span class="countdown"></span>
        </p>
    </div>
</script>
<script id="ht-tigo-money-info-message" type="text/x-handlebars-template">
    <div class="alert alert-block alert-info">
        <h4 class="alert-heading text-left"><strong>{{data.status}}!</strong></h4>
        <p class="text-justify">
            {{data.message}}
        </p>
    </div>
</script>
<script id="ht-tigo-money-warning-message" type="text/x-handlebars-template">
    <div class="alert alert-block alert-warning">
        <h4 class="alert-heading text-left"><strong>{{data.status}}!</strong></h4>
        <p class="text-justify">
            {{data.message}}
        </p>
    </div>
</script>