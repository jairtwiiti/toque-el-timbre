<!DOCTYPE html>
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.5
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!-- <![endif] -->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Toque El Timbre | En mantenimiento</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Portal inmobiliario" name="description" />
        <meta content="" name="author"/>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="theme/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="theme/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="theme/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="theme/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="theme/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="theme/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="theme/assets/pages/css/coming-soon.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" />
    </head>
    <!-- END HEAD -->

    <body class="">
        <div class="container">
            <div class="row">
                <div class="col-md-12 coming-soon-header">
                    <a class="brand" href="index.html">
                        <img src="theme/assets/global/img/logo.png" alt="logo" /> </a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 coming-soon-content">
                    <h1 style="text-shadow: 3px 3px #6a686336;color: #6f6e70;font-weight: 400;">En mantenimiento</h1>
                    <p style="text-shadow: 3px 3px #6a686336;color: #6f6e70;"> Estamos haciendo mantimiento de nuestro sistema, estaremos de vuelta en poco tiempo.<br>Disculpe las molestias</p>
                    <br>

                    <ul class="social-icons margin-top-20">
                        <li>
                            <a style="background-color: #706f71;" href="https://business.facebook.com/ToqueelTimbre/" data-original-title="Facebook" class="facebook"> </a>
                        </li>
                        <li>
                            <a style="background-color: #706f71;" href="https://twitter.com/ToqueElTimbreBO" data-original-title="Twitter" class="twitter"> </a>
                        </li>
                        <li>
                            <a style="background-color: #706f71;" href="https://www.youtube.com/channel/UCTA7OVG82_pTTc9iEeHLCGg" data-original-title="Youtube" class="youtube"> </a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-6 coming-soon-countdown">
<!--                    <div id="defaultCountdown"> </div>-->
                </div>
            </div>
            <!--/end row-->
            <div class="row">
                <div class="col-md-12 coming-soon-footer" style="text-shadow: 3px 3px #6a686336;color: #6f6e70;"> <?=date("Y")?> &copy; Toque El Timbre. Portal Inmobiliario</div>
            </div>
        </div>
        <!--[if lt IE 9]>
<script src="theme/theme/assets/global/plugins/respond.min.js"></script>
<script src="theme/assets/global/plugins/excanvas.min.js"></script> 
<script src="theme/assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="theme/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="theme/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="theme/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="theme/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="theme/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="theme/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="theme/assets/global/plugins/countdown/jquery.countdown.min.js" type="text/javascript"></script>
        <script src="theme/assets/global/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="theme/assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="theme/assets/pages/scripts/coming-soon.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <!-- END THEME LAYOUT SCRIPTS -->
        <script>
            $(document).ready(function()
            {
                $('#clickmewow').click(function()
                {
                    $('#radio1003').attr('checked', 'checked');
                });
            })
        </script>
    </body>
</html>