<?php
$path_assets = base_url() . "assets/backend";
?>
</div>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?php echo $path_assets; ?>/global/plugins/respond.min.js"></script>
<script src="<?php echo $path_assets; ?>/global/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="<?php echo $path_assets; ?>/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo $path_assets; ?>/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<?php echo $path_assets; ?>/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="<?php echo $path_assets; ?>/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo $path_assets; ?>/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="<?php echo $path_assets; ?>/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo $path_assets; ?>/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo $path_assets; ?>/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="<?php echo $path_assets; ?>/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="<?php echo $path_assets; ?>/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo $path_assets; ?>/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
<script src="<?php echo $path_assets; ?>/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
<script src="<?php echo $path_assets; ?>/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
<script src="<?php echo $path_assets; ?>/global/plugins/flot/jquery.flot.pie.min.js" type="text/javascript"></script>
<script src="<?php echo $path_assets; ?>/global/plugins/flot/jquery.flot.tooltip.min.js" type="text/javascript"></script>

<?php if($page == "profile"): ?>
	<script src="<?php echo $path_assets; ?>/global/plugins/jquery.pulsate.min.js" type="text/javascript"></script>
	<script src="<?php echo $path_assets; ?>/global/plugins/bootstrap-daterangepicker/moment.min.js" type="text/javascript"></script>
	<script src="<?php echo $path_assets; ?>/global/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
	<!-- IMPORTANT! fullcalendar depends on jquery-ui-1.10.3.custom.min.js for drag & drop support -->
	<script src="<?php echo $path_assets; ?>/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
	<script src="<?php echo $path_assets; ?>/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
	<script src="<?php echo $path_assets; ?>/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
	<script src="<?php echo $path_assets; ?>/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>

	<script src="https://maps.google.com/maps/api/js" type="text/javascript"></script>
	<script src="<?php echo $path_assets; ?>/global/plugins/gmaps/gmaps.min.js" type="text/javascript"></script>
<?php endif; ?>

<?php if($page == "publication"): ?>
	<script type="text/javascript" src="<?php echo $path_assets; ?>/global/plugins/select2/select2.min.js"></script>
	<script type="text/javascript" src="<?php echo $path_assets; ?>/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="<?php echo $path_assets; ?>/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>

	<script type="text/javascript" src="<?php echo $path_assets; ?>/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

	<script type="text/javascript" src="<?php echo $path_assets; ?>/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
	<script type="text/javascript" src="<?php echo $path_assets; ?>/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js"></script>
	<script type="text/javascript" src="<?php echo $path_assets; ?>/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js"></script>
	<script type="text/javascript" src="<?php echo $path_assets; ?>/global/plugins/fancybox/source/jquery.fancybox.pack.js"></script>
	<script type="text/javascript" src="<?php echo $path_assets; ?>/global/plugins/plupload/js/plupload.full.min.js"></script>

	<script type="text/javascript" src="<?php echo $path_assets; ?>/global/plugins/plupload/js/i18n/es.js"></script>

<!--	<script src="https://maps.google.com/maps/api/js" type="text/javascript"></script>-->
<!--	<script src="--><?php //echo $path_assets; ?><!--/global/plugins/gmaps/gmaps.min.js" type="text/javascript"></script>-->
<?php endif; ?>

<?php if($page == "payment"): ?>
	<script src="<?php echo $path_assets; ?>/global/plugins/icheck/icheck.min.js"></script>
<?php endif; ?>

<?php if($page == "project"): ?>
	<script type="text/javascript" src="<?php echo $path_assets; ?>/global/plugins/select2/select2.min.js"></script>
	<script type="text/javascript" src="<?php echo $path_assets; ?>/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="<?php echo $path_assets; ?>/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<?php endif; ?>

<?php if($page == "admin_paymentt"): ?>
	<script type="text/javascript" src="<?php echo $path_assets; ?>/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="<?php echo $path_assets; ?>/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
	<script type="text/javascript" src="<?php echo $path_assets; ?>/global/plugins/datatables/extensions/FilterDelay/dataTables.FilterDelay.js"></script>
<?php endif; ?>
<!-- END PAGE LEVEL PLUGINS -->


<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo $path_assets; ?>/global/scripts/metronic.js" type="text/javascript"></script>
<script src="<?php echo $path_assets; ?>/admin/scripts/layout.js" type="text/javascript"></script>
<script src="<?php echo $path_assets; ?>/admin/scripts/demo.js" type="text/javascript"></script>
<script src="<?php echo $path_assets; ?>/admin/pages/scripts/index.js" type="text/javascript"></script>
<script src="<?php echo $path_assets; ?>/admin/coupon.js"></script>
<?php if($page == "profile"): ?>
	<script src="<?php echo $path_assets; ?>/admin/pages/scripts/profile.js" type="text/javascript"></script>
	<script src="<?php echo $path_assets; ?>/admin/pages/scripts/maps-google.js" type="text/javascript"></script>
<?php endif; ?>
<?php if($page == "inbox"): ?>
	<script src="<?php echo $path_assets; ?>/admin/pages/scripts/inbox.js" type="text/javascript"></script>
<?php endif; ?>
<?php if($page == "publication"): ?>
	<script src="<?php echo $path_assets; ?>/admin/pages/scripts/table-managed.js"></script>
	<script src="<?php echo $path_assets; ?>/global/scripts/datatable.js"></script>
	<script src="<?php echo $path_assets; ?>/admin/pages/scripts/ecommerce-products-edit.js"></script>
	<script src="<?php echo $path_assets; ?>/admin/pages/scripts/PollModal.js"></script>
	<script src="<?php echo $path_assets; ?>/admin/pages/scripts/maps-google.js" type="text/javascript"></script>
<?php endif; ?>
<?php if($page == "payment"): ?>
	<script src="<?php echo $path_assets; ?>/admin/pages/scripts/form-icheck.js"></script>
<?php endif; ?>
<?php if($page == "project"): ?>
	<script src="<?php echo $path_assets; ?>/admin/pages/scripts/table-editable.js"></script>
<?php endif; ?>
<?php $complementHandler->printViewJs()?>
<!-- END PAGE LEVEL SCRIPTS -->
<script>

	jQuery(document).ready(function() {
		Metronic.init(); // init metronic core componets
		Layout.init(); // init layout
		Demo.init(); // init demo features
		Index.init();
		Index.initDashboardDaterange();
		Index.initJQVMAP(); // init index page's custom scripts

		var visitors = null;
		var visitorsBar = null;
		var publicationsCreated = null;
		var categoriesPieOffer = null;
		var categoriesPieDemand = null;

		if (visitors != null)
			Index.initCharts(visitors, publicationsCreated); // init index page's custom scripts

		if (categoriesPieOffer != null && categoriesPieDemand != null)
			Index.initPieCharts(categoriesPieOffer, categoriesPieDemand);

		if (visitorsBar != null) {
			Index.initBarCharts(visitorsBar);
		}

		Index.initMiniCharts();

		<?php if($page == "profile"): ?>
			Profile.init();
			MapsGoogle.init();
		<?php endif; ?>
		<?php if($page == "inbox"): ?>
			Inbox.init();
		<?php endif; ?>
		<?php if($page == "publication"): ?>
			TableManaged.init();
			EcommerceProductsEdit.init();
		<?php endif; ?>
		<?php if($page == "payment"): ?>
			FormiCheck.init();
		<?php endif; ?>
		<?php if($page == "project"): ?>
			TableEditable.init();
		<?php endif; ?>

		var stateLocations = {
			'Santa Cruz': {city: 'Bolivia, Santa cruz de la sierra, plaza 24 de septiembre', latitude:-17.784559, longitude:-63.1590514},
			'La Paz'	: {city: 'Bolivia, La paz, centro', latitude:-16.488274, longitude:-68.1143932},
			'Cochabamba': {city: 'Bolivia, Cochabamba, centro', latitude:-17.39404, longitude:-66.1638755},
			'Tarija'	: {city: 'Bolivia, Tarija, centro', latitude:-21.5278145, longitude:-64.7295199},
			'Beni'	  : {city: 'Bolivia, Beni, trinidad', latitude:-14.8340025, longitude:-64.9033503},
			'Oruro'	 : {city: 'Bolivia, Oruro, centro', latitude:-17.9653555, longitude:-67.1086121},
			'Potosi'	: {city: 'Bolivia, Potosi, centro', latitude:-19.5720068, longitude:-65.7529913},
			'Chuquisaca': {city: 'Bolivia, Chuquisaca, sucre', latitude:-19.0202041, longitude:-65.2610797},
			'Pando'	 : {city: 'Bolivia, Pando, cobija', latitude:-11.0302227, longitude:-68.775359},
			'defautl'   : {city: 'Bolivia, Santa cruz de la sierra, plaza 24 de septiembre', latitude:-17.784559, longitude:-63.1590514}
		}

		$('#state').change (function(){
			var departmentName = $('#state option:selected').text();
			var department = stateLocations[departmentName];

			$('#gmap_geocoding').attr('data-latitud', department.latitude);
			$('#gmap_geocoding').attr('data-longitud', department.longitude);
			$('#gmap_geocoding').attr('data-city', department.city);

			$('#gmap_geocoding_btn').click();
		});

		var getTotal = function() {
			var arrayLabels = $('div.icheck-list > label input:checked').parents('label');
			var total = 0;
			arrayLabels.each(function(){
				var cost = parseInt($(this).attr('cost'));
				total += cost;
			});
			$('#paymentTotal').val(total);
			return total;
		}
		$('.form-group-clear').click(function(){
			$(this).parent().find("input[name='services[]']").prop("checked",false);
			calculateTotalAmount();
		});

		$('#select-radio-principal > label').on('ifChecked', function() {
			var days = $(this).attr('days');
			var cost = parseInt($(this).attr('cost'));
			var total = getTotal();

			$('.show-payment-principal').find('.payment-days').html(days);
			$('.show-payment-principal').find('.payment-cost').html(cost);
			$('label.show-payment-principal').show();
			$('.total-payment').html(total);
		});

		$('#select-radio-similars > label').on('ifChecked', function() {
			var days = $(this).attr('days');
			var cost = parseInt($(this).attr('cost'));
			var total = getTotal();

			$('.show-payment-similares').find('.payment-days').html(days);
			$('.show-payment-similares').find('.payment-cost').html(cost);
			$('label.show-payment-similares').show();
			$('.total-payment').html(total);
		});

		$('#select-radio-anuncio > label').on('ifChecked', function() {
			var days = $(this).attr('days');
			var cost = parseInt($(this).attr('cost'));
			var total = getTotal();

			$('.show-payment-anuncio').find('.payment-days').html(days);
			$('.show-payment-anuncio').find('.payment-cost').html(cost);
			$('label.show-payment-anuncio').show();
			$('.total-payment').html(total);
		});

		$('#select-radio-banner > label').on('ifChecked', function() {
			var days = $(this).attr('days');
			var cost = parseInt($(this).attr('cost'));
			var total = getTotal();

			$('.show-payment-banner').find('.payment-days').html(days);
			$('.show-payment-banner').find('.payment-cost').html(cost);
			$('label.show-payment-banner').show();
			$('.total-payment').html(total);
		});

		$('#select-radio-busqueda > label').on('ifChecked', function() {
			var days = $(this).attr('days');
			var cost = parseInt($(this).attr('cost'));
			var total = getTotal();

			$('.show-payment-busqueda').find('.payment-days').html(days);
			$('.show-payment-busqueda').find('.payment-cost').html(cost);
			$('label.show-payment-busqueda').show();
			$('.total-payment').html(total);
		});

		var accordionHeaderColor = '#19BDF4';
		if ($('#accordion .collapse.in').length > 0) {
			$('#accordion .collapse.in').parent().children().first().css('background', accordionHeaderColor);
			//var paymentType = $('#accordion .collapse.in').parent().children().first().find('a').html().trim();
            var paymentType = $('#accordion .collapse.in').parent().children().first().find('a').attr("rel").trim();
            if(paymentType == "TIGOMONEY"){
                $('#accordion input[name="linea"]').attr("required", true);
            }else{
                $('#accordion input[name="linea"]').attr("required", false);
            }
			$('#accordion input[name="paymentType"]').val(paymentType);
		}

		$('#accordion').on('shown.bs.collapse', function () {
			$('#accordion .collapse.in').parent().children().first().css('background', accordionHeaderColor);
			//var paymentType = $('#accordion .collapse.in').parent().children().first().find('a').html().trim();
            var paymentType = $('#accordion .collapse.in').parent().children().first().find('a').attr("rel").trim();
            if(paymentType == "TIGOMONEY"){
                $('#accordion input[name="linea"]').attr("required", true);
            }else{
                $('#accordion input[name="linea"]').attr("required", false);
            }
			$('#accordion input[name="paymentType"]').val(paymentType);
		});
		$('#accordion').on('hide.bs.collapse', function () {
			$('#accordion .collapse.in').parent().children().first().css('background', '#F9F9F9');
            if(paymentType == "TIGOMONEY"){
                $('#accordion input[name="linea"]').attr("required", true);
            }else{
                $('#accordion input[name="linea"]').attr("required", false);
            }
			$('#accordion input[name="paymentType"]').val("NONE");
		});

		//$('#userName').prop('disabled', true);
		//$('#userNit').prop('disabled', true);

		if($('input:radio:checked').length > 0){
			//$('#userName').prop('disabled', false);
			//$('#userNit').prop('disabled', false);
		}

		/*$('#invoiced').on('change', function() {
			var disabled = true;
			if(this.checked) {
				disabled = false;
			}
			//$('#userName').prop('disabled', disabled);
			//$('#userNit').prop('disabled', disabled);
		});*/

		$("#form_payment_step1 .dashboard-stat, #form_payment .dashboard-stat").click(function (){
			$(".dashboard-stat").removeClass("red");

			$("input[name='groupradio']").parent().attr('class', '');
			$("input[name='groupradio']").attr('checked', '');

			$(this).addClass("red");
			$(this).find("input").parent().attr('class', 'checked');
			$(this).find("input").attr('checked', 'checked');

			var ser_id = $(this).attr('rel');
			var total = $(this).attr('data-price');
			$("#select_subscription").attr('value', ser_id);
			$("#total_subscription").html(total);
			$('input[name="anuncio"]').val(ser_id);

			var sum = 0;
			$(".select-service option:selected").each(function(key, value) {
				console.log($(value).attr('data-price'));

				sum += Number($(value).attr('data-price'));
			})

            //var descuento = $('input[name="descuento"]').val();
            var total_sin_discount = sum + Number(total);
            var descuento = $('#descuentoAjax').val();
            descuento = (descuento / 100) * (sum + Number(total));
			sum = sum + Number(total) - descuento;

			$("#total_payment").html(sum);

            if(descuento != 0){
                $('#total_discount').html(descuento.toFixed(2));
                $('#descuento').val(descuento.toFixed(2));
                $('input[name="paymentTotalNew"]').val(sum);
                $('input[name="paymentTotal"]').val(total_sin_discount);
            }else{
                $('input[name="paymentTotal"]').val(sum);
            }
		});

		$("#total_payment").html(0);
		$(".select-service_DEPRECATED").change(function (){
			var sum = 0;
			$(".select-service option:selected").each(function(key, value) {
				sum += Number($(value).attr('data-price'));
			})

			var aux = Number($(".dashboard-stat.green.red").attr('data-price')) | 0;
            var descuento = $('#descuentoAjax').val();
            descuento = (descuento / 100) * (aux + sum);
			var total = aux + sum - descuento;
            var total_sin_discount = aux + sum;

			$("#total_payment").html(total);
            if(descuento != 0){
                $('#total_discount').html(descuento.toFixed(2));
                $('#descuento').val(descuento.toFixed(2));
                $('input[name="paymentTotalNew"]').val(total);
                $('input[name="paymentTotal"]').val(total_sin_discount);
            }else{
                $('input[name="paymentTotal"]').val(total);
            }
		});
		$(document).on("change","input[name='services[]'],input[name=principal],input[name=busqueda],input[name=similares]",function(){
			calculateTotalAmount();
		});

		$(document).on("click","a[data-service-type]",function(){
			var serviceType = $(this).data("service-type");
			var serviceTitle = $(this).data("service-title");
			$("img."+serviceType+"-service")
					.removeClass("hide")
					.siblings().addClass("hide");
			$("#service-title-example").text(serviceTitle);
		});
		$("#imprime").click(function (){
			imprimir();
		});

		$("#form_payment_step1").on("submit",function(){
			$(this).find("input[type=submit]").addClass("hide");
			$("#payment-progress-bar").removeClass("hide");
			progressBarAnimated();
		});
		var i = 1;
		function progressBarAnimated ()
		{
			setTimeout(function ()
			{
				$(".progress-bar-striped").css("width",i+"%");
				i++;
				if (i < 100)
				{
					progressBarAnimated();
				}
			}, 150);
		}

		function imprimir(){
			var objeto=document.getElementById('imprimir_html');  //obtenemos el objeto a imprimir
			var ventana=window.open('',"PrintWindow","width=450,height=550,top=100,left=250,toolbars=no,scrollbars=no,status=no,resizable=yes");  //abrimos una ventana vacía nueva
			ventana.document.write(objeto.innerHTML);  //imprimimos el HTML del objeto en la nueva ventana
			ventana.document.close();  //cerramos el documento
			//ventana.print();  //imprimimos la ventana
			//ventana.close();  //cerramos la ventana
		}
		
		$("#printPublicationList").click(function () {
			printPublications();
		});
		
		function printPublications() {
			var objeto=document.getElementById('print_publications_html');  //obtenemos el objeto a imprimir
			var ventana=window.open('',"PrintWindow","width=990,height=800,top=100,left=250, toolbars=no, scrollbars=yes, status=no, resizable=yes");  //abrimos una ventana vacía nueva
			ventana.document.write(objeto.innerHTML);  //imprimimos el HTML del objeto en la nueva ventana
			ventana.document.close();  //cerramos el documento
		}

        $('#input-title').keypress(function(event) {
            $('#description-preview').html($(this).val());
        });

		$('#input-title').change(function(event) {
            $('#description-preview').html($(this).val());
        });
		$("button.property-save").on("click",function(e){
			$(".map-error-message").text("");
			if($("#latitude").val() == "")
			{
				e.preventDefault();
                $('a[href="#tab_general"]').trigger("click");
				$(".map-error-message").text("DEBE INGRESAR UNA UBICACION");
				$('html, body').animate({
					scrollTop: $(".map-error-message").offset().top-90
				}, 2000);
			}

		});
        $('#myTextbox').keyup('input', function() {
    		$('#description-preview').html($(this).val());
		});

		$('#myTextbox').keydown('input', function() {
    		$('#description-preview').html($(this).val());
		});
		
        $('#categories').change(function(event){
			$('#categoria-preview').html($("#categories option:selected").text());
        });

        $('#type-select').change(function(event){
			$('#tipo-preview').html('EN ' + $("#type-select option:selected").text());;
        });

        $('#vendido').change(function(event){
            var status = $(this).val();
            if(status == "No"){
                $("#responsive").modal();
            }
        });
	});
</script>
<script>

</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>