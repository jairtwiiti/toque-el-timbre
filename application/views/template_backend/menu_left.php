
<?php
$user = $this->user;
$aux = $user->usu_tipo;
$arrayMenu = array(
                    //ADMIN MENU
                    array("name" => "Inicio", "usersAllowed" => "Admin", "url" => "admin/Home", "securityString" => "admin_home", "active" => "1" ,"icon" => "icon-home"),
                    array("name" => "Usuarios", "usersAllowed" => "Admin", "url" => "admin/User", "securityString" => "admin_user", "active" => "1" ,"icon" => "icon-users"),
                    array("name" => "Pagos", "usersAllowed" => "Admin", "url" => "admin/Payment", "securityString" => "admin_payment", "active" => "1" ,"icon" => "fa fa-money"),
                    array("name" => "Proyectos", "usersAllowed" => "Admin", "url" => "admin/Project", "securityString" => "admin_project", "active" => "1" ,"icon" => "fa fa-folder-open-o"),
                    array("name" => "Publicaciones", "usersAllowed" => "Admin", "url" => "admin/Property", "securityString" => "admin_property", "active" => "1" ,"icon" => "fa fa-file-text-o"),
                    array("name" => "Busquedas", "usersAllowed" => "Admin", "url" => "admin/NotifySearch", "securityString" => "admin_notify_search", "active" => "1" ,"icon" => "fa fa-search"),
                    array("name" => "Mensajes", "usersAllowed" => "Admin", "url" => "admin/Message", "securityString" => "admin_messages", "active" => "1" ,"icon" => "fa fa-envelope"),
                    array("name" => "Leads", "usersAllowed" => "Admin", "url" => "admin/Lead", "securityString" => "admin_leads", "active" => "1" ,"icon" => "fa fa-list-alt"),
                    array("name" => "Services", "usersAllowed" => "Admin", "url" => "admin/Service", "securityString" => "admin_services", "active" => "1" ,"icon" => "fa fa-list-alt"),
                    //PUBLIC MENU
                    array("name" => "Inicio", "usersAllowed" => "Particular,Inmobiliaria,Constructora,Buscador", "url" => "dashboard", "securityString" => "home", "active" => "1" ,"icon" => "icon-home"),
                    array("name" => "Mi cuenta", "usersAllowed" => "Particular,Inmobiliaria,Constructora,Buscador", "url" => "dashboard/edit-profile", "securityString" => "profile", "active" => "1", "icon" => "glyphicon glyphicon-user"),
                    array("name" => "Mis Mensajes", "usersAllowed" => "Particular,Inmobiliaria,Constructora,Buscador", "url" => "dashboard/messages", "securityString" => "inbox", "active" => "1", "icon" => "glyphicon glyphicon-envelope"),
                    array("name" => "Mis Anuncios", "usersAllowed" => "Particular,Inmobiliaria,Constructora,Buscador", "url" => "admin/Property","securityString" => "admin_property", "active" => "1", "icon" => "glyphicon glyphicon-bullhorn"),
                    array("name" => "Mis Pagos", "usersAllowed" => "Particular,Inmobiliaria,Constructora,Buscador", "url" => "dashboard/payments","securityString" => "payment", "active" => "1", "icon" => "fa fa-credit-card"),
                    array("name" => "Mis Suscripciones", "usersAllowed" => "Particular,Inmobiliaria,Constructora,Buscador", "url" => "dashboard/subscriptions","securityString" => "subscription", "active" => "0", "icon" => "fa fa-folder-open-o"),
                    array("name" => "Comprar Suscripcion", "usersAllowed" => "Inmobiliaria", "url" => "dashboard/payment/subscription","securityString" => "payment_subscription", "active" => "0", "icon" => "fa fa-shopping-cart"),
                    array("name" => "Mis Proyectos", "usersAllowed" => "Constructora", "url" => "dashboard/projects","securityString" => "dashboard/projects", "active" => "1", "icon" => "icon-paper-plane")
                    );
?>
<div class="page-sidebar navbar-collapse collapse">
<!-- BEGIN SIDEBAR MENU -->
    <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
    <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
    <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
    <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <ul class="page-sidebar-menu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">

        <?php
        $i=0;
        foreach($arrayMenu as $menu)
        {
            $menu = (object)$menu;
            $name = $menu->name;
            $active = $page == $menu->securityString?"active":"";
            $url = base_url($menu->url);
            $icon = $menu->icon;
            $usersAllowed = $menu->usersAllowed;
            $start = $i==0?"start":"";
            $menuItem = "
            <li class='".$start." ".$active."'>
                <a href='".$url."'>
                    <i class='".$icon."'></i>
                    <span class='title'>".$name."</span>
                    <span class='selected'></span>
                </a>
            </li>
            ";
            if(is_numeric(strpos($usersAllowed,$aux)) && $menu->active == 1)
            {
                echo $menuItem;
            }
            $i++;
        }
        ?>
    </ul>
<!-- END SIDEBAR MENU -->
</div>