<?php
$path_assets = base_url() . "assets/";
?>
<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner">
        <?= date('Y')?>  &copy; Toqueeltimbre.com. Todos los derechos reservados.
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
<!-- END FOOTER -->