<?php
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 3/7/2017
 * Time: 13:53 PM
 */
?>
<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-pencil"></i>
            <span class="caption-subject font-green-sharp bold uppercase">Galeria</span>
            <span class="caption-helper"><a href="<?=base_url("buscar/inmuebles-en-bolivia/".$property["inm_seo"])?>" target="_blank"><?=$property["inm_nombre"]?></a></span>
        </div>
        <?php $this->load->view("admin/property/form-buttons-action");?>
    </div>
    <div class="portlet-body">
        <div class="tmp_message">
            <?php
            $this->load->view("flash-data-basic-messages")
            ?>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-info">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                        &times;
                    </button>
                    <i class="fa fa-info-circle sign"></i><strong>Info!</strong>
                    Para subir imágenes arrastre una o mas fotos en la seccion de <b>Drop files</b>, puedes subir <b>hasta 15 fotos al mismo tiempo.</b>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <button type="button" class="btn btn-primary col-md-6 dropzone-add-to-queue">Agregar <i class="fa fa-plus" aria-hidden="true"></i></button>
                <button type="button" class="btn btn-danger col-md-6 dropzone-start-queue">Guardar <i class="fa fa-upload" aria-hidden="true"></i></button><br><br>
                <input type="hidden" name="property-id" value="<?=$property["inm_id"]?>">
                <input type="hidden" name="publication-id" value="<?=$publication["pub_id"]?>">
                <input type="hidden" name="throw-ad" value="<?=$this->session->flashdata('throwAd')?>">
                <input type="hidden" name="throw-last-step-explanation" value="<?=$this->session->flashdata('lastStepExplanation')?>">
                <form name="upload-property-images" id="my-dropzone" action="<?=base_url("admin/Property/images/".$property["inm_id"])?>" class="dropzone">
                    <div class="fallback">
                        <input name="file" type="file" multiple />
                    </div>
                </form>
            </div>
            <div class="col-md-8">
                <div class="alert alert-info">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                        &times;
                    </button>
                    <i class="fa fa-info-circle sign"></i><strong>Info!</strong> Después de guardar las imágenes, puede arrastrarlas para cambiar su orden.
                </div>
                <div class="row sortable list-group">
                </div>
            </div>
        </div>
    </div>
</div>
<script id="ht-image-item" type="text/x-handlebars-template">
        <div class="col-md-2 image-item" id="{{item.fot_id}}">
            <div class="thumbnail">
                    <img data-src="holder.js/300x300"
                         src="{{item.src}}"
                         class="profile-avatar" id="avatar-preview" style="max-height:100px">
                <div class="caption">
                    <div data-property-image-id="{{item.fot_id}}">
                        <a class="btn btn-primary btn-xs property-image-see-{{item.fot_id}}" href="#" title="" data-original-title="VER" data-toggle="tooltip" data-placement="top"><i class="fa fa-search"></i></a>
                        <a class="btn btn-danger btn-xs property-image-delete-{{item.fot_id}}" href="#" title="" data-original-title="ELIMINAR" data-toggle="tooltip" data-placement="top"><i class="fa fa-times"></i></a>
                    </div>
                </div>
            </div>
        </div>
</script>