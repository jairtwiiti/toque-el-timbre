<?php
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 04/12/2018
 * Time: 09:33 AM
 */
?>
    <div class="tmp_message">
        <?php
        $this->load->view("flash-data-basic-messages")
        ?>
    </div>
    <form name="checkout-form" method="post" autocomplete="off" data-parsley-validate>
        <div class="row">
            <div class="col-md-offset-2 col-md-8">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i> Mejora tu publicacion
                        </div>
                        <div class="tools"></div>
                    </div>
                    <div class="portlet-body form">
                        <div class="form-body">
                            <div class="form-group">
                                 <div class="row">
                                    <div class="col-md-3"></div>
                                    <input type="hidden" value="" name="anuncio" value="">
                                    <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
                                        <div class="dashboard-stat green" style="height: 122px; cursor: pointer">
                                            <div class="visual">
                                                <!-- <i class="fa fa-home"></i> -->
                                            </div>
                                            <div class="details2">
                                                <div class="published">
                                                    Tu anuncio ya esta publicado
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label><b>Puedes destacar tu anuncio publicando en nuestros espacios especiales:</b></label>
                                <div class="portlet box white checkout">
                                    <div class="portlet-body form">
                                        <div class="form-body">
                                            <div class="panel-group accordion" id="accordion" role="tablist" aria-multiselectable="true">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading" role="tab" id="headingOne">
                                                        <h4 class="panel-title">
                                                            <a class="accordion-toggle accordion-toggle-styled " data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-controls="collapseOne">
                                                                <i class="fa fa-arrow-right"></i> DESTACAR ANUNCIO
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne" class="panel-collapse" role="tabpanel" aria-labelledby="headingOne">
                                                        <div class="panel-body">
                                                            <div id="html_pagos">
                                                                <div class="form-body" id="service-list">
<!--                                                                    Here goes the services -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" id="customer-information" style="display: none;">
                                            <div class="col-md-12 hide">
                                                <div class="row">
                                                    <div class="col-md-6 pull-right">
                                                        <div class="row">
                                                            <div class="col-md-12 mensaje_cupon"></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="input-group pull-right">
                                                                    <a class="btn btn-info blue" href="javascript:;" id="canjear_cupon" rel="<?php echo base_url(); ?>dashboard/payment/validar_cupon" style="max-width: 300px;">Carjear Cupon</a>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="input-group pull-right">
                                                                    <input type="text" name="coupon" class="form-control" >
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div style="text-align: right;padding-right: 15px;">
                                                <input id="paymentTotal" type="hidden" name="paymentTotal" value="0">
                                                <input id="paymentTotalNew" type="hidden" name="paymentTotalNew" value="0">
                                                <input id="descuentoAjax" type="hidden" name="descuentoAjax" value="0">
                                                <input id="descuento" type="hidden" name="descuento" value="0">
                                                <input id="descuentoId" type="hidden" name="descuentoId" value="0">
                                                <input type="hidden" name="concepto" value="<?php echo $concepto; ?>">
                                                <h4 class="hide">Descuento: &nbsp;-<span id="total_discount">0</span> Bs.</h4>
                                                <h4>Total: &nbsp;<span id="total_payment">0</span> Bs.</h4>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label><b>DATOS DE FACTURACIÓN</b></label>
                                                    <br>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="input-group">
                                                                <input type="text" name="userName" class="form-control" placeholder="Nombre Completo" id="userName" required>
                                                                <span class="input-group-addon">
																<i class="fa fa-user"></i>
															</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="input-group">
                                                                <input type="text" name="userNit" class="form-control" id="userNit" placeholder="Nro. de NIT/C.I:" required>
                                                                <span class="input-group-addon">
																<i class="fa fa-slack"></i>
															</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="pull-right" id="buttons-action">
                                    <a class="btn btn-info blue see-my-publishments" href="<?php echo base_url(); ?>dashboard/publications" title="Aceptar">Ver mis publicaciones</a>
                                    <input class="btn btn-info blue hide" type="submit" value="Pagar" name="btnPagar">
                                    <div class="progress no-margin hide" id="payment-progress-bar">
                                        <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width:0%"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--                            </form>-->
                    </div>
                </div>
            </div>
        </div>
    </form>
<!--<div class="portlet light">-->
<!--    <div class="portlet-body">-->
<!--        -->
<!--    </div>-->
<!--</div>-->
<?php
$this->load->view("handlebar-template/ht-services-list");
?>