<?php
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 12/11/2017
 * Time: 06:43 PM
 */
?>
<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-plus"></i>
            <span class="caption-subject font-green-sharp bold uppercase">Agregar inmueble</span>
        </div>
    </div>
    <div class="portlet-body">
        <div class="tmp_message">
            <?php
            $this->load->view("flash-data-basic-messages")
            ?>
        </div>
        <form name="add-project-form" method="post" enctype="multipart/form-data" autocomplete="off" data-parsley-validate>
            <div class="hide">
                <input name="latitude" value="<?=set_value('latitude')?>" type="text" required>
                <input name="longitude" value="<?=set_value('longitude')?>" type="text" required data-parsley-errors-container="#map-location-error-message-parsley" data-parsley-error-message="Debe marcar un punto en el mapa">
                <input type="hidden" name="property-type-feature-list" value='<?=json_encode((array)$arrayPropertyTypeFeatures)?>'>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <label class="required" data-field="name">Titulo <span class="required">*</span></label>
                                        <input type="text" name="name" required parsley-trigger="change" placeholder="Titulo del inmueble" class="form-control" value="<?=set_value('name')?>">
                                    </div>
                                </div>
                                <div class="col-md-12 col-lg-12">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="required" data-field="property-type">Tipo de inmueble</label>
                                                <select class="select2" name="property-type">
                                                    <?php
                                                    $options = "";
                                                    foreach($arrayPropertyTypes as $propertyType)
                                                    {
                                                        $options .= '<option value="'.$propertyType->getId().'"  >'.$propertyType->getName().'</option>';
                                                    }
                                                    echo $options;
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="required" data-field="transaction-type">Tipo de transacción</label>
                                                <select class="select2" name="transaction-type">
                                                    <?php
                                                    $options = "";
                                                    foreach($arrayTransactionTypes as $key => $transactionType)
                                                    {
                                                        $options .= '<option value="'.$key.'" >'.$transactionType.'</option>';
                                                    }
                                                    echo $options;
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="required" data-field="transaction-type">Estado</label>
                                                <select class="select2" required name="status">
                                                    <option value=""></option>
                                                    <option value="0" <?=set_select('status', '0')?>>Pre venta</option>
                                                    <option value="1" <?=set_select('status', '1')?>>A estrenar</option>
                                                    <option value="2" <?=set_select('status', '2')?>>Buen estado</option>
                                                    <option value="3" <?=set_select('status', '3')?>>Requiere mantenimiento</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-lg-4">
                                            <div class="form-group">
                                                <label class="required" data-field="state">Departamento</label>
                                                <select class="select2" name="state">
                                                    <?php
                                                    $options = "";
                                                    foreach($arrayState as $stateId => $stateName)
                                                    {
                                                        $selected = $city["ciu_dep_id"] == $stateId?"selected":"";
                                                        $options .= '<option value="'.$stateId.'" '.$selected.' >'.$stateName.'</option>';
                                                    }
                                                    echo $options;
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-lg-4">
                                            <div class="form-group">
                                                <?php
                                                $select2Data = "";
                                                if(isset($city))
                                                {
                                                    $select2Data["id"] = $city["ciu_id"];
                                                    $select2Data["text"] = $city["ciu_nombre"];
                                                    $select2Data = json_encode($select2Data);
                                                }
                                                ?>
                                                <label>Ciudad</label>
                                                <input type="text" data-parsley-required id="ajax-get-cities" name="city" parsley-trigger="change" value='<?=$select2Data?>'>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-lg-4">
                                            <div class="form-group">
                                                <?php
                                                $select2Data = "";
                                                if(isset($zone))
                                                {
                                                    $select2Data["id"] = $zone["zon_id"];
                                                    $select2Data["text"] = $zone["zon_nombre"];
                                                    $select2Data = json_encode($select2Data);
                                                }
                                                ?>
                                                <label>Zona</label>
                                                <input type="text" data-parsley-required id="ajax-get-zones" name="zone" parsley-trigger="change" value='<?=$select2Data?>'>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-lg-12">
                                            <div class="form-group">
                                                <label>Dirección <span class="required">*</span></label>
                                                <input type="text" name="address" required parsley-trigger="change" placeholder="Direccion del inmueble" class="form-control" value="<?=set_value('address')?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-lg-12">
                                    <label class="required" data-field="name">
                                        <div class="input-group">
                                            <input type="text" class="form-control search-address-data" placeholder="Ingrese ubicacion">
                                            <span class="input-group-addon search-address-data-city hidden-xs <?=isset($city["ciu_id"])?"":"hide"?>"><?=$city["ciu_nombre"]?></span>
                                            <span class="input-group-addon search-address-data-state hidden-xs">Santa Cruz</span>
                                        <span class="input-group-btn">
                                            <button class="btn btn-primary search-address-button" type="button">Buscar</button>
                                        </span>
                                        </div>
                                    </label><!-- /input-group -->
                                    <div class="map-fancy-framework">
                                        <div id="maps" style="height: 300px;width: auto">
                                        </div>
                                        <em class="map-search-message"></em>
                                        <div id="map-location-error-message-parsley"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="required" data-field="description">Descripción <span class="required">*</span></label>
                                        <textarea name="description" class="form-control" rows="5" required placeholder="Ingrese la descripcion de su inmueble"><?=set_value('description')?></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <label class="required" data-field="name">UV</label>
                                        <input type="text" name="uv" parsley-trigger="change" placeholder="" class="form-control" value="<?=set_value('uv')?>">
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <label class="required" data-field="name">Manzano</label>
                                        <input type="text" name="manzano" parsley-trigger="change" placeholder="" class="form-control" value="<?=set_value('manzano')?>">
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-6">
                                    <?php
                                    $inputMasked = $this->agent->is_mobile()?"number":"text";
                                    ?>
                                    <script type="application/javascript">
                                        var inputMasked = '<?=$inputMasked?>'
                                    </script>
                                    <div class="form-group">
                                        <label class="required" data-field="name">Superficie <span class="required">*</span></label>
                                        <input type="<?=$inputMasked?>" name="surface" required parsley-trigger="change" placeholder="Superficie" class="form-control input-masked" value="<?=set_value('surface')?>" data-inputmask="'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true">
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <label>Tipo de superficie</label>
                                        <div class="mt-radio-inline">
                                            <label class="mt-radio">
                                                <input type="radio" name="surface-type" data-type="mts2" value="Metros Cuadrados" checked required data-parsley-errors-container="#surface-type-error-message-parsley"> Metros Cuadrados
                                                <span></span>
                                            </label>
                                            <label class="mt-radio">
                                                <input type="radio" name="surface-type" data-type="has" value="Hectareas"> Hectareas
                                                <span></span>
                                            </label>
                                        </div>
                                        <div id="surface-type-error-message-parsley"></div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-6">

                                    <div class="form-group">
                                        <label class="required" data-field="name">Precio <span class="required">*</span></label>
                                        <input type="<?=$inputMasked?>" name="price" required parsley-trigger="change" placeholder="Precio" class="form-control input-masked" value="<?=set_value('price')?>" data-inputmask="'alias': 'integer', 'groupSeparator': ',', 'autoGroup': true">
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <label>Moneda</label>
                                        <div class="mt-radio-inline">
                                            <label class="mt-radio">
                                                <input type="radio" name="currency-type" data-type="bs" value="1" required data-parsley-errors-container="#currency-error-message-parsley"> Bolivianos
                                                <span></span>
                                            </label>
                                            <label class="mt-radio">
                                                <input type="radio" name="currency-type" data-type="usd" checked value="2"> Dolares
                                                <span></span>
                                            </label>
                                        </div>
                                        <div id="currency-error-message-parsley"></div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-list-alt"></i>
                                            <span class="caption-subject font-green-sharp bold uppercase">Características especificas</span><br><br>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="row" id="specific-features"></div>
                                </div>
                                <div class="col-md-12">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-list-alt"></i>
                                            <span class="caption-subject font-green-sharp bold uppercase">Amenidades</span><br><br>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="mt-checkbox-inline">
                                            <?php
                                                $checkbox = "";
                                                foreach ($amenityList as $amenity)
                                                {
                                                    $checkbox.= '
                                                    <label class="mt-checkbox">
                                                        <input type="checkbox" name="amenities[]" value="'.$amenity['code'].'" data-parsley-errors-container="#currency-error-message-parsley"> '.$amenity['name'].'
                                                        <span></span>
                                                    </label>
                                                    ';
                                                }
                                                echo $checkbox;
                                            ?>
                                        </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary">
                        Guardar
                    </button>
                    <a href="<?=$cancelUrl?>" class="btn btn-danger" role="button">Cancelar</a>
                </div>
            </div>
        </form>
    </div>
</div>
<?php
$this->load->view("handlebar-template/property-features-snippet");
?>