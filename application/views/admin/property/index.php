<?php
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 06/09/2017
 * Time: 3:02 PM
 */
?>
<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-table"></i>
            <span class="caption-subject font-green-sharp bold uppercase">Publicaciones</span>
        </div>
    </div>
    <div class="portlet-body">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <form class="form-group" id="extra-request-data">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="radio-inline">
                                                <input type="radio" name="date-to-filter" value="start" checked> Fecha de inicio
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="date-to-filter" value="end"> Fecha de fin
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="basic-addon1">Desde</span>
                                            <input type="text" name="start-date" data-date-format="dd-mm-yyyy" parsley-trigger="change" placeholder="dia-mes-año" readonly class="form-control date-picker">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="basic-addon1">Hasta</span>
                                            <input type="text" name="end-date" data-date-format="dd-mm-yyyy" parsley-trigger="change" placeholder="dia-mes-año" readonly class="form-control date-picker" value="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="checkbox-inline">
                                        <input type="checkbox" name="show-expired" value="1"> Expirados
                                    </label>
                                </div>
                            </div>
                            <?php
                            if($userType == "Admin")
                            {
                            ?>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?php
                                    $select2Data = "";
                                    if(isset($userProject))
                                    {
                                        $select2Data["id"] = $userProject["usu_id"];
                                        $select2Data["text"] = $userProject["usu_nombre"]." ".$userProject["usu_apellido"]." ".$userProject["usu_email"];
                                        $select2Data = json_encode($select2Data);
                                    }
                                    ?>
                                    <input class="form-inline" type="text" data-parsley-required id="ajax-get-users" name="user" parsley-trigger="change" value='<?=$select2Data?>'>
                                </div>
                            </div>
                            <?php
                            }
                            ?>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary input-sm" id="send-filters" type="button" data-content-data="chart-property-offers-based-on-property-types">Filtrar</button>
                            <button class="btn btn-danger input-sm" id="remove-additional-parameters" type="button" data-content-data="chart-property-offers-based-on-property-types">Remove parametros adicionales</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover data-table-property" id="data-table">
                            <thead>
                            <tr>
                                <th class="text-center">PUPLICATION ID</th>
                                <th>Nombre inmueble</th>
                                <th>Visitas</th>
                                <th>Publicado</th>
                                <th>Publicado</th>
                                <th>Fech. inicio</th>
                                <th>Fech. fin</th>
                                <th>Opciones</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script id="ht-record-detail" type="text/x-handlebars-template">
    <div class="row">
        <div class="col-md-5">
            <img src="{{detail.src}}" class="img-thumbnail">
        </div>
        <div class="col-md-7">
            <p>
                <b>Publication ID:</b> {{detail.publicationId}}<br>
                <b>Nombre:</b> {{detail.propertyName}}<br>
                <b>Fecha creacion:</b> {{detail.publicationCreatedOn}}<br>
                <b>Visitas:</b> {{detail.propertyVisits}}<br>
                <b>Inicia:</b> {{detail.publicationStartValidation}}<br>
                <b>Finaliza:</b> {{detail.publicationEndValidation}}
            </p>
        </div>
    </div>
</script>