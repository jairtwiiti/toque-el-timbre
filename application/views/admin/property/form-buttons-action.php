<?php
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 07/09/2017
 * Time: 11:17 AM
 */
$url = base_url("admin/Property");
$propertyId = $property["inm_id"];
$formButtonsAction["edit"] = array("icon" => "fa fa-pencil", "title" => "EDITAR", "url" => $url."/edit/".$propertyId);
$formButtonsAction["images"] = array("icon" => "fa fa-file-image-o", "title" => "IMAGENES", "url" => $url."/images/".$propertyId);
$formButtonsAction["leads"] = array("icon" => "fa fa-list-alt", "title" => "LEADS", "url" => $url."/leads/".$propertyId);
?>
<div class="actions">
    <?php
        $buttons = "";
        foreach($formButtonsAction as $key => $value)
        {
            $style = $key == $currentAction?"danger":"primary";
            $buttons .= '
                <a class="btn btn-circle btn-icon-only btn-'.$style.'" href="'.$value["url"].'" data-original-title="'.$value["title"].'" data-toggle="tooltip" data-placement="top">
                    <i class="'.$value["icon"].'"></i>
                </a>
            ';
        }
        echo $buttons;
    ?>
</div>
