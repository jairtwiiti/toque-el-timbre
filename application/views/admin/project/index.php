<?php
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 28/7/2017
 * Time: 8:45 AM
 */
?>
<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-table"></i>
            <span class="caption-subject font-green-sharp bold uppercase">Proyectos</span>
        </div>
    </div>
    <div class="portlet-body">
        <div class="tmp_message">
            <?php $this->load->view("flash-data-basic-messages")?>
        </div>
        <div class="row">
            <div class="col-md-12">
                <button type="button" class="btn btn-primary" onclick="parent.location = 'Project/add'">
                    Agregar proyecto
                </button>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover" id="data-table">
                        <thead>
                        <tr>
                            <th class="text-center">PROJECT ID</th>
                            <th>Nombre</th>
                            <th>Estado</th>
                            <th>Fecha<br>creacion</th>
                            <th>Visitas</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script id="ht-project-detail" type="text/x-handlebars-template">
    <div class="row">
        <div class="col-md-5">
            <img src="{{detail.src}}" class="img-thumbnail">
        </div>
        <div class="col-md-7">
            <p>
                <b>Project ID:</b> {{detail.proy_id}}<br>
                <b>Nombre:</b> {{detail.proy_nombre}}<br>
                <b>Fecha creacion:</b> {{detail.proy_creado}}<br>
                <b>Visitas:</b> {{detail.proy_visitas}}<br>
                <b>Inicia:</b> {{detail.proy_vig_ini}}<br>
                <b>Finaliza:</b> {{detail.proy_vig_fin}}
            </p>
        </div>
    </div>
</script>