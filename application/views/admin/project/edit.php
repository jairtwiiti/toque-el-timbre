<?php
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 2/7/2017
 * Time: 05:17 PM
 */
?>
<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-pencil"></i>
            <span class="caption-subject font-green-sharp bold uppercase">Editar proyecto</span>
            <span class="caption-helper"><a href="<?=base_url($project["proy_seo"])?>" target="_blank"><?=$project["proy_nombre"]?></a></span>
        </div>
        <?php $this->load->view("admin/project/form-buttons-action");?>
    </div>
    <div class="portlet-body">
        <div class="tmp_message">
            <?php
            $this->load->view("flash-data-basic-messages")
            ?>
        </div>
        <form name="edit-project-form" method="post" enctype="multipart/form-data" data-parsley-validate>
            <div class="hide">
                <input name="latitude" value="<?=set_value('latitude',$project["proy_latitud"])?>" type="text" required>
                <input name="longitude" value="<?=set_value('longitude',$project["proy_longitud"])?>" type="text" required data-parsley-errors-container="#map-location-error-message-parsley" data-parsley-error-message="Debe marcar un punto en el mapa">
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <?php
                                        $select2Data = "";
                                        if(isset($userProject))
                                        {
                                            $select2Data["id"] = $userProject["usu_id"];
                                            $select2Data["text"] = $userProject["usu_nombre"]." ".$userProject["usu_apellido"]." ".$userProject["usu_email"];
                                            $select2Data = json_encode($select2Data);
                                        }
                                        ?>
                                        <label>Usuario</label>
                                        <input type="text" data-parsley-required id="ajax-get-users" name="user" parsley-trigger="change" value='<?=$select2Data?>'>
                                    </div>
                                </div>
                                <div class="col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <label class="required" data-field="name">Nombre</label>
                                        <input type="text" name="name" required parsley-trigger="change" placeholder="Nombre del Proyecto" class="form-control" value="<?=set_value('name',$project["proy_nombre"])?>">
                                    </div>
                                </div>
                                <div class="col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <label class="required" data-field="video">Video</label>
                                        <input type="text" name="video" parsley-trigger="change" placeholder="Video del Proyecto" class="form-control" value="<?=set_value('video',$project["proy_video"])?>">
                                    </div>
                                </div>
                                <div class="col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <?php
                                        $status = $project["proy_estado"];
                                        $postStatus = $this->input->post("status");
                                        if($postStatus === "0" || $postStatus != FALSE && is_numeric((int)$postStatus))
                                        {
                                            $status = $arrayStatus[$this->input->post("status")];
                                        }
                                        ?>
                                        <label>Estado</label>
                                        <div class="mt-radio-inline">
                                            <label class="mt-radio">
                                                <input type="radio" name="status" value="1" <?=$status == model_project::APPROVED?"checked":""?>> Aprobado
                                                <span></span>
                                            </label>
                                            <label class="mt-radio">
                                                <input type="radio" name="status" value="0" <?=$status == model_project::NOT_APPROVED?"checked":""?>> No aprobado
                                                <span></span>
                                            </label>
                                            <label class="mt-radio">
                                                <input type="radio" name="status" value="2" <?=$status == model_project::CANCELED?"checked":""?>> Cancelado
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-lg-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div>
                                                <label data-field="avatar">Logo</label>
                                            </div>
                                            <div class="avatar-upload">
                                                <div style="max-width: 130px; max-height: 130px; overflow:hidden">
                                                    <?php
                                                    $fileHandler = new File_Handler($project["proy_logo"],"projectImage");
                                                    $thumbnail = $fileHandler->getThumbnail(130,130);
                                                    ?>
                                                    <img src="<?=$thumbnail->getSource()?>" class="profile-avatar img-thumbnail" id="avatar-preview" alt="Click to change." title="Click to change"/>
                                                </div>
                                                <input id="avatar-file" type="file" name="logo" accept="image/*">
                                                <div id="progress" class="overlay"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <?php
                                                        $startValidityDate = date('d-m-Y', strtotime($project["proy_vig_ini"]));
                                                        ?>
                                                        <label data-field="start-date">Fecha de inicio (dia-mes-año)</label>
                                                        <input type="text" name="start-date" data-date-format="dd-mm-yyyy" parsley-trigger="change" placeholder="dia-mes-año" readonly class="form-control date-picker" value="<?=set_value('start-date',$startValidityDate)?>">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <?php
                                                        $finishValidityDate = date('d-m-Y', strtotime($project["proy_vig_fin"]));
                                                        ?>
                                                        <label data-field="finish-date">Fecha de fin (dia-mes-año)</label>
                                                        <input type="text" name="finish-date" data-date-format="dd-mm-yyyy" parsley-trigger="change" placeholder="dia-mes-año" readonly class="form-control date-picker" value="<?=set_value('finish-date',$finishValidityDate)?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-lg-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div>
                                                <label data-field="avatar">Imagen de ubicaion</label>
                                            </div>
                                            <div class="avatar-upload-2">
                                                <div style="max-width: 130px; max-height: 130px; overflow:hidden">
                                                    <?php
                                                    $fileHandler = new File_Handler($project["proy_ubicacion_img"],"projectImage");
                                                    $thumbnail = $fileHandler->getThumbnail(130,130);
                                                    ?>
                                                    <img src="<?=$thumbnail->getSource()?>" class="profile-avatar img-thumbnail" id="avatar-preview-2" alt="Click to change." title="Click to change"/>
                                                </div>
                                                <input id="avatar-file-2" type="file" name="logo-2" accept="image/*">
                                                <div id="progress" class="overlay"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-4 col-lg-12">
                                                    <div class="form-group">
                                                        <label class="required" data-field="state">Departamento</label>
                                                        <select class="select2" name="state">
                                                            <?php
                                                            $options = "";
                                                            foreach($arrayState as $stateId => $stateName)
                                                            {
                                                                $selected = $city["ciu_dep_id"] == $stateId?"selected":"";
                                                                $options .= '<option value="'.$stateId.'" '.$selected.' >'.$stateName.'</option>';
                                                            }
                                                            echo $options;
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-lg-12">
                                                    <div class="form-group">
                                                        <?php
                                                        $select2Data = "";
                                                        if(isset($city))
                                                        {
                                                            $select2Data["id"] = $city["ciu_id"];
                                                            $select2Data["text"] = $city["ciu_nombre"];
                                                            $select2Data = json_encode($select2Data);
                                                        }
                                                        ?>
                                                        <label>Ciudad</label>
                                                        <input type="text" data-parsley-required id="ajax-get-cities" name="city" parsley-trigger="change" value='<?=$select2Data?>'>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-lg-12">
                                                    <div class="form-group">
                                                        <?php
                                                        $select2Data = "";
                                                        if(isset($zone))
                                                        {
                                                            $select2Data["id"] = $zone["zon_id"];
                                                            $select2Data["text"] = $zone["zon_nombre"];
                                                            $select2Data = json_encode($select2Data);
                                                        }
                                                        ?>
                                                        <label>Zona</label>
                                                        <input type="text" data-parsley-required id="ajax-get-zones" name="zone" parsley-trigger="change" value='<?=$select2Data?>'>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-12 col-lg-12">
                                    <label class="required" data-field="name">
                                        <div class="input-group">
                                            <input type="text" class="form-control search-address-data" placeholder="Ingrese ubicacion">
                                            <span class="input-group-addon search-address-data-city <?=isset($city["ciu_id"])?"":"hide"?>"><?=$city["ciu_nombre"]?></span>
                                            <span class="input-group-addon search-address-data-state <?=isset($city["ciu_dep_id"])?"":"hide"?>"><?=$arrayState[$city["ciu_dep_id"]]?></span>
                                        <span class="input-group-btn">
                                            <button class="btn btn-primary search-address-button" type="button">Buscar</button>
                                        </span>
                                        </div>
                                    </label><!-- /input-group -->
                                    <div class="map-fancy-framework">
                                        <div id="maps" style="height: 300px;width: auto">
                                        </div>
                                        <em class="map-search-message"></em>
                                        <div id="map-location-error-message-parsley"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="required" data-field="description">Descripción</label>
                                        <textarea class="tinymce" name="description" placeholder="Ingrese la descripcion de su proyecto"><?=set_value('description',$project["proy_descripcion"])?></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="required" data-field="contact">Contacto</label>
                                        <textarea class="tinymce" name="contact" placeholder="Informacion de contacto"><?=set_value('contact',$project["proy_contacto"])?></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="required" data-field="address">Direccion</label>
                                        <textarea class="tinymce" name="address"><?=set_value('address',$project["proy_ubicacion"])?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary">
                        Guardar
                    </button>
                    <a href="<?=base_url("admin/Project")?>" class="btn btn-danger" role="button">Cancelar</a>
                </div>
            </div>
        </form>
    </div>
</div>