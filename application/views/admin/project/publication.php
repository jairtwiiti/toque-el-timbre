<?php
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 3/7/2017
 * Time: 13:53 PM
 */
?>
<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-pencil"></i>
            <span class="caption-subject font-green-sharp bold uppercase">Publicacion</span>
            <span class="caption-helper"><a href="<?=base_url($project["proy_seo"])?>" target="_blank"><?=$project["proy_nombre"]?></a></span>
        </div>
        <?php $this->load->view("admin/project/form-buttons-action");?>
    </div>
    <div class="portlet-body">
        <div class="tmp_message">
            <?php
            $this->load->view("flash-data-basic-messages")
            ?>
        </div>
        <form name="publication-project-form" method="post" enctype="multipart/form-data" data-parsley-validate>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="required" data-field="state">Tipo de propiedad</label>
                        <select class="select2" required name="property-type">
                            <option></option>
                            <?php
                            $options = "";
                            foreach($propertyTypeList as $propertyType)
                            {
                                $selected = $property["inm_cat_id"] == $propertyType->getId()?"selected":"";
                                $options .= '<option value="'.$propertyType->getId().'" '.$selected.' >'.$propertyType->getName().'</option>';
                            }
                            echo $options;
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="required" data-field="state">Tipo de transaccion</label>
                        <select class="select2" required name="transaction-type">
                            <option></option>
                            <?php
                            $options = "";
                            foreach($transactionTypeList as $transactionType)
                            {
                                $selected = $property["inm_for_id"] == $transactionType->getId()?"selected":"";
                                $options .= '<option value="'.$transactionType->getId().'" '.$selected.' >'.$transactionType->getType().'</option>';
                            }
                            echo $options;
                            ?>
                        </select>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Tipo de superficie</label>
                        <div class="mt-radio-inline">
                            <label class="mt-radio">
                                <input type="radio" name="surface-type" data-type="mts2" value="Metros Cuadrados" <?=$property["inm_tipo_superficie"] == "Metros Cuadrados"?"checked":""?> required data-parsley-errors-container="#surface-type-error-message-parsley"> Metros Cuadrados
                                <span></span>
                            </label>
                            <label class="mt-radio">
                                <input type="radio" name="surface-type" data-type="has" value="Hectareas" <?=$property["inm_tipo_superficie"] == "Hectareas"?"checked":""?>> Hectareas
                                <span></span>
                            </label>
                        </div>
                        <div id="surface-type-error-message-parsley"></div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Moneda</label>
                        <div class="mt-radio-inline">
                            <label class="mt-radio">
                                <input type="radio" name="currency-type" data-type="bs" value="1" <?=$property["inm_mon_id"] == "1"?"checked":""?> required data-parsley-errors-container="#currency-error-message-parsley"> Bolivianos
                                <span></span>
                            </label>
                            <label class="mt-radio">
                                <input type="radio" name="currency-type" data-type="usd" value="2" <?=$property["inm_mon_id"] == "2"?"checked":""?>> Dolares
                                <span></span>
                            </label>
                        </div>
                        <div id="currency-error-message-parsley"></div>
                    </div>
                </div>
            </div>
            <?php
            $inputMasked = $this->agent->is_mobile()?"number":"text";
            ?>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="required" data-field="name">Superficie</label>
                        <input type="<?=$inputMasked?>" name="surface" required parsley-trigger="change" placeholder="Superficie" class="form-control input-masked" value="<?=set_value('surface', $property["inm_superficie"])?>" data-inputmask="'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="required" data-field="name">Precio</label>
                        <input type="<?=$inputMasked?>" name="price" required parsley-trigger="change" placeholder="Precio" class="form-control input-masked" value="<?=set_value('price',$property["inm_precio"])?>" data-inputmask="'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary">
                        Guardar
                    </button>
                    <a href="<?=base_url("admin/Project")?>" class="btn btn-danger" role="button">Cancelar</a>
                </div>
            </div>
        </form>
    </div>
</div>