<?php
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 3/7/2017
 * Time: 13:53 PM
 */
?>
<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-pencil"></i>
            <span class="caption-subject font-green-sharp bold uppercase">Configuracion del menú</span>
            <span class="caption-helper"><a href="<?=base_url($project["proy_seo"])?>" target="_blank"><?=$project["proy_nombre"]?></a></span>
        </div>
        <?php $this->load->view("admin/project/form-buttons-action");?>
    </div>
    <div class="portlet-body">
        <div class="tmp_message">
            <?php
            $this->load->view("flash-data-basic-messages")
            ?>
        </div>
        <form name="settings-project-form" method="post" enctype="multipart/form-data" data-parsley-validate>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="required" data-field="name">Inicio</label>
                        <input type="text" name="home-label" required parsley-trigger="change" placeholder="" class="form-control" value="<?=set_value('home-label',$projectSettings["menu_inicio"])?>">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label>Mostrar descripción</label>
                        <div class="mt-radio-inline">
                            <label class="mt-radio">
                                <span></span>
                                <input type="radio" name="show-description" value="Si" <?=$projectSettings["check_descripcion"] == "Si"?"checked":""?> required data-parsley-errors-container="#description-error-message-parsley"> Si
                            </label>
                            <label class="mt-radio">
                                <input type="radio" name="show-description" value="No" <?=$projectSettings["check_descripcion"] == "No"?"checked":""?>> No
                                <span></span>
                            </label>
                        </div>
                        <div id="description-error-message-parsley"></div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Mostrar Detalle Tipologia en Precio</label>
                        <div class="mt-radio-inline">
                            <label class="mt-radio">
                                <input type="radio" name="show-typology-detail" value="Si" <?=$projectSettings["check_tipologia_det"] == "Si"?"checked":""?> required data-parsley-errors-container="#typology-detail-error-message-parsley"> Si
                                <span></span>
                            </label>
                            <label class="mt-radio">
                                <input type="radio" name="show-typology-detail" value="No" <?=$projectSettings["check_tipologia_det"] == "No"?"checked":""?>> No
                                <span></span>
                            </label>
                        </div>
                        <div id="typology-detail-error-message-parsley"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-12">
                            <label>Tipologia</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <input type="checkbox" value="Si" name="show-typology" <?=$projectSettings["check_tipologia"] == "Si"?"checked":""?>>
                                </span>
                                <input type="text" class="form-control" name="typology-label" required value="<?=set_value('typology-label',$projectSettings["menu_tipologia"])?>">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label>Ubicacion</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <input type="checkbox" value="Si" name="show-location" <?=$projectSettings["check_ubicacion"] == "Si"?"checked":""?>>
                                </span>
                                <input type="text" class="form-control" name="location-label" required value="<?=set_value('location-label',$projectSettings["menu_ubicacion"])?>">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label>Formulario</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <input type="checkbox" value="Si" name="show-form" <?=$projectSettings["check_formulario"] == "Si"?"checked":""?>>
                                </span>
                                <input type="text" class="form-control" name="form-label" required value="<?=set_value('form-label',$projectSettings["menu_formulario"])?>">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label>Contacto</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <input type="checkbox" value="Si" name="show-contact" <?=$projectSettings["check_contacto"] == "Si"?"checked":""?>>
                                </span>
                                <input type="text" class="form-control" name="contact-label" required value="<?=set_value('contact-label',$projectSettings["menu_contacto"])?>">
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-12">
                            <label>Precio</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <input type="checkbox" value="Si" name="show-price" <?=$projectSettings["check_precio"] == "Si"?"checked":""?>>
                                </span>
                                <input type="text" class="form-control" name="price-label" required value="<?=set_value('price-label',$projectSettings["menu_precio"])?>">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label>Acabado</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <input type="checkbox" value="Si" name="show-finishing-detail" <?=$projectSettings["check_acabado"] == "Si"?"checked":""?>>
                                </span>
                                <input type="text" class="form-control" name="finishing-detail-label" required value="<?=set_value('finishing-detail-label',$projectSettings["menu_acabado"])?>">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label>Social</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <input type="checkbox" value="Si" name="show-social-area" <?=$projectSettings["check_social"] == "Si"?"checked":""?>>
                                </span>
                                <input type="text" class="form-control" name="social-area-label" required value="<?=set_value('social-area-label',$projectSettings["menu_social"])?>">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label>Galeria</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <input type="checkbox" value="Si" name="show-gallery" <?=$projectSettings["check_galeria"] == "Si"?"checked":""?>>
                                </span>
                                <input type="text" class="form-control" name="gallery-label" required value="<?=set_value('gallery-label',$projectSettings["menu_galeria"])?>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary">
                        Guardar
                    </button>
                    <a href="<?=base_url("admin/Project")?>" class="btn btn-danger" role="button">Cancelar</a>
                </div>
            </div>
        </form>
    </div>
</div>