<?php
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 3/7/2017
 * Time: 13:53 PM
 */
?>
<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-pencil"></i>
            <span class="caption-subject font-green-sharp bold uppercase">Galeria</span>
            <span class="caption-helper"><a href="<?=base_url($project["proy_seo"])?>" target="_blank"><?=$project["proy_nombre"]?></a></span>
        </div>
        <?php $this->load->view("admin/project/form-buttons-action");?>
    </div>
    <div class="portlet-body">
        <div class="tmp_message">
            <?php
            $this->load->view("flash-data-basic-messages")
            ?>
        </div>
        <div class="row">
            <div class="col-md-4">
                <input type="hidden" name="project-id" value="<?=$project["proy_id"]?>">
                <form name="upload-project-images" id="my-dropzone" action="<?=base_url("admin/Project/images/".$project["proy_id"])?>" class="dropzone">
                    <div class="fallback">
                        <input name="file" type="file" multiple />
                    </div>
                </form>
            </div>
            <div class="col-md-8">
                <div class="row hide">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover" id="data-table">
                                <thead>
                                <tr>
                                    <th>Imagen</th>
                                    <th>Descripcion</th>
                                    <th>Opciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Imagen</td>
                                    <td>Descripcion</td>
                                    <td>
                                        <a class="btn btn-primary btn-xs" href="#" data-payment-id="1" title="" data-original-title="VER" data-toggle="tooltip" data-placement="top"><i class="fa fa-pencil"></i></a>
                                        <a class="btn btn-primary btn-xs" href="#" data-payment-id="1" title="" data-original-title="EDITAR" data-toggle="tooltip" data-placement="top"><i class="fa fa-search"></i></a>
                                        <a class="btn btn-danger btn-xs" href="#" data-payment-id="1" title="" data-original-title="ELIMINAR" data-toggle="tooltip" data-placement="top"><i class="fa fa-times"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Imagen</td>
                                    <td>Descripcion</td>
                                    <td>
                                        <a class="btn btn-primary btn-xs" href="#" data-payment-id="1" title="" data-original-title="VER" data-toggle="tooltip" data-placement="top"><i class="fa fa-pencil"></i></a>
                                        <a class="btn btn-primary btn-xs" href="#" data-payment-id="1" title="" data-original-title="EDITAR" data-toggle="tooltip" data-placement="top"><i class="fa fa-search"></i></a>
                                        <a class="btn btn-danger btn-xs" href="#" data-payment-id="1" title="" data-original-title="ELIMINAR" data-toggle="tooltip" data-placement="top"><i class="fa fa-times"></i></a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row sortable list-group">
                </div>
            </div>
        </div>
    </div>
</div>
<script id="ht-image-item" type="text/x-handlebars-template">
        <div class="col-md-2 image-item" id="{{item.proy_fot_id}}">
            <div class="thumbnail">
                    <img data-src="holder.js/300x300"
                         src="{{item.src}}"
                         class="profile-avatar" id="avatar-preview" style="max-height:100px">
                <div class="caption">
                    <div data-project-image-id="{{item.proy_fot_id}}">
                        <a class="btn btn-primary btn-xs project-image-see-{{item.proy_fot_id}}" href="#" title="" data-original-title="VER" data-toggle="tooltip" data-placement="top"><i class="fa fa-search"></i></a>
                        <a class="btn btn-primary btn-xs project-image-edit-{{item.proy_fot_id}}" href="#" title="" data-original-title="EDITAR" data-toggle="tooltip" data-placement="top"><i class="fa fa-pencil"></i></a>
                        <a class="btn btn-danger btn-xs project-image-delete-{{item.proy_fot_id}}" href="#" title="" data-original-title="ELIMINAR" data-toggle="tooltip" data-placement="top"><i class="fa fa-times"></i></a>
                    </div>
                </div>
            </div>
        </div>
</script>
<script id="ht-image-edit" type="text/x-handlebars-template">
    <form name="image-edit-{{detail.proy_fot_id}}" method="post" enctype="multipart/form-data">
        <input type="hidden" value="{{detail.proy_fot_id}}" name="project-image-id">
        <div class="row">
            <div class="col-md-6">
                <div>
                    <label data-field="avatar">Image</label>
                </div>
                <div class="avatar-upload-{{detail.proy_fot_id}}">
                    <div style="max-width: 140px; max-height: 140px; overflow:hidden">
                        <img src="{{detail.src}}" class="profile-avatar img-thumbnail" id="avatar-preview-{{detail.proy_fot_id}}" alt="Click to change." title="Click to change"/>
                    </div>
                    <input id="avatar-file-{{detail.proy_fot_id}}" type="file" name="logo" accept="image/*" class="hide">
                    <div id="progress" class="overlay"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Portada?</label>
                            <div class="mt-radio-inline">
                                <label class="mt-radio">
                                    <input type="radio" name="cover-image" value="1" {{#ifCond detail.proy_fot_portada '==' "true"}} checked {{/ifCond}}> Si
                                    <span></span>
                                </label>
                                <label class="mt-radio">
                                    <input type="radio" name="cover-image" value="0" {{#ifCond detail.proy_fot_portada '==' "false"}} checked {{/ifCond}}> No
                                    <span></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Anuncio</label>
                            <div class="mt-radio-inline">
                                <label class="mt-radio">
                                    <input type="radio" name="ad-image" value="1" {{#ifCond detail.proy_fot_anuncio '==' "true"}} checked {{/ifCond}}> Si
                                    <span></span>
                                </label>
                                <label class="mt-radio">
                                    <input type="radio" name="ad-image" value="0" {{#ifCond detail.proy_fot_anuncio '==' "false"}} checked {{/ifCond}}> No
                                    <span></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="required" data-field="description">Descripción</label>
                    <textarea class="tinymce" name="description" placeholder="Ingrese la descripcion de la imagen">{{detail.proy_fot_descripcion}}</textarea>
                </div>
            </div>
        </div>
    </form>
</script>