<?php
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 3/7/2017
 * Time: 13:53 PM
 */
?>
<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-pencil"></i>
            <span class="caption-subject font-green-sharp bold uppercase">Area Social</span>
            <span class="caption-helper"><a href="<?=base_url($project["proy_seo"])?>" target="_blank"><?=$project["proy_nombre"]?></a></span>
        </div>
        <?php $this->load->view("admin/project/form-buttons-action");?>
    </div>
    <div class="portlet-body">
        <div class="tmp_message">
            <?php
            $this->load->view("flash-data-basic-messages")
            ?>
        </div>
        <div class="row">
            <div class="col-md-4">
                <input type="hidden" name="project-id" value="<?=$project["proy_id"]?>">
                <form name="upload-project-images" id="my-dropzone" action="<?=base_url("admin/Project/socialArea/".$project["proy_id"])?>" class="dropzone">
                    <div class="fallback">
                        <input name="file" type="file" multiple />
                    </div>
                </form>
            </div>
            <div class="col-md-8">
                <div class="row sortable list-group">

                </div>
            </div>
        </div>
    </div>
</div>
<script id="ht-image-item" type="text/x-handlebars-template">
        <div class="col-md-2 image-item">
            <div class="thumbnail">
                    <img data-src="holder.js/300x300"
                         src="{{item.src}}"
                         class="profile-avatar" id="avatar-preview" style="max-height:100px">
                <div class="caption">
                    <div data-project-image-id="{{item.are_soc_id}}">
                        <a class="btn btn-primary btn-xs project-image-see-{{item.are_soc_id}}" href="#" title="" data-original-title="VER" data-toggle="tooltip" data-placement="top"><i class="fa fa-search"></i></a>
                        <a class="btn btn-primary btn-xs project-image-edit-{{item.are_soc_id}}" href="#" title="" data-original-title="EDITAR" data-toggle="tooltip" data-placement="top"><i class="fa fa-pencil"></i></a>
                        <a class="btn btn-danger btn-xs project-image-delete-{{item.are_soc_id}}" href="#" title="" data-original-title="ELIMINAR" data-toggle="tooltip" data-placement="top"><i class="fa fa-times"></i></a>
                    </div>
                </div>
            </div>
        </div>
</script>
<script id="ht-image-edit" type="text/x-handlebars-template">
    <form name="image-edit-{{detail.are_soc_id}}" method="post" enctype="multipart/form-data">
        <input type="hidden" value="{{detail.are_soc_id}}" name="item-id">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-4">
                        <div>
                            <label data-field="avatar">Image</label>
                        </div>
                        <div class="avatar-upload-{{detail.are_soc_id}}">
                            <div style="max-width: 140px; max-height: 140px; overflow:hidden">
                                <img src="{{detail.src}}" class="profile-avatar img-thumbnail" id="avatar-preview-{{detail.are_soc_id}}" alt="Click to change." title="Click to change"/>
                            </div>
                            <input id="avatar-file-{{detail.are_soc_id}}" type="file" name="logo" accept="image/*" class="hide">
                            <div id="progress" class="overlay"></div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label class="required" data-field="title">Titulo</label>
                            <input type="text" name="title" required parsley-trigger="change" placeholder="Titulo del area social" class="form-control input-sm" value="{{detail.are_soc_titulo}}">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="required" data-field="description">Descripción</label>
                    <textarea class="tinymce" name="description" placeholder="Ingrese la descripcion de la imagen">{{detail.are_soc_descripcion}}</textarea>
                </div>
            </div>
        </div>
    </form>
</script>