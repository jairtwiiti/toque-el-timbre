<?php
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 23/03/2018
 * Time: 11:41 AM
 */
?>
<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-pencil"></i>
            <span class="caption-subject font-green-sharp bold uppercase">leads</span>
            <span class="caption-helper"><a href="<?=base_url($project["proy_seo"])?>" target="_blank"><?=$project["proy_nombre"]?></a></span>
        </div>
        <?php
        if($user->usu_tipo == "Admin")
        {
            $this->load->view("admin/project/form-buttons-action");
        }
        ?>
    </div>
    <div class="portlet-body">
        <div class="tmp_message">
            <?php
            $this->load->view("flash-data-basic-messages")
            ?>
        </div>

        <?php
        $optionListHtml = '
                            <div class="col-md-12 col-lg-12">
                                <div class="form-group">
                                    <label>Formularios</label>
                                    <div class="mt-radio-inline">{options}</div>
                                </div>
                            </div>
                            ';
        $optionHtml = "";
        $checked = count($formList) == 1?"checked":"";
        foreach ($formList as $form)
        {
            $formName = $form->getName() == ""?$form->getFbFormId():$form->getName();
            $optionHtml .= '
                    <label class="mt-radio">
                        <input type="radio" class="filter-leads" data-project-id="'.$project["proy_id"].'" name="form-id" value="'.$form->getId().'" '.$checked.'> '.$formName.'
                        <span></span>
                    </label>';
        }
        if(count($formList) > 1)
        {
            $optionHtml .= '
                        <label class="mt-radio">
                            <input type="radio" class="filter-leads" data-project-id="'.$project["proy_id"].'" name="form-id" value="all" checked> Todos
                            <span></span>
                        </label>';
        }
        if(count($formList) <= 0)
        {
            $optionHtml = "Ningun formulario registrado para este proyecto";
        }
        $optionListHtml = str_replace("{options}",$optionHtml,$optionListHtml);
        if($isAdmin)
            echo $optionListHtml;
        ?>
        <input type="hidden" value="<?=$isAdmin?>" name="is-admin">
        <div class="col-md-12 col-lg-12">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover" id="project-facebook-leads" data-project-id="<?=$project["proy_id"]?>">
                    <thead>
                    <tr>
                        <th>Nombre completo</th>
                        <th>Email</th>
                        <th>Telefono</th>
                        <th>Consulta</th>
                        <th>Fecha registro</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($arrayProjectLeads as $lead)
                    {
                        $lead = $lead->toArray();
                        ?>
                        <tr>

                            <td><?=$lead["fle_full_name"] == ""?$lead["fle_first_name"]." ".$lead["fle_last_name"]:$lead["fle_full_name"]?></td>
                            <td><?=$lead["fle_email"]?></td>
                            <td><?=str_replace("+591","",$lead["fle_phone_number"])?></td>
                            <td><?=$lead["fle_consulta"]?></td>
                            <td><?=$lead["fle_createdon"]?></td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>