<?php
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 3/8/2017
 * Time: 1:55 PM
 */
$url = base_url("admin/Project");
$projectId = $project["proy_id"];
$formButtonsAction["edit"] = array("icon" => "fa fa-pencil", "title" => "EDITAR", "url" => $url."/edit/".$projectId);
$formButtonsAction["publication"] = array("icon" => "fa fa-home", "title" => "PUBLICACION", "url" => $url."/publication/".$projectId);
$formButtonsAction["images"] = array("icon" => "fa fa-file-image-o", "title" => "IMAGENES", "url" => $url."/images/".$projectId);
$formButtonsAction["typology"] = array("icon" => "fa fa-cubes", "title" => "TIPOLOGIA", "url" => $url."/typology/".$projectId);
$formButtonsAction["finishingDetail"] = array("icon" => "fa fa-paint-brush", "title" => "ACABADO", "url" => $url."/finishingDetail/".$projectId);
$formButtonsAction["socialArea"] = array("icon" => "fa fa-users", "title" => "AREA SOCIAL", "url" => $url."/socialArea/".$projectId);
$formButtonsAction["slideShow"] = array("icon" => "fa fa-caret-square-o-right", "title" => "SLIDE SHOW", "url" => $url."/slideShow/".$projectId);
$formButtonsAction["settings"] = array("icon" => "fa fa-wrench", "title" => "CONFIGURACION", "url" => $url."/settings/".$projectId);
$formButtonsAction["leads"] = array("icon" => "fa fa-list-alt", "title" => "LEADS", "url" => $url."/leads/".$projectId);
?>
<div class="actions">
    <?php
        $buttons = "";
        foreach($formButtonsAction as $key => $value)
        {
            $style = $key == $currentAction?"danger":"primary";
            $buttons .= '
                <a class="btn btn-circle btn-icon-only btn-'.$style.'" href="'.$value["url"].'" data-original-title="'.$value["title"].'" data-toggle="tooltip" data-placement="top">
                    <i class="'.$value["icon"].'"></i>
                </a>
            ';
        }
        echo $buttons;
    ?>
</div>
