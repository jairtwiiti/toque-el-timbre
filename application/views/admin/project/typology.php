<?php
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 8/8/2017
 * Time: 3:24 PM
 */
?>
<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-pencil"></i>
            <span class="caption-subject font-green-sharp bold uppercase">Tipologia</span>
            <span class="caption-helper"><a href="<?=base_url($project["proy_seo"])?>" target="_blank"><?=$project["proy_nombre"]?></a></span>
        </div>
        <?php $this->load->view("admin/project/form-buttons-action");?>
    </div>
    <div class="portlet-body">
        <div class="tmp_message">
            <?php
            $this->load->view("flash-data-basic-messages")
            ?>
        </div>
        <div class="row">
            <div class="col-md-4">
                <input type="hidden" name="project-id" value="<?=$project["proy_id"]?>">
                <form name="upload-project-tipologies" id="my-dropzone" action="<?=base_url("admin/Project/typology/".$project["proy_id"])?>" class="dropzone">
                    <div class="fallback">
                        <input name="file" type="file" multiple />
                    </div>
                </form>
            </div>
            <div class="col-md-8">
                <div class="row hide">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover" id="data-table">
                                <thead>
                                <tr>
                                    <th>Imagen</th>
                                    <th>Descripcion</th>
                                    <th>Opciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Imagen</td>
                                    <td>Descripcion</td>
                                    <td>
                                        <a class="btn btn-primary btn-xs" href="#" data-payment-id="1" title="" data-original-title="VER" data-toggle="tooltip" data-placement="top"><i class="fa fa-pencil"></i></a>
                                        <a class="btn btn-primary btn-xs" href="#" data-payment-id="1" title="" data-original-title="EDITAR" data-toggle="tooltip" data-placement="top"><i class="fa fa-search"></i></a>
                                        <a class="btn btn-danger btn-xs" href="#" data-payment-id="1" title="" data-original-title="ELIMINAR" data-toggle="tooltip" data-placement="top"><i class="fa fa-times"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Imagen</td>
                                    <td>Descripcion</td>
                                    <td>
                                        <a class="btn btn-primary btn-xs" href="#" data-payment-id="1" title="" data-original-title="VER" data-toggle="tooltip" data-placement="top"><i class="fa fa-pencil"></i></a>
                                        <a class="btn btn-primary btn-xs" href="#" data-payment-id="1" title="" data-original-title="EDITAR" data-toggle="tooltip" data-placement="top"><i class="fa fa-search"></i></a>
                                        <a class="btn btn-danger btn-xs" href="#" data-payment-id="1" title="" data-original-title="ELIMINAR" data-toggle="tooltip" data-placement="top"><i class="fa fa-times"></i></a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row sortable list-group">
                </div>
            </div>
        </div>
    </div>
</div>
<script id="ht-typology-item" type="text/x-handlebars-template">
        <div class="col-md-2 typology-item">
            <div class="thumbnail">
                    <img data-src="holder.js/300x300"
                         src="{{item.srcImage}}"
                         class="profile-avatar" id="avatar-preview" style="max-height:100px">
                <div class="caption">
                    <div data-project-typology-id="{{item.tip_id}}">
                        <a class="btn btn-primary btn-xs project-typology-see-{{item.tip_id}}" href="#" title="" data-original-title="VER" data-toggle="tooltip" data-placement="top"><i class="fa fa-search"></i></a>
                        <a class="btn btn-primary btn-xs project-typology-edit-{{item.tip_id}}" href="#" title="" data-original-title="EDITAR" data-toggle="tooltip" data-placement="top"><i class="fa fa-pencil"></i></a>
                        <a class="btn btn-danger btn-xs project-typology-delete-{{item.tip_id}}" href="#" title="" data-original-title="ELIMINAR" data-toggle="tooltip" data-placement="top"><i class="fa fa-times"></i></a>
                    </div>
                </div>
            </div>
        </div>
</script>

<script id="ht-typology-edit" type="text/x-handlebars-template">
    <form name="typology-edit-{{detail.tip_id}}" method="post" enctype="multipart/form-data" data-parsley-validate>
        <input type="hidden" value="{{detail.tip_id}}" name="typology-id">
        <div class="row">
            <div class="col-md-4">
                <div>
                    <label data-field="avatar">Imagen</label>
                </div>
                <div class="avatar-upload-{{detail.tip_id}}">
                    <div style="max-width: 140px; max-height: 140px; overflow:hidden">
                        <img src="{{detail.srcImage}}" class="profile-avatar img-thumbnail" id="avatar-preview-{{detail.tip_id}}" alt="Click to change." title="Click to change"/>
                    </div>
                    <input id="avatar-file-{{detail.tip_id}}" type="file" name="logo" accept="image/*" class="hide">
                    <div id="progress" class="overlay"></div>
                </div>
            </div>
            <div class="col-md-4">
                <div>
                    <label data-field="avatar-2">Imagen precio</label>
                </div>
                <div class="avatar-upload-{{detail.tip_id}}-2">
                    <div style="max-width: 140px; max-height: 140px; overflow:hidden">
                        <img src="{{detail.srcImagePrice}}" class="profile-avatar img-thumbnail" id="avatar-preview-{{detail.tip_id}}-2" alt="Click to change." title="Click to change"/>
                    </div>
                    <input id="avatar-file-{{detail.tip_id}}-2" type="file" name="logo2" accept="image/*" class="hide">
                    <div id="progress" class="overlay"></div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="required" data-field="title">Titulo</label>
                            <input type="text" name="title" required parsley-trigger="change" placeholder="Titulo de la tipologia" class="form-control input-sm" value="{{detail.tip_titulo}}">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="required" data-field="title-price">Titulo precio</label>
                            <input type="text" name="title-price" required parsley-trigger="change" placeholder=":|" class="form-control input-sm" value="{{detail.tip_titulo_precio}}">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label class="required" data-field="price">Precio</label>
                    <input type="text" name="price" required parsley-trigger="change" placeholder="Precio" class="form-control input-sm input-masked" value="{{detail.tip_precio}}" data-inputmask="'alias': 'integer', 'groupSeparator': ',', 'autoGroup': true">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label class="required" data-field="built-surface">Superf. constr.</label>
                    <input type="text" name="built-surface" required parsley-trigger="change" placeholder="Superficie construida" class="form-control input-sm input-masked" value="{{detail.tip_mts}}" data-inputmask="'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label class="required" data-field="total-surface">Superf. total</label>
                    <input type="text" name="total-surface" required parsley-trigger="change" placeholder="Superficie total" class="form-control input-sm input-masked" value="{{detail.tip_terreno_mts}}" data-inputmask="'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label class="required" data-field="availability">Disponibilidad</label>
                    <input type="text" name="availability" required parsley-trigger="change" placeholder="Disponibilidad" class="form-control input-sm" value="{{detail.tip_disponibilidad}}">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Mostrar precio?</label>
                    <div class="mt-radio-inline">
                        <label class="mt-radio">
                            <input type="radio" name="show-price" value="1" {{#ifCond detail.tip_ver_precio '==' "1"}} checked {{/ifCond}}> Si
                            <span></span>
                        </label>
                        <label class="mt-radio">
                            <input type="radio" name="show-price" value="0" {{#ifCond detail.tip_ver_precio '==' "0"}} checked {{/ifCond}}> No
                            <span></span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Mostrar superf. constr.?</label>
                    <div class="mt-radio-inline">
                        <label class="mt-radio">
                            <input type="radio" name="show-built-surface" value="1" {{#ifCond detail.tip_ver_supcon '==' "1"}} checked {{/ifCond}}> Si
                            <span></span>
                        </label>
                        <label class="mt-radio">
                            <input type="radio" name="show-built-surface" value="0" {{#ifCond detail.tip_ver_supcon '==' "0"}} checked {{/ifCond}}> No
                            <span></span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Mostrar superf. total?</label>
                    <div class="mt-radio-inline">
                        <label class="mt-radio">
                            <input type="radio" name="show-total-surface" value="1" {{#ifCond detail.tip_ver_suptot '==' "1"}} checked {{/ifCond}}> Si
                            <span></span>
                        </label>
                        <label class="mt-radio">
                            <input type="radio" name="show-total-surface" value="0" {{#ifCond detail.tip_ver_suptot '==' "0"}} checked {{/ifCond}}> No
                            <span></span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="required" data-field="description">Descripción</label>
                    <textarea class="tinymce" name="description" placeholder="Ingrese la descripcion de la imagen">{{detail.tip_descripcion}}</textarea>
                </div>
            </div>
        </div>
    </form>
</script>