<?php
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 28/7/2017
 * Time: 10:01 AM
 */
?>
<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-plus"></i>
            <span class="caption-subject font-green-sharp bold uppercase">Nuevo proyecto</span>
        </div>
    </div>
    <div class="portlet-body">
        <div class="tmp_message">
            <?php
            $this->load->view("flash-data-basic-messages")
            ?>
        </div>
        <form name="add-project-form" method="post" enctype="multipart/form-data" data-parsley-validate>
            <div class="hide">
                <input name="latitude" type="text" required>
                <input name="longitude" type="text" required data-parsley-errors-container="#map-location-error-message-parsley" data-parsley-error-message="Debe marcar un punto en el mapa">
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <?php
                                        $select2Data = "";
                                        if(isset($userProject))
                                        {
                                            $select2Data["id"] = $userProject["usu_id"];
                                            $select2Data["text"] = $userProject["usu_nombre"]." ".$userProject["usu_apellido"]." ".$userProject["usu_email"];
                                            $select2Data = json_encode($select2Data);
                                        }
                                        ?>
                                        <label>Usuario</label>
                                        <input type="text" data-parsley-required id="ajax-get-users" name="user" parsley-trigger="change" value='<?=$select2Data?>'>
                                    </div>
                                </div>
                                <div class="col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <label class="required" data-field="name">Nombre</label>
                                        <input type="text" name="name" required parsley-trigger="change" placeholder="Nombre del Proyecto" class="form-control" value="<?=set_value('name')?>">
                                    </div>
                                </div>
                                <div class="col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <label class="required" data-field="video">Video</label>
                                        <input type="text" name="video" parsley-trigger="change" placeholder="Video del Proyecto" class="form-control" value="<?=set_value('video')?>">
                                    </div>
                                </div>
                                <div class="col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <?php
                                        $postStatus = $this->input->post("status");
                                        if($postStatus === "0" || $postStatus != FALSE && is_numeric((int)$postStatus))
                                        {
                                            $status = $arrayStatus[$this->input->post("status")];
                                        }
                                        ?>
                                        <label>Estado</label><br>
                                        <div class="mt-radio-inline">
                                            <label class="mt-radio">
                                                <input type="radio" name="status" required value="1" <?=$status == model_project::APPROVED?"checked":""?> data-parsley-errors-container="#status-error-message-parsley"> Aprobado
                                                <span></span>
                                            </label>
                                            <label class="mt-radio">
                                                <input type="radio" name="status" value="0" <?=$status == model_project::NOT_APPROVED?"checked":""?>> No aprobado
                                                <span></span>
                                            </label>
                                            <label class="mt-radio">
                                                <input type="radio" name="status" value="2" <?=$status == model_project::CANCELED?"checked":""?>> Cancelado
                                                <span></span>
                                            </label>
                                        </div>
                                        <div id="status-error-message-parsley"></div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-lg-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div>
                                                <label data-field="avatar">Logo</label>
                                            </div>
                                            <div class="avatar-upload">
                                                <div style="max-width: 130px; max-height: 130px; overflow:hidden">
                                                    <img src="<?php echo assets_url("backend/admin/img/img_not_available.png")?>" class="profile-avatar img-thumbnail" id="avatar-preview" alt="Click to change." title="Click to change"/>
                                                </div>
                                                <input id="avatar-file" type="file" name="logo" accept="image/*">
                                                <div id="progress" class="overlay"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label data-field="start-date">Fecha de inicio (dia-mes-año)</label>
                                                        <input type="text" name="start-date" data-date-format="dd-mm-yyyy" parsley-trigger="change" placeholder="dia-mes-año" readonly class="form-control date-picker" value="<?=set_value('start-date')?>">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label data-field="finish-date">Fecha de fin (dia-mes-año)</label>
                                                        <input type="text" name="finish-date" data-date-format="dd-mm-yyyy" parsley-trigger="change" placeholder="dia-mes-año" readonly class="form-control date-picker" value="<?=set_value('finish-date')?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-lg-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div>
                                                <label data-field="avatar">Imagen de ubicaion</label>
                                            </div>
                                            <div class="avatar-upload-2">
                                                <div style="max-width: 130px; max-height: 130px; overflow:hidden">
                                                    <img src="<?php echo assets_url("backend/admin/img/img_not_available.png")?>" class="profile-avatar img-thumbnail" id="avatar-preview-2" alt="Click to change." title="Click to change"/>
                                                </div>
                                                <input id="avatar-file-2" type="file" name="logo-2" accept="image/*">
                                                <div id="progress" class="overlay"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-4 col-lg-12">
                                                    <div class="form-group">
                                                        <label class="required" data-field="state">Departamento</label>
                                                        <select class="select2" name="state">
                                                            <?php
                                                            $options = "";
                                                            foreach($arrayState as $stateId => $stateName)
                                                            {
                                                                $options .= '<option value="'.$stateId.'">'.$stateName.'</option>';
                                                            }
                                                            echo $options;
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-lg-12">
                                                    <div class="form-group">
                                                        <?php
                                                        $select2Data = "";
                                                        if(isset($city))
                                                        {
                                                            $select2Data["id"] = $city["ciu_id"];
                                                            $select2Data["text"] = $city["ciu_nombre"];
                                                            $select2Data = json_encode($select2Data);
                                                        }
                                                        ?>
                                                        <label>Ciudad</label>
                                                        <input type="text" data-parsley-required id="ajax-get-cities" name="city" parsley-trigger="change" value='<?=$select2Data?>'>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-lg-12">
                                                    <div class="form-group">
                                                        <?php
                                                        $select2Data = "";
                                                        if(isset($zone))
                                                        {
                                                            $select2Data["id"] = $zone["zon_id"];
                                                            $select2Data["text"] = $zone["zon_nombre"];
                                                            $select2Data = json_encode($select2Data);
                                                        }
                                                        ?>
                                                        <label>Zona</label>
                                                        <input type="text" data-parsley-required id="ajax-get-zones" name="zone" parsley-trigger="change" value='<?=$select2Data?>'>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-lg-12">
                                    <label class="required" data-field="name">
                                        <div class="input-group">
                                            <input type="text" class="form-control search-address-data" placeholder="Ingrese ubicacion">
                                            <span class="input-group-addon search-address-data-city <?=isset($city)?"":"hide"?>"><?=isset($city)?$city["ciu_nombre"]:""?></span>
                                            <span class="input-group-addon search-address-data-state"><?=isset($city)?$arrayState[$city["ciu_dep_id"]]:"Santa Cruz"?></span>
                                        <span class="input-group-btn">
                                            <button class="btn btn-primary search-address-button" type="button">Buscar</button>
                                        </span>
                                        </div>
                                    </label><!-- /input-group -->
                                    <div class="map-fancy-framework">
                                        <div id="maps" style="height: 300px;width: auto">
                                        </div>
                                        <em class="map-search-message"></em>
                                        <div id="map-location-error-message-parsley"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="required" data-field="description">Descripción</label>
                                        <textarea class="tinymce" name="description" placeholder="Ingrese la descripcion de su proyecto"><?=set_value('description')?></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="required" data-field="contact">Contacto</label>
                                        <textarea class="tinymce" name="contact" placeholder="Informacion de contacto"><?=set_value('contact')?></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="required" data-field="address">Direccion</label>
                                        <textarea class="tinymce" name="address"><?=set_value('address')?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary">
                        Guardar
                    </button>
                    <a href="<?=base_url("admin/Project")?>" class="btn btn-danger" role="button">Cancelar</a>
                </div>
            </div>
        </form>
    </div>
</div>