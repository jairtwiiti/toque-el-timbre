<?php
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 20/7/2017
 * Time: 3:02 PM
 */
?>
<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-table"></i>
            <span class="caption-subject font-green-sharp bold uppercase">Notify search</span>
        </div>
    </div>
    <div class="portlet-body">
        <div class="row">
            <div class="col-md-12">
                <form class="form-group" id="extra-request-data">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-lg-3">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="basic-addon1">Desde</span>
                                        <input type="text" name="start-date" data-date-format="dd-mm-yyyy" parsley-trigger="change" placeholder="dia-mes-año" readonly class="form-control date-picker">
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="basic-addon1">Hasta</span>
                                        <input type="text" name="end-date" data-date-format="dd-mm-yyyy" parsley-trigger="change" placeholder="dia-mes-año" readonly class="form-control date-picker" value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary input-sm" id="send-filters" type="button" data-content-data="chart-property-offers-based-on-property-types">Filtrar</button>
                        <button class="btn btn-danger input-sm" id="remove-additional-parameters" type="button" data-content-data="chart-property-offers-based-on-property-types">Remove parametros adicionales</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover" id="search-notify-index-table">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Description</th>
                            <th>Property type</th>
                            <th>Transaction type</th>
                            <th>State</th>
                            <th>Price</th>
                            <th>Sent on</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>