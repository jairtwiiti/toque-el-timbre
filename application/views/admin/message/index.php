<?php
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 02/02/2018
 * Time: 10:17
 */
?>
<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-table"></i>
            <span class="caption-subject font-green-sharp bold uppercase">Messages</span>
        </div>
    </div>
    <div class="portlet-body">
            <div class="col-md-12">
            <form class="form-group" id="extra-request-data">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="radio-inline">
                                        <input type="radio" name="addresses" value="0"> Anuncios
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="addresses" value="1"> Proyectos
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="addresses" value="2"> Agentes inmobiliarios
                                    </label><label class="radio-inline">
                                        <input type="radio" name="addresses" value="3" checked> Todos
                                    </label>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1">Desde</span>
                                    <input type="text" name="start-date" data-date-format="dd-mm-yyyy" parsley-trigger="change" placeholder="dia-mes-año" readonly class="form-control date-picker">
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1">Hasta</span>
                                    <input type="text" name="end-date" data-date-format="dd-mm-yyyy" parsley-trigger="change" placeholder="dia-mes-año" readonly class="form-control date-picker" value="">
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn btn-primary input-sm" id="send-filters" type="button">Filtrar</button>
                    <button class="btn btn-danger input-sm" id="remove-additional-parameters" type="button">Remove parametros adicionales</button>
                </div>
            </form>
        </div>
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover" id="data-table-messages">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Business</th>
                        <th>Phone</th>
                        <th>Email</th>
                        <th>Description</th>
                        <th>Sent on</th>
                        <th>Addressee</th>
                        <th>User type</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
