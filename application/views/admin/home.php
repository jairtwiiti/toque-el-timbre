<?php
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 19/7/2017
 * Time: 2:40 PM
 */
?>
<div class="row">
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject bold uppercase font-dark">Master control</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="col-md-4">
                    <div class="form-group">
                        <?php
                        $startDate = "01-".date('m')."-".date('Y');
                        ?>
                        <label data-field="master-start-date">Desde (dia-mes-año)</label>
                        <input type="text" name="master-start-date" data-date-format="dd-mm-yyyy" parsley-trigger="change" placeholder="dia-mes-año" readonly class="form-control date-picker" value="<?=set_value('master-start-date',$startDate)?>">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <?php
                        $endDate = date("t-m-Y", strtotime($startDate));
                        ?>
                        <label data-field="master-end-date">Hasta (dia-mes-año)</label>
                        <input type="text" name="master-end-date" data-date-format="dd-mm-yyyy" parsley-trigger="change" placeholder="dia-mes-año" readonly class="form-control date-picker" value="<?=set_value('master-end-date',$endDate)?>">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <button class="btn btn-primary master-search-data" type="button">Buscar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject bold uppercase font-dark">Usuarios registrados por dia</span>
                </div>
                <div class="actions">
                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="#" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body" style="height: auto;">
                <input type="hidden" name="report-name" value="">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div data-date-range-chart="chart-users-registered">
                                <?php $this->load->view("chart-date-range-snippet")?>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <button class="btn btn-primary chart-search-data" type="button" data-content-data="chart-users-registered">Buscar</button>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-4">
                                <h3 class="total-register-box">
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div id="chart-users-registered" class="chartdiv" data-report-name="Reporte de usuarios" data-source-data="admin/AjaxUser/getAllUsersRegisteredByDateRange" data-chart-type="stackedColumns">
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject bold uppercase font-dark">Publicaciones por dia</span>
                </div>
                <div class="actions">
                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="#" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body" style="height: auto;">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div data-date-range-chart="chart-publications-activity">
                                <?php $this->load->view("chart-date-range-snippet")?>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <button class="btn btn-primary chart-search-data" type="button" data-content-data="chart-publications-activity">Buscar</button>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="required" data-field="only-visible-publications">Criterio de busqueda</label>
                                    <select class="select2" name="only-visible-publications" data-chart="chart-publications-activity">
                                        <option value="1">Visibles y vigentes en el rango de fecha</option>
                                        <option value="0">Vigentes en el rango de fecha</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <h3 class="total-register-box">
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div id="chart-publications-activity" class="chartdiv" data-report-name="Reporte de publicaciones" data-source-data="admin/AjaxProperty/getAllPublicationsActivityByDateRange" data-chart-type="createMultipleValueAxes">
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject bold uppercase font-dark">Pagos</span>
                </div>
                <div class="actions">
                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="#" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body" style="height: auto;">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div data-date-range-chart="chart-payments-settlement-registered">
                                <?php $this->load->view("chart-date-range-snippet")?>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <button class="btn btn-primary chart-search-data" type="button" data-content-data="chart-payments-settlement-registered">Buscar</button>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-4">
                                <h3 class="total-register-box">
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div id="chart-payments-settlement-registered" class="chartdiv" data-report-name="Reporte de pagos" data-source-data="admin/AjaxPayment/getAllPaymentsSettlementByDateRange" data-chart-type="createSingleChart">
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="col-md-6">
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject bold uppercase font-dark">Mensajes</span>
                </div>
                <div class="actions">
                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="#" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body" style="height: auto;">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div data-date-range-chart="chart-messages-to-publications">
                                <?php $this->load->view("chart-date-range-snippet")?>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <button class="btn btn-primary chart-search-data" type="button" data-content-data="chart-messages-to-publications">Buscar</button>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-4">
                                <h3 class="total-register-box">
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div id="chart-messages-to-publications" class="chartdiv" data-report-name="Reporte de mensajes" data-source-data="admin/AjaxProperty/getAllMessagesToPublicationsByDateRange" data-chart-type="stackedColumns">
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject bold uppercase font-dark">Oferta</span>
                    <form class="form-inline">
                        <div data-date-range-chart="chart-property-offers-based-on-property-types">
                            <div class="form-group">
                                <?php
                                $startDate = "01-".date('m')."-".date('Y');
                                ?>
                                <label data-field="start-date">Desde (dia-mes-año)</label>
                                <input type="text" name="start-date" data-date-format="dd-mm-yyyy" parsley-trigger="change" placeholder="dia-mes-año" readonly class="form-control date-picker" value="<?=set_value('start-date',$startDate)?>">
                            </div>
                            <div class="form-group">
                                <?php
                                $endDate = date("t-m-Y", strtotime($startDate));
                                ?>
                                <label data-field="end-date">Hasta (dia-mes-año)</label>
                                <input type="text" name="end-date" data-date-format="dd-mm-yyyy" parsley-trigger="change" placeholder="dia-mes-año" readonly class="form-control date-picker" value="<?=set_value('end-date',$endDate)?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary chart-search-offer" type="button" data-content-data="chart-property-offers-based-on-property-types">Buscar</button>
                        </div>
                    </form>
                </div>
                <div class="actions">
                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="#" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body" style="height: auto;">
                <div class="row">
                    <div class="col-md-4">
                        <div class="portlet light">
                            <div class="portlet-body" style="height: auto;">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-6 col-lg-6">
                                                <div class="form-group">
                                                    <label class="required" data-field="transaction-type">Tipo de usuario</label>
                                                    <select class="select2" name="user-type" data-chart="chart-property-offers-based-on-property-types">
                                                        <option value="">Todos</option>
                                                        <option value="Particular">Particular</option>
                                                        <option value="Inmobiliaria">Inmobiliaria</option>
                                                        <option value="Constructora">Constructora</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6">
                                                <div class="form-group">
                                                    <label class="required" data-field="transaction-type">Tipo de transacción</label>
                                                    <select class="select2" name="transaction-type" data-chart="chart-property-offers-based-on-property-types">
                                                        <option value="">Todos</option>
                                                        <?php
                                                        $options = "";
                                                        foreach($arrayTransactionTypes as $key => $transactionType)
                                                        {
                                                            $selected = $property["inm_for_id"] == $key?"selected":"";
                                                            $options .= '<option value="'.$key.'" '.$selected.' >'.$transactionType.'</option>';
                                                        }
                                                        echo $options;
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-lg-4">
                                                <h3 class="total-register-box">
                                                    <i class="fa fa-spinner fa-pulse fa-fw pull-left"></i>
                                                </h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div id="chart-property-offers-based-on-property-types" class="chartdiv" data-report-name="Oferta segun el tipo de inmueble" data-source-data="admin/AjaxProperty/getPropertyOffersBasedOnPropertyTypes" data-chart-type="createPieChart">
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="portlet light">
                            <div class="portlet-body" style="height: auto;">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-6 col-lg-6">
                                                <div class="form-group">
                                                    <label class="required" data-field="transaction-type">Tipo de usuario</label>
                                                    <select class="select2" name="user-type" data-chart="chart-property-offers-based-on-transactions-type">
                                                        <option value="">Todos</option>
                                                        <option value="Particular">Particular</option>
                                                        <option value="Inmobiliaria">Inmobiliaria</option>
                                                        <option value="Constructora">Constructora</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6">
                                                <div class="form-group">
                                                    <label class="required" data-field="property-type">Tipo de inmueble</label>
                                                    <select class="select2" name="property-type" data-chart="chart-property-offers-based-on-transactions-type">
                                                        <option value="">Todos</option>
                                                        <?php
                                                        $options = "";
                                                        foreach($arrayPropertyTypes as $propertyType)
                                                        {
                                                            $selected = $property["inm_cat_id"] == $propertyType->getId()?"selected":"";
                                                            $options .= '<option value="'.$propertyType->getId().'" '.$selected.' >'.$propertyType->getName().'</option>';
                                                        }
                                                        echo $options;
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-lg-4">
                                                <h3 class="total-register-box">
                                                    <i class="fa fa-spinner fa-pulse fa-fw pull-left"></i>
                                                </h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div id="chart-property-offers-based-on-transactions-type" class="chartdiv" data-report-name="Oferta segun el tipo de transaccion" data-source-data="admin/AjaxProperty/getPropertyOffersBasedOnTransactionsType" data-chart-type="createPieChart">
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="portlet light">
                            <div class="portlet-body" style="height: auto;">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-6 col-lg-6">
                                                <div class="form-group">
                                                    <label class="required" data-field="property-type">Tipo de inmueble</label>
                                                    <select class="select2" name="property-type" data-chart="chart-property-offers-based-on-users">
                                                        <option value="">Todos</option>
                                                        <?php
                                                        $options = "";
                                                        foreach($arrayPropertyTypes as $propertyType)
                                                        {
                                                            $selected = $property["inm_cat_id"] == $propertyType->getId()?"selected":"";
                                                            $options .= '<option value="'.$propertyType->getId().'" '.$selected.' >'.$propertyType->getName().'</option>';
                                                        }
                                                        echo $options;
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6">
                                                <div class="form-group">
                                                    <label class="required" data-field="transaction-type">Tipo de transacción</label>
                                                    <select class="select2" name="transaction-type" data-chart="chart-property-offers-based-on-users">
                                                        <option value="">Todos</option>
                                                        <?php
                                                        $options = "";
                                                        foreach($arrayTransactionTypes as $key => $transactionType)
                                                        {
                                                            $selected = $property["inm_for_id"] == $key?"selected":"";
                                                            $options .= '<option value="'.$key.'" '.$selected.' >'.$transactionType.'</option>';
                                                        }
                                                        echo $options;
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-lg-4">
                                                <h3 class="total-register-box">
                                                    <i class="fa fa-spinner fa-pulse fa-fw pull-left"></i>
                                                </h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div id="chart-property-offers-based-on-users" class="chartdiv" data-report-name="Oferta segun el tipo de usuario" data-source-data="admin/AjaxProperty/getPropertyOffersBasedOnUsers" data-chart-type="createPieChart">
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject bold uppercase font-dark">Demanda</span>
                    <form class="form-inline">
                        <div data-date-range-chart="chart-property-offers-based-on-property-types-demand">
                            <div class="form-group">
                                <?php
                                $startDate = "01-".date('m')."-".date('Y');
                                ?>
                                <label data-field="start-date">Desde (dia-mes-año)</label>
                                <input type="text" name="start-date" data-date-format="dd-mm-yyyy" parsley-trigger="change" placeholder="dia-mes-año" readonly class="form-control date-picker" value="<?=set_value('start-date',$startDate)?>">
                            </div>
                            <div class="form-group">
                                <?php
                                $endDate = date("t-m-Y", strtotime($startDate));
                                ?>
                                <label data-field="end-date">Hasta (dia-mes-año)</label>
                                <input type="text" name="end-date" data-date-format="dd-mm-yyyy" parsley-trigger="change" placeholder="dia-mes-año" readonly class="form-control date-picker" value="<?=set_value('end-date',$endDate)?>">
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary chart-search-demand" type="button" data-content-data="chart-property-offers-based-on-property-types-demand">Buscar</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="actions">
                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="#" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body" style="height: auto;">
                <div class="row">
                    <div class="col-md-4">
                        <div class="portlet light">
                            <div class="portlet-body" style="height: auto;">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-6 col-lg-6">
                                                <div class="form-group">
                                                    <label class="required" data-field="transaction-type">Tipo de usuario</label>
                                                    <select class="select2" name="user-type" data-chart="chart-property-offers-based-on-property-types-demand">
                                                        <option value="">Todos</option>
                                                        <option value="Particular">Particular</option>
                                                        <option value="Inmobiliaria">Inmobiliaria</option>
                                                        <option value="Constructora">Constructora</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6">
                                                <div class="form-group">
                                                    <label class="required" data-field="transaction-type">Tipo de transacción</label>
                                                    <select class="select2" name="transaction-type" data-chart="chart-property-offers-based-on-property-types-demand">
                                                        <option value="">Todos</option>
                                                        <?php
                                                        $options = "";
                                                        foreach($arrayTransactionTypes as $key => $transactionType)
                                                        {
                                                            $selected = $property["inm_for_id"] == $key?"selected":"";
                                                            $options .= '<option value="'.$key.'" '.$selected.' >'.$transactionType.'</option>';
                                                        }
                                                        echo $options;
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-lg-4">
                                                <h3 class="total-register-box">
                                                    <i class="fa fa-spinner fa-pulse fa-fw pull-left"></i>
                                                </h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div id="chart-property-offers-based-on-property-types-demand" class="chartdiv" data-report-name="Demanda segun el tipo de inmueble" data-source-data="admin/AjaxProperty/getPropertyOffersBasedOnPropertyTypes" data-chart-type="createPieChart">
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="portlet light">
                            <div class="portlet-body" style="height: auto;">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-6 col-lg-6">
                                                <div class="form-group">
                                                    <label class="required" data-field="transaction-type">Tipo de usuario</label>
                                                    <select class="select2" name="user-type" data-chart="chart-property-offers-based-on-transactions-type-demand">
                                                        <option value="">Todos</option>
                                                        <option value="Particular">Particular</option>
                                                        <option value="Inmobiliaria">Inmobiliaria</option>
                                                        <option value="Constructora">Constructora</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6">
                                                <div class="form-group">
                                                    <label class="required" data-field="property-type">Tipo de inmueble</label>
                                                    <select class="select2" name="property-type" data-chart="chart-property-offers-based-on-transactions-type-demand">
                                                        <option value="">Todos</option>
                                                        <?php
                                                        $options = "";
                                                        foreach($arrayPropertyTypes as $propertyType)
                                                        {
                                                            $selected = $property["inm_cat_id"] == $propertyType->getId()?"selected":"";
                                                            $options .= '<option value="'.$propertyType->getId().'" '.$selected.' >'.$propertyType->getName().'</option>';
                                                        }
                                                        echo $options;
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-lg-4">
                                                <h3 class="total-register-box">
                                                    <i class="fa fa-spinner fa-pulse fa-fw pull-left"></i>
                                                </h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div id="chart-property-offers-based-on-transactions-type-demand" class="chartdiv" data-report-name="Demanda segun el tipo de transaccion" data-source-data="admin/AjaxProperty/getPropertyOffersBasedOnTransactionsType" data-chart-type="createPieChart">
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="portlet light">
                            <div class="portlet-body" style="height: auto;">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-6 col-lg-6">
                                                <div class="form-group">
                                                    <label class="required" data-field="property-type">Tipo de inmueble</label>
                                                    <select class="select2" name="property-type" data-chart="chart-property-offers-based-on-users-demand">
                                                        <option value="">Todos</option>
                                                        <?php
                                                        $options = "";
                                                        foreach($arrayPropertyTypes as $propertyType)
                                                        {
                                                            $selected = $property["inm_cat_id"] == $propertyType->getId()?"selected":"";
                                                            $options .= '<option value="'.$propertyType->getId().'" '.$selected.' >'.$propertyType->getName().'</option>';
                                                        }
                                                        echo $options;
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6">
                                                <div class="form-group">
                                                    <label class="required" data-field="transaction-type">Tipo de transacción</label>
                                                    <select class="select2" name="transaction-type" data-chart="chart-property-offers-based-on-users-demand">
                                                        <option value="">Todos</option>
                                                        <?php
                                                        $options = "";
                                                        foreach($arrayTransactionTypes as $key => $transactionType)
                                                        {
                                                            $selected = $property["inm_for_id"] == $key?"selected":"";
                                                            $options .= '<option value="'.$key.'" '.$selected.' >'.$transactionType.'</option>';
                                                        }
                                                        echo $options;
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-lg-4">
                                                <h3 class="total-register-box">
                                                    <i class="fa fa-spinner fa-pulse fa-fw pull-left"></i>
                                                </h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div id="chart-property-offers-based-on-users-demand" class="chartdiv" data-report-name="Demanda segun el tipo de usuario" data-source-data="admin/AjaxProperty/getPropertyOffersBasedOnUsers" data-chart-type="createPieChart">
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>