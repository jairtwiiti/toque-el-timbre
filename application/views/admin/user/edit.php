<?php
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 25/10/2017
 * Time: 10:17 PM
 */
?>
<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-pencil"></i>
            <span class="caption-subject font-green-sharp bold uppercase">Editar Usuario</span>
        </div>
    </div>
    <div class="portlet-body">
        <div class="tmp_message">
            <?php
            $this->load->view("flash-data-basic-messages")
            ?>
        </div>
        <form name="form-edit-user" method="post" enctype="multipart/form-data" data-parsley-validate>

            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PROFILE SIDEBAR -->
                    <div class="profile-sidebar">
                        <!-- PORTLET MAIN -->
                        <div class="portlet light profile-sidebar-portlet">
                            <!-- SIDEBAR USERPIC -->
                            <div class="profile-userpic">
                                <?php
                                $file = new File_Handler($userToEdit["usu_foto"],"userImage");
                                $thumbnail = $file->getThumbnail("200","200");
                                ?>
                                <img alt="" class="img-circle img-responsive" src="<?=$thumbnail->getSource()?>"/>
                            </div>
                            <!-- END SIDEBAR USERPIC -->
                            <!-- SIDEBAR USER TITLE -->
                            <div class="profile-usertitle">
                                <div class="profile-usertitle-name">
                                    <?php
                                    if($userToEdit["usu_tipo"] != "Particular"){
                                        echo $userToEdit["usu_empresa"];
                                    }else{
                                        echo $userToEdit["usu_nombre"] . " " . $userToEdit["usu_apellido"];
                                    }
                                    ?>
                                </div>
                                <div class="profile-usertitle-job">
                                    <?php echo $userToEdit["usu_tipo"]; ?>
                                </div>
                            </div>
                            <!-- END SIDEBAR USER TITLE -->
                            <!-- SIDEBAR MENU -->
                            <!-- END MENU -->
                        </div>
                        <!-- END PORTLET MAIN -->
                    </div>
                    <!-- END BEGIN PROFILE SIDEBAR -->
                    <!-- BEGIN PROFILE CONTENT -->
                    <div class="profile-content">
                        <div class="row">
                            <div class="col-md-12 col-mobile-1">
                                <div class="portlet light">
                                    <div class="portlet-title tabbable-line col-md-12 col-mobile-1">
                                        <ul class="nav nav-tabs pull-left">
                                            <li class="active">
                                                <a href="#tab_edit_user" data-toggle="tab">Editar Usuario</a>
                                            </li>
                                            <?php
                                            if($userToEdit["usu_tipo"] != 'Particular')
                                            {
                                            ?>
                                                <li class="<?=$position == 'company' ? 'active' : ''; ?>">
                                                    <a href="#tab_edit_company" id="tab_company" data-toggle="tab">Datos de la Empresa</a>
                                                </li>
                                            <?php
                                            }
                                            ?>
                                            <li class="">
                                                <a href="#tab_change_image" data-toggle="tab">Cambiar Imagen</a>
                                            </li>
                                            <li class="">
                                                <a href="#tab_change_password" data-toggle="tab">Cambiar Contraseña</a>
                                            </li>
                                            <li class="">
                                                <a href="#tab_notifications" data-toggle="tab">Notificaciones</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="tab-content">
                                            <!-- PERSONAL INFO TAB -->
                                            <div class="tab-pane active" id="tab_edit_user">

                                                    <div class="form-group col-md-12 col-mobile-1">
                                                        <label class="control-label">Nombre</label>
                                                        <input type="text" name="first_name" class="form-control" required value="<?=set_value("first_name",$userToEdit["usu_nombre"])?>" />
                                                    </div>
                                                    <div class="form-group col-md-12 col-mobile-1">
                                                        <label class="control-label">Apellido</label>
                                                        <input type="text" name="last_name" class="form-control" required value="<?=set_value("last_name",$userToEdit["usu_apellido"])?>" />
                                                    </div>
                                                    <div class="form-group col-md-12 col-mobile-1">
                                                        <label class="control-label">Email</label>
                                                        <input type="email" name="email" class="form-control" required value="<?=set_value("email",$userToEdit["usu_email"])?>" />
                                                    </div>
                                                    <div class="form-group col-md-3 col-mobile-1">
                                                        <label class="control-label">Ciudad</label>
                                                        <select name="state" required class="bs-select form-control">
                                                            <?php
                                                            $option = '<option value="">Seleccionar Ciudad</option>';
                                                            foreach ($arrayState as $id => $state)
                                                            {
                                                                $selected = $userToEdit["usu_ciu_id"] == $id?" selected ":"";
                                                                $option .= '<option value="'.$id.'" '.$selected.'>'.$state.'</option>';
                                                            }
                                                            echo $option;
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-12 col-mobile-1">
                                                        <label class="control-label">Telefono</label>
                                                        <input type="text" name="phone" class="form-control" <?=$isAdmin?"":"required";?> data-parsley-type="digits" value="<?=set_value("phone",$userToEdit["usu_telefono"])?>" />
                                                    </div>
                                                    <div class="form-group col-md-12 col-mobile-1">
                                                        <label class="control-label">Celular</label>
                                                        <input type="text" name="cellphone" class="form-control" <?=$isAdmin?"":"required";?> data-parsley-type="digits" value="<?=set_value("cellphone",$userToEdit["usu_celular"])?>" />
                                                    </div>
                                                    <?php
                                                    if($userToEdit["usu_tipo"] == "Particular")
                                                    {
                                                        ?>
                                                        <style>
                                                            .btn-group .active {
                                                                background-color: #24a5d9;
                                                                color: #fff;
                                                            }
                                                        </style>
                                                        <div class="form-group">
                                                            <label class="control-label">Cambiar tipo de Usuario</label>
                                                            <div class="clearfix">
                                                                <div class="btn-group btn-group-circle"
                                                                     data-toggle="buttons">
                                                                    <label class="btn btn-default ">
                                                                        <input type="radio" name="user-type"
                                                                               value="Inmobiliaria" class="toggle">
                                                                        Inmobiliaria </label>
                                                                    <label class="btn btn-default active">
                                                                        <input type="radio" name="user-type"
                                                                               value="Particular" <?php echo $userToEdit["usu_tipo"] == "Particular" ? 'checked' : ''; ?>
                                                                               class="toggle"> Particular </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php
                                                    }
                                                    ?>
                                                    <br />
                                            </div>
                                            <!-- END PERSONAL INFO TAB -->

                                            <?php
                                            if($userToEdit["usu_tipo"] == "Inmobiliaria")
                                            {
                                                ?>
                                                <!-- COMPANY INFO TAB -->
                                                <div class="tab-pane" id="tab_edit_company">
                                                    <div class="form-group col-md-12 col-mobile-1">
                                                        <label class="control-label">Empresa</label>
                                                        <input type="text" name="company" class="form-control" required value="<?=set_value("company",$userToEdit["usu_empresa"])?>"/>
                                                    </div>
                                                    <div class="form-group col-md-12 col-mobile-1">
                                                        <label class="control-label">NIT</label>
                                                        <input type="text" name="ci_nit" class="form-control" value="<?=set_value("ci_nit",$userToEdit["usu_ci"])?>"/>
                                                    </div>
                                                    <div class="form-group col-md-12 col-mobile-1">
                                                        <label class="control-label">Direccion</label>
                                                        <input type="text" name="address" class="form-control" value="<?=set_value("address",$userToEdit["usu_direccion"])?>"/>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <label class="control-label">Acerca de la Empresa</label>
                                                        <textarea class="form-control" rows="3" name="description"><?=set_value("description",$userToEdit["usu_descripcion"])?></textarea>
                                                    </div>
                                                    <div class="form-group col-md-12 col-mobile-1">
                                                        <label class="control-label">Skype</label>
                                                        <input type="text" name="skype" class="form-control" value="<?=set_value("skype",$userToEdit["usu_skype"])?>"/>
                                                    </div>
                                                    <div class="form-group col-md-12 col-mobile-1">
                                                        <label class="control-label">Facebook</label>
                                                        <input type="text" name="facebook" placeholder="https://www.facebook.com/su_perfil" data-parsley-type="url" value="<?=set_value("facebook",$userToEdit["usu_facebook"])?>" class="form-control"/>
                                                    </div>
                                                    <div class="form-group col-md-12 col-mobile-1">
                                                        <label class="control-label">Twitter</label>
                                                        <input type="text" name="twitter" placeholder="https://www.twitter.com/su_perfil" data-parsley-type="url" value="<?=set_value("twitter",$userToEdit["usu_twitter"])?>" class="form-control"/>
                                                        <input type="hidden" name="tab_position" value="company"/>
                                                    </div>
                                                    <div class="form-group col-md-12 col-mobile-1">
                                                        <label class="control-label">Ubicacion:
                                                        </label>
                                                        <br/>

                                                    </div>
                                                    <!-- BEGIN GEOCODING PORTLET-->
                                                    <div class="col-md-4 col-lg-4">
                                                        <div class="form-group">
                                                            <label class="required" data-field="state">Departamento</label>
                                                            <select class="select2" name="enterprise-state">
                                                                <?php
                                                                $options = "";
                                                                foreach($arrayState as $stateId => $stateName)
                                                                {
                                                                    $selected = $city["ciu_dep_id"] == $stateId?"selected":"";
                                                                    $options .= '<option value="'.$stateId.'" '.$selected.' >'.$stateName.'</option>';
                                                                }
                                                                echo $options;
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 col-lg-4">
                                                        <div class="form-group">
                                                            <?php
                                                            $select2Data["id"] = 1;
                                                            $select2Data["text"] = "Santa Cruz de la Sierra";
                                                            $select2Data = json_encode($select2Data);
                                                            if(isset($city))
                                                            {
                                                                $select2Data["id"] = $city["ciu_id"];
                                                                $select2Data["text"] = $city["ciu_nombre"];
                                                                $select2Data = json_encode($select2Data);
                                                            }
                                                            ?>
                                                            <label>Ciudad</label>
                                                            <input type="text" id="ajax-get-cities" name="city" parsley-trigger="change" value='<?=$select2Data?>'>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <label class="required" data-field="name">
                                                            <div class="input-group">
                                                                <input type="text" class="form-control search-address-data" placeholder="Ingrese ubicacion">
                                                                <span class="input-group-addon search-address-data-city hide">Santa Cruz de la Sierra</span>
                                                                <span class="input-group-addon search-address-data-state hide">Santa Cruz</span>
                                                                <span class="input-group-btn"><button class="btn btn-primary search-address-button" type="button"><i class="fa fa-search"></i></button></span>
                                                            </div>
                                                        </label>
                                                        <div class="form-group">
                                                            <div class="map-fancy-framework">
                                                                <div class="hide">
                                                                    <input name="latitude" value="<?=set_value('latitude',$userToEdit["usu_latitud"])?>" required>
                                                                    <input name="longitude" value="<?=set_value('longitude',$userToEdit["usu_longitud"])?>" required data-parsley-errors-container="#map-location-error-message-parsley" data-parsley-error-message="Debe marcar un punto en el mapa">
                                                                </div>
                                                                <div id="maps">
                                                                </div>
                                                                <em class="map-search-message"></em>
                                                                <div id="map-location-error-message-parsley"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- END GEOCODING PORTLET-->
                                                    <br/>
                                                </div>
                                                <!-- END COMPANY INFO TAB -->
                                                <?php
                                            }
                                            ?>
                                            <!-- CHANGE AVATAR TAB -->
                                            <div class="tab-pane" id="tab_change_image">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <p>
                                                            Para modificar la imagen de su perfil, seleccione una imagen y presione el boton "Subir Imagen".
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="row">

                                                    <div class="col-md-12">
                                                        <div>
                                                            <label data-field="avatar">Logo<br><em>Click en la imagen para elegir otra</em></label>
                                                        </div>
                                                        <div class="avatar-upload">
                                                            <div style="max-width: 200px; max-height: 200px; overflow:hidden">
                                                                <?php
                                                                    $fileHandler = new File_Handler($userToEdit["usu_foto"],"userImage");
                                                                    $thumbnail = $fileHandler->getThumbnail(200,200);
                                                                ?>
                                                                <img src="<?=$thumbnail->getSource()?>" class="profile-avatar img-thumbnail" id="avatar-preview" alt="Click to change." title="Click to change"/>
                                                            </div>
                                                            <input id="avatar-file" type="file" name="logo" accept="image/*">
                                                            <div id="progress" class="overlay"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br />
                                            </div>
                                            <!-- END CHANGE AVATAR TAB -->

                                            <!-- CHANGE PASSWORD TAB -->
                                            <div class="tab-pane" id="tab_change_password">
                                                    <div class="form-group col-md-12 col-mobile-1">
                                                        <label class="control-label">Nueva Contraseña</label>
                                                        <input type="password" name="password" class="form-control" minlength="6" id="password"/>
                                                    </div>
                                                    <div class="form-group col-md-12 col-mobile-1">
                                                        <label class="control-label">Repetir Nueva Contraseña</label>
                                                        <input type="password" name="repassword" class="form-control" minlength="6" data-parsley-equalto="#password" data-parsley-equalto-message="Las contraseñas introducidas no coinciden"/>
                                                    </div>
                                                    <br />
                                            </div>
                                            <!-- END CHANGE PASSWORD TAB -->

                                            <!-- PRIVACY SETTINGS TAB -->
                                            <div class="tab-pane" id="tab_notifications">
                                                    <table class="table table-light table-hover">
                                                        <tr>
                                                            <td>Desea mostrar su datos de contacto?</td>
                                                            <td>
                                                                <label class="uniform-inline"><input type="radio" name="datos_contacto" value="Si" <?php echo $userToEdit["usu_dat_contacto"] == 'Si' ? 'checked':''; ?> /> Si </label>
                                                                <label class="uniform-inline"><input type="radio" name="datos_contacto" value="No" <?php echo $userToEdit["usu_dat_contacto"] == 'No' ? 'checked':''; ?> /> No </label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Recibir notificaciones por email sobre sus anuncios</td>
                                                            <td>
                                                                <label class="uniform-inline"><input type="radio" name="email_reporte" value="1" <?php echo $userToEdit["usu_email_reporte"] == 1 ? 'checked':''; ?>/> Si </label>
                                                                <label class="uniform-inline"><input type="radio" name="email_reporte" value="0" <?php echo $userToEdit["usu_email_reporte"] == 0 ? 'checked':''; ?>/> No </label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <br />
                                            </div>
                                            <!-- END PRIVACY SETTINGS TAB -->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button class="btn blue">Guardar Cambios</button>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PROFILE CONTENT -->
                </div>
            </div>
        </form>
    </div>
</div>