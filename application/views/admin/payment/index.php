<?php
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 20/7/2017
 * Time: 3:02 PM
 */
?>
<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-table"></i>
            <span class="caption-subject font-green-sharp bold uppercase">Pagos</span>
        </div>
    </div>
    <div class="portlet-body">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <form class="form-group" id="extra-request-data">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="radio-inline">
                                                <input type="radio" name="date-to-filter" value="payment-request" checked> Solicitud de pago
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="date-to-filter" value="payment-settled"> Fecha de pago
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="basic-addon1">Desde</span>
                                            <input type="text" name="start-date" data-date-format="dd-mm-yyyy" parsley-trigger="change" placeholder="dia-mes-año" readonly class="form-control date-picker">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="basic-addon1">Hasta</span>
                                            <input type="text" name="end-date" data-date-format="dd-mm-yyyy" parsley-trigger="change" placeholder="dia-mes-año" readonly class="form-control date-picker" value="">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="radio-inline">
                                                <input type="radio" name="payment-status" value="Pagado"> Pagado
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="payment-status" value="No Pagado"> No pagado
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="payment-status" value="Pendiente"> Pendiente
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="payment-status" value="Eliminado"> Eliminado
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="payment-status" value="" checked> Todos
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary input-sm" id="send-filters" type="button" data-content-data="chart-property-offers-based-on-property-types">Filtrar</button>
                            <button class="btn btn-danger input-sm" id="remove-additional-parameters" type="button" data-content-data="chart-property-offers-based-on-property-types">Remove parametros adicionales</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover" id="data-table">
                            <thead>
                            <tr>
                                <th class="text-center">PAYMENT ID</th>
                                <th>Nombre inmueble</th>
                                <th class="text-center">Fecha de solic. de pago</th>
                                <th class="text-center">Fecha de pago</th>
                                <th class="text-center">Nombre<br>usuario</th>
                                <th class="text-center">Apellido<br>usuario</th>
                                <th>Email</th>
                                <th>Tipo</th>
                                <th>Monto</th>
                                <th>Metodo</th>
                                <th>Concepto</th>
                                <th>Estado</th>
                                <th>Ver/Aprobar pago</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script id="ht-payment-detail" type="text/x-handlebars-template">
    <div class="row">
        <div class="col-md-6">
            <p>
                <b>Payment ID:</b> {{detail.paymentId}}<br>
                <b>Nombre usuario:</b> {{detail.userFullName}}<br>
                <b>Fecha solicitud:</b> {{detail.paymentCreatedOn}}
                <b>Nombre pago:</b> {{detail.paymentName}}<br>
                <b>Nit pago:</b> {{detail.paymentNit}}
            </p>
        </div>
        <div class="col-md-6">
            <p>
                <b>Tipo usuario:</b> {{detail.userType}}<br>
                <b>Metodo de pago:</b> {{detail.paymentMethod}}<br>
                <b>Monto:</b> {{detail.paymentAmount}}Bs.
            </p>
        </div>
    </div>
    <table class="table table-condensed">
        <thead>
        <tr>
            <th>#</th>
            <th>Servicio</th>
            <th>Inicio</th>
            <th>Fin</th>
            <th>Precio</th>
        </tr>
        </thead>
        <tbody>
        {{#each detail.serviceList}}
        <tr>
            <th scope="row">{{index}}</th>
            <td>{{name}}</td>
            <td>{{startDate}}</td>
            <td>{{finishDate}}</td>
            <td>{{price}}</td>
        </tr>
        {{/each}}
        </tbody>
    </table>
</script>