<?php
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 4/12/2017
 * Time: 4:12 PM
 */
$menu_active = $this->uri->segment(2);
$project_url = $this->uri->segment(1);
$path_assets = base_url() . "assets/";
$project = $project_data["project"];
$lower_price = $project_data["lower_price"];
$slideshow = $project_data["slideshow"];
$social = $project_data["sw_area_social"];
$data["cities"] = $cities;
$data["project_data"] = $project_data;
$data["project_config"] = $project_config;

$tabList =
    array(
        array("href" => "#hero-area", "label" => "Inicio", "show" => "Si", "class" => "active", "style1" => "", "style2" => ""),
        array("href" => "#about-area", "label" => "Acerca de..", "show" => "Si", "class" => "", "style1" => "", "style2" => ""),
        array("href" => "#typology", "label" => $project_config->menu_precio, "show" => $project_config->check_precio, "style1" => "", "style2" => ""),
        array("href" => "#finishing-detail", "label" => $project_config->menu_acabado, "show" => $project_config->check_acabado, "style1" => "", "style2" => ""),
        array("href" => "#gallery-area", "label" => $project_config->menu_galeria, "show" => $project_config->check_galeria, "style1" => "", "style2" => ""),
        array("href" => "#location-area", "label" => "Mapa", "show" => "Si", "style1" => "", "style2" => ""),
        array("href" => "#location-area", "label" => "Consultar", "show" => $project_config->check_formulario, "class" => "navbar-contact-button", "style1" => "background-color: #567fbb;color: #FFF;", "style2" => "color: #fff;"),
        array("href" => base_url(), "label" => "buscar un inmueble", "show" => "Si", "class" => "navbar-toque-el-timbre-link", "style1" => "background-color: #567fbb;color: #FFF;", "style2" => "color: #fff;")
    );
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?=$project->proy_nombre?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php
    $this->load->view("facebook-meta-tags");
    ?>
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
    <!-- CSS -->
    <!-- Bootstrap CSS
	============================================ -->
    <link rel="stylesheet" href="<?=assets_url("project-template/css/bootstrap.min.css")?>">
    <!-- Icon Font CSS
	============================================ -->
    <link rel="stylesheet" href="<?=assets_url("project-template/css/material-design-iconic-font.min.css")?>">
    <link rel="stylesheet" href="<?=assets_url("project-template/css/font-awesome.min.css")?>">
    <script defer src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>
    <!-- Plugins CSS
	============================================ -->
    <link rel="stylesheet" href="<?=assets_url("project-template/css/plugins.css")?>">
    <!-- Style CSS
	============================================ -->
    <link rel="shortcut icon" href="<?=assets_url("backend/img/icons/favicon.ico")?>">
    <link rel="stylesheet" href="<?=assets_url("project-template/style.css")?>">
    <link rel="stylesheet" href="<?=assets_url("css/owlcarousel/owl.carousel.css")?>">
    <link rel="stylesheet" href="<?=assets_url("css/owlcarousel/owl.theme.css")?>">
    <link rel="stylesheet" type="text/css" href="<?=assets_url("backend/css/quick-login.css")?>">
    <link rel="stylesheet" href="<?=assets_url("project-template/custom-styles.css")?>">
    <!-- Modernizer JS
	============================================ -->
    <script src="<?=assets_url("project-template/js/vendor/modernizr-2.8.3.min.js")?>"></script>
    <script>
        var base_url ="<?=base_url()?>";
    </script>
</head>

<body>
<!-- Facebook Pixel Code -->
<script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window,document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '1481111128837486');
    fbq('track', 'PageView');
</script>
<noscript>
    <img height="1" width="1"
         src="https://www.facebook.com/tr?id=1481111128837486&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->
<!-- Facebook Login Code -->
<div id="fb-root"></div>
<script>
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v2.10&appId=<?=$facebookAppIdAndSecretKey["appId"]?>";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<!-- END Facebook Login Code -->
<!-- Body main wrapper start -->
<div class="wrapper fix">

    <!-- Header Area Start -->
    <div id="header-area" class="header-area section">

        <!-- Header Top Start -->
        <div class="header-top hidden-xs">
            <div class="container">
                <div class="row">
                    <div class="header-info text-left col-md-6">
                        <img src="<?=base_url("assets/img/icons/icono_map1.png")?>" width="20px" height="auto">
                    </div>
                    <div class="header-social text-right col-md-6 hidden-xs">
                        <a target="_blank" href="<?=base_url()?>" class="btn btn-primary btn-xs search-property">BUSCAR UN INMUEBLE</a>
                        <a href="https://www.facebook.com/ToqueelTimbre/?ref=br_rs" target="_blank"><i class="fab fa-facebook-f"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Header Top End -->

        <!-- Header Bottom Start -->
        <div class="header-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">

                        <div class="navbar-header">
                            <button class="menu-toggle navbar-toggle collapsed" data-toggle="collapse" data-target="#main-menu">
                                <i class="zmdi open zmdi-menu"></i>
                                <i class="zmdi close zmdi-close"></i>
                            </button>
                            <?php
                            $image = new File_Handler($project->proy_logo,"projectImage");
                            $thumbnail = $image->getThumbnail("auto","75","Logo del proyecto");
                            ?>
                            <img src="<?=$thumbnail->getSource()?>" alt="<?=$thumbnail->getAlternateText()?>" style="max-width: 200px">
                        </div>

                        <!---- Menu ---->
                        <div id="main-menu" class="main-menu onepage-nav collapse navbar-collapse float-right">
                            <nav>
                                <ul>
                                    <?php
                                    $html = "";
                                    foreach ($tabList as $tab)
                                    {
                                        if($tab['show'] == "Si")
                                        {
                                            $html .= '<li class="'.$tab['class'].'" style="'.$tab['style1'].'"><a href="'.$tab['href'].'" style="'.$tab['style2'].'">'.$tab['label'].'</a></li>';
                                        }
                                    }
                                    echo $html;
                                    ?>
                                </ul>
                            </nav>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- Header Bottom End -->

    </div>
    <!-- Header Area End -->

    <!-- Hero Area Start -->
    <div id="hero-area" class="hero-area section <?=showContent($tabList,"#hero-area")?>">

        <!-- Hero Slider Start -->
        <!-- desktop version -->
        <div class="hero-slider desktop-version">
            <?php
            if(!empty($slideshow))
            {
                $i = 0;
                foreach ($slideshow as $image)
                {
                    $image = new File_Handler($image->imagen,"projectSlideShowImage");
                    $banner = $image->getThumbnail("1920","500");
                    $thumbnail = $image->getThumbnail("76","76");
                    if($i == 0)
                    {
                        $about = $image->getThumbnail("318","352");
                    }
                    ?>
                    <!-- Single Hero Item -->
                    <div class="hero-item overlay" data-thumb="<?=$thumbnail->getSource()?>">
                        <img src="<?=$banner->getSource()?>" alt="">
                        <div class="container">
                            <div class="row">
                                <div class="hero-content col-xs-12">
                                    <h2>Bienvenido a</h2>
                                    <h1><?=$project->proy_nombre?></h1>
                                    <a href="#location-area">Consulte <?=$project->proy_id == 74?"a Park City":"al anunciante";?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    $i++;
                }
            }
            ?>
        </div>
        <!-- mobile version -->
        <div class="hero-slider mobile-version">
            <?php
            if(!empty($slideshow))
            {
                $i = 0;
                foreach ($slideshow as $image)
                {
                    $image = new File_Handler($image->imagen,"projectSlideShowImage");
                    $banner = $image->getThumbnail("360","640");
                    $thumbnail = $image->getThumbnail("76","76");
                    if($i == 0)
                    {
                        $about = $image->getThumbnail("318","352");
                    }
                    ?>
                    <!-- Single Hero Item -->
                    <div class="hero-item overlay" data-thumb="<?=$thumbnail->getSource()?>">
                        <img src="<?=$banner->getSource()?>" alt="">
                        <div class="container">
                            <div class="row">
                                <div class="hero-content col-xs-12">
                                    <h2>Bienvenido a</h2>
                                    <h1><?=$project->proy_nombre?></h1>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    $i++;
                }
            }
            ?>
        </div>
        <div class="fixed-buttons-box" data-phone="<?=$project->usu_celular?>">
<!--            <div class="col-xs-12">-->
                <a href="#" id="open-modal-form-message" style="display: block">
                    CONSULTAR <i class="far fa-envelope"></i>
                </a>
<!--            </div>-->
        </div>
        <!---- Hero Slider End ---->
    </div>
    <!-- Hero Area End -->

    <!-- About Area Start -->
    <div id="about-area" class="about-area bg-gray section pb-120 pt-120 <?=showContent($tabList,"#about-area")?>">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="about-image float-left hidden-xs hidden-sm">
                        <img src="<?=$about->getSource()?>" alt="">
                    </div>
                    <div class="about-content fix">
                        <h1>Acerca de <span class="title-span"><?=$project->proy_nombre?></span></h1>
                        <?php
                        if($user_logged->usu_tipo == "Admin") {
                            ?>
                            <a href="#" id="test-whatsapp">test</a>
                            <?php
                        }
                        ?>
                        <h4>Detalles del proyecto</h4>
                        <p>
                            <?=$project->proy_descripcion?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- About Area End -->

    <!-- Amenities Area Start -->
    <div id="amenities-area" class="amenities-area section hide">
        <img src="img/amenities/1.jpg" class="hidden-lg hidden-md" alt="">
        <div class="amenities-content overlay col-lg-7 col-lg-offset-5 col-md-8 col-md-offset-4 col-xs-12">
            <h2>AMENITIES</h2>
            <p>Contrary to popular belief, Lorem Ipsutm is nosimply ran roots in a piece of classical Latinrature from 45 BC, mag it over 2old. Richard Mc fm sonte valobasi.</p>

            <ul>
                <li>
                <span class="border">
                </span><span class="left">Bedrooms</span>
                    <span class="right">05</span>
                </li>
                <li>
                <span class="border">
                </span><span class="left">Kindergardens nearby</span>
                    <span class="right">02</span>
                </li>
                <li>
                <span class="border">
                </span><span class="left">Living Rooms</span>
                    <span class="right">04</span>
                </li>
                <li>
                <span class="border">
                </span><span class="left">Security system</span>
                    <span class="right">04</span>
                </li>
                <li>
                <span class="border">
                </span><span class="left">Bathrooms</span>
                    <span class="right">05</span>
                </li>
                <li>
                <span class="border">
                </span><span class="left">Air Conditioning</span>
                    <span class="right">05</span>
                </li>
                <li>
                <span class="border">
                </span><span class="left">Swimming Pool</span>
                    <span class="right">05</span>
                </li>
                <li>
                <span class="border">
                </span><span class="left">Swimming Pool</span>
                    <span class="right">05</span>
                </li>
                <li>
                <span class="border">
                </span><span class="left">Parking places</span>
                    <span class="right">02</span>
                </li>
                <li>
                <span class="border">
                </span><span class="left">Gym</span>
                    <span class="right">yes</span>
                </li>
                <li>
                <span class="border">
                </span><span class="left">Parking places</span>
                    <span class="right">02</span>
                </li>
                <li>
                <span class="border">
                </span><span class="left">Parking places</span>
                    <span class="right">02</span>
                </li>
                <li>
                <span class="border">
                </span><span class="left">Garage places</span>
                    <span class="right">04</span>
                </li>
                <li>
                <span class="border">
                </span><span class="left">Fire alarm</span>
                    <span class="right">yes</span>
                </li>
                <li>
                <span class="border">
                </span><span class="left">Schools nearby</span>
                    <span class="right">05</span>
                </li>
                <li>
                <span class="border">
                </span><span class="left">Schools nearby</span>
                    <span class="right">05</span>
                </li>
            </ul>

        </div>

    </div>

    <!-- Amenities Area End -->

    <div id="typology" class="gallery-area section pb-105 pt-120 <?=showContent($tabList,"#typology")?>">
        <div class="container">
            <div class="row">
                <div class="gallery-section-title text-center col-xs-12 mb-80">
                    <?php
                    $titleArray = explode(" ",$project_config->menu_precio);
                    $middle = floor(count($titleArray)/2);
                    $i = 0;
                    $titleStyled = "";
                    foreach ($titleArray as $word)
                    {
                        if($i == $middle)
                        {
                            $titleStyled .= "<span class=\"title-span\">".$word." ";
                        }
                        elseif($i == (count($titleArray)))
                        {
                            $titleStyled .= "</span> ";
                        }
                        else
                        {
                            $titleStyled .= $word." ";
                        }
                        $i++;
                    }
                    ?>
                    <h2><?=$titleStyled?></h2>
                </div>
            </div>
            <div class="row">
            <?php
            $prices = $project_data["tipologia"];
            $i=0;

            foreach($prices as $price)
            {
                $title= !empty($price->tip_titulo_precio) ? $price->tip_titulo_precio : $price->tip_titulo;
                $image = new File_Handler($price->tip_imagen_precio,"projectImage");
                $thumbnail = $image->getThumbnail(400,"auto");
                if($i == 0)
                {
                    echo "<div class='row'>";
                }
                elseif($i%3 == 0)
                {
                    echo "</div><div class='row'>";
                }
                    ?>
                    <!-- Floor Plan Area Start -->
                        <div class="col-md-4">
                            <div class="thumbnail">
                                <img src="<?=$thumbnail->getSource()?>" alt="">
                                <div class="caption">
                                    <h3><?=$title?></h3>
                                    <p><?=$price->tip_descripcion?></p>
                                    <div class="area-features funfact fix">
                                        <?php
                                        $count = 0;
                                        $hidePrice = $hideMts = $hideTypMts = " hide ";
                                        if($price->tip_precio > 0)
                                        {
                                            $count ++;
                                            $hidePrice = "";
                                        }
                                        if($price->tip_mts > 0)
                                        {
                                            $count ++;
                                            $hideMts = "";
                                        }

                                        if($price->tip_terreno_mts > 0)
                                        {
                                            $count ++;
                                            $hideTypMts = "";
                                        }
                                        $width = $count == 0?"0":100/ $count;
                                        ?>
                                        <div class="single-funfact text-center <?=$hidePrice?>" style="width: <?=$width?>%">
                                            <h2 class="counter"><?=number_format($price->tip_precio,0,".",",")?></h2>
                                            <p>Precio</p>
                                        </div>
                                        <div class="single-funfact text-center <?=$hideMts?>" style="width: <?=$width?>%">
                                            <h2 class="counter"><?=number_format($price->tip_mts,2,".",",")?></h2>
                                            <p>Sup. Const.</p>
                                        </div>
                                        <div class="single-funfact text-center <?=$hideTypMts?>" style="width: <?=$width?>%">
                                            <h2 class="counter"><?=number_format($price->tip_terreno_mts,2,".",",")?></h2>
                                            <p>Superficie</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <!-- Floor Plan Area End -->
                    <?php
                    $i++;
                    if($i == count($prices))
                    {
                        echo "</div>";
                    }

            }
            ?>
            </div>
        </div>
    </div>

    <div id="finishing-detail" class="gallery-area bg-gray section pb-105 pt-120 <?=showContent($tabList,"#finishing-detail")?>">
        <div class="container">
            <div class="row">
                <div class="gallery-section-title text-center col-xs-12 mb-80">
                    <?php
                    $titleArray = explode(" ",$project_config->menu_acabado);
                    $middle = floor(count($titleArray)/2);
                    $i = 0;
                    $titleStyled = "";
                    foreach ($titleArray as $word)
                    {
                        if($i == $middle)
                        {
                            $titleStyled .= "<span class=\"title-span\">".$word." ";
                        }
                        elseif($i == (count($titleArray)))
                        {
                            $titleStyled .= "</span> ";
                        }
                        else
                        {
                            $titleStyled .= $word." ";
                        }
                        $i++;
                    }
                    ?>
                    <h2><?=$titleStyled?></h2>
                </div>
            </div>
            <div class="row">
                <?php
                $finishingDetail = $project_data["acabado"];
                $i = 0;
                foreach($finishingDetail as $detail)
                {
                    $image = new File_Handler($detail->aca_imagen,"projectImage");
                    $thumbnail = $image->getThumbnail(400,"auto");
                    if($i == 0)
                    {
                        echo "<div class='row'>";
                    }
                    elseif($i%3 == 0)
                    {
                        echo "</div><div class='row'>";
                    }
                    ?>
                    <!-- Floor Plan Area Start -->
                    <div class="col-md-4">
                        <div class="thumbnail">
                            <img src="<?=$thumbnail->getSource()?>" alt="">
                            <div class="caption hide">
                                <p><?=$detail->aca_descripcion?></p>
                            </div>
                        </div>
                    </div>

                    <!-- Floor Plan Area End -->
                    <?php
                    $i++;
                    if($i == count($finishingDetail))
                    {
                        echo "</div>";
                    }

                }
                $hideQualityDetails = $project->proy_id == 74?"":" hide ";
                ?>
                <div class="row <?=$hideQualityDetails?>">
                    <div class="col-md-12">
                        <div class="about-content">
                            <p class="MsoNormal">
                                <strong>Detalles de Calidad:</strong><br>
                                - Cableado subterráneo en todo el condominio, elimina contaminación visual y reduce riesgo de accidentes.<br>
                                - Doble pared de ladrillo para aislamiento acústico entre casa y casa.<br>
                                - Micropilotes implementados por Incotec, previenen fisuras.<br>
                                - Teja gran española con encastre (oculta) para evitar goteras, con cercha metálica que evita turiros.<br>
                                - Materiales externos  impermeabilizantes.<br>
                                - Ventanas de aluminio español.<br>
                                - Vidrio laminado que brinda mayor seguridad, mejor térmica y acústica.<br>
                                - Puertas Vicaima importadas de Portugal con certificación Europea, que evitan ruidos y ensanchamientos.<br>
                                - Mesones de baño y cocina en Corian de Dupont material que evita manchas, olores y bacterias.<br>
                                - Porcelanato 60 X 60 cm.<br>
                                - Pisos con aislamiento de humedad.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Feature Area Start -->
    <div id="feature-area" class="feature-area section pb-20 pt-80 hide">
        <div class="container">
            <div class="row">

                <div class="feature-wrapper col-md-8">
                    <h2>Awesome Features</h2>
                    <p>Anything embarrassing hide in the middle of text. All the Lorem Ipsum generators on</p>

                    <div class="row">

                        <div class="single-feature col-sm-6">
                            <img src="img/feature/1.png" alt="">
                            <h4>Awesome Amenities</h4>
                            <p>Anything embarrassing hide in the middle of text. All the Lorem Ipsum generators on the</p>
                        </div>

                        <div class="single-feature col-sm-6">
                            <img src="img/feature/2.png" alt="">
                            <h4>Power and Energy</h4>
                            <p>Anything embarrassing hide in the middle of text. All the Lorem Ipsum generators on the</p>
                        </div>

                        <div class="single-feature col-sm-6">
                            <img src="img/feature/3.png" alt="">
                            <h4>24 Hours Service</h4>
                            <p>Anything embarrassing hide in the middle of text. All the Lorem Ipsum generators on the</p>
                        </div>

                        <div class="single-feature col-sm-6">
                            <img src="img/feature/4.png" alt="">
                            <h4>Good Neighbourhood</h4>
                            <p>Anything embarrassing hide in the middle of text. All the Lorem Ipsum generators on the</p>
                        </div>

                    </div>

                </div>

            </div>
        </div>
    </div>
    <!-- Feature Area End -->

    <!-- Gallery Area Start -->
    <div id="gallery-area" class="gallery-area section pb-105 pt-120 <?=showContent($tabList,"#gallery-area")?>">
        <div class="container">

            <div class="row">

                <div class="gallery-section-title text-center col-xs-12 mb-80">
                    <p>Tour Del Proyecto</p>
                    <?php
                    $titleArray = explode(" ",$project_config->menu_galeria);
                    $middle = floor(count($titleArray)/2);
                    $i = 0;
                    $titleStyled = "";

                    foreach ($titleArray as $word)
                    {
                        if($i == $middle)
                        {
                            $titleStyled .= "<span class=\"title-span\">".$word." ";
                        }
                        elseif($i == (count($titleArray)))
                        {
                            $titleStyled .= "</span> ";
                        }
                        else
                        {
                            $titleStyled .= $word." ";
                        }
                        $i++;
                    }
                    ?>
                    <h2><?=$titleStyled?></h2>
                    <p></p>
                    <p class="hide">
                        <a href="#" class="filter-images" data-image-type="all">Todos</a> |
                        <a href="#" class="filter-images" data-image-type="prices"><?=$project_config->menu_precio?></a> |
                        <a href="#" class="filter-images" data-image-type="finishing-detail"><?=$project_config->menu_acabado?></a>
                    </p>
                </div>

            </div>

            <div class="row row-8 gallery-image-content">
                <div id="properties-carousel" class="on-carousel" style="margin-bottom: 15px;">
                <?php
                $gallery = $project_data["gallery"];
                $i = 0;

                foreach($gallery as $image)
                {
                    $fileHandler = new File_Handler($image->proy_fot_imagen,"projectImage");
                    $image = $fileHandler->getThumbnail(900,"auto");
                    $height = ((191*100)/288)/100;
                    $finalWidth = 380;
                    $finalHeight = 380*$height;
                    $thumbnailImage = $fileHandler->getThumbnail($finalWidth,$finalHeight);
                ?>
                <div class="gallery-item all">
                    <a href="<?=$image->getSource()?>" class="image gallery-popup">
                        <img src="<?=$thumbnailImage->getSource()?>" alt="">
                    </a>
                </div>
                <?php
                    $i++;
                    if($i == count($gallery))
                    {
                        echo "</div>";
                    }
                }
                foreach($prices as $price)
                {break;
                    $fileHandler = new File_Handler($price->tip_imagen_precio,"projectImage");
                    $image = $fileHandler->getThumbnail(500,500);
                    $thumbnailImage = $fileHandler->getThumbnail(380,300);
                    ?>
                    <div class="gallery-item col-md-3 col-sm-6 col-xs-12 prices">
                        <a href="<?=$image->getSource()?>" class="image gallery-popup">
                            <img src="<?=$thumbnailImage->getSource()?>" alt="">
                        </a>
                    </div>
                    <?php
                }
                foreach($finishingDetail as $detail)
                {break;
                    $fileHandler = new File_Handler($detail->aca_imagen,"projectImage");
                    $image = $fileHandler->getThumbnail(500,500);
                    $thumbnailImage = $fileHandler->getThumbnail(380,300);
                    ?>
                    <div class="gallery-item col-md-3 col-sm-6 col-xs-12 finishing-detail">
                        <a href="<?=$image->getSource()?>" class="image gallery-popup">
                            <img src="<?=$thumbnailImage->getSource()?>" alt="">
                        </a>
                    </div>
                    <?php
                }
                ?>
                </div>
            </div>
        </div>
    </div>
    <!-- Gallery Area End -->
    <!-- Location Area Start -->
    <?php
    $hideForm = $project_config->check_formulario == "Si"?"":" hide ";
    ?>
    <div id="location-area" class="location-area bg-gray section pb-120 pt-120 <?=showContent($tabList,"#location-area")?>">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="gallery-section-title text-center col-xs-12 mb-80">
                        <h2>Nuestra <span class="title-span">Ubicacion</span></h2>
                        <?=$project->usu_email." ".$project->usu_celular?>
                    </div>
                    <div class="col-xs-12">
                        <input name="latitude" value="<?=$project->proy_latitud?>" type="hidden" required>
                        <input name="longitude" value="<?=$project->proy_longitud?>" type="hidden" required>
                        <div class="map-fancy-framework" style="margin-bottom: 20px;">
                            <div id="maps" style="height: 300px;width: auto;margin-bottom:10px">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 <?=showContent($tabList,"#agent-area")?> contact-section">
                    <div class="gallery-section-title text-center col-xs-12 mb-80">
                        <h2>CONSULTE <span class="title-span" style="line-height: 27px;"><?=$project->proy_id == 74?"a Park City":"al anunciante";?></span></h2>
                    </div>
                    <div class="col-xs-12">
                        <?php
                        $hideContact = $project_config->check_contacto == " hide "?"":" hide ";
                        ?>
                        <div class="<?=$hideContact?>">
                            <?=$project->proy_contacto?>
                        </div>
                        <div class="agent-contact-form fix project-contact-form">
                            <form name="new-form-contact" action="#" class="form_contact_project"  data-parsley-validate>
                                <input type="hidden" name="proy_nombre" value="<?=$project->proy_nombre?>">
                                <input type="hidden" name="email_send" value="<?=$project->usu_email?>">
                                <input type="hidden" name="proy_id" value="<?=$project->proy_id?>" />
                                <?php
                                if(!$user_logged instanceof stdClass)
                                {
                                    ?>
                                    <div class="input-box">
                                        <input type="text" placeholder="Nombre completo" name="nombre" required>
                                    </div>
                                    <div class="input-box">
                                        <input type="text" placeholder="Telefono" name="telefono" required
                                               data-parsley-type="number">
                                    </div>
                                    <?php
                                }
                                ?>
                                    <?php
                                    if(!$user_logged instanceof stdClass)
                                    {
                                        ?>
                                        <div class="input-box">
                                            <input type="email" placeholder="Email" name="email" required>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                    <div class="input-box">
                                        <select name="ciudad" required>
                                            <option value="Santa Cruz">Santa Cruz</option>
                                            <option value="La Paz">La Paz</option>
                                            <option value="Cochabamba">Cochabamba</option>
                                            <option value="Tarija">Tarija</option>
                                            <option value="Chuquisaca">Chuquisaca</option>
                                            <option value="Oruro">Oruro</option>
                                            <option value="Potosi">Potosi</option>
                                            <option value="Pando">Pando</option>
                                            <option value="Beni">Beni</option>
                                        </select>
                                    </div>
                                <div class="input-box"><textarea placeholder="Consulta" required name="mensaje"></textarea></div>
                                <div class="input-box submit-box">
                                    <div class="g-recaptcha"
                                         data-sitekey="<?=$reCaptchaKeys["publicKey"]?>"
                                         data-callback="sendRequest"
                                         data-size="invisible">
                                    </div>
                                    <input type="submit" value="Enviar">
                                    <img src="<?=base_url("assets/img/loading.gif")?>" class="hide"/>
                                    <br><br>
                                    <div class="content-result-message"></div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Location Area End -->
    <!-- Testimonial Area Start -->
    <div id="testimonial-area" class="testimonial-area section pb-120 pt-120 hide">
        <div class="container">

            <div class="row">

                <div class="testimonial-section-title text-center col-xs-12 mb-40">
                    <h2>Clientes Felices</h2>
                </div>

            </div>

            <div class="row">

                <div class="col-md-8 col-md-offset-2 col-xs-12">
                    <div class="testimonial-slider text-center">

                        <div class="testimonial-item">
                            <p>There are many variations of passages of Lorem Ipsum available, but the majority have sffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage</p>
                            <i class="zmdi zmdi-quote"></i>
                            <h4>Arif Hussain</h4>
                        </div>

                        <div class="testimonial-item">
                            <p>There are many variations of passages of Lorem Ipsum available, but the majority have sffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage</p>
                            <i class="zmdi zmdi-quote"></i>
                            <h4>Arif Hussain</h4>
                        </div>

                    </div>
                </div>

            </div>

        </div>
    </div>
    <!-- Testimonial Area End -->

    <!-- Agent Area Start -->

    <div id="agent-area" class="agent-area section hide">
        <div class="container">

            <div class="row">



            </div>

        </div>
    </div>
    <!-- Agent Area End -->

    <!-- Blog Area Start -->
    <div id="blog-area" class="blog-area overlay section pb-90 pt-120 hide">
        <div class="container">

            <div class="row">

                <div class="blog-section-title text-center col-xs-12 mb-70">
                    <h2>latest blog</h2>
                </div>

            </div>

            <div class="row">
                <div class="blog-slider">

                    <div class="blog-item col-md-4 col-sm-6 col-xs-12 mb-30">
                        <a href="blog-details.html" class="image"><img src="img/blog/1.jpg" alt=""></a>
                        <div class="content white">
                            <h4><a href="blog-details.html">Fall In Love With Single Property</a></h4>
                            <div class="meta fix">
                                <span>June 20, 2017</span>
                                <span>By <a href="#">Ashes</a></span>
                            </div>
                            <p>The standard chunk of Lorem Ipsum used since the 1500s is reprduced below for those interested. Sections 1.10.32 and 1.10.33 from psum used since the 1500s is reprduced below for</p>
                        </div>
                    </div>

                    <div class="blog-item col-md-4 col-sm-6 col-xs-12 mb-30">
                        <a href="blog-details.html" class="image"><img src="img/blog/2.jpg" alt=""></a>
                        <div class="content white">
                            <h4><a href="blog-details.html">Best 50 Tips For Single Property</a></h4>
                            <div class="meta fix">
                                <span>June 20, 2017</span>
                                <span>By <a href="#">Ashes</a></span>
                            </div>
                            <p>The standard chunk of Lorem Ipsum used since the 1500s is reprduced below for those interested. Sections 1.10.32 and 1.10.33 from psum used since the 1500s is reprduced below for</p>
                        </div>
                    </div>

                    <div class="blog-item col-md-4 col-sm-6 col-xs-12 mb-30">
                        <a href="blog-details.html" class="image"><img src="img/blog/3.jpg" alt=""></a>
                        <div class="content white">
                            <h4><a href="blog-details.html">The Ultimate Deal On Single Property</a></h4>
                            <div class="meta fix">
                                <span>June 20, 2017</span>
                                <span>By <a href="#">Ashes</a></span>
                            </div>
                            <p>The standard chunk of Lorem Ipsum used since the 1500s is reprduced below for those interested. Sections 1.10.32 and 1.10.33 from psum used since the 1500s is reprduced below for</p>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
    <!-- Blog Area End -->

    <!-- Newsletter Area Start -->
    <div id="newsletter-area" class="newsletter-area section pb-120 pt-120 hide">
        <div class="container">

            <div class="row">

                <div class="newsletter-section-title text-center col-xs-12 mb-45">
                    <h2>Submit <span>NEWSLETTERS</span></h2>
                </div>

            </div>

            <div class="row">

                <div class="col-xs-12 text-center">

                    <form action="#" id="subscribe-form">
                        <input type="email" placeholder="Your email address">
                        <input type="submit" value="Subscribe">
                    </form>
                    <div class="social">
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-rss"></i></a>
                        <a href="#"><i class="fa fa-google-plus"></i></a>
                        <a href="#"><i class="fa fa-pinterest-p"></i></a>
                        <a href="#"><i class="fa fa-instagram"></i></a>
                    </div>
                </div>

            </div>

        </div>
    </div>
    <!-- Newsletter Area End -->

    <!-- Footer Area Start -->
    <div id="footer-area" class="footer-area section">
        <div class="container">

            <div class="row">

                <!-- Copyright -->
                <div class="copyright text-left col-sm-6 col-xs-12">
                    <p>Copyright &copy; toqueeltimbre.com <?=date("Y")?>. All right reserved.</p>
                </div>

                <!-- Author Credit -->
                <div class="author-credit text-right col-sm-6 col-xs-12 hide">
                    <p>Created by <a href="http://devitems.com/">Devitems</a> With <i class="fa fa-heart-o"></i></p>
                </div>

            </div>

        </div>
    </div>
    <!-- Footer Area End -->


</div>
<!-- Body main wrapper end -->

<!-- JS -->

<!-- jQuery JS
============================================ -->
<script src="<?=assets_url("project-template/js/vendor/jquery-1.12.0.min.js")?>"></script>
<!-- Bootstrap JS
============================================ -->
<script src="<?=assets_url("project-template/js/bootstrap.min.js")?>"></script>
<!-- Plugins JS
============================================ -->
<script src="<?=assets_url("project-template/js/plugins.js")?>"></script>
<!-- Ajax Mail JS
============================================ -->
<script src="<?=assets_url("project-template/js/ajax-mail.js")?>"></script>
<!-- Main JS
============================================ -->
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script src="<?=assets_url("js/parsley.min.js")?>"></script>
<script src="<?=assets_url("js/parsley.spanish.js")?>"></script>
<script type="text/javascript" src="<?=assets_url("backend/global/plugins/bootbox.min.js")?>"></script>
<script type="text/javascript" src="<?=assets_url("backend/global/plugins/handlebars-v4.0.10.js")?>"></script>
<script type="text/javascript" src="<?=assets_url("js/handlerbars.custom.helpers.js")?>"></script>
<script type="text/javascript" src="<?=assets_url("backend/js/facebook.login.js")?>"></script>
<script type="text/javascript" src="<?=assets_url("backend/js/quick-login.js")?>"></script>
<script src="<?=assets_url("project-template/js/main.js")?>"></script>
<script src="https://maps.google.com/maps/api/js?key=AIzaSyArzfSdkJxIlplT_5VpgGNzeb4lj9aGcCc"></script>
<script src="<?=assets_url('backend/global/plugins/gmaps0425/gmaps.min.js')?>"></script>
<script src="<?=assets_url("js/owlcarousel/owl.carousel.js")?>"></script>
<script src="<?=assets_url("backend/js/MapHandler.js")?>"></script>
<script src="<?=assets_url("project-template/custom-scripts.js")?>"></script>
</body>

</html>
<?php
function showContent($tabList, $contentId)
{
    $key = array_search($contentId, array_column($tabList, 'href'));

    $showContent = $tabList[$key]["show"] == "Si"?"":" hide ";
    return $showContent;
}
$this->load->view("handlebar-template/quick-login-form");
$this->load->view('handlebar-template/welcome-to-tet-login-facebook');
$this->load->view('handlebar-template/project-contact-message-modal-form');
?>
