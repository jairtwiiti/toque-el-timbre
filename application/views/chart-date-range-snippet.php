<div class="col-md-4">
    <div class="form-group">
        <?php
        $startDate = "01-".date('m')."-".date('Y');
        ?>
        <label data-field="start-date">Desde (dia-mes-año)</label>
        <input type="text" name="start-date" data-date-format="dd-mm-yyyy" parsley-trigger="change" placeholder="dia-mes-año" readonly class="form-control date-picker" value="<?=set_value('start-date',$startDate)?>">
    </div>
</div>
<div class="col-md-4">
    <div class="form-group">
        <?php
        $endDate = date("t-m-Y", strtotime($startDate));
        ?>
        <label data-field="end-date">Hasta (dia-mes-año)</label>
        <input type="text" name="end-date" data-date-format="dd-mm-yyyy" parsley-trigger="change" placeholder="dia-mes-año" readonly class="form-control date-picker" value="<?=set_value('end-date',$endDate)?>">
    </div>
</div>