<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('translate_encode_url')){
    function translate_encode_url($filters) {
        $translate = array(
            "search" => "criterio",
            "type" => "categoria",
            "in" => "tipo",
            "city" => "ciudad",
            "room" => "habitacion",
            "bathroom" => "banio",
            "parking" => "parqueo",
            "currency" => "moneda",
            "price_min" => "desde",
            "price_max" => "hasta",
            "sort" => "ordenar"
        );

        foreach($filters as $key => $filter) {
            if($key != "type" && $key != "in" && $key != "city") {
                $aux_filters[$translate[$key]] = $filter;
            }
        }
        return $aux_filters;
    }
}


if (!function_exists('translate_decode_url')){
    function translate_decode_url($filters) {
        $translate = array(
            "criterio"  => "search",
            "categoria" => "type",
            "tipo" => "in",
            "ciudad" => "city",
            "habitacion" => "room",
            "banio" => "bathroom",
            "parqueo" => "parking",
            "moneda" => "currency",
            "desde" => "price_min",
            "hasta" => "price_max",
            "ordenar" => "sort"
        );

        foreach($filters as $key => $filter) {
            $aux_filters[$translate[$key]] = $filter;
        }
        return $aux_filters;
    }
}


if (!function_exists('convert_values_url')){
    function convert_values_url($filters) {
        //$filters->in = strtolower($filters->in) == "venta" ? 'comprar' : $filters->in;
        $filters->city = empty($filters->city) ? '' : $filters->city;
        $url_seo .= 'buscar/';
        $url_seo .= !empty($filters->type) ? seo_url($filters->type, false) : '';
        $url_seo .= !empty($filters->in) ? "-en-" . seo_url($filters->in, false) : '';
        $url_seo .= !empty($filters->city) ? "-en-" . seo_url($filters->city, false) : '';

        $config_filter = translate_encode_url($filters);
        if(!empty($config_filter)){
            $config_filter = http_build_query($config_filter);
            $url_seo .= '/?'. $config_filter;
        }
        return $url_seo;
    }
}


if (!function_exists('get_filters_segments')){
    function get_filters_segments($filter) {
        $CI =& get_instance();
        /*$value = $CI->uri->segment(1);
        transform_segment_in_filter($value, $filter);*/

        $value = $CI->uri->segment(2);
        transform_segment_in_filter($value, $filter);
        return $filter;

        /*$value = $CI->uri->segment(3);
        transform_segment_in_filter($value, $filter);
        return $filter;*/
    }
}


if (!function_exists('transform_segment_in_filter')){
    function transform_segment_in_filter($value, &$filter) {

        $value = explode("-en-", $value);
        foreach($value as $obj_value){
            $aux_string = str_replace("-", " ", $obj_value);
            $aux_string = str_replace("?", "", $aux_string);
            $aux_string = trim($aux_string);
            $aux_values[] = $aux_string;
        }

        if(empty($filter)){
            $filter = new stdClass();
        }

        $filter->type = $aux_values[0];
        $filter->in = $aux_values[1];
        $filter->city = $aux_values[2];

        /*if($value == "santa-cruz" || $value == "la-paz" || $value == "cochabamba" || $value == "beni" || $value == "pando" || $value == "chuquisaca" || $value == "tarija" || $value == "oruro" || $value == "potosi"){
            $filter->city = ucwords(str_replace("-", " ", $value));
        }else{
            $city = unserialize($_COOKIE['cookie_city']);
            $city = isset($_COOKIE['cookie_city']) ? $city['city'] : 'Bolivia';
            $filter->city = $city;
        }

        if($value == "comprar" || $value == "alquiler" || $value == "anticretico"){
            $value = str_replace("comprar", 'Venta', $value);
            $value = str_replace("alquiler", 'Alquiler', $value);
            $value = str_replace("anticretico", 'Anticretico', $value);
            $filter->in = $value;
        }

        if($value == "casas" || $value == "departamentos" || $value == "terrenos" || $value == "oficinas" || $value == "locales-comerciales" || $value == "quintas-y-propiedades"){
            $filter->type = ucwords(str_replace("-", " ", $value));
        }*/
    }
}

