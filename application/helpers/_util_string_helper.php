<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('crop_string')){
    function crop_string($frase_entrada,$cortar){ 

    	if (strlen($frase_entrada) > $cortar){ 
			$frase_corta=substr($frase_entrada,0,$cortar); // obtener la frase cortada.
			$palabras=str_word_count(utf8_decode($frase_corta),1,'���������������'); // obtener array con las palabras.
			$total_palabras=count($palabras)-1; // contar total array elementos y restar 1 elementos 
			$palabras=array_splice($palabras,0,$total_palabras); // le quitamos la ultima palabra.
			$frase_salida=implode(' ',$palabras); //  y concatenamos con el espacio hacia una cadena. 
	  
			$frase_salida .= "..."; // se a�aden los puntos suspensivos a la cadena obtenida..
			$frase_salida = utf8_encode($frase_salida); 
		}else{
			$frase_salida=$frase_entrada;
		}
		return $frase_salida; 
    }   
}

if (!function_exists('seo_url')){
    function seo_url($texto, $lower = true){
        $spacer = "-";
        $texto = trim($texto);
        if($lower){
            $texto = strtolower($texto);
        }

        $texto = str_replace("á", "a", $texto);
        $texto = str_replace("é", "e", $texto);
        $texto = str_replace("í", "i", $texto);
        $texto = str_replace("ó", "o", $texto);
        $texto = str_replace("ú", "u", $texto);
        $texto = str_replace("ñ", "n", $texto);

        $texto = trim(ereg_replace("[^ A-Za-z0-9_]", " ", $texto));
        $texto = ereg_replace("[ \t\n\r]+", "-", $texto);
        $texto = str_replace(" ", $spacer, $texto);
        $texto = ereg_replace("[ -]+", "-",$texto);
        return $texto;
    }
}

if (!function_exists('check_favorite_view')) {
    function check_favorite_view($id_user, $id_inmueble){
        $CI = get_instance();
        $CI->load->library('session');

        if (!empty($id_user)) {
            $CI->load->model('model_favorito');
            $num = $CI->model_favorito->get_num_favorite($id_user, $id_inmueble);
            if ($num > 0) {
                return true;
            } else {
                return false;
            }
        } else {
            $favorite = $CI->session->userdata('favorite');

            if (!empty($favorite)) {
                $favorite = array_values($favorite);
                if (in_array($id_inmueble, $favorite)) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }
}

if (!function_exists('breadcrumb_nav')) {
    function breadcrumb_nav($data){
        $i = 1;
        $html = "";
        foreach($data as $value){
            if($i == 1){
                $html .= '<li><i class="fa fa-home"></i><a href="'.$value['link'].'">'.$value['name'].'</a><i class="fa fa-angle-right"></i></li>';
            }else{
                $html .= '<li><a href="'.$value['link'].'">'.$value['name'].'</a></li>';
            }
            $i++;
        }
        return $html;
    }
}

if (!function_exists('keywords_filter')) {
    function keywords_filter($values){
        $denegate = array("en", "de", "la", "para", "con", "sin", "por");
        $auxiliar = $values;
        foreach($values as $key=>$val){
            if(in_array($val, $denegate)){
                unset($auxiliar[$key]);
            }
        }
        return $auxiliar;
    }
}