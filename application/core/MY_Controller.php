<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Base_page extends CI_Controller {

    /*
      |--------------------------------------------------------------------------
      | Constantes
      |--------------------------------------------------------------------------
     */

    public $geolocation = "";
    public $user_id = "";
    public $user = "";
    public $expire_cookie;
    protected  $_ci;
    protected $_amenityList;

    /*
      |--------------------------------------------------------------------------
      | Atributos
      |--------------------------------------------------------------------------
     */
    private $memory_manager = NULL;

    /*
      |--------------------------------------------------------------------------
      | Métodos
      |--------------------------------------------------------------------------
     */
    /**
     * @var ComplementHandler
     */
    protected $complementHandler;
    /**
     * Constructor
     *
     */
    public function __construct() {
        parent::__construct();
        date_default_timezone_set('America/La_Paz');
        $this->load->helper('url');
        $this->load->helper('util_string');
        $this->load->helper('translate_url');
        $this->load->library('session');
        $this->load->library('user_agent');
        $this->load->helper('cookie');
	    $this->load->library('breadcrumbs');
        $this->load->helper("ssl_helper");
        $this->_evalSslUsage();
        $this->complementHandler = new ComplementHandler();
        $this->expire_cookie = time()+31556926; // 1 año
        $this->_amenityList = array(
            array("id" => 1, "code" => "a", "name" => "Seguridad"),
            array("id" => 2, "code" => "b", "name" => "Churrasquera"),
//            array("id" => 3, "code" => "c", "name" => "Calefaccion"),
            array("id" => 4, "code" => "d", "name" => "Ascensor"),
            array("id" => 5, "code" => "e", "name" => "Area de recreacion"),
            array("id" => 6, "code" => "f", "name" => "Lavanderia"),
            array("id" => 7, "code" => "g", "name" => "Balcon"),
            array("id" => 8, "code" => "h", "name" => "Patio"),
            array("id" => 9, "code" => "i", "name" => "Piscina"),
            array("id" => 10, "code" => "j", "name" => "Amoblado"),
            array("id" => 11, "code" => "k", "name" => "Aire acondicionado"),
            array("id" => 12, "code" => "l", "name" => "Areas Comunes"),
            array("id" => 13, "code" => "m", "name" => "Condominio")
        );

        $aux = unserialize($_COOKIE['cookie_city']);
        if(empty($aux)){
            $this->geolocation = array("city" => "Bolivia");
            setcookie('cookie_city', serialize($this->geolocation), $this->expire_cookie);
        }else{
            $this->geolocation = $aux;
        }

        //$this->user = @$this->session->userdata('logged_user');
        $this->user = !empty($_COOKIE['logged_user']) ? unserialize($_COOKIE['logged_user']) : '';
        $this->user_id = $this->user->usu_id;
        $this->_ci =&get_instance();
    }


    public function get_current_url() {
        $query = $_SERVER['QUERY_STRING'] ? '?' . $_SERVER['QUERY_STRING'] : '';
        return $this->config->site_url() . $this->uri->uri_string() . $query;
    }

    public function get_ci() {
        $CI = &get_instance();
        return $CI;
    }


    public function load_template($view, $title = "", $data = array(), $meta = array()) {
        $this->load->model('model_testimonios');
        $data['title'] = $title;
        $data['view'] = $view;
        $data['mobile'] = $this->agent->is_mobile();
        $data['ipad'] = $this->agent->is_mobile("ipad");
        $data['metatag'] = $meta;
        $data['testimonials'] = $this->model_testimonios->get_all();
        $data['geolocation'] = $this->geolocation['city'];
        $data['user_logged'] = $this->user;
        $data["complementHandler"] = $this->complementHandler;

        if(ENVIRONMENT == "production")
        {
            $this->complementHandler->addPublicJs("livechat");

        }
        $this->complementHandler->addViewComplement("line-height");
        $this->complementHandler->addProjectCss('custom-styles');
        $this->complementHandler->addProjectJs("general-scripts");
        $this->load->view('template/base_template',$data);
    }

    public function loadHomePage($view, $title = "", $data = array(), $meta = array())
    {
        $this->load->model('model_testimonios');

//        $this->complementHandler->addViewComplement("jquery");
//        $this->complementHandler->addViewComplement("tinynav");
//        $this->complementHandler->addViewComplement("superfish");
//        $this->complementHandler->addViewComplement("hover-intent");
//        $this->complementHandler->addViewComplement("hover-intent");
//        $this->complementHandler->addViewComplement("jquery.ui.totop");
//        $this->complementHandler->addViewComplement("owl.carousel");
//        $this->complementHandler->addViewComplement("jquery.content-panel-switcher");
//        $this->complementHandler->addViewComplement("theme-options");
//        $this->complementHandler->addViewComplement("jquery.cookies");
//        $this->complementHandler->addViewComplement("bootstrapjs");
//        $this->complementHandler->addViewComplement("bootstrapjs");
//        $this->complementHandler->addViewComplement("animate");
//        $this->complementHandler->addViewComplement("camera");
//        $this->complementHandler->addViewComplement("jquery.easing");
//        $this->complementHandler->addViewComplement("jquery-ui");
//        $this->complementHandler->addViewComplement("jquery.ui.touch-punch");
//        $this->complementHandler->addViewComplement("jquery.switchButton");
//        $this->complementHandler->addViewComplement("numeral");
//        $this->complementHandler->addViewComplement("jquery.colorbox");
//        $this->complementHandler->addViewComplement("jquery.print");


        $data['title'] = $title;
        $data['view'] = $view;
        $data['mobile'] = $this->agent->is_mobile();
        $data['ipad'] = $this->agent->is_mobile("ipad");
        $data['metatag'] = $meta;
        $data['testimonials'] = $this->model_testimonios->get_all();
        $data['geolocation'] = $this->geolocation['city'];
        $data['user_logged'] = $this->user;
        $data["complementHandler"] = $this->complementHandler;

        if(ENVIRONMENT == "production")
        {
            $this->complementHandler->addPublicJs("livechat");
        }
        $this->load->view('template-home-page/base_template',$data);
    }

    public function loadProjectTemplate($view, $title = "", $data = array(), $meta = array()) {
        $this->load->model('model_testimonios');
        $data['title'] = $title;
        $data['view'] = $view;
        $data['mobile'] = $this->agent->is_mobile();
        $data['ipad'] = $this->agent->is_mobile("ipad");
        $data['metatag'] = $meta;
        $data['testimonials'] = $this->model_testimonios->get_all();
        $data['geolocation'] = $this->geolocation['city'];
        $data['user_logged'] = $this->user;
        $data["complementHandler"] = $this->complementHandler;

        $this->load->view('project-template/index',$data);
    }

    public function load_template_blank($view, $title = "", $data = array()) {
        $data['title'] = $title;
        $data['view'] = $view;
        $data['mobile'] = $this->agent->is_mobile();

        $this->load->view('template/base_template_blank',$data);
    }


    /**
     * Obtiene datos del archivo de configuración
     * @param object $llave
     * @return
     */
    public function config($llave) {
        return $this->config->item($llave);
    }

    public function set_config($llave, $valor) {
        return $this->config->set_item($llave, $valor);
    }

    /**
     * Gets the memory manager for the application
     *
     * @return an instance of Memory_manager.
     */
    public function get_memory_manager() {
        if (Sistema::es_nada($this->memory_manager))
            $this->memory_manager = new Memory_manager();
        return $this->memory_manager;
    }

    /*
      |--------------------------------------------------------------------------
      | Redirecciones especiales
      |--------------------------------------------------------------------------
     */


    /**
     * Redirecciona a la página no encontrada
     * @return void
     */
    public function redirect_404() {
        show_404("Pagina no encontrada");
    }


    /*
      |--------------------------------------------------------------------------
      | Funciones especiales
      |--------------------------------------------------------------------------
     */

    /**
     * Execute a curl php method, for get location data from external url.
     *
     * @param string $url
     * @return array
     */
    private function getExternalUrlData($url = "http://api.hostip.info/get_json.php?position=true") {
        //  Initiate curl
        $ch = curl_init();
        // Disable SSL verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        // Will return the response, if false it print the response
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Set the url
        curl_setopt($ch, CURLOPT_URL, $url);
        // Execute
        $result=curl_exec($ch);
        // Closing
        curl_close($ch);
        // Will dump a beauty json :3
        return json_decode($result, true);
    }

    public function check_favorite($id_user, $id_inmueble){
        $this->load->library('session');

        if(!empty($id_user)){
            $this->load->model('model_favorito');
            $num = $this->model_favorito->get_num_favorite($id_user, $id_inmueble);
            if($num > 0){
                return true;
            }else{
                return false;
            }
        }else{
            $favorite = $this->session->userdata('favorite');

            if(!empty($favorite)){
                $favorite = array_values($favorite);
                if(in_array($id_inmueble, $favorite)){
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }
    }

    public function get_filters_segments($filter){
        $value = $this->uri->segment(2);
        $this->transform_segment_in_filter($value, $filter);

        /*$value = $this->uri->segment(2);
        $this->transform_segment_in_filter($value, $filter);

        $value = $this->uri->segment(3);
        $this->transform_segment_in_filter($value, $filter);*/
        return $filter;
    }

    public function transform_segment_in_filter($value, &$filter){

        $value = explode("-en-", $value);
        foreach($value as $obj_value){
            $aux_string = str_replace("-", " ", $obj_value);
            $aux_string = str_replace("?", "", $aux_string);
            $aux_string = trim($aux_string);
            $aux_values[] = $aux_string;
        }

        if(empty($filter)){
            $filter = new stdClass();
        }

        $filter->type = $aux_values[0];
        $filter->in = $aux_values[1];
        $filter->city = $aux_values[2];

        /*if($value == "santa-cruz" || $value == "la-paz" || $value == "cochabamba" || $value == "beni" || $value == "pando" || $value == "chuquisaca" || $value == "tarija" || $value == "oruro" || $value == "potosi"){
            $filter->city = ucwords(str_replace("-", " ", $value));
        }else{
            $filter->city = $this->geolocation['city'];
        }

        if($value == "comprar" || $value == "alquiler" || $value == "anticretico"){
            $value = str_replace("comprar", 'Venta', $value);
            $value = str_replace("alquiler", 'Alquiler', $value);
            $value = str_replace("anticretico", 'Anticretico', $value);
            $filter->in = $value;
        }

        if($value == "casas" || $value == "departamentos" || $value == "terrenos" || $value == "oficinas" || $value == "locales-comerciales" || $value == "quintas-y-propiedades"){
            $filter->type = ucwords(str_replace("-", " ", $value));
        }*/
    }

    // variable de filtros ya transformada y en json
    public function convert_values_url($filters)
    {
        //$filters->in = strtolower($filters->in) == "venta" ? 'comprar' : $filters->in;
        $filters->city = empty($filters->city) ? '' : $filters->city;
        $url_seo = "/";
        $url_seo .= 'buscar/';
        $url_seo .= !empty($filters->type) ? seo_url($filters->type, false) : '';
        $url_seo .= !empty($filters->in) ? "-en-" . seo_url($filters->in, false) : '';
        $url_seo .= !empty($filters->city) ? "-en-" . seo_url($filters->city, false) : '';

        $config_filter = $this->translate_encode_url($filters);
        if(!empty($config_filter))
        {
            $config_filter = http_build_query($config_filter);
            $url_seo .= '/?'. $config_filter;
        }
        return $url_seo;
    }

    public function translate_encode_url($filters)
    {
        $translate = array(
            "search" => "criterio",
            "type" => "categoria",
            "in" => "tipo",
            "city" => "ciudad",
            "room" => "habitacion",
            "bathroom" => "bano",
            "parking" => "parqueo",
            "currency" => "moneda",
            "price_min" => "desde",
            "price_max" => "hasta",
            "sort" => "ordenar",
            "layout" => "tipo"
        );
        $aux_filters = array();
        foreach($filters as $key => $filter)
        {
            if($key != "type" && $key != "in" && $key != "city")
            {
                $aux_filters[$translate[$key]] = $filter;
            }
        }

        return $aux_filters;
    }

    public function translate_decode_url($filters) {
        $translate = array(
            "criterio"  => "search",
            "categoria" => "type",
            "tipo" => "in",
            "ciudad" => "city",
            "habitacion" => "room",
            "bano" => "bathroom",
            "parqueo" => "parking",
            "moneda" => "currency",
            "desde" => "price_min",
            "hasta" => "price_max",
            "ordenar" => "sort",
            "tipo" => "layout"
        );
        $aux_filters = array();
        foreach($filters as $key => $filter) {
            $aux_filters[$translate[$key]] = $filter;
        }
        return $aux_filters;
    }

    public function registerLogVisitor($visitorType, $visitorTypeId) {

        if ($visitorType == 'Inmueble') {
            $this->load->model('model_inmueble');
            $inmueble = $this->model_inmueble->get_all($visitorTypeId);

            $data = array(
                "inm_visitas" => $inmueble['inm_visitas'] + 1,
            );
            $this->model_inmueble->update($visitorTypeId, $data);

        } elseif ($visitorType == 'Proyecto') {
            $this->load->model('model_proyecto');
            $proyecto = $this->model_proyecto->get_all($visitorTypeId);
            $data = array(
                "proy_visitas" => $proyecto['proy_visitas'] + 1,
            );
            $this->model_proyecto->update($visitorTypeId, $data);

        } elseif ($visitorType == 'Perfil') {
            $this->load->model('visitor');
        }

        $this->load->model('visitor');
        if ($this->visitor->exist_current_date_visitor($visitorType, $visitorTypeId)) {
            $visitor = array_shift($this->visitor->get_current($visitorType, $visitorTypeId));
            $visitorData = array(
                "count" => $visitor->count + 1,
            );
            $this->visitor->update($visitor->id, $visitorData);
        } else {
            $visitorData = array(
                "visitor_date" => date('Y-m-d'),
                "type_id" => $visitorTypeId,
                "type" => $visitorType,
                "count" => 1

            );
            $this->visitor->insert($visitorData);
        }
    }

    public static function getBitlyKeys()
    {
        $genericAccessToken = "6825b82423d7e7a9a82945e77c12c1ab71ef6e82";
        return $genericAccessToken;
    }

    public static function getReCaptchaKeys()
    {
        $arrayKeys = array();
        switch(ENVIRONMENT)
        {
            CASE "production":
                $arrayKeys["publicKey"] = "6LftBykUAAAAAHbUTS-B8Ni25DI7bGfwJ2eXIb5w";
                $arrayKeys["secretKey"] = "6LftBykUAAAAAFmCVHJfK7Sdzb6Cue-jPtj2jk7q";
                break;
            default:
                $arrayKeys["publicKey"] = "6Lf7ICkUAAAAAFsX-ONUz1lSefiUGIAIsBgN39jD";
                $arrayKeys["secretKey"] = "6Lf7ICkUAAAAAP-fFjUWI_JiRXYfCWGxGjBF7ALA";
        }
        return $arrayKeys;
    }

    public static function getAccessTokenApiMarketing()
    {
        switch(ENVIRONMENT)
        {
            CASE "production":
                $accessTokens = "EAAJV0ZCETlqIBAMV0pJhcYKZCTljdQ2jCeGbot1fBZCDzfah70R5iOBmVfXL5kM1JdKtDqDb2zNDZCtX2HqZBYi1GGNeGnGjBo2MMmbZChbMg6CWeZAoXx517yj64rAauC32aYMZAApN07ipf3oGlCgeNlZApKH2awnYKDSwdMBBt1gZDZD";
                $accessTokens = "EAAJV0ZCETlqIBAN0ijyaR209as5jZATX9S029IAulExOKjAq1TPBEMSF9xZCkLYwClFsBrZBisa6nY88iTiowPkpWQjasHgwTK0vgKSLazr1VZA08O9Li13XYLBXWPfKi1F99pqM9MtSCj189GWrrs5CYE2nvlGihLx6IO9fGZAgZDZD";
                break;
            default:
                $accessTokens = "EAABqFSujosgBADkAhBaUdlgTVtQZC4dY298U22TYPZAZCRgGvZCSm66ODMUxvhk5whdZCZAKGJtIEEBKKVvxcFS4CzJZBJ52t0AOZAa8ZA3IebSnMEDhZCfUphoMTmxAHAJRzByOzyAxqnoI0GlbQBcnubR35YHWiS5oXSacZBywOzaVAZDZD";
                $accessTokens = "EAAJV0ZCETlqIBAN0ijyaR209as5jZATX9S029IAulExOKjAq1TPBEMSF9xZCkLYwClFsBrZBisa6nY88iTiowPkpWQjasHgwTK0vgKSLazr1VZA08O9Li13XYLBXWPfKi1F99pqM9MtSCj189GWrrs5CYE2nvlGihLx6IO9fGZAgZDZD";
        }
        return $accessTokens;
    }

    public static function validateToken($token)
    {
        $ci=&get_instance();
        $curl = curl_init();
        $reCaptchaKeys = static::getReCaptchaKeys();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://www.google.com/recaptcha/api/siteverify",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "secret=".$reCaptchaKeys["secretKey"]."&response=".$token."&remoteip=".$ci->input->ip_address(),
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded"
            ),
        ));

        $response = curl_exec($curl);
        $response = json_decode($response);
        return $response;
    }

    protected function _evalSslUsage()
    {
        if(ENVIRONMENT == "production" || ENVIRONMENT == "testing")
        {
            force_ssl();
        }
    }

    protected function _isAdmin()
    {
        $response = FALSE;
        if($this->user->usu_tipo == "Admin")
        {
            $response = TRUE;
        }
        return $response;
    }

    protected function _validateAccess()
    {
        if(!$this->_isAdmin())
        {
            redirect(base_url("dashboard"));
        }
    }

    public static function getFacebookAppIdAndSecretKey()
    {
        switch(ENVIRONMENT)
        {
            CASE "production":
                $facebookConfig["appId"] = "657318724474530";
                $facebookConfig["secretKey"] = "6023a2908dc9053c6508306f5acab21c";
                break;
            CASE "testing":
                $facebookConfig["appId"] = "370287410268541";
                $facebookConfig["secretKey"] = "80d4491dc56b51c9b794714507250359";
                break;
            default:
                $facebookConfig["appId"] = "116639159001800";
                $facebookConfig["secretKey"] = "3ccf855111c016be13a0b6716a703b04";
        }
        return $facebookConfig;
    }
    public static function getCookieDomain()
    {
        switch(ENVIRONMENT)
        {
            CASE "production":
                $cookieDomain = 'toqueeltimbre.com';
                break;
            default:
                $cookieDomain = $_SERVER['HTTP_HOST'];
        }
        return $cookieDomain;
    }

    protected function _validateObjectToEdit($parameter, $class, $onFailRedirectTo)
    {
        if($this->input->is_ajax_request())
        {
            if(!is_numeric($parameter))
            {
                $response["success"] = 0;
                $response["message"] = "Wrong request.";
                echo json_encode($response);exit;
            }
            $object = $class::getById($parameter);

            if(!$object instanceof $class)
            {
                $response["success"] = 0;
                $response["message"] = "The object doesn't exist.";
                echo json_encode($response);exit;
            }

            return $object;
        }
        else
        {
            if(!is_numeric($parameter))
            {
                $this->session->set_flashdata("errorMessage", "Parametro incorrecto.");
                redirect(base_url($onFailRedirectTo));
            }
            $object = $class::getById($parameter);

            if(!$object instanceof $class)
            {
                $this->session->set_flashdata("errorMessage", "El objeto no existe.");
                redirect(base_url($onFailRedirectTo));
            }

            return $object;
        }
    }

    public function loadView($viewFile, $contentData = array(), $returnAsData = FALSE)
    {
        if($returnAsData)
        {
            return $this->load->view($viewFile, $contentData,$returnAsData);
        }
        else
        {
            $this->load->view($viewFile, $contentData);
        }
    }
}


class Private_Controller extends Base_page {

    public $breadcrumb;
    public $page;
    public $page_title;
    public $sw_subscription;
    public $sw_certification;
    public $limit_publications = 200;

    function __construct() {
        parent::__construct();

        $this->load->helper("url");
        $this->load->helper("form");
        $this->load->helper("cookie");
        $this->load->helper("util_string");
        $this->load->library('form_validation');
        $this->load->model('model_mensaje');
        $this->load->model('model_suscripcion');
        $this->load->model('model_usuario');
        $this->load->model('model_proyecto');

        $this->breadcrumb['level1'] = array('name' => 'Home', 'link' => base_url().'dashboard');
        $url = $this->uri->segment(1);

        if(empty($this->user) && $url != 'login' && $url != 'logout' && $url != 'registrarse' && $url != 'recuperar-contrasena' && $url != 'confirmar' && $url != 'cambiar-contrasena'){
            redirect('login');
        }else{
            $this->sw_subscription = $this->model_suscripcion->has_subscription($this->user_id);
            $this->sw_certification = $this->model_usuario->has_certification($this->user_id);
        }

        if($this->user_id == 10053){
            $this->limit_publications = 200;
        }
    }

    public function load_template_backend($view, $data = array(), $title = 'Panel de Administracion'){
        $user_info = $this->model_usuario->get_info_agent($this->user->usu_id);
        #Add necessary files if the user doesn't have connected his account with facebook
        if(!isset($this->user->usu_facebookid) || $this->user->usu_facebookid == "")
        {
            $this->complementHandler->addProjectCss('facebook.connect-with');
            $this->complementHandler->addProjectJs('facebook.connect-with');
            $facebookAppIdAndSecretKey = Base_page::getFacebookAppIdAndSecretKey();
            $data["facebookAppIdAndSecretKey"] = $facebookAppIdAndSecretKey;
        }
        $this->complementHandler->addViewComplement("jquery.blockui");
        $this->complementHandler->addViewComplement("bootbox");
        $this->complementHandler->addViewComplement("handlebars");
        $this->complementHandler->addProjectCss('custom-styles');
        $this->complementHandler->addPublicJs('handlerbars.custom.helpers');
        $this->complementHandler->addProjectJs('general-scripts');

        #set special js files and plugins to the admin
        $this->_setSpecialFilesToAdmin($user_info);

        $data['title'] = $title;
        $data['view'] = $view;
        $data['mobile'] = $this->agent->is_mobile();
        $data['user'] = $user_info;
        $data['subscription'] = $this->sw_subscription;
        $data['certification'] = $this->sw_certification;
        $data['page'] = $this->page;
        $data['page_title'] = $this->page_title;
        $data['breadcrumb'] = breadcrumb_nav($this->breadcrumb);
        $data["complementHandler"] = $this->complementHandler;
        /* MESSAGES */
        $num_projects = $this->model_proyecto->get_num_projects_by_user($this->user_id);
        if($num_projects > 0){
            $data['num_messages'] = $this->model_mensaje->get_num_messages_unread_project($this->user_id);
            $data['messages'] = $this->model_mensaje->get_messages_top_menu_project($this->user_id);
            $data['messages_project'] = true;
        }else{
            $data['num_messages'] = $this->model_mensaje->get_num_messages_unread($this->user_id);
            $data['messages'] = $this->model_mensaje->get_messages_top_menu($this->user_id);
            $data['messages_project'] = false;
        }
        $this->load->view('template_backend/base_template',$data);
    }

    private function _setSpecialFilesToAdmin($user)
    {
        if($user->usu_tipo == "Admin")
        {
            //custom scripts and complements

            $this->complementHandler->addProjectJs("globalAjaxEventHandlers", TRUE);
        }
    }

    public function loadAdminTemplate($view, $data = array(), $title = 'Admin')
    {
        $user = $this->model_usuario->get_info_agent($this->user->usu_id);
        $data['title'] = $title;
        $data['view'] = $view;
        $data['mobile'] = $this->agent->is_mobile();
        $data['user'] = $user;
        $data['subscription'] = $this->sw_subscription;
        $data['certification'] = $this->sw_certification;
        $data['page'] = $this->page;
        $data['page_title'] = $this->page_title;
        $data['breadcrumb'] = breadcrumb_nav($this->breadcrumb);
        /* MESSAGES */
        $num_projects = $this->model_proyecto->get_num_projects_by_user($this->user_id);
        if($num_projects > 0){
            $data['num_messages'] = $this->model_mensaje->get_num_messages_unread_project($this->user_id);
            $data['messages'] = $this->model_mensaje->get_messages_top_menu_project($this->user_id);
            $data['messages_project'] = true;
        }else{
            $data['num_messages'] = $this->model_mensaje->get_num_messages_unread($this->user_id);
            $data['messages'] = $this->model_mensaje->get_messages_top_menu($this->user_id);
            $data['messages_project'] = false;
        }
        $this->load->view('template_backend/base_template',$data);
    }

    public function codigoAlfanumerico($length=10,$uc=FALSE,$n=TRUE,$sc=FALSE,$min=FALSE) {
        $source = '';
        if($min==1) $source .= 'abcdefghijklmnopqrstuvwxyz';;
        if($uc==1) $source .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        if($n==1) $source .= '1234567890';
        if($sc==1) $source .= '|@#~$%()=^*+[]{}-_';
        if($length>0){
            $rstr = "";
            $source = str_split($source,1);
            for($i=1; $i<=$length; $i++){
                mt_srand((double)microtime() * 1000000);
                $num = mt_rand(1,count($source));
                $rstr .= $source[$num-1];
            }
        }
        return "217".$rstr;
    }

    public static function alphaNumericCode($length=10,$uc=FALSE,$n=TRUE,$sc=FALSE,$min=FALSE) {
        $source = '';
        if($min==1) $source .= 'abcdefghijklmnopqrstuvwxyz';;
        if($uc==1) $source .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        if($n==1) $source .= '1234567890';
        if($sc==1) $source .= '|@#~$%()=^*+[]{}-_';
        if($length>0){
            $rstr = "";
            $source = str_split($source,1);
            for($i=1; $i<=$length; $i++){
                mt_srand((double)microtime() * 1000000);
                $num = mt_rand(1,count($source));
                $rstr .= $source[$num-1];
            }
        }
        return "217".$rstr;
    }

    public static function getEmail($emailType = "info")
    {
        switch(ENVIRONMENT)
        {
            CASE "production":
                $emails = array(
                                "info"=>"info@toqueeltimbre.com",
                                "pagos"=>"pagos@toqueeltimbre.com",
                                "proyectos"=>"proyectos@toqueeltimbre.com",
                                "carla" => "carla@toqueeltimbre.com",
                                "alejandro" => "alejandro@toqueeltimbre.com"
                                );
                break;
            default:
                $emails = array(
                            "info"=>"jcussy@mailinator.com",
                            "pagos"=>"jcussy@mailinator.com",
                            "proyectos"=>"jcussy@mailinator.com",
                            "carla" => "carla@mailinator.com",
                            "alejandro" => "alejandro@mailinator.com");
        }
        return $emails[$emailType];
    }
}