<?php

/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 24/7/2017
 * Time: 11:59 AM
 */
class MY_Model extends CI_Model
{
    const TABLE_NAME = "";
    const TABLE_ID = "";

    /** Messages response - begin */
    const MESSAGE_RESP_ERROR = 0;
    const MESSAGE_RESP_SUCCESS = 1;
    const MESSAGE_RESP_LOGIN_TO_SEND = 2;
    const MESSAGE_RESP_RECAPTCHA_ERROR = 3;
    /** Messages response - end */

    protected $_id;
    protected $_deleted;
    protected $_createdOn;
    protected $_createdBy;
    protected $_editedOn;
    protected $_editedBy;

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/La_Paz');
        $this->_id = NULL;
        $this->_deleted = 0;
        $this->_createdOn = NULL;
        $this->_createdBy = NULL;
        $this->_editedOn = NULL;
        $this->_editedBy = NULL;
    }

    /**
     * @return array
     */
    public static function getAll($limit, $offset, $orderBy = null, $orderType = 'asc')
    {
        $ci=&get_instance();
        $ci->load->database();
        $sql = "select * from ".static::TABLE_NAME;
        $query = $ci->db->query($sql);
        $response = static::recastArray(get_called_class(),$query->result());
        return $response;
    }
    /**
     * Get a object
     *
     * @param int $id
     * @return object
     */
    public static function getById($id)
    {
        $ci=&get_instance();
        $ci->load->database();
        $sql = "
            select * from ".static::TABLE_NAME." where ".static::TABLE_ID." = ".$ci->db->escape($id)."
        ";
        $query = $ci->db->query($sql);
        $response = static::recast(get_called_class(),$query->row());
        return $response;
    }

    /**
     * Get a object
     *
     * @param int $id
     * @return object
     */
    public static function getById2($id)
    {
        $ci=&get_instance();
        $ci->load->database();
        $sql = "
            select * from ".static::TABLE_NAME." where ".static::TABLE_ID." = ".$ci->db->escape($id)."
        ";
        $query = $ci->db->query($sql);
        $response = $query->row();
        return $response;
    }
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->_id;
    }
    public function __toString()
    {
        $jsonEncode = json_encode($this->toArray());
        return $jsonEncode;
    }

    public function toArray()
    {
        return array();
    }

    protected static function recastArray($className, $array)
    {
        $resultArray = array();
        foreach ($array as $stdObject)
        {
            $classObject = static::recast($className, $stdObject);
            $resultArray[$classObject->getId()] = $classObject;
        }
        return $resultArray;
    }

    public function save()
    {
        $ci=&get_instance();
        $ci->load->library("session");
        date_default_timezone_set('America/La_Paz');
        $now = new DateTime();
        if ( $this->getId() !== null && $this->getId() !== "" )
        {
            $this->_editedOn = $now->format("Y-m-d H:i:s" );
            $ci->db->update( static::TABLE_NAME, $this->toArray(), array( static::TABLE_ID => $this->getId()));
        }
        else
        {
            $this->_createdOn = $now->format( "Y-m-d H:i:s" );
            $result = $ci->db->insert( static::TABLE_NAME, $this->toArray() );
            if ( $result === true )
            {
                $this->_id = $ci->db->insert_id();
            }
        }
        return $this;
    }

    public function delete($makePhysicalDelete = FALSE)
    {
        if (!$makePhysicalDelete)
        {
            // Delete logically the row. Change the state.
            //return $this->db->update(static::TABLE_NAME, array(static::TABLE_ID . static::ATTRIB_SUFIX => static::DELETE_STATE), array(static::ID_FIELD => $this->getId()));
        }
        else
        {
            //Delete fisically the row.
            return $this->db->delete(static::TABLE_NAME, array(static::TABLE_ID => $this->getId()));
        }
    }

    public static function sendContactMessageResponse($view, $data, $subject = "Respuesta Contacto Inmueble")
    {
        $ci = &get_instance();
        $sendMessageResponse = array("response" => static::MESSAGE_RESP_ERROR,"message" => '<div class="alert alert-danger">Su mensaje no pudo ser enviado.</div>');
        $calledClass = get_called_class();
        $userSender = model_user::getCurrentUserLoggedIn();
        $formData = $ci->input->post();
        $userReceiver = $data['user'];
        $data['state'] = isset($formData['ciudad'])?$formData['ciudad']:"";
        $data['comment'] = nl2br($formData['mensaje']);
        $data['userSender'] = $userSender->toArray();
        $emailHandler = new EmailHandler();
        $email = $emailHandler->initialize();
        $email->from(EmailHandler::getSender(), 'ToqueElTimbre');
        $email->to($userReceiver['usu_email']);
        $email->subject($subject);
        $email->message($ci->load->view($view,$data, true));
        try
        {
            if($email->Send())
            {
                /** Create the message after send the email */
                $calledClass::createMessage($userSender, $userReceiver['usu_id']);
                $sendMessageResponse['response'] = static::MESSAGE_RESP_SUCCESS;
                $sendMessageResponse['message'] = '<div class="alert alert-success">Su mensaje fue enviado correctamente</div>';
            }
        }
        catch (Exception $e)
        {
            $sendMessageResponse['response'] = static::MESSAGE_RESP_ERROR;
            $sendMessageResponse['message'] = '<div class="alert alert-success">'.$e->getMessage().'</div>';
        }
        return $sendMessageResponse;
    }

    public static function sendContactMessage($view, $data, $subject = "Contacto Inmueble")
    {
        $ci = &get_instance();
        $calledClass = get_called_class();
        $sendMessageResponse = array("response" => static::MESSAGE_RESP_ERROR,"message" => '<div class="alert alert-danger">Su mensaje no pudo ser enviado.</div>');
        $validationResponse = base_page::validateToken($ci->input->post("g-recaptcha-response"));
        if($validationResponse->success)
        {
            $userSender = static::getUserSender();
            if($userSender instanceof model_user)
            {
                $formData = $ci->input->post();
                $userReceiver = $data['user'];
                $data['state'] = isset($formData['ciudad'])?$formData['ciudad']:"";
                $data['comment'] = nl2br($formData['mensaje']);
                $data['userSender'] = $userSender->toArray();
                $emailHandler = new EmailHandler();
                $email = $emailHandler->initialize();
                $email->from(EmailHandler::getSender(), 'ToqueElTimbre');
                $email->to($userReceiver['usu_email']);
                $email->subject($subject);
                $email->message($ci->load->view($view,$data, true));
                try
                {
                    if($email->Send())
                    {
                        /** Create the message after send the email */
                        $calledClass::createMessage($userSender, $userReceiver['usu_id']);
                        $userSender->autoLogin();
                        $sendMessageResponse['response'] = static::MESSAGE_RESP_SUCCESS;
                        $sendMessageResponse['message'] = '<div class="alert alert-success">Su mensaje fue enviado correctamente</div>';
                        $user['cellPhone'] =  $userReceiver["usu_celular"];
                        $user['comment'] = $formData['mensaje'];
                        $user['project'] =  isset($data["project"])?static::_generateShortUrl(1,$data["project"]):array();
                        $user['property'] =  isset($data["property"])?static::_generateShortUrl(2,$data["property"]):array();
                        $sendMessageResponse["user"] = $user;
                    }
                }
                catch (Exception $e)
                {
                    $sendMessageResponse['response'] = static::MESSAGE_RESP_ERROR;
                    $sendMessageResponse['message'] = '<div class="alert alert-success">'.$e->getMessage().'</div>';
                }
            }
            else
            {
                $sendMessageResponse['response'] = static::MESSAGE_RESP_LOGIN_TO_SEND;
                $sendMessageResponse['message'] = '<div class="alert alert-success">Necesita iniciar sesion para enviar su mensaje</div>';
            }
        }
        else
        {
            $sendMessageResponse['response'] = static::MESSAGE_RESP_RECAPTCHA_ERROR;
            $sendMessageResponse['message'] = '<div class="alert alert-success">Lo sentimos, no pudimos validar su mensaje, intente de nuevo por favor</div>';
        }
        return $sendMessageResponse;
    }

    public static function getUserSender()
    {
        $ci = &get_instance();
        $formData = $ci->input->post();
        $response = NULL;
        $currentUser = model_user::getCurrentUserLoggedIn();
        if($currentUser instanceof model_user)
        {
            $response = $currentUser;
        }
        else
        {
            /** If there isn't a started session then let's make sure the user has the email field on contact form */
            if(isset($formData['email']) && $formData['email'] != "")
            {
                $user = model_user::getByEmail($formData['email']);
                /** If the user not exist then let's create it*/
                if(!$user instanceof model_user)
                {
                    $response = model_user::createUser('Buscador');
                }
            }
        }
        return $response;
    }

    private static function _generateShortUrl($publicationType, $publication)
    {
        $ci = &get_instance();
        $ci->load->database();
        $tableId = Model_project::TABLE_ID;
        $tableName = Model_project::TABLE_NAME;
        $seoField = "proy_short_seo";
        $longUrl = base_url($publication["proy_seo"]);
        if($publicationType == 2)
        {
            $tableId = Model_property::TABLE_ID;
            $tableName = Model_property::TABLE_NAME;
            $seoField = "inm_short_seo";
            $longUrl = base_url("buscar/inmuebles-en-bolivia/".$publication["inm_seo"]);
        }
        if($publication[$seoField] == "" || is_null($publication[$seoField]))
        {
            include_once(FCPATH."/application/libraries/BitlyPHP-master/bitly.php");
            $params = array();
            $params['access_token'] = '6825b82423d7e7a9a82945e77c12c1ab71ef6e82';
            $params['longUrl'] = $longUrl;
            $params['domain'] = 'bit.ly';
            $results = bitly_get('shorten', $params);
            if(count($results) > 0 && $results["status_code"] == 200 && $results["status_txt"] == "OK")
            {
                $publication[$seoField] = $results["data"]["hash"];
                $ci->db->where($tableId, $publication[$tableId]);
                $ci->db->update($tableName, $publication);
            }
        }
        return $publication;
    }

    public static function getAllInIdsArray(array $idsArray, $limit, $offset, $orderBy = null, $orderType = 'asc')
    {
        $resultArray = array();
        if (count($idsArray) > 0)
        {
            if ($orderBy === null)
            {
                $orderBy = static::TABLE_ID;
            }
            $ci = &get_instance();
            $ci->load->database();

            $sql = 'select * from ' . static::TABLE_NAME . ' where ' . static::TABLE_ID . ' in ( ';

            foreach ($idsArray as $id)
            {
                $sql .= $ci->db->escape($id);
                if ($id !== end($idsArray))
                {
                    $sql .= ", ";
                }
            }

            $sql .= ') order by ' . $orderBy . ' ' . $orderType . ' limit ' . $limit . ' offset ' . $offset;

            $query = $ci->db->query($sql);
            $resultArray = static::recastArray(get_called_class(), $query->result());
        }
        return $resultArray;
    }

    public static function updateBatch($list = array())
    {
        $ci=&get_instance();
        $ci->load->database();
        $ci->db->update_batch(static::TABLE_NAME, $list, static::TABLE_ID);
    }

    public static function insertBatch($list = array())
    {
        $ci=&get_instance();
        $ci->load->database();
        $ci->db->insert_batch(static::TABLE_NAME, $list);
    }
}