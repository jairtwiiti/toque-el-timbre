<?php
require_once FCPATH . '/application/libraries/facebook-php-ads-sdk/vendor/autoload.php';
use FacebookAds\Api;
use FacebookAds\Object\Lead;
use FacebookAds\Object\LeadgenForm;

class AdManagement
{
    const MESSAGE_RESP_ERROR = 0;
    const MESSAGE_RESP_SUCCESS = 1;

	private $_accessToken;
	private $_keys;
	private $_leadgenFormId;
	private $_leadgenId;
	private $_leadgenNotifyContent;
	private $_leadgenDataToSave;
	private $_ci;
	public function __construct(array $content = array())
	{
	    $this->_ci = &get_instance();
        $this->_ci->load->model("model_user");
        $this->_ci->load->model("model_project");
        $this->_ci->load->model("model_property");
	    $this->_ci->load->model("model_facebook_lead_form");
	    $this->_ci->load->model("model_facebook_lead");

        $this->_accessToken = Base_page::getAccessTokenApiMarketing();
        $this->_keys = Base_page::getFacebookAppIdAndSecretKey();
        Api::init(
            $this->_keys["appId"], // App ID
            $this->_keys["secretKey"],
            $this->_accessToken // Your user access token
        );
        $this->_leadgenNotifyContent = $content;
        $this->_leadgenDataToSave = array(
            "consulta" => "",
            "email" => "",
            "full_name" => "",
            "first_name" => "",
            "last_name" => "",
            "phone_number" => "",
            "city" => "",
            "province" => "",
            "post_code" => "",
            "street_address" => "",
            "country" => "",
            "date_of_birth" => "",
            "marital_status" => "",
            "military_status" => "",
            "gender" => "",
            "relationship_status" => "",
            "job_title" => "",
            "work_email" => "",
            "work_phone_number" => "",
            "company_name" => ""
        );
	}

    /**
     * Save the leadgen and leadgen form on tet system
     */
	public function saveLeadgenOnSystem()
    {
        if(count($this->_leadgenNotifyContent) > 0)
        {
            $fbLeadgenForm = $this->_saveLeadgenForm();
            $this->_saveLeadgen($fbLeadgenForm);
        }
    }

    /**
     * Update the leadgen form using the leadgenformid tha we got on webhook entry, if the leadgenform doesn't exist then
     * let's create it
     * @return model_facebook_lead_form
     */
    private function _saveLeadgenForm()
    {
        //get leadgenformid from webhook entry data
        $this->_leadgenFormId = $this->_leadgenNotifyContent["entry"][0]["changes"][0]["value"]["form_id"];
        //search data through facebook api
        $form = new LeadgenForm($this->_leadgenFormId);
        $leadgenFormData = $form->read()->getData();
        //search the leadgen form on tet system
        $fbLeadgenForm = model_facebook_lead_form::getByFbFormId($this->_leadgenFormId);

        //let's verify if the form already exist on tet system
        if($fbLeadgenForm instanceof model_facebook_lead_form)
        {
            $fbLeadgenForm->setLeadgenExportCsvUrl($leadgenFormData["leadgen_export_csv_url"]);
            $fbLeadgenForm->setName($leadgenFormData["name"]);
            $fbLeadgenForm->setStatus($leadgenFormData["status"]);
            $fbLeadgenForm->save();
        }
        //if the leadgen form doest'n exit then let's create it without project id
        else
        {
            $fbLeadgenForm = new model_facebook_lead_form(
                NULL,
                NULL,
                $leadgenFormData["id"],
                $leadgenFormData["leadgen_export_csv_url"],
                $leadgenFormData["name"],
                $leadgenFormData["status"],
                serialize($leadgenFormData)
            );
        }
        //Once created or update de leadgen form let's save it
        $fbLeadgenForm->save();
        return $fbLeadgenForm;
    }

    /**
     * Save the leadgen entry got form webhook
     * @param $fbLeadgenForm model_facebook_lead_form object
     * @return model_facebook_lead $fbLeadgenForm
     */
    private function _saveLeadgen(model_facebook_lead_form $fbLeadgenForm)
    {
        $this->_leadgenFormId = $this->_leadgenNotifyContent["entry"][0]["changes"][0]["value"]["leadgen_id"];
        $lead = new Lead($this->_leadgenFormId);
        $leadFields = $lead->read()->getData()["field_data"];
        $this->_leadgenPrepareDataToSave($leadFields);
        $fbLeadgen = new model_facebook_lead(
            $fbLeadgenForm->getId(),
            $this->_leadgenFormId,
            $this->_leadgenDataToSave["consulta"],
            $this->_leadgenDataToSave["email"],
            $this->_leadgenDataToSave["full_name"],
            $this->_leadgenDataToSave["first_name"],
            $this->_leadgenDataToSave["last_name"],
            $this->_leadgenDataToSave["phone_number"],
            $this->_leadgenDataToSave["city"],
            $this->_leadgenDataToSave["province"],
            $this->_leadgenDataToSave["post_code"],
            $this->_leadgenDataToSave["street_address"],
            $this->_leadgenDataToSave["country"],
            $this->_leadgenDataToSave["date_of_birth"],
            $this->_leadgenDataToSave["marital_status"],
            $this->_leadgenDataToSave["military_status"],
            $this->_leadgenDataToSave["gender"],
            $this->_leadgenDataToSave["relationship_status"],
            $this->_leadgenDataToSave["job_title"],
            $this->_leadgenDataToSave["work_email"],
            $this->_leadgenDataToSave["work_phone_number"],
            $this->_leadgenDataToSave["company_name"],
            serialize($leadFields)
            );
        $fbLeadgen->save();
        $fbLeadgen->notifyToUser();
        return $fbLeadgen;
    }

    /**
     * Store all data entry trough webhook in an array that is used to save the leadgen on database
     * @param $leadFields
     */
    private function _leadgenPrepareDataToSave($leadFields)
    {
        foreach ($this->_leadgenDataToSave as $name => $value)
        {
            $valuePosition = array_search($name, array_column($leadFields, 'name'));
            if($valuePosition !== FALSE)
            {
                switch ($name)
                {
                    case "date_of_birth":
                        $date = $leadFields[$valuePosition]["values"][0];
                        $date = str_replace('/', '-', $date);
                        $dateOfBirth =  date('Y-m-d', strtotime($date));
                        $value = $dateOfBirth;
                        break;
                    default:
                        $value = $leadFields[$valuePosition]["values"][0];
                        break;
                }
                $this->_leadgenDataToSave[$name] = $value;
            }
        }
    }
}