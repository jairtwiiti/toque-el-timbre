<?php

class ComplementHandler
{
    private $_viewJavascript;
    private $_viewCss;
    private $_complements;
    private $_projectJsPath;
    private $_projectCssPath;
    private $_isBaseComplement;

    public function __construct()
    {
        $ci = get_instance();
        $ci->config->load("complements");
        $this->_complements = $ci->config->item("complements");
        $this->_projectJsPath = $ci->config->item("projectJsPath");
        $this->_projectCssPath = $ci->config->item("projectCssPath");
        $this->_publicJsPath = $ci->config->item("publicJsPath");
        $this->_publicCssPath = $ci->config->item("publicCssPath");
        $this->_isBaseComplement = FALSE;
    }

    public function addViewComplement($name)
    {
        if (array_key_exists($name, $this->_complements))
        {
            if (array_key_exists("js", $this->_complements[$name]))
            {
                $this->_viewJavascript[] = $this->_complements[$name]["js"];
            }
            if (array_key_exists("css", $this->_complements[$name]))
            {
                $this->_viewCss[] = $this->_complements[$name]["css"];
            }
        }
    }

    public function addProjectJs($name, $applyVersion = FALSE)
    {
        $version = "";
        if($applyVersion)
        {
            $version = strtotime(date("Y-m-d"));
        }
        $this->_viewJavascript[] = $this->_projectJsPath . "/" . $name . ".js?v=".$version;
    }
    
    public function addPublicJs($name, $applyVersion = FALSE)
    {
        $version = "";
        if($applyVersion)
        {
            $version = strtotime(date("Y-m-d"));
        }
        $this->_viewJavascript[] = $this->_publicJsPath . "/" . $name . ".js?v=".$version;
    }

    public function addProjectCss($name, $applyVersion = FALSE)
    {
        $version = "";
        if($applyVersion)
        {
            $version = strtotime(date("Y-m-d"));
        }
        $this->_viewCss[] = $this->_projectCssPath . "/" . $name . ".css?v=".$version;
    }
    
    public function addPublicCss($name, $applyVersion = FALSE)
    {
        $version = "";
        if($applyVersion)
        {
            $version = strtotime(date("Y-m-d"));
        }
        $this->_viewCss[] = $this->_publicCssPath . "/" . $name . ".css?v=".$version;
    }

    public function printViewJs()
    {
        if(!empty($this->_viewJavascript)){
            foreach ($this->_viewJavascript as $javascript)
            {
                echo "\n";
                echo "<script type=\"text/javascript\" src=\"" . $javascript . "\"></script>";
            }
        }
    }

    public function printViewCss()
    {
        if(!empty($this->_viewCss))
        {
            foreach ($this->_viewCss as $css)
            {
                echo "\n";
                echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"" . $css . "\" />";
            }
        }

    }

    public function baseComplementsStart()
    {

    }

    public function baseComplementsEnd()
    {

    }

}
