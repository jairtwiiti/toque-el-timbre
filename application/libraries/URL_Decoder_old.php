<?php
/**
 *      CODE    PRIMARY_FILTER  PAGINATION  PROPERTY    THANK_YOU_PAGE  ROUTE
 *      1110         1               0           1           0           inmueble/index
 *      1100         1               1           0           0           search/index
 *      0100         0               1           0           0           search/index
 *      1000         1               0           0           0           search/index
 *      0000         0               0           0           0           search/index
 *      1001         1               0           1           1           search/thankYouPage
 *
 * Created by PhpStorm.
 * User: Jair
 * Date: 16/5/2017
 * Time: 3:37 PM
 */
class URL_Decoder_old
{
    private $_param1;
    private $_param2;
    private $_param3;
    private $_advancedSearch;
    private $_filterKeyValueArray;
    private $_primaryFilterArrayData;
    private $_arrayPropertiesType;
    private $_arrayTransactionsType;
    private $_arrayCities;
    private $_arraySearchCode;
    private $_requestCode;
    private $_hasPagination;
    private $_hasPrimaryFilter;
    private $_hasProperty;
    private $_hasThankYouPage;
    private $_ci;

    public function __construct($param1 = "", $param2 = "", $param3 = "", $advancedSearch)
    {
        $this->_param1 = $param1;
        $this->_param2 = $param2;
        $this->_param3 = $param3;
        $this->_advancedSearch = $advancedSearch;
        $this->_filterKeyValueArray = array(
            "criterio"  => "search",
            "categoria" => "type",
            "tipo" => "in",
            "ciudad" => "city",
            "habitacion" => "room",
            "bano" => "bathroom",
            "parqueo" => "parking",
            "moneda" => "currency",
            "desde" => "price_min",
            "hasta" => "price_max",
            "ordenar" => "sort",
            "tipo" => "layout"
        );
        $this->_primaryFilterArrayData = array();
        $this->_arrayPropertiesType = array("Inmuebles", "Casas", "Departamentos", "Oficinas", "Locales Comerciales", "Terrenos", "Quintas Y Propiedades");
        $this->_arrayTransactionsType = array("Venta", "Alquiler", "Anticretico");
        $this->_arrayCities = array("Bolivia", "Santa Cruz", "La Paz", "Cochabamba", "Tarija", "Chuquisaca", "Oruro", "Potosi", "Pando", "Beni");
        $this->_arraySearchCode = array();
        $this->_requestCode = "";
        $this->_hasPrimaryFilter = FALSE;
        $this->_hasPagination = FALSE;
        $this->_hasProperty = FALSE;
        $this->_hasThankYouPage = FALSE;
        $this->_ci=&get_instance();

    }

    private function _defineRequestCode()
    {
        $this->_hasPrimaryFilter();
        $this->_hasPagination();
        $this->_hasProperty();
        $this->_hasThankYouPage();
        $this->_requestCode = (+$this->_hasPrimaryFilter).(+$this->_hasPagination).(+$this->_hasProperty).(+$this->_hasThankYouPage);
    }

    public function callController()
    {
        $this->_defineRequestCode();
        $url = "{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
            //var_dump($url);exit;
        switch($this->_requestCode)
        {
            case "1010":
                //This section prevent change the url
                require_once(FCPATH."application/controllers/inmueble.php");
                $controller = new Inmueble();
                $controller->index($this->_primaryFilterArrayData);
                break;
            case "1100":
            case "0100":
            case "1000":
            case "0000":
                require_once(FCPATH."application/controllers/search.php");
                //$this->_setLastSearch();
                $controller = new Search();
                $controller->index($this->_primaryFilterArrayData);
                break;
            case "1011":
                require_once(FCPATH."application/controllers/search.php");
                $controller = new Search();
                $controller->thankYouPage($this->_primaryFilterArrayData);
                break;
        }
    }

    private function _hasPrimaryFilter()
    {
        if(count($this->_primaryFilterArrayData)==0)
        {
            for($i=0; $i<=count( $this->_arrayPropertiesType); $i++)
            {
                if( $this->_arrayPropertiesType[$i] != "") {
                    for ($j = 0; $j <= count($this->_arrayTransactionsType); $j++)
                    {
                        if ($this->_arrayTransactionsType[$j] != "")
                        {
                            for ($k = 0; $k <= count($this->_arrayCities); $k++)
                            {
                                if ($this->_arrayCities[$k] != "")
                                {
                                    $propertyType = str_replace(" ","-", $this->_arrayPropertiesType[$i]);
                                    $transactionType = str_replace(" ","-",$this->_arrayTransactionsType[$j]);
                                    $city = str_replace(" ","-",$this->_arrayCities[$k]);

                                    $this->_arraySearchCode[] =  array(
                                                                        "propertyType" => $propertyType,
                                                                        "transactionType" => $transactionType,
                                                                        "city" => $city,
                                                                        "primaryFilter" => strtolower($propertyType. "-en-" . $transactionType . "-en-" . $city)
                                                                        );
                                    ;
                                }
                            }
                        }
                    }
                }
            }

            if(!function_exists('array_column'))
            {
                $arrayColumnResponse = $this->_array_column_php52($this->_arraySearchCode, 'primaryFilter');
            }
            else
            {
                $arrayColumnResponse = array_column($this->_arraySearchCode, 'primaryFilter');
            }
            $key = array_search(strtolower($this->_param1), $arrayColumnResponse);
            if(is_numeric($key))
            {
                $this->_hasPrimaryFilter = TRUE;
                $this->_primaryFilterArrayData = $this->_arraySearchCode[$key];
            }
        }
    }

    private function _hasPagination()
    {
        if(strtolower($this->_param1) == "p" || strtolower($this->_param2) == "p")
        {
            $this->_hasPagination = TRUE;
        }
    }

    private function _hasProperty()
    {
        if($this->_param2 != "" && $this->_param2!="$2" && !$this->_hasPagination)
        {
            $this->_hasProperty = TRUE;
        }
    }

    public function getPrimaryFilterArrayData()
    {
        return $this->_primaryFilterArrayData;
    }
    private function _hasThankYouPage()
    {
        if(strtolower($this->_param3) == "thank-you")
        {
            $this->_hasThankYouPage = TRUE;
        }
    }

    private function _array_column_php52(array $input, $columnKey, $indexKey = null)
    {
        $array = array();
        foreach ($input as $value) {
            if ( !array_key_exists($columnKey, $value)) {
                trigger_error("Key \"$columnKey\" does not exist in array");
                return false;
            }
            if (is_null($indexKey)) {
                $array[] = $value[$columnKey];
            }
            else {
                if ( !array_key_exists($indexKey, $value)) {
                    trigger_error("Key \"$indexKey\" does not exist in array");
                    return false;
                }
                if ( ! is_scalar($value[$indexKey])) {
                    trigger_error("Key \"$indexKey\" does not contain scalar value");
                    return false;
                }
                $array[$value[$indexKey]] = $value[$columnKey];
            }
        }
        return $array;
    }
    private function _setLastSearch()
    {
        $url = "{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
            $this->_ci->session->set_userdata(array("urlLastSearch" => $url));
    }
}