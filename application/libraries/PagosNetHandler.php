<?php
//require BASE_DIRECTORY . DS . 'application' . DS .'libraries'.DS. 'tigomoney' . DS . 'lib'. DS .'nusoap.php';
include FCPATH."/application/libraries/nusoap-095/lib/nusoap.php";

class PagosNetHandler
{

    private $_payment;
    private $_businessCode;
    private $_account;
    private $_password;
    private $_webServiceUrl;
    private $_sintesisError;

	public function __construct(model_payment $payment = NULL)
	{
	    $this->_setEnvironmentVariables();
        $this->_account = "wstoqueeltimbre";
        $this->_password = "Wstoque2019";
        $this->_sintesisError = FALSE;
        $this->_payment = $payment;
	}

	private function _setEnvironmentVariables()
    {
        switch (ENVIRONMENT)
        {
            case 'development':
            case 'testing':
                $this->_webServiceUrl = "http://test.sintesis.com.bo/WSApp-war/ComelecWS?WSDL";
                $this->_businessCode = "575";
                break;
            case 'production':
                $this->_webServiceUrl = "https://web.sintesis.com.bo:443/WSApp-war/contract/ComelecWS.wsdl";
                $this->_businessCode = "37";
                break;
            default:
                exit('The application environment is not set correctly.');
        }
    }

    public function cmRegistroPlan()
    {
        try {
            $client = new nusoap_client($this->_webServiceUrl, true);
            $registroPlan = $client->call(
                "registroPlan",
                array(
                    'datos' 	=> $this->_defineDataToSend(),
                    'cuenta' 	=> $this->_account,
                    'password' 	=> $this->_password
                )
            );
            $response = new PagosNetResponse($registroPlan);
//            echo"<pre>";var_dump($registroPlan, $client->responseData);
//            echo '<h2>Debug</h2><pre>' . htmlspecialchars($client->responseData, ENT_QUOTES) . '</pre>';
//            exit;
        }
        catch (Exception $e)
        {
            $response = new PagosNetResponse();
            $response->setMessage($e->getMessage());
        }
        return $response;
    }

    private function _defineDataToSend()
    {
        /**
         * Model payment
         * model
         */
        $expireDate = new DateTime();
        $expireDate->add(new DateInterval('P14D'));
        $expireDate = $expireDate->format('Ymd');
        $expireTime = date('His');
        $paymentDetail = model_payment::prepareArrayPaymentMasterDetail($this->_payment->getId());
//        echo"<pre>";var_dump($this->_payment->getId(), $paymentDetail);exit;
        $serviceDetailList = implode(', ', array_column($paymentDetail["serviceList"],'name'));
        $CMEDetallePlan = array(
            "numeroPago" => "1",
            "montoPago" => $paymentDetail["paymentAmount"],
            "descripcion" => $serviceDetailList,
            "montoCreditoFiscal" => $paymentDetail["paymentAmount"],
            "nombreFactura" => $paymentDetail["paymentName"],
            "nitFactura" => $paymentDetail["paymentNit"]
        );
        $CMEDatosPlan = array(
            "transaccion" => "A",
            "nombreComprador" => $paymentDetail["userFullName"],
            "documentoIdentidadComprador" => $paymentDetail["userCI"],
            "codigoComprador" => $paymentDetail["userId"],
//            "codigoCliente" => $paymentDetail["userId"],
            "fecha" => intval(date("Ymd")),
            "hora" => intval(date("His")),
            "correoElectronico" => $paymentDetail["userEmail"],
            "moneda" => "BS",
            "codigoRecaudacion" => $paymentDetail["paymentId"],
            "descripcionRecaudacion" => "RECAUDACION NRO: ".$paymentDetail["paymentId"],
            "fechaVencimiento" => $expireDate,
            "horaVencimiento" => $expireTime,
            "categoriaProducto" => "1",
            "precedenciaCobro" => "",
            "planillas" => $CMEDetallePlan
        );
//        echo"<pre>";var_dump($paymentDetail, $CMEDatosPlan);exit;
        return $CMEDatosPlan;
    }
}

class PagosNetResponse
{
    private $_gateway;
    private $_isSuccess;
    private $_message;
    private $_transactionId;
    private $_data;

    public function __construct($gateway = FALSE)
    {
        $this->_gateway = $gateway;
        if($this->_gateway === FALSE)
        {
            $this->_isSuccess = $this->_gateway;
            $this->_message = "No es posible conectar con PagosNet";
            $this->_transactionId = NULL;
            $this->_data = array();
        }
        else
        {
            $this->_isSuccess = $this->_gateway["return"]["codigoError"] == 0?TRUE:FALSE;
            $this->_message = $this->_gateway["return"]["descripcionError"];
            $this->_transactionId = $this->_gateway["return"]["idTransaccion"];
            $this->_data = $this->_gateway;
        }
    }

    public function isSuccess()
    {
        return $this->_isSuccess;
    }

    public function getMessage()
    {
        return $this->_message;
    }

    public function setMessage($message)
    {
        $this->_message = $message;
    }

    public function getData()
    {
        return $this->_data;
    }

    public function getTransactionId()
    {
        return $this->_transactionId;
    }

    public function toArray()
    {
        $response = array(
            "success" => $this->_isSuccess,
            "message" => $this->_message,
            "transactionId" => $this->_transactionId,
            "data" => $this->_data
        );
        return $response;
    }

    public function __toString()
    {
        return json_encode($this->toArray());
    }
}