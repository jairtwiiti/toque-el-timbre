<?php
/**
 *      CODE    PRIMARY_FILTER  PAGINATION  PROPERTY    THANK_YOU_PAGE  ROUTE
 *      1110         1               0           1           0           inmueble/index
 *      1100         1               1           0           0           search/index
 *      0100         0               1           0           0           search/index
 *      1000         1               0           0           0           search/index
 *      0000         0               0           0           0           search/index
 *      1001         1               0           1           1           search/thankYouPage
 *
 * Created by PhpStorm.
 * User: Jair
 * Date: 16/5/2017
 * Time: 3:37 PM
 */
class URL_Decoder
{
    private $_urlToDecode;
    private $_arrayPropertiesType;
    private $_arrayTransactionsType;
    private $_arrayCities;
    private $_arrayCurrency;
    private $_arrayStatus;
    private $_arrayExtraParams;
    private $_ci;
    private $_zoneStringArray;

    public function __construct($url = "")
    {
        $this->_urlToDecode = $url;
        $this->_arrayPropertiesType = array("casas" => 1, "departamentos" => 2, "oficinas" => 3, "locales-comerciales" => 4, "terrenos" => 13, "quintas-y-propiedades" => 14);
        $this->_arrayTransactionsType = array("venta" => 1, "alquiler" => 2, "anticretico" => 3);
        $this->_arrayCities = array("santa-cruz" => 1, "la-paz" => 2, "cochabamba" => 3, "tarija" => 4, "chuquisaca" => 5, "oruro" => 6, "potosi" => 7, "pando" => 8, "beni" => 9);
        $this->_arrayCurrency = array("bolivianos" => 1, "dolares" => 2);
        $this->_arrayStatus = array("pre-venta" => model_property::STATUS_PRE_SALE, "a-estrenar" => model_property::STATUS_BRAND_NEW, "buen-estado" => model_property::STATUS_GOOD, "requiere-mantenimiento" => model_property::STATUS_REQUIRE_MAINTENANCE);
        $this->_arrayExtraParams = array();
        $this->_ci=&get_instance();
    }

    public function setZoneStringArray(array $zoneStringArray = array())
    {
        $this->_zoneStringArray = $zoneStringArray;
    }

    public function getMainQueryFilter($projectMode = FALSE)
    {
        $mainQueryFilter = "";
        $mainQueryFilter .= $this->getPrimaryFilter();
        $mainQueryFilter .= $this->getSecondaryFilter();
        $mainQueryFilter .= !$projectMode?$this->getSpecialFeatures():"";
        if($this->getSpecialFeatures() == "" || $projectMode)
        {
            $mainQueryFilter = substr($mainQueryFilter,0, -5);
        }

        return $mainQueryFilter;
    }

    public function getProjectMainFilter()
    {
        $sqlStatement = "";
        $primaryFilter = explode("?",strtolower($this->_urlToDecode));
        $primaryFilter = explode("/",strtolower($primaryFilter[0]));
        $arrayPrimaryFilter = explode("-en-",strtolower($primaryFilter[0]));

        if(isset($this->_arrayPropertiesType[strtolower($arrayPrimaryFilter[0])]))
        {
            $sqlStatement .= " inm_cat_id = ".$this->_arrayPropertiesType[strtolower($arrayPrimaryFilter[0])]." and ";
        }
        $sqlStatement .= " inm_for_id = ".$this->_arrayTransactionsType[strtolower($arrayPrimaryFilter[1])]." and ";
        if(isset($this->_arrayCities[strtolower($arrayPrimaryFilter[2])]))
        {
            $sqlStatement .= " dep_id = ".$this->_arrayCities[strtolower($arrayPrimaryFilter[2])]." and ";
        }

        $arrayExtraParams = $this->_getExtraParams();
        foreach ($arrayExtraParams as $param => $value)
        {
            $param = strtolower($param);
            switch ($param)
            {
                case "moneda":
                    $sqlStatement.= "inm_mon_id = ".$this->_arrayCurrency[strtolower($value)]." and ";
                    break;
                case "desde":
                    $sqlStatement.= "inm_precio >= ".$value." and ";
                    break;
                case "hasta":
                    $sqlStatement.= "inm_precio <= ".$value." and ";
            }
        }

        return $sqlStatement;
    }

    public function getOffset()
    {
        $offsetExist = strpos($this->_urlToDecode,"/p/");
        $offset = 0;
        if($offsetExist !== FALSE)
        {
            $offset = substr($this->_urlToDecode,$offsetExist+3,1);
        }
        return (int)$offset;
    }

    public function getOrderCriteria()
    {
        $orderCriteria = array("orderBy" => "", "orderType" => "");
        $extraParams = $this->_getExtraParams();
        if(isset($extraParams["ordenar"]))
        {
            switch ($extraParams["ordenar"])
            {
                case "date-desc":
                    $orderBy = ", pub_creado ";
                    $orderType = " desc ";
                    break;
                case "date-asc":
                    $orderBy = ", pub_creado ";
                    $orderType = " asc ";
                    break;
                case "price-asc":
                    $orderBy = ", inm_precio ";
                    $orderType = " asc ";
                    break;
                case "price-desc":
                    $orderBy = ", inm_precio ";
                    $orderType = " desc ";
                    break;
                default:
                    $orderBy = "";
                    $orderType = "";

            }
            $orderCriteria["orderBy"] = $orderBy;
            $orderCriteria["orderType"] = $orderType;
        }
        return $orderCriteria;
    }
    public function getSearchTitle()
    {
        $primaryFilter = explode("?",strtolower($this->_urlToDecode));
        $primaryFilter = explode("/",strtolower($primaryFilter[0]));
        $searchTitle = str_replace("-"," ",strtolower($primaryFilter[0]));
        $searchTitle = ucwords($searchTitle);
        return $searchTitle;
    }
    public function getPrimaryFilter()
    {
        $sqlStatement = "";
        $primaryFilter = explode("?",strtolower($this->_urlToDecode));
        $primaryFilter = explode("/",strtolower($primaryFilter[0]));
        $arrayPrimaryFilter = explode("-en-",strtolower($primaryFilter[0]));

        if(isset($this->_arrayPropertiesType[strtolower($arrayPrimaryFilter[0])]))
        {
            $sqlStatement .= " inm_cat_id = ".$this->_arrayPropertiesType[strtolower($arrayPrimaryFilter[0])]." and ";
        }
        $sqlStatement .= " inm_for_id = ".$this->_arrayTransactionsType[strtolower($arrayPrimaryFilter[1])]." and ";
        if(isset($this->_arrayCities[strtolower($arrayPrimaryFilter[2])]))
        {
            $sqlStatement .= " dep_id = ".$this->_arrayCities[strtolower($arrayPrimaryFilter[2])]." and ";
        }

        return $sqlStatement;
    }

    public function getSecondaryFilter()
    {
        $ci = &get_instance();
        $ci->load->database();
        $arrayExtraParams = $this->_getExtraParams();
        $sqlStatement = "";
        foreach ($arrayExtraParams as $param => $value)
        {
            $param = strtolower($param);
            switch ($param)
            {
                case "moneda":
                    $sqlStatement.= "inm_mon_id = ".$this->_arrayCurrency[strtolower($value)]." and ";
                    break;
                case "desde":
                    $value = str_replace(",","",$value);
                    $sqlStatement.= "inm_precio >= ".$ci->db->escape($value)." and ";
                    break;
                case "hasta":
                    $value = str_replace(",","",$value);
                    $sqlStatement.= "inm_precio <= ".$ci->db->escape($value)." and ";
                    break;
                case "zona":
                    $value = explode(",",$value);
                    $zoneIdEscaped = "";
                    foreach ($value as $zoneId)
                    {
                        $zoneIdEscaped .= $ci->db->escape($zoneId).", ";
                    }
                    $zoneIdEscaped = substr($zoneIdEscaped,0,-2);
                    $zoneSql = "";
                    if($zoneIdEscaped != "")
                    {
                        $zoneSql = " inm_zon_id in (".$zoneIdEscaped.") and ";
                    }
                    $sqlStatement .= $this->_includeAddressFilterByZoneName($zoneSql);
                    break;
                case "estado":
                    $sqlStatement.= "inm_status = ".$ci->db->escape($this->_arrayStatus[strtolower($value)])." and ";
                    break;
                case "publicado-por":
                    $sqlStatement.= "usu_tipo = ".$ci->db->escape(ucfirst($value))." and ";
                    break;
                case "amenidades":
                    if($value != "")
                    {
                        $sqlStatement .= " ( ";
                        $amenityList = str_split($value);
                        foreach ($amenityList as $amenity)
                        {
                            $sqlStatement.= "inm_amenities LIKE '%".$amenity."%' or ";
                        }
                        $sqlStatement = substr($sqlStatement,0,-3);
                        $sqlStatement .= " ) and ";
                    }

                    break;
            }
        }
        return $sqlStatement;
    }

    private function _includeAddressFilterByZoneName($sqlZone)
    {
        //if the zoneStringArray attrib has items them modify the sql to search by zones
        if(count($this->_zoneStringArray) > 0)
        {
            $sqlAddress = "";
            foreach ($this->_zoneStringArray as $zoneName)
            {
                $sqlAddress.= "inm_direccion LIKE '%".$zoneName["text"]."%' or ";
            }
            $sqlAddress = substr($sqlAddress,0,-3);
            //let's remove the 'and' at end of sql
            $sqlZone = substr($sqlZone,0,-4);
            $sqlZone = " (".$sqlZone." or ".$sqlAddress.") and ";

        }
        return $sqlZone;
    }

    public function getSpecialFeatures()
    {
        $ci = &get_instance();
        $ci->load->database();
        $arrayExtraParams = $this->_getExtraParams();
        $specialFeatures = "";

        foreach ($arrayExtraParams as $param => $value)
        {
            $param = strtolower($param);
            $value = trim($value);
            switch ($param)
            {
                case "habitacion":
                    if($value == "5")
                    {
                        $specialFeatures.= "inmueble_caracteristica_optimized.4 >= 5 or ";
                    }
                    else
                    {
                        $specialFeatures.= "inmueble_caracteristica_optimized.4 <= ".$value." or ";
                    }
                    break;
                case "bano":
                    if($value == "5")
                    {
                        $specialFeatures.= "inmueble_caracteristica_optimized.5 >= 5 or ";
                    }
                    else
                    {
                        $specialFeatures.= "inmueble_caracteristica_optimized.5 = ".$value." or ";
                    }
                    break;
                case "parqueo":
                    if($value == "5")
                    {
                        $specialFeatures.= "inmueble_caracteristica_optimized.9 >= 5 or ";
                    }
                    else
                    {
                        $specialFeatures.= "inmueble_caracteristica_optimized.9 = ".$value." or ";
                    }
                    break;
                case "condominio":
                    $specialFeatures.= "inmueble_caracteristica_optimized.23 = ".$ci->db->escape($value)." or ";
                    break;
                case "vivienda-social":
                    $specialFeatures.= "inmueble_caracteristica_optimized.24 = ".$ci->db->escape($value)." or ";

            }
        }

        if($specialFeatures != "")
        {
            $specialFeatures = substr($specialFeatures,0,-4);
            $specialFeatures = " (".$specialFeatures.") ";
        }

        return $specialFeatures;
    }

    private function _getExtraParams()
    {
        $extraParamsExist = strpos($this->_urlToDecode,"?");

        if(count($this->_arrayExtraParams) <= 0)
        {
            if($extraParamsExist !== FALSE)
            {
                $extraParams = substr($this->_urlToDecode, $extraParamsExist + 1, strlen($this->_urlToDecode));
                parse_str("?".$extraParams, $this->_arrayExtraParams);
                foreach($this->_arrayExtraParams as $key => $value)
                {
                    if(strpos($key, "?") !== FALSE)
                    {
                        unset($this->_arrayExtraParams[$key]);
                        $key = substr($key,1,strlen($key));
                    }
                    $this->_arrayExtraParams[$key] = $value;
                }
            }
        }
        return $this->_arrayExtraParams;
    }
}