<?php
require BASE_DIRECTORY . DS . 'application' . DS .'libraries'.DS. 'tigomoney' . DS . 'lib'. DS .'nusoap.php';
require BASE_DIRECTORY . DS . 'application' . DS .'libraries'.DS. 'tigomoney' . DS . 'CTripleDes.php';
require BASE_DIRECTORY . DS . 'application' . DS .'libraries'.DS. 'tigomoney' . DS . 'IntegrationLogic.php';

class TigoMoneyHandler
{
	private $_identificationKey;
	private $_encryptionKey;
	private $_webServicesUrl;
    private $_tigoMoney;

	public function __construct()
	{
        $this->_defineEnvironmentVariables();
        $this->_tigoMoney = new IntegrationLogic($this->_identificationKey, $this->_encryptionKey, $this->_webServicesUrl);
	}

	public function requestPayment($orderId, $amount, $cellNumber, $confirmationMessage, $notificationMessage, $items, $legalName, $nit)
    {
        $response = $this->_tigoMoney->tmPaymentAsyncWithtBill($orderId,$amount,$cellNumber,$confirmationMessage,$notificationMessage,$items,$legalName,$nit);

        $tigoMoneyResponse = new TigoMoneyRequestPaymentResponse($response);
        return $tigoMoneyResponse;
    }

    public function getPaymentStatus($orderId)
    {
        $response = $this->_tigoMoney->checkStatus($orderId);
        $tigoMoneyStatusResponse = new TigoMoneyStatusResponse($response);
        return $tigoMoneyStatusResponse;
    }

	private function _defineEnvironmentVariables()
    {
        switch (ENVIRONMENT)
        {
            case 'development':
            case 'testing':
                //$this->_identificationKey = "JCUSSY_TET";
                //passed by tigo on 15/05/2019
                $this->_identificationKey = "2c522f105107418cc9d5a57632d676f66ee35a2b615e46c73c7cb293b0c7e2fb12725208ef1d2cfd9a360196ff41d095e170911460a56608df12d4514b8a538a";
                $this->_encryptionKey = "7LJ4J18WS98FNVO7N01VFHX4";
                //$this->_webServicesUrl = "https://200.73.103.48/PasarelaSandbox/service.php?wsdl";
                //passed by tigo on 15/05/2019
                $this->_webServicesUrl = "https://pasarelatest.tigomoney.com.bo/PasarelaServices_V2/CustomerServices?wsdl";
                break;
            case 'production':
                $this->_identificationKey = "cb6ce94b52d5fc21bfa35ad64baaff6301c154ac981180ec994d2ca64114e2899d21bb596011f52bb913b7ea3152d375490384bf7d99d3562dbf00021e826f1f";
                $this->_encryptionKey = "PHF07Z8GAFBJTZAYBRGNCYN0";
                $this->_webServicesUrl = "";
                break;
            default:
                exit('TigoMoneyHandler: The application environment is not set correctly.');
        }
    }

    public function IPN()
    {

    }

}

class TigoMoneyRequestPaymentResponse
{
    private $_gateway;
    private $_isSuccess;
    private $_data;
    private $_status;
    private $_message;
    private $_orderId;

    public function __construct($gateway)
    {
        parse_str($gateway, $this->_gateway);
        $response = FALSE;
        if($this->_gateway["codRes"] == '0')
        {
            $response = TRUE;
        }
        $this->_isSuccess = $response;
        $this->_data = $this->_gateway;
        $this->_status = $this->_isSuccess?"SUCCESS":"ERROR";
        $this->_message = $this->_gateway['mensaje'];
        $this->_orderId = $this->_gateway['orderId'];
    }

    public function isSuccess()
    {
        return $this->_isSuccess;
    }

    public function getData()
    {
        return $this->_data;
    }

    public function getStatus()
    {
        return $this->_status;
    }

    public function getMessage()
    {
        return $this->_message;
    }

    public function getTigoOrderId()
    {
        return $this->_orderId;
    }

    public function toArray()
    {
        $toArray = array();
        $toArray['success'] = $this->_isSuccess?1:0;
        $toArray['message'] = $this->_message;
        $toArray['status'] = $this->_status;
        $toArray['data'] = $this->_data;
        $toArray['orderId'] = $this->_orderId;
        return $toArray;
    }
}

class TigoMoneyStatusResponse
{
    private $_isSuccess;
    private $_data;
    private $_status;
    private $_message;
    private $_tigoTransactionReference;
    private $_gateway;

    public function __construct($gateway)
    {
        $this->_gateway = explode(";",$gateway);
    }

    public function isSuccess()
    {
        $this->_isSuccess = ($this->_gateway[0] == 0) ? TRUE : FALSE;
        return $this->_isSuccess;
    }

    public function getData()
    {
        $this->_data = $this->_gateway;
        return $this->_data;
    }

    public function getStatus()
    {
        $codeList = array("CORRECTO", "INCORRECTO", "REVERTIDO", "EN PROGRESO");
        $this->_status = $codeList[$this->_gateway[0]];
        return $this->_status;
    }

    public function getMessage()
    {
        $errorMessages = array(
            3 => "Cuenta no habilitada con Tigo Money, regístrate marcando *555# o descarga la App Tigo Money a tu celular. Mas info llama al *555, o contáctate con soporte directamente desde la App Tigo Money.",
            4 => "Cuenta no habilitada con Tigo Money, regístrate marcando *555# o descarga la App Tigo Money a tu celular. Mas info llama al *555, o contáctate con soporte directamente desde la App Tigo Money.",
            7 => "Acceso denegado por favor intenta nuevamente verificando los datos ingresados.",
            8 => "El PIN ingresado es inválido, si olvidaste tu pin, llama al *555 o contáctate con soporte directamente desde la App Tigo Money, si tu saldo es mayor a Bs 313, debes pasar por un of. Tigo con tu carnet.",
            11 => "Tiempo agotado, por favor inicia nuevamente la transaccion.",
            14 => "Cuenta no habilitada con Tigo Money, regístrate marcando *555# o descarga la App Tigo Money a tu celular. Mas info llama al *555, o contáctate con soporte directamente desde la App Tigo Money.",
            17 => "El monto solicitado no es válido. Verifica los datos ingresados.",
            19 => "Comercio no Habilitado para el pago con Tigo Money.",
            16 => "Cuenta Tigo Money suspendida, por favor comunícate al *555, o contáctate con soporte directamente desde la App Tigo Money.",
            23 => "El monto solicitado es inferior al requerido, por favor verifica los datos ingresados.",
            24 => "El monto solicitado es superior al requerido, por favor verifica los datos ingresados.",
            1001 => "Tu saldo es insuficiente para completar la transaccion, carga tu cuenta desde la web de tu banco, desde un cajero Tigo Money ó desde un Punto más cercano a ti, marcando *555# o ingresando a la App Tigo Money.",
            1002 => "Ingresa a Completa tu transaccion desde la App Tigo Money o marcando *555#, Si olvidaste tu PIN, llama al *555, o contáctate con soporte directamente desde la App Tigo Money. Si tu saldo es mayor a Bs 313, debes pasar por un of. Tigo con tu carnet.",
            1004 => "Estimado cliente, llegaste al límite maximo para realizar transacciones, para consultas por favor llama al *555, o contáctate con soporte directamente desde la App Tigo Money. También puedes pasar por una Of. Tigo con tu Carnet.",
            1012 => "Estimado Cliente excediste el límite de intentos para introducir tu PIN, por favor comunícate con el *555 para solicitar nuevo PIN, o contáctate con soporte directamente desde la App Tigo Money, si tu saldo es mayor a Bs 313, debes pasar por una of. Tigo con tu Carnet.",
            560 => "Estimado cliente tu transaccion no pudo ser completada, por favor intenta nuevamente.",
            1003 => "El monto ingresado excede el limite diario máximo de transacción de tu cuenta. Intenta con un monto menor.",
            "x" => "Ocurrio un error inesperado, por favor intentalo mas tarde. Disculpe las molestias causadas."
        );
        $additionalMessage = "";
        //if the gateway response 1 then let's find the 'motivo'.
        if($this->_gateway[0] == 1)
        {
            $additionalErrorInfoUrlEncoded = $this->_gateway[2];
            parse_str($additionalErrorInfoUrlEncoded, $additionalErrorInfoArray);
            $additionalErrorCode = isset($additionalErrorInfoArray["motivo"])?$additionalErrorInfoArray["motivo"]:"";
            $additionalMessage = isset($errorMessages[$additionalErrorCode])?": ".$errorMessages[$additionalErrorCode]:"";
        }
        $this->_message = $additionalMessage;
        return $this->_message;
    }

    public function getTigoTransactionReference()
    {
        $invoiceDataUrlEncoded = $this->_gateway[2];
        parse_str($invoiceDataUrlEncoded, $invoiceDataArray);
        $this->_tigoTransactionReference = isset($invoiceDataArray["transaccion"])?$invoiceDataArray["transaccion"]:"";
        return $this->_tigoTransactionReference;
    }

    public function toArray()
    {
        $toArray = array();
        $toArray['success'] = $this->isSuccess()?1:0;
        $toArray['status'] = $this->getStatus();
        $toArray['message'] = $this->getMessage();
        $toArray['data'] = $this->getData();
        $toArray['transactionReference'] = $this->getTigoTransactionReference();
        return $toArray;
    }
}