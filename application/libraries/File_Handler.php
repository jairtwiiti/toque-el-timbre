<?php
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 30/5/2017
 * Time: 12:15 PM
 */
class File_Handler
{
    private $_fileName;
    private $_imageType;
    private $_arrayImageTypeSource;
    private $_createBackup;

    public function __construct($fileName = "", $imageType = "propertyImage")
    {
        $this->_fileName = $fileName;
        $this->_imageType = $imageType;
        $this->_arrayImageTypeSource = array(
            "propertyImage" => "/admin/imagenes/inmueble",
            "propertyPlan" => "/admin/imagenes/planos",
            "projectImage" => "/admin/imagenes/proyecto",
            "projectSlideShowImage" => "/admin/imagenes/proyecto_slideshow",
            "userImage" => "/assets/uploads/users"
        );
        $this->_createBackup = FALSE;
    }

    public function getThumbnail($width = "150", $height = "150", $alternateText = "ToqueElTimbre.com", $title = "")
    {
        return new Thumbnail($this->_imageType, $this->_fileName, $width, $height, $alternateText, $title);
    }

    public function delete()
    {
        $fileExist = file_exists(FCPATH.$this->_arrayImageTypeSource[$this->_imageType]."/".$this->_fileName);
        if($this->_fileName != "" && $fileExist)
            return unlink(FCPATH.$this->_arrayImageTypeSource[$this->_imageType]."/".$this->_fileName);
    }

    public function enableCreateBackup($createBackup = FALSE)
    {
        $this->_createBackup = $createBackup;
    }

    public function compressImage($ruta, $filename)
    {
        //original path
        $extencion = substr(strrchr($filename, '.'), 1);
        $extencion = strtolower($extencion);
        $dir = $ruta ."/". $filename;
        $rtOriginal = $dir;
        switch($extencion)
        {
            case 'jpg' :
                $original = imagecreatefromjpeg($rtOriginal);
                break;
            case 'jpeg' :
                $original = imagecreatefromjpeg($rtOriginal);
                break;
            case 'png' :
                $original = imagecreatefrompng($rtOriginal);
                break;
        }
        //Crear variable de imagen a partir de la original
        //Recoger ancho y alto de la original
        list($ancho, $alto) = getimagesize($rtOriginal);
        //Definir tamaño máximo y mínimo
        $height = (($alto*100)/$ancho)/100;
        $ancho_final = 1024;
        $alto_final = 1024*$height;
        $lienzo = imagecreate($ancho_final, $alto_final);
        //Copiar $original sobre la imagen que acabamos de crear en blanco ($tmp)
        $lienzo=imagecreatetruecolor($ancho_final, $alto_final);
        imagecopyresampled($lienzo, $original, 0, 0, 0, 0, $ancho_final, $alto_final, $ancho, $alto);
        //imagecopyresampled($lienzo, $original, 0, 0, 0, 0, $ancho, $alto, $ancho, $alto);
        //Limpiar memoria
        imagedestroy($original);
        //Definimos la calidad de la imagen final
        $cal = 75;
        //Se crea la imagen final en el directorio indicado
        //imagetruecolortopalette($lienzo, false, 255);
        //$filename = str_replace(".","_test_.",$filename);
        $this->_createBackup($rtOriginal);
        switch($extencion)
        {
            case 'jpg' :
                imagejpeg($lienzo, FCPATH.$this->_arrayImageTypeSource[$this->_imageType].'/'. $filename, $cal);
                break;
            case 'jpeg' :
                imagejpeg($lienzo, FCPATH.$this->_arrayImageTypeSource[$this->_imageType].'/'. $filename, $cal);
                break;
            default :
                imagepng($lienzo, FCPATH.$this->_arrayImageTypeSource[$this->_imageType].'/'. $filename, 9);
        }
    }

    private function _createBackup($rtOriginal)
    {
        if($this->_createBackup)
        {
            $oldName = $rtOriginal;
            $newName = $oldName."_bkp";
            rename($oldName, $newName);
        }
    }

    public function uploadImage($file)
    {
        if (empty($file["tmp_name"]))
        {
            //TODO: VALIDATE THE FILE SIZE BEFORE IT IS ULPLOADED TO THE TMP
            // DIRECTORY
            throw new Exception("The file could not be uploaded");
        }
        $filePath = FCPATH.$this->_arrayImageTypeSource[$this->_imageType];

        $fileExt = pathinfo($file['name'], PATHINFO_EXTENSION);

//        $finfo = new finfo(FILEINFO_MIME_TYPE);
//        $mimeType = $finfo->file($file['tmp_name']);

        //$fileSize = filesize($file['tmp_name']);

//        if (!empty($subDirectory))
//        {
//            $filePath .= $subDirectory . "/";
//        }

        //$fullPath = $this->config['basePath'] . $filePath;
//        if (!file_exists($fullPath))
//        {
//            mkdir($fullPath);
//        }
//        $uploadFileName = $this->sanitize($file['name']);
//        if (!empty($newFileName))
//        {
//            $fileName = $this->sanitize($newFileName) . "." . strtolower($fileExt);
//        }
//        else
//        {
//            $fileName = $uploadFileName;
//        }
        $fileName = date("Y_m_d_H_i_s_").rand().".".$fileExt;
        $url = $filePath ."/". $fileName;
        $fullFilePath = $url;
//        var_dump($file,$fullFilePath,$fileExt);exit;
//        $hash = sha1_file($file['tmp_name']);
//        $imageInfo = getimagesize($file['tmp_name']);
//        $dbFile = new Model_File();
//        if ($imageInfo !== false)
//        {
//            $dbFile->setWidth($imageInfo[0]);
//            $dbFile->setHeight($imageInfo[1]);
//        }
//        $dbFile->setExtension($fileExt);
//        $dbFile->setFilename($fileName);
//        $dbFile->setHash($hash);
//        $dbFile->setFilepath($filePath);
//        $dbFile->setUrl($url);
//        $dbFile->setMimetype($mimeType);
//        $dbFile->setSize($fileSize);
//        $dbFile->setUploadfilename($uploadFileName);

//        $this->validate($dbFile,$typeUpload);
        if (!is_writable($filePath)) {
            throw new Exception("The destiny directory is not writable");
        }
        if (move_uploaded_file($file['tmp_name'], $fullFilePath) === false)
        {
            throw new Exception("The file could not be uploaded");
        }
        //chmod($fullFilePath, 0775);
        try{
            $this->compressImage($filePath,$fileName);
        }
        catch (Exception $e)
        {
            throw new Exception($e->getMessage());
        }

        return $fileName;
    }

}

class Thumbnail
{
    private $_imageType;
    private $_fileName;
    private $_width;
    private $_height;
    private $_defaultImageSource;
    private $_timThumbLibrary;
    private $_propertyImageFolder;
    private $_fileSource;
    private $_alternateText;
    private $_title;
    private $_arrayImageTypeSource;

    public function __construct($imageType, $fileName, $width, $height, $alternateText = "ToqueElTimbre.com", $title = "ToqueElTimbre.com")
    {
        $this->_imageType = $imageType;
        $this->_fileName = $fileName;
        $this->_width = $width;
        $this->_height = $height;
        $this->_defaultImageSource = base_url("assets/img/default.png");
        $this->_timThumbLibrary = base_url("librerias/timthumb.php");
        $this->_propertyImageFolder = base_url("admin/imagenes/inmueble");//This attribute seems to be not used
        $this->_fileSource = $this->_timThumbLibrary."?src=".$this->_defaultImageSource."&w=".$this->_width."&h=".$this->_height;
        $this->_alternateText = $alternateText;
        $this->_title = $title;
        $this->_arrayImageTypeSource = array(
                                            "propertyImage" => "/admin/imagenes/inmueble",
                                            "propertyPlan" => "/admin/imagenes/planos",
                                            "projectImage" => "/admin/imagenes/proyecto",
                                            "projectSlideShowImage" => "/admin/imagenes/proyecto_slideshow",
                                            "userImage" => "/assets/uploads/users"
                                                );
    }

    public function getSource()
    {
        $fileExist = $this->fileExist();
        if($this->_fileName!="" && $fileExist)
        {
            $this->_fileSource = $this->_timThumbLibrary."?src=".base_url($this->_arrayImageTypeSource[$this->_imageType])."/".$this->_fileName."&w=".$this->_width."&h=".$this->_height;
        }
        return $this->_fileSource;
    }

    public function getAlternateText()
    {
        return $this->_alternateText;
    }

    public function getTitle()
    {
        if($this->_title == "")
        {
            $this->_title = $this->_alternateText;
        }

        return $this->_title;
    }

    public function fileExist()
    {
        return file_exists(BASE_DIRECTORY.$this->_arrayImageTypeSource[$this->_imageType]."/".$this->_fileName);
    }
}