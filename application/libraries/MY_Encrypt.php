<?php
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 16/4/2019
 * Time: 9:56 AM
 */

class MY_Encrypt extends CI_Encryption
{
    /**
     * Encodes a string.
     *
     * @param string $string The string to encrypt.
     * @param string $key[optional] The key to encrypt with.
     * @param bool $url_safe[optional] Specifies whether or not the
     *                returned string should be url-safe.
     * @return string
     */

    protected $_key;
    public function __construct() {
        $ci = &get_instance();
        $this->_key = $ci->config->item('encryption_key');
        $params = array(
            'driver' => 'openssl',
            'key' => $this->_key
        );
        parent::__construct($params);

    }

    function encode($string, $params="", $url_safe=TRUE)
    {
        $ret = parent::encrypt($string);

        if ($url_safe)
        {
            $ret = strtr(
                $ret,
                array(
                    '+' => '.',
                    '=' => '-',
                    '/' => '~'
                )
            );
        }

        return $ret;
    }

    /**
     * Decodes the given string.
     *
     * @access public
     * @param string $string The encrypted string to decrypt.
     * @param string $key[optional] The key to use for decryption.
     * @return string
     */
    function decode($string, $params = array())
    {
        $string = strtr(
            $string,
            array(
                '.' => '+',
                '-' => '=',
                '~' => '/'
            )
        );

        return parent::decrypt($string);
    }
}