<?php 
$config = array();

$config['projectJsPath'] = assets_url("backend/js");
$config['projectCssPath'] = assets_url("backend/css");

$config['publicJsPath'] = assets_url("js");
$config['publicCssPath'] = assets_url("css");

$config['complements']['tinymce']['js'] = assets_url('backend/global/plugins/tinymce/tinymce.min.js');

$config['complements']['parsley']['js'] = assets_url('backend/global/plugins/Parsleyjs-272/dist/parsley.min.js');
$config['complements']['parsley.spanish']['js'] = assets_url('backend/global/plugins/Parsleyjs-272/dist/i18n/es.js');
$config['complements']['parsley']['css'] = assets_url('backend/global/plugins/Parsleyjs-272/src/parsley.css');

$config['complements']['bootstrap.datepicker']['js'] = assets_url('backend/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
$config['complements']['bootstrap.datepicker']['css'] = assets_url('backend/global/plugins/bootstrap-datepicker/css/datepicker.css');

$config['complements']['jquery']['js'] = assets_url('js/jquery.min.js');
$config['complements']['jquery.select2']['js'] = assets_url('backend/global/plugins/select2/select2.min.js');

$config['complements']['inputmask']['js'] = assets_url('backend/global/plugins/inputmask/inputmask.js');
$config['complements']['jquery.inputmask.bundle']['js'] = assets_url('backend/global/plugins/inputmask/jquery.inputmask.bundle.js');
$config['complements']['jquery.inputmask.phone']['js'] = assets_url('backend/global/plugins/inputmask/phone-codes/phone.js');
$config['complements']['jquery.inputmask.phone-be']['js'] = assets_url('backend/global/plugins/inputmask/phone-codes/phone-be.js');
$config['complements']['jquery.inputmask.phone-ru']['js'] = assets_url('backend/global/plugins/inputmask/phone-codes/phone-ru.js');

$config['complements']['jquery.select2.bootstrap']['css'] = assets_url('backend/global/plugins/select2/select2-bootstrap.css');

$config['complements']['jquery.datatables']['js'] = assets_url('backend/global/plugins/DataTables11016/media/js/jquery.dataTables.min.js');

$config['complements']['handlebars']['js'] = assets_url('backend/global/plugins/handlebars-v4.0.10.js');

$config['complements']['jquery.datatables.bootstrap']['js'] = assets_url('backend/global/plugins/DataTables11016/media/js/dataTables.bootstrap.js');
$config['complements']['jquery.datatables.bootstrap']['css'] = assets_url('backend/global/plugins/DataTables11016/media/css/dataTables.bootstrap.css');

$config['complements']['jquery.datatables.jszip']['js'] = assets_url('backend/global/plugins/DataTables11016/plugins/jszip.min.js');
$config['complements']['jquery.datatables.pdfmake']['js'] = assets_url('backend/global/plugins/DataTables11016/plugins/pdfmake.min.js');
$config['complements']['jquery.datatables.vfs_fonts']['js'] = assets_url('backend/global/plugins/DataTables11016/plugins/vfs_fonts.js');
$config['complements']['jquery.datatables.buttons.html5']['js'] = assets_url('backend/global/plugins/DataTables11016/extensions/Buttons/js/buttons.html5.min.js');
$config['complements']['jquery.datatables.buttons.print']['js'] = assets_url('backend/global/plugins/DataTables11016/extensions/Buttons/js/buttons.print.min.js');
$config['complements']['jquery.datatables.buttons.flash']['js'] = assets_url('backend/global/plugins/DataTables11016/extensions/Buttons/js/buttons.flash.js');

$config['complements']['jquery.datatables.buttons']['js'] = assets_url('backend/global/plugins/DataTables11016/extensions/Buttons/js/dataTables.buttons.js');
$config['complements']['jquery.datatables.buttons']['css'] = assets_url('backend/global/plugins/DataTables11016/extensions/Buttons/css/buttons.dataTables.min.css');
$config['complements']['jquery.datatables.buttons.bootstrap']['css'] = assets_url('backend/global/plugins/DataTables11016/extensions/Buttons/css/buttons.bootstrap.min.css' );

$config['complements']['jquery.datatables.filterdelay']['js'] = assets_url('backend/global/plugins/DataTables11016/extensions/FilterDelay/dataTables.FilterDelay.js');

$config['complements']['bootbox']['js'] = assets_url('backend/global/plugins/bootbox.min.js');

//$config['complements']['google.maps.api']['js'] = "https://maps.google.com/maps/api/js?key=AIzaSyC_XEN-l56YPaC0l6n4I83cp_gtSHAH48k";
$config['complements']['google.maps.api']['js'] = "https://maps.google.com/maps/api/js?key=AIzaSyArzfSdkJxIlplT_5VpgGNzeb4lj9aGcCc";
$config['complements']['gmaps']['js'] = assets_url('backend/global/plugins/gmaps0425/gmaps.min.js');

$config['complements']['dropzone']['js'] = assets_url('backend/global/plugins/dropzone4dev/dropzone.js');
$config['complements']['dropzone']['css'] = assets_url('backend/global/plugins/dropzone4dev/dropzone.css');

$config['complements']['amcharts3.21']['js'] = assets_url('backend/global/plugins/amcharts321/amcharts.js');
$config['complements']['amcharts.serial']['js'] = assets_url('backend/global/plugins/amcharts321/serial.js');
$config['complements']['amcharts.pie']['js'] = assets_url('backend/global/plugins/amcharts321/pie.js');
$config['complements']['amcharts.export']['js'] = assets_url('backend/global/plugins/amcharts321/plugins/export/export.min.js');
$config['complements']['amcharts.export']['css'] = assets_url('backend/global/plugins/amcharts321/plugins/export/export.css');
$config['complements']['amcharts.theme.light']['js'] = assets_url('backend/global/plugins/amcharts321/themes/light.js');
$config['complements']['amcharts.lang.es']['js'] = assets_url('backend/global/plugins/amcharts321/lang/es.js');

$config['complements']['sticky-element']['js'] = assets_url('backend/global/plugins/jquery.sticky-kit.min.js');
$config['complements']['pagination']['js'] = assets_url('backend/global/plugins/pagination.min.js');

$config['complements']['tinynav']['js'] = assets_url('js/nav/tinynav.js');
$config['complements']['superfish']['js'] = assets_url('js/nav/superfish.js');
$config['complements']['hover-intent']['js'] = assets_url('js/nav/hoverIntent.min.js');
$config['complements']['jquery.ui.totop']['js'] = assets_url('js/totop/jquery.ui.totop.js');
$config['complements']['owl.carousel']['js'] = assets_url('js/owlcarousel/owl.carousel.js');
$config['complements']['owl.carousel']['css'] = assets_url('css/owlcarousel/owl.carousel.css');
$config['complements']['jquery.content-panel-switcher']['js'] = assets_url('js/efect_switcher/jquery.content-panel-switcher.js');
$config['complements']['theme-options']['js'] = assets_url('js/theme-options/theme-options.js');
$config['complements']['jquery.cookies']['js'] = assets_url('js/theme-options/jquery.cookies.js');
$config['complements']['bootstrapjs']['js'] = assets_url('js/bootstrap/bootstrap.min.js');
$config['complements']['dropdown']['js'] = assets_url('js/bootstrap/bootstrap-dropdown/js/dropdown.min.js');
$config['complements']['dropdown']['css'] = assets_url('css/bootstrap/bootstrap-dropdown/css/dropdown.min.css');
$config['complements']['dropdown.theme']['css'] = assets_url('css/bootstrap/bootstrap-dropdown/css/dropdown-theme.min.css');
$config['complements']['animate']['js'] = assets_url('js/animate.js');
$config['complements']['camera']['js'] = assets_url('js/slide/camera.min.js');
$config['complements']['jquery.easing']['js'] = assets_url('js/slide/jquery.easing.1.3.min.js');
$config['complements']['jquery.ui']['js'] = assets_url('js/jquery-ui.min.js');
$config['complements']['jquery.ui.touch-punch']['js'] = assets_url('js/jquery.ui.touch-punch.min.js');
$config['complements']['jquery.switchButton']['js'] = assets_url('js/jquery.switchButton.js');
$config['complements']['numeral']['js'] = assets_url('js/numeral.min.js');
$config['complements']['jquery.colorbox']['js'] = assets_url('js/jquery.colorbox-min.js');
$config['complements']['jquery.colorbox']['css'] = assets_url('css/colorbox/colorbox.css');
$config['complements']['jquery.print']['js'] = assets_url('js/jQuery.print.min.js');
$config['complements']['main']['js'] = assets_url('js/main.js');
$config['complements']['jquery.blockui']['js'] = assets_url('js/jquery.blockUI.js');

$config['complements']['pricing']['css'] = assets_url('backend/css/pricing.css');
$config['complements']['components']['css'] = assets_url('backend/css/components.min.css');

$config['complements']['sweet-alerts']['css'] = assets_url('backend/global/plugins/sweetalert2/sweetalert2.min.css');
$config['complements']['sweet-alerts']['js'] = assets_url('backend/global/plugins/sweetalert2/sweetalert2.min.js');

$config['complements']['bootstrap-wizard']['js'] = assets_url('backend/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js');
$config['complements']['form-wizard']['js'] = assets_url('backend/admin/pages/scripts/form-wizard.js');
$config['complements']['jquery.countdown']['js'] = assets_url('backend/global/plugins/jquery.countdown204/jquery.countdown.min.js');
$config['complements']['moment']['js'] = assets_url('backend/global/plugins/moment.js');
$config['complements']['moment-with-locales']['js'] = assets_url('backend/global/plugins/moment-with-locales.js');
$config['complements']['line-height']['js'] = assets_url('js/line-height.js');