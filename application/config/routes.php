<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/
/* GENERALES */
$route['login'] = "dashboard/login/index";
$route['dashboard'] = "dashboard/panel/index";
$route['logout'] = "dashboard/login/logout";
$route['registrarse'] = "dashboard/login/register";
$route['recuperar-contrasena'] = "dashboard/login/forgot_password";
$route['confirmar/(:num)/(:any)'] = "dashboard/verification/index/$2/$3";
$route['cambiar-contrasena/(:any)/(:any)'] = "dashboard/verification/reset/$2/$3";

$route['mis-favoritos'] = "favorite/index";

/* PROFILE */
$route['dashboard/edit-profile'] = "dashboard/panel/profile";
$route['dashboard/messages'] = "dashboard/messages/index";
$route['dashboard/message/(:num)'] = "dashboard/messages/detail/$3";

/* PUBLICATIONS */
$route['dashboard/publications'] = "dashboard/publications/index";
$route['dashboard/publications/p/(:num)'] = "dashboard/publications/index/$4";
$route['dashboard/publication/register'] = "dashboard/publications/register";
//$route['dashboard/publication/edit/(:num)/(:num)'] = "dashboard/publications/update/$4/$5";
$route['dashboard/publication/edit/(:num)/(:num)'] = "admin/Property/edit/$4/$5";
$route['dashboard/publication/delete/(:num)'] = "dashboard/publications/delete/$4";
$route['dashboard/publication/renew/(:num)'] = "dashboard/publications/renew/$4";
$route['dashboard/publication/paymentinfo/(:num)'] = "dashboard/publications/paymentinfo/$4";
$route['dashboard/publication/pay/(:num)'] = "dashboard/publications/pay/$4";

/* PAYMENTS */
$route['dashboard/payments'] = "dashboard/payment/index";
$route['dashboard/payments/p/(:num)'] = "dashboard/payment/index/$4";
$route['dashboard/payment/detail/(:num)'] = "dashboard/payment/detail/$4";
$route['dashboard/payment/(:num)'] = "dashboard/payment/register/$4";
$route['dashboard/payment/publication/(:num)'] = "dashboard/payment/register_publication/$4";
$route['dashboard/payment/delete/(:num)'] = "dashboard/payment/delete/$4";

/* PROJECTS */
$route['dashboard/projects'] = "dashboard/projects/index";
$route['dashboard/projects/p/(:num)'] = "dashboard/projects/index/$4";
$route['dashboard/project/available/(:num)'] = "dashboard/projects/available/$4";

/* LANDING */
//This pages seems to be incomplete
/*$route['alquiler'] = "landing/alquilar";
$route['comprar'] = "landing/comprar";
$route['anticretico'] = "landing/anticretico";*/

/*---------------------------------------------------------------*/
$route['acerca-de-toqueeltimbre'] = "pages/nosotros";
$route['preguntas-frecuentes'] = "pages/preguntas";
$route['politicas-de-privacidad'] = "pages/politicas";
$route['formas-de-pago'] = "pages/pagos";
$route['videotutoriales'] = "pages/videotutoriales";
$route['tutoriales'] = "pages/videotutoriales";
$route['contactenos'] = "pages/contacto";
$route['contacto-agente'] = "pages/agente";
$route['terminos-y-condiciones'] = "pages/terminos";
$route['publicar-inmobiliarias'] = "pages/inmobiliaria";
$route['publicar-particulares'] = "pages/particular";
$route['publicar-proyectos'] = "pages/proyectos";
$route['publicar-como-inmobiliaria'] = "pages/inmobiliaria";
$route['publicar-como-particular'] = "pages/particular";
$route['publicar-proyecto-inmobiliario'] = "pages/proyectos";
$route['tabla-de-precios'] = "pages/precios";
$route['busqueda/detalle/(:num)'] = "inmueble/detail_search_user/$3";
$route['inicio'] = "home/index";


$route['perfil/(:num)/(:any)/p/(:num)'] = "inmobiliaria/index/$2/$3/$5";
$route['perfil/(:num)/(:any)/p'] = "inmobiliaria/index/$2/$3";
$route['perfil/(:num)/(:any)'] = "inmobiliaria/index/$2/$3";

/*---------------------------------------------------------------*/
// MODIFICACIONES SEO
/*$route['bolivia'] = "home/change_bolivia/";
$route['santa-cruz'] = "home/change_city/";
$route['cochabamba'] = "home/change_city/";
$route['la-paz'] = "home/change_city/";*/

/*$route['buscar/p/(:num)'] = "search/index/p/$3";//This doesn't work and needs to be fixed
$route['buscar/limpiar'] = "search/clear";
$route['buscar'] = "search/index";//This is showing up all result
$route['buscar/(:any)'] = "search/index";//once the criteria are passed to index
$route['buscar/(:any)/p/(:num)'] = "search/index";//if there are criteria and pagination
//$route['buscar/(:any)'] = "search/search_seo/$2";
$route['busqueda/(:any)'] = "search/search_seo/$2";*/

/*combination algorithm*/
//$route['buscar/(.+)'] = "home/seoHandler/$1/$2/$3";
//$route['buscar'] = "home/seoHandler/$1/$2/$3";
//$route['buscar/limpiar'] = "search/clear";
$route['buscar/(:any)'] = "search/index";//once the criteria are passed to index
#example
//$route["santa-cruz-de-la-sierra/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";



/*---------------------------------------------------------------*/
$route['proyecto/(:any)/area-social'] = "project/social/$2";
$route['proyecto/(:any)/fotogaleria'] = "project/gallery/$2";
$route['proyecto/(:any)/acabado'] = "project/acabado/$2";
$route['proyecto/(:any)/precio'] = "project/prices/$2";
$route['proyecto/(:any)'] = "project/index/$2";

// PROYECTOS------------------------------------------------

$route['jardinesdelremanso'] = "project/index/$1";
$route['jardines'] = "project/index/$1";

$route['jardines-del-remanso'] = "project/index/$1";
$route['jardines-del-remanso/precio'] = "project/prices/$1";
$route['jardines-del-remanso/acabado'] = "project/acabado/$1";
$route['jardines-del-remanso/fotogaleria'] = "project/gallery/$1";
$route['jardines-del-remanso/area-social'] = "project/social/$1";
$route['jardines-del-remanso/thank-you'] = "search/thankYouPage";

$route['oxford-tower'] = "project/index/$1";
$route['oxford-tower/precio'] = "project/prices/$1";
$route['oxford-tower/acabado'] = "project/acabado/$1";
$route['oxford-tower/fotogaleria'] = "project/gallery/$1";
$route['oxford-tower/area-social'] = "project/social/$1";
$route['oxford-tower/thank-you'] = "search/thankYouPage";

$route['condominio-san-jorge'] = "project/index/$1";
$route['condominio-san-jorge/precio'] = "project/prices/$1";
$route['condominio-san-jorge/acabado'] = "project/acabado/$1";
$route['condominio-san-jorge/fotogaleria'] = "project/gallery/$1";
$route['condominio-san-jorge/area-social'] = "project/social/$1";
$route['condominio-san-jorge/thank-you'] = "search/thankYouPage";

$route['sevilla-pinatar'] = "project/index/$1";
$route['sevilla-pinatar/precio'] = "project/prices/$1";
$route['sevilla-pinatar/acabado'] = "project/acabado/$1";
$route['sevilla-pinatar/fotogaleria'] = "project/gallery/$1";
$route['sevilla-pinatar/area-social'] = "project/social/$1";
$route['sevilla-pinatar/thank-you'] = "search/thankYouPage";

$route['torre-meridian'] = "project/index/$1";
$route['torre-meridian/precio'] = "project/prices/$1";
$route['torre-meridian/acabado'] = "project/acabado/$1";
$route['torre-meridian/fotogaleria'] = "project/gallery/$1";
$route['torre-meridian/area-social'] = "project/social/$1";
$route['torre-meridian/thank-you'] = "search/thankYouPage";

$route['edificio-tannat'] = "project/index/$1";
$route['edificio-tannat/precio'] = "project/prices/$1";
$route['edificio-tannat/acabado'] = "project/acabado/$1";
$route['edificio-tannat/fotogaleria'] = "project/gallery/$1";
$route['edificio-tannat/area-social'] = "project/social/$1";
$route['edificio-tannat/thank-you'] = "search/thankYouPage";

$route['condominio-brisas-del-norte'] = "project/index/$1";
$route['condominio-brisas-del-norte/precio'] = "project/prices/$1";
$route['condominio-brisas-del-norte/acabado'] = "project/acabado/$1";
$route['condominio-brisas-del-norte/fotogaleria'] = "project/gallery/$1";
$route['condominio-brisas-del-norte/area-social'] = "project/social/$1";
$route['condominio-brisas-del-norte/thank-you'] = "search/thankYouPage";

$route['condominio-las-tacuaras'] = "project/index/$1";
$route['condominio-las-tacuaras/precio'] = "project/prices/$1";
$route['condominio-las-tacuaras/acabado'] = "project/acabado/$1";
$route['condominio-las-tacuaras/fotogaleria'] = "project/gallery/$1";
$route['condominio-las-tacuaras/area-social'] = "project/social/$1";
$route['condominio-las-tacuaras/thank-you'] = "search/thankYouPage";

$route['urbanizacion-palos-verdes'] = "project/index/$1";
$route['urbanizacion-palos-verdes/precio'] = "project/prices/$1";
$route['urbanizacion-palos-verdes/acabado'] = "project/acabado/$1";
$route['urbanizacion-palos-verdes/fotogaleria'] = "project/gallery/$1";
$route['urbanizacion-palos-verdes/area-social'] = "project/social/$1";
$route['urbanizacion-palos-verdes/thank-you'] = "search/thankYouPage";

$route['urbanizacion-cavyar'] = "project/index/$1";
$route['urbanizacion-cavyar/precio'] = "project/prices/$1";
$route['urbanizacion-cavyar/acabado'] = "project/acabado/$1";
$route['urbanizacion-cavyar/fotogaleria'] = "project/gallery/$1";
$route['urbanizacion-cavyar/area-social'] = "project/social/$1";
$route['urbanizacion-cavyar/thank-you'] = "search/thankYouPage";

$route['torres-praga'] = "project/index/$1";
$route['torres-praga/precio'] = "project/prices/$1";
$route['torres-praga/acabado'] = "project/acabado/$1";
$route['torres-praga/fotogaleria'] = "project/gallery/$1";
$route['torres-praga/area-social'] = "project/social/$1";
$route['torres-praga/thank-you'] = "search/thankYouPage";

$route['urbanizacion-tupa-ruete'] = "project/index/$1";
$route['urbanizacion-tupa-ruete/precio'] = "project/prices/$1";
$route['urbanizacion-tupa-ruete/acabado'] = "project/acabado/$1";
$route['urbanizacion-tupa-ruete/fotogaleria'] = "project/gallery/$1";
$route['urbanizacion-tupa-ruete/area-social'] = "project/social/$1";
$route['urbanizacion-tupa-ruete/thank-you'] = "search/thankYouPage";

$route['smart-studio-isuto-10'] = "project/index/$1";
$route['smart-studio-isuto-10/precio'] = "project/prices/$1";
$route['smart-studio-isuto-10/acabado'] = "project/acabado/$1";
$route['smart-studio-isuto-10/fotogaleria'] = "project/gallery/$1";
$route['smart-studio-isuto-10/area-social'] = "project/social/$1";
$route['smart-studio-isuto-10/thank-you'] = "search/thankYouPage";

$route['smart-studio-equipetrol-10'] = "project/index/$1";
$route['smart-studio-equipetrol-10/precio'] = "project/prices/$1";
$route['smart-studio-equipetrol-10/acabado'] = "project/acabado/$1";
$route['smart-studio-equipetrol-10/fotogaleria'] = "project/gallery/$1";
$route['smart-studio-equipetrol-10/area-social'] = "project/social/$1";
$route['smart-studio-equipetrol-10/thank-you'] = "search/thankYouPage";

$route['altos-del-sur'] = "project/index/$1";
$route['altos-del-sur/precio'] = "project/prices/$1";
$route['altos-del-sur/acabado'] = "project/acabado/$1";
$route['altos-del-sur/fotogaleria'] = "project/gallery/$1";
$route['altos-del-sur/area-social'] = "project/social/$1";
$route['altos-del-sur/thank-you'] = "search/thankYouPage";

$route['torres-de-la-sierra'] = "project/index/$1";
$route['torres-de-la-sierra/precio'] = "project/prices/$1";
$route['torres-de-la-sierra/acabado'] = "project/acabado/$1";
$route['torres-de-la-sierra/fotogaleria'] = "project/gallery/$1";
$route['torres-de-la-sierra/area-social'] = "project/social/$1";
$route['torres-de-la-sierra/thank-you'] = "search/thankYouPage";

$route['villa-doria-pamphili'] = "project/index/$1";
$route['villa-doria-pamphili/precio'] = "project/prices/$1";
$route['villa-doria-pamphili/acabado'] = "project/acabado/$1";
$route['villa-doria-pamphili/fotogaleria'] = "project/gallery/$1";
$route['villa-doria-pamphili/area-social'] = "project/social/$1";
$route['villa-doria-pamphili/thank-you'] = "search/thankYouPage";

$route['london-tower'] = "project/index/$1";
$route['london-tower/precio'] = "project/prices/$1";
$route['london-tower/acabado'] = "project/acabado/$1";
$route['london-tower/fotogaleria'] = "project/gallery/$1";
$route['london-tower/area-social'] = "project/social/$1";
$route['london-tower/thank-you'] = "search/thankYouPage";

$route['condominio-asai'] = "project/index/$1";
$route['condominio-asai/precio'] = "project/prices/$1";
$route['condominio-asai/acabado'] = "project/acabado/$1";
$route['condominio-asai/fotogaleria'] = "project/gallery/$1";
$route['condominio-asai/area-social'] = "project/social/$1";
$route['condominio-asai/thank-you'] = "search/thankYouPage";

$route['el-palacio-del-pescado'] = "project/index/$1";
$route['el-palacio-del-pescado/precio'] = "project/prices/$1";
$route['el-palacio-del-pescado/acabado'] = "project/acabado/$1";
$route['el-palacio-del-pescado/fotogaleria'] = "project/gallery/$1";
$route['el-palacio-del-pescado/area-social'] = "project/social/$1";
$route['el-palacio-del-pescado/thank-you'] = "search/thankYouPage";

$route['jade-del-urubo'] = "project/index/$1";
$route['jade-del-urubo/precio'] = "project/prices/$1";
$route['jade-del-urubo/acabado'] = "project/acabado/$1";
$route['jade-del-urubo/fotogaleria'] = "project/gallery/$1";
$route['jade-del-urubo/area-social'] = "project/social/$1";
$route['jade-del-urubo/thank-you'] = "search/thankYouPage";

$route['condominio-bella-grecia'] = "project/index/$1";
$route['condominio-bella-grecia/precio'] = "project/prices/$1";
$route['condominio-bella-grecia/acabado'] = "project/acabado/$1";
$route['condominio-bella-grecia/fotogaleria'] = "project/gallery/$1";
$route['condominio-bella-grecia/area-social'] = "project/social/$1";
$route['condominio-bella-grecia/thank-you'] = "search/thankYouPage";

$route['eco-studio'] = "project/index/$1";
$route['eco-studio/precio'] = "project/prices/$1";
$route['eco-studio/acabado'] = "project/acabado/$1";
$route['eco-studio/fotogaleria'] = "project/gallery/$1";
$route['eco-studio/area-social'] = "project/social/$1";
$route['eco-studio/thank-you'] = "search/thankYouPage";

$route['condominio-laguna-maggiore'] = "project/index/$1";
$route['condominio-laguna-maggiore/precio'] = "project/prices/$1";
$route['condominio-laguna-maggiore/acabado'] = "project/acabado/$1";
$route['condominio-laguna-maggiore/fotogaleria'] = "project/gallery/$1";
$route['condominio-laguna-maggiore/area-social'] = "project/social/$1";
$route['condominio-laguna-maggiore/thank-you'] = "search/thankYouPage";

$route['jardines-del-remanso-1'] = "project/index/$1";
$route['jardines-del-remanso-1/precio'] = "project/prices/$1";
$route['jardines-del-remanso-1/acabado'] = "project/acabado/$1";
$route['jardines-del-remanso-1/fotogaleria'] = "project/gallery/$1";
$route['jardines-del-remanso-1/area-social'] = "project/social/$1";
$route['jardines-del-remanso-1/thank-you'] = "search/thankYouPage";

$route['torre-link'] = "project/index/$1";
$route['torre-link/precio'] = "project/prices/$1";
$route['torre-link/acabado'] = "project/acabado/$1";
$route['torre-link/fotogaleria'] = "project/gallery/$1";
$route['torre-link/area-social'] = "project/social/$1";
$route['torre-link/thank-you'] = "search/thankYouPage";

$route['urbanizacion-riviera-norte'] = "project/index/$1";
$route['urbanizacion-riviera-norte/precio'] = "project/prices/$1";
$route['urbanizacion-riviera-norte/acabado'] = "project/acabado/$1";
$route['urbanizacion-riviera-norte/fotogaleria'] = "project/gallery/$1";
$route['urbanizacion-riviera-norte/area-social'] = "project/social/$1";
$route['urbanizacion-riviera-norte/thank-you'] = "search/thankYouPage";

$route['reserva-esmeralda'] = "project/index/$1";
$route['reserva-esmeralda/precio'] = "project/prices/$1";
$route['reserva-esmeralda/acabado'] = "project/acabado/$1";
$route['reserva-esmeralda/fotogaleria'] = "project/gallery/$1";
$route['reserva-esmeralda/area-social'] = "project/social/$1";
$route['reserva-esmeralda/thank-you'] = "search/thankYouPage";

$route['santa-cruz-de-la-colina'] = "project/index/$1";
$route['santa-cruz-de-la-colina/precio'] = "project/prices/$1";
$route['santa-cruz-de-la-colina/acabado'] = "project/acabado/$1";
$route['santa-cruz-de-la-colina/fotogaleria'] = "project/gallery/$1";
$route['santa-cruz-de-la-colina/area-social'] = "project/social/$1";
$route['santa-cruz-de-la-colina/thank-you'] = "search/thankYouPage";

$route['torre-equis'] = "project/index/$1";
$route['torre-equis/precio'] = "project/prices/$1";
$route['torre-equis/acabado'] = "project/acabado/$1";
$route['torre-equis/fotogaleria'] = "project/gallery/$1";
$route['torre-equis/area-social'] = "project/social/$1";
$route['torre-equis/thank-you'] = "search/thankYouPage";

$route['panorama-concepts'] = "project/index/$1";
$route['panorama-concepts/precio'] = "project/prices/$1";
$route['panorama-concepts/acabado'] = "project/acabado/$1";
$route['panorama-concepts/fotogaleria'] = "project/gallery/$1";
$route['panorama-concepts/area-social'] = "project/social/$1";
$route['panorama-concepts/thank-you'] = "search/thankYouPage";

$route['los-tajibos-del-urubo'] = "project/index/$1";
$route['los-tajibos-del-urubo/precio'] = "project/prices/$1";
$route['los-tajibos-del-urubo/acabado'] = "project/acabado/$1";
$route['los-tajibos-del-urubo/fotogaleria'] = "project/gallery/$1";
$route['los-tajibos-del-urubo/area-social'] = "project/social/$1";
$route['los-tajibos-del-urubo/thank-you'] = "search/thankYouPage";

$route['condominio-los-almendros'] = "project/index/$1";
$route['condominio-los-almendros/precio'] = "project/prices/$1";
$route['condominio-los-almendros/acabado'] = "project/acabado/$1";
$route['condominio-los-almendros/fotogaleria'] = "project/gallery/$1";
$route['condominio-los-almendros/area-social'] = "project/social/$1";
$route['condominio-los-almendros/thank-you'] = "search/thankYouPage";

$route['mirador-del-urubo'] = "project/index/$1";
$route['mirador-del-urubo/precio'] = "project/prices/$1";
$route['mirador-del-urubo/acabado'] = "project/acabado/$1";
$route['mirador-del-urubo/fotogaleria'] = "project/gallery/$1";
$route['mirador-del-urubo/area-social'] = "project/social/$1";
$route['mirador-del-urubo/thank-you'] = "search/thankYouPage";

$route['mirador-del-norte'] = "project/index/$1";
$route['mirador-del-norte/precio'] = "project/prices/$1";
$route['mirador-del-norte/acabado'] = "project/acabado/$1";
$route['mirador-del-norte/fotogaleria'] = "project/gallery/$1";
$route['mirador-del-norte/area-social'] = "project/social/$1";
$route['mirador-del-norte/thank-you'] = "search/thankYouPage";

$route['le-parc-costanera-club'] = "project/index/$1";
$route['le-parc-costanera-club/precio'] = "project/prices/$1";
$route['le-parc-costanera-club/acabado'] = "project/acabado/$1";
$route['le-parc-costanera-club/fotogaleria'] = "project/gallery/$1";
$route['le-parc-costanera-club/area-social'] = "project/social/$1";
$route['le-parc-costanera-club/thank-you'] = "search/thankYouPage";

$route['oficentro'] = "project/index/$1";
$route['oficentro/precio'] = "project/prices/$1";
$route['oficentro/acabado'] = "project/acabado/$1";
$route['oficentro/fotogaleria'] = "project/gallery/$1";
$route['oficentro/area-social'] = "project/social/$1";
$route['oficentro/thank-you'] = "search/thankYouPage";

$route['condominio-curupau-roca'] = "project/index/$1";
$route['condominio-curupau-roca/precio'] = "project/prices/$1";
$route['condominio-curupau-roca/acabado'] = "project/acabado/$1";
$route['condominio-curupau-roca/fotogaleria'] = "project/gallery/$1";
$route['condominio-curupau-roca/area-social'] = "project/social/$1";
$route['condominio-curupau-roca/thank-you'] = "search/thankYouPage";

$route['portofino-v'] = "project/index/$1";
$route['portofino-v/precio'] = "project/prices/$1";
$route['portofino-v/acabado'] = "project/acabado/$1";
$route['portofino-v/fotogaleria'] = "project/gallery/$1";
$route['portofino-v/area-social'] = "project/social/$1";
$route['portofino-v/thank-you'] = "search/thankYouPage";

$route['edificio-top-center'] = "project/index/$1";
$route['edificio-top-center/precio'] = "project/prices/$1";
$route['edificio-top-center/acabado'] = "project/acabado/$1";
$route['edificio-top-center/fotogaleria'] = "project/gallery/$1";
$route['edificio-top-center/area-social'] = "project/social/$1";
$route['edificio-top-center/thank-you'] = "search/thankYouPage";

$route['edificio-santa-fe'] = "project/index/$1";
$route['edificio-santa-fe/precio'] = "project/prices/$1";
$route['edificio-santa-fe/acabado'] = "project/acabado/$1";
$route['edificio-santa-fe/fotogaleria'] = "project/gallery/$1";
$route['edificio-santa-fe/area-social'] = "project/social/$1";
$route['edificio-santa-fe/thank-you'] = "search/thankYouPage";

$route['condominio-san-andres'] = "project/index/$1";
$route['condominio-san-andres/precio'] = "project/prices/$1";
$route['condominio-san-andres/acabado'] = "project/acabado/$1";
$route['condominio-san-andres/fotogaleria'] = "project/gallery/$1";
$route['condominio-san-andres/area-social'] = "project/social/$1";
$route['condominio-san-andres/thank-you'] = "search/thankYouPage";

$route['residencias-de-playa'] = "project/index/$1";
$route['residencias-de-playa/precio'] = "project/prices/$1";
$route['residencias-de-playa/acabado'] = "project/acabado/$1";
$route['residencias-de-playa/fotogaleria'] = "project/gallery/$1";
$route['residencias-de-playa/area-social'] = "project/social/$1";
$route['residencias-de-playa/thank-you'] = "search/thankYouPage";

$route['condominio-jardines-del-norte-iv'] = "project/index/$1";
$route['condominio-jardines-del-norte-iv/precio'] = "project/prices/$1";
$route['condominio-jardines-del-norte-iv/acabado'] = "project/acabado/$1";
$route['condominio-jardines-del-norte-iv/fotogaleria'] = "project/gallery/$1";
$route['condominio-jardines-del-norte-iv/area-social'] = "project/social/$1";
$route['condominio-jardines-del-norte-iv/thank-you'] = "search/thankYouPage";

$route['condominio-marzenia'] = "project/index/$1";
$route['condominio-marzenia/precio'] = "project/prices/$1";
$route['condominio-marzenia/acabado'] = "project/acabado/$1";
$route['condominio-marzenia/fotogaleria'] = "project/gallery/$1";
$route['condominio-marzenia/area-social'] = "project/social/$1";
$route['condominio-marzenia/thank-you'] = "search/thankYouPage";

$route['panorama-nuovo'] = "project/index/$1";
$route['panorama-nuovo/precio'] = "project/prices/$1";
$route['panorama-nuovo/acabado'] = "project/acabado/$1";
$route['panorama-nuovo/fotogaleria'] = "project/gallery/$1";
$route['panorama-nuovo/area-social'] = "project/social/$1";
$route['panorama-nuovo/thank-you'] = "search/thankYouPage";

$route['torre-urucu'] = "project/index/$1";
$route['torre-urucu/precio'] = "project/prices/$1";
$route['torre-urucu/acabado'] = "project/acabado/$1";
$route['torre-urucu/fotogaleria'] = "project/gallery/$1";
$route['torre-urucu/area-social'] = "project/social/$1";
$route['torre-urucu/thank-you'] = "search/thankYouPage";

$route['porto-fino-beni'] = "project/index/$1";
$route['porto-fino-beni/precio'] = "project/prices/$1";
$route['porto-fino-beni/acabado'] = "project/acabado/$1";
$route['porto-fino-beni/fotogaleria'] = "project/gallery/$1";
$route['porto-fino-beni/area-social'] = "project/social/$1";
$route['porto-fino-beni/thank-you'] = "search/thankYouPage";

$route['condominio-solaris'] = "project/index/$1";
$route['condominio-solaris/precio'] = "project/prices/$1";
$route['condominio-solaris/acabado'] = "project/acabado/$1";
$route['condominio-solaris/fotogaleria'] = "project/gallery/$1";
$route['condominio-solaris/area-social'] = "project/social/$1";
$route['condominio-solaris/thank-you'] = "search/thankYouPage";

$route['glaz-isuto'] = "project/index/$1";
$route['glaz-isuto/precio'] = "project/prices/$1";
$route['glaz-isuto/acabado'] = "project/acabado/$1";
$route['glaz-isuto/fotogaleria'] = "project/gallery/$1";
$route['glaz-isuto/area-social'] = "project/social/$1";
$route['glaz-isuto/thank-you'] = "search/thankYouPage";

$route['flamboyan-casas'] = "project/index/$1";
$route['flamboyan-casas/precio'] = "project/prices/$1";
$route['flamboyan-casas/acabado'] = "project/acabado/$1";
$route['flamboyan-casas/fotogaleria'] = "project/gallery/$1";
$route['flamboyan-casas/area-social'] = "project/social/$1";
$route['flamboyan-casas/thank-you'] = "search/thankYouPage";

$route['nano-by-smart-studio'] = "project/index/$1";
$route['nano-by-smart-studio/precio'] = "project/prices/$1";
$route['nano-by-smart-studio/acabado'] = "project/acabado/$1";
$route['nano-by-smart-studio/fotogaleria'] = "project/gallery/$1";
$route['nano-by-smart-studio/area-social'] = "project/social/$1";
$route['nano-by-smart-studio/thank-you'] = "search/thankYouPage";

$route['porto-fino-ovidio'] = "project/index/$1";
$route['porto-fino-ovidio/precio'] = "project/prices/$1";
$route['porto-fino-ovidio/acabado'] = "project/acabado/$1";
$route['porto-fino-ovidio/fotogaleria'] = "project/gallery/$1";
$route['porto-fino-ovidio/area-social'] = "project/social/$1";
$route['porto-fino-ovidio/thank-you'] = "search/thankYouPage";

$route['tower-evolution'] = "project/index/$1";
$route['tower-evolution/precio'] = "project/prices/$1";
$route['tower-evolution/acabado'] = "project/acabado/$1";
$route['tower-evolution/fotogaleria'] = "project/gallery/$1";
$route['tower-evolution/area-social'] = "project/social/$1";
$route['tower-evolution/thank-you'] = "search/thankYouPage";

$route['itauba-norte'] = "project/index/$1";
$route['itauba-norte/precio'] = "project/prices/$1";
$route['itauba-norte/acabado'] = "project/acabado/$1";
$route['itauba-norte/fotogaleria'] = "project/gallery/$1";
$route['itauba-norte/area-social'] = "project/social/$1";
$route['itauba-norte/thank-you'] = "search/thankYouPage";

$route['grigota-center'] = "project/index/$1";
$route['grigota-center/precio'] = "project/prices/$1";
$route['grigota-center/acabado'] = "project/acabado/$1";
$route['grigota-center/fotogaleria'] = "project/gallery/$1";
$route['grigota-center/area-social'] = "project/social/$1";
$route['grigota-center/thank-you'] = "search/thankYouPage";

$route['ocho-casas'] = "project/index/$1";
$route['ocho-casas/precio'] = "project/prices/$1";
$route['ocho-casas/acabado'] = "project/acabado/$1";
$route['ocho-casas/fotogaleria'] = "project/gallery/$1";
$route['ocho-casas/area-social'] = "project/social/$1";
$route['ocho-casas/thank-you'] = "search/thankYouPage";

$route['edificio-san-miguel'] = "project/index/$1";
$route['edificio-san-miguel/precio'] = "project/prices/$1";
$route['edificio-san-miguel/acabado'] = "project/acabado/$1";
$route['edificio-san-miguel/fotogaleria'] = "project/gallery/$1";
$route['edificio-san-miguel/area-social'] = "project/social/$1";
$route['edificio-san-miguel/thank-you'] = "search/thankYouPage";

$route['in-towers'] = "project/index/$1";
$route['in-towers/precio'] = "project/prices/$1";
$route['in-towers/acabado'] = "project/acabado/$1";
$route['in-towers/fotogaleria'] = "project/gallery/$1";
$route['in-towers/area-social'] = "project/social/$1";
$route['in-towers/thank-you'] = "search/thankYouPage";

$route['one-soul-by-smart-studio'] = "project/index/$1";
$route['one-soul-by-smart-studio/precio'] = "project/prices/$1";
$route['one-soul-by-smart-studio/acabado'] = "project/acabado/$1";
$route['one-soul-by-smart-studio/fotogaleria'] = "project/gallery/$1";
$route['one-soul-by-smart-studio/area-social'] = "project/social/$1";
$route['one-soul-by-smart-studio/thank-you'] = "search/thankYouPage";

$route['park-city'] = "project/index/$1";
$route['park-city/precio'] = "project/prices/$1";
$route['park-city/acabado'] = "project/acabado/$1";
$route['park-city/fotogaleria'] = "project/gallery/$1";
$route['park-city/area-social'] = "project/social/$1";
$route['park-city/thank-you'] = "search/thankYouPage";

$route['brickell-1'] = "project/index/$1";
$route['brickell-1/precio'] = "project/prices/$1";
$route['brickell-1/acabado'] = "project/acabado/$1";
$route['brickell-1/fotogaleria'] = "project/gallery/$1";
$route['brickell-1/area-social'] = "project/social/$1";
$route['brickell-1/thank-you'] = "search/thankYouPage";

$route['urbanizacion-la-herradura'] = "project/index/$1";
$route['urbanizacion-la-herradura/precio'] = "project/prices/$1";
$route['urbanizacion-la-herradura/acabado'] = "project/acabado/$1";
$route['urbanizacion-la-herradura/fotogaleria'] = "project/gallery/$1";
$route['urbanizacion-la-herradura/area-social'] = "project/social/$1";
$route['urbanizacion-la-herradura/thank-you'] = "search/thankYouPage";

$route['urbanizacion-viru-viru'] = "project/index/$1";
$route['urbanizacion-viru-viru/precio'] = "project/prices/$1";
$route['urbanizacion-viru-viru/acabado'] = "project/acabado/$1";
$route['urbanizacion-viru-viru/fotogaleria'] = "project/gallery/$1";
$route['urbanizacion-viru-viru/area-social'] = "project/social/$1";
$route['urbanizacion-viru-viru/thank-you'] = "search/thankYouPage";

$route['edificio-torre-roble'] = "project/index/$1";
$route['edificio-torre-roble/precio'] = "project/prices/$1";
$route['edificio-torre-roble/acabado'] = "project/acabado/$1";
$route['edificio-torre-roble/fotogaleria'] = "project/gallery/$1";
$route['edificio-torre-roble/area-social'] = "project/social/$1";
$route['edificio-torre-roble/thank-you'] = "search/thankYouPage";

$route['edificio-praga'] = "project/index/$1";
$route['edificio-praga/precio'] = "project/prices/$1";
$route['edificio-praga/acabado'] = "project/acabado/$1";
$route['edificio-praga/fotogaleria'] = "project/gallery/$1";
$route['edificio-praga/area-social'] = "project/social/$1";
$route['edificio-praga/thank-you'] = "search/thankYouPage";

$route['condominio-jerico'] = "project/index/$1";
$route['condominio-jerico/precio'] = "project/prices/$1";
$route['condominio-jerico/acabado'] = "project/acabado/$1";
$route['condominio-jerico/fotogaleria'] = "project/gallery/$1";
$route['condominio-jerico/area-social'] = "project/social/$1";
$route['condominio-jerico/thank-you'] = "search/thankYouPage";

$route['condominio-jardines-del-norte-v'] = "project/index/$1";
$route['condominio-jardines-del-norte-v/precio'] = "project/prices/$1";
$route['condominio-jardines-del-norte-v/acabado'] = "project/acabado/$1";
$route['condominio-jardines-del-norte-v/fotogaleria'] = "project/gallery/$1";
$route['condominio-jardines-del-norte-v/area-social'] = "project/social/$1";
$route['condominio-jardines-del-norte-v/thank-you'] = "search/thankYouPage";

$route['palma-mallorca'] = "project/index/$1";
$route['palma-mallorca/precio'] = "project/prices/$1";
$route['palma-mallorca/acabado'] = "project/acabado/$1";
$route['palma-mallorca/fotogaleria'] = "project/gallery/$1";
$route['palma-mallorca/area-social'] = "project/social/$1";
$route['palma-mallorca/thank-you'] = "search/thankYouPage";

$route['las-palmas-del-urubo-iii'] = "project/index/$1";
$route['las-palmas-del-urubo-iii/precio'] = "project/prices/$1";
$route['las-palmas-del-urubo-iii/acabado'] = "project/acabado/$1";
$route['las-palmas-del-urubo-iii/fotogaleria'] = "project/gallery/$1";
$route['las-palmas-del-urubo-iii/area-social'] = "project/social/$1";
$route['las-palmas-del-urubo-iii/thank-you'] = "search/thankYouPage";
// FIN PROYECTOS --------------------------------------------

//$route['(:any)/(:any)/(:any)'] = "inmueble/index/$1/$2/$3";*/
$route['default_controller'] = "home";
$route['404_override'] = '';






// CIUDADES --------------------------------------------------------------

$route["santa-cruz-de-la-sierra/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["la-paz/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["cochabamba/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["tarija/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["sucre/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["oruro/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["potosi/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["cobija/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["trinidad/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["cotoca/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["porongo-ayacucho/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["la-guardia/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["el-torno/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["warnes/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["okinawa/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["san-ignacio/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["san-miguel/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["san-rafael/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["buena-vista/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["san-carlos/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["yapacani/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["san-juan/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["san-jose/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["pailon/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["robore/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["portachuelo/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["santa-rosa-del-sara/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["colpa-belgica/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["lagunillas/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["charagua/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["cabezas/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["cuevo/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["gutierrez/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["camiri/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["boyuibe/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["vallegrande/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["el-trigal/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["moro-moro/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["postrervalle/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["pucara/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["samaipata/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["pampa-grande/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["mairana/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["quirusillas/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["montero/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["gral-saavedra/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["mineros/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["fernandez-alonso/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["san-pedro/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["concepcion/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["san-javier/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["san-ramon/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["san-julian/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["san-antonio-de-lomerio/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["cuatro-canadas/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["san-matias/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["comarapa/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["saipina/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["puerto-suarez/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["puerto-quijarro/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["ascencion-de-guarayos/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["urubicha/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["el-puente/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["palca/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["mecapaca/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["achocalla/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["el-alto/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["achacachi/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["ancoraimes/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["coro-coro/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["caquiaviri/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["calacoto/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["comanche/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["charana/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["waldo-ballivian/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["nazacara-de-pacajes/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["santiago-de-callapa/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["puerto-acosta/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["mocomoco/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["puerto-carabuco/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["chuma/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["ayata/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["aucapata/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["sorata/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["guanay/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["tacacoma/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["quiabaya/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["combaya/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["tipuani/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["mapiri/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["teoponte/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["apolo/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["pelechuco/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["viacha/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["guaqui/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["tiahuanacu/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["desaguadero/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["san-andres-de-machaca/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["jesus-de-machaca/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["taraco/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["luribay/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["malla/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["cairoma/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["inquisivi/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["quime/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["cajuata/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["colquiri/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["ichoca/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["licoma-pampa/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["chulumani/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["irupana/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["yanacachi/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["palos-blancos/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["la-asunta/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["pucarani/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["laja/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["batallas/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["puerto-perez/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["sica-sica/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["umala/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["ayo-ayo/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["calamarca/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["patacamaya/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["colquencha/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["collana/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["coroico/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["coripata/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["ixiamas/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["san-buenaventura/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["gral-juan-jose-perez-charazani/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["curva/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["copacabana/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["san-pedro-de-tiquina/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["tito-yupanqui/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["san-pedro-de-curahuara/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["papel-pampa/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["chacarilla/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["santiago-de-machaca/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["catacora/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["caranavi/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["aiquile/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["pasorapa/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["omereque/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["independencia-ayopaya/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["morochata/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["tarata/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["anzaldo/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["arbieto/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["sacabamba/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["arani/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["vacas/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["arque/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["tacopaya/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["capinota/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["santivanez/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["sicaya/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["cliza/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["toco/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["tolata/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["quillacollo/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["sipe-sipe/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["tiquipaya/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["vinto/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["cercado/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["tiquipaya/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["cona-cona/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["las-cuadras/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["america-este/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["arocagua/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["colcapirhua/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["sacaba/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["colomi/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["villa-tunari/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["tapacari/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["totora/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["pojo/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["pocona/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["chimore/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["puerto-villarroel/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["entre-rios-bulo-bulo/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["mizque/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["vila-vila/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["alalay/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["punata/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["villa-rivero/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["san-benito/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["tacachi/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["cuchumuela-villa-gualberto-villarroel/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["bolivar/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["tiraque/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["padcaya/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["bermejo/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["yacuiba/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["carapari/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["villamontes/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["uriondo/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["yunchara/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["san-lorenzo/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["el-puente/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["entre-rios/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["yotala/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["poroma/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["villa-azurduy/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["tarvita/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["villa-zudanez/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["presto/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["villa-mojocoya/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["icla/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["padilla/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["tomina/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["sopachuy/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["villa-alcala/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["el-villar/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["monteagudo/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["san-pablo-de-huacareta/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["tarabuco/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["yamparaez/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["camargo/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["san-lucas/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["incahuasi/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["villa-serrano/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["villa-abecia-camataqui/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["culpina/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["las-carreras/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["villa-vaca-guzman-muyupampa/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["huacaya/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["machareti/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["caracollo/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["el-choro/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["soracachi-paria/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["challapata/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["santuario-de-quillacas/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["corque/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["choquecota/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["curahuara-de-carangas/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["turco/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["huachacalla/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["escara/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["cruz-de-machacamarca/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["yunguyo-del-litoral/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["esmeralda/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["villa-poopo/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["pazna/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["antequera/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["villa-huanuni/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["machacamarca/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["salinas-de-garcia-mendoza/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["pampa-aullagas/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["sabaya/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["coipasa/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["chipaya/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["toledo/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["eucaliptus/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["belen-de-andamarca/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["totora/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["santiago-de-huari/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["la-rivera/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["todos-santos/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["carangas/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["santiago-de-huayllamarca/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["tinguipaya/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["villa-de-yocalla/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["belen-de-urmiri/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["uncia/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["chayanta/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["llallagua/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["betanzos/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["chaqui/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["tacobamba/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["colquechaca/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["ravelo/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["pocoata/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["ocuri/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["san-pedro-de-buena-vista/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["toro-toro/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["cotagaita/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["vitichi/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["sacaca/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["caripuyo/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["tupiza/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["atocha/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["colcha-k/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["san-pedro-de-quemes/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["san-pablo-de-lipez/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["mojinete/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["san-antonio-de-esmoruco/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["puna/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["caiza/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["uyuni/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["tomave/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["porco/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["arampampa/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["acasio/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["llica/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["tahua/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["villazon/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["san-agustin/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["porvenir/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["bolpebra/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["bella-flor/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["puerto-rico/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["san-pedro/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["filadelfia/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["puerto-gonzalo-moreno/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["blanca-flor-san-lorenzo/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["el-sena/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["santa-rosa-del-abuna/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["humaita-ingavi/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["nueva-esperanza/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["villa-nueva-loma-alta/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["santos-mercado-reserva/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["san-javier/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["riberalta/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["guayaramerin/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["reyes/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["san-borja/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["santa-rosa/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["rurrenabaque/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["santa-ana/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["exaltacion/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["san-ignacio/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["loreto/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["san-andres/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["san-joaquin/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["san-ramon/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["puerto-siles/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["magdalena/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["baures/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";
$route["huacaraje/(:any)/(:any)"] = "inmueble/index/$1/$2/$3";

// FIN CIUDADES ----------------------------------------------------------



/* End of file routes.php */
/* Location: ./application/config/routes.php */