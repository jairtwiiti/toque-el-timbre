<?php
// LIBRERIA REQUERIDA DE NuSOAP
//
require_once('lib/nusoap.php');
/**
 * Funci�n para el consumo del WS cmeRegistroItem
 * @param type $datos
 * @param type $cuenta
 * @param type $webservice
 * @param type $consulta
 * @return type 
 */  
function wsRegistroPlan( $datos, $cuenta, $webservice, $consulta)
{
$proxyhost = isset($_POST['proxyhost']) ? $_POST['proxyhost'] : '';
$proxyport = isset($_POST['proxyport']) ? $_POST['proxyport'] : '';
$proxyusername = isset($_POST['proxyusername']) ? $_POST['proxyusername'] : '';
$proxypassword = isset($_POST['proxypassword']) ? $_POST['proxypassword'] : '';
$proxyhost = "";
$proxyport = "";
$client = new nusoap_client($webservice,'wsdl', $proxyhost, $proxyport, $proxyusername, $proxypassword);
$err = $client->getError();
if ($err) 
    {
    // Display the error
    echo '<h2>Constructor error</h2><pre>' . $err . '</pre>';
    // At this point, you know the call that follows will fail
    }
$param = array(
'datos' => $datos,
'cuenta' => $cuenta);
$result = $client->call($consulta, $param, '', '', true, true);
print_r($client);
return $result; 
}


/**
 * BEGIN cmeRegistroPlan
 */

    $TRANSACCION='A';
    $NOMBRE='JORGE SILES';
    $NIT = '3536662';
    $CODCLIENTE = '1';
    $CODEMPRESA = 1;
    $FECHAIN =20110907;
    $HORAIN =044810;
    $EMAIL = 'jorges@sintesis.com.bo';
    $MONEDA = 'BS';
    $CODRECAUDA = '118';
    $DESRECAUDA = 'TEST WS';
    $FECHAVEN =20110917;
    $HORAVEN =044810;
    $COD_PRODUCTO = '000001';
    
    $NROPAGO = 1;
    $MONTOPAGO = 11.1;
    $DESCPAGO = "CUOTA1";
    $MONTOCF = 11.1; 
    $NOBREFAC = "SILES";
    $NITFAC = "35366662";
    
    $DPLANILLAS =array('DPlanilla'=>Array('numeroPago'=>$NROPAGO,'montoPago'=>$MONTOPAGO, 'descripcion'=>$DESCPAGO, 'montoCreditoFiscal'=>$MONTOCF, 'nombreFactura'=>$NOBREFAC, 'nitFactura'=>$NITFAC));//
    
    $datos = array('transaccion'=>$TRANSACCION,'nombre'=>$NOMBRE,'nit_CI_cliente'=>$NIT,'codigoCliente'=>$CODCLIENTE,'codigoEmpresa'=>$CODEMPRESA, 'fecha'=>$FECHAIN, 'hora'=>$HORAIN, 'correoElectronico'=>$EMAIL, 'moneda'=>$MONEDA, 'codigoRecaudacion'=>$CODRECAUDA,'descripcionRecaudacion'=>$DESRECAUDA,'fechaVencimiento'=>$FECHAVEN, 'horaVencimiento'=>$HORAVEN,'codigoProducto'=>$COD_PRODUCTO, 'planillas'=>$DPLANILLAS);
    //print_r($datos);
    $result = wsRegistroPlan($datos, 'wstoqueeltimbre','https://web.sintesis.com.bo/wsComelec/service/Comelec.jws?WSDL=', 'cmeRegistroPlan');
    echo "<pre>";
	print_r($result);
	echo "</pre>";
/**
 * END cmeRegistroPlan
 */
?>
