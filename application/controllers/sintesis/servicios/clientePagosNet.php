<?php
// LIBRERIA REQUERIDA DE NuSOAP
//
require_once( 'lib/nusoap.php' );
/**
 * Funcion para el consumo del WS cmeRegistroPlan
 * @param type $datos
 * @param type $cuenta
 * @param type $password
 * @param type $webservice
 * @param type $consulta
 * @return type 
 */  
// En la llamada el metodo se adiciono el campo password
/*function wsRegistroPlan( $datos, $cuenta, $webservice, $consulta, &$errorSintesis) {
	$proxyhost = isset($_POST['proxyhost']) ? $_POST['proxyhost'] : '';
	$proxyport = isset($_POST['proxyport']) ? $_POST['proxyport'] : '';
	$proxyusername = isset($_POST['proxyusername']) ? $_POST['proxyusername'] : '';
	$proxypassword = isset($_POST['proxypassword']) ? $_POST['proxypassword'] : '';
	$proxyhost = "";
	$proxyport = "";
	
	$client = new nusoap_client($webservice,'wsdl', $proxyhost, $proxyport, $proxyusername, $proxypassword);
    $err = $client->getError();
    if ($err){
       // Display the error
       //echo '<div style="display:none"><h2>Constructor error</h2><pre>' . $err . '</pre></div>';
       $errorSintesis = "Si";
       // At this point, you know the call that follows will fail
    }

    $param = array(
        'datos' => $datos,
        'cuenta' => $cuenta
    );
    
    $result = $client->call($consulta, $param, '', '', true, true);
    $err = $client->getError();
    if ($err){
       //echo '<h2>error al invocar</h2><pre>' . $err . '</pre>';
       $errorSintesis = "Si";
    }
    
    return $result; 
}*/

function wsRegistroPlan( $datos, $cuenta, $webservice, $consulta, &$errorSintesis)
{
    $proxyhost = isset($_POST['proxyhost']) ? $_POST['proxyhost'] : '';
    $proxyport = isset($_POST['proxyport']) ? $_POST['proxyport'] : '';
    $proxyusername = isset($_POST['proxyusername']) ? $_POST['proxyusername'] : '';
    $proxypassword = isset($_POST['proxypassword']) ? $_POST['proxypassword'] : '';
    $proxyhost = "";
    $proxyport = "";
    $client = new nusoap_client($webservice,'wsdl', $proxyhost, $proxyport, $proxyusername, $proxypassword);
    $err = $client->getError();
    if ($err)
    {
        $errorSintesis = "Si";
    }
    $param = array(
        'datos' => $datos,
        'cuenta' => $cuenta
    );
    $result = $client->call($consulta, $param, '', '', true, true);
    $err = $client->getError();
    if ($err){
        //echo '<h2>error al invocar</h2><pre>' . $err . '</pre>';
        $errorSintesis = "Si";
    }

    return $result;
}


/**
 * BEGIN cmeRegistroPlan
 */
 try{
 	
	$aux = explode(" ", $fecha);
	$fecha_format = str_replace("-","",$aux[0]);
	$hora_format = str_replace(":","",$aux[1]);
	
	$fecha_actual = $fecha_format;
	$hora_actual = $hora_format;
	$fecha_expiracion = date("Ymd", strtotime($aux[0]. " + 14 days"));
	
	/***************************************/
	$COD_PRODUCTO = '1';
	$CODCLIENTE = '1';
    $CODEMPRESA = 37;
	$CODRECAUDA = $pago_id;
	$EMAIL = $usuario->usu_email;
	$DESRECAUDA = 'RECAUDACION NRO: ' . $pago_id;
	$NIT = 0;
	$FECHAIN = $fecha_actual;
	$FECHAVEN = $fecha_expiracion;
	$HORAIN = $hora_actual;
	$HORAVEN = 235959;
	$MONEDA = 'BS';
	$NOMBRE = "";
	$PRECECOBRO = 'N';
	$TRANSACCION='A';

    
    $NROPAGO = 1;
	$MONTOPAGO = $MontoTotal;
	$MONTOCF = $MontoTotal; 
	$NOBREFAC = $nombre_fact;
	$NITFAC = $nit_fact;
	$DESCPAGO = $Descripcion;
    
    //$DPLANILLAS =array('DPlanilla'=>Array('numeroPago'=>$NROPAGO,'montoPago'=>$MONTOPAGO, 'descripcion'=>$DESCPAGO, 'montoCreditoFiscal'=>$MONTOCF, 'nombreFactura'=>$NOBREFAC, 'nitFactura'=>$NITFAC));//
    /*$cmeDetallePlan =array(
        'descripcion'=>$DESCPAGO,
        'montoCreditoFiscal'=>$MONTOCF,
        'montoPago'=>$MONTOPAGO,
        'nitFactura'=>$NITFAC,
        'nombreFactura'=>$NOBREFAC,
        'numeroPago'=>$NROPAGO
    );*/

    //$datos = array('transaccion'=>$TRANSACCION,'nombre'=>$NOMBRE,'nit_CI_cliente'=>$NIT,'codigoCliente'=>$CODCLIENTE,'codigoEmpresa'=>$CODEMPRESA, 'fecha'=>$FECHAIN, 'hora'=>$HORAIN, 'correoElectronico'=>$EMAIL, 'moneda'=>$MONEDA, 'codigoRecaudacion'=>$CODRECAUDA,'descripcionRecaudacion'=>$DESRECAUDA,'fechaVencimiento'=>$FECHAVEN, 'horaVencimiento'=>$HORAVEN,'codigoProducto'=>$COD_PRODUCTO, 'planillas'=>$DPLANILLAS);
    /*$cmeDatosPlan = array(
        'categoriaProducto'=>$COD_PRODUCTO,
        'codigoComprador'=>$CODCLIENTE,
        'codigoRecaudacion'=>$CODRECAUDA,
        'correoElectronico'=>$EMAIL,
        'descripcionRecaudacion'=>$DESRECAUDA,
        'documentoIdentidadComprador'=>$NIT,
        'fecha'=>$FECHAIN,
        'fechaVencimiento'=>$FECHAVEN,
        'hora'=>$HORAIN,
        'horaVencimiento'=>$HORAVEN,
        'moneda'=>$MONEDA,
        'nombreComprador'=>$NOMBRE,
        'planillas'=>$cmeDetallePlan,
        'precedenciaCobro' =>$PRECECOBRO,
        'transaccion'=>$TRANSACCION
    );*/

     $cmeDetallePlan = array(
         'DPlanilla'=>Array(
             'descripcion'=>$DESCPAGO,
             'montoPago'=>$MONTOPAGO,
             'montoCreditoFiscal'=>$MONTOCF,
             'nitFactura'=>$NITFAC,
             'nombreFactura'=>$NOBREFAC,
             'numeroPago'=>$NROPAGO
         )
     );
     $cmeDatosPlan = array(
         'codigoProducto'=>$COD_PRODUCTO,
         'codigoCliente'=>$CODCLIENTE,
         'codigoRecaudacion'=>$CODRECAUDA,
         'codigoEmpresa'=>$CODEMPRESA,
         'correoElectronico'=>$EMAIL,
         'descripcionRecaudacion'=>$DESRECAUDA,
         'fecha'=>$FECHAIN,
         'fechaVencimiento'=>$FECHAVEN,
         'hora'=>$HORAIN,
         'horaVencimiento'=>$HORAVEN,
         'moneda'=>$MONEDA,
         'nit_CI_cliente'=>$NIT,
         'nombre'=>$NOMBRE,
         'planillas'=>$cmeDetallePlan,
         'transaccion'=>$TRANSACCION,
     );

    $errorSintesis = "No";
    $result = wsRegistroPlan($cmeDatosPlan,'wstoqueeltimbre','https://web.sintesis.com.bo/wsComelec/service/Comelec.jws?WSDL=','cmeRegistroPlan', $errorSintesis);
    /*echo "<pre>";
    print_r($result);
    echo "</pre>";
    echo "-------------------";*/
}catch(Exception $e){
    echo 'Excepcion capturada: ',  $e->getMessage(), "\n";
    print_r($e);
}
?>
