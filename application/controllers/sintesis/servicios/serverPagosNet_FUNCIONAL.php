<?php
// Pull in the NuSOAP code
require_once( './lib/nusoap.php' );
require_once('../../admin/config/database.conf.php');
require_once('../../admin/config/constante.php');
require_once('./class/mod_usuario.php');

mysql_connect(_SERVIDOR_BASE_DE_DATOS, _USUARIO_BASE_DE_DATOS, _PASSWORD_BASE_DE_DATOS) or
        die("Could not connect: " . mysql_error());
mysql_select_db(_BASE_DE_DATOS);

function verificar_existencia_otros_pagos($pag_id, $pub_id, $ser_id){
	$sql = "
	SELECT p.pag_entidad, ps.fech_ini, ps.fech_fin FROM pagos p
	INNER JOIN pago_servicio ps ON (ps.pag_id = p.pag_id AND ps.ser_id = ".$ser_id." AND (CURDATE() >= ps.`fech_ini` AND CURDATE() <= ps.`fech_fin`) )
	WHERE p.pag_pub_id = ".$pub_id." AND p.`pag_id` <> '$pag_id'
	ORDER BY p.pag_fecha_pagado DESC, p.pag_fecha DESC
    LIMIT 1
	";
	$result = mysql_query($sql);
	$resultado = mysql_fetch_object($result);
	return $resultado;
}

function verificar_existencia_otros_pagos_suscripciones($pag_id, $ser_id){
    $sql = "
	SELECT p.pag_entidad, ps.fech_ini, ps.fech_fin FROM pagos p
	INNER JOIN pago_servicio ps ON (ps.pag_id = p.pag_id AND ps.ser_id = ".$ser_id.")
	WHERE p.pag_pub_id = ".$pub_id." AND ps.`fech_fin` >= CURDATE()
	ORDER BY p.pag_fecha DESC LIMIT 1,1
	";
    $result = mysql_query($sql);
    $resultado = mysql_fetch_object($result);
    return $resultado;
}

function obtener_id_publicacion($pag_id){
	$sql = "
	SELECT pag_pub_id FROM pagos 
	WHERE pag_id = '".$pag_id."'
	LIMIT 1
	";
	$result = mysql_query($sql);
	$resultado = mysql_fetch_object($result);
	return $resultado->pag_pub_id;
}

function obtener_email_de_pago($pag_id){
    $sql = "
		SELECT `usu_email` FROM pagos
		INNER JOIN publicacion ON pub_id = pag_pub_id
		INNER JOIN usuario ON usu_id = pub_usu_id
		WHERE pag_id =  '".$pag_id."'
		";
    $result = mysql_query($sql);
    $email = mysql_fetch_object($result);

    return $email->usu_email;
}

function verificar_cod_recaudacion_pagado($pag_id){
	$sql = "
	SELECT pag_id FROM pagos 
	WHERE pag_id = '".$pag_id."' and pag_estado = 'Pagado'
	LIMIT 1
	";
	$result = mysql_query($sql);
	$num = mysql_num_rows($result);
	$aux = $num > 0 ? true : false;
	return $aux;
}

function obtener_id_inmueble($pub_id){
	$sql = "
	SELECT pub_inm_id FROM publicacion 
	WHERE pub_id = '".$pub_id."'
	LIMIT 1
	";
	$result = mysql_query($sql);
	$resultado = mysql_fetch_object($result);
	return $resultado->pub_inm_id;
}

function obtener_pago_by_id($pag_id){
    $sql = "
	SELECT * FROM pagos
	WHERE pag_id = '".$pag_id."'
	LIMIT 1
	";
    $result = mysql_query($sql);
    $resultado = mysql_fetch_object($result);
    return $resultado;
}

function obtener_datos_inmueble($pag_id){
    $sql = "
		SELECT inm_id, inm_nombre, pub_id, pub_vig_fin FROM pagos
		INNER JOIN publicacion on (pub_id = pag_pub_id)
		INNER JOIN inmueble on (inm_id = pub_inm_id)
		WHERE pag_id = '".$pag_id."'
		LIMIT 1
		";
    $result = mysql_query($sql);
    $resultado = mysql_fetch_object($result);
    return $resultado;
}

function obtener_mayor_fecha_servicio($pub_id){
    $sql = "
        SELECT MAX(ps.`fech_fin`) AS mayor_fecha FROM pagos p
        INNER JOIN pago_servicio ps ON (ps.`pag_id` = p.`pag_id` AND CURDATE() <= ps.`fech_fin`)
        WHERE p.`pag_pub_id` = '$pub_id' AND p.`pag_estado` = 'Pagado'
        ";
    $result = mysql_query($sql);
    $resultado = mysql_fetch_object($result);
    return $resultado->mayor_fecha;
}

function obtener_id_servicios($pag_id){
	$sql = "
	SELECT ser_id FROM pago_servicio
	WHERE pag_id = '".$pag_id."'
	";
	$result = mysql_query($sql);
	while ($row = mysql_fetch_array($result)){
		$resultado[] = $row['ser_id'];
	}
	return $resultado;
}




// Create the server instance
$server = new soap_server();
// Initialize WSDL support
$server->configureWSDL('wsComelecServer', 'urn:wsComelecServer');
// Register the data structures used by the service
$server->wsdl->addComplexType(
    'WsTransaccion',
    'complexType',
    'struct',
    'all',
    '',
    array(
        'CodigoEmpresa' => array('name' => 'CodigoEmpresa', 'type' => 'xsd:int'),
	'CodigoRecaudacion' => array('name' => 'CodigoRecaudacion', 'type' => 'xsd:string'),
	'CodigoProducto' => array('name' => 'CodigoProducto', 'type' => 'xsd:string'),
	'NumeroPago' => array('name' => 'NumeroPago', 'type' => 'xsd:int'),
	'Fecha' => array('name' => 'Fecha', 'type' => 'xsd:int'),
	'Secuencial' => array('name' => 'Secuencial', 'type' => 'xsd:int'),
	'Hora' => array('name' => 'Hora', 'type' => 'xsd:int'),
	'OrigenTransaccion' => array('name' => 'OrigenTransaccion', 'type' => 'xsd:string'),
	'Pais' => array('name' => 'pais', 'type' => 'xsd:int'),
	'Departamento' => array('name' => 'Departamento', 'type' => 'xsd:int'),
	'Ciudad' => array('name' => 'Ciudad', 'type' => 'xsd:int'),	
	'Entidad' => array('name' => 'Entidad', 'type' => 'xsd:string'),
	'Agencia' => array('name' => 'Agencia', 'type' => 'xsd:string'),
	'Operador' => array('name' => 'Operador', 'type' => 'xsd:int'),
	'Monto' => array('name' => 'Monto', 'type' => 'xsd:double'),
	'LoteDosificacion' => array('name' => 'LoteDosificacion', 'type' => 'xsd:int', 'minOccurs'=>'0'),
	'NroRentaRecibo' => array('name' => 'NroRentaRecibo', 'type' => 'xsd:string'),
	'MontoCreditoFiscal' => array('name' => 'MontoCreditoFiscal', 'type' => 'xsd:double', 'minOccurs'=>'0'),
	'CodigoAutorizacion' => array('name' => 'CodigoAutorizacion', 'type' => 'xsd:string', 'minOccurs'=>'0'),
        'CodigoControl' => array('name' => 'CodigoControl', 'type' => 'xsd:string', 'minOccurs'=>'0'),
        'NitFacturar' => array('name' => 'NitFacturar', 'type' => 'xsd:string', 'minOccurs'=>'0'),
	'NombreFacturar' => array('name' => 'NombreFacturar', 'type' => 'xsd:string', 'minOccurs'=>'0'),
	'Transaccion' => array('name' => 'Transaccion', 'type' => 'xsd:string')
    )
);
$server->wsdl->addComplexType(
    'RespTransaccion',
    'complexType',
    'struct',
    'all',
    '',
    array(
        'CodError' => array('name' => 'CodError', 'type' => 'xsd:int'),
        'Descripcion' => array('name' => 'Descripcion', 'type' => 'xsd:string')
    )
);
// Register the method to expose
$server->register('datosTransaccion',                    // method name
    array('datos' => 'tns:WsTransaccion', 'user'=> 'xsd:string', 'password' => 'xsd:string'),   // input parameters
    array('return' => 'tns:RespTransaccion'),      // output parameters
    'urn:wsComelecServer',                         // namespace
    'urn:wsComelecServer#datosTransaccion',        // soapaction
    'rpc',                                    // style
    'encoded',                                     // use
    'Aqu&iacute; se describe la documentaci&oacute;n y tipos de error posibles'     // documentation
);
// Define the method as a PHP function
function datosTransaccion($datos, $user, $password) {
	  // echo "datos recibidos:";		// acá se define lo que se hace con los parámetros de entrada
	  //print_r($transacc); 
	 
	if(($user=='wstimbre')&&($password=='wstimbre')){
		
		mail('cristian.inarra@gmail.com', 'SINTESIS SERVER', "LLEGO UNA VARIABLE ES: " . $datos['CodigoRecaudacion']." user:".$user." pass:".$password);
		
		$retval = array(
					'CodError' => 0,
					'Descripcion' => 'OK datos enviados:'
					. $datos['CodigoEmpresa'] . "|"
					. $datos['CodigoRecaudacion'] . "|"
					. $datos['CodigoProducto'] . "|"
					. $datos['NumeroPago'] . "|"
					. $datos['Fecha'] . "|"
					. $datos['Secuencial'] . "|"
					. $datos['Hora'] . "|"
					. $datos['Pais'] . "|"
					. $datos['Departamento'] . "|"
					. $datos['Ciudad'] . "|"
					. $datos['Entidad'] . "|"
					. $datos['Agencia'] . "|"
					. $datos['Operador'] . "|"
					. $datos['Monto'] . "|"
					. $datos['LoteDosificacion'] . "|"
					. $datos['NroRentaRecibo'] . "|"
					. $datos['MontoCreditoFiscal'] . "|"
					. $datos['CodigoAutorizacion'] . "|"
					. $datos['CodigoControl'] . "|"
					. $datos['NitFacturar'] . "|"
					. $datos['NombreFacturar'] . "|"
					. $datos['Transaccion'] . "|"
					);
		
		
		$id_pago = $datos['CodigoRecaudacion'];
        $obj_pago = obtener_pago_by_id($id_pago);
		
		if(!verificar_cod_recaudacion_pagado($id_pago)){
            $codigos_servicios = obtener_id_servicios($id_pago);
            $email = obtener_email_de_pago($id_pago);
            $usuario = new Mod_usuario();
            $html_ser = "";
            $total = 0;

            // Pago de suscripciones
            if($obj_pago->pag_pub_id == "0" && $obj_pago->pag_concepto == "Suscripcion"){
                $sql = "UPDATE pagos SET pag_estado = 'Pagado', pag_entidad = 'Pagosnet', pag_fecha_pagado = NOW() WHERE pag_id = '" . $id_pago . "'";
                mysql_query($sql);

                if(!empty($codigos_servicios)):
                    foreach($codigos_servicios as $ser_id){
                        if($ser_id != "") {
                            $html_ser .= $ser_id . "|";
                            $sql = "SELECT * FROM servicios WHERE ser_id = " . $ser_id;
                            $result = mysql_query($sql);
                            $obj_servicio = mysql_fetch_object($result);
                            //$num = mysql_num_rows($result);
                            /*$aux = verificar_existencia_otros_pagos_suscripciones($id_pub, $ser_id);

                            if ($num > 0 && $aux->fech_ini != "") {
                                //$fech_inicio = $aux->fech_fin;
                                $fech_inicio = date("Y-m-d");
                            } else {
                                $fech_inicio = date("Y-m-d");
                            }*/
                            $fech_inicio = date("Y-m-d");
                            $fec_vencimi = date("Y-m-d", strtotime($fech_inicio. " + " . ($obj_servicio->ser_dias - 1 ) . " days"));

                            $sql = "UPDATE pago_servicio SET fech_ini = '".$fech_inicio."', fech_fin = '".$fec_vencimi."' WHERE pag_id = '" . $id_pago . "' and ser_id = " . $ser_id;
                            $result = mysql_query($sql);

                            $total = $total + $obj_servicio->ser_precio;
                        }
                    }
                endif;

                $sql = "UPDATE suscripcion SET estado='Activo', fecha_inicio='". $fech_inicio ."', fecha_fin='". $fec_vencimi ."' WHERE pago_id = '".$id_pago."'";
                mysql_query($sql);
            }else{
                // Pago de anuncios

                $obj_inmueble = obtener_datos_inmueble($id_pago);
                $id_pub = $obj_inmueble->pub_id;
                $sql = "UPDATE pagos SET pag_estado = 'Pagado', pag_entidad = 'Pagosnet', pag_fecha_pagado = NOW() WHERE pag_id = '" . $id_pago . "'";
                mysql_query($sql);

                if(!empty($codigos_servicios)):
                    foreach($codigos_servicios as $ser_id){
                        if($ser_id != ""){
                            $html_ser .= $ser_id. "|";
                            $sql = "SELECT * FROM servicios WHERE ser_id = " . $ser_id;
                            $result = mysql_query($sql);
                            $obj_servicio = mysql_fetch_object($result);

                            $fecha_fin_ultimo = verificar_existencia_otros_pagos($id_pago, $id_pub, $ser_id);
                            if($fecha_fin_ultimo->fech_fin != ""){
                                $fech_inicio = $fecha_fin_ultimo->fech_fin;
                            }else{
                                $fech_inicio = date("Y-m-d");
                            }
                            $fec_vencimi = date("Y-m-d", strtotime($fech_inicio. " + " . ($obj_servicio->ser_dias - 1 ). " days"));
                            $sql = "UPDATE pago_servicio SET fech_ini = '".$fech_inicio."', fech_fin = '".$fec_vencimi."' WHERE pag_id = '" . $id_pago . "' and ser_id = " . $ser_id;
                            $result = mysql_query($sql);

                            $total = $total + $obj_servicio->ser_precio;
                        }
                    }
                endif;
            }

            $mayor_fecha = obtener_mayor_fecha_servicio($obj_inmueble->pub_id);
            if($mayor_fecha > $obj_inmueble->pub_vig_fin){
                $pub_vig_fin = date("Y-m-d", strtotime($mayor_fecha. " - 1 days"));
                $sql = "UPDATE publicacion SET pub_estado='Aprobado', pub_vig_fin='". $pub_vig_fin ."' WHERE pub_id = '".$obj_inmueble->pub_id."'";
            }else{
                $sql = "UPDATE publicacion SET pub_estado='Aprobado' WHERE pub_id = '".$obj_inmueble->pub_id."'";
            }
            mysql_query($sql);
            $sql="update inmueble set inm_publicado='Si' where inm_id = '".$obj_inmueble->inm_id."'";
            mysql_query($sql);

			mail('cristian.inarra@gmail.com', 'SINTESIS PAGADO', "El anuncio: " .$id_pago . " fue pagado y estos son los servicios. ".$html_ser);
			$usuario->enviar_mail_confirmar_pago($total, $email, $obj_inmueble->inm_nombre);
			
		}//fin verificar pago esta pagado
		
			
	}else{	
		$retval = array(
				'CodError' => 99,
				'Descripcion' => 'Usuario o Contraseña erroneos');
	}
	
 	return $retval; //new soapval('return', 'RespTransaccion', $retval, false, 'urn:wsComelecServer');
}

// Use the request to (try to) invoke the service
$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
$server->service($HTTP_RAW_POST_DATA);

//reference: http://www.scottnichol.com/nusoapprogwsdl.htm

?>
