<?php
// Pull in the NuSOAP code
require_once( 'lib/nusoap.php' );
require_once(FCPATH.'admin/config/database.conf.php');
require_once(FCPATH.'admin/config/constante.php');
//require_once('./class/mod_usuario.php');
//$system_path = '../../system/';
//$application_folder = '../../application/';
//define('BASEPATH', str_replace("\\", "/", $system_path));
//define('APPPATH', $application_folder.'/');
//require_once '../../system/core/CodeIgniter.php';
//ob_start();
//require('../../index.php');
//$data = ob_get_clean();
//ob_end_clean();

//mysql_connect(_SERVIDOR_BASE_DE_DATOS, _USUARIO_BASE_DE_DATOS, _PASSWORD_BASE_DE_DATOS) or
//        die("Could not connect: " . mysql_error());
//mysql_select_db(_BASE_DE_DATOS);

function verificar_existencia_otros_pagos($pag_id, $pub_id, $ser_id){
	$sql = "
	SELECT p.pag_entidad, ps.fech_ini, ps.fech_fin FROM pagos p
	INNER JOIN pago_servicio ps ON (ps.pag_id = p.pag_id AND ps.ser_id = ".$ser_id." AND (CURDATE() >= ps.`fech_ini` AND CURDATE() <= ps.`fech_fin`) )
	WHERE p.pag_pub_id = ".$pub_id." AND p.`pag_id` <> '$pag_id' AND p.`pag_entidad` <> 'Suscripcion'
	ORDER BY p.pag_fecha_pagado DESC, p.pag_fecha DESC
    LIMIT 1
	";
	$result = mysql_query($sql);
	$resultado = mysql_fetch_object($result);
	return $resultado;
}

function verificar_existencia_otros_pagos_suscripciones($pag_id, $ser_id){
    $sql = "
	SELECT p.pag_entidad, ps.fech_ini, ps.fech_fin FROM pagos p
	INNER JOIN pago_servicio ps ON (ps.pag_id = p.pag_id AND ps.ser_id = ".$ser_id.")
	WHERE p.pag_pub_id = ".$pub_id." AND ps.`fech_fin` >= CURDATE()
	ORDER BY p.pag_fecha DESC LIMIT 1,1
	";
    $result = mysql_query($sql);
    $resultado = mysql_fetch_object($result);
    return $resultado;
}

function obtener_id_publicacion($pag_id){
	$sql = "
	SELECT pag_pub_id FROM pagos 
	WHERE pag_id = '".$pag_id."'
	LIMIT 1
	";
	$result = mysql_query($sql);
	$resultado = mysql_fetch_object($result);
	return $resultado->pag_pub_id;
}

function habilitar_usuario($pub_id){
    $sql = "
		SELECT pub_usu_id FROM publicacion p
		WHERE pub_id = '".$pub_id."'
		LIMIT 1
		";
    $result = mysql_query($sql);
    $resultado = mysql_fetch_object($result);
    $sql = "UPDATE usuario SET usu_estado = 'Habilitado' WHERE usu_id = ".$resultado->pub_usu_id;
    mysql_query($sql);
}

function obtener_email_de_pago($pag_id){
    $sql = "
		SELECT `usu_email` FROM pagos
		INNER JOIN publicacion ON pub_id = pag_pub_id
		INNER JOIN usuario ON usu_id = pub_usu_id
		WHERE pag_id =  '".$pag_id."'
		";
    $result = mysql_query($sql);
    $email = mysql_fetch_object($result);

    return $email->usu_email;
}

function verificar_cod_recaudacion_pagado($pag_id){
	$sql = "
	SELECT pag_id FROM pagos 
	WHERE pag_id = '".$pag_id."' and pag_estado = 'Pagado'
	LIMIT 1
	";
	$result = mysql_query($sql);
	$num = mysql_num_rows($result);
	$aux = $num > 0 ? true : false;
	return $aux;
}

function obtener_id_inmueble($pub_id){
	$sql = "
	SELECT pub_inm_id FROM publicacion 
	WHERE pub_id = '".$pub_id."'
	LIMIT 1
	";
	$result = mysql_query($sql);
	$resultado = mysql_fetch_object($result);
	return $resultado->pub_inm_id;
}

function obtener_pago_by_id($pag_id){
    $sql = "
	SELECT * FROM pagos
	WHERE pag_id = '".$pag_id."'
	LIMIT 1
	";
    $result = mysql_query($sql);
    $resultado = mysql_fetch_object($result);
    return $resultado;
}

function obtener_datos_inmueble($pag_id){
    $sql = "
		SELECT inm_id, inm_nombre, pub_id, pub_vig_fin FROM pagos
		INNER JOIN publicacion on (pub_id = pag_pub_id)
		INNER JOIN inmueble on (inm_id = pub_inm_id)
		WHERE pag_id = '".$pag_id."'
		LIMIT 1
		";
    $result = mysql_query($sql);
    $resultado = mysql_fetch_object($result);
    return $resultado;
}

function obtener_mayor_fecha_servicio($pub_id){
    $sql = "
        SELECT MAX(ps.`fech_fin`) AS mayor_fecha FROM pagos p
        INNER JOIN pago_servicio ps ON (ps.`pag_id` = p.`pag_id` AND CURDATE() <= ps.`fech_fin`)
        WHERE p.`pag_pub_id` = '$pub_id' AND p.`pag_estado` = 'Pagado'
        ";
    $result = mysql_query($sql);
    $resultado = mysql_fetch_object($result);
    return $resultado->mayor_fecha;
}

function obtener_id_servicios($pag_id){
	$sql = "
	SELECT ser_id FROM pago_servicio
	WHERE pag_id = '".$pag_id."'
	";
	$result = mysql_query($sql);
	while ($row = mysql_fetch_array($result)){
		$resultado[] = $row['ser_id'];
	}
	return $resultado;
}




// Create the server instance
$server = new soap_server();
// Initialize WSDL support
$server->configureWSDL('WSPagosNet', 'urn:toqueeltimbre.com', base_url("sintesis/WSPagosNet"));
// Register the data structures used by the service
$server->wsdl->addComplexType(
    'WsTransaccion',
    'complexType',
    'struct',
    'all',
    '',
    array(
        'CodigoEmpresa' => array('name' => 'CodigoEmpresa', 'type' => 'xsd:int'),
	'CodigoRecaudacion' => array('name' => 'CodigoRecaudacion', 'type' => 'xsd:string'),
	'CodigoProducto' => array('name' => 'CodigoProducto', 'type' => 'xsd:string'),
	'NumeroPago' => array('name' => 'NumeroPago', 'type' => 'xsd:int'),
	'Fecha' => array('name' => 'Fecha', 'type' => 'xsd:int'),
	'Secuencial' => array('name' => 'Secuencial', 'type' => 'xsd:int'),
	'Hora' => array('name' => 'Hora', 'type' => 'xsd:int'),
	'OrigenTransaccion' => array('name' => 'OrigenTransaccion', 'type' => 'xsd:string'),
	'Pais' => array('name' => 'pais', 'type' => 'xsd:int'),
	'Departamento' => array('name' => 'Departamento', 'type' => 'xsd:int'),
	'Ciudad' => array('name' => 'Ciudad', 'type' => 'xsd:int'),	
	'Entidad' => array('name' => 'Entidad', 'type' => 'xsd:string'),
	'Agencia' => array('name' => 'Agencia', 'type' => 'xsd:string'),
	'Operador' => array('name' => 'Operador', 'type' => 'xsd:int'),
	'Monto' => array('name' => 'Monto', 'type' => 'xsd:double'),
	'LoteDosificacion' => array('name' => 'LoteDosificacion', 'type' => 'xsd:int', 'minOccurs'=>'0'),
	'NroRentaRecibo' => array('name' => 'NroRentaRecibo', 'type' => 'xsd:string'),
	'MontoCreditoFiscal' => array('name' => 'MontoCreditoFiscal', 'type' => 'xsd:double', 'minOccurs'=>'0'),
	'CodigoAutorizacion' => array('name' => 'CodigoAutorizacion', 'type' => 'xsd:string', 'minOccurs'=>'0'),
    'CodigoControl' => array('name' => 'CodigoControl', 'type' => 'xsd:string', 'minOccurs'=>'0'),
    'NitFacturar' => array('name' => 'NitFacturar', 'type' => 'xsd:string', 'minOccurs'=>'0'),
	'NombreFacturar' => array('name' => 'NombreFacturar', 'type' => 'xsd:string', 'minOccurs'=>'0'),
	'Transaccion' => array('name' => 'Transaccion', 'type' => 'xsd:string')
    )
);
$server->wsdl->addComplexType(
    'RespTransaccion',
    'complexType',
    'struct',
    'all',
    '',
    array(
        'CodError' => array('name' => 'CodError', 'type' => 'xsd:int'),
        'Descripcion' => array('name' => 'Descripcion', 'type' => 'xsd:string')
    )
);
// Register the method to expose
$server->register('datosTransaccion',                    // method name
    array('datos' => 'tns:WsTransaccion', 'user'=> 'xsd:string', 'password' => 'xsd:string'),   // input parameters
    array('return' => 'tns:RespTransaccion'),      // output parameters
    'urn:toqueeltimbre.com',                         // namespace
//    'urn:wsComelecServer#datosTransaccion',        // soapaction
    base_url("sintesis/WSPagosNet"),        // soapaction
    'rpc',                                    // style
    'encoded',                                     // use
    'Aqu&iacute; se describe la documentaci&oacute;n y tipos de error posibles'     // documentation
);

// Define the method as a PHP function
function datosTransaccion($datos, $user, $password)
{
//	   echo "datos recibidos:";		// acá se define lo que se hace con los parámetros de entrada
//	  print_r($datos);
	 
	if(($user=='wstimbre')&&($password=='wstimbre'))
	{
		$retval = array(
					'CodError' => 0,
					'Descripcion' => 'OK datos enviados:'
					. $datos['CodigoEmpresa'] . "|"
					. $datos['CodigoRecaudacion'] . "|"
					. $datos['CodigoProducto'] . "|"
					. $datos['NumeroPago'] . "|"
					. $datos['Fecha'] . "|"
					. $datos['Secuencial'] . "|"
					. $datos['Hora'] . "|"
					. $datos['Pais'] . "|"
					. $datos['Departamento'] . "|"
					. $datos['Ciudad'] . "|"
					. $datos['Entidad'] . "|"
					. $datos['Agencia'] . "|"
					. $datos['Operador'] . "|"
					. $datos['Monto'] . "|"
					. $datos['LoteDosificacion'] . "|"
					. $datos['NroRentaRecibo'] . "|"
					. $datos['MontoCreditoFiscal'] . "|"
					. $datos['CodigoAutorizacion'] . "|"
					. $datos['CodigoControl'] . "|"
					. $datos['NitFacturar'] . "|"
					. $datos['NombreFacturar'] . "|"
					. $datos['Transaccion'] . "|"
					);

		$paymentId = $datos['CodigoRecaudacion'];
        $paymentMethod = model_payment::PAGOS_NET;
        /** @var model_payment $payment */
		$payment = model_payment::getById($paymentId);
		if($payment instanceof model_payment)
        {
            $response = $payment->approve($paymentMethod);
            if(count($response) < 2)
            {
                $retval['CodError'] = 'ERROR_ON_APPROVING_PAYMENT';
                $retval['Descripcion'] = $response[0];
            }
        }
        else
        {
            $retval['CodError'] = 'ERROR_ON_APPROVING_PAYMENT';
            $retval['Descripcion'] = 'El codigo de recaudacion no existe.';
        }

	}
	else
    {
		$retval = array(
				'CodError' => 99,
				'Descripcion' => 'Usuario o Contrasenia erroneos');
	}
	
 	return $retval;//new soapval('return', 'RespTransaccion', $retval, false, 'urn:wsComelecServer');
}

// Use the request to (try to) invoke the service
//$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
//$server->service($HTTP_RAW_POST_DATA);
$server->service(file_get_contents("php://input"));

//reference: http://www.scottnichol.com/nusoapprogwsdl.htm
