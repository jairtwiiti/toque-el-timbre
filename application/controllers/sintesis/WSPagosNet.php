<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class WSPagosNet extends CI_Controller
{
	public function index()
	{
        $ci = &get_instance();
//		$ci->load->database();
        $ci->load->model('model_payment');
        $ci->load->model('model_paymentservice');
        $ci->load->model('model_publication');
        $ci->load->model('model_property');
        $ci->load->model('model_service');

        require_once('servicios/serverPagosNet.php');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */