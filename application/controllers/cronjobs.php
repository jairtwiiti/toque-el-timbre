<?php
/**
 * Created by PhpStorm.
 * User: David
 * Date: 31/03/2015
 * Time: 15:51
 */

class Cronjobs extends Base_page {

	public function __construct() {
		parent::__construct();

		$this->load->model('model_parametros');
		$this->load->model('model_publicacion');
		$this->load->model('model_usuario');
		$this->load->model('model_suscripcion');
	}

	public function send_publications_ended_this_week() {
		require_once(BASE_DIRECTORY . DS . 'admin' . DS . 'clases' . DS. 'phpmailer' . DS . 'class.phpmailer.php');

		$endPublications = $this->model_publicacion->get_publications_ended_this_week_users();
		foreach($endPublications as $endPublication) {
			$publications = $this->model_publicacion->get_publications_ended_this_week_was_sent($endPublication->pub_usu_id, false);
			$mail_user = $this->model_usuario->get_info_agent($endPublication->pub_usu_id)->usu_email;

			if (!empty($publications) && !empty($mail_user)) {
				$html_aux = $this->get_html_publications($publications);

				$parametros = $this->model_parametros->get_all();
				$template = base_url() . "assets/template_mail/recordatorio_usuario.html";
				$contenido = $this->obtener_contenido($template);
				$contenido = str_replace('@@contenido', $html_aux, $contenido);
				$cue_nombre = $parametros->par_salida;
				$cue_pass = $parametros->par_pas_salida;

				$mail = new PHPMailer(true);
				$mail->SetLanguage('es');
				$mail->IsSMTP();
				$mail->SMTPAuth = true;
				$mail->Host = $parametros->par_smtp;
				$mail->Timeout = 30;
				$mail->CharSet = 'utf-8';
				$mail->Subject = "Anuncios que finalizan dentro de los siguientes 7 dias";
				$body = $contenido;
				$mail->MsgHTML($body);
				$mail->AddAddress($mail_user);
				//$mail->AddAddress("cristian.inarra@gmail.com");
				$mail->SetFrom($cue_nombre, model_parametros::SITE_DOMAIN);
				$mail->Username = $cue_nombre;
				$mail->Password = $cue_pass;

				if($mail->Send()){
					foreach($publications as $publication) {
						$data = array(
							"pub_enviado_1" => true
						);
						$this->model_publicacion->update($publication->pub_id, $data);
					}
				}
			}
		}
	}

	private function get_html_publications($publications = array()) {
		//$str = "<ol>";
        $str = '
        <table width="100%" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" align="center">
        <tbody>
        <tr>
            <td bgcolor="#00AEEF" style="padding-left:2px"><font size="2" color="white" face="Verdana">Anuncio</font></td>
            <td bgcolor="#00AEEF" style="padding-right:2px"><font size="2" color="white" face="Verdana">Link</font></td>
        </tr>
        ';
			foreach($publications as $publication) {
				$str .= '
				<td style="vertical-align:top;padding:4px 1px;line-height:16px">
                    <font size="2" color="#474747" face="Verdana">'.$publication->inm_nombre.'</font>
                </td>
                <td style="vertical-align:top;padding:4px 1px;line-height:16px">
                    <font size="2" color="#00AEEF" face="Verdana">
                        <a target="_blank" href="'.base_url().'dashboard/publication/renew/'.$publication->pub_id.'" style="color:#00aeef">Renovar</a>
                    </font>
                </td>
				';
                //$str .= "<li>$publication->inm_nombre (Renovar)</li>";
			}

        $str .= '
        </tbody>
        </table>
        ';
		//$str .= "</ol>";
		return $str;
	}

	public function expiry_ads() {
		$data = array(
			'estado' => 'Expirado'
		);

		$this->model_suscripcion->update_all($data);
	}

	public function expiry_sucriptions() {
		$data = array(
			'estado' => 'Expirado'
		);

		$this->model_suscripcion->update_all($data);
	}

	private function obtener_contenido($url)
	{
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_HEADER, 0);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.3) Gecko/20070309 Firefox/2.0.0.3");
		curl_setopt($curl, CURLOPT_URL, $url);
		$contenido = curl_exec($curl);
		curl_close($curl);

		return $contenido;
	}

	public function index() {
	}

}