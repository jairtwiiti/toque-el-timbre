<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 28/07/2017
 * Time: 9:30
 */

class AjaxProject extends Base_page
{
    public function __construct()
    {
        parent::__construct();
        if(! $this->input->is_ajax_request())
        {
            redirect('404');
        }
        $this->load->model('model_user');
        $this->load->model('model_project');
        $this->load->model('model_project_message');
    }

    public function sendContactMessage()
    {
        $data = array();
        $formData = $this->input->post();
        $projectId = $formData['proy_id'];
        $projectUser = model_user::getByProjectId($projectId);
        $project = model_project::getById($projectId);
        $view = 'email_template/contact-message-project';
        $data['user'] = $projectUser->toArray();
        $data['project'] = $project->toArray();
        $sendMessageResponse = model_project_message::sendContactMessage($view,$data,"Contacto Proyecto");
        echo json_encode($sendMessageResponse);
    }

    public function getEnabledProjects()
    {
        $enabledProjects = model_project::getEnabledProjects();
        for($i = 0; $i < count($enabledProjects); $i++)
        {
            $enabledProjects[$i]['proy_ubicacion'] = strip_tags($enabledProjects[$i]['proy_ubicacion']);
            $enabledProjects[$i]['proy_ubicacion'] = html_entity_decode($enabledProjects[$i]['proy_ubicacion']);
            $enabledProjects[$i]['proy_ubicacion'] = substr($enabledProjects[$i]['proy_ubicacion'],0,60)."...";
        }

        echo json_encode($enabledProjects);exit;
    }
}