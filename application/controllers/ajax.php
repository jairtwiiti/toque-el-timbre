<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Christian
 * Date: 13/11/2014
 * Time: 17:11
 */

class Ajax extends Base_page{

    function __construct() {
        parent::__construct();
        $this->load->model("model_usuario");
        $this->load->model("model_ciudad");
        $this->load->model("model_zona");
        $this->load->model("model_caracteristica");
        $this->load->model("model_inmueble_foto");
        $this->load->model("model_inmueble");
    }

    public function index() {

    }

    public function calculator(){
        $precio = $_POST['precio'];
        $plazo = 20; // 20años
        $interes = 1.055; // 5.5 vivienda social

        $total = ($precio * $interes) / (20*12);
        echo 'Mensualmente: $' . round($total,2);
    }

    public function register_phone_action(){
        $this->load->model("model_inmueble");
        $inm_id = $this->input->post('id_inmueble');
        if(!empty($inm_id) && is_numeric($inm_id)){
            $this->model_inmueble->update_visitor_llamadas($inm_id);
        }
    }

    public function validate_email(){
        $email = $_GET['email'];
        $validate = $this->model_usuario->email_is_registered($email);
        if(!$validate){
            echo "true";
        }else{
            echo "false";
        }
    }

    public function city(){
        $dep_id = $this->input->post('departamento');
        $cities = $this->model_ciudad->get_cities_by_state($dep_id);
        foreach($cities as $city){
            echo '<option value="'.$city->ciu_id.'">'.$city->ciu_nombre.'</option>';
        }
    }

    public function zone(){
        $dep_id = $this->input->post('departamento');
        $areas = $this->model_zona->get_zone_by_state($dep_id);
        foreach($areas as $zone){
            echo '<option value="'.$zone->zon_id.'">'.$zone->zon_nombre.'</option>';
        }
    }

    public function features(){
        $cat_id = $this->input->post('category');
        if(!empty($cat_id)){
            $data['features'] = $this->model_caracteristica->get_all_features($cat_id);
            $this->load->view('backend/publications/ajax_features', $data);
        }
    }

    public function delete_image(){
        $image_id = $this->input->post('image_id');
        $this->model_inmueble_foto->delete($image_id);
    }

    public function delete_plano(){
        $image_id = $this->input->post('plano_id');
        $this->model_inmueble_foto->delete_plano($image_id);
    }

    public function upload_images()
    {
        if(!empty($_FILES['file']['name']))
        {
            $image = $this->do_upload();

            if($image["upload"] == FALSE)
            {
                $data["error_profile_image"] = $image["error"];
            }
            else
            {
                $filename = $image["filename"];
                //$this->do_resize($filename);
            }
            $url = base_url() ."librerias/timthumb.php?src=" . base_url() . "admin/imagenes/inmueble/" .$filename ."&w=120";
            $values = array(
                'status' => 'OK',
                'image' => '<img src="'.$url.'" />',
                'result' => '<input type="hidden" name="file_upload[]" value="'.$filename.'" />'
            );
        }
        else
        {
            $values = array(
                'status' => 'FAIL',
                'result' => ''
            );
        }
        echo json_encode($values);
    }

    public function upload_images_planos(){

        if(!empty($_FILES['file']['name'])) {
            $image = $this->do_upload_planos();

            if($image["upload"] == FALSE){
                $data["error_profile_image"] = $image["error"];
            }else{
                $filename = $image["filename"];
                //$this->do_resize($filename);
            }
            $url = base_url() ."librerias/timthumb.php?src=" . base_url() . "admin/imagenes/inmueble/" .$filename ."&w=120";
            $values = array(
                'status' => 'OK',
                'image' => '<img src="'.$url.'" />',
                'result' => '<input type="hidden" name="upload_planos[]" value="'.$filename.'" />'
            );
        }else{
            $values = array(
                'status' => 'FAIL',
                'result' => ''
            );
        }
        echo json_encode($values);
    }

    private function do_upload()
    {
        $filename = date("Y_m_d_H_i_s_").rand();
        $config['file_name'] = $filename;
        $config['upload_path'] = realpath(BASE_DIRECTORY. '/admin/imagenes/inmueble/');
        $config['overwrite'] = FALSE;
        $config['encrypt_name'] = FALSE;
        $config["allowed_types"] = 'jpg|jpeg|png|gif';
        $config["max_size"] = 30720;//3048;
        $this->load->library('upload', $config);
        if(!$this->upload->do_upload('file'))
        {

            $error = $this->upload->display_errors();
            return array(
                "upload" => FALSE,
                "filename" => "",
                "error" => $error
            );
        }
        else
        {
            $fileHandler = new File_Handler();
            $array = explode('.', $_FILES['file']['name']);
            $extension = end($array);
            $fileHandler->compressImage($config['upload_path'],$filename.".".$extension);
            $aux = $this->upload->data();
            return array(
                "upload" => TRUE,
                "filename" => $aux["file_name"],
                "error" => ""
            );
        }
    }

    private function do_upload_planos(){

        $filename = date("Y_m_d_H_i_s_").rand();
        $config['file_name'] = $filename;
        $config['upload_path'] = realpath(BASE_DIRECTORY. '/admin/imagenes/planos/');
        $config['overwrite'] = FALSE;
        $config['encrypt_name'] = FALSE;
        $config["allowed_types"] = 'jpg|jpeg|png|gif';
        $config["max_size"] = 30720;
        $this->load->library('upload', $config);
        if(!$this->upload->do_upload('file')) {
            $error = $this->upload->display_errors();
            return array(
                "upload" => FALSE,
                "filename" => "",
                "error" => $error
            );
        }else{
            $fileHandler = new File_Handler();
            $array = explode('.', $_FILES['file']['name']);
            $extension = end($array);
            $fileHandler->compressImage($config['upload_path'],$filename.".".$extension);
            $aux = $this->upload->data();
            return array(
                "upload" => TRUE,
                "filename" => $aux["file_name"],
                "error" => ""
            );
        }
    }

    public function form_busqueda_user(){
        $this->load->model("model_notificaciones_busqueda");
        $this->load->model("model_forma");
        $this->load->model("model_categoria");
        $this->load->model("model_departamento");

        $data['categories'] = $this->model_categoria->get_all();
        $data['forma'] = $this->model_forma->get_all();
        $data['cities'] = $this->model_departamento->get_all();

        if($_POST){
            $values = array(
                "not_bus_nombre" => $this->input->post("nombre"),
                "not_bus_email" => $this->input->post("email"),
                "not_bus_telefono" => $this->input->post("telefono"),
                "not_bus_descripcion" => $this->input->post("descripcion"),
                "not_bus_cat_id" => $this->input->post("categoria"),
                "not_bus_for_id" => $this->input->post("in"),
                "not_bus_dep_id" => $this->input->post("ciudad"),
                "not_bus_vig_ini" => date("Y-m-d"),
                "not_bus_vig_fin" => date("Y-m-d", strtotime("+30 days"))
            );
            $data['sw_insert'] = $this->model_notificaciones_busqueda->insert($values);
        }

        $this->load->view('frontend/ajax_busqueda_user', $data);
    }

    /*public function send_mail($content_mail){
        require_once(BASE_DIRECTORY . DS . 'admin' . DS . 'clases' . DS. 'phpmailer' . DS . 'class.phpmailer.php');
        $parametros = $this->model_parametros->get_all();
        $email_send = $_POST['email'];

        $template = base_url() . "assets/template_mail/notificaciones_busqueda_inmobiliaria.html";
        $contenido = $this->obtener_contenido($template);
        $contenido = str_replace('@@nombre', "Busquedas favoritas",$contenido);
        $contenido = str_replace('@@mensaje',$content_mail,$contenido);

        $cue_nombre = $parametros->par_salida;
        $cue_pass = $parametros->par_pas_salida;

        $mail = new PHPMailer(true);
        $mail->SetLanguage('es');
        $mail->IsSMTP();
        $mail->SMTPAuth = true;
        $mail->Host = $parametros->par_smtp;
        $mail->Timeout=30;
        $mail->CharSet = 'utf-8';
        $mail->Subject = "Resultados de su Busqueda";
        $body = $contenido;
        $mail->MsgHTML($body);
        $mail->AddAddress($email_send);
        $mail->SetFrom($cue_nombre, model_parametros::DOMAIN_SITE);
        $mail->Username = $cue_nombre;
        $mail->Password = $cue_pass;

        if($mail->Send()){
            return '<div class="alert alert-success">Su mensaje fue enviado correctamente</div>';
        }else{
            return '<div class="alert alert-danger">Su mensaje no puedo ser enviado.</div>';
        }
    }

    private function obtener_contenido($url)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.3) Gecko/20070309 Firefox/2.0.0.3");
        curl_setopt($curl, CURLOPT_URL, $url);
        $contenido = curl_exec($curl);
        curl_close($curl);

        return $contenido;
    }*/
    public function saveSurvey()
    {
        $ci=&get_instance();
        $ci->load->model('model_survey');
        $formData = $ci->input->post();
        $publicationId = $formData["publication-id"];
        $propertySold = $formData["property-sold"];
        if($propertySold == 1)
        {
            $meansOfSale = $formData["means-of-sale"];
            $salesPrice = $formData["sales-price"];
            $noSoldDescription = NULL;
        }
        else
        {
            $meansOfSale = NULL;
            $salesPrice = NULL;
            $noSoldDescription = $formData["no-sold-description"];
        }
        $survey = new model_survey($publicationId, $propertySold, $meansOfSale, $salesPrice,$noSoldDescription);
        $survey->save();
    }

    public function updatePublication()
    {
        $formData = $this->input->post();
        $propertyId = $formData["propertyId"];
        $dataToUpdate = array("inm_publicado"=>$formData['isPublished']);
        $this->model_inmueble->update($propertyId, $dataToUpdate);
    }
}