<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Christian
 * Date: 14/11/2014
 * Time: 9:44
 */
class Pages extends Base_page{

    function __construct() {
        parent::__construct();
        $this->load->model('model_contenido');
        $this->load->model('model_parametros');
	    $this->load->model('model_usuario');

    }

    public function nosotros() {
        $data['contenido'] = $this->model_contenido->get_all(1);
        $meta = array(
            'description' => 'ToqueelTimbre.com es el primer buscador inmobiliario de Bolivia. Somos especialistas en el manejo y optimización del internet y hemos creado esta herramienta que será de gran ayuda a quienes buscan inmuebles centralizando el mercado de los bienes raíces de forma útil y sencilla.',
            'keywords' => 'que es toqueeltimbre, portales inmbiliarios, oferta de casas, buscador de inmuebles, portal de bienes raices, portal de inmuebles en bolivia, el primer portal de bienes raices'
        );
        $this->load_template('frontend/pages/nosotros', "Acerca de ToqueelTimbre.com", $data, $meta);
    }

    public function contacto()
    {
        $this->complementHandler->addViewComplement("google.maps.api");
        $this->complementHandler->addViewComplement("gmaps");
        $this->complementHandler->addProjectJs("MapHandler");
        $this->complementHandler->addProjectCss("pages.contacto");
        $this->complementHandler->addProjectJs("pages.contacto");

        $data['contenido'] = $this->model_contenido->get_all(8);
        $this->load->library('form_validation');

        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('name', 'Nombre', 'required');
        $this->form_validation->set_rules('phone', 'Telefono', 'required');
        $this->form_validation->set_rules('message', 'Mensaje', 'required');

        $this->form_validation->set_message('required', 'El campo <b>%s</b> es requerido');
        $data["reCaptchaKeys"] = static::getReCaptchaKeys();
        $meta = array(
            'description' => 'Nuestras oficinas estan ubicadas en la ciudad de Santa Cruz de la Sierra. Nuestro numero de contacto es (591) 78526002 o escribenos a info@toqueeltimbre.com.',
            'keywords' => 'oficinas de toqueeltimbre, como anunciar bienes inmuebles, contactarme para anunciar casas, contactarme para subir un anuncio inmobiliario, Contactar para vender mi casa, Contactar para vender mi departamento, contactar para vender mi propiedad, Contactar para alquilar mi inmueble'
        );
        if ($this->form_validation->run() == false)
        {
            $this->load_template('frontend/pages/contacto', "Comuniquese con nosotros", $data, $meta);
        }
        else
        {
            try{
                $response = $this->send_mail();
                $data['mensaje'] = $response["message"];
            }
            catch (Exception $e)
            {
                $data['mensaje'] = $e->getMessage();
            }

            $this->load_template('frontend/pages/contacto', "Comuniquese con nosotros", $data, $meta);
        }
    }

    public function agente()
    {
        $this->complementHandler->addViewComplement("google.maps.api");
        $this->complementHandler->addViewComplement("gmaps");
        $this->complementHandler->addProjectJs("MapHandler");
        $this->complementHandler->addProjectCss("pages.agent");
        $this->complementHandler->addProjectJs("pages.agent");

        $this->load->library('form_validation');

        $this->form_validation->set_rules('name', 'Nombre', 'trim|required');
        $this->form_validation->set_rules('razon_social', 'Razon social', 'trim|required');
        $this->form_validation->set_rules('nit', 'Nit', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required');
        $this->form_validation->set_rules('phone', 'Telefono', 'trim|required');
        $this->form_validation->set_rules('experiencia', 'Años de Experiencia', 'trim|required');
        $this->form_validation->set_rules('message', 'Mensaje', 'trim|required');

        $meta = array(
            'description' => 'Nuestras oficinas estan ubicadas en la ciudad de Santa Cruz de la Sierra. si deseas publicar como inmobiliaria certificada te invitamos a contactarnos para mas informacion.',
            'keywords' => 'oficinas de toqueeltimbre, como anunciar bienes inmuebles, contactarme para anunciar casas, contactarme para subir un anuncio inmobiliario, Contactar para alquilar mi inmueble'
        );

        $data['contenido'] = $this->model_contenido->get_all(8);
        $data["reCaptchaKeys"] = static::getReCaptchaKeys();
        if ($this->form_validation->run() == false)
        {
            $this->load_template('frontend/pages/contacto_inmobiliaria', "Agentes Certificados", $data, $meta);
        }
        else
        {
            try{
                $response = $this->send_mail_agente();
                $messageType = $response["response"] == 1?'successMessage':'errorMessage';
                $this->session->set_flashdata($messageType,$response["message"]);
            }
            catch (Exception $e)
            {
                $this->session->set_flashdata("errorMessage",$e->getMessage());
            }
            redirect(base_url("contacto-agente"));
        }
    }

    public function check_captcha($str){
        $word = $this->session->userdata('captchaWord');
        if(strcmp(strtoupper($str),strtoupper($word)) == 0){
            return true;
        }
        else{
            $this->form_validation->set_message('check_captcha', 'El codigo captcha es erroneo.');
            return false;
        }
    }

    public function preguntas(){
        $data['contenido'] = $this->model_contenido->get_all(5);
        $meta = array(
            'description' => 'Preguntas frecuentes',
            'keywords' => 'preguntas toqueeltimbre'
        );
        $this->load_template('frontend/pages/preguntas', "Preguntas Frecuentes", $data, $meta);
    }

    public function pagos(){
        $data['contenido'] = $this->model_contenido->get_all(9);
        $meta = array(
            'description' => 'Formas de Pago',
            'keywords' => 'formas de pago toqueeltimbre'
        );
        $this->load_template('frontend/pages/pagos', "Formas de Pago", $data, $meta);
    }

    public function videotutoriales(){
        $data['contenido'] = $this->model_contenido->get_all(4);
        $meta = array(
            'description' => 'Videotutoriales',
            'keywords' => 'videotutoriales toqueeltimbre'
        );
        $this->load_template('frontend/pages/videotutoriales', "Videotutoriales", $data, $meta);
    }

    public function politicas(){
        $data['contenido'] = $this->model_contenido->get_all(3);
        $meta = array(
            'description' => 'Politicas de privacidad',
            'keywords' => 'politicas privacidad toqueeltimbre'
        );
        $this->load_template('frontend/pages/privacidad', "Politicas de Privacidad", $data, $meta);
    }

    public function inmobiliaria(){
        $data['contenido'] = $this->model_contenido->get_all(10);
	    $data['inmobiliarias'] = $this->model_usuario->certificated_users();
        $meta = array(
            'description' => '¿Deseas publicar como inmobiliaria?',
            'keywords' => 'formas de pago toqueeltimbre'
        );

        $this->load_template('frontend/pages/publicar_inmobiliaria', "¿Deseas publicar como inmobiliaria?", $data, $meta);
    }

    public function particular(){
        $data['contenido'] = $this->model_contenido->get_all(12);
        $meta = array(
            'description' => '¿Deseas publicar como Particular?',
            'keywords' => 'formas de pago toqueeltimbre'
        );
        $this->load_template('frontend/pages/publicar_particular', "¿Deseas publicar como Particular?", $data, $meta);
    }

    public function proyectos(){
        $data['contenido'] = $this->model_contenido->get_all(11);
        $meta = array(
            'description' => '¿Deseas publicar tus Proyectos Inmobiliarios?',
            'keywords' => 'formas de pago toqueeltimbre'
        );
        $this->load_template('frontend/pages/publicar_proyectos', "¿Deseas publicar publicar tus Proyectos Inmobiliarios?", $data, $meta);
    }

    public function precios(){
        $data = array();
        /*$data['contenido'] = $this->model_contenido->get_all(11);

        $meta = array(
            'description' => '¿Deseas publicar tus Proyectos Inmobiliarios?',
            'keywords' => 'formas de pago toqueeltimbre'
        );*/
        $this->load_template('frontend/pages/tabla_precios', "Precios", $data);
    }

    public function mapa(){
        $this->load->view("frontend/inmueble/iframe_mapa");
    }

    public function send_mail()
    {
        $validationResponse = static::validateToken($_POST["g-recaptcha-response"]);
        $sendMessageResponse = array("response" => 0,"message" => '<div class="alert alert-danger">Su mensaje no pudo ser enviado.</div>');
        if($validationResponse->success)
        {
            require_once(BASE_DIRECTORY . DS . 'admin' . DS . 'clases' . DS. 'phpmailer' . DS . 'class.phpmailer.php');
            $parametros = $this->model_parametros->get_all();

            $template = base_url() . "assets/template_mail/mensaje_contacto_toqueeltimbre.html";
            $contenido = $this->obtener_contenido($template);
            $contenido = str_replace('@@nombre',$_POST['name'],$contenido);
            $contenido = str_replace('@@ciudad',$_POST['city'],$contenido);
            $contenido = str_replace('@@telefono',$_POST['phone'],$contenido);
            $contenido = str_replace('@@email',$_POST['email'],$contenido);
            $contenido = str_replace('@@comentario',nl2br($_POST['message']),$contenido);

            $cue_nombre = $parametros->par_salida;
            $cue_pass = $parametros->par_pas_salida;

            $mail = new PHPMailer(true);
            $mail->SetLanguage('es');
            $mail->IsSMTP();
            $mail->SMTPAuth = true;
            $mail->Host = $parametros->par_smtp;
            $mail->Timeout=30;
            $mail->CharSet = 'utf-8';
            $mail->Subject = "Formulario de Contacto - Nuevo Mensaje de " . $_POST['email'];
            $body = $contenido;
            $mail->MsgHTML($body);
            $infoEmail = Private_Controller::getEmail();
            $mail->AddAddress($infoEmail);
            $mail->SetFrom($cue_nombre, model_parametros::SITE_DOMAIN);
            $mail->Username = $cue_nombre;
            $mail->Password = $cue_pass;

            if($mail->Send())
            {
                $sendMessageResponse['response'] = 1;
                $sendMessageResponse['message'] = '<div class="alert alert-success">Su mensaje fue enviado correctamente</div>';
            }
        }
        return $sendMessageResponse;
    }

    public function send_mail_agente()
    {
        $validationResponse = static::validateToken($_POST["g-recaptcha-response"]);
        $sendMessageResponse = array("response" => 0,"message" => 'Su mensaje no pudo ser enviado.');
        if($validationResponse->success)
        {
            require_once(BASE_DIRECTORY . DS . 'admin' . DS . 'clases' . DS. 'phpmailer' . DS . 'class.phpmailer.php');
            $parametros = $this->model_parametros->get_all();

            $template = base_url() . "assets/template_mail/mensaje_contacto_agentes.html";
            $contenido = $this->obtener_contenido($template);
            $contenido = str_replace('@@nombre',$_POST['name'],$contenido);
            $contenido = str_replace('@@razon_social',$_POST['razon_social'],$contenido);
            $contenido = str_replace('@@nit',$_POST['nit'],$contenido);
            $contenido = str_replace('@@telefono',$_POST['phone'],$contenido);
            $contenido = str_replace('@@email',$_POST['email'],$contenido);
            $contenido = str_replace('@@fundaempresa',$_POST['fundaempresa'],$contenido);
            $contenido = str_replace('@@experiencia',$_POST['experiencia'],$contenido);
            $contenido = str_replace('@@comentario',nl2br($_POST['message']),$contenido);

            $cue_nombre = $parametros->par_salida;
            $cue_pass = $parametros->par_pas_salida;

            $mail = new PHPMailer(true);
            $mail->SetLanguage('es');
            $mail->IsSMTP();
            $mail->SMTPAuth = true;
            $mail->Host = $parametros->par_smtp;
            $mail->Timeout=30;
            $mail->CharSet = 'utf-8';
            $mail->Subject = "Agente Inmobiliario Interesado - " . $_POST['email'];
            $body = $contenido;
            $mail->MsgHTML($body);
            $carlaEmail = Private_Controller::getEmail('carla');
            $mail->AddAddress($carlaEmail);
            $infoEmail = Private_Controller::getEmail();
            $mail->AddCC($infoEmail);
            $mail->SetFrom($cue_nombre, model_parametros::SITE_DOMAIN);
            $mail->Username = $cue_nombre;
            $mail->Password = $cue_pass;

            if($mail->Send())
            {
                $sendMessageResponse['response'] = 1;
                $sendMessageResponse['message'] = 'Su mensaje fue enviado correctamente';
            }
        }
        return $sendMessageResponse;
    }

    private function obtener_contenido($url)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.3) Gecko/20070309 Firefox/2.0.0.3");
        curl_setopt($curl, CURLOPT_URL, $url);
        $contenido = curl_exec($curl);
        curl_close($curl);

        return $contenido;
    }

    public function terminos() {
        $data['contenido'] = $this->model_contenido->get_all(7);
        $meta = array(
            'description' => 'Terminoas y condiciones de uso',
            'keywords' => 'terminos condiciones toqueeltimbre'
        );
        $this->load_template('frontend/pages/terminos', "Terminos y condiciones", $data, $meta);
    }

    public function testReCaptcha()
    {
        $this->load->view("test-recaptcha");
    }
}