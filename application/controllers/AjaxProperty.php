<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 07/09/2017
 * Time: 10:25
 */

class AjaxProperty extends Base_page
{
    public function __construct()
    {
        parent::__construct();
        if(!$this->input->is_ajax_request())
        {
            redirect('404');
        }
        $this->load->model('model_user');
        $this->load->model('model_property');
        $this->load->model('model_message');
    }

    public function sendContactMessage()
    {
        $data = array();
        $formData = $this->input->post();
        $propertyId = $formData['id_inmueble'];
        $propertyUser = model_user::getByPropertyId($propertyId);
        $property = model_property::getFullDetailById($propertyId);
        $view = 'email_template/contact-message-property';
        $data['user'] = $propertyUser->toArray();
        $data['property'] = (array)$property;
        $sendMessageResponse = model_message::sendContactMessage($view,$data,'Contacto Inmueble');
        echo json_encode($sendMessageResponse);
    }
}