<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: David
 * Date: 10/10/2014
 * Time: 10:38
 */

class Publication extends Base_page{

    /**
    * Index Page for this controller.
    *
    * Maps to the following URL
    * 		http://example.com/index.php/welcome
    *	- or -
    * 		http://example.com/index.php/welcome/index
    *	- or -
    * Since this controller is set as the default controller in
    * config/routes.php, it's displayed at http://example.com/
    *
    * So any other public methods not prefixed with an underscore will
    * map to /index.php/welcome/<method_name>
    * @see http://codeigniter.com/user_guide/general/urls.html
    */

    function __construct() {
        parent::__construct();
        $this->load->model('model_usuario');
        $this->load->model('model_publicacion');
        $this->load->model('model_categoria');
        $this->load->model('model_departamento');
        $this->load->model('model_ciudad');
        $this->load->model('model_zona');
        $this->load->model('model_forma');
        $this->load->model('model_moneda');
        $this->load->model('model_inmueble');
        $this->load->model('model_caracteristica');
        $this->load->model('model_inmueble_caracteristica');
        $this->load->model('model_inmueble_foto');
        $this->load->model('model_pagos');
        $this->load->model('model_pago_servicio');
        $this->load->model('model_servicios');
        $this->load->model('model_parametros');

        $this->load->library('excel');
        $this->load->helper("url");
        $this->load->helper("form");
        $this->load->helper("util_string");
        $this->load->library('recaptcha');
        $this->load->library('form_validation');

        $this->breadcrumb['level2'] = array('name' => 'Publicaciones', 'link' => base_url().'dashboard/publications');
        $this->page_title = 'Mis Anuncios';
        $this->page = 'publication';
    }

    public function register(){
        $data['categories'] = $this->model_categoria->get_all();
        $data['states'] = $this->model_departamento->get_all();
        $data['types'] = $this->model_forma->get_all();
        $data['currency'] = $this->model_moneda->get_all();

        $sw_validate = $this->form_validate();
        if($sw_validate == true) {
            $user_id = $this->register_user($data);

            if(!empty($user_id)){
                $url_seo = seo_url($this->input->post('title'));
                $url_seo = $this->exist_url_seo($url_seo);
                $images = $this->input->post('file_upload');
                $planos = $this->input->post('upload_planos');
                $status = $this->input->post('status');

                $values = array(
                    'inm_nombre' => $this->input->post('title'),
                    'inm_direccion' => $this->input->post('address'),
                    'inm_precio' => $this->input->post('price'),
                    'inm_superficie' => $this->input->post('area'),
                    'inm_tipo_superficie' => $this->input->post('type_area'),
                    'inm_detalle' => $this->input->post('description'),
                    'inm_ciu_id' => $this->input->post('city'),
                    'inm_orden' => 9999,
                    'inm_latitud' => $this->input->post('latitude'),
                    'inm_longitud' => $this->input->post('longitude'),
                    'inm_cat_id' => $this->input->post('category'),
                    'inm_zon_id' => $this->input->post('zone'),
                    'inm_uv' => $this->input->post('uv'),
                    'inm_manzano' => $this->input->post('manzano'),
                    'inm_for_id' => $this->input->post('type'),
                    'inm_tipo_superficie' => $this->input->post('type_area'),
                    'inm_mon_id' => $this->input->post('currency'),
                    'inm_visitas' => 0,
                    'inm_publicado' => $status,
                    'inm_seo' => $url_seo
                );

                $id_inm = $this->model_inmueble->insert($values);
                $this->insert_features($this->input->post('category'), $id_inm);

                if(!empty($images)) {
                    foreach ($images as $img) {
                        $array_image = array(
                            'fot_archivo' => $img,
                            'fot_inm_id' => $id_inm
                        );
                        $this->model_publicacion->insert_images($array_image);
                    }
                }

                if(!empty($planos)) {
                    foreach ($planos as $img) {
                        $array_image = array(
                            'picture' => $img,
                            'inmueble_id' => $id_inm
                        );
                        $this->model_publicacion->insert_planos($array_image);
                    }
                }

                $desde = date('Y-m-d');
                $nuevafecha = strtotime ('+30 day', strtotime($desde));
                $hasta = date('Y-m-d', $nuevafecha);

                $values_publication = array(
                    'pub_creado' => date('Y-m-d H:i:s'),
                    'pub_vig_ini' => $desde,
                    'pub_vig_fin' => $hasta,
                    'pub_inm_id' => $id_inm,
                    'pub_estado' => 'No Aprobado',
                    'pub_usu_id' => $user_id
                );
                $publicationId = $this->model_publicacion->insert($values_publication);

                //debemos colocar un estado al anuncion para ver si  ya esta creado

                $data['particularServices'] = $this->model_servicios->get_services_by_type('Anuncios');
                $data['mainServices']       = $this->model_servicios->get_services_by_type('Principal');
                $data['bannerServices']     = $this->model_servicios->get_services_by_type('Landing');
                $data['searchServices']     = $this->model_servicios->get_services_by_type('Busqueda');
                $data['sw_subscription']    = $this->sw_subscription;
                $data['concepto'] = "Anuncio";
                $data['publicationId']      = $publicationId;

                $this->load_template_backend('frontend/payment/payment_services_step1', $data);
            }else{
                $cat_id = $this->input->post('category');
                $data['features'] = $this->features($cat_id);
                $data['recaptcha_html'] = $this->recaptcha->recaptcha_get_html();
                $this->load_template_backend('frontend/publications/register', $data);
            }
        } else {
            $cat_id = $this->input->post('category');
            $data['features'] = $this->features($cat_id);
            $data['recaptcha_html'] = $this->recaptcha->recaptcha_get_html();
            $this->load_template_backend('frontend/publications/register', $data);
        }
    }

    private function features($cat_id){
        if(!empty($cat_id)){
            $data['features'] = $this->model_caracteristica->get_all_features($cat_id);
            return $this->load->view('backend/publications/ajax_features', $data, TRUE);
        }else{
            return null;
        }
    }

    public function insert_features($cat_id, $inm_id){
        $features = $this->model_caracteristica->get_features_by_category($cat_id);

        foreach($features as $feature){
            $valor = $_POST[$feature->car_nombre];
            $values = array(
                'eca_inm_id' => $inm_id,
                'eca_car_id' => $feature->car_id,
                'eca_valor' => $valor
            );
            $this->model_inmueble_caracteristica->insert($values);
        }
    }

    private function exist_url_seo($seo){
        $sw = true;
        $i = 1;
        $aux = $seo;
        while($sw){
            $sw = $this->model_publicacion->exist_url_seo($aux);
            if($sw){
                $aux = $seo . "-".$i;
                $i++;
            }
        }
        return $aux;
    }

    public function form_validate($action = "register") {

        $this->form_validation->set_rules('title', 'Titulo', 'trim|required|max_length[150]');
        $this->form_validation->set_rules('description', 'Descripcion', 'trim|required|max_length[2000]');
        $this->form_validation->set_rules('type', 'Tipo', 'required');
        $this->form_validation->set_rules('area', 'Metros cuadrados', 'required');
        $this->form_validation->set_rules('price', 'Precio', 'required');
        $this->form_validation->set_rules('category', 'Categoria', 'required');
        $this->form_validation->set_rules('state', 'Departamento', 'required');
        $this->form_validation->set_rules('city', 'Ciudad', 'required');
        $this->form_validation->set_rules('zone', 'Zona', 'required');
        $this->form_validation->set_rules('g-recaptcha-response','Captcha','callback_recaptcha');
        $this->form_validation->set_rules('email','Email','callback_verify_email');

        if($action == "register"){
            $this->form_validation->set_rules('file_upload', 'Imagen', 'required');
        }

        $this->form_validation->set_message('required', 'El campo %s es requerido.');
        if(strlen($this->input->post('title')) > 150){
            $this->form_validation->set_message('max_length', 'El titulo solo puede tener una longitud maxima de 150 caracteres.');
        }elseif(strlen($this->input->post('description')) > 2000){
            $this->form_validation->set_message('max_length', 'La descripcion solo puede tener una longitud maxima de 2000 caracteres.');
        }

        if($this->form_validation->run() == TRUE){
            return true;
        }else{
            return false;
        }
    }

    public function load_template_backend($view, $data = array(), $title = 'Panel de Administracion') {
        $user_info = $this->model_usuario->get_info_agent($this->user->usu_id);
        $data['title'] = $title;
        $data['view'] = $view;
        $data['mobile'] = $this->agent->is_mobile();
        $data['page'] = $this->page;
        $data['page_title'] = $this->page_title;
        $data['breadcrumb'] = breadcrumb_nav($this->breadcrumb);

        $this->load->view('template_backend_anuncio/base_template',$data);
    }

    private function register_user($data){
        if($_POST){
            $key = $this->generate_key(9);

            $social = $this->input->post('social');
            $status = "Deshabilitado";
            $password = sha1($this->input->post('password'));

            $values = array(
                "usu_nombre" => '',
                "usu_apellido" => '',
                "usu_email" => $this->input->post('email'),
                "usu_password" => $password,
                "usu_telefono" => '',
                "usu_ciu_id" => 0,
                "usu_empresa" => '',
                "usu_estado" => $status,
                "usu_tipo" => 'Particular',
                "usu_fecha_registro" => date("Y-m-d H:i:s"),
                "usu_key" => $key
            );
            $id_user = $this->model_usuario->insert($values);
            return $id_user;
        }
    }

    public function recaptcha(){
        $this->recaptcha->recaptcha_check_answer();
        if($this->recaptcha->getIsValid()) {
            return TRUE;
        }else{
            if(!$this->recaptcha->getIsValid()) {
                $error = 'El captcha ingresado es invalido.';
            } else {
                $error = 'Error al ejecutar el captcha.';
            }
            $this->form_validation->set_message('recaptcha', $error);
            return FALSE;
        }
    }

    public function verify_email(){
        $email = $this->input->post('email');
        $sw_email = $this->model_usuario->email_is_registered($email);

        if($sw_email){
            $this->form_validation->set_message('verify_email', "El email ya esta en uso, intente con otro diferente.");
            return false;
        }else{
            return true;
        }
    }

    private function generate_key($longitud) {
        $key = '';
        $pattern = '1234567890abcdefghijklmnopqrstuvwxyz';
        $max = strlen($pattern)-1;
        for($i=0;$i < $longitud;$i++) $key .= $pattern{mt_rand(0,$max)};
        return $key;
    }

    public function payment() {
        $publicationId = $this->uri->segment(3);

        if(!empty($_POST)) {
            $this->form_validation->set_rules('anuncio', 'Anuncio', 'trim|required');

            $this->form_validation->set_message('required', 'Seleccionar los dias para su anuncio');

            if ($this->form_validation->run()) {
                $paymentType   = NULL;
                $paymentDate    = date( "Y-m-d H:i:s" );
                $endPaymentDate = strtotime( '+14 days', strtotime( $paymentDate ) );
                $endPaymentDate = date( 'Y-m-d H:i:s', $endPaymentDate );
                $totalPayment = $_POST['paymentTotal'];
                $estadoPago = 'Pendiente';
                $fechaPagado = NULL;

                $anuncio = $_POST['anuncio'];
                if(empty($anuncio) || $anuncio == null){
                    $anuncio = 0;
                }

                if ($anuncio == 20) {
                    $publicationAux = $this->model_publicacion->get_created_publication($publicationId);
                    $values = array(
                        "pub_vig_fin" => date('Y-m-d H:i:s', strtotime('+30 days', strtotime($publicationAux->pub_vig_fin)))
                    );
                    $this->model_publicacion->update($publicationId, $values);
                }

                $paymentParams = array (
                    'paymentType' => $paymentType,
                    'paymentDate' => $paymentDate,
                    'endPaymentDate' => $endPaymentDate,
                    'totalPayment' => $totalPayment,
                    'services' => array($anuncio, $_POST['principal'], $_POST['banner'], $_POST['busqueda']),
                    'estado' => $estadoPago,
                    'concepto' => $_POST['concepto'],
                    'fechaPago' => $fechaPagado
                );

                $paymentType   = $paymentParams['paymentType'];
                $paymentDate    = $paymentParams['paymentDate'];
                $endPaymentDate = $paymentParams['endPaymentDate'];
                $totalPayment = $paymentParams['totalPayment'];
                $services = $paymentParams['services'];
                $estado = $paymentParams['estado'];
                $concepto = $paymentParams['concepto'];
                $fechaPago = $paymentParams['fechaPago'];

                $sw   = true;
                while ($sw) {
                    $pag_id = $this->codigoAlfanumerico(7);
                    if (!$this->model_pagos->exist($pag_id)) {
                        $sw = false;
                    }
                }

                $userName = "S/N";
                if ( isset( $_POST["userName"] ) && $_POST["userName"] != "" ) {
                    $userName = $_POST["userName"];
                }

                $userNit = 0;
                if ( isset( $_POST["userNit"] ) && $_POST["userNit"] != "" ) {
                    $userNit = $_POST["userNit"];
                }

                $payment = array(
                    "pag_id"        => $pag_id,
                    "pag_pub_id"    => $publicationId,
                    "pag_fecha"     => $paymentDate,
                    "pag_nombre"    => $userName,
                    "pag_nit"       => $userNit,
                    "pag_monto"     => $totalPayment,
                    "pag_estado"    => $estado,
                    "pag_entidad"   => $paymentType,
                    "pag_concepto"   => $concepto,
                    "pag_fecha_ven" => $endPaymentDate,
                    "pag_fecha_pagado" => $fechaPago
                );

                $paymentId = $this->model_pagos->insert( $payment );

                foreach($services as $serviceId) {
                    if($serviceId != 0) {
                        if (!$this->sw_subscription) {
                            $pago_servicio = array(
                                "pag_id" => $pag_id,
                                "ser_id" => $serviceId
                            );
                        }else{
                            $pago_servicio = array(
                                "pag_id" => $pag_id,
                                "ser_id" => $serviceId,
                                "fech_ini" => date('Y-m-d'),
                                "fech_fin" => date('Y-m-d', strtotime('+30 days'))
                            );
                        }
                        $this->model_pago_servicio->insert($pago_servicio);
                    }
                }

                $Descripcion = "PAGO DE ANUNCIO";
                $result = array();
                $data = array();
                $pago_id = $pag_id;
                $MontoTotal = $totalPayment;
                $fecha_vencimiento = $endPaymentDate;
                $fecha = $paymentDate;
                $nombre_fact = $userName;
                $nit_fact = $userNit;
                include BASE_DIRECTORY . DS . 'sintesis' . DS . 'servicios' . DS . 'clientePagosNet.php';

                $data['errorSintesis'] = $errorSintesis;
                if($errorSintesis == "No"){
                    $paymentUp = array(
                        "pag_cod_transaccion" => $result["cmeRegistroPlanResult"]["idTransaccion"],
                    );
                    $this->model_pagos->update_transaccion_id($pago_id, $paymentUp);
                }

                $data['pagoId']       = $pag_id;
                $data['totalPayment'] = $totalPayment;
                $data['paymentType']  = $_POST['paymentType'];

                //$this->load_template_backend('backend/payment/payment_confirmation', $data);

                //if (!$this->sw_subscription) {
                // Email notificacion de pagos al admin del sitio para seguimiento
                $aux_inm  = $this->model_publicacion->get_name_inmueble_by_pub_id($publicationId);
                $user_email = $this->model_usuario->get_user_by_publication($publicationId);
                $concepto = "#".$aux_inm->inm_id . " - ".$aux_inm->inm_nombre;
                $usuario = $user_email->nombre;
                $telefono = $user_email->usu_telefono . " - " . $user_email->usu_celular;
                $email = $user_email->usu_email;
                $monto = $totalPayment;
                $tipo = "Anuncio Estandar";
                $forma = $_POST['paymentType'];
                $facturacion_name = $_POST["userName"];
                $facturacion_nit = $_POST["userNit"];
                $this->send_mail_pago_notificacion($concepto,$usuario,$telefono,$email,$pag_id,$monto,$tipo,$forma,$facturacion_name,$facturacion_nit);

                // Email CODIGDO DE PAGO para que el cliente pase por PAGOSNET
                $this->send_mail_codigo_pago_not_cliente($concepto,$email,$pag_id, $monto);

                $this->load_template_backend( 'frontend/payment/payment_mandatory', $data);
                //}
            } else {
                $data['mainServices']       = $this->model_servicios->get_services_by_type('Principal');
                $data['bannerServices']     = $this->model_servicios->get_services_by_type('Landing');
                $data['searchServices']     = $this->model_servicios->get_services_by_type('Busqueda');
                $data['particularServices'] = $this->model_servicios->get_services_by_type('Anuncios');
                $data['concepto'] = $_POST['concepto'];
                $data['publicationId']      = $publicationId;

                $this->load_template_backend('frontend/payment/payment_services_step1', $data);
            }
        }
    }

    public function send_mail_pago_notificacion($concepto, $usuario, $telefono, $email, $pag_id, $monto, $tipo, $forma,$facName,$facNit){
        require_once(BASE_DIRECTORY . DS . 'admin' . DS . 'clases' . DS. 'phpmailer' . DS . 'class.phpmailer.php');
        $parametros = $this->model_parametros->get_all();

        $template = base_url() . "assets/template_mail/mail_pago_notificacion_admin.html";
        $contenido = $this->obtener_contenido($template);
        $contenido = str_replace('@@concepto',$concepto,$contenido);
        $contenido = str_replace('@@telefono',$telefono,$contenido);
        $contenido = str_replace('@@email',$email,$contenido);
        $contenido = str_replace('@@pag_id',$pag_id,$contenido);
        $contenido = str_replace('@@monto',$monto,$contenido);
        $contenido = str_replace('@@tipo',$tipo,$contenido);
        $contenido = str_replace('@@fac_name',$facName,$contenido);
        $contenido = str_replace('@@fac_nit',$facNit,$contenido);

        $cue_nombre = $parametros->par_salida;
        $cue_pass = $parametros->par_pas_salida;

        $mail = new PHPMailer(true);
        $mail->SetLanguage('es');
        $mail->IsSMTP();
        $mail->SMTPAuth = true;
        $mail->Host = $parametros->par_smtp;
        $mail->Timeout=30;
        $mail->CharSet = 'utf-8';
        $mail->Subject = "Nuevo Pago #" . $pag_id;
        $body = $contenido;
        $mail->MsgHTML($body);
        $mail->AddReplyTo($email, $usuario);
        $mail->AddAddress("pagos@toqueeltimbre.com");
        //$mail->AddAddress("cristian.inarra@gmail.com");
        $mail->SetFrom($cue_nombre, model_parametros::SITE_DOMAIN);
        $mail->Username = $cue_nombre;
        $mail->Password = $cue_pass;

        $mail->Send();
    }

    public function send_mail_codigo_pago_not_cliente($inmueble, $email, $pag_id, $monto){
        require_once(BASE_DIRECTORY . DS . 'admin' . DS . 'clases' . DS. 'phpmailer' . DS . 'class.phpmailer.php');
        $parametros = $this->model_parametros->get_all();

        $template = base_url() . "assets/template_mail/codigo_pago_anuncios.html";
        $contenido = $this->obtener_contenido($template);
        $contenido = str_replace('@@inmueble',$concepto,$contenido);
        $contenido = str_replace('@@pag_id',$pag_id,$contenido);
        $contenido = str_replace('@@monto',$monto,$contenido);

        $cue_nombre = $parametros->par_salida;
        $cue_pass = $parametros->par_pas_salida;

        $mail = new PHPMailer(true);
        $mail->SetLanguage('es');
        $mail->IsSMTP();
        $mail->SMTPAuth = true;
        $mail->Host = $parametros->par_smtp;
        $mail->Timeout=30;
        $mail->CharSet = 'utf-8';
        $mail->Subject = "Nuevo Pago #" . $pag_id;
        $body = $contenido;
        $mail->MsgHTML($body);
        //$mail->AddReplyTo($email, $usuario);
        $mail->AddAddress($email);
        //$mail->AddAddress("cristian.inarra@gmail.com");
        $mail->SetFrom($cue_nombre, model_parametros::SITE_DOMAIN);
        $mail->Username = $cue_nombre;
        $mail->Password = $cue_pass;

        $mail->Send();
    }

    public function codigoAlfanumerico($length=10,$uc=FALSE,$n=TRUE,$sc=FALSE,$min=FALSE) {
        $source = '';
        if($min==1) $source .= 'abcdefghijklmnopqrstuvwxyz';;
        if($uc==1) $source .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        if($n==1) $source .= '1234567890';
        if($sc==1) $source .= '|@#~$%()=^*+[]{}-_';
        if($length>0){
            $rstr = "";
            $source = str_split($source,1);
            for($i=1; $i<=$length; $i++){
                mt_srand((double)microtime() * 1000000);
                $num = mt_rand(1,count($source));
                $rstr .= $source[$num-1];
            }
        }
        return "217".$rstr;
    }

    private function obtener_contenido($url)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.3) Gecko/20070309 Firefox/2.0.0.3");
        curl_setopt($curl, CURLOPT_URL, $url);
        $contenido = curl_exec($curl);
        curl_close($curl);

        return $contenido;
    }


    public function tigomoney(){

        if($_POST){
            $order_pay = $this->model_pagos->get_payment_detail_tigomoney($_POST['pag_id']);
            $url_return = base_url() . "publications/ipn";

            $pv_nroDocumento= "pv_nroDocumento=".$order_pay->usu_ci;
            $pv_orderId="pv_orderId=".$order_pay->pag_id;
            $monto = round($order_pay->pag_monto, 0);
            $pv_monto="pv_monto=".$monto;
            //$pv_monto="pv_monto=1";
            $pv_linea="pv_linea=".$_POST['linea'];
            $pv_nombre="pv_nombre=".$order_pay->nombre_completo;
            $pv_urlCorrecto = "pv_urlCorrecto=".$url_return;
            $pv_urlError = "pv_urlError=".$url_return;
            $pv_confirmacion = "pv_confirmacion=Toqueeltimbre";
            $pv_notificacion = "pv_notificacion=Orden de Pago ".$order_pay->pag_id;
            $pv_items= "pv_items=*i1|1|Servicios de Toqueeltimbre|".$order_pay->pag_monto."|".$order_pay->pag_monto;
            $pv_razonSocial= "pv_razonSocial=".$order_pay->pag_nombre;
            $pv_nit= "pv_nit=".$order_pay->pag_nit;

            require BASE_DIRECTORY . DS . 'sintesis' . DS . 'tigomoney' . DS . 'CTripleDes.php';
            $montant = $pv_nroDocumento.";".$pv_orderId.";".$pv_monto.";".$pv_linea.";".$pv_nombre.";".$pv_urlCorrecto.";".$pv_urlError.";".$pv_confirmacion.";".$pv_notificacion.";".$pv_items.";".$pv_razonSocial.";".$pv_nit;

            $keyPublica = "cb6ce94b52d5fc21bfa35ad64baaff6301c154ac981180ec994d2ca64114e2899d21bb596011f52bb913b7ea3152d375490384bf7d99d3562dbf00021e826f1f";
            $keyPrivada = "PHF07Z8GAFBJTZAYBRGNCYN0";

            $tripleDes = new CTripleDes();
            $tripleDes->setMessage($montant);
            $tripleDes->setPrivateKey($keyPrivada);
            $encrypt = $tripleDes->encrypt();

            header("Location: https://vipagos.com.bo/vipagos/faces/payment.xhtml?key=".$keyPublica."&parametros=".$encrypt);
        }else{
            $this->load->view("frontend/payment/iframe_tigomoney", array());
        }
    }

    public function ipn(){
        $this->load->model('model_inmueble');
        $this->load->model('model_suscripcion');
        $values = $this->get_tigomoney_decrypt($_GET['r']);
        $sw_result = $values['codRes'];

        $id_pago = trim($values['orderId']);
        $user = $this->model_pagos->get_user_by_paid($id_pago);
        $data['user_info'] = $this->model_usuario->get_info_agent($user->usu_id);

        if($sw_result == 0 && !empty($sw_result)){

            $obj_paid = $this->model_pagos->get_id_paid($id_pago);
            $sw_pago = $this->model_pagos->verify_id_paid($id_pago);

            if(!$sw_pago){
                $codigos_servicios = $this->model_pagos->get_services($id_pago);
                $email = $this->model_pagos->get_email_idpag($id_pago);
                $fecha_pagado = date("Y-m-d H:i:s");

                $pago_values = array(
                    "pag_estado" => 'Pagado',
                    'pag_entidad' => 'TigoMoney',
                    'pag_fecha_pagado' => $fecha_pagado
                );

                $this->model_pagos->update($id_pago, $pago_values);

                if($obj_paid->pag_concepto == "Suscripcion"){
                    $total = 0;
                    foreach($codigos_servicios as $row){
                        if($row->ser_id != ""){
                            $obj_servicio = $this->model_servicios->get_id($row->ser_id);
                            $fech_inicio = date("Y-m-d");
                            $fec_vencimi = date("Y-m-d", strtotime($fech_inicio. " + " . $obj_servicio->ser_dias . " days"));

                            $pag_service_values = array(
                                'fech_ini' => $fech_inicio,
                                'fech_fin' => $fec_vencimi
                            );

                            $this->model_pago_servicio->update($id_pago, $row->ser_id, $pag_service_values);
                        }
                    }
                    $obj_values = array(
                        'fecha_inicio' => $fech_inicio,
                        'fecha_fin' => $fec_vencimi,
                        'estado' => "Activo"
                    );
                    $this->model_suscripcion->update_by_pag_id($id_pago, $obj_values);
                    $this->pay_publications_pending_suscription($user->usu_id);

                    $total_pagado = $this->model_pagos->get_total_paid($id_pago);
                    $this->enviar_mail($total_pagado, $email, "Pago por suscripcion");
                }else{
                    $total = 0;
                    foreach($codigos_servicios as $row){
                        if($row->ser_id != ""){
                            $obj_servicio = $this->model_servicios->get_id($row->ser_id);
                            $id_pub = $this->model_pagos->get_id_publication($id_pago);

                            $fecha_fin_anterior_pago = $this->model_pagos->verify_exist_other_paids($id_pago, $id_pub, $obj_servicio->ser_id);
                            if(!empty($fecha_fin_anterior_pago)){
                                $fech_inicio = $fecha_fin_anterior_pago;
                            }else{
                                $fech_inicio = date("Y-m-d");
                            }

                            $fec_vencimi = date("Y-m-d", strtotime($fech_inicio. " + " . $obj_servicio->ser_dias . " days"));
                            $pag_service_values = array(
                                'fech_ini' => $fech_inicio,
                                'fech_fin' => $fec_vencimi
                            );
                            $this->model_pago_servicio->update($id_pago, $row->ser_id, $pag_service_values);

                            $total = $total + $obj_servicio->ser_precio;
                        }
                    }
                    $publicacion = $this->model_pagos->get_publication($id_pago);
                    $mayor_fecha = $this->model_pagos->get_max_date_service($publicacion->pub_id);
                    if($mayor_fecha > $publicacion->pub_vig_fin){
                        $pub_vig_fin = $mayor_fecha;
                        $pub_values = array(
                            'pub_estado' => 'Aprobado',
                            'pub_vig_fin' => $pub_vig_fin
                        );
                    }else{
                        $pub_values = array(
                            'pub_estado' => 'Aprobado'
                        );
                    }
                    $this->model_publicacion->update($publicacion->pub_id, $pub_values);
                    $obj_inmueble = $this->model_pagos->get_id_inmueble($publicacion->pub_id);
                    $this->model_inmueble->update($obj_inmueble->inm_id, array('inm_publicado' => 'Si'));

                    $total_pagado = $this->model_pagos->get_total_paid($id_pago);
                    $this->enviar_mail($total_pagado, $email, $obj_inmueble->inm_nombre);
                }

                $data['pagoId'] = $id_pago;
                $data['totalPayment'] = $total_pagado;
                $data['paymentType'] = "TIGOMONEY";
                $data['errorSintesis'] = "No";

                $this->load_template_backend('frontend/payment/payment_confirmationtigo', $data);
            }else{
                $id_pago = trim($values['orderId']);
                $total_pagado = $this->model_pagos->get_total_paid($id_pago);

                $data['pagoId'] = $id_pago;
                $data['totalPayment'] = $total_pagado;
                $data['paymentType'] = "TIGOMONEY";
                $data['errorSintesis'] = "No";

                $this->load_template_backend('frontend/payment/payment_confirmationtigo', $data);
            }

        }else{
            $data["result"] = $sw_result;
            $this->load_template_backend('frontend/payment/payment_failed_tigomoney', $data);
        }

    }

}
