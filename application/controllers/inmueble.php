<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: David
 * Date: 10/10/2014
 * Time: 10:38
 */

class Inmueble extends Base_page
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('model_inmueble');
        $this->load->model('model_inmueble_foto');
        $this->load->model('model_publicacion');
        $this->load->model('model_departamento');
        $this->load->model('model_categoria');
        $this->load->model('model_forma');
        $this->load->model('model_parametros');
        $this->load->model('model_mensaje');
        $this->load->model('model_usuario');
        $this->load->model('model_user');
        $this->load->model('model_property');

        $this->load->helper('url');
    }

    public function index()
    {
        $this->complementHandler->addViewComplement("google.maps.api");
        $this->complementHandler->addViewComplement("gmaps");
        $this->complementHandler->addViewComplement("bootbox");
        $this->complementHandler->addViewComplement("handlebars");
        $this->complementHandler->addProjectJs("RegisterVisitor");
        $this->complementHandler->addPublicJs('handlerbars.custom.helpers');
        $this->complementHandler->addProjectJs("MapHandler");
        $this->complementHandler->addProjectCss("inmueble.index");
        $this->complementHandler->addProjectJs("facebook.login");
        $this->complementHandler->addProjectJs("quick-login");
        $this->complementHandler->addProjectCss("quick-login");
        $this->complementHandler->addProjectJs("inmueble.index");
        $url = $this->uri->segment(3);
        $inm_seo = str_replace(".html","", $url);
        $inmueble = $this->model_inmueble->get_by_url($inm_seo);
        $url = "{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
        $this->session->set_userdata(array("lastPropertyViewed" => $url));
        if(!empty($inmueble))
        {
            $inmueble->caracteristicas = $this->model_inmueble->get_caracteristicas($inmueble->id);
            $inmueble->fotos = $this->model_inmueble_foto->get_fotos($inmueble->id);
            $inmueble->planos = $this->model_inmueble_foto->get_planos($inmueble->id);
            $inmueble->similar = $this->model_publicacion->get_similar_properties($inmueble->id_forma,$inmueble->id_ciudad, $inmueble->id_categoria, $inmueble->id_zona, $inmueble->id);
            $data['favorite'] = $this->check_favorite($this->user_id, $inmueble->id);
            $data['inmueble'] = $inmueble;
        }
        else
        {
            $data['inmueble'] = array();
        }

        $now = date("Y-m-d");
        $sw_fecha = $inmueble->pub_vig_fin <= $now ? true : false;
        if(empty($inmueble) || $sw_fecha) {
            $ciudad = $this->uri->segment(1);
            $categoria = $this->uri->segment(2);
            $ciudad = str_replace("-", " ", $ciudad);
            $categoria = explode("-", $categoria);
            $data['relacionadas'] = $this->model_publicacion->get_relacionadas_empty_properties($ciudad, $categoria[0]);
        }

        $data["back_search"] = $this->session->userdata('back_search');

        $keywords_array = explode(" ", $inmueble->nombre);
        $keywords_array = keywords_filter($keywords_array);
        $keywords_array = implode(",", $keywords_array);
        $keywords_array = strtolower($keywords_array);
        $meta = array(
            "title" => !empty($inmueble->nombre) ? crop_string($inmueble->nombre, 80) : ".: ToqueElTimbre.com :.",
            "keywords" => $keywords_array,
            "description" => crop_string(strip_tags($inmueble->detalle), 200)
        );

	    $this->session->set_userdata('url_inm_detalle', $this->uri->uri_string());
	    $this->breadcrumbs->push('Inicio', '/');
	    $this->breadcrumbs->push('Buscar', '/buscar');
	    $this->breadcrumbs->push('Detalle de Inmueble', '/inmueble/index/');
	    $data['breacrumbs'] = $this->breadcrumbs->show();
        $data["reCaptchaKeys"] = static::getReCaptchaKeys();
        /****** facebook  */
        $data["facebookShareButtonUrl"] = current_url();
        $facebookMetaTags = $this->_getFacebookMetaTags($inmueble);
        $data["facebookMetaTags"] = $facebookMetaTags;
        $facebookAppIdAndSecretKey = Base_page::getFacebookAppIdAndSecretKey();
        $data["facebookAppIdAndSecretKey"] = $facebookAppIdAndSecretKey;
        /****** facebook  */
        $this->load_template('frontend/inmueble/detalle_inmueble', $meta["title"], $data, $meta);
    }

    public function index_deprecated(array $primaryFilterArrayData = array())
    {
        $this->complementHandler->addProjectCss("inmueble.index");
        $this->complementHandler->addProjectJs("inmueble.index");
        $data['categories'] = $this->model_categoria->get_all();
        $data['cities'] = $this->model_departamento->get_all();
        $data['forma'] = $this->model_forma->get_all();
        $url = $this->uri->segment(3);
        $inm_seo = str_replace(".html","", $url);
        $inmueble = $this->model_inmueble->get_by_url($inm_seo);
        $this->session->set_userdata(array("lastPropertyViewed" => $url));
        if(!empty($inmueble))
        {
            $inmueble->caracteristicas = $this->model_inmueble->get_caracteristicas($inmueble->id);
            $inmueble->fotos = $this->model_inmueble_foto->get_fotos($inmueble->id);
            $inmueble->planos = $this->model_inmueble_foto->get_planos($inmueble->id);
            $inmueble->similar = $this->model_publicacion->get_similar_properties($inmueble->id_forma,$inmueble->id_ciudad, $inmueble->id_categoria, $inmueble->id_zona, $inmueble->id,4);
            $data['projects'] = $this->model_inmueble->get_main_pay_projects(4);
            $data['favorite'] = $this->check_favorite($this->user_id, $inmueble->id);
            $data['inmueble'] = $inmueble;
            $this->registerLogVisitor('Inmueble', $inmueble->id);
            $this->session->set_userdata(array("lastPropertyIdViewed" => $inmueble->id));
        }
        else
        {
            $data['projects'] = $this->model_inmueble->get_main_pay_projects(4);
            $data['inmueble'] = array();
        }

        $now = date("Y-m-d");
        $sw_fecha = $inmueble->pub_vig_fin <= $now ? true : false;
        if(empty($inmueble) || $sw_fecha)
        {
            $ciudad = $primaryFilterArrayData["city"];//$this->uri->segment(1);
            $categoria = $primaryFilterArrayData["transactionType"];//$this->uri->segment(2);
            $ciudad = str_replace("-", " ", $ciudad);
            $categoria = explode("-", $categoria);
            $data['relacionadas'] = $this->model_publicacion->get_relacionadas_empty_properties($ciudad, $categoria[0]);
        }

        $data["back_search"] = $this->session->userdata('back_search');

        $keywords_array = explode(" ", $inmueble->nombre);
        $keywords_array = keywords_filter($keywords_array);
        $keywords_array = implode(",", $keywords_array);
        $keywords_array = strtolower($keywords_array);
        $meta = array(
            "title" => !empty($inmueble->nombre) ? crop_string($inmueble->nombre, 80) : ".: ToqueElTimbre.com :.",
            "keywords" => $keywords_array,
            "description" => crop_string(strip_tags($inmueble->detalle), 200)
        );

        $this->session->set_userdata('url_inm_detalle', $this->uri->uri_string());
        $this->breadcrumbs->push('Inicio', '/');
        $this->breadcrumbs->push('Buscar', '/buscar');
        $this->breadcrumbs->push('Detalle de Inmueble', '/inmueble/index/');
        $data['breacrumbs'] = $this->breadcrumbs->show();
        $data["primaryFilterArrayData"] = $primaryFilterArrayData;
        $data["reCaptchaKeys"] = static::getReCaptchaKeys();
        $data["facebookShareButtonUrl"] = current_url();
        $facebookMetaTags = $this->_getFacebookMetaTags($inmueble);
        $data["facebookMetaTags"] = $facebookMetaTags;
        $this->load_template('frontend/inmueble/detalle_inmueble', $meta["title"], $data, $meta);
    }

    private function _getFacebookMetaTags($property)
    {
        $facebookMetaTags = array();
        if(!empty($property))
        {
            $facebookMetaTags["url"] = current_url();
            $facebookMetaTags["title"] = $property->nombre;
            $facebookMetaTags["description"] = $property->detalle;
            $image = "";
            if(isset($property->fotos[0]))
            {
                $img = $property->fotos[0];

                $fileHandler = new File_Handler($img->fot_archivo);
                $thumbnail = $fileHandler->getThumbnail(850,470);
                $image = $thumbnail->getSource();
            }
            $facebookMetaTags["image"] = $image;
            $facebookMetaTags["site_name"] = "ToqueElTimbre";
            $facebookAppId = Base_page::getFacebookAppIdAndSecretKey();
            $facebookMetaTags["app_id"] = $facebookAppId["appId"];
        }
        return $facebookMetaTags;
    }

    public function mapa()
    {
        $this->load->view("frontend/inmueble/iframe_mapa");
    }

    public function favorito(){
        $id_user = $this->user_id;
        $inmueble_id = $_POST["id_inmueble"];

        // Si existe usuario guardamos en BD, sino en session
        if(!empty($id_user)){
            // Verificamos si el favorito ya esta agregado, para no volverlo a insertar
            if(!$this->check_favorite($id_user, $inmueble_id)){
                $values = array(
                    "inmueble_id" => $inmueble_id,
                    "usuario_id" => $id_user
                );
                $this->model_favorito->insert($values);
                echo "true";
            }else{
                $obj = $this->model_favorito->get_id_favorite($id_user, $inmueble_id);
                if(!empty($obj)){
                    $this->model_favorito->delete($obj->id);
                }
                echo "false";
            }
        }else{
            $favorite_array = $this->session->userdata('favorite');
            if(!$this->check_favorite(null, $inmueble_id)){
                $favorite_array[] = $inmueble_id;
                $this->session->set_userdata(array("favorite" => $favorite_array));
                echo "true";
            }else{
                if(!empty($favorite_array)){
                    $position = array_search($inmueble_id, $favorite_array);
                    unset($favorite_array[$position]);
                    $this->session->set_userdata(array("favorite" => $favorite_array));
                }
                echo "false";
            }
        }
    }

    public function delete_favorite(){
        $inmueble_id = $_POST["id_inmueble"];
        $favorite_array = $this->session->userdata('favorite');
        if(!empty($favorite_array)){
            $position = array_search($inmueble_id, $favorite_array);
            unset($favorite_array[$position]);
            $this->session->set_userdata(array("favorite" => $favorite_array));
        }
        if(!empty($this->user_id)){
            $this->load->model('model_favorito');
            $user_id = $this->user_id;
            $this->model_favorito->delete_favorite($inmueble_id, $user_id);
        }
    }

    public function detail_mobile()
    {
        $inmueble = $this->model_inmueble->get_by_id_detail($_POST["id_inmueble"]);
        $this->registerLogVisitor('Inmueble', $inmueble->id);

        if(!empty($inmueble)) {
            $inmueble->caracteristicas = $this->model_inmueble->get_caracteristicas($inmueble->id);
            $inmueble->fotos = $this->model_inmueble_foto->get_fotos($inmueble->id);
            $inmueble->planos = $this->model_inmueble_foto->get_planos($inmueble->id);
            $data['projects'] = $this->model_inmueble->get_main_pay_projects(4);
            $data['favorite'] = $this->check_favorite($this->user_id, $inmueble->id);
            $data['inmueble'] = $inmueble;
        }else{
            $data['projects'] = $this->model_inmueble->get_main_pay_projects(4);
            $data['inmueble'] = array();
        }
        $data["back_search"] = $this->session->userdata('back_search');
        $this->load->view('frontend/inmueble/detail_mobile', $data);
    }

    private function obtener_contenido($url)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.3) Gecko/20070309 Firefox/2.0.0.3");
        curl_setopt($curl, CURLOPT_URL, $url);
        $contenido = curl_exec($curl);
        curl_close($curl);

        return $contenido;
    }

    public function detail_search_user() {
        $id_busqueda = $this->uri->segment(3);
        $data['cities'] = $this->model_departamento->get_states();
        $data['categories'] = $this->model_categoria->get_categories();
        $data['forma'] = $this->model_forma->get_all();
        $search_cookie = $this->session->userdata("back_search");
        $data['back_search'] = $search_cookie;

        $data['busqueda_info'] = $this->model_inmueble->get_busqueda_id($id_busqueda);
        $this->load_template('frontend/detail_search_user', ".:: ToqueElTimbre ::.", $data);
    }

    public function deactivate($propertyId = "")
    {
        //$ci=&get_instance();
        //$ci->load->library("form_validation");
        if(empty($propertyId) || !is_numeric($propertyId))
        {
            $response = "Wrong request!";
        }
        else
        {
            $property = model_inmueble::getById($propertyId);
            //var_dump($property);exit;
            if($property instanceof model_inmueble)
            {
                $propertyData = array(
                    'inm_publicado' => "No"
                );
                $result = $this->model_inmueble->update($property->inm_id, $propertyData);
                $response = "Oops.. something went wrong, please try again.";
                if($result == TRUE)
                {
                    $response = "The property is no longer published!";
                }

            }
            else
            {
                $response = "Property not found!";
            }
        }


        exit($response);
        //$ci->form_validation->set_rules('deactive', 'Deactivar', 'numeric|trim|required');

//        if ($ci->form_validation->run() == FALSE)
//        {
//            $this->load_template('frontend/deactivate-publication', "Deactivating publication | ToqueElTimbre.com", $data);
//        }
//        else
//        {
//            $this->load_template('frontend/deactivate-publication', "Request completed | ToqueElTimbre.com", $data);
//        }
    }
    public function removeImage($propertyId = "", $imageName = "")
    {
        if(empty($imageName) || empty($propertyId) || !is_numeric($propertyId) )
        {
            $response = "Wrong request!";
        }
        else
        {
            $propertyImage = model_inmueble_foto::getByNameAndPropertyId($imageName, $propertyId);
            if($propertyImage instanceof model_inmueble_foto)
            {
                $result = $this->model_inmueble_foto->delete($propertyImage->fot_id);
                $response = "Oops.. something went wrong, please try again.";
                if($result == TRUE)
                {
                    $response = "The image was removed!";
                }
            }
            else
            {
                $response = "Image not found!";
            }
        }


        exit($response);
        //$ci->form_validation->set_rules('deactive', 'Deactivar', 'numeric|trim|required');

//        if ($ci->form_validation->run() == FALSE)
//        {
//            $this->load_template('frontend/deactivate-publication', "Deactivating publication | ToqueElTimbre.com", $data);
//        }
//        else
//        {
//            $this->load_template('frontend/deactivate-publication', "Request completed | ToqueElTimbre.com", $data);
//        }
    }
}