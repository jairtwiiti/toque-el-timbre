<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 19/07/2017
 * Time: 12:23
 */

class Payment extends Private_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->_validateAccess();
        $this->load->model('model_usuario');
        $this->load->model('model_payment');
        $this->load->model('model_service');
        $this->load->model('model_paymentservice');
        $this->load->model('model_property');
        $this->load->model('model_publication');
        $this->load->model('model_publicacion');
        $this->load->model('model_proyecto');
        $this->load->model('model_inmueble');
        $this->load->model('visitor');
        $this->load->model('model_categoria');
        $this->load->library('image_lib');
        $this->load->library('JqdtHandler');
    }

    public function index()
    {
        $this->complementHandler->addViewComplement("jquery.datatables");
        $this->complementHandler->addViewComplement("jquery.datatables.bootstrap");
        $this->complementHandler->addViewComplement("jquery.datatables.buttons");
        $this->complementHandler->addViewComplement("jquery.datatables.buttons.bootstrap");
        $this->complementHandler->addViewComplement("jquery.datatables.buttons.flash");
        $this->complementHandler->addViewComplement("jquery.datatables.buttons.html5");
        $this->complementHandler->addViewComplement("jquery.datatables.buttons.print");
        $this->complementHandler->addViewComplement("jquery.datatables.jszip");
        $this->complementHandler->addViewComplement("jquery.datatables.pdfmake");
        $this->complementHandler->addViewComplement("jquery.datatables.vfs_fonts");
        $this->complementHandler->addViewComplement("jquery.datatables.filterdelay");
        $this->complementHandler->addViewComplement("bootstrap.datepicker");
        $this->complementHandler->addProjectJs('DTAdditionalParameterHandler');
        $this->complementHandler->addProjectJs('payment.index');
        $this->complementHandler->addProjectCss('payment.index');

        $data = array();
        $this->page = 'admin_payment';
        $this->load_template_backend('admin/payment/index', $data);
    }
}