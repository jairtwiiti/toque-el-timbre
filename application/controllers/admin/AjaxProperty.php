<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 07/09/2017
 * Time: 10:25
 */

class AjaxProperty extends Private_Controller
{
    public function __construct()
    {
        parent::__construct();
        if(! $this->input->is_ajax_request())
        {
            redirect('404');
        }
        $this->load->model('model_payment');
        $this->load->model('model_paymentservice');
        $this->load->model('model_publication');
        $this->load->model('model_property');
        $this->load->model('model_service');
        $this->load->model('model_propertyimage');
        $this->load->model('model_user');
        $this->load->model('model_city');
        $this->load->model('model_departamento');
        $this->load->model('model_ciudad');
        $this->load->library('JqdtHandler');
    }

    public function ajaxDtAllProperties()
    {
        $dt = new JqdtHandler($this->input->post());
        $additionalParameters = $this->input->post("additionalParameters");

        if($this->user->usu_tipo != "Admin")
        {
            $additionalParameters["user"] = $this->user->usu_id;
            $additionalParameters["date-to-filter"] = "start";
        }
        $recordsTotal = model_property::countAll();
        $recordsFiltered = $recordsTotal;
        if (!$dt->hasSearchValue() and count($additionalParameters)<=1)
        {
            $resultArray = model_property::getAll($dt->getLength(), $dt->getStart(), $dt->getOrderName(0), $dt->getOrderDir(0));
        }
        else
        {
            $resultArray = model_property::search($dt->getSearchValue(), $dt->getLength(), $dt->getStart(), $dt->getOrderName(0), $dt->getOrderDir(0), $dt->getSearchableColumnDefs(),$additionalParameters);
            $recordsFiltered = model_property::searchTotalCount($dt->getSearchValue(),$dt->getSearchableColumnDefs(),$additionalParameters);
        }

        echo $dt->getJsonResponse($recordsTotal, $recordsFiltered, $resultArray);
        exit;
    }

    public function approve()
    {
        /** @var model_payment $payment */
        $formData = $this->input->post();
        $paymentId = $formData["paymentId"];
        $paymentMethod = $formData["paymentMethod"];
        $payment = model_payment::getById($paymentId);
        $response = $payment->approve($paymentMethod);
        echo json_encode($response);exit;
    }

    public function getDetail()
    {
        $formData = $this->input->post();
        $recordId = $formData["recordId"];
        $publicationDetailById = model_publication::getPublicationDetailByIdComposedData($recordId);
        $publicationDetail = array();
        if(count($publicationDetailById)>0)
        {
            foreach($publicationDetailById as $detail)
            {
                $publicationDetail["publicationId"] = $detail->pub_id;
                $publicationDetail["propertyId"] = $detail->inm_id;
                $publicationDetail["propertyName"] = $detail->inm_nombre;
                $publicationDetail["currency"] = $detail->mon_abreviado;
                $publicationDetail["publicationCreatedOn"] = $detail->pub_creado;
                $publicationDetail["publicationStartValidation"] = $detail->pub_vig_ini;
                $publicationDetail["publicationEndValidation"] = $detail->pub_vig_fin;
                $publicationDetail["propertyVisits"] = $detail->inm_visitas;
                $file = new File_Handler($detail->fot_archivo);
                $thumbnail = $file->getThumbnail("150","100");
                $publicationDetail["src"] = $thumbnail->getSource();
            }
        }
        echo json_encode($publicationDetail);exit;
    }

    public function getImages()
    {
        $formData = $this->input->post();
        $propertyId = $formData["propertyId"];
        $imageList = model_propertyimage::getByPropertyId($propertyId);
        $arrayImageList = array();
        $i=0;
        foreach($imageList as $image)
        {
            $arrayItem = $image->toArray();
            $file = new File_Handler($arrayItem["fot_archivo"]);
            $thumbnail = $file->getThumbnail("150","100");
            $arrayItem["src"] = $thumbnail->getSource();
            $arrayImageList[] = $arrayItem;
            $i++;
        }
        echo json_encode($arrayImageList);exit;
    }

    public function getImage()
    {
        $formData = $this->input->post();
        $propertyImageId = $formData["propertyImageId"];
        $propertyImage = model_propertyimage::getById($propertyImageId);
        $arrayPropertyImage = $propertyImage->toArray();
        $file = new File_Handler($arrayPropertyImage["fot_archivo"]);
        $thumbnail = $file->getThumbnail("150","100");
        $arrayProjectImage["src"] = $thumbnail->getSource();
        echo json_encode($arrayProjectImage);exit;
    }

    public function deleteResource()
    {
        $formData = $this->input->post();
        $itemId = $formData["itemId"];
        $propertyId = $formData["propertyId"];
        $imageList = model_propertyimage::getByPropertyId($propertyId);
        if(count($imageList) >= 2)
        {
            $item = model_propertyimage::getById($itemId);
            $item->delete(TRUE);
            $response = array("response" => 1,"message"=>"Image deleted successfully");
        }
        else
        {
            $response = array("response" => 0,"message"=>"No puedes elimitar todas tus imagenes.");
        }
        echo json_encode($response);exit;
    }

    public function getAllPublicationsRegisteredByDateRange()
    {
        $startDate = $this->input->post("startDate");
        $endDate = $this->input->post("endDate");
        $list = model_publication::getAllPublicationsRegisteredByDateRange($startDate, $endDate);
        echo json_encode((array)$list);exit;
    }

    public function getAllPublicationsActivityByDateRange()
    {
        $startDate = $this->input->post("startDate");
        $endDate = $this->input->post("endDate");
        $onlyVisiblePublications = $this->input->post("onlyVisiblePublications")=="1"?TRUE:FALSE;
        $reportType = $this->input->post("reportType");
        if($reportType == "0")
        {
            $list = model_publication::getAllPublicationsActivityByDateRange($startDate, $endDate, $onlyVisiblePublications);
        }
        else
        {
            $list = model_publication::getCreatedAndExpiredPublicationsComposedList($startDate, $endDate);
        }

        echo json_encode((array)$list);exit;
    }

    public function getPropertyOffersBasedOnUsers()
    {
        $startDate = $this->input->post("startDate");
        $endDate = $this->input->post("endDate");
        $propertyTypeId = $this->input->post("propertyTypeId");
        $transactionTypeId = $this->input->post("transactionTypeId");
        $getDemand = $this->input->post("getDemand")=="1"?TRUE:FALSE;
        $reportType = $this->input->post("reportType");
        if($reportType == "0")
        {
            $list = model_property::getPropertyOffersBasedOnUsers($startDate, $endDate, $propertyTypeId, $transactionTypeId, $getDemand);
        }
        else
        {
            $list = model_publication::getAllPublicationsByDateRangeAndVigency($startDate, $endDate);
        }
        echo json_encode((array)$list);exit;
    }

    public function getPropertyOffersBasedOnTransactionsType()
    {
        $startDate = $this->input->post("startDate");
        $endDate = $this->input->post("endDate");
        $userType = $this->input->post("userType");
        $propertyTypeId = $this->input->post("propertyTypeId");
        $getDemand = $this->input->post("getDemand")=="1"?TRUE:FALSE;
        $reportType = $this->input->post("reportType");
        if($reportType == "0")
        {
            $list = model_property::getPropertyOffersBasedOnTransactionsType($startDate, $endDate, $userType, $propertyTypeId, $getDemand);
        }
        else
        {
            $list = model_publication::getAllPublicationsByDateRangeAndVigency($startDate, $endDate);
        }

        echo json_encode((array)$list);exit;
    }

    public function getPropertyOffersBasedOnPropertyTypes()
    {
        $startDate = $this->input->post("startDate");
        $endDate = $this->input->post("endDate");
        $userType = $this->input->post("userType");
        $transactionTypeId = $this->input->post("transactionTypeId");
        $getDemand = $this->input->post("getDemand")=="1"?TRUE:FALSE;
        $reportType = $this->input->post("reportType");
        if($reportType == "0")
        {
            $list = model_property::getPropertyOffersBasedOnPropertyTypes($startDate, $endDate, $userType, $transactionTypeId, $getDemand);
        }
        else
        {
            $list = model_publication::getAllPublicationsByDateRangeAndVigency($startDate, $endDate);
        }
        echo json_encode((array)$list);exit;
    }

    public function getAllMessagesToPublicationsByDateRange()
    {
        $startDate = $this->input->post("startDate");
        $endDate = $this->input->post("endDate");
        $reportType = $this->input->post("reportType");
        if($reportType == "0")
        {
            $list = model_publication::getAllMessagesToPublicationsByDateRange($startDate, $endDate);
        }
        else
        {
            $list = model_publication::getAllMessagesToPublicationsByDateRangeDetailed($startDate, $endDate);
        }
        echo json_encode((array)$list);exit;
    }

    public function updatePublication()
    {
        $formData = $this->input->post();
        $propertyId = $formData["propertyId"];
        $isPublished = $formData['isPublished'];
        $property = model_property::getById($propertyId);
        $property->setIsPublished($isPublished);
        $property->save();
    }

    public function renewPublication()
    {
        $publicationId = $this->input->post("publicationId");
        $publication = model_publication::getById($publicationId);
        $currentExpirationDate = $publication->getFinishValidity();
        $newExpirationDate = date('Y-m-d', strtotime($currentExpirationDate . " +180 days"));
        $publication->setFinishValidity($newExpirationDate);
        $publication->save();
    }

    public function updateImageOrder()
    {
        $formData = $this->input->post();
        $imageArrayIds = $formData["ids"];

        $arrayImageToUpdate = array();
        foreach ($imageArrayIds as $order => $id)
        {
            $arrayImageToUpdate[] = array(
                                            "fot_id" => $id,
                                            "fot_order" => $order+1
                                            );
        }
        model_propertyimage::updateBatch($arrayImageToUpdate);
    }

    public function emailNewImageNotification()
    {
        $formData = $this->input->post();
        $propertyId = $formData["propertyId"];
        $propertyImageArray = $formData["propertyImageArray"];
        $property = model_property::getById($propertyId);
        $publication = NULL;

        if($property instanceof model_property)
        {
            $publication = model_publication::getByPropertyId($property->getId());
        }
        if($publication instanceof model_publication)
        {
            if(is_array($propertyImageArray) && count($propertyImageArray) > 0)
            {
                model_property::emailNewImageNotification($publication,$property,$propertyImageArray);
            }
        }
    }

    public function checkIfPendingToApproval()
    {
        $formData = $this->input->post();
        $currentItemsLoaded = $formData["currentItemsUploaded"];
        $propertyId = $formData["propertyId"];

        $publication = model_publication::getByPropertyId($propertyId);
        $publicationArray = $publication->toArray();
        $isExpired = $publicationArray["pub_vig_fin"] >= date("Y-m-d")?FALSE:TRUE;
        $isApproved = $publicationArray["pub_estado"] == "Aprobado"?TRUE:FALSE;
        $imageList = model_propertyimage::getByPropertyId($propertyId);
        if(count($imageList) == $currentItemsLoaded && !$isExpired && !$isApproved)
        {
            $publication->setStatus("Aprobado");
            $publication->save();
            $response = array("response" => 1,"message"=>"Pending to approval");
        }
        else
        {
            $response = array("response" => 0,"message"=>"No pending to approval");
        }
        echo json_encode($response);exit;
    }

    public function tigoMoneyRequestPayment()
    {

    }
}