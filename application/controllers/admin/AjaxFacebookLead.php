<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 28/07/2017
 * Time: 4:30
 */

class AjaxFacebookLead extends Private_Controller
{
    public function __construct()
    {
        parent::__construct();
        if(! $this->input->is_ajax_request())
        {
            redirect('404');
        }
        $this->load->model('model_project');
        $this->load->model('model_paymentservice');
        $this->load->model('model_publication');
        $this->load->model('model_property');
        $this->load->model('model_service');
        $this->load->model('model_city');
        $this->load->model('model_facebook_lead');
    }

    public function getByProjectIdAndFormId()
    {
        $formData = $this->input->post();
        $projectId = $formData["projectId"];
        $formId = $formData["formId"] == "all"?NULL:$formData["formId"];
        $projectLeads = model_facebook_lead::getByProjectId($projectId, $formId);
        $arrayProjectLeads = array();
        foreach ($projectLeads as $lead)
        {
            $arrayProjectLeads[] = $lead->toArray();
        }
         echo json_encode($arrayProjectLeads);exit;
    }
}