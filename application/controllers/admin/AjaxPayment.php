<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 19/07/2017
 * Time: 12:23
 */

class AjaxPayment extends Private_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('model_payment');
        $this->load->model('model_paymentservice');
        $this->load->model('model_publication');
        $this->load->model('model_property');
        $this->load->model('model_service');
        $this->load->library('JqdtHandler');
    }

    public function ajaxDtAllPayments()
    {
        $this->_validateAccess();
        $dt = new JqdtHandler($this->input->post());
        $additionalParameters = $this->input->post("additionalParameters");
        $recordsTotal = model_payment::countAll();
        $recordsFiltered = $recordsTotal;
        if (!$dt->hasSearchValue() and count($additionalParameters)<=1)
        {
            $resultArray = model_payment::getAll($dt->getLength(), $dt->getStart(), $dt->getOrderName(0), $dt->getOrderDir(0));
        }
        else
        {
            $resultArray = model_payment::search($dt->getSearchValue(), $dt->getLength(), $dt->getStart(), $dt->getOrderName(0), $dt->getOrderDir(0), $dt->getSearchableColumnDefs(),$additionalParameters);
            $recordsFiltered = model_payment::searchTotalCount($dt->getSearchValue(),$dt->getSearchableColumnDefs(),$additionalParameters);
        }

        echo $dt->getJsonResponse($recordsTotal, $recordsFiltered, $resultArray);
        exit ;
    }

    public function approve()
    {
        $this->_validateAccess();
        /** @var model_payment $payment */
        $formData = $this->input->post();
        $paymentId = $formData["paymentId"];
        $paymentMethod = $formData["paymentMethod"];
        $payment = model_payment::getById($paymentId);
        $response = $payment->approve($paymentMethod);
        echo json_encode($response);exit;
    }

    public function getDetail()
    {
        $this->_validateAccess();
        $formData = $this->input->post();
        $paymentId = $formData["paymentId"];
        $paymentDetail = model_payment::prepareArrayPaymentMasterDetail($paymentId);
//        $paymentDetailAndArrayService = model_payment::getPaymentDetailComposedData($paymentId);
//        $paymentDetail = array();
//        $serviceList = array();
//        if(count($paymentDetailAndArrayService)>0)
//        {
//            $i=0;
//            foreach($paymentDetailAndArrayService as $detail)
//            {
//                if($i==0)
//                {
//                    $paymentDetail["paymentId"] = $detail->payment_id;
//                    $paymentDetail["userFullName"] = $detail->user_full_name;
//                    $paymentDetail["userType"] = $detail->user_type;
//                    $paymentDetail["paymentAmount"] = $detail->payment_amount;
//                    $paymentDetail["paymentCreatedOn"] = $detail->payment_created_on;
//                    $paymentDetail["paymentMethod"] = $detail->payment_method;
//                    $paymentDetail["paymentName"] = $detail->payment_name;
//                    $paymentDetail["paymentNit"] = $detail->payment_nit;
//                }
//                $serviceList[] = array(
//                                    "index" => ($i+1),
//                                    "name" => $detail->payment_service,
//                                    "startDate" => $detail->start_date,
//                                    "finishDate" => $detail->finish_date,
//                                    "price" => $detail->service_price,
//                                    );
//                $i++;
//            }
//        }
//        $paymentDetail["serviceList"] = $serviceList;

        echo json_encode($paymentDetail);exit;
    }
    public function getAllPaymentsSettlementByDateRange()
    {
        $this->_validateAccess();
        $startDate = $this->input->post("startDate");
        $endDate = $this->input->post("endDate");
        $reportType = $this->input->post("reportType");
        if($reportType == 0)
        {
            $list = model_payment::getAllPaymentsSettlementByDateRange($startDate, $endDate);
        }
        else
        {
            $list = model_payment::getAllPaymentsSettlementByDateRangeDetailed($startDate, $endDate);
        }

        echo json_encode((array)$list);exit;
    }

    public function purchase($publicationId = NULL)
    {
        $this->_validateObjectToEdit($publicationId,"model_publication","dashboard/publications");
        /** Server Side Validations **/
        $this->form_validation->set_rules('purchase-option[]', "Opcion de compra", 'trim|required');
        $this->form_validation->set_rules('full-name', "Nombre completo", 'trim|required');
        $this->form_validation->set_rules('nit', "Nit", 'trim|required');

        if($this->form_validation->run() === FALSE)
        {
            $validationErrors = validation_errors();
            $validationErrors = str_replace("<p>","",$validationErrors);
            $validationErrors = str_replace("</p>","<br>",$validationErrors);
            $response = array("success" => 0, "message" => $validationErrors);
            $success = $validationErrors != ""?0:1;
            $response["success"] = $success;
            $response["message"] = $validationErrors;
            $template = $this->loadView("handlebar-template/ht-payment-handler", array(),true);
            $publication = model_publication::getMasterDetailById($publicationId);
            $servicesList = model_service::getByArrayServiceType(array("Principal","Busqueda","Similares"));
            $arrayServices = array();
            foreach($servicesList as $service)
            {
                $service = $service->toArray();
                $arrayServices[] = array("id" => $service["ser_id"], "type" => $service["ser_tipo"], "description" => $service["ser_descripcion"], "price" => $service["ser_precio"]);
            }
            $response["data"]["servicesList"] = $arrayServices;
            $response["data"]["publication"]["id"] = $publication["pub_id"];
            $response["data"]["publication"]["property"]["id"] = $publication["inm_id"];
            $response["data"]["publication"]["property"]["name"] = $publication["inm_nombre"];
            $response["data"]["template"] = $template;
            $response["data"]["templateName"] = "#ht-purchase-option-list";
        }
        else
        {
            include FCPATH."/application/libraries/PagosNetHandler.php";

            $formData = $this->input->post();
            $purchaseOptions = $formData["purchase-option"];
            $fullName = $formData["full-name"];
            $nit = $formData["nit"];
            $totalPayment = model_service::getTotalPaymentServiceByArrayIds($purchaseOptions);
            if($totalPayment <= 0)
            {
                $response["success"] = 0;
                $response["data"]["payment"] = array();
                $response["message"] = "El monto total es igual o menor a 0.s";
            }
            else
            {
                $expireDate = new DateTime();
                $expireDate->add(new DateInterval('P14D'));
                $expireDate = $expireDate->format('Y-m-d');
                $paymentId = model_payment::generatePaymentId();
                $payment = new model_payment(
                    $paymentId,
                    "",
                    $publicationId,
                    NULL,
                    date("Y-m-d H:i:s"),
                    $expireDate,
                    $fullName,
                    $nit,
                    $totalPayment,
                    0,
                    "Pendiente",
                    NULL,
                    "Mejora",
                    NULL,
                    0
                );
                model_payment::insertBatch(array($payment->toArray()));
                $payment = model_payment::getById($paymentId);
                if($payment instanceof model_payment)
                {
                    $payment->addServices($purchaseOptions);
                    $pagoNetResponse = $payment->sendToPagosNet();
                    //Assign pagosNet transactionId to payment object
//                    if($pagoNetResponse->isSuccess())
//                    {
                        $payment->setTransactionCode($pagoNetResponse->getTransactionId());
                        $payment->save();

//                    }
                    $paymentMasterDetail = model_payment::prepareArrayPaymentMasterDetail($payment->getId());
                    model_payment::userPaymentNotify($paymentMasterDetail);
                    model_payment::sendPaymentId($paymentMasterDetail);
                    $payment = $payment->toArray();
                    $response["success"] = 1;
                    $response["data"]["payment"]["id"] = $payment["pag_id"];
                    $response["data"]["payment"]["pagosNetTransactionId"] = $payment["pag_cod_transaccion"];
                    $response["data"]["payment"]["pagosNetSuccess"] = $pagoNetResponse->isSuccess();
                    $response["data"]["payment"]["pagosNetData"] = $pagoNetResponse->getData();
                    $response["data"]["payment"]["amount"] = $payment["pag_monto"];
                    $response["message"] = "Su pago ha sido creado exitosamente y esta como pendiente.";
                }
                else
                {
                    $response["success"] = 0;
                    $response["data"]["payment"] = array();
                    $response["message"] = "Ocurrio un problema, intente de nuevo o comuniquese con info@toqueeltimbre.com";
                }
            }
        }
        echo json_encode($response);exit;
    }

    public function paymentOptions($paymentId = NULL)
    {
        $payment = $this->_validateObjectToEdit($paymentId,"model_payment","dashboard/publications");
        if($payment->getStatus() != model_payment::PENDING)
        {
            $response["success"] = 0;
            $response["data"]["payment"] = array();
            $message = 'Este pago fue eliminado o ya fue liquidado.';
            switch ($payment->getStatus())
            {
                case model_payment::PAID_OUT:
                    $message = "Ya ha liquidado este pago.";
                    break;
                case model_payment::DELETED:
                    $message = "Este pago estaba pendiente pero fue eliminado.";
            }
            $response["message"] = $message;
        }
        else
        {
            $payment = $payment->toArray();
            $validationErrors = validation_errors();
            $validationErrors = str_replace("<p>","",$validationErrors);
            $validationErrors = str_replace("</p>","<br>",$validationErrors);
            $response = array("success" => 0, "message" => $validationErrors);
            $success = $validationErrors != ""?0:1;
            $response["success"] = $success;
            $response["message"] = $validationErrors;
            $template = $this->loadView("handlebar-template/ht-payment-handler", array(),true);
            $response["data"]["template"] = $template;
            $response["data"]["templateName"] = "#ht-payment-option-list";
            $response["data"]["payment"]["id"] = $payment["pag_id"];
            $response["data"]["payment"]["pagosNetTransactionId"] = $payment["pag_cod_transaccion"];
            $response["data"]["payment"]["amount"] = $payment["pag_monto"];
        }

        echo json_encode($response);exit;
    }

    public function tigoMoneyRequestPayment()
    {
//        $payment = $this->_validateObjectToEdit($paymentId,"model_payment","dashboard/publications");
        /** Server Side Validations **/
        $this->form_validation->set_rules('payment-id', "ID de Pago", 'trim|required');
        $this->form_validation->set_rules('cellphone', "Celular", 'trim|required|numeric');

        if($this->form_validation->run() === FALSE)
        {
//            TODO: Here could be a special form to pay out through tigo money(GET)
            exit('in progress');
        }
        else
        {
            require BASE_DIRECTORY . DS . 'application' . DS .'libraries'.DS. 'TigoMoneyHandler.php';
            $formData = $this->input->post();

            $cellNumber = $formData['cellphone'];
            $orderId = $formData['payment-id'];
            $paymentMasterDetail = model_payment::prepareArrayPaymentMasterDetail($orderId);
            $amount = 1;//round($paymentMasterDetail['paymentAmount'], 0);
            $confirmationMessage = "ToqueElTimbre";
            $notificationMessage = "Orden de Pago ".$orderId;
            $items = "";
            foreach($paymentMasterDetail['serviceList'] as $service)
            {
                $items .= "*i".$service['index']."|1|".$service['name']."|".number_format($service['price'],2)."|".number_format($service['price'],2);
            }
            $legalName = $paymentMasterDetail['paymentName'];
            $nit = $paymentMasterDetail['paymentNit'];
            $tigoMoneyHandler = new TigoMoneyHandler();
            // echo"<pre>";var_dump($orderId, $amount, $cellNumber, $confirmationMessage, $notificationMessage, $items, $legalName, $nit);exit;
            $TigoMoneyRequestPaymentResponse = $tigoMoneyHandler->requestPayment($orderId, $amount, $cellNumber, $confirmationMessage, $notificationMessage, $items, $legalName, $nit);
            $template = $this->loadView("handlebar-template/ht-payment-handler", array(),true);
            $response["success"] = $TigoMoneyRequestPaymentResponse->isSuccess()?1:0;
            $response["message"] = $TigoMoneyRequestPaymentResponse->getMessage();
            $response["status"] = $TigoMoneyRequestPaymentResponse->getStatus();
            $response["data"]["tigoMoneyRequestPaymentResponse"] = $TigoMoneyRequestPaymentResponse->toArray();
            $response["data"]["template"] = $template;
            $response["data"]["templateName"] = $TigoMoneyRequestPaymentResponse->isSuccess()?"#ht-tigo-money-success-message":"#ht-tigo-money-danger-message";

        }
        echo json_encode($response);exit;
    }

    public function tigoResponse($orderId)
    {
        require BASE_DIRECTORY . DS . 'application' . DS .'libraries'.DS. 'TigoMoneyHandler.php';
        $tigoMoneyHandler = new TigoMoneyHandler();
        $tigoStatusResponse = $tigoMoneyHandler->getPaymentStatus($orderId);
        $template = $this->loadView("handlebar-template/ht-payment-handler", array(),true);
        $response["success"] = $tigoStatusResponse->isSuccess()?1:0;
        $response["message"] = $tigoStatusResponse->getMessage();
        $response["status"] = $tigoStatusResponse->getStatus();
        $response["data"]["transactionReference"] = $tigoStatusResponse->getTigoTransactionReference();
        $response["data"]["debugInfo"] = $tigoStatusResponse->getData();
        $response["data"]["template"] = $template;
        echo json_encode($response);exit;
    }
}