<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: jair
 * Date: 2017-07-28
 * Time: 8:51 AM
 */

class Lead extends Private_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->_validateAccess();
        $this->load->model('model_facebook_lead');
    }

    public function index()
    {
        $this->complementHandler->addViewComplement("jquery.select2");
        $this->complementHandler->addViewComplement("jquery.select2.bootstrap");
        $this->complementHandler->addViewComplement("jquery.datatables");
        $this->complementHandler->addViewComplement("jquery.datatables.bootstrap");
        $this->complementHandler->addViewComplement("jquery.datatables.buttons");
        $this->complementHandler->addViewComplement("jquery.datatables.buttons.bootstrap");
        $this->complementHandler->addViewComplement("jquery.datatables.buttons.flash");
        $this->complementHandler->addViewComplement("jquery.datatables.buttons.html5");
        $this->complementHandler->addViewComplement("jquery.datatables.buttons.print");
        $this->complementHandler->addViewComplement("jquery.datatables.jszip");
        $this->complementHandler->addViewComplement("jquery.datatables.pdfmake");
        $this->complementHandler->addViewComplement("jquery.datatables.vfs_fonts");
        $this->complementHandler->addViewComplement("jquery.datatables.filterdelay");
        $this->complementHandler->addViewComplement("bootstrap.datepicker");
        $this->complementHandler->addProjectJs('DTAdditionalParameterHandler');
        $this->complementHandler->addProjectJs('lead.index');
        $this->complementHandler->addProjectCss('lead.index');
        $this->page = 'admin_leads';
        $data = array();
        $this->load_template_backend('admin/lead/index', $data);
    }
}