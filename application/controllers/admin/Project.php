<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: jair
 * Date: 2017-07-28
 * Time: 8:51 AM
 */

class Project extends Private_Controller
{
    private $_arrayStatus;
    private $_arrayState;
    private $_arraySurfaceType;

    public function __construct()
    {
        parent::__construct();

        $this->load->model('model_user');
        $this->load->model('model_payment');
        $this->load->model('model_service');
        $this->load->model('model_project');
        $this->load->model('model_paymentservice');
        $this->load->model('model_property');
        $this->load->model('model_publication');
        $this->load->model('model_city');
        $this->load->model('model_zone');
        $this->load->model('model_propertytype');
        $this->load->model('model_transactiontype');
        $this->load->model('model_projectimage');
        $this->load->model('model_typology');
        $this->load->model('model_finishingdetail');
        $this->load->model('model_socialarea');
        $this->load->model('model_projectslideshow');
        $this->load->model('model_projectsettings');
        $this->load->library('JqdtHandler');
        $this->_arrayStatus = array(
            0 => model_project::NOT_APPROVED,
            1 => model_project::APPROVED,
            2 => model_project::CANCELED
        );
        $this->_arrayState = array(
            "1" => "Santa Cruz",
            "2" => "La Paz",
            "3" => "Cochabamba",
            "4" => "Tarija",
            "5" => "Chuquisaca",
            "6" => "Oruro",
            "7" => "Potosi",
            "8" => "Pando",
            "9" => "Beni"
        );
        $this->_arraySurfaceType = array('Metros Cuadrados','Hectareas');
    }

    public function index()
    {
        $this->_validateAccess();
        $this->complementHandler->addViewComplement("jquery.datatables");
        $this->complementHandler->addViewComplement("jquery.datatables.bootstrap");
        $this->complementHandler->addViewComplement("jquery.datatables.buttons");
        $this->complementHandler->addViewComplement("jquery.datatables.buttons.bootstrap");
        $this->complementHandler->addViewComplement("jquery.datatables.buttons.flash");
        $this->complementHandler->addViewComplement("jquery.datatables.buttons.html5");
        $this->complementHandler->addViewComplement("jquery.datatables.buttons.print");
        $this->complementHandler->addViewComplement("jquery.datatables.jszip");
        $this->complementHandler->addViewComplement("jquery.datatables.pdfmake");
        $this->complementHandler->addViewComplement("jquery.datatables.vfs_fonts");
        $this->complementHandler->addViewComplement("jquery.datatables.filterdelay");
        $this->complementHandler->addProjectJs('project.index');
        $this->complementHandler->addProjectCss('project.index');
        $this->page = 'admin_project';
        $data = array();
        $this->load_template_backend('admin/project/index', $data);
    }

    public function add()
    {
        $this->_validateAccess();
        /** complements **/
        $this->complementHandler->addViewComplement("jquery.select2");
        $this->complementHandler->addViewComplement("jquery.select2.bootstrap");
        $this->complementHandler->addViewComplement("bootstrap.datepicker");
        $this->complementHandler->addViewComplement("parsley");
        $this->complementHandler->addViewComplement("parsley.spanish");
        $this->complementHandler->addViewComplement("tinymce");
        $this->complementHandler->addViewComplement("google.maps.api");
        $this->complementHandler->addViewComplement("gmaps");
        $this->complementHandler->addProjectJs("general.tinymce");
        $this->complementHandler->addProjectJs('project.add');
        $this->complementHandler->addProjectCss('project.add');
        $this->complementHandler->addProjectJs("initializeSelect2");

        /** Server Side Validations **/
        $this->form_validation->set_rules('name', 'Nombre del proyecto', 'trim|required');
        $this->form_validation->set_rules('video', 'Video', 'trim');
        $this->form_validation->set_rules('description', 'Descripcion', 'trim');
        $this->form_validation->set_rules('latitude', 'Latitud', 'trim');
        $this->form_validation->set_rules('longitude', 'Longitude', 'trim');
        $this->form_validation->set_rules('address', 'Direccion', 'trim');
        $this->form_validation->set_rules('address-image', 'Imagen de direccion', 'trim');
        $this->form_validation->set_rules('start-validity-date', 'Fecha de inicio del proyecto', 'trim');
        $this->form_validation->set_rules('finish-validity-date', 'Fecha de finalizacion del proyecto', 'trim');
        $this->form_validation->set_rules('city', 'Ciudad', 'trim');
        $this->form_validation->set_rules('zone', 'Zona', 'trim');

        $data = array();
        $data["arrayState"] = $this->_arrayState;
        /** user **/
        $user = NULL;
        if($this->input->post("user")!= FALSE && is_numeric((int)$this->input->post("user")))
        {
            $user = model_user::getById($this->input->post("user"));
        }
        if($user instanceof model_user)
        {
            $data["userProject"] = $user->toArray();
        }
        /** city **/
        $city = NULL;
        if($this->input->post("city")!= FALSE && is_numeric((int)$this->input->post("city")))
        {
            $city = model_city::getById($this->input->post("city"));
        }
        if($city instanceof model_city)
        {
            $data["city"] = $city->toArray();
        }
        /** zone **/
        $zone = NULL;
        if($this->input->post("zone")!= FALSE && is_numeric((int)$this->input->post("zone")))
        {
            $zone = model_zone::getById($this->input->post("zone"));
        }
        if($zone instanceof model_zone)
        {
            $data["zone"] = $zone->toArray();
        }

        /** If there isn't a request **/
        if ($this->form_validation->run() === FALSE)
        {
            $this->load_template_backend("admin/project/add", $data);
        }
        else
        {
            $formData = $this->input->post();
            $fileName = "";
            $addressImage = "";
            $fileHandler = new File_Handler("","projectImage");
            if (!empty($_FILES['logo']['name']))
            {
                try
                {
                    $fileName = $fileHandler->uploadImage($_FILES['logo']);
                } catch(Exception $e)
                {
                    $data["errorMessage"] = $e->getMessage();
                    $this->load_template_backend("admin/project/add", $data);
                    return;
                }
            }
            if (!empty($_FILES['logo-2']['name']))
            {
                try
                {
                    $addressImage = $fileHandler->uploadImage($_FILES['logo-2']);
                } catch(Exception $e)
                {
                    $data["errorMessage"] = $e->getMessage();
                    $this->load_template_backend("admin/project/add", $data);
                    return;
                }
            }
            $startValidityDate = $formData["start-date"];
            $startValidityDate = date('Y-m-d', strtotime($startValidityDate));
            $finishValidityDate = $formData["finish-date"];
            $finishValidityDate = date('Y-m-d', strtotime($finishValidityDate));
            $seo = $this->_sanitize($formData["name"]);
            $seo = $this->_toASCII($seo);
            $seo = $this->_validateSeo($seo);
            $project = new model_project(
                $formData["name"],
                $fileName,
                $formData["video"],
                $formData["description"],
                $formData["latitude"],
                $formData["longitude"],
                $formData["address"],
                $addressImage,
                $this->_arrayStatus[$formData["status"]],
                date("Y-m-d H:i:s"),
                $startValidityDate,
                $finishValidityDate,
                $formData["city"],
                $formData["zone"],
                $seo,
                "",
                $formData["contact"],
                $formData["user"],
                $formData["image-dist"]
            );
            $project->save();
            $project->createDependencies();

            $this->session->set_flashdata('successMessage', 'El proyecto se guardo correctamente');
            redirect(base_url("admin/Project/edit/".$project->getId()));
        }
    }

    public function edit($projectId = null)
    {
        $this->_validateAccess();
        /** validate param **/
        if(!is_numeric($projectId))
        {
            $this->session->set_flashdata('errorMessage', 'El parametro enviado es incorrecto');
            redirect(base_url("admin/Project"));
        }
        $project = model_project::getById($projectId);
        if(!$project instanceof model_project)
        {
            $this->session->set_flashdata('errorMessage', 'Ha solicitado editar un proyecto que no existe');
            redirect(base_url("admin/Project"));
        }

        /** complements **/
        $this->complementHandler->addViewComplement("jquery.select2");
        $this->complementHandler->addViewComplement("jquery.select2.bootstrap");
        $this->complementHandler->addViewComplement("bootstrap.datepicker");
        $this->complementHandler->addViewComplement("parsley");
        $this->complementHandler->addViewComplement("parsley.spanish");
        $this->complementHandler->addViewComplement("tinymce");
        $this->complementHandler->addViewComplement("google.maps.api");
        $this->complementHandler->addViewComplement("gmaps");
        $this->complementHandler->addViewComplement("jquery.inputmask.bundle");
        $this->complementHandler->addProjectJs("general.tinymce");
        $this->complementHandler->addProjectJs('project.edit');
        $this->complementHandler->addProjectCss('project.edit');
        $this->complementHandler->addProjectJs("initializeSelect2");

        /** Server Side Validations **/
        $this->form_validation->set_rules('name', 'Nombre del proyecto', 'trim|required');
        $this->form_validation->set_rules('video', 'Video', 'trim');
        $this->form_validation->set_rules('description', 'Descripcion', 'trim|required');
        $this->form_validation->set_rules('latitude', 'Latitud', 'trim');
        $this->form_validation->set_rules('longitude', 'Longitude', 'trim');
        $this->form_validation->set_rules('address', 'Direccion', 'trim');
        $this->form_validation->set_rules('address-image', 'Imagen de direccion', 'trim');
        $this->form_validation->set_rules('start-validity-date', 'Fecha de inicio del proyecto', 'trim');
        $this->form_validation->set_rules('finish-validity-date', 'Fecha de finalizacion del proyecto', 'trim');
        $this->form_validation->set_rules('city', 'Ciudad', 'trim');
        $this->form_validation->set_rules('zone', 'Zona', 'trim');

        $data = array();
        $data["currentAction"] = "edit";
        $data["arrayState"] = $this->_arrayState;
        $data["arrayStatus"] = $this->_arrayStatus;
        $data["project"] = $project->toArray();

        /** user **/
        $user = NULL;
        if($this->input->post("user")!= FALSE && is_numeric((int)$this->input->post("user")))
        {
            $user = model_user::getById($this->input->post("user"));
        }
        elseif(is_numeric((int)$data["project"]["proy_usu_id"]))
        {
            $user = model_user::getById($data["project"]["proy_usu_id"]);
        }
        if($user instanceof model_user)
        {
            $data["userProject"] = $user->toArray();
        }
        /** city **/
        $city = NULL;
        if($this->input->post("city")!= FALSE && is_numeric((int)$this->input->post("city")))
        {
            $city = model_city::getById($this->input->post("city"));
        }
        elseif(is_numeric((int)$data["project"]["proy_ciu_id"]))
        {
            $city = model_city::getById($data["project"]["proy_ciu_id"]);
        }
        if($city instanceof model_city)
        {
            $data["city"] = $city->toArray();
        }
        /** zone **/
        $zone = NULL;
        if($this->input->post("zone")!= FALSE && is_numeric((int)$this->input->post("zone")))
        {
            $zone = model_zone::getById($this->input->post("zone"));
        }
        elseif(is_numeric((int)$data["project"]["proy_zon_id"]))
        {
            $zone = model_zone::getById($data["project"]["proy_zon_id"]);
        }
        if($zone instanceof model_zone)
        {
            $data["zone"] = $zone->toArray();
        }

        /** If there isn't a request **/
        if ($this->form_validation->run() === FALSE)
        {
            $this->load_template_backend("admin/project/edit", $data);
        }
        else
        {
            $formData = $this->input->post();
            $fileName = "";
            $addressImage = "";
            $fileHandler = new File_Handler("","projectImage");
            if (!empty($_FILES['logo']['name']))
            {
                try
                {
                    $fileName = $fileHandler->uploadImage($_FILES['logo']);
                }
                catch(Exception $e)
                {
                    $data["errorMessage"] = $e->getMessage();
                    $this->load_template_backend("admin/project/add", $data);
                    return;
                }
            }
            if (!empty($_FILES['logo-2']['name']))
            {
                try
                {
                    $addressImage = $fileHandler->uploadImage($_FILES['logo-2']);
                } catch(Exception $e)
                {
                    $data["errorMessage"] = $e->getMessage();
                    $this->load_template_backend("admin/project/add", $data);
                    return;
                }
            }
            $startValidityDate = $formData["start-date"];
            $startValidityDate = date('Y-m-d', strtotime($startValidityDate));
            $finishValidityDate = $formData["finish-date"];
            $finishValidityDate = date('Y-m-d', strtotime($finishValidityDate));

            $project->setName($formData["name"]);
            $project->setLogo($fileName);
            $project->setVideo($formData["video"]);
            $project->setDescription($formData["description"]);
            $project->setLatitude($formData["latitude"]);
            $project->setLongitude($formData["longitude"]);
            $project->setAddress($formData["address"]);
            $project->setAddressImage($addressImage);
            $project->setStatus($this->_arrayStatus[$formData["status"]]);
            $project->setStartValidityDate($startValidityDate);
            $project->setFinishValidityDate($finishValidityDate);
            $project->setCityId($formData["city"]);
            $project->setZoneId($formData["zone"]);
            $project->setContact($formData["contact"]);
            $project->setUser($formData["user"]);
            $project->save();
            $this->session->set_flashdata('successMessage', 'El proyecto se actualizo correctamente');
            redirect(base_url("admin/Project/edit/".$project->getId()));
        }
    }

    public function publication($projectId = NULL)
    {
        $this->_validateAccess();
        /** validate param **/
        if(!is_numeric($projectId))
        {
            $this->session->set_flashdata('errorMessage', 'El parametro enviado es incorrecto');
            redirect(base_url("admin/Project"));
        }
        $project = model_project::getById($projectId);
        if(!$project instanceof model_project)
        {
            $this->session->set_flashdata('errorMessage', 'Ha solicitado editar un proyecto que no existe');
            redirect(base_url("admin/Project"));
        }

        /** Complements **/
        $this->complementHandler->addViewComplement("parsley");
        $this->complementHandler->addViewComplement("parsley.spanish");
        $this->complementHandler->addViewComplement("jquery.select2");
        $this->complementHandler->addViewComplement("jquery.inputmask.bundle");
        $this->complementHandler->addProjectJs("initializeSelect2");
        $this->complementHandler->addProjectCss("project.publication");
        $this->complementHandler->addProjectJs("project.publication");

        /** Server Side Validations **/
        $this->form_validation->set_rules('property-type', 'Tipo de propiedad', 'trim');

        $data = array();
        $data["currentAction"] = "publication";
        $data["project"] = $project->toArray();
        $property = model_property::getByProjectId($project->getId());
        if(!$property instanceof model_property)
        {
            $project->createDependencies();
            $property = model_property::getByProjectId($project->getId());
        }
        $data["property"] = $property->toArray();
        $propertyTypeList = model_propertytype::getAll();
        $data["propertyTypeList"] = $propertyTypeList;
        $transactionTypeList = model_transactiontype::getAll();
        $data["transactionTypeList"] = $transactionTypeList;
        $data["arraySurfaceType"] = $this->_arraySurfaceType;

        /** If there isn't a request **/
        if ($this->form_validation->run() === FALSE)
        {
            $this->load_template_backend("admin/project/publication", $data);
        }
        else
        {

            $formData = $this->input->post();
            $price = str_replace(",","",$formData["price"]);
            $surface = str_replace(",","",$formData["surface"]);
            $property->setPropertyType($formData["property-type"]);
            $property->setTransactionType($formData["transaction-type"]);
            $property->setSurfaceType($formData["surface-type"]);
            $property->setCurrencyType($formData["currency-type"]);
            $property->setSurface($surface);
            $property->setPrice($price);
            $property->save();
            $this->session->set_flashdata('successMessage', 'Informacion actualizada correctamente!');
            redirect(base_url("admin/Project/publication/".$project->getId()));
        }
    }

    public function images($projectId = NULL)
    {
        $this->_validateAccess();
        /** validate param **/
        if(!is_numeric($projectId))
        {
            $this->session->set_flashdata('errorMessage', 'El parametro enviado es incorrecto');
            redirect(base_url("admin/Project"));
        }
        $project = model_project::getById($projectId);
        if(!$project instanceof model_project)
        {
            $this->session->set_flashdata('errorMessage', 'Ha solicitado editar un proyecto que no existe');
            redirect(base_url("admin/Project"));
        }

        /** Complements **/
        $this->complementHandler->addViewComplement("jquery.datatables");
        $this->complementHandler->addViewComplement("jquery.datatables.bootstrap");
        $this->complementHandler->addViewComplement("dropzone");
        $this->complementHandler->addViewComplement("tinymce");
        $this->complementHandler->addProjectJs("general.tinymce");
        $this->complementHandler->addProjectCss("project.images");
        $this->complementHandler->addProjectJs("project.images");


        $data = array();
        $data["currentAction"] = "images";
        $data["project"] = $project->toArray();
        if (empty($_FILES))
        {
            $this->load_template_backend("admin/project/images", $data);
        }
        else
        {
            $fileName = "";
            $fileHandler = new File_Handler("","projectImage");
            if (!empty($_FILES['file']['name']))
            {
                try
                {
                    $fileName = $fileHandler->uploadImage($_FILES['file']);
                }
                catch(Exception $e)
                {
                    $data["errorMessage"] = $e->getMessage();
                    $this->load_template_backend("admin/project/images", $data);
                    return;
                }
            }
            $projectImage = new model_projectimage($fileName,$project->getId(),FALSE,FALSE,"");
            $projectImage->save();
            $arrayItem = $projectImage->toArray();
            $file = new File_Handler($arrayItem["proy_fot_imagen"],"projectImage");
            $thumbnail = $file->getThumbnail("150","100");
            $arrayItem["src"] = $thumbnail->getSource();
            echo json_encode($arrayItem);
        }
    }

    public function typology($projectId = NULL)
    {
        $this->_validateAccess();
        /** validate param **/
        if(!is_numeric($projectId))
        {
            $this->session->set_flashdata('errorMessage', 'El parametro enviado es incorrecto');
            redirect(base_url("admin/Project"));
        }
        $project = model_project::getById($projectId);
        if(!$project instanceof model_project)
        {
            $this->session->set_flashdata('errorMessage', 'Ha solicitado editar un proyecto que no existe');
            redirect(base_url("admin/Project"));
        }

        /** Complements **/
        $this->complementHandler->addViewComplement("dropzone");
        $this->complementHandler->addViewComplement("tinymce");
        $this->complementHandler->addViewComplement("parsley");
        $this->complementHandler->addViewComplement("parsley.spanish");
        $this->complementHandler->addViewComplement("jquery.inputmask.bundle");
        $this->complementHandler->addProjectJs("general.tinymce");
        $this->complementHandler->addProjectCss("project.typology");
        $this->complementHandler->addProjectJs("project.typology");


        $data = array();
        $data["currentAction"] = "typology";
        $data["project"] = $project->toArray();
        if (empty($_FILES))
        {
            $this->load_template_backend("admin/project/typology", $data);
        }
        else
        {
            $fileName = "";
            $fileHandler = new File_Handler("","projectImage");
            if (!empty($_FILES['file']['name']))
            {
                try
                {
                    $fileName = $fileHandler->uploadImage($_FILES['file']);
                }
                catch(Exception $e)
                {
                    $data["errorMessage"] = $e->getMessage();
                    $this->load_template_backend("admin/project/typology", $data);
                    return;
                }
            }
            //echo"<pre>";var_dump($formData,$fileName);exit;
            $projectTypology = new model_typology("",$fileName,0,$project->getId(),"","","","","","",0,0,0);
            $projectTypology->save();
            $arrayItem = $projectTypology->toArray();
            $file = new File_Handler($arrayItem["tip_imagen"],"projectImage");
            $thumbnail = $file->getThumbnail("150","100");
            $arrayItem["srcImage"] = $thumbnail->getSource();
            $file = new File_Handler($arrayItem["tip_imagen_precio"],"projectImage");
            $thumbnail = $file->getThumbnail("150","100");
            $arrayItem["srcImagePrice"] = $thumbnail->getSource();
            echo json_encode($arrayItem);
        }
    }

    public function finishingDetail($projectId = NULL)
    {
        $this->_validateAccess();
        /** validate param **/
        if (!is_numeric($projectId)) {
            $this->session->set_flashdata('errorMessage', 'El parametro enviado es incorrecto');
            redirect(base_url("admin/Project"));
        }
        $project = model_project::getById($projectId);
        if (!$project instanceof model_project) {
            $this->session->set_flashdata('errorMessage', 'Ha solicitado editar un proyecto que no existe');
            redirect(base_url("admin/Project"));
        }

        /** Complements **/
        $this->complementHandler->addViewComplement("dropzone");
        $this->complementHandler->addViewComplement("tinymce");
        $this->complementHandler->addProjectJs("general.tinymce");
        $this->complementHandler->addProjectCss("project.finishingdetail");
        $this->complementHandler->addProjectJs("project.finishingdetail");

        $data = array();
        $data["currentAction"] = "finishingDetail";
        $data["project"] = $project->toArray();
        if (empty($_FILES)) {
            $this->load_template_backend("admin/project/finishingdetail", $data);
        } else {
            $fileName = "";
            $fileHandler = new File_Handler("", "projectImage");
            if (!empty($_FILES['file']['name'])) {
                try {
                    $fileName = $fileHandler->uploadImage($_FILES['file']);
                } catch (Exception $e) {
//                    $data["errorMessage"] = $e->getMessage();
//                    $this->load_template_backend("admin/project/finishingdetail", $data);
                    echo $e->getMessage();
                    return;
                }
            }
            $finishingDetail = new model_finishingdetail("", $fileName, $project->getId());
            $finishingDetail->save();
            $arrayItem = $finishingDetail->toArray();
            $file = new File_Handler($arrayItem["aca_imagen"], "projectImage");
            $thumbnail = $file->getThumbnail("150", "100");
            $arrayItem["src"] = $thumbnail->getSource();
            echo json_encode($arrayItem);
        }
    }

    public function socialArea($projectId = NULL)
    {
        $this->_validateAccess();
        /** validate param **/
        if (!is_numeric($projectId)) {
            $this->session->set_flashdata('errorMessage', 'El parametro enviado es incorrecto');
            redirect(base_url("admin/Project"));
        }
        $project = model_project::getById($projectId);
        if (!$project instanceof model_project) {
            $this->session->set_flashdata('errorMessage', 'Ha solicitado editar un proyecto que no existe');
            redirect(base_url("admin/Project"));
        }

        /** Complements **/
        $this->complementHandler->addViewComplement("dropzone");
        $this->complementHandler->addViewComplement("tinymce");
        $this->complementHandler->addProjectJs("general.tinymce");
        $this->complementHandler->addProjectCss("project.socialarea");
        $this->complementHandler->addProjectJs("project.socialarea");

        $data = array();
        $data["currentAction"] = "socialArea";
        $data["project"] = $project->toArray();
        if (empty($_FILES))
        {
            $this->load_template_backend("admin/project/socialarea", $data);
        }
        else
        {
            $fileName = "";
            $fileHandler = new File_Handler("", "projectImage");
            if (!empty($_FILES['file']['name'])) {
                try {
                    $fileName = $fileHandler->uploadImage($_FILES['file']);
                } catch (Exception $e) {
                    echo $e->getMessage();
                    return;
                }
            }
            $socialArea = new model_socialarea("", "",$fileName, $project->getId());
            $socialArea->save();
            $arrayItem = $socialArea->toArray();
            $file = new File_Handler($arrayItem["are_soc_imagen"], "projectImage");
            $thumbnail = $file->getThumbnail("150", "100");
            $arrayItem["src"] = $thumbnail->getSource();
            echo json_encode($arrayItem);
        }
    }

    public function slideShow($projectId = NULL)
    {
        $this->_validateAccess();
        /** validate param **/
        if (!is_numeric($projectId)) {
            $this->session->set_flashdata('errorMessage', 'El parametro enviado es incorrecto');
            redirect(base_url("admin/Project"));
        }
        $project = model_project::getById($projectId);
        if (!$project instanceof model_project) {
            $this->session->set_flashdata('errorMessage', 'Ha solicitado editar un proyecto que no existe');
            redirect(base_url("admin/Project"));
        }

        /** Complements **/
        $this->complementHandler->addViewComplement("dropzone");
        $this->complementHandler->addProjectCss("project.slideshow");
        $this->complementHandler->addProjectJs("project.slideshow");

        $data = array();
        $data["currentAction"] = "slideShow";
        $data["project"] = $project->toArray();
        if (empty($_FILES))
        {
            $this->load_template_backend("admin/project/slideshow", $data);
        }
        else
        {
            $fileName = "";
            $fileHandler = new File_Handler("", "projectSlideShowImage");
            if (!empty($_FILES['file']['name'])) {
                try {
                    $fileName = $fileHandler->uploadImage($_FILES['file']);
                } catch (Exception $e) {
                    echo $e->getMessage();
                    return;
                }
            }
            $item = new model_projectslideshow($fileName, "", $project->getId());
            $item->save();
            $arrayItem = $item->toArray();
            $file = new File_Handler($arrayItem["imagen"], "projectSlideShowImage");
            $thumbnail = $file->getThumbnail("150", "100");
            $arrayItem["src"] = $thumbnail->getSource();
            echo json_encode($arrayItem);
        }
    }

    public function settings($projectId = NULL)
    {
        $this->_validateAccess();
        /** validate param **/
        if(!is_numeric($projectId))
        {
            $this->session->set_flashdata('errorMessage', 'El parametro enviado es incorrecto');
            redirect(base_url("admin/Project"));
        }
        $project = model_project::getById($projectId);
        if(!$project instanceof model_project)
        {
            $this->session->set_flashdata('errorMessage', 'Ha solicitado editar un proyecto que no existe');
            redirect(base_url("admin/Project"));
        }

        /** Complements **/
        $this->complementHandler->addViewComplement("parsley");
        $this->complementHandler->addViewComplement("parsley.spanish");
        $this->complementHandler->addProjectCss("project.settings");
        $this->complementHandler->addProjectJs("project.settings");

        /** Server Side Validations **/
        $this->form_validation->set_rules('home-label', 'Inicio', 'trim');
        $this->form_validation->set_rules('typology-label', 'Tipologia', 'trim');
        $this->form_validation->set_rules('location-label', 'Ubicacion', 'trim');
        $this->form_validation->set_rules('form-label', 'Formulario de contacto', 'trim');
        $this->form_validation->set_rules('contact-label', 'Vendedor', 'trim');
        $this->form_validation->set_rules('price-label', 'Precio', 'trim');
        $this->form_validation->set_rules('finishing-detail-label', 'Acabado', 'trim');
        $this->form_validation->set_rules('social-area-label', 'Area social', 'trim');
        $this->form_validation->set_rules('gallery-label', 'Foto galeria', 'trim');

        $data = array();
        $data["currentAction"] = "settings";
        $data["project"] = $project->toArray();
        $projectSettings = model_projectsettings::getByProjectId($project->getId());
        if(!$projectSettings instanceof model_projectsettings)
        {
            $project->createSettings();
            $projectSettings = model_projectsettings::getByProjectId($project->getId());
        }
        $data["projectSettings"] = $projectSettings->toArray();
        $propertyTypeList = model_propertytype::getAll();
        $data["propertyTypeList"] = $propertyTypeList;
        $transactionTypeList = model_transactiontype::getAll();
        $data["transactionTypeList"] = $transactionTypeList;
        $data["arraySurfaceType"] = $this->_arraySurfaceType;

        /** If there isn't a request **/
        if ($this->form_validation->run() === FALSE)
        {
            $this->load_template_backend("admin/project/settings", $data);
        }
        else
        {
            $formData = $this->input->post();
            $projectSettings->setHomeLabel($formData["home-label"]);
            $projectSettings->setTypologyLabel($formData["typology-label"]);
            $projectSettings->setLocationLabel($formData["location-label"]);
            $projectSettings->setFormLabel($formData["form-label"]);
            $projectSettings->setContactLabel($formData["contact-label"]);
            $projectSettings->setPriceLabel($formData["price-label"]);
            $projectSettings->setFinishingDetailLabel($formData["finishing-detail-label"]);
            $projectSettings->setSocialAreaLabel($formData["social-area-label"]);
            $projectSettings->setGalleryLabel($formData["gallery-label"]);
            $projectSettings->setShowDescription($formData["show-description"]);
            $projectSettings->setShowTypology($formData["show-typology"]);
            $projectSettings->setShowLocation($formData["show-location"]);
            $projectSettings->setShowForm($formData["show-form"]);
            $projectSettings->setShowContact($formData["show-contact"]);
            $projectSettings->setShowPrice($formData["show-price"]);
            $projectSettings->setShowFinishingDetail($formData["show-finishing-detail"]);
            $projectSettings->setShowSocialArea($formData["show-social-area"]);
            $projectSettings->setShowGallery($formData["show-gallery"]);
            $projectSettings->setShowTypologyDetail($formData["show-typology-detail"]);
            $projectSettings->save();
            $this->session->set_flashdata('successMessage', 'Informacion actualizada correctamente!');
            redirect(base_url("admin/Project/settings/".$project->getId()));
        }
    }

    public function leads($projectId = NULL)
    {
        /** validate param **/
        if(!is_numeric($projectId))
        {
            $this->session->set_flashdata('errorMessage', 'El parametro enviado es incorrecto');
            redirect(base_url("admin/Project"));
        }
        $project = model_project::getById($projectId);
        if(!$project instanceof model_project)
        {
            $this->session->set_flashdata('errorMessage', 'Ha solicitado editar un proyecto que no existe');
            redirect(base_url("admin/Project"));
        }
        if(!$this->_isAdmin() && $this->user->usu_id != $project->getUserId())
        {
            $this->session->set_flashdata('errorMessage', 'No tiene permisos a este lead');
            redirect(base_url("admin/Project"));
        }
        /** Complements **/
        $this->complementHandler->addViewComplement("jquery.datatables");
        $this->complementHandler->addViewComplement("jquery.datatables.bootstrap");
        $this->complementHandler->addViewComplement("jquery.datatables.buttons");
        $this->complementHandler->addViewComplement("jquery.datatables.buttons.bootstrap");
        $this->complementHandler->addViewComplement("jquery.datatables.buttons.flash");
        $this->complementHandler->addViewComplement("jquery.datatables.buttons.html5");
        $this->complementHandler->addViewComplement("jquery.datatables.buttons.print");
        $this->complementHandler->addViewComplement("jquery.datatables.jszip");
        $this->complementHandler->addViewComplement("jquery.datatables.pdfmake");
        $this->complementHandler->addViewComplement("jquery.datatables.vfs_fonts");
        $this->complementHandler->addViewComplement("jquery.datatables.filterdelay");
        $this->complementHandler->addProjectCss("project.facebook-leads");
        $this->complementHandler->addProjectJs("project.facebook-leads");

        $formList = model_facebook_lead_form::getByProjectId($project->getId());

        $data["project"] = $project->toArray();
        $data["formList"] = $formList;
        $data["currentAction"] = "leads";
        $data["arrayProjectLeads"] = model_facebook_lead::getByProjectId($projectId);
        $data["isAdmin"] = $this->_isAdmin();

        $this->load_template_backend("admin/project/facebook-leads", $data);
    }

    public function delete()
    {

    }

    private function _validateSeo($projectSeo, $projectId = "")
    {
        $seoExist = model_project::getSeoNotProjectIdExists($projectSeo, $projectId);
        if ($seoExist)
        {
            $arrayWords = explode("-",$projectSeo);
            $counter = (int)$arrayWords[count($arrayWords)-1];
            if(is_numeric($arrayWords[count($arrayWords)-1]))
            {
                $counter++;
                $arrayWords[count($arrayWords)-1] = $counter;
            }
            else
            {
                $counter = 1;
                $arrayWords[count($arrayWords)+1] = $counter;
            }

            $projectSeo = implode("-",$arrayWords);
            $projectSeo = $this->_validateSeo($projectSeo,$projectId);
        }
        return $projectSeo;
    }

    private function _sanitize($string, $force_lowercase = true, $ranal = false)
    {
        $strip = array(
            "´",
            "~",
            "`",
            "!",
            "@",
            "#",
            "$",
            "%",
            "^",
            "&",
            "*",
            "(",
            ")",
            "_",
            "=",
            "+",
            "[",
            "{",
            "]",
            "}",
            "\\",
            "|",
            ";",
            ":",
            "\"",
            "'",
            "&#8216;",
            "&#8217;",
            "&#8220;",
            "&#8221;",
            "&#8211;",
            "&#8212;",
            "â€”",
            "â€“",
            ",",
            "<",
            ".",
            ">",
            "/",
            "?"
        );
        $clean = trim(str_replace($strip, "", strip_tags($string)));
        $clean = preg_replace('/\s+/', "-", $clean);
        $clean = ($ranal) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean;
        return ($force_lowercase) ? (function_exists('mb_strtolower')) ? mb_strtolower($clean, 'UTF-8') : strtolower($clean) : $clean;
    }
    private function _toASCII( $str )
    {
        return strtr(utf8_decode($str),
            utf8_decode(
                'ŠŒŽšœžŸ¥µÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝßàáâãäåæçèéêëìíîïðñòóôõöøùúûüýÿ'),
            'SOZsozYYuAAAAAAACEEEEIIIIDNOOOOOOUUUUYsaaaaaaaceeeeiiiionoooooouuuuyy');
    }
}