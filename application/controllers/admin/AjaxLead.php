<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 02/02/2018
 * Time: 10:00
 */

class AjaxLead extends Base_page
{
    public function __construct()
    {
        parent::__construct();
        if(! $this->input->is_ajax_request())
        {
            redirect('404');
        }
        $this->load->model("model_facebook_lead");
        $this->load->library('JqdtHandler');
    }

    public function ajaxDtAllLeads()
    {
        $dt = new JqdtHandler($this->input->post());
        $additionalParameters = $this->input->post("additionalParameters");
        $recordsTotal = model_facebook_lead::countAll();
        $recordsFiltered = $recordsTotal;
        if (!$dt->hasSearchValue() and count($additionalParameters) <= 0)
        {
            $resultArray = model_facebook_lead::getAll($dt->getLength(), $dt->getStart(), $dt->getOrderName(0), $dt->getOrderDir(0));
        }
        else
        {
            $resultArray = model_facebook_lead::search($dt->getSearchValue(), $dt->getLength(), $dt->getStart(), $dt->getOrderName(0), $dt->getOrderDir(0), $dt->getSearchableColumnDefs(),$additionalParameters);
            $recordsFiltered = model_facebook_lead::searchTotalCount($dt->getSearchValue(),$dt->getSearchableColumnDefs(),$additionalParameters);
        }

        echo $dt->getJsonResponse($recordsTotal, $recordsFiltered, $resultArray);
        exit;
    }
}