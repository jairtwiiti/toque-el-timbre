<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 04/12/2018
 * Time: 9:54 AM
 */

class AjaxService extends Private_Controller
{
    public function __construct()
    {
        parent::__construct();
        if(! $this->input->is_ajax_request())
        {
            redirect('404');
        }
        $this->load->model('model_service');
        $this->load->library('JqdtHandler');
    }

    public function ajaxDtAllServices()
    {
        $dt = new JqdtHandler($this->input->post());
        $additionalParameters = $this->input->post("additionalParameters");
        $recordsTotal = model_service::countAll();
        $recordsFiltered = $recordsTotal;
        if (!$dt->hasSearchValue() and count($additionalParameters)<=1)
        {
            $resultArray = model_service::getAll($dt->getLength(), $dt->getStart(), $dt->getOrderName(0), $dt->getOrderDir(0));
        }
        else
        {
            $resultArray = model_service::search($dt->getSearchValue(), $dt->getLength(), $dt->getStart(), $dt->getOrderName(0), $dt->getOrderDir(0), $dt->getSearchableColumnDefs(), $additionalParameters);
            $recordsFiltered = model_service::searchTotalCount($dt->getSearchValue(),$dt->getSearchableColumnDefs(),$additionalParameters);
        }

        echo $dt->getJsonResponse($recordsTotal, $recordsFiltered, $resultArray);
        exit;
    }

    public function getServiceList()
    {
        $serviceList = model_service::getByArrayServiceType();
        $arrayServiceList = array();
        $singleList = array();
        $arrayResponse = array();

        //Parse array object to multidimensional array
        foreach ($serviceList as $service)
        {
            $arrayServiceList[] = $service->toArray();
        }

        for ($i = 0; $i < count($arrayServiceList); $i++)
        {
            $serviceType = $arrayServiceList[$i]["ser_tipo"];
            $singleList[] = $arrayServiceList[$i];
            if(isset($arrayServiceList[$i+1]))
            {
                if($arrayServiceList[$i]["ser_tipo"] != $arrayServiceList[$i+1]["ser_tipo"])
                {
                    $arrayResponse[$serviceType]["type"] = $arrayServiceList[$i]["ser_tipo"];
                    $arrayResponse[$serviceType]["list"] = $singleList;
                    $singleList = array();
                }
            }
            else
            {
                $arrayResponse[$serviceType]["type"] = $arrayServiceList[$i]["ser_tipo"];
                $arrayResponse[$serviceType]["list"] = $singleList;
            }
        }
        echo json_encode($arrayResponse);exit;
    }
}