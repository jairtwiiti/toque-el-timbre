<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: jair
 * Date: 2017-07-28
 * Time: 8:51 AM
 */

class Property extends Private_Controller
{
    private $_arrayState;
    private $_arraySurfaceType;
    private $_arrayTransactionTypes;

    public function __construct()
    {
        parent::__construct();

        $this->load->model('model_property');
        $this->load->model('model_publication');
        $this->load->model('model_user');
        $this->load->model('model_propertytype');
        $this->load->model('model_city');
        $this->load->model('model_zone');
        $this->load->model('model_propertytypefeature');
        $this->load->model('model_propertyfeature');
        $this->load->model('model_propertyimage');
        $this->load->model('model_servicios');
        $this->load->model('model_service');
        $this->load->model('model_payment');

        $this->_arrayState = array(
            "1" => "Santa Cruz",
            "2" => "La Paz",
            "3" => "Cochabamba",
            "4" => "Tarija",
            "5" => "Chuquisaca",
            "6" => "Oruro",
            "7" => "Potosi",
            "8" => "Pando",
            "9" => "Beni"
        );
        $this->_arraySurfaceType = array('Metros Cuadrados','Hectareas');
        $this->_arrayTransactionTypes = array(
                                    "1"	=> "VENTA",
                                    "2"	=> "ALQUILER",
                                    "3"	=> "ANTICRETICO"
                                    );
    }

    public function index()
    {
//        $this->_validateAccess();
        $this->complementHandler->addViewComplement("jquery.select2");
        $this->complementHandler->addViewComplement("jquery.select2.bootstrap");
        $this->complementHandler->addViewComplement("jquery.datatables");
        $this->complementHandler->addViewComplement("jquery.datatables.bootstrap");
        $this->complementHandler->addViewComplement("jquery.datatables.buttons");
        $this->complementHandler->addViewComplement("jquery.datatables.buttons.bootstrap");
        $this->complementHandler->addViewComplement("jquery.datatables.buttons.flash");
        $this->complementHandler->addViewComplement("jquery.datatables.buttons.html5");
        $this->complementHandler->addViewComplement("jquery.datatables.buttons.print");
        $this->complementHandler->addViewComplement("jquery.datatables.jszip");
        $this->complementHandler->addViewComplement("jquery.datatables.pdfmake");
        $this->complementHandler->addViewComplement("jquery.datatables.vfs_fonts");
        $this->complementHandler->addViewComplement("jquery.datatables.filterdelay");
        $this->complementHandler->addViewComplement("bootstrap.datepicker");
        $this->complementHandler->addViewComplement("parsley");
        $this->complementHandler->addViewComplement("parsley.spanish");
        $this->complementHandler->addViewComplement("bootstrap-wizard");
        $this->complementHandler->addViewComplement("form-wizard");
        $this->complementHandler->addViewComplement("sweet-alerts");
        $this->complementHandler->addViewComplement('pricing');
        $this->complementHandler->addViewComplement('components');
        $this->complementHandler->addViewComplement('jquery.countdown');
        $this->complementHandler->addViewComplement('moment');
        $this->complementHandler->addViewComplement('moment-with-locales');
        $this->complementHandler->addProjectCss('PaymentHandler', TRUE);
        $this->complementHandler->addProjectJs('PaymentHandler', TRUE);
        $this->complementHandler->addProjectJs('DTAdditionalParameterHandler', TRUE);
        $this->complementHandler->addProjectJs('property.index', TRUE);
        $this->complementHandler->addProjectCss('property.index', TRUE);
        $this->complementHandler->addProjectJs("initializeSelect2", TRUE);
        $this->page = 'admin_property';
        $data = array();
        $data["userType"] = $this->user->usu_tipo;
        $this->load_template_backend('admin/property/index', $data);
    }

    public function add()
    {
        $cancelUrl = $this->_isAdmin()?"admin/property":"dashboard/publications";
        $currentPublications = model_property::getPublishedPropertiesComposedList($this->user->usu_id);
        if(count($currentPublications) >= $this->limit_publications)
        {
            $this->session->set_flashdata('errorMessage', 'Alcanzó la cantidad maxima de publicaciones');
            redirect(base_url($cancelUrl));
        }
        /** complements **/
        $this->complementHandler->addViewComplement("jquery.select2");
        $this->complementHandler->addViewComplement("jquery.select2.bootstrap");
        $this->complementHandler->addViewComplement("bootstrap.datepicker");
        $this->complementHandler->addViewComplement("parsley");
        $this->complementHandler->addViewComplement("parsley.spanish");
        $this->complementHandler->addViewComplement("tinymce");
        $this->complementHandler->addViewComplement("google.maps.api");
        $this->complementHandler->addViewComplement("gmaps");
        $this->complementHandler->addViewComplement("jquery.inputmask.bundle");
        $this->complementHandler->addProjectJs("general.tinymce");
        $this->complementHandler->addProjectJs("PropertyFeatureHandler");
        $this->complementHandler->addProjectJs("MapHandler");
        $this->complementHandler->addProjectJs('property.add');
        $this->complementHandler->addProjectCss('property.add');
        $this->complementHandler->addProjectJs("initializeSelect2");

        /** Server Side Validations **/
        $this->form_validation->set_rules('name', 'Titulo del inmueble', 'trim|required');
        $this->form_validation->set_rules('address', 'Direccion', 'trim|required');
        $this->form_validation->set_rules('description', 'Descripcion', 'trim|required');
        $this->form_validation->set_rules('latitude', 'Latitud', 'trim');
        $this->form_validation->set_rules('longitude', 'Longitude', 'trim');
        $this->form_validation->set_rules('uv', 'Uv', 'trim');
        $this->form_validation->set_rules('manzano', 'Manzana', 'trim');
        $this->form_validation->set_rules('surface', 'Superficie', 'trim|required');
        $this->form_validation->set_rules('price', 'Precio', 'trim|required');
        $this->form_validation->set_rules('city', 'Ciudad', 'trim');
        $this->form_validation->set_rules('zone', 'Zona', 'trim');

        $data = array();
        $data["currentAction"] = "add";
        $data["arrayState"] = $this->_arrayState;
        $data["arrayTransactionTypes"] = $this->_arrayTransactionTypes;
        $data["arrayPropertyFeatures"] = array();
        $data["arrayPropertyTypeFeatures"] = model_propertytypefeature::getFullDetailComposedList();
        $data["amenityList"] = $this->_amenityList;

        /** city **/
        $city = NULL;
        if($this->input->post("city")!= FALSE && is_numeric((int)$this->input->post("city")))
        {
            $city = model_city::getById($this->input->post("city"));
        }
        elseif(is_numeric((int)$data["property"]["inm_ciu_id"]))
        {
            $city = model_city::getById($data["property"]["inm_ciu_id"]);
        }
        if($city instanceof model_city)
        {
            $data["city"] = $city->toArray();
        }
        /** zone **/
        $zone = NULL;
        if($this->input->post("zone")!= FALSE && is_numeric((int)$this->input->post("zone")))
        {
            $zone = model_zone::getById($this->input->post("zone"));
        }
        elseif(is_numeric((int)$data["property"]["inm_zon_id"]))
        {
            $zone = model_zone::getById($data["property"]["inm_zon_id"]);
        }
        if($zone instanceof model_zone)
        {
            $data["zone"] = $zone->toArray();
        }
        /** Get property type list */
        $arrayPropertyTypes = model_propertytype::getAll();
        $data["arrayPropertyTypes"] = $arrayPropertyTypes;

        $data["cancelUrl"] = base_url($cancelUrl);
        $data["isAdmin"] = $this->_isAdmin();
        /** If there isn't a request **/
        if ($this->form_validation->run() === FALSE)
        {
            $this->load_template_backend("admin/property/add", $data);
        }
        else
        {
            $formData = $this->input->post();
            $fileName = "";
            $fileHandler = new File_Handler("");
            if (!empty($_FILES['logo']['name']))
            {
                try
                {
                    $fileName = $fileHandler->uploadImage($_FILES['logo']);
                }
                catch(Exception $e)
                {
                    $data["errorMessage"] = $e->getMessage();
                    $this->load_template_backend("admin/project/add", $data);
                    return;
                }
            }

            $urlSeo = $this->_sanitize($formData["name"]);
            $urlSeo = $this->_toASCII($urlSeo);
            $urlSeo = $this->_validateSeo($urlSeo);

            $price = str_replace(",","",$formData["price"]);
            $surface = str_replace(",","",$formData["surface"]);
            $amenities = "";
            $amenityList = $formData["amenities"];

            if(count($amenityList) > 0)
            {
                foreach ($amenityList as $amenity)
                {
                    $amenities .= $amenity;
                }
            }
            $property = new model_property(
                $formData["name"],
                "",
                "",
                $formData["city"],
                $price,
                $formData["currency-type"],
                $formData["address"],
                $surface,
                $formData["description"],
                $formData["property-type"],
                $formData["zone"],
                $formData["transaction-type"],
                $formData["latitude"],
                $formData["longitude"],
                $formData["uv"],
                $formData["manzano"],
                $formData["surface-type"],
                "",
                "",
                0,
                "Si",
                $urlSeo,
                "Si",
                0,
                0,
                $formData["status"],
                $amenities
            );
            $property->save();
            $property->saveFeatures($formData["features"]);
            $property->createPublication($this->user->usu_id);
            if($this->user->usu_tipo = "Buscador")
            {
                $user = model_user::getById($this->user->usu_id);
                $user->setUserType("Particular");
                $user->save();
            }
            $this->session->set_flashdata('successMessage', 'El inmueble se agregó correctamente, ya puede agregar sus imagenes');
            $this->session->set_flashdata('throwAd', 1);
            $this->session->set_flashdata('lastStepExplanation', 1);
            redirect(base_url("admin/Property/images/".$property->getId()));
        }
    }

    public function edit($propertyId = null)
    {
        $propertyId = $propertyId == ""?$this->uri->segment(4):$propertyId;
        $cancelUrl = $this->_isAdmin()?"admin/property":"dashboard/publications";
        /** validate param **/
        if(!is_numeric($propertyId))
        {
            $this->session->set_flashdata('errorMessage', 'El parametro enviado es incorrecto');
            redirect(base_url($cancelUrl));
        }
        $property = model_property::getById($propertyId);

        if(!$property instanceof model_property)
        {
            $this->session->set_flashdata('errorMessage', 'No se encontró el contenido');
            redirect(base_url($cancelUrl));
        }
        /** complements **/
        $this->complementHandler->addViewComplement("jquery.select2");
        $this->complementHandler->addViewComplement("jquery.select2.bootstrap");
        $this->complementHandler->addViewComplement("bootstrap.datepicker");
        $this->complementHandler->addViewComplement("parsley");
        $this->complementHandler->addViewComplement("parsley.spanish");
        $this->complementHandler->addViewComplement("tinymce");
        $this->complementHandler->addViewComplement("google.maps.api");
        $this->complementHandler->addViewComplement("gmaps");
        $this->complementHandler->addViewComplement("jquery.inputmask.bundle");
        $this->complementHandler->addProjectJs("general.tinymce");
        $this->complementHandler->addProjectJs("PropertyFeatureHandler");
        $this->complementHandler->addProjectJs('property.edit');
        $this->complementHandler->addProjectCss('property.edit');
        $this->complementHandler->addProjectJs("initializeSelect2");

        /** Server Side Validations **/
        $this->form_validation->set_rules('name', 'Titulo del inmueble', 'trim|required');
        $this->form_validation->set_rules('address', 'Direccion', 'trim|required');
        $this->form_validation->set_rules('description', 'Descripcion', 'trim|required');
        $this->form_validation->set_rules('latitude', 'Latitud', 'trim');
        $this->form_validation->set_rules('longitude', 'Longitude', 'trim');
        $this->form_validation->set_rules('uv', 'Uv', 'trim');
        $this->form_validation->set_rules('manzano', 'Manzana', 'trim');
        $this->form_validation->set_rules('surface', 'Superficie', 'trim|required');
        $this->form_validation->set_rules('price', 'Precio', 'trim|required');
        $this->form_validation->set_rules('city', 'Ciudad', 'trim');
        $this->form_validation->set_rules('zone', 'Zona', 'trim');

        $data = array();
        $data["currentAction"] = "edit";
        $data["arrayState"] = $this->_arrayState;
        $data["property"] = $property->toArray();
        $data["arrayTransactionTypes"] = $this->_arrayTransactionTypes;
        $data["arrayPropertyFeatures"] = model_property::getFeaturesFullDetailByPropertyIdComposedList($property->getId());
        $data["arrayPropertyTypeFeatures"] = model_propertytypefeature::getFullDetailComposedList();
        $data["amenityList"] = $this->_amenityList;
        /** publication **/
        $publication = model_publication::getByPropertyId($property->getId());

        /** validate publication and user */
        if(!$publication instanceof model_publication)
        {
            $this->session->set_flashdata('errorMessage', 'This publication from this property is corrupted!');
            redirect(base_url($cancelUrl));
        }
        $data["publication"] = $publication->toArray();
        /** user **/
        $user = $publication->getUser();
        if(!$user instanceof model_user)
        {
            $this->session->set_flashdata('errorMessage', 'This publication from this property is corrupted!');
            redirect(base_url($cancelUrl));
        }
        $data["userProperty"] = $user->toArray();
        if($this->user_id != $user->getId() && !$this->_isAdmin())
        {
            $this->session->set_flashdata('errorMessage', 'Ups.. no puedes modificar ese inmueble!');
            redirect(base_url($cancelUrl));
        }
        /** city **/
        $city = NULL;
        if($this->input->post("city")!= FALSE && is_numeric((int)$this->input->post("city")))
        {
            $city = model_city::getById($this->input->post("city"));
        }
        elseif(is_numeric((int)$data["property"]["inm_ciu_id"]))
        {
            $city = model_city::getById($data["property"]["inm_ciu_id"]);
        }
        if($city instanceof model_city)
        {
            $data["city"] = $city->toArray();
        }
        /** zone **/
        $zone = NULL;
        if($this->input->post("zone")!= FALSE && is_numeric((int)$this->input->post("zone")))
        {
            $zone = model_zone::getById($this->input->post("zone"));
        }
        elseif(is_numeric((int)$data["property"]["inm_zon_id"]))
        {
            $zone = model_zone::getById($data["property"]["inm_zon_id"]);
        }
        if($zone instanceof model_zone)
        {
            $data["zone"] = $zone->toArray();
        }
        /** Get property type list */
        $arrayPropertyTypes = model_propertytype::getAll();
        $data["arrayPropertyTypes"] = $arrayPropertyTypes;

        $data["cancelUrl"] = base_url($cancelUrl);
        $data["isAdmin"] = $this->_isAdmin();
        /** If there isn't a request **/
        if ($this->form_validation->run() === FALSE)
        {
            $this->load_template_backend("admin/property/edit", $data);
        }
        else
        {
            $formData = $this->input->post();
//            echo"<pre>";var_dump($formData);exit;
            $fileName = "";
            $fileHandler = new File_Handler("","projectImage");
            if (!empty($_FILES['logo']['name']))
            {
                try
                {
                    $fileName = $fileHandler->uploadImage($_FILES['logo']);
                }
                catch(Exception $e)
                {
                    $data["errorMessage"] = $e->getMessage();
                    $this->load_template_backend("admin/project/add", $data);
                    return;
                }
            }
            $price = str_replace(",","",$formData["price"]);
            $surface = str_replace(",","",$formData["surface"]);
            $amenities = "";
            $amenityList = $formData["amenities"];
//            echo "<pre>";var_dump($amenityList);
            if(count($amenityList) > 0)
            {
                foreach ($amenityList as $amenity)
                {
                    $amenities .= $amenity;
                }
            }
//            var_dump($amenities);exit;
            $property->setName($formData["name"]);
            $property->setPropertyType($formData["property-type"]);
            $property->setTransactionType($formData["transaction-type"]);
            $property->setCityId($formData["city"]);
            $property->setZoneId($formData["zone"]);
            $property->setAddress($formData["address"]);
            $property->setLatitude($formData["latitude"]);
            $property->setLongitude($formData["longitude"]);
            $property->setDetail($formData["description"]);
            $property->setUv($formData["uv"]);
            $property->setBlock($formData["manzano"]);
            $property->setSurface($surface);
            $property->setSurfaceType($formData["surface-type"]);
            $property->setPrice($price);
            $property->setCurrencyType($formData["currency-type"]);
            $property->setStatus($formData["status"]);
            $property->setAmenities($amenities);
            $property->save();
            $property->saveFeatures($formData["features"]);
            $this->session->set_flashdata('successMessage', 'El inmueble se actualizo correctamente');
            redirect(base_url("admin/Property/edit/".$property->getId()));
        }
    }

    public function images($propertyId = NULL)
    {
        /** validate param **/
        $cancelUrl = $this->_isAdmin()?"admin/property":"dashboard/publications";
        if(!is_numeric($propertyId))
        {
            $this->session->set_flashdata('errorMessage', 'El parametro enviado es incorrecto');
            redirect(base_url($cancelUrl));
        }
        $property = model_property::getById($propertyId);

        if(!$property instanceof model_property)
        {
            $this->session->set_flashdata('errorMessage', 'Ha solicitado editar un inmueble que no existe');
            redirect(base_url($cancelUrl));
        }

        $publication = model_publication::getByPropertyId($property->getId());
        /** user **/
        $user = $publication->getUser();
        if(!$user instanceof model_user)
        {
            $this->session->set_flashdata('errorMessage', 'This publication from this property is corrupted!');
            redirect(base_url($cancelUrl));
        }
        $data["userProperty"] = $user->toArray();
        if($this->user_id != $user->getId() && !$this->_isAdmin())
        {
            $this->session->set_flashdata('errorMessage', 'Ups.. no puedes modificar las imágenes de este inmueble!');
            redirect(base_url($cancelUrl));
        }
        /** Complements **/
        $this->complementHandler->addViewComplement("jquery.datatables");
        $this->complementHandler->addViewComplement("jquery.datatables.bootstrap");
        $this->complementHandler->addViewComplement("dropzone");
        $this->complementHandler->addViewComplement("tinymce");
        $this->complementHandler->addProjectJs("general.tinymce");
        $this->complementHandler->addProjectCss("property.images");
        $this->complementHandler->addProjectJs("property.images");

        $data = array();
        $data["currentAction"] = "images";
        $data["property"] = $property->toArray();
        $data["publication"] = $publication->toArray();
        $formData = $this->input->post();
        if (empty($_FILES))
        {
            $this->load_template_backend("admin/property/images", $data);
        }
        else
        {
            $fileName = "";
            $fileHandler = new File_Handler("");
            if (!empty($_FILES['file']['name']))
            {
                try
                {
                    $fileName = $fileHandler->uploadImage($_FILES['file']);
                }
                catch(Exception $e)
                {
                    $data["errorMessage"] = $e->getMessage();
                    $this->load_template_backend("admin/property/images", $data);
                    return;
                }
            }
            $propertyImage = new model_propertyimage($fileName,$property->getId(),100,FALSE,"");
            $propertyImage->save();
            $arrayItem = $propertyImage->toArray();
            $file = new File_Handler($arrayItem["fot_archivo"]);
            $thumbnail = $file->getThumbnail("150","100");
            $arrayItem["src"] = $thumbnail->getSource();
            echo json_encode($arrayItem);
        }
    }

    public function leads($propertyId = NULL)
    {
        /** validate param **/
        if(!is_numeric($propertyId))
        {
            $this->session->set_flashdata('errorMessage', 'El parametro enviado es incorrecto');
            redirect(base_url("admin/Property"));
        }
        $property = model_property::getById($propertyId);
        if(!$property instanceof model_property)
        {
            $this->session->set_flashdata('errorMessage', 'Ha solicitado editar un inmueble que no existe');
            redirect(base_url("admin/Property"));
        }

        /** Complements **/
        $this->complementHandler->addViewComplement("jquery.datatables");
        $this->complementHandler->addViewComplement("jquery.datatables.bootstrap");
        $this->complementHandler->addViewComplement("jquery.datatables.buttons");
        $this->complementHandler->addViewComplement("jquery.datatables.buttons.bootstrap");
        $this->complementHandler->addViewComplement("jquery.datatables.buttons.flash");
        $this->complementHandler->addViewComplement("jquery.datatables.buttons.html5");
        $this->complementHandler->addViewComplement("jquery.datatables.buttons.print");
        $this->complementHandler->addViewComplement("jquery.datatables.jszip");
        $this->complementHandler->addViewComplement("jquery.datatables.pdfmake");
        $this->complementHandler->addViewComplement("jquery.datatables.vfs_fonts");
        $this->complementHandler->addViewComplement("jquery.datatables.filterdelay");
        $this->complementHandler->addProjectCss("property.facebook-leads");
        $this->complementHandler->addProjectJs("property.facebook-leads");

        $formList = model_facebook_lead_form::getByPropertyId($property->getId());

        $data["property"] = $property->toArray();
        $data["formList"] = $formList;
        $data["currentAction"] = "leads";
        $data["isAdmin"] = $this->_isAdmin();
        $data["arrayPropertyLeads"] = model_facebook_lead::getByPropertyId($property->getId());
        $this->load_template_backend("admin/property/facebook-leads", $data);
    }

    public function delete()
    {

    }

    private function _validateSeo($propertySeo, $propertyId = "")
    {
        $seoExist = model_property::getSeoNotPropertyIdExists($propertySeo, $propertyId);
        if ($seoExist)
        {
            $arrayWords = explode("-",$propertySeo);
            $counter = (int)$arrayWords[count($arrayWords)-1];
            if(is_numeric($arrayWords[count($arrayWords)-1]))
            {
                $counter++;
                $arrayWords[count($arrayWords)-1] = $counter;
            }
            else
            {
                $counter = 1;
                $arrayWords[count($arrayWords)+1] = $counter;
            }

            $propertySeo = implode("-",$arrayWords);
            $propertySeo = $this->_validateSeo($propertySeo,$propertyId);
        }
        return $propertySeo;
    }

    private function _sanitize($string, $force_lowercase = true, $ranal = false)
    {
        $strip = array(
            "°",
            "´",
            "~",
            "`",
            "!",
            "@",
            "#",
            "$",
            "%",
            "^",
            "&",
            "*",
            "(",
            ")",
            "_",
            "=",
            "+",
            "[",
            "{",
            "]",
            "}",
            "\\",
            "|",
            ";",
            ":",
            "\"",
            "'",
            "&#8216;",
            "&#8217;",
            "&#8220;",
            "&#8221;",
            "&#8211;",
            "&#8212;",
            "â€”",
            "â€“",
            ",",
            "<",
            ".",
            ">",
            "/",
            "?"
        );
        $clean = trim(str_replace($strip, "", strip_tags($string)));
        $clean = preg_replace('/\s+/', "-", $clean);
        $clean = ($ranal) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean;
        return ($force_lowercase) ? (function_exists('mb_strtolower')) ? mb_strtolower($clean, 'UTF-8') : strtolower($clean) : $clean;
    }

    private function _toASCII( $str )
    {
        return strtr(utf8_decode($str),
            utf8_decode(
                'ŠŒŽšœžŸ¥µÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝßàáâãäåæçèéêëìíîïðñòóôõöøùúûüýÿ'),
            'SOZsozYYuAAAAAAACEEEEIIIIDNOOOOOOUUUUYsaaaaaaaceeeeiiiionoooooouuuuyy');
    }

    public function pay($publicationId)
    {
        /** complements **/
        $this->complementHandler->addProjectCss("property.pay");
        $this->complementHandler->addProjectJs("property.pay");

        /** Server Side Validations **/
        $this->form_validation->set_rules('principal', 'Principal', 'trim');
        $this->form_validation->set_rules('busqueda', 'Busqueda', 'trim');
        $this->form_validation->set_rules('similares', 'Similares', 'trim');

        $data['mainServices']       = array();
        $data['searchServices']     = array();
        $data['similarServices']    = array();

        $data['concepto'] = "Anuncio";
        $data['publicationId'] = $publicationId;

        /** If there isn't a request **/
        if ($this->form_validation->run() === FALSE)
        {
            $this->load_template_backend('admin/property/pay', $data);
        }
        else
        {
            $formData = $this->input->post();
            //TODO:Extract services ids from $formData["services"]
            echo "<pre>";var_dump($formData);exit;
            $userName = isset($formData["userName"]) && $formData["userName"] != ""?$formData["userName"]:"S/N";
            $userNit = isset($formData["userNit"]) && $formData["userNit"] != ""?$formData["userNit"]:"S/N";
            $statement = $formData["concepto"];
            //begin - Horrible way to assign an Id, but the previous developer choose it!!!!!!!!!!!!!!!!!!!
            $sw   = true;
            $paymentId = NULL;
            while ($sw)
            {
                $paymentId = $this->codigoAlfanumerico(7);
                $paymentExist = model_payment::getById($paymentId);
                if ($paymentExist instanceof model_payment)
                {
                    $sw = false;
                }
            }
            //end - Horrible way to assign an Id, but the previous developer choose it!!!!!!!!!!!!!!!!!!!

            $serviceArrayIds = $formData["services"];
            $totalPaymentService = model_service::getTotalPaymentServiceByArrayIds($serviceArrayIds);
            $paymentDate    = NULL;
            $endPaymentDate = strtotime( '+14 days', strtotime( $paymentDate ) );
            $endPaymentDate = date( 'Y-m-d H:i:s', $endPaymentDate );
            $payment = new model_payment(
                $paymentId,
                "",
                $publicationId,
                NULL,
                $paymentDate,
                $endPaymentDate,
                $userName,
                $userNit,
                $totalPaymentService,
                0,
                "Pendiente",
                NULL,
                $statement,
                $paymentDate
            );

            $payment->save();
            $payment->addServices($serviceArrayIds);

            redirect(base_url("admin/Project/edit/".$project->getId()));
        }
    }

    public function tigoResponse()
    {
        require BASE_DIRECTORY . DS . 'application' . DS .'libraries'.DS. 'TigoMoneyHandler.php';
        $tigoMoneyHandler = new TigoMoneyHandler();
        $tigoStatusResponse = $tigoMoneyHandler->getPaymentStatus('2176816860');
        echo"<pre>";var_dump($tigoStatusResponse->isSuccess(), $tigoStatusResponse->getMessage(), $tigoStatusResponse->getData());exit;
    }
}