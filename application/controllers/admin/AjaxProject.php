<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 28/07/2017
 * Time: 9:30
 */

class AjaxProject extends Private_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->_validateAccess();
        $this->load->model('model_project');
        $this->load->model('model_paymentservice');
        $this->load->model('model_publication');
        $this->load->model('model_property');
        $this->load->model('model_service');
        $this->load->model('model_projectimage');
        $this->load->model('model_typology');
        $this->load->model('model_finishingdetail');
        $this->load->model('model_socialarea');
        $this->load->model('model_projectslideshow');
        $this->load->model('model_user');
        $this->load->library('JqdtHandler');
    }

    public function ajaxDtAllProjects()
    {
        $dt = new JqdtHandler($this->input->post());

        $recordsTotal = model_project::countAll();
        $recordsFiltered = $recordsTotal;
        if (!$dt->hasSearchValue())
        {
            $resultArray = model_project::getAll($dt->getLength(), $dt->getStart(), $dt->getOrderName(0), $dt->getOrderDir(0));
        }
        else
        {
            $resultArray = model_project::search($dt->getSearchValue(), $dt->getLength(), $dt->getStart(), $dt->getOrderName(0), $dt->getOrderDir(0), $dt->getSearchableColumnDefs());
            $recordsFiltered = model_project::searchTotalCount($dt->getSearchValue(),$dt->getSearchableColumnDefs());
        }

        echo $dt->getJsonResponse($recordsTotal, $recordsFiltered, $resultArray);
        exit ;
    }

    public function getDetail()
    {
        $formData = $this->input->post();
        $projectId = $formData["projectId"];
        $project = model_project::getById($projectId);

        $arrayProject = $project->toArray();
        $file = new File_Handler($arrayProject["proy_logo"],"projectImage");
        $thumbnail = $file->getThumbnail("150","100");
        $arrayProject["src"] = $thumbnail->getSource();
        echo json_encode($arrayProject);exit;
    }
    public function deleteResource()
    {
        $formData = $this->input->post();
        $itemId = $formData["itemId"];
        $itemModel = $formData["itemModel"];
        switch($itemModel)
        {
            case "model_projectimage":
                $item = model_projectimage::getById($itemId);
                break;
            case "model_typology":
                $item = model_typology::getById($itemId);
                break;
            case "model_finishingdetail":
                $item = model_finishingdetail::getById($itemId);
                break;
            case "model_socialarea":
                $item = model_socialarea::getById($itemId);
                break;
            case "model_projectslideshow":
                $item = model_projectslideshow::getById($itemId);
                break;
        }
        $item->delete(TRUE);
    }
    /** ********************************************************************** begin - image functions **/
    public function getImages()
    {
        $formData = $this->input->post();
        $projectId = $formData["projectId"];
        $imageList = model_projectimage::getByProjectId($projectId);
        $arrayImageList = array();
        $i=0;
        foreach($imageList as $image)
        {
            $arrayItem = $image->toArray();
            $file = new File_Handler($arrayItem["proy_fot_imagen"],"projectImage");
            $thumbnail = $file->getThumbnail("150","100");
            $arrayItem["src"] = $thumbnail->getSource();
            $arrayImageList[] = $arrayItem;
            $i++;
        }

        echo json_encode($arrayImageList);exit;
    }

    public function updateImageOrder()
    {
        $formData = $this->input->post();
        $imageArrayIds = $formData["ids"];

        $arrayImageToUpdate = array();
        foreach ($imageArrayIds as $order => $id)
        {
            $arrayImageToUpdate[] = array(
                "proy_fot_id" => $id,
                "proy_fot_order" => $order+1
            );
        }
        model_projectimage::updateBatch($arrayImageToUpdate);
    }

    public function getImage()
    {
        $formData = $this->input->post();
        $projectImageId = $formData["projectImageId"];
        $projectImage = model_projectimage::getById($projectImageId);
        $arrayProjectImage = $projectImage->toArray();
        $file = new File_Handler($arrayProjectImage["proy_fot_imagen"],"projectImage");
        $thumbnail = $file->getThumbnail("150","100");
        $arrayProjectImage["src"] = $thumbnail->getSource();
        echo json_encode($arrayProjectImage);exit;
    }

    public function updateImageInformation()
    {
        $formData = $this->input->post();

        $fileName = "";
        $fileHandler = new File_Handler("","projectImage");
        if (!empty($_FILES['file']['name']))
        {
            try
            {
                $fileName = $fileHandler->uploadImage($_FILES['file']);
            }
            catch(Exception $e)
            {
//                $data["errorMessage"] = $e->getMessage();
//                $this->load_template_backend("admin/project/add", $data);
                echo $e->getMessage();
                return;
            }
        }

        $projectImage = model_projectimage::getById($formData["projectImageId"]);
        if($fileName != "")
            $projectImage->setImageName($fileName);
        $adImage = $formData["adImage"] == "1"?"true":"false";
        $projectImage->setIsAdImage($adImage);
        $coverImage = $formData["coverImage"] == "1"?"true":"false";
        $projectImage->setIsCoverImage($coverImage);
        $projectImage->setDescription($formData["description"]);
//        echo"<pre>";var_dump($projectImage,$formData,$_FILES);exit;
        $projectImage->save();
        $arrayItem = $projectImage->toArray();
        $file = new File_Handler($arrayItem["proy_fot_imagen"],"projectImage");
        $thumbnail = $file->getThumbnail("150","100");
        $arrayItem["src"] = $thumbnail->getSource();
        echo json_encode($arrayItem);exit;
    }
    /** ********************************************************************** end - image functions **/
    /** ********************************************************************** begin - typologies functions **/

    public function getTypologies()
    {
        $formData = $this->input->post();
        $projectId = $formData["projectId"];
        $imageList = model_typology::getByProjectId($projectId);
        $arrayImageList = array();
        $i=0;
        foreach($imageList as $image)
        {
            $arrayItem = $image->toArray();
            $file = new File_Handler($arrayItem["tip_imagen"],"projectImage");
            $thumbnail = $file->getThumbnail("150","100");
            $arrayItem["srcImage"] = $thumbnail->getSource();
            $file2 = new File_Handler($arrayItem["tip_imagen_precio"],"projectImage");
            $thumbnail2 = $file2->getThumbnail("150","100");
            $arrayItem["srcImagePrice"] = $thumbnail2->getSource();
            $arrayImageList[] = $arrayItem;
            $i++;
        }

        echo json_encode($arrayImageList);exit;
    }

    public function getTypology()
    {
        $formData = $this->input->post();
        $projectImageId = $formData["projectImageId"];
        $projectImage = model_typology::getById($projectImageId);
        $arrayItem = $projectImage->toArray();
        $file = new File_Handler($arrayItem["tip_imagen"],"projectImage");
        $thumbnail = $file->getThumbnail("150","100");
        $arrayItem["srcImage"] = $thumbnail->getSource();
        $file2 = new File_Handler($arrayItem["tip_imagen_precio"],"projectImage");
        $thumbnail2 = $file2->getThumbnail("150","100");
        $arrayItem["srcImagePrice"] = $thumbnail2->getSource();
        echo json_encode($arrayItem);exit;
    }
    public function updateTypologyInformation()
    {
        $formData = $this->input->post();
        $fileName = "";
        $fileHandler = new File_Handler("","projectImage");
        if (!empty($_FILES['file']['name']))
        {
            try
            {
                $fileName = $fileHandler->uploadImage($_FILES['file']);
            }
            catch(Exception $e)
            {
                echo $e->getMessage();
                return;
            }
        }
        $fileName2 = "";
        if (!empty($_FILES['file2']['name']))
        {
            try
            {
                $fileName2 = $fileHandler->uploadImage($_FILES['file2']);
            }
            catch(Exception $e)
            {
                echo $e->getMessage();
                return;
            }
        }

        $typology = model_typology::getById($formData["typologyId"]);
        $typology->setTitle($formData["title"]);
        $typology->setTitlePrice($formData["titlePrice"]);
        $price = str_replace(",","",$formData["price"]);
        $typology->setPrice($price);
        $builtSurface = str_replace(",","",$formData["builtSurface"]);
        $typology->setBuiltSurface($builtSurface);
        $totalSurface = str_replace(",","",$formData["totalSurface"]);
        $typology->setSurface($totalSurface);
        $typology->setAvailability($formData["availability"]);
        $typology->setShowPrice($formData["showPrice"]);
        $typology->setShowBuiltSurface($formData["showBuiltSurface"]);
        $typology->setShowSurface($formData["showTotalSurface"]);
        $typology->setDescription($formData["description"]);
        if($fileName != "")
            $typology->setImage($fileName);
        if($fileName2 != "")
            $typology->setImagePrice($fileName2);
        $typology->save();
        $arrayItem = $typology->toArray();
        $file = new File_Handler($arrayItem["tip_imagen"],"projectImage");
        $thumbnail = $file->getThumbnail("150","100");
        $arrayItem["srcImage"] = $thumbnail->getSource();
        $file = new File_Handler($arrayItem["tip_imagen_precio"],"projectImage");
        $thumbnail = $file->getThumbnail("150","100");
        $arrayItem["srcImagePrice"] = $thumbnail->getSource();
        echo json_encode($arrayItem);exit;
    }

    /** ********************************************************************** end - typologies functions **/

    /** ********************************************************************** begin - finishing details functions **/
    public function getFinishingDetails()
    {
        $formData = $this->input->post();
        $projectId = $formData["projectId"];
        $imageList = model_finishingdetail::getByProjectId($projectId);
        $arrayImageList = array();
        $i=0;
        foreach($imageList as $image)
        {
            $arrayItem = $image->toArray();
            $file = new File_Handler($arrayItem["aca_imagen"],"projectImage");
            $thumbnail = $file->getThumbnail("150","100");
            $arrayItem["src"] = $thumbnail->getSource();
            $arrayImageList[] = $arrayItem;
            $i++;
        }

        echo json_encode($arrayImageList);exit;
    }

    public function getFinishingDetail()
    {
        $formData = $this->input->post();
        $itemId = $formData["itemId"];
        $projectImage = model_finishingdetail::getById($itemId);
        $arrayProjectImage = $projectImage->toArray();
        $file = new File_Handler($arrayProjectImage["aca_imagen"],"projectImage");
        $thumbnail = $file->getThumbnail("150","100");
        $arrayProjectImage["src"] = $thumbnail->getSource();
        echo json_encode($arrayProjectImage);exit;
    }

    public function updateFinishingDetailInformation()
    {
        $formData = $this->input->post();

        $fileName = "";
        $fileHandler = new File_Handler("","projectImage");
        if (!empty($_FILES['file']['name']))
        {
            try
            {
                $fileName = $fileHandler->uploadImage($_FILES['file']);
            }
            catch(Exception $e)
            {
                echo $e->getMessage();
                return;
            }
        }

        $finishingDetail = model_finishingdetail::getById($formData["itemId"]);
        if($fileName != "")
            $finishingDetail->setImage($fileName);
        $finishingDetail->setDescription($formData["description"]);
        $finishingDetail->save();
        $arrayItem = $finishingDetail->toArray();
        $file = new File_Handler($arrayItem["aca_imagen"],"projectImage");
        $thumbnail = $file->getThumbnail("150","100");
        $arrayItem["src"] = $thumbnail->getSource();
        echo json_encode($arrayItem);exit;
    }
    /** ********************************************************************** end - finishing details functions **/

    /** ********************************************************************** begin - social area functions **/
    public function getSocialAreas()
    {
        $formData = $this->input->post();
        $projectId = $formData["projectId"];
        $imageList = model_socialarea::getByProjectId($projectId);
        $arrayImageList = array();
        $i=0;
        foreach($imageList as $image)
        {
            $arrayItem = $image->toArray();
            $file = new File_Handler($arrayItem["are_soc_imagen"],"projectImage");
            $thumbnail = $file->getThumbnail("150","100");
            $arrayItem["src"] = $thumbnail->getSource();
            $arrayImageList[] = $arrayItem;
            $i++;
        }

        echo json_encode($arrayImageList);exit;
    }

    public function getSocialArea()
    {
        $formData = $this->input->post();
        $itemId = $formData["itemId"];
        $item = model_socialarea::getById($itemId);
        $arrayItem = $item->toArray();
        $file = new File_Handler($arrayItem["are_soc_imagen"],"projectImage");
        $thumbnail = $file->getThumbnail("150","100");
        $arrayItem["src"] = $thumbnail->getSource();
        echo json_encode($arrayItem);exit;
    }

    public function updateSocialAreaInformation()
    {
        $formData = $this->input->post();

        $fileName = "";
        $fileHandler = new File_Handler("","projectImage");
        if (!empty($_FILES['file']['name']))
        {
            try
            {
                $fileName = $fileHandler->uploadImage($_FILES['file']);
            }
            catch(Exception $e)
            {
                echo $e->getMessage();
                return;
            }
        }

        $item = model_socialarea::getById($formData["itemId"]);
        if($fileName != "")
            $item->setImage($fileName);
        $item->setTitle($formData["title"]);
        $item->setDescription($formData["description"]);
        $item->save();
        $arrayItem = $item->toArray();
        $file = new File_Handler($arrayItem["are_soc_imagen"],"projectImage");
        $thumbnail = $file->getThumbnail("150","100");
        $arrayItem["src"] = $thumbnail->getSource();
        echo json_encode($arrayItem);exit;
    }
    /** ********************************************************************** end - social area functions **/
    /** ********************************************************************** begin - slide show functions **/
    public function getSlideShowList()
    {
        $formData = $this->input->post();
        $projectId = $formData["projectId"];
        $imageList = model_projectslideshow::getByProjectId($projectId);
        $arrayImageList = array();
        $i=0;
        foreach($imageList as $image)
        {
            $arrayItem = $image->toArray();
            $file = new File_Handler($arrayItem["imagen"],"projectSlideShowImage");
            $thumbnail = $file->getThumbnail("150","100");
            $arrayItem["src"] = $thumbnail->getSource();
            $arrayImageList[] = $arrayItem;
            $i++;
        }

        echo json_encode($arrayImageList);exit;
    }

    public function getSlideShow()
    {
        $formData = $this->input->post();
        $itemId = $formData["itemId"];
        $item = model_projectslideshow::getById($itemId);
        $arrayItem = $item->toArray();
        $file = new File_Handler($arrayItem["imagen"],"projectSlideShowImage");
        $thumbnail = $file->getThumbnail("150","100");
        $arrayItem["src"] = $thumbnail->getSource();
        echo json_encode($arrayItem);exit;
    }

    public function updateSlideShowInformation()
    {
        $formData = $this->input->post();

        $fileName = "";
        $fileHandler = new File_Handler("","projectSlideShowImage");
        if (!empty($_FILES['file']['name']))
        {
            try
            {
                $fileName = $fileHandler->uploadImage($_FILES['file']);
            }
            catch(Exception $e)
            {
                echo $e->getMessage();
                return;
            }
        }

        $item = model_projectslideshow::getById($formData["itemId"]);
        if($fileName != "")
            $item->setImage($fileName);
        $item->setYouTubeCode($formData["youTubeCode"]);
        $item->save();
        $arrayItem = $item->toArray();
        $file = new File_Handler($arrayItem["imagen"],"projectSlideShowImage");
        $thumbnail = $file->getThumbnail("150","100");
        $arrayItem["src"] = $thumbnail->getSource();
        echo json_encode($arrayItem);exit;
    }
    /** ********************************************************************** end - slide show functions **/
}