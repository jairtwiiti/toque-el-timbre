<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 01/02/2018
 * Time: 11:35
 */

class AjaxNotifySearch extends Private_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_notify_search');
        $this->load->model('model_property');
        $this->load->model('model_transactiontype');
        $this->load->model('model_city');
        $this->load->library('JqdtHandler');
    }
    public function ajaxDtAllNotifies()
    {
        $dt = new JqdtHandler($this->input->post());
        $additionalParameters = $this->input->post("additionalParameters");
        $recordsTotal = model_notify_search::countAll();
        $recordsFiltered = $recordsTotal;
        if (!$dt->hasSearchValue() && count($additionalParameters) == 0)
        {
            $resultArray = model_notify_search::getAll($dt->getLength(), $dt->getStart(), $dt->getOrderName(0), $dt->getOrderDir(0));
        }
        else
        {
            $resultArray = model_notify_search::search($dt->getSearchValue(), $dt->getLength(), $dt->getStart(), $dt->getOrderName(0), $dt->getOrderDir(0), $dt->getSearchableColumnDefs(), $additionalParameters);
            $recordsFiltered = model_notify_search::searchTotalCount($dt->getSearchValue(),$dt->getSearchableColumnDefs(), $additionalParameters);
        }

        echo $dt->getJsonResponse($recordsTotal, $recordsFiltered, $resultArray);
        exit;
    }
}