<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 28/07/2017
 * Time: 4:30
 */

class AjaxZone extends Private_Controller
{
    public function __construct()
    {
        parent::__construct();
        if(! $this->input->is_ajax_request())
        {
            redirect('404');
        }
        $this->load->model('model_project');
        $this->load->model('model_paymentservice');
        $this->load->model('model_publication');
        $this->load->model('model_property');
        $this->load->model('model_service');
        $this->load->model('model_zone');
    }

    public function getAllZonesByStateId()
    {
        $stateId = $this->input->post("stateId");
        $term = $this->input->post("term");
        $limit = $this->input->post("limit");
        $cities = model_zone::search($stateId, $term, $limit, 0, 'zon_nombre', 'asc', array('zon_nombre'));

        $resultArray = array();
        foreach ($cities as $city)
        {
            /**
             * @var Model_Organization
             */
            $city;
            $resultArray[] = array(
                "id" => $city->getId(),
                "text" => $city->getName()
            );
        }

        echo json_encode($resultArray);
        exit ;
    }
}