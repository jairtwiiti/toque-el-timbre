<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 28/07/2017
 * Time: 4:30
 */

class AjaxUser extends Private_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_project');
        $this->load->model('model_paymentservice');
        $this->load->model('model_publication');
        $this->load->model('model_property');
        $this->load->model('model_service');
        $this->load->model('model_city');
        $this->load->model('model_user');
        $this->load->library('JqdtHandler');
    }
    public function ajaxDtAllUsers()
    {
        $dt = new JqdtHandler($this->input->post());
        $additionalParameters = $this->input->post("additionalParameters")??[];
        $recordsTotal = model_user::countAll();
        $recordsFiltered = $recordsTotal;
        if (!$dt->hasSearchValue() && count($additionalParameters) == 0)
        {
            $resultArray = model_user::getAll($dt->getLength(), $dt->getStart(), $dt->getOrderName(0), $dt->getOrderDir(0));
        }
        else
        {
            $resultArray = model_user::search($dt->getSearchValue(), $dt->getLength(), $dt->getStart(), $dt->getOrderName(0), $dt->getOrderDir(0), $dt->getSearchableColumnDefs(), $additionalParameters);
            $recordsFiltered = model_user::searchTotalCount($dt->getSearchValue(),$dt->getSearchableColumnDefs(), $additionalParameters);
        }

        echo $dt->getJsonResponse($recordsTotal, $recordsFiltered, $resultArray);
        exit;
    }
    public function getAllUsers()
    {
//        $this->_validateAccess();
        $term = $this->input->post("term");
        $limit = $this->input->post("limit");
        $users = model_user::search($term, $limit, 0, 'usu_nombre', 'asc', array('concat(TRIM(usu_nombre)," ",TRIM(usu_apellido)," ",TRIM(usu_email))'));

        $resultArray = array();

        foreach ($users as $user)
        {
            $resultArray[] = array(
                "id" => $user->usu_id,
                "text" => $user->usu_nombre." ".$user->usu_apellido." ".$user->usu_email
            );
        }

        echo json_encode($resultArray);
        exit ;
    }

    public function getAllUsersRegisteredByDateRange()
    {
        $startDate = $this->input->post("startDate");
        $endDate = $this->input->post("endDate");
        $reportType = $this->input->post("reportType");
        if($reportType == "0")
        {
            $userList = model_user::getAllUsersRegisteredByDateRange($startDate, $endDate);
        }
        else
        {
            $userList = model_user::getAllUsersRegisteredByDateRangeDetailed($startDate, $endDate);
        }
        echo json_encode((array)$userList);exit;
    }

    public function ajaxCertificateUser()
    {
        $formData = $this->input->post();
        $userId = $formData["userId"];
        $isCertificated = $formData["isCertificated"];
        $user = model_user::getById($userId);
        if($user instanceof  model_user)
        {
            $user->setCertificated($isCertificated);
            $user->save();
            $response = array("success" => 1, "message" => "El usuario ".$user->getEmail()." fue certificado");
        }
        else
        {
            $response = array("success" => 0, "message" => "O.o no se encontro al usuario");
        }

        echo json_encode($response);exit;
    }
//    public function contactMessageResponse()
//    {
//        $formData = $this->input->post();
//        $messageId = $formData["messageId"];
//        $subject = $formData["subject"];
//        $replyMessage = $formData["replyMessage"];
//        $message = model_message::getById($messageId);
//        $response = array("result" => 0, "resultMessage" => "Ocurrio un problema por favor intente de nuevo o comuniquese con la administracion. Gracias.");
//        if($message instanceof model_message)
//        {
//            $send = $message->propertyContactResponse($subject, $replyMessage);
//            if($send)
//            {
//                $response = array("result" => 1, "resultMessage" => "Mensaje enviado correctamente!");
//            }
//        }
//        echo json_encode($response);exit;
//    }
    public function contactMessageResponse()
    {
        $data = array();
        $formData = $this->input->post();
        $messageType = $formData["messageType"];
        $modelMessage = "";
        $subject = "";
        switch ($messageType)
        {
            case "Mensaje_Proyecto":
                $modelMessage = "model_project_message";
                $project = model_project::getById($this->input->post('projectId'));
                $data['reference'] =  "Recibiste una respuesta referente al proyecto <strong>".$project->getName().":</strong>";
                $subject = "Respuesta contacto proyecto";
                break;
            case "Mensaje":
                $modelMessage = "model_message";
                $property = model_property::getById($this->input->post('propertyId'));
                $data['reference'] =  "Recibiste una respuesta referente al anuncio <strong>".$property->getName().":</strong>";
                $subject = "Respuesta contacto inmueble";
                break;
            case "Mensaje inmobiliaria":
                $modelMessage = "model_real_state_message";
                $data['reference'] =  "Recibiste una respuesta referente a <strong>Servicios Inmobiliarios:</strong>";
                $subject = "Respuesta contacto inmobiliaria";
                break;
        }
        $user = model_user::getByEmail($formData['email']);
        /** If the user not exist then let's create it using the data on message*/
        if(!$user instanceof model_user)
        {
            $message = $modelMessage::getById($formData['messageId']);
            $user = model_user::createUserFromMessage($message);
        }

        $view = 'email_template/mensaje_contacto_respuesta';
        $data['user'] = $user->toArray();
        $data['message'] = $formData["replyMessage"];

        $sendMessageResponse = $modelMessage::sendContactMessageResponse($view,$data, $subject);
        echo json_encode($sendMessageResponse);
    }

}