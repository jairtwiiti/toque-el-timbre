<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 19/07/2017
 * Time: 12:23
 */

class Home extends Private_Controller
{
    private $_arrayTransactionTypes;

    public function __construct()
    {
        parent::__construct();
        $this->_validateAccess();
        $this->load->model('model_usuario');
        $this->load->model('model_publicacion');
        $this->load->model('model_proyecto');
        $this->load->model('model_inmueble');
        $this->load->model('visitor');
        $this->load->model('model_categoria');
        $this->load->model('model_propertytypefeature');
        $this->load->model('model_propertytype');
        $this->load->library('image_lib');
        $this->_arrayTransactionTypes = array(
            "1"	=> "VENTA",
            "2"	=> "ALQUILER",
            "3"	=> "ANTICRETICO"
        );
    }

    public function index()
    {
        /** Complements **/
        $this->complementHandler->addViewComplement("jquery.select2");
        $this->complementHandler->addViewComplement("jquery.select2.bootstrap");
        $this->complementHandler->addViewComplement("amcharts3.21");
        $this->complementHandler->addViewComplement("amcharts.serial");
        $this->complementHandler->addViewComplement("amcharts.pie");
        $this->complementHandler->addViewComplement("amcharts.export");
        $this->complementHandler->addViewComplement("amcharts.theme.light");
        $this->complementHandler->addViewComplement("amcharts.lang.es");
        $this->complementHandler->addViewComplement("bootstrap.datepicker");
        $this->complementHandler->addProjectCss("home.index");
        $this->complementHandler->addProjectJs("home.index");
        $this->complementHandler->addProjectJs("initializeSelect2");

        $this->page = 'admin_home';
        $data = array();
        $data["arrayPropertyTypes"] = model_propertytype::getAll();
        $data["arrayTransactionTypes"] = $this->_arrayTransactionTypes;
        $this->load_template_backend('admin/home', $data);
    }

    public function listDirectory()
    {
        ini_set('max_execution_time', 3600);
        $propertyImageDirectory = FCPATH."/admin/imagenes/inmueble";
        $files = scandir($propertyImageDirectory);
        $arrayImage = array();
        $pngSize = 0;
        $jpgSize = 0;
        $size = 0;
        foreach($files as $key => $value)
        {
            //if (!in_array($value,array(".","..","chica","upload_logs","small")) && strpos(strtolower($value),"timthumb_int_")!= false)
            if (is_numeric(strpos($value,"2017_08_15")) || is_numeric(strpos($value,"2017_08_16")))
            {
                 //echo unlink($propertyImageDirectory."/".$value)."<br>";
                //$this->compressImage($value);
                echo $value." => ".$this->fileSizeConvert(filesize($propertyImageDirectory."/".$value))."<br>";
                $arrayImage[] =  $propertyImageDirectory."/".$value;
                $size+=filesize($propertyImageDirectory."/".$value);
                /*if(strpos(strtolower($value),"txt")!= false)
                {
                    $pngSize+=filesize($propertyImageDirectory."/".$value);
                }
                if(strpos(strtolower($value),".jpg")!= false || strpos(strtolower($value),".jpeg")!= false)
                {

                    $jpgSize+=filesize($propertyImageDirectory."/".$value);
                }*/
                /*$oldName = $propertyImageDirectory."/".$value;
                $newName = str_replace("_bkp","",$oldName);
                rename($oldName, $newName);*/
            }
        }
        echo "total size => ".$this->fileSizeConvert($size)."<br>";
        echo $this->create_zip($arrayImage,'my-archive.zip',true);
        //echo "png size => ".$this->fileSizeConvert($pngSize)."<br>jpg/jpeg size => ".$this->fileSizeConvert($jpgSize);
    }
    public function fileSizeConvert($bytes)
    {
        $bytes = floatval($bytes);
        $arBytes = array(
            0 => array(
                "UNIT" => "TB",
                "VALUE" => pow(1024, 4)
            ),
            1 => array(
                "UNIT" => "GB",
                "VALUE" => pow(1024, 3)
            ),
            2 => array(
                "UNIT" => "MB",
                "VALUE" => pow(1024, 2)
            ),
            3 => array(
                "UNIT" => "KB",
                "VALUE" => 1024
            ),
            4 => array(
                "UNIT" => "B",
                "VALUE" => 1
            ),
        );

        foreach($arBytes as $arItem)
        {
            if($bytes >= $arItem["VALUE"])
            {
                $result = $bytes / $arItem["VALUE"];
                $result = str_replace(".", "," , strval(round($result, 2)))." ".$arItem["UNIT"];
                break;
            }
        }
        return $result;
    }
    public function compressImage($fileName, $imageType = "userImage")
    {
        $propertyImageDirectory = FCPATH."/assets/uploads/users";
        $file = new File_Handler("",$imageType);
        $file->enableCreateBackup(TRUE);
        $file->compressImage($propertyImageDirectory,$fileName);
        echo$fileName." => converted<br>";
    }
    private function _array_column_php52(array $input, $columnKey, $indexKey = null)
    {
        $array = array();
        foreach ($input as $value) {
            if ( !array_key_exists($columnKey, $value)) {
                trigger_error("Key \"$columnKey\" does not exist in array");
                return false;
            }
            if (is_null($indexKey)) {
                $array[] = $value[$columnKey];
            }
            else {
                if ( !array_key_exists($indexKey, $value)) {
                    trigger_error("Key \"$indexKey\" does not exist in array");
                    return false;
                }
                if ( ! is_scalar($value[$indexKey])) {
                    trigger_error("Key \"$indexKey\" does not contain scalar value");
                    return false;
                }
                $array[$value[$indexKey]] = $value[$columnKey];
            }
        }
        return $array;
    }

    /* creates a compressed zip file */
    function create_zip($files = array(),$destination = '',$overwrite = false) {
        //if the zip file already exists and overwrite is false, return false
        if(file_exists($destination) && !$overwrite) { return false; }
        //vars
        $valid_files = array();
        //if files were passed in...
        if(is_array($files)) {
            //cycle through each file
            foreach($files as $file) {
                //make sure the file exists
                if(file_exists($file)) {
                    $valid_files[] = $file;
                }
            }
        }
        //if we have good files...
        if(count($valid_files)) {
            //create the archive
            $zip = new ZipArchive();
            if($zip->open($destination,$overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
                return false;
            }
            //add the files
            foreach($valid_files as $file) {
                $zip->addFile($file,$file);
            }
            //debug
            //echo 'The zip archive contains ',$zip->numFiles,' files with a status of ',$zip->status;

            //close the zip -- done!
            $zip->close();

            //check to make sure the file exists
            return file_exists($destination);
        }
        else
        {
            return false;
        }
    }

    public function testMail()
    {
        $this->load->model("model_parametros");
        require_once(BASE_DIRECTORY . DS . 'admin' . DS . 'clases' . DS. 'phpmailer' . DS . 'class.phpmailer.php');
        $parametros = $this->model_parametros->get_all();
        $info = $this->model_inmueble->get_info_property_mail($_POST["id_inmueble"]);
        $insertMessage = TRUE;
        $template = base_url() . "assets/template_mail/mensaje_contacto.html";
        /*$contenido = $this->obtener_contenido($template);
        $contenido = str_replace('@@nombre',$_POST['nombre'],$contenido);
        $contenido = str_replace('@@empresa',$_POST['empresa'],$contenido);
        $contenido = str_replace('@@telefono',$_POST['telefono'],$contenido);
        $contenido = str_replace('@@email',$_POST['email'],$contenido);
        $contenido = str_replace('@@comentario',nl2br($_POST['comentario']),$contenido);
        $contenido = str_replace('@@inm_nombre',$info->inm_nombre,$contenido);
        $contenido = str_replace('@@inm_id',$info->inm_id,$contenido);*/

        $cue_nombre = $parametros->par_salida;
        $cue_pass = $parametros->par_pas_salida;

//        $mail = new PHPMailer(true);
//        $mail->SetLanguage('es');
//        $mail->IsSMTP();
//        $mail->SMTPAuth = true;
//        $mail->Host = $parametros->par_smtp;
//        $mail->Timeout=30;
//        $mail->CharSet = 'utf-8';
//        $mail->Subject = "Contacto Inmueble";
//        $body = "test mail";
//        $mail->MsgHTML($body);
//        $mail->AddAddress("jcussy@mailinator.com");
//        $mail->SetFrom($cue_nombre, model_parametros::SITE_DOMAIN);
//        $mail->Username = $cue_nombre;
//        $mail->Password = $cue_pass;
//        $mail->Send();


        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'toqueeltimbre.com',
            'smtp_port' => '465',
            'smtp_user' => 'info@toqueeltimbre.com',
            'smtp_pass' => 'jcussy123',
            'mailtype' => 'html',
            //'charset' => 'utf-8',
            //'newline' => "\r\n"
        );
//        $config = array();
        $this->load->library('email');
        $this->email->initialize($config);
        $this->email->set_mailtype("html");
        $this->email->from('info@toqueeltimbre.com', 'test');
        //$ci->email->to('jcussy@mailinator.com');
        //$ci->email->to('jaironmanjcs@gmail.com');
        $this->email->to(array('jcussy@mailinator.com'));
        $this->email->subject('prueba de envio');
        $this->email->message("toc toc test");
        $this->email->send();
        echo $this->email->print_debugger(array('headers'));
    }

    public function test()
    {
        $matrix = array(
            "a" => "amenity1",//
            "b" => "amenity2",//
            "c" => "amenity3",//
            "d" => "amenity4"
        );
        array("a","b","c");
        $properties = array(
            array("id" => 1, "name" => "property 1", "amenity" => "ab"),
            array("id" => 1, "name" => "property 2", "amenity" => "b"),
            array("id" => 1, "name" => "property 3", "amenity" => "a"),
            array("id" => 1, "name" => "property 4", "amenity" => "dc")
        );
    }

    public function pagosNet()
    {
        include FCPATH."/application/libraries/PagosNetHandler.php";
//        echo var_dump(intval(date("His")));exit;
        $pagosNet = new PagosNetHandler();
        $pagosNet->cmRegistroPlan();
    }
}