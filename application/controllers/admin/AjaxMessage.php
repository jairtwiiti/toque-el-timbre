<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 02/02/2018
 * Time: 10:00
 */

class AjaxMessage extends Base_page
{
    public function __construct()
    {
        parent::__construct();
        if(! $this->input->is_ajax_request())
        {
            redirect('404');
        }
        $this->load->model("model_message");
        $this->load->library('JqdtHandler');
    }

    public function ajaxDtAllMessages()
    {
        $dt = new JqdtHandler($this->input->post());
        $additionalParameters = $this->input->post("additionalParameters");
        $recordsTotal = model_message::countAll();
        $recordsFiltered = $recordsTotal;

        if (!$dt->hasSearchValue() and $additionalParameters["addresses"] == 3)
        {
            $resultArray = model_message::getAll($dt->getLength(), $dt->getStart(), $dt->getOrderName(0), $dt->getOrderDir(0));
        }
        else
        {
            $resultArray = model_message::search($dt->getSearchValue(), $dt->getLength(), $dt->getStart(), $dt->getOrderName(0), $dt->getOrderDir(0), $dt->getSearchableColumnDefs(),$additionalParameters);
            $recordsFiltered = model_message::searchTotalCount($dt->getSearchValue(),$dt->getSearchableColumnDefs(),$additionalParameters);
        }

        echo $dt->getJsonResponse($recordsTotal, $recordsFiltered, $resultArray);
        exit;
    }
}