<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 28/07/2017
 * Time: 4:30
 */

class AjaxFacebookForm extends Private_Controller
{
    public function __construct()
    {
        parent::__construct();
        if(! $this->input->is_ajax_request())
        {
            redirect('404');
        }
        $this->load->model('model_project');
        $this->load->model('model_paymentservice');
        $this->load->model('model_publication');
        $this->load->model('model_property');
        $this->load->model('model_service');
        $this->load->model('model_city');
        $this->load->model('model_facebook_lead');
        $this->load->model('model_facebook_lead_form');
    }

    public function AddForm()
    {
        $formData = $this->input->post();
        $publicationType = $formData["publicationType"];
        $projectId = $propertyId = NULL;
        if($publicationType == 1)
        {
            $projectId = $formData["publicationId"];
        }
        elseif($publicationType == 2)
        {
            $propertyId = $formData["publicationId"];
        }

        $formId = $formData["formId"];
        $facebookForm = new model_facebook_lead_form(
            $projectId,
            $propertyId,
            $formId
        );
        $facebookForm->save();
    }

    public function getAllForms()
    {
        $this->_validateAccess();
        $term = $this->input->post("term");
        $limit = $this->input->post("limit");
        $forms = model_facebook_lead_form::search($term, $limit, 0, 'flf_name', 'asc', array('flf_name'));

        $resultArray = array();

        foreach ($forms as $form)
        {
            $resultArray[] = array(
                "id" => $form->flf_id,
                "text" => $form->flf_name
            );
        }

        echo json_encode($resultArray);
        exit ;
    }
}