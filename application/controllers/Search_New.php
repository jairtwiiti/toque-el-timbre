<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 15/5/2017
 * Time: 10:33 AM
 */
class Search extends Base_page
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_inmueble');
        $this->load->model('model_forma');
        $this->load->model('model_departamento');
        $this->load->model('model_categoria');
        $this->load->model('model_publicacion');
        $this->load->model('model_proyecto');
        $this->load->model('model_landing');
        $this->load->helper('util_string');
        $this->load->library('session');
    }

    public function index()
    {
        //This section is only to prepare the SEO URL and redirect to it
        if($_POST)
        {
            $config_filter = $this->_getArraySearchCriteria();
            $config_filter_session = (object)$config_filter;
            $this->session->set_userdata(array("back_search" => $config_filter_session));

            $config_filter = $this->translate_encode_url($config_filter);
            $config_filter = http_build_query($config_filter);
            $seoUrl = $this->convert_values_url($config_filter_session);
            redirect($seoUrl);
        }
        else
        {
            $config_filter = $_GET;
            $config_filter_session = $config_filter;
            $config_filter_session = $this->translate_decode_url($config_filter_session);
            $config_filter_session = json_decode(json_encode($config_filter_session), FALSE);
            $config_filter_session = $this->get_filters_segments($config_filter_session);

            $this->session->set_userdata(array("back_search" => $config_filter_session));
        }

        //Check if the filters are empty, if so make a redirect
        $this->_checkEmptyFilters($config_filter_session);

        if($this->agent->is_mobile())
        {
            $this->search_mobile($config_filter_session);
        }
        else
        {
            $config_filter = $this->translate_decode_url($config_filter);

            $config_filter = json_decode(json_encode($config_filter), FALSE);

            $config_filter = $this->get_filters_segments($config_filter);
            //echo"<pre>";var_dump($config_filter);exit;
            if($_GET['layout'] == "mapa" || $_GET['tipo'] == "mapa")
            {
                $config_filter_session->layout = "lista";
                $url_seo_mapa = $this->convert_values_url($config_filter_session);
                $this->session->set_userdata(array("back_search_map" => $url_seo_mapa));
                $this->search_desktop_map($config_filter);
            }
            else
            {
                $this->search_desktop($config_filter);
            }
        }
    }
    private function search_desktop($config_filter)
    {
        $categories = $this->model_categoria->get_all();
        $forma = $this->model_forma->get_all();
        $states = $this->model_departamento->get_all();
        $data['categories'] = json_decode(json_encode($categories));
        $data['forma'] = json_decode(json_encode($forma));
        $data['cities'] = json_decode(json_encode($states));
        $data["box_search"] = $this->load->view("template/box_search", array("states" => $states, "categories" => $categories, "forma" => $forma, "page" => ""), true);

        $this->load->library('pagination');

        $url_base = $this->uri->segment(2);
        $from = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $options['base_url'] = base_url(). 'buscar/'.$url_base.'/p/';
        $options['first_url'] = base_url(). 'buscar/'.$url_base.'/p/1?' . http_build_query($_GET, '', "&");
        $options['per_page'] = 15;
        $options['uri_segment'] = 4;
        $options['first_link'] = '&laquo;';
        $options['last_link'] = '&raquo;';
        $options['next_link'] = '&rsaquo;';
        $options['prev_link'] = '&lsaquo;';
        $options['cur_tag_open'] = '<a class="active">';
        $options['cur_tag_close'] = '</a>';

        $aux = ($from-1) >= 0 ? ($from-1) : 0;
        $return_query = $this->model_publicacion->get_all_publications_query($options['per_page'], $aux, $config_filter);
        //echo"<pre>";var_dump($return_query);exit;
        $data["publications"] = $return_query["results"];
        $options['total_rows'] = $return_query["num_total"];

        $data['total_rows'] = $return_query["num_total"];

        //$choice = $options["total_rows"] / $options["per_page"];
        $options["num_links"] = 10;

        $options['suffix'] = '?' . http_build_query($_GET, '', "&");
        $options['use_page_numbers'] = TRUE;

        $this->pagination->initialize($options);
        $data['pagination'] = $this->pagination->create_links();
        $data['inmueble_projects'] = $this->model_inmueble->get_main_pay_projects(4);
        $data['usuario_id'] = $this->user_id;
        $data['config_filter'] = $config_filter;
        $data['search_template'] = TRUE;

        $this->breadcrumbs->push('Inicio', '/');
        $this->breadcrumbs->push('Buscar', '/buscar');
        $this->breadcrumbs->push('Resultados', '/section/page');
        $data['breacrumbs'] = $this->breadcrumbs->show();

        $title = $config_filter->type . " en " . $config_filter->in . " en " . $config_filter->city;
        $data['title_search'] = $title;
        $this->load_template('frontend/search', $title . " | ToqueElTimbre.com", $data);
    }
    private function search_mobile($config_filter){
        $categories = $this->model_categoria->get_all();
        $forma = $this->model_forma->get_all();
        $states = $this->model_departamento->get_all();
        $data['categories'] = json_decode(json_encode($categories));
        $data['forma'] = json_decode(json_encode($forma));
        $data['cities'] = json_decode(json_encode($states));

        //$return_query = $this->model_publicacion->get_all_publications_query(14, $aux, $config_filter);
        //$data["publications"] = $return_query["results"];
        //$data['total_rows'] = $return_query["num_total"];

        $data['search_template'] = TRUE;
        $data['config_filter'] = $config_filter;
        $filter = array('config'  => $config_filter);
        $this->session->set_userdata($filter);

        $this->breadcrumbs->push('Inicio', '/');
        $this->breadcrumbs->push('Buscar', '/buscar');
        $this->breadcrumbs->push('Resultados', '/section/page');
        $data['breacrumbs'] = $this->breadcrumbs->show();

        $this->load_template('frontend/search_mobile', ".:: ToqueElTimbre ::.", $data);
    }
    public function index_new()
    {

    }

    private function _getArraySearchCriteria()
    {
        //$_POST["city"] = $this->geolocation['city'];
        $var_search = !empty($_POST["search"]) ? $_POST["search"] : null;
        $var_type = !empty($_POST["type"]) ? $_POST["type"] : null;
        $var_in = !empty($_POST["in"]) ? $_POST["in"] : null;
        $var_city = !empty($_POST["city"]) ? $_POST["city"] : null;
        $var_room = !empty($_POST["room"]) ? $_POST["room"] : null;
        $var_bathroom = !empty($_POST["bathroom"]) ? $_POST["bathroom"] : null;
        $var_parking = !empty($_POST["parking"]) ? $_POST["parking"] : null;
        $var_currency =  empty($_POST["currency"]) ? "" : $_POST["currency"];
        $var_layout =  empty($_POST["layout"]) ? "" : $_POST["layout"];


        if($_POST["price_min"] != null && $_POST["price_min"] != null){
            if($_POST["price_min"] <= $_POST["price_max"]){
                $var_price_min = $_POST["price_min"] != null ? $_POST["price_min"] : null;
                $var_price_max = !empty($_POST["price_max"]) ? $_POST["price_max"] : null;
            }else{
                $_POST["price_min"] = 0;
                $_POST["price_max"] = 150000;
            }
        }

        $config_filter = array("search" => $var_search, "type" => $var_type, "in" => $var_in, "city" => $var_city, "room" => $var_room, "bathroom" => $var_bathroom, "parking" => $var_parking, "currency" => $var_currency, "price_min" => $var_price_min, "price_max" => $var_price_max, "layout" => $var_layout);
        foreach($config_filter as $key=>$value) {
            if(is_null($value) || $value == ''){
                unset($config_filter[$key]);
            }
        }

        return $config_filter;
    }

    private function _checkEmptyFilters($config_filter)
    {
        if(empty($config_filter))
        {
            $config_filter = array("search" => null, "type" => null, "in" => null, "city" => null, "room" => null, "bathroom" => null, "parking" => null, "currency" => "Dolares", "price_min" => null, "price_max" => null);
            foreach($config_filter as $key=>$value)
            {
                if(is_null($value) || $value == '')
                {
                    unset($config_filter[$key]);
                }
            }
            $url_seo = $this->convert_values_url(json_decode(json_encode($config_filter)));
            redirect($url_seo);
        }
    }

    public function p()
    {

    }
}