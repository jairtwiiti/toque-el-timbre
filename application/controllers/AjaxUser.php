<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 28/07/2017
 * Time: 4:30
 */

class AjaxUser extends Base_page
{
    public function __construct()
    {
        parent::__construct();
        if(!$this->input->is_ajax_request())
        {
            redirect('404');
        }
        $this->load->library('form_validation');
        $this->load->model('model_project');
        $this->load->model('model_paymentservice');
        $this->load->model('model_publication');
        $this->load->model('model_property');
        $this->load->model('model_service');
        $this->load->model('model_city');
        $this->load->model('model_user');
        $this->load->model("model_parametros");
        $this->load->model("model_real_state_message");
    }

    public function getByFacebookId()
    {
        $facebookId = $this->input->post("facebookId");
        $user = model_user::getByFacebookId($facebookId);
        $response = 0;
        if($user instanceof model_user)
        {
            $response = 1;
        }
        echo $response;
    }

    public function getByEmail()
    {
        $email = $this->input->post("email");
        $user = model_user::getByEmail($email);
    }

    public function login()
    {
        $formData = $this->input->post();
        $email = $formData['email'];
        $password = $formData['password'];
        $response = array("response" => 1,"message" => 'Iniciando sesion..');
        $user = model_user::login($email, $password);
        if(!$user instanceof model_user)
        {
            $response['response'] = 0;
            $response['message'] = 'Usuario o contraseña incorrecta!';
        }
        else
        {
            setcookie('logged_user', serialize((object)$user->toArray()), $this->expire_cookie,'/', $this->getCookieDomain());
        }
        echo json_encode($response);exit;
    }

    public function register()
    {
        $this->form_validation->set_rules('first-name', 'Nombre', 'trim');
        $this->form_validation->set_rules('last-name', 'Apellido', 'trim|required');
        $this->form_validation->set_rules('phone', 'Telefono', 'trim|required|numeric');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        $this->form_validation->set_rules('confirm-password', 'Confirm password', 'trim|required');

        if ($this->form_validation->run() === FALSE)
        {
            $errorList = validation_errors();
            $response['response'] = 0;
            $response['message'] = $errorList;
        }
        else
        {
            $formData = $this->input->post();
            $email = $formData['email'];
            $validationResponse = base_page::validateToken($formData["g-recaptcha-response"]);
            if($validationResponse->success)
            {
                $user = model_user::getByEmail($email);
                if(!$user instanceof model_user)
                {
                    $response = array("response" => 1,"message" => 'Iniciando sesion..');
                    $user = model_user::createUser();
                    if(!$user instanceof model_user)
                    {
                        $response['response'] = 0;
                        $response['message'] = 'No se pudo crear su registro, intente de nuevo por favor!';
                    }
                    else
                    {
                        setcookie('logged_user', serialize((object)$user->toArray()), $this->expire_cookie,'/', $this->getCookieDomain());
                    }
                }
                else
                {
                    $response['response'] = 0;
                    $response['message'] = 'El email ya esta registrado!';
                }
            }
            else
            {
                $response['response'] = 0;
                $response['message'] = 'Nuestro detector de robots encontro una anomalia con sus datos, por favor intente de nuevo! '.$validationResponse->success;
            }

        }

        echo json_encode($response);exit;
    }

    public function loginFacebook()
    {
        $accessToken = $this->input->post("accessToken");
        $graphUser = $this->_getGraphUser($accessToken);
        $user = model_user::getByFacebookId($graphUser["id"]);
        $quickLogin = $this->input->post("quickLogin");
        $response = array();
        $response["userFacebookEmail"] = $graphUser["email"];
        if($user instanceof model_user)
        {
            // setLastLogin
            $lastLogin = date("Y-m-d H:i:s");
            $user->setLastLogin($lastLogin);
            $user->save();
            $user->autoLogin();
            $redirectTo = "dashboard";

            if($quickLogin == 1)
            {
                $redirectTo = $this->input->post("redirectTo");
            }
            elseif($user->getUserType() == "Admin")
            {
                $redirectTo = "admin/Home";
            }
            $response["userFound"] = 1;
            $response["url"] = base_url($redirectTo);
        }
        else
        {
            $response["userFound"] = 0;
            $response["url"] = "";
        }
        echo json_encode($response);exit;
    }

    public function signUpFacebook()
    {
        $email = $this->input->post("email");
        $accessToken = $this->input->post("accessToken");
        $quickLogin = $this->input->post("quickLogin");
        $user = model_user::getByEmail($email);
        $response = array();
        if($user instanceof model_user)
        {
            //If the email exist then don't create the account
            $response["userRegistered"] = 0;
            $response["message"] = "No puedes registrarte con '".$email."' por que ya esta asociado a una cuenta, si la cuenta es suya acceda a ella y presione 'Conectame con facebook'";
            $response["url"] = "";
        }
        else
        {
            $graphUser = $this->_getGraphUser($accessToken);
            $key = $this->_generateKey(9);
            $status = "Habilitado";
            $password = sha1($key);

            $user = new model_user(
                            $graphUser["first_name"],
                            $graphUser["last_name"],
                            $email,
                            $key,
                            "","","","","","","","","","","","","","","","","","","",
                            $status,
                            "Particular",
                            $password,
                            date("Y-m-d H:i:s"),
                            "","","","","","","","",
                            $graphUser["id"]
                                    );
            $user->save();

            $template = base_url() . "assets/template_mail/registro_usuario.html";
            $link = base_url() . 'confirmar/' . $user->getId() . '/' . sha1($key);
            $asunto = 'Confirmacion de Usuario';
            $contenido = $this->_getContent($template);
            $contenido = str_replace('@@email', $email, $contenido);
            $contenido = str_replace('@@password', $password, $contenido);
            $contenido = str_replace('@@url_activacion', $link, $contenido);

            $asunto = 'Confirma tu cuenta';
            //$sw_send = $this->send_mail($contenido, $email, $asunto);

            $user->autoLogin();
            $response["userRegistered"] = 1;
            $response["message"] = "Listo! <br> Un momento por favor, estamos preparando tu cuenta..";

            if($quickLogin == 1)
            {
                $redirectTo = $this->input->post("redirectTo");
            }
            else
            {
                $redirectTo = "dashboard?r=new";
            }
            $response["url"] = base_url($redirectTo);
        }
        echo json_encode($response);exit;
    }

    private function _generateKey($longitud) {
        $key = '';
        $pattern = '1234567890abcdefghijklmnopqrstuvwxyz';
        $max = strlen($pattern)-1;
        for($i=0;$i < $longitud;$i++)
            $key .= $pattern[mt_rand(0,$max)];
        return $key;
    }

    private function _getGraphUser($accessToken)
    {
        require_once FCPATH.'/application/libraries/facebook-sdk/vendor/autoload.php'; // change path as needed
        // begin - retrieve user
        $facebookAppIdAndSecretKey = Base_page::getFacebookAppIdAndSecretKey();
        $fb = new Facebook\Facebook([
            'app_id' => $facebookAppIdAndSecretKey["appId"],
            'app_secret' => $facebookAppIdAndSecretKey["secretKey"],
            'default_graph_version' => 'v2.10',
        ]);
        try {
            // Returns a `Facebook\FacebookResponse` object
            $response = $fb->get('/me?fields=id,name, first_name, last_name, email', $accessToken);
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }
        return $response->getGraphUser();
    }

    private function _getContent($url)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.3) Gecko/20070309 Firefox/2.0.0.3");
        curl_setopt($curl, CURLOPT_URL, $url);
        $contenido = curl_exec($curl);
        curl_close($curl);

        return $contenido;
    }

    private function send_mail($contenido, $email, $asunto)
    {
        require_once(BASE_DIRECTORY . DS . 'admin' . DS . 'clases' . DS. 'phpmailer' . DS . 'class.phpmailer.php');
        $parametros = $this->model_parametros->get_all();

        $cue_nombre = $parametros->par_salida;
        $cue_pass = $parametros->par_pas_salida;

        $mail = new PHPMailer(true);
        $mail->SetLanguage('es');
        $mail->IsSMTP();
        $mail->SMTPAuth = true;
        $mail->Host = $parametros->par_smtp;
        $mail->SMTPSecure = "ssl";
        $mail->Timeout=30;
        $mail->CharSet = 'utf-8';
        $mail->Subject = $asunto;
        $mail->MsgHTML($contenido);
        $mail->AddAddress($email);
        //$mail->AddAddress("cristian.inarra@gmail.com");
        $mail->SetFrom($cue_nombre, model_parametros::SITE_DOMAIN);
        $mail->Username = $cue_nombre;
        $mail->Password = $cue_pass;

        if($mail->Send()){
            return true;
        }else{
            return false;
        }
    }

    public function connectWithFacebook()
    {
        $facebookId = $this->input->post("facebookId");
        $response = array();

        if(is_numeric($this->user_id))
        {
            $response["userConnetectedToFacebook"] = 0;
            $response["message"] = "Tenemos un pequeño inconveniente..<br>Por favor actualize la pagina.";
            $user = model_user::getById($this->user_id);
            if($user instanceof model_user)
            {
                $response["userConnetectedToFacebook"] = 0;
                $response["message"] = "No hemos podido vincular su cuenta de ToqueElTimbre a su cuenta de Facebook.<br>Por favor intente de nuevo";
                if($facebookId != "")
                {
                    $user->setFacebookId($facebookId);
                    $user->save();
                    setcookie('logged_user', serialize((object)$user->toArray()), $this->expire_cookie,'/', $this->getCookieDomain());
                    $response["userConnetectedToFacebook"] = 1;
                    $response["message"] = "Listo!.. esta cuenta de ToqueElTimbre esta vinculada a su cuenta de facebook.";
                }
            }
        }
        else
        {
            $response["userConnetectedToFacebook"] = 0;
            $response["message"] = "Lo siento, hay un problema con la sesion.<br>Por favor cierre su cuenta de ToqueElTimbre e ingrese de nuevo, luego presione el boton 'Conectame con facebook'.<br>Si el problema persiste contacte a atencion al cliente. Gracias.";
        }
        echo json_encode($response);exit;
    }

    public function sendContactMessage()
    {
        $data = array();
        $formData = $this->input->post();
        $realStateUser = model_user::getById($formData['user-id']);
        $view = 'email_template/contact-message-real-state';
        $data['user'] = $realStateUser->toArray();
        $sendMessageResponse = model_real_state_message::sendContactMessage($view,$data,'Contacto Inmobiliaria');
        echo json_encode($sendMessageResponse);
    }
}