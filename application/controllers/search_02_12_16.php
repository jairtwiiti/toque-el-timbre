<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search extends Base_page{

    /**
    * Index Page for this controller.
    *
    * Maps to the following URL
    * 		http://example.com/index.php/welcome
    *	- or -
    * 		http://example.com/index.php/welcome/index
    *	- or -
    * Since this controller is set as the default controller in
    * config/routes.php, it's displayed at http://example.com/
    *
    * So any other public methods not prefixed with an underscore will
    * map to /index.php/welcome/<method_name>
    * @see http://codeigniter.com/user_guide/general/urls.html
    */

    function __construct() {
        parent::__construct();
        $this->load->model('model_inmueble');
        $this->load->model('model_forma');
        $this->load->model('model_departamento');
        $this->load->model('model_categoria');
        $this->load->model('model_publicacion');
        $this->load->model('model_proyecto');
        $this->load->model('model_landing');
        $this->load->helper('util_string');
        $this->load->library('session');
    }

    public function search_seo(){
        $url_seo = $this->uri->segment(2);
        $result = $this->model_landing->get_landing_by_url($url_seo);

        if(!empty($result->config)){
            $config_filter = $result->config;
            $config_filter = json_decode($config_filter);
            foreach($config_filter as $key=>$value) {
                if(is_null($value) || $value == ''){
                    unset($config_filter->$key);
                }
            }
            $url_seo = $this->convert_values_url($config_filter);
            redirect($url_seo);
        }else{
            show_404();
        }
    }

    public function sort(){
        $sort = $_POST["sort-type"];
        $in = $_POST["in"];
        $type = $_POST["type"];
        $city = $_POST["city"];

        $search_session = $this->session->userdata('back_search');
        if(!empty($search_session)){
            $search_session->sort = $sort;
            $url_seo = $this->convert_values_url($search_session);
            $search_session = $this->translate_encode_url($search_session);
            $search_session = http_build_query($search_session, '', '&');
        }else{
            $search_session = array("type" => $type, "city" => $city, "in" => $in, "currency" => "Dolares", "sort" => "price-asc");
            $url_seo = $this->convert_values_url(json_encode($search_session));
            $search_session = $this->translate_encode_url($search_session);
            $search_session = http_build_query($search_session, '', '&');
        }
        redirect($url_seo);
    }

    /*public function search_post(){
        //$config_filter = urldecode($_GET["q"]);
        //$config_filter = urlencode($config_filter);
        $config_filter = http_build_query($_GET, '', '&');
        $config_filter = $this->translate_encode_url($config_filter);
        redirect('buscar/?' . $config_filter);
    }*/

    private function get_post_filters(){
        //$_POST["city"] = $this->geolocation['city'];
        $var_search = !empty($_POST["search"]) ? $_POST["search"] : null;
        $var_type = !empty($_POST["type"]) ? $_POST["type"] : null;
        $var_in = !empty($_POST["in"]) ? $_POST["in"] : null;
        $var_city = !empty($_POST["city"]) ? $_POST["city"] : null;
        $var_room = !empty($_POST["room"]) ? $_POST["room"] : null;
        $var_bathroom = !empty($_POST["bathroom"]) ? $_POST["bathroom"] : null;
        $var_parking = !empty($_POST["parking"]) ? $_POST["parking"] : null;
        $var_currency =  empty($_POST["currency"]) ? "" : $_POST["currency"];
        $var_layout =  empty($_POST["layout"]) ? "" : $_POST["layout"];


        if($_POST["price_min"] != null && $_POST["price_min"] != null){
            if($_POST["price_min"] <= $_POST["price_max"]){
                $var_price_min = $_POST["price_min"] != null ? $_POST["price_min"] : null;
                $var_price_max = !empty($_POST["price_max"]) ? $_POST["price_max"] : null;
            }else{
                $_POST["price_min"] = 0;
                $_POST["price_max"] = 150000;
            }
        }

        $config_filter = array("search" => $var_search, "type" => $var_type, "in" => $var_in, "city" => $var_city, "room" => $var_room, "bathroom" => $var_bathroom, "parking" => $var_parking, "currency" => $var_currency, "price_min" => $var_price_min, "price_max" => $var_price_max, "layout" => $var_layout);
        foreach($config_filter as $key=>$value) {
            if(is_null($value) || $value == ''){
                unset($config_filter[$key]);
            }
        }

        return $config_filter;
    }

    private function filters_empty($config_filter){
        if(empty($config_filter)){
            $config_filter = array("search" => null, "type" => null, "in" => null, "city" => null, "room" => null, "bathroom" => null, "parking" => null, "currency" => "Dolares", "price_min" => null, "price_max" => null);
            foreach($config_filter as $key=>$value) {
                if(is_null($value) || $value == ''){
                    unset($config_filter[$key]);
                }
            }
            $url_seo = $this->convert_values_url(json_decode(json_encode($config_filter)));
            redirect($url_seo);
        }
    }

    public function index() {
        if($_POST){
            $config_filter = $this->get_post_filters();
            $config_filter_session = json_decode(json_encode($config_filter), FALSE);
            $this->session->set_userdata(array("back_search" => $config_filter_session));

            $config_filter = $this->translate_encode_url($config_filter);
            $config_filter = http_build_query($config_filter);
            $url_seo = $this->convert_values_url($config_filter_session);
            redirect($url_seo);
        }else{
            $config_filter = $_GET;
            $config_filter_session = $config_filter;
            $config_filter_session = $this->translate_decode_url($config_filter_session);
            $config_filter_session = json_decode(json_encode($config_filter_session), FALSE);
            $config_filter_session = $this->get_filters_segments($config_filter_session);
            $this->session->set_userdata(array("back_search" => $config_filter_session));
        }

        $this->filters_empty($config_filter_session);

        if($this->agent->is_mobile()){
            $this->search_mobile($config_filter_session);
        }else{
            $config_filter = $this->translate_decode_url($config_filter);
            $config_filter = json_decode(json_encode($config_filter), FALSE);
            $config_filter = $this->get_filters_segments($config_filter);
            if($_GET['layout'] == "mapa" || $_GET['tipo'] == "mapa"){
                $this->search_desktop_map($config_filter);
            }else{
                $this->search_desktop($config_filter);
            }
        }
    }

    private function search_desktop($config_filter){
        $categories = $this->model_categoria->get_all();
        $forma = $this->model_forma->get_all();
        $states = $this->model_departamento->get_all();
        $data['categories'] = json_decode(json_encode($categories));
        $data['forma'] = json_decode(json_encode($forma));
        $data['cities'] = json_decode(json_encode($states));
        $data["box_search"] = $this->load->view("template/box_search", array("states" => $states, "categories" => $categories, "forma" => $forma, "page" => ""), true);

        $this->load->library('pagination');

        $url_base = $this->uri->segment(2);
        $from = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $options['base_url'] = base_url(). 'buscar/'.$url_base.'/p/';
        $options['first_url'] = base_url(). 'buscar/'.$url_base.'/p/1?' . http_build_query($_GET, '', "&");
        $options['per_page'] = 15;
        $options['uri_segment'] = 4;
        $options['first_link'] = '&laquo;';
        $options['last_link'] = '&raquo;';
        $options['next_link'] = '&rsaquo;';
        $options['prev_link'] = '&lsaquo;';
        $options['cur_tag_open'] = '<a class="active">';
        $options['cur_tag_close'] = '</a>';

        $aux = ($from-1) >= 0 ? ($from-1) : 0;
        $return_query = $this->model_publicacion->get_all_publications_query($options['per_page'], $aux, $config_filter);

        $data["publications"] = $return_query["results"];
        $options['total_rows'] = $return_query["num_total"];

        $data['total_rows'] = $return_query["num_total"];

        //$choice = $options["total_rows"] / $options["per_page"];
        $options["num_links"] = 10;

        $options['suffix'] = '?' . http_build_query($_GET, '', "&");
        $options['use_page_numbers'] = TRUE;


        $this->pagination->initialize($options);
        $data['pagination'] = $this->pagination->create_links();
        $data['inmueble_projects'] = $this->model_inmueble->get_main_pay_projects(4);
        $data['usuario_id'] = $this->user_id;
        $data['config_filter'] = $config_filter;
        $data['search_template'] = TRUE;

	    $this->breadcrumbs->push('Inicio', '/');
	    $this->breadcrumbs->push('Buscar', '/buscar');
	    $this->breadcrumbs->push('Resultados', '/section/page');
	    $data['breacrumbs'] = $this->breadcrumbs->show();

	    $this->load_template('frontend/search', ".:: ToqueElTimbre ::.", $data);
    }

    private function search_desktop_map($config_filter){
        $categories = $this->model_categoria->get_all();
        $forma = $this->model_forma->get_all();
        $states = $this->model_departamento->get_all();
        $data['categories'] = json_decode(json_encode($categories));
        $data['forma'] = json_decode(json_encode($forma));
        $data['cities'] = json_decode(json_encode($states));
        $data["box_search"] = $this->load->view("template/box_search", array("states" => $states, "categories" => $categories, "forma" => $forma, "page" => ""), true);

        $return_query = $this->model_publicacion->get_all_publications_query_map($config_filter);

        $data["publications"] = $return_query["results"];
        $data['total_rows'] = $return_query["num_total"];

        $data['inmueble_projects'] = $this->model_inmueble->get_main_pay_projects(4);
        $data['usuario_id'] = $this->user_id;
        $data['config_filter'] = $config_filter;
        $data['search_template'] = TRUE;

        $this->breadcrumbs->push('Inicio', '/');
        $this->breadcrumbs->push('Buscar', '/buscar');
        $this->breadcrumbs->push('Resultados', '/section/page');
        $data['breacrumbs'] = $this->breadcrumbs->show();

        $this->load_template('frontend/search_map', ".:: ToqueElTimbre ::.", $data);
    }

    private function search_mobile($config_filter){
        $categories = $this->model_categoria->get_all();
        $forma = $this->model_forma->get_all();
        $states = $this->model_departamento->get_all();
        $data['categories'] = json_decode(json_encode($categories));
        $data['forma'] = json_decode(json_encode($forma));
        $data['cities'] = json_decode(json_encode($states));

        //$return_query = $this->model_publicacion->get_all_publications_query(14, $aux, $config_filter);
        //$data["publications"] = $return_query["results"];
        //$data['total_rows'] = $return_query["num_total"];

        $data['search_template'] = TRUE;
        $data['config_filter'] = $config_filter;
        $filter = array('config'  => $config_filter);
        $this->session->set_userdata($filter);

	    $this->breadcrumbs->push('Inicio', '/');
	    $this->breadcrumbs->push('Buscar', '/buscar');
	    $this->breadcrumbs->push('Resultados', '/section/page');
	    $data['breacrumbs'] = $this->breadcrumbs->show();

        $this->load_template('frontend/search_mobile', ".:: ToqueElTimbre ::.", $data);
    }

    public function clear(){
        $config_filter = array("search" => null, "type" => "Inmuebles", "in" => "Venta", "city" => "Bolivia", "room" => null, "bathroom" => null, "parking" => null, "currency" => "Dolares", "price_min" => null, "price_max" => null);
        foreach($config_filter as $key=>$value) {
            if(is_null($value) || $value == ''){
                unset($config_filter[$key]);
            }
        }
        $config_filter = json_decode(json_encode($config_filter), false);
        $url_seo = $this->convert_values_url($config_filter);
        redirect($url_seo);
    }

    public function ajax_pagination(){
        $config_filter = $this->session->userdata('config');

        $page = $_POST["offset"];
        $return_query = $this->model_publicacion->get_all_publications_query(14, $page, $config_filter);
        $data["publications"] = $return_query["results"];
        echo $this->load->view("frontend/ajax_load", $data, true);
    }

}
