<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Jair
 * Date: 28/07/2017
 * Time: 4:30
 */

class AjaxPublication extends Base_page
{
    public function __construct()
    {
        parent::__construct();
        if(! $this->input->is_ajax_request())
        {
            redirect('404');
        }
        $this->load->model('model_project');
        $this->load->model('model_paymentservice');
        $this->load->model('model_publication');
        $this->load->model('model_property');
        $this->load->model('model_service');
        $this->load->model('model_city');
        $this->load->model('model_user');
        $this->load->model("model_parametros");
        $this->load->model("model_message");
    }

    public function getAll()
    {
        $this->load->library("URL_Decoder");
        $urlToDecode = $this->input->post("urlRequested");
        $pageSize = $this->input->post("pageSize");
        $offset = ((int)$this->input->post("pageNumber")-1)*$pageSize;
        $onlyProjects = $this->input->post("showOnlyProjects") == 1?TRUE:FALSE;
        $zoneStringArray = $this->input->post("zoneStringArray") == NULL?array():$this->input->post("zoneStringArray");
        $urlDecoder = new URL_Decoder($urlToDecode);
        $urlDecoder->setZoneStringArray($zoneStringArray);
        $orderCriteria = $urlDecoder->getOrderCriteria();
        $totalResult = model_publication::countAllPublicResult($urlDecoder,$onlyProjects);

        $result = model_publication::getAllPublicResult($urlDecoder, $pageSize,$offset,$orderCriteria["orderBy"],$orderCriteria["orderType"],$onlyProjects);
        $response["searchTitle"] = $urlDecoder->getSearchTitle();
//        for($i = 0; $i < count($result); $i++)
//        {
//            $result[$i]["inm_nombre"] = strtolower($result[$i]["inm_nombre"]);
//            $result[$i]["inm_nombre"] = ucfirst($result[$i]["inm_nombre"]);
//            $result[$i]["proy_ubicacion"] = strlen($result[$i]["proy_ubicacion"]) > 97?substr($result[$i]["proy_ubicacion"],0, 97)."...":substr($result[$i]["proy_ubicacion"],0, 97);
//            $result[$i]["inm_direccion"] = strlen($result[$i]["inm_direccion"]) > 97?substr($result[$i]["inm_direccion"],0, 97)."...":substr($result[$i]["inm_direccion"],0, 97);
//        }
        $response["result"] = $result;
        $response["totalResult"] = $totalResult;
        $response["currentPageSelected"] = $offset;
//        echo"<pre>";var_dump($result);exit;

        echo json_encode($response);exit;
    }
    function utf8ize($d) {
        if (is_array($d)) {
            foreach ($d as $k => $v) {
                $d[$k] = $this->utf8ize($v);
            }
        } else if (is_string ($d)) {
            return utf8_encode($d);
        }
        return $d;
    }
    public function getByEmail()
    {
        $email = $this->input->post("email");
        $user = model_user::getByEmail($email);
    }

    public function loginFacebook()
    {
        $accessToken = $this->input->post("accessToken");
        $graphUser = $this->_getGraphUser($accessToken);
        $user = model_user::getByFacebookId($graphUser["id"]);
        $response = array();
        $response["userFacebookEmail"] = $graphUser["email"];
        if($user instanceof model_user)
        {
            // setLastLogin
            $lastLogin = date("Y-m-d H:i:s");
            $user->setLastLogin($lastLogin);
            $user->save();
            setcookie('logged_user', serialize((object)$user->toArray()), $this->expire_cookie,'/', $this->getCookieDomain());
            $redirectTo = "dashboard";
            if($user->getUserType() == "Admin")
            {
                $redirectTo = "admin/Home";
            }
            $response["userFound"] = 1;
            $response["url"] = base_url($redirectTo);
        }
        else
        {
            $response["userFound"] = 0;
            $response["url"] = "";
        }
        echo json_encode($response);exit;
    }

    public function signUpFacebook()
    {
        $email = $this->input->post("email");
        $accessToken = $this->input->post("accessToken");
        $user = model_user::getByEmail($email);
        $response = array();
        if($user instanceof model_user)
        {
            //If the email exist then don't create the account
            $response["userRegistered"] = 0;
            $response["message"] = "No puedes registrarte con '".$email."' por que ya esta asociado a una cuenta, si la cuenta es suya acceda a ella y presione 'Conectame con facebook'";
            $response["url"] = "";
        }
        else
        {
            $graphUser = $this->_getGraphUser($accessToken);
            $key = $this->_generateKey(9);
            $status = "Habilitado";
            $password = sha1($key);

            $user = new model_user(
                            $graphUser["first_name"],
                            $graphUser["last_name"],
                            $email,
                            $key,
                            "","","","","","","","","","","","","","","","","","","",
                            $status,
                            "Particular",
                            $password,
                            date("Y-m-d H:i:s"),
                            "","","","","","","","",
                            $graphUser["id"]
                                    );
            $user->save();

            $template = base_url() . "assets/template_mail/registro_usuario.html";
            $link = base_url() . 'confirmar/' . $user->getId() . '/' . sha1($key);
            $asunto = 'Confirmacion de Usuario';
            $contenido = $this->_getContent($template);
            $contenido = str_replace('@@email', $email, $contenido);
            $contenido = str_replace('@@password', $password, $contenido);
            $contenido = str_replace('@@url_activacion', $link, $contenido);

            $asunto = 'Confirma tu cuenta';
            //$sw_send = $this->send_mail($contenido, $email, $asunto);

            setcookie('logged_user', serialize((object)$user->toArray()), $this->expire_cookie, '/', $this->getCookieDomain());
            $response["userRegistered"] = 1;
            $response["message"] = "Listo! <br> Un momenot por favor, estamos preparando tu cuenta..";
            $response["url"] = base_url("dashboard?r=new");
            //redirect('dashboard?r=new');
        }
        echo json_encode($response);exit;
    }

    private function _generateKey($longitud) {
        $key = '';
        $pattern = '1234567890abcdefghijklmnopqrstuvwxyz';
        $max = strlen($pattern)-1;
        for($i=0;$i < $longitud;$i++) $key .= $pattern[mt_rand(0,$max)];
        return $key;
    }

    private function _getGraphUser($accessToken)
    {
        require_once FCPATH.'/application/libraries/facebook-sdk/vendor/autoload.php'; // change path as needed
        // begin - retrieve user
        $facebookAppIdAndSecretKey = Base_page::getFacebookAppIdAndSecretKey();
        $fb = new Facebook\Facebook([
            'app_id' => $facebookAppIdAndSecretKey["appId"],
            'app_secret' => $facebookAppIdAndSecretKey["secretKey"],
            'default_graph_version' => 'v2.10',
        ]);
        try {
            // Returns a `Facebook\FacebookResponse` object
            $response = $fb->get('/me?fields=id,name, first_name, last_name, email', $accessToken);
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }
        return $response->getGraphUser();
    }

    private function _getContent($url)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.3) Gecko/20070309 Firefox/2.0.0.3");
        curl_setopt($curl, CURLOPT_URL, $url);
        $contenido = curl_exec($curl);
        curl_close($curl);

        return $contenido;
    }

    private function send_mail($contenido, $email, $asunto)
    {
        require_once(BASE_DIRECTORY . DS . 'admin' . DS . 'clases' . DS. 'phpmailer' . DS . 'class.phpmailer.php');
        $parametros = $this->model_parametros->get_all();

        $cue_nombre = $parametros->par_salida;
        $cue_pass = $parametros->par_pas_salida;

        $mail = new PHPMailer(true);
        $mail->SetLanguage('es');
        $mail->IsSMTP();
        $mail->SMTPAuth = true;
        $mail->Host = $parametros->par_smtp;
        $mail->SMTPSecure = "ssl";
        $mail->Timeout=30;
        $mail->CharSet = 'utf-8';
        $mail->Subject = $asunto;
        $mail->MsgHTML($contenido);
        $mail->AddAddress($email);
        //$mail->AddAddress("cristian.inarra@gmail.com");
        $mail->SetFrom($cue_nombre, model_parametros::SITE_DOMAIN);
        $mail->Username = $cue_nombre;
        $mail->Password = $cue_pass;

        if($mail->Send()){
            return true;
        }else{
            return false;
        }
    }

    public function connectWithFacebook()
    {
        $facebookId = $this->input->post("facebookId");
        $response = array();

        if(is_numeric($this->user_id))
        {
            $response["userConnetectedToFacebook"] = 0;
            $response["message"] = "Tenemos un pequeño inconveniente..<br>Por favor actualize la pagina.";
            $user = model_user::getById($this->user_id);
            if($user instanceof model_user)
            {
                $response["userConnetectedToFacebook"] = 0;
                $response["message"] = "No hemos podido vincular su cuenta de ToqueElTimbre a su cuenta de Facebook.<br>Por favor intente de nuevo";
                if($facebookId != "")
                {
                    $user->setFacebookId($facebookId);
                    $user->save();
                    setcookie('logged_user', serialize((object)$user->toArray()), $this->expire_cookie,'/', $this->getCookieDomain());
                    $response["userConnetectedToFacebook"] = 1;
                    $response["message"] = "Listo!.. esta cuenta de ToqueElTimbre esta vinculada a su cuenta de facebook.";
                }
            }
        }
        else
        {
            $response["userConnetectedToFacebook"] = 0;
            $response["message"] = "Lo siento, hay un problema con la sesion.<br>Por favor cierre su cuenta de ToqueElTimbre e ingrese de nuevo, luego presione el boton 'Conectame con facebook'.<br>Si el problema persiste contacte a atencion al cliente. Gracias.";
        }
        echo json_encode($response);exit;
    }

    public function contactMessageResponse()
    {
        $formData = $this->input->post();
        $messageId = $formData["messageId"];
        $subject = $formData["subject"];
        $replyMessage = $formData["replyMessage"];
        $message = model_message::getById($messageId);
        $response = array("result" => 0, "resultMessage" => "Ocurrio un problema por favor intente de nuevo o comuniquese con la administracion. Gracias.");
        if($message instanceof model_message)
        {
            $send = $message->propertyContactResponse($subject, $replyMessage);
            if($send)
            {
                $response = array("result" => 1, "resultMessage" => "Mensaje enviado correctamente!");
            }
        }
        echo json_encode($response);exit;
    }



    public function getPublicationsOnMainPage()
    {
        $list = model_publication::getPublicationsOnMainPage(6);
        echo json_encode($list);exit;
    }

    public function getByRealStateId()
    {
        $formData = $this->input->post();
        $userId = $formData["userId"];
        $limit = $formData["pageSize"];
        $offset = ((int)$this->input->post("pageNumber")-1)*$limit;
        $list = model_publication::getRealStatePublications($userId, $limit, $offset);
        $total = model_publication::countAllRealStatePublications($userId);
        $response["totalResult"] = $total;
        $response["result"] = $list;
        $response["currentPageSelected"] = $offset;
        echo json_encode($response);exit;
    }

    public function registerVisitor()
    {
        $formData = $this->input->post();
        $visitorType = $formData["visitorType"];
        $visitorTypeId = $formData["visitorTypeId"];
        $this->registerLogVisitor($visitorType, $visitorTypeId);
    }

    public function delete($publicationId)
    {
         /** @var model_publication $publication */
        $publication = model_publication::getById($publicationId);
        /** @var model_user $publicationUser */
        $publicationUser = $publication->getUser();
        if($this->user->usu_id == $publicationUser->getId() || $this->user->usu_tipo == "Admin")
        {
            $publication->setStatus("Eliminado");
            $publication->save();
            $response["success"] = 1;
            $response["message"] = "Publicacion eliminada";
        }
        else
        {
            $response["success"] = 0;
            $response["message"] = "No tiene permisos para eliminar esta publicacion";
        }
        echo json_encode($response);exit;
    }
}