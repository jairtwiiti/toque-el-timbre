<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: David
 * Date: 10/10/2014
 * Time: 10:38
 */
require_once FCPATH . '/application/libraries/facebook-php-ads-sdk/vendor/autoload.php';
use FacebookAds\Api;
use FacebookAds\Object\Lead;
use FacebookAds\Object\LeadgenForm;

class testFacebookApi extends Base_page
{
    /**
    * Index Page for this controller.
    *
    * Maps to the following URL
    * 		http://example.com/index.php/welcome
    *	- or -
    * 		http://example.com/index.php/welcome/index
    *	- or -
    * Since this controller is set as the default controller in
    * config/routes.php, it's displayed at http://example.com/
    *
    * So any other public methods not prefixed with an underscore will
    * map to /index.php/welcome/<method_name>
    * @see http://codeigniter.com/user_guide/general/urls.html
    */

    function __construct()
    {
        parent::__construct();
        $this->load->model('model_departamento');
        $this->load->model('model_proyecto');
        $this->load->model('model_inmueble');
        $this->load->model('model_categoria');
        $this->load->model('model_slideshow');
        $this->load->model('model_forma');
        $this->load->model('model_publicacion');
        $this->load->helper('util_string');
    }

    public function index()
    {

    }

    public function tetWebhook()
    {
        /*facebook webhook */
        $log = FCPATH."/assets/uploads/log.txt";
        $me = "desafio-tet";
        $content = file_get_contents("php://input");
        $content = json_decode($content, TRUE);
        if(isset($_GET['hub_mode']) && isset($_GET['hub_verify_token']) && $_GET['hub_verify_token'] == $me)
        {
            echo $_GET['hub_challenge'];

        }
        $content = is_null($content)?array():$content;
        $adManagement = new AdManagement($content);
        $adManagement->saveLeadgenOnSystem();
        ob_start();
        //
        print_r($content);
        //
        $data = ob_get_contents();
        ob_end_clean();
        file_put_contents($log,$data,FILE_APPEND);
    }

    public function facebookAdManagementLogin()
    {
        $keys = static::getFacebookAppIdAndSecretKey();     // Add to header of your file
        $data = array();
        $data["appId"] = $keys["appId"];
        $this->load->view("facebook-ad-management-login", $data);
    }

    public function adManagement()
    {
        $keys = static::getFacebookAppIdAndSecretKey();
        Api::init(
            $keys["appKey"], // App ID
            $keys["secretKey"],
            Base_page::getAccessTokenApiMarketing() // Your user access token
        );
        $form = new LeadgenForm("2047079508639525");
        $lead = new Lead("2046469648933291");

        echo"<pre>";var_dump("Form: ",$form->read()->getData(),"Lead: ",$lead->read()->getData());exit;
    }

    public function tetArray()
    {
        $test =
            array(
                array("name" => "consulta", "value" => "my consulta"),
                array("name" => "email", "value" => "jaironman_jcs@hotmail.com"),
                array("name" => "phone_number", "value" => "jaironman_jcs@hotmail.com")
            );
        $key1 = array_search("phone_number", array_column($test, 'name'));
        $key2 = array_search("full_name", array_column($test, 'name'));
        var_dump($key1, $key2);exit;
    }

    public function testLeadView()
    {
        $this->load->view("email_template/contact-message-facebook-lead");
    }

    function array_multi_key_exists( $arrNeedles, $arrHaystack){
        $Needle = array_shift($arrNeedles);
        if(count($arrNeedles) == 0){
            return(array_key_exists($Needle, $arrHaystack));
        }else{
            if(!array_key_exists($Needle, $arrHaystack)){
                return false;
            }else{
                return array_multi_key_exists($arrNeedles, $arrHaystack[$Needle]);
            }
        }
    }

    public function testLibrary()
    {
        $content = Array
                (
                "entry" => Array
                            (
                            "0" => Array
                                    (
                                        "changes" => Array
                                                (
                                                    "0" => Array
                                                            (
                                                                "field" => "leadgen",
                                                                "value" => Array
                                                                        (
                                                                            "created_time" => "1521471590",
                                                                            "page_id" => "288985344521207",
                                                                            "form_id" => "2047079508639525",
                                                                            "leadgen_id" => "2046469648933291"
                                                                        )

                                                            )

                                                ),

                                        "id" => "288985344521207",
                                        "time" => "1521471591"
                                    )

                            ),

                "object" => "page"
                );
        $adManagement = new AdManagement($content);
        $adManagement->saveLeadgenOnSystem();
    }
}
