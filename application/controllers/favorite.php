<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Christian
 * Date: 13/11/2014
 * Time: 11:47
 */

class Favorite extends Base_page
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('model_inmueble');
        $this->load->model('model_publicacion');
        $this->load->model('model_parametros');
        $this->load->model('model_departamento');
        $this->load->model('model_categoria');
        $this->load->model('model_forma');
        $this->load->library('session');
    }

    public function index()
    {
        $data['cities'] = $this->model_departamento->get_states();
        $data['categories'] = $this->model_categoria->get_categories();
        $data['forma'] = $this->model_forma->get_all();
        $search_cookie = $this->session->userdata("back_search");
        $data['back_search'] = $search_cookie;

        $favorites_ids = $this->session->userdata('favorite');
        if(!empty($favorites_ids))
        {
            $data['favorites'] = $this->model_publicacion->get_favorites_by_ids($favorites_ids);
            if($_POST)
            {
                $data['message'] = $this->html_email_favorites($data['favorites']);
            }

        }
        else
        {
            $id_user = $this->user_id;
            if(!empty($id_user)){
                $data['favorites'] = $this->model_publicacion->get_favorites_by_user_id($id_user);
                if($_POST) {
                    $data['message'] = $this->html_email_favorites($data['favorites']);
                }
            }else{
                $data['favorites'] = array();
            }

        }
        $this->load_template('frontend/list_favorites', ".:: ToqueElTimbre ::.", $data);
    }

    public function html_email_favorites($favorites){
        foreach($favorites as $fav):
            $url_detalle = base_url();
            $url_detalle .= seo_url($fav->dep_nombre)."/";
            $url_detalle .= seo_url($fav->cat_nombre . " EN ". $fav->for_descripcion)."/";
            $url_detalle .= seo_url($fav->inm_seo).".html";

            $html .= "<p>";
            $html .= $fav->inm_direccion . "<br>";
            $html .= '<a href="'. $url_detalle .'">'.$fav->inm_nombre.'</a><br>';
            $html .= $fav->zon_nombre .'<br>';
            $html .= '<b>Precio:</b> '.$fav->mon_abreviado . " " .number_format($fav->inm_precio, 0, ",", ".");
            foreach($fav->features as $feature):
                if($feature->caracteristica == 'habitaciones'){
                    $html .= '/ ' .$feature->valor . ' Habitaciones';
                }
                if($feature->caracteristica == 'banos'){
                    $html .= '/ ' .$feature->valor . ' Baños';
                }
            endforeach;
            $html .= '<br>';
            $html .= '<b>Referencia:</b> '.$fav->usu_nombre . " " . $fav->usu_apellido .' - '. $fav->usu_telefono;
            $html .= "</p>";
        endforeach;
        return $this->send_mail($html);
    }

    public function send_mail($content_mail){
        require_once(BASE_DIRECTORY . DS . 'admin' . DS . 'clases' . DS. 'phpmailer' . DS . 'class.phpmailer.php');
        $parametros = $this->model_parametros->get_all();
        $email_send = $_POST['email'];

        $template = base_url() . "assets/template_mail/mensaje_favoritos_mail.html";
        $contenido = $this->obtener_contenido($template);
        $contenido = str_replace('@@nombre', "Busquedas favoritas",$contenido);
        $contenido = str_replace('@@mensaje',$content_mail,$contenido);

        $cue_nombre = $parametros->par_salida;
        $cue_pass = $parametros->par_pas_salida;

        $mail = new PHPMailer(true);
        $mail->SetLanguage('es');
        $mail->IsSMTP();
        $mail->SMTPAuth = true;
        $mail->Host = $parametros->par_smtp;
        $mail->Timeout=30;
        $mail->CharSet = 'utf-8';
        $mail->Subject = "Resultados de su Busqueda";
        $body = $contenido;
        $mail->MsgHTML($body);
        $mail->AddAddress($email_send);
        $mail->SetFrom($cue_nombre, model_parametros::SITE_DOMAIN);
        $mail->Username = $cue_nombre;
        $mail->Password = $cue_pass;

        if($mail->Send()){
            return '<div class="alert alert-success">Su mensaje fue enviado correctamente</div>';
        }else{
            return '<div class="alert alert-danger">Su mensaje no puedo ser enviado.</div>';
        }
    }

    private function obtener_contenido($url)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.3) Gecko/20070309 Firefox/2.0.0.3");
        curl_setopt($curl, CURLOPT_URL, $url);
        $contenido = curl_exec($curl);
        curl_close($curl);

        return $contenido;
    }

}