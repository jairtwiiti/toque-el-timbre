<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Christian
 * Date: 18/11/2014
 * Time: 10:24
 */

class Login extends Private_Controller{

    protected $open_auth_config;

    public function __construct() {
        parent::__construct();
        $this->load->model('model_usuario');
        $this->load->model('model_parametros');
    }

    public function index()
    {
        // Añadimos las reglas necesarias.
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('password', 'Contraseña', 'required');

        $this->form_validation->set_message('valid_email', 'Ingrese un email valido.');
        $this->form_validation->set_message('required', 'El campo %s es requerido');

        if($this->input->post())
        {
            $email = $this->input->post('email', TRUE);
            $email = strtolower($email);
            $password = $this->input->post('password', TRUE);
            $url_redirect = $this->input->post('url_redirect', TRUE);

            if ($this->form_validation->run() == TRUE)
            {
                $user = $this->model_usuario->authentification($email, $password);
                if (!empty($user))
                {
                    // Ultima vez que logueo
                    $values = array("usu_last_login" => date("Y-m-d H:i:s"));
                    $this->model_usuario->update($user->usu_id, $values);

                    //$this->session->set_userdata('logged_user', $user);
                    setcookie('logged_user', serialize($user), $this->expire_cookie,'/', $this->getCookieDomain());
                    if(!empty($url_redirect))
                    {
                        redirect($url_redirect);
                    }
                    else
                    {
                        $redirectTo = "dashboard";
                       if($user->usu_tipo == "Admin")
                       {
                           $redirectTo = "admin/Home";
                       }
                        redirect($redirectTo);
                    }
                }
                else
                {
                    $data['error_login'] = 'El email o contraseña son incorrectos.';
                }
            }
            else
            {
                $data['error_validation'] = strip_tags(@validation_errors());
            }
        }
        /****** facebook  */
        $facebookAppIdAndSecretKey = Base_page::getFacebookAppIdAndSecretKey();
        $data["facebookAppIdAndSecretKey"] = $facebookAppIdAndSecretKey;
        /****** facebook  */
        $this->load->view('backend/login', $data);
    }

    public function logout()
    {
        $this->session->unset_userdata('logged_user');
        unset($_COOKIE['logged_user']);

        setcookie('logged_user', null, -1, '/', $this->getCookieDomain());
        redirect('login');
    }

    public function register(){
        $this->load->library('recaptcha');

        if($_POST){
            $recaptcha = $this->input->post('g-recaptcha-response');
            $response = $this->recaptcha->verifyResponse($recaptcha);

            if (isset($response['success']) && $response['success'] == true) {
                $key = $this->generate_key(9);

                $status = "Habilitado";
                $password = sha1($this->input->post('password'));

                $values = array(
                    "usu_email" => $this->input->post('email'),
                    "usu_password" => $password,
                    "usu_ciu_id" => 0,
                    "usu_estado" => $status,
                    "usu_tipo" => "Particular",
                    "usu_fecha_registro" => date("Y-m-d H:i:s"),
                    "usu_key" => $key
                );
                $id_user = $this->model_usuario->insert($values);

                $template = base_url() . "assets/template_mail/registro_usuario.html";
                $link = base_url() . 'confirmar/' . $id_user . '/' . sha1($key);
                $asunto = 'Confirmacion de Usuario';
                $contenido = $this->obtener_contenido($template);
                $contenido = str_replace('@@email', $this->input->post('email'), $contenido);
                $contenido = str_replace('@@password', $this->input->post('password'), $contenido);
                $contenido = str_replace('@@url_activacion', $link, $contenido);

                $email = $this->input->post('email');
                $password = $this->input->post('password');
                $asunto = 'Confirma tu cuenta';
                $sw_send = $this->send_mail($contenido, $email, $asunto);

                $user = $this->model_usuario->authentification($email, $password);
                setcookie('logged_user', serialize($user), $this->expire_cookie, '/', $this->getCookieDomain());
                redirect('dashboard?r=new');

            }else{
                $data['error_captcha'] = 'El captcha ingresado es invalido.';
            }
        }
        $data['widget'] = $this->recaptcha->getWidget();
        $data['script'] = $this->recaptcha->getScriptTag();
        /****** facebook  */
        $facebookAppIdAndSecretKey = Base_page::getFacebookAppIdAndSecretKey();
        $data["facebookAppIdAndSecretKey"] = $facebookAppIdAndSecretKey;
        /****** facebook  */
        $this->load->view('backend/register', $data);
    }

    public function forgot_password(){
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');

        $this->form_validation->set_message('valid_email', 'Ingrese un email valido.');
        $this->form_validation->set_message('required', 'El campo %s es requerido');

        if($this->input->post()){
            $email = $this->input->post('email', TRUE);

            if ($this->form_validation->run() == TRUE) {
                $user = $this->model_usuario->get_user_by_email($email);
                if (!empty($user)) {
                    $key = $this->generate_key(9);
                    $data_change = array(
                        "usu_key" => $key
                    );
                    $this->model_usuario->update($user->usu_id, $data_change);

                    $template = base_url() . "assets/template_mail/recuperar_contrasena.html";
                    $link = base_url() . 'cambiar-contrasena/'.sha1($user->usu_email).'/'.sha1($key);
                    $asunto = 'Recuperar Contraseña';
                    $contenido = $this->obtener_contenido($template);
                    $contenido = str_replace('@@nombre', $user->usu_nombre, $contenido);
                    $contenido = str_replace('@@link', $link, $contenido);

                    if($this->send_mail($contenido, $email, $asunto)){
                        $data['ok_mail'] = 'Mensaje enviado correctamente.';
                    }else{
                        $data['error'] = 'El mensaje no pudo ser enviado.';
                    }
                }else{
                    $data['error'] = 'No existe ningun registro con ese email.';
                }
            }else{
                $data['error_validation'] = strip_tags(@validation_errors());
            }
        }
        $this->load->view('backend/reset_password', $data);
    }

    public function send_mail($contenido, $email, $asunto)
    {
        require_once(BASE_DIRECTORY . DS . 'admin' . DS . 'clases' . DS. 'phpmailer' . DS . 'class.phpmailer.php');
        $parametros = $this->model_parametros->get_all();

        $cue_nombre = $parametros->par_salida;
        $cue_pass = $parametros->par_pas_salida;

        $mail = new PHPMailer(true);
        $mail->SetLanguage('es');
        $mail->IsSMTP();
        $mail->SMTPAuth = true;
        $mail->Host = $parametros->par_smtp;
        $mail->Timeout=30;
        $mail->CharSet = 'utf-8';
        $mail->Subject = $asunto;
        $mail->MsgHTML($contenido);
        $mail->AddAddress($email);
        //$mail->AddAddress("cristian.inarra@gmail.com");
        $mail->SetFrom($cue_nombre, model_parametros::SITE_DOMAIN);
        $mail->Username = $cue_nombre;
        $mail->Password = $cue_pass;

        if($mail->Send()){
            return true;
        }else{
            return false;
        }
    }

    private function obtener_contenido($url)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.3) Gecko/20070309 Firefox/2.0.0.3");
        curl_setopt($curl, CURLOPT_URL, $url);
        $contenido = curl_exec($curl);
        curl_close($curl);

        return $contenido;
    }

    private function generate_key($longitud) {
        $key = '';
        $pattern = '1234567890abcdefghijklmnopqrstuvwxyz';
        $max = strlen($pattern)-1;
        for($i=0;$i < $longitud;$i++) $key .= $pattern{mt_rand(0,$max)};
        return $key;
    }

}