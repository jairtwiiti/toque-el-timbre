<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Christian
 * Date: 16/01/2015
 * Time: 14:33
 */

class Subscriptions extends Private_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_usuario');
        $this->load->model('model_suscripcion');
        $this->load->model('model_pagos');

        $this->breadcrumb['level2'] = array('name' => 'Suscripciones', 'link' => base_url() . 'dashboard/subscriptions');
        $this->page_title = 'Mis Suscripciones';
        $this->page = 'subscription';
    }

    public function index(){
        $data['user_info'] = $this->model_usuario->get_info_agent($this->user_id);
        $data['total'] = $this->model_suscripcion->get_num_subscription($this->user_id);

        $this->load->library('pagination');

        $from = ($this->uri->segment(4)) ? $this->uri->segment(4) : 1;
        $options['base_url'] = base_url(). 'dashboard/subscriptions/p/';
        $options['first_url'] = base_url(). 'dashboard/subscriptions/p/1';
        $options['per_page'] = 30;
        $options['uri_segment'] = 4;
        $options['first_link'] = '&laquo;';
        $options['last_link'] = '&raquo;';
        $options['next_link'] = '&rsaquo;';
        $options['prev_link'] = '&lsaquo;';
        $options['num_tag_open'] = '<li class="paginate_button">';
        $options['num_tag_close'] = '</li>';
        $options['cur_tag_open'] = '<li class="paginate_button active"><a href="javascript:;">';
        $options['cur_tag_close'] = '</a></li>';
        $options['next_link'] = '&gt;';
        $options['next_tag_open'] = '<li class="paginate_button next">';
        $options['next_tag_close'] = '</li>';
        $options['prev_link'] = '&lt;';
        $options['prev_tag_open'] = '<li class="paginate_button previous">';
        $options['prev_tag_close'] = '</li>';

        $options['total_rows'] = $data['total'];
        $choice = ceil($options["total_rows"] / $options["per_page"]);
        $options["num_links"] = $choice;
        //$options['suffix'] = '?' . http_build_query($_GET, '', "&");
        $options['use_page_numbers'] = TRUE;

        $this->pagination->initialize($options);
        $data['pagination'] = $this->pagination->create_links();
        $data['subscriptions'] = $this->model_suscripcion->get_subscription($this->user_id, $from, $options['per_page']);
        $data['certification'] = $this->sw_certification;

        $this->load_template_backend('backend/subscription/list', $data);
    }

    public function delete(){
        $pag_id = $this->uri->segment(4);

        $sus_values = array(
            "estado" => 'Eliminado'
        );
        $this->model_suscripcion->update_by_pag_id($pag_id, $sus_values);
        $pag_values = array(
            "pag_estado" => 'Eliminado'
        );
        $this->model_pagos->update($pag_id, $pag_values);
        redirect('dashboard/subscriptions?action=delete');
    }
}