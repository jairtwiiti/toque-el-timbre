<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Christian
 * Date: 08/01/2015
 * Time: 11:29
 */
class Projects extends Private_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_usuario');
        $this->load->model('model_proyecto');
        $this->load->model('model_tipologia');
        $this->breadcrumb['level2'] = array('name' => 'Proyectos', 'link' => base_url().'dashboard/projects');
        $this->page_title = 'Mis Proyectos';
        $this->page = 'project';
    }

    public function index(){
        $data['user_info'] = $this->model_usuario->get_info_agent($this->user_id);
        $data['total'] = $this->model_proyecto->get_num_projects_by_user($this->user_id);

        $this->load->library('pagination');

        $from = ($this->uri->segment(4)) ? $this->uri->segment(4) : 1;
        $options['base_url'] = base_url(). 'dashboard/projects/p/';
        $options['first_url'] = base_url(). 'dashboard/projects/p/1';
        $options['per_page'] = 30;
        $options['uri_segment'] = 4;
        $options['first_link'] = '&laquo;';
        $options['last_link'] = '&raquo;';
        $options['next_link'] = '&rsaquo;';
        $options['prev_link'] = '&lsaquo;';
        $options['num_tag_open'] = '<li class="paginate_button">';
        $options['num_tag_close'] = '</li>';
        $options['cur_tag_open'] = '<li class="paginate_button active"><a href="javascript:;">';
        $options['cur_tag_close'] = '</a></li>';
        $options['next_link'] = '&gt;';
        $options['next_tag_open'] = '<li class="paginate_button next">';
        $options['next_tag_close'] = '</li>';
        $options['prev_link'] = '&lt;';
        $options['prev_tag_open'] = '<li class="paginate_button previous">';
        $options['prev_tag_close'] = '</li>';

        $options['total_rows'] = $data['total'];
        $choice = ceil($options["total_rows"] / $options["per_page"]);
        $options["num_links"] = $choice;
        //$options['suffix'] = '?' . http_build_query($_GET, '', "&");
        $options['use_page_numbers'] = TRUE;

        $this->pagination->initialize($options);
        $data['pagination'] = $this->pagination->create_links();
        $data['projects'] = $this->model_proyecto->get_list_projects_by_user($this->user_id, $from, $options['per_page']);

        $this->load_template_backend('backend/projects/list', $data);
    }

    public function available(){
        $project_id = $this->uri->segment(4);
        $data['tipologias'] = $this->model_tipologia->get_tipologia_list($project_id);
        $this->load_template_backend('backend/projects/available', $data);
    }
}