<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Christian
 * Date: 04/12/2014
 * Time: 15:01
 */

class Payment extends Private_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_usuario');
        $this->load->model('model_pagos');
        $this->load->model('model_suscripcion');
        $this->load->model('model_publicacion');
	    $this->load->model('model_servicios');
	    $this->load->model('model_pago_servicio');
        $this->load->model('model_publicacion');
        $this->load->model('model_parametros');

        $this->breadcrumb['level2'] = array('name' => 'Pagos', 'link' => base_url().'dashboard/payments');
        $this->page_title = 'Mis Pagos';
        $this->page = 'payment';
    }

    public function index(){
        $data['user_info'] = $this->model_usuario->get_info_agent($this->user_id);
        $status = !empty($_GET['status']) ? $_GET['status'] : 'Pagado';
        $pub_id = !empty($_GET['uid']) ? $_GET['uid'] : "";
        $data['total'] = $this->model_pagos->get_num_payment_by_user($this->user_id, $status);

        $this->load->library('pagination');

        $from = ($this->uri->segment(4)) ? $this->uri->segment(4) : 1;
        $options['base_url'] = base_url(). 'dashboard/payments/p/';
        $options['first_url'] = base_url(). 'dashboard/payments/p/1';
        $options['per_page'] = 30;
        $options['uri_segment'] = 4;
        $options['first_link'] = '&laquo;';
        $options['last_link'] = '&raquo;';
        $options['next_link'] = '&rsaquo;';
        $options['prev_link'] = '&lsaquo;';
        $options['num_tag_open'] = '<li class="paginate_button">';
        $options['num_tag_close'] = '</li>';
        $options['cur_tag_open'] = '<li class="paginate_button active"><a href="javascript:;">';
        $options['cur_tag_close'] = '</a></li>';
        $options['next_link'] = '&gt;';
        $options['next_tag_open'] = '<li class="paginate_button next">';
        $options['next_tag_close'] = '</li>';
        $options['prev_link'] = '&lt;';
        $options['prev_tag_open'] = '<li class="paginate_button previous">';
        $options['prev_tag_close'] = '</li>';

        $options['total_rows'] = $data['total'];
        $choice = ceil($options["total_rows"] / $options["per_page"]);
        $options["num_links"] = $choice;
        //$options['suffix'] = '?' . http_build_query($_GET, '', "&");
        $options['use_page_numbers'] = TRUE;

        $this->pagination->initialize($options);
        $data['pagination'] = $this->pagination->create_links();
        $data['payments'] = $this->model_pagos->get_payment_by_user($this->user_id, $from, $options['per_page'], $status, $pub_id);
        $this->load_template_backend('backend/payment/list', $data);
    }

    public function detail(){
        $data['user_info'] = $this->model_usuario->get_info_agent($this->user_id);
        $pag_id = $this->uri->segment(4);
        $pag_id = $pag_id;
        $data['detail'] = $this->model_pagos->get_payment_detail($pag_id);
        $this->load_template_backend('backend/payment/detail', $data);
    }

    public function register() {
        $data['user_info'] = $this->model_usuario->get_info_agent($this->user_id);

	    if(!empty($_POST)) {

			if ($this->form_validate()) {
				$publicationId = $this->uri->segment(3);

				$paymentType = $_POST['paymentType'];
				/*$dateTimeZone = new DateTimeZone("America/La_Paz");
				$paymentDate = new DateTime("now", $dateTimeZone);
				$endPaymentDate = clone $paymentDate;
				$endPaymentDate = $endPaymentDate->modify('+2 days');*/

				$paymentDate = date("Y-m-d H:i:s");
				$endPaymentDate = strtotime ('+14 days', strtotime($paymentDate)) ;
				$endPaymentDate = date ('Y-m-d H:i:s', $endPaymentDate);

				$userName = "S/N";
				if(isset($_POST["userName"]) && $_POST["userName"] != ""){
					$userName = $_POST["userName"];
				}

				$userNit = 0;
				if(isset($_POST["userNit"]) && $_POST["userNit"] != ""){
					$userNit = $_POST["userNit"];
				}

				$totalPayment = $_POST['paymentTotal'];

				$data = array();
				$sw = true;
				while($sw){
					$pag_id = $this->codigoAlfanumerico(7);
					if(!$this->model_pagos->exist($pag_id)){
						$sw = false;
					}
				}
				$payment = array(
					"pag_id" => $pag_id,
					"pag_pub_id" => $publicationId,
					"pag_fecha" => $paymentDate,
					"pag_nombre" =>$userName,
					"pag_nit" => $userNit,
					"pag_monto" => $totalPayment,
					"pag_estado" => 'Pendiente',
                    'pag_concepto' => 'Mejora',
					"pag_entidad" => $paymentType,
					"pag_fecha_ven" => $endPaymentDate,
                );

				$paymentId = $this->model_pagos->insert($payment);
				$Descripcion = "";
				if(isset($_POST['principal']) && $_POST['principal'] != ""){
					$Descripcion .= "PRIN-".$_POST['principal']."DIAS, ";

					$pago_servicio = array(
						"pag_id" => $pag_id,
						"ser_id" => $_POST['principal']
                    );
					$this->model_pago_servicio->insert($pago_servicio);
				}
				if(isset($_POST['banner']) && $_POST['banner'] != ""){
					$Descripcion .= "BANN-".$_POST['banner']."DIAS, ";

					$pago_servicio = array(
						"pag_id" => $pag_id,
						"ser_id" => $_POST['banner']
                    );
					$this->model_pago_servicio->insert($pago_servicio);
				}
				if(isset($_POST['busqueda']) && $_POST['busqueda'] != ""){
					$Descripcion .= "BUSQ-".$_POST['busqueda']."DIAS";

					$pago_servicio = array(
						"pag_id" => $pag_id,
						"ser_id" => $_POST['busqueda']
                    );
					$this->model_pago_servicio->insert($pago_servicio);
				}

				if($_POST['paymentType'] === 'PAGOSNET') {
					$result = array();
					$pago_id = $pag_id;
					$MontoTotal = $totalPayment;
					$fecha_vencimiento = $endPaymentDate;
					$fecha = $paymentDate;
					$nombre_fact = $userName;
					$nit_fact = $userNit;
					include BASE_DIRECTORY . DS . 'sintesis' . DS . 'servicios' . DS . 'clientePagosNet.php';
                    $data['errorSintesis'] = $errorSintesis;
                    if($errorSintesis == "No"){
                        $paymentUp = array(
                            "pag_cod_transaccion" => $result["cmeRegistroPlanResult"]["idTransaccion"],
                        );
                        $this->model_pagos->update_transaccion_id($pago_id, $paymentUp);
                    }
				}elseif($_POST['paymentType'] === 'TIGOMONEY') {
                    $_POST['pag_id'] = $pag_id;
                } elseif($_POST['paymentType'] === 'OFICINA') {
				} elseif($_POST['paymentType'] === 'BANCO') {}

				$data['pagoId'] = $pag_id;
				$data['totalPayment'] = $totalPayment;
				$data['paymentType'] = $_POST['paymentType'];

                // Email notificacion de pagos al admin del sitio para seguimiento
                $aux_inm  = $this->model_publicacion->get_name_inmueble_by_pub_id($publicationId);
                $user_email = $this->model_usuario->get_user_by_publication($publicationId);
                $concepto = "#".$aux_inm->inm_id . " - ".$aux_inm->inm_nombre;
                $usuario = $user_email->nombre;
                $telefono = $user_email->usu_telefono . " - " . $user_email->usu_celular;
                $email = $user_email->usu_email;
                $monto = $totalPayment;
                $tipo = "Mejora de Anuncio";
                $forma = $_POST['paymentType'];
                $facturacion_name = $_POST["userName"];
                $facturacion_nit = $_POST["userNit"];
                $this->send_mail_pago_notificacion($concepto,$usuario,$telefono,$email,$pag_id, $monto,$tipo,$forma,$facturacion_name,$facturacion_nit);

                if($_POST['paymentType'] === 'TIGOMONEY') {
                    $this->tigomoney();
                }

				$this->load_template_backend('backend/payment/payment_confirmation', $data);
			}
		} else {

		    $data['mainServices'] = $this->model_servicios->get_services_by_type('Principal');
		    $data['bannerServices'] = $this->model_servicios->get_services_by_type('Landing');
		    $data['searchServices'] = $this->model_servicios->get_services_by_type('Busqueda');
            $data['particularServices'] = $this->model_servicios->get_services_by_type('Anuncios');
            $data['sw_subscription']= $this->sw_subscription;
            $user = $this->model_usuario->get_info_agent($this->user_id);
            $this->load_template_backend('backend/payment/register', $data);
        }
    }

    public function register_publication() {
        $data['user_info'] = $this->model_usuario->get_info_agent($this->user_id);
        $publicationId = $this->uri->segment(4);

        if(!empty($_POST)) {
            $this->form_validation->set_rules('anuncio', 'Anuncio', 'trim|required');

	        $this->form_validation->set_message('required', 'Seleccionar los dias para su anuncio');

            if ($this->form_validation->run()) {
                $paymentType   = NULL;
                $paymentDate    = date( "Y-m-d H:i:s" );
                $endPaymentDate = strtotime( '+14 days', strtotime( $paymentDate ) );
                $endPaymentDate = date( 'Y-m-d H:i:s', $endPaymentDate );
                $totalPayment = $_POST['paymentTotal'];
                $estadoPago = 'Pendiente';
                $fechaPagado = NULL;

                $anuncio = $_POST['anuncio'];
                if(empty($anuncio) || $anuncio == null){
                    $anuncio = 0;
                }

                if ($anuncio == 20) {
                    $publicationAux = $this->model_publicacion->get_created_publication($publicationId);
                    $values = array(
                        "pub_vig_fin" => date('Y-m-d H:i:s', strtotime('+30 days', strtotime($publicationAux->pub_vig_fin)))
                    );
                    $this->model_publicacion->update($publicationId, $values);
                }

                $paymentParams = array (
                    'paymentType' => $paymentType,
                    'paymentDate' => $paymentDate,
                    'endPaymentDate' => $endPaymentDate,
                    'totalPayment' => $totalPayment,
                    'services' => array($anuncio, $_POST['principal'], $_POST['banner'], $_POST['busqueda']),
                    'estado' => $estadoPago,
                    'concepto' => $_POST['concepto'],
                    'fechaPago' => $fechaPagado
                );

                $paymentType   = $paymentParams['paymentType'];
                $paymentDate    = $paymentParams['paymentDate'];
                $endPaymentDate = $paymentParams['endPaymentDate'];
                $totalPayment = $paymentParams['totalPayment'];
                $services = $paymentParams['services'];
                $estado = $paymentParams['estado'];
                $concepto = $paymentParams['concepto'];
                $fechaPago = $paymentParams['fechaPago'];

                $sw   = true;
                while ($sw) {
                    $pag_id = $this->codigoAlfanumerico(7);
                    if (!$this->model_pagos->exist($pag_id)) {
                        $sw = false;
                    }
                }

                $userName = "S/N";
                if ( isset( $_POST["userName"] ) && $_POST["userName"] != "" ) {
                    $userName = $_POST["userName"];
                }

                $userNit = 0;
                if ( isset( $_POST["userNit"] ) && $_POST["userNit"] != "" ) {
                    $userNit = $_POST["userNit"];
                }

                $payment = array(
                    "pag_id"        => $pag_id,
                    "pag_pub_id"    => $publicationId,
                    "pag_fecha"     => $paymentDate,
                    "pag_nombre"    => $userName,
                    "pag_nit"       => $userNit,
                    "pag_monto"     => $totalPayment,
                    "pag_estado"    => $estado,
                    "pag_entidad"   => $paymentType,
                    "pag_concepto"   => $concepto,
                    "pag_fecha_ven" => $endPaymentDate,
                    "pag_fecha_pagado" => $fechaPago
                );

                $paymentId = $this->model_pagos->insert( $payment );

                foreach($services as $serviceId) {
                    if($serviceId != 0) {
                        if (!$this->sw_subscription) {
                            $pago_servicio = array(
                                "pag_id" => $pag_id,
                                "ser_id" => $serviceId
                            );
                        }else{
                            $pago_servicio = array(
                                "pag_id" => $pag_id,
                                "ser_id" => $serviceId,
                                "fech_ini" => date('Y-m-d'),
                                "fech_fin" => date('Y-m-d', strtotime('+30 days'))
                            );
                        }
                        $this->model_pago_servicio->insert($pago_servicio);
                    }
                }

                $Descripcion = "PAGO DE ANUNCIO";
                $result = array();
                $data = array();
                $pago_id = $pag_id;
                $MontoTotal = $totalPayment;
                $fecha_vencimiento = $endPaymentDate;
                $fecha = $paymentDate;
                $nombre_fact = $userName;
                $nit_fact = $userNit;
                include BASE_DIRECTORY . DS . 'sintesis' . DS . 'servicios' . DS . 'clientePagosNet.php';

                $data['errorSintesis'] = $errorSintesis;
                if($errorSintesis == "No"){
                    $paymentUp = array(
                        "pag_cod_transaccion" => $result["cmeRegistroPlanResult"]["idTransaccion"],
                    );
                    $this->model_pagos->update_transaccion_id($pago_id, $paymentUp);
                }

                $data['pagoId']       = $pag_id;
                $data['totalPayment'] = $totalPayment;
                $data['paymentType']  = $_POST['paymentType'];

                //$this->load_template_backend('backend/payment/payment_confirmation', $data);

                //if (!$this->sw_subscription) {
                    // Email notificacion de pagos al admin del sitio para seguimiento
                    $aux_inm  = $this->model_publicacion->get_name_inmueble_by_pub_id($publicationId);
                    $user_email = $this->model_usuario->get_user_by_publication($publicationId);
                    $concepto = "#".$aux_inm->inm_id . " - ".$aux_inm->inm_nombre;
                    $usuario = $user_email->nombre;
                    $telefono = $user_email->usu_telefono . " - " . $user_email->usu_celular;
                    $email = $user_email->usu_email;
                    $monto = $totalPayment;
                    $tipo = "Anuncio Estandar";
                    $forma = $_POST['paymentType'];
                    $facturacion_name = $_POST["userName"];
                    $facturacion_nit = $_POST["userNit"];
                    $this->send_mail_pago_notificacion($concepto,$usuario,$telefono,$email,$pag_id,$monto,$tipo,$forma,$facturacion_name,$facturacion_nit);

                    $this->load_template_backend( 'backend/payment/payment_mandatory', $data);
                //}
            } else {
                $data['mainServices']       = $this->model_servicios->get_services_by_type('Principal');
                $data['bannerServices']     = $this->model_servicios->get_services_by_type('Landing');
                $data['searchServices']     = $this->model_servicios->get_services_by_type('Busqueda');
                $data['particularServices'] = $this->model_servicios->get_services_by_type('Anuncios');
                $data['sw_subscription']    = $this->sw_subscription;
                $data['concepto'] = $_POST['concepto'];
                $data['publicationId']      = $publicationId;

                $this->load_template_backend('backend/payment/payment_services_step1', $data);
            }
        }
    }

	public function form_validate() {

		$this->form_validation->set_rules('paymentType', 'Metodo de pago', 'trim|required');

		if(!$_POST['principal'] &&
            !$_POST['banner'] &&
            !$_POST['busqueda'] &&
            !$_POST['anuncio']) {

			$this->form_validation->set_rules('principal', 'Costo ', 'trim|required');
		}

		if($_POST['invoiced'] == 'on'){
			//$this->form_validation->set_rules('paymentType', 'Tipo de pago', 'trim|required|numeric');
		}

		//$this->form_validation->set_message('numeric', 'El Nit solo debe contener Numeros');
		$this->form_validation->set_message('required', 'Debe seleccionar al menos un Costo y metodo de pago');

		if($this->form_validation->run() == TRUE){
			return true;
		}else{
			return false;
		}
	}

    public function delete(){
        // Eliminar pago pendiente
        $pag_id = $this->uri->segment(4);
        $values = array(
            "pag_estado" => "Eliminado"
        );
        $this->model_pagos->update($pag_id, $values);
        redirect('dashboard/payments?status=Pendiente&action=delete');
    }

    public function subscription(){
        $data['user_info'] = $this->model_usuario->get_info_agent($this->user_id);
        $this->page = 'payment_subscription';
        if($_POST){

            $this->form_validation->set_rules('subscription', 'Suscripcion', 'trim|required');
	        $this->form_validation->set_rules('paymentType', 'paymentType', 'trim|required');
            $this->form_validation->set_message('required', 'Seleccionar una de las siguientes suscripciones y una forma de pago');

            if ($this->form_validation->run()) {

                $id_service = $this->input->post('subscription');
                $service = $this->model_servicios->get_id($id_service);
                $paymentType = $_POST['paymentType'];

                $paymentDate = date("Y-m-d H:i:s");
                $endPaymentDate = strtotime ('+14 days', strtotime($paymentDate)) ;
                $endPaymentDate = date ('Y-m-d H:i:s', $endPaymentDate);

                $userName = "S/N";
                if(isset($_POST["userName"]) && $_POST["userName"] != ""){
                    $userName = $_POST["userName"];
                }

                $userNit = 0;
                if(isset($_POST["userNit"]) && $_POST["userNit"] != ""){
                    $userNit = $_POST["userNit"];
                }

                //$totalPayment = $service->ser_precio;
                $totalPayment = $_POST["paymentTotal"];

                $data = array();
                $sw = true;
                while($sw){
                    $pag_id = $this->codigoAlfanumerico(7);
                    if(!$this->model_pagos->exist($pag_id)){
                        $sw = false;
                    }
                }
                $payment = array(
                    "pag_id" => $pag_id,
                    "pag_pub_id" => 0,
                    "pag_fecha" => $paymentDate,
                    "pag_nombre" =>$userName,
                    "pag_nit" => $userNit,
                    "pag_monto" => $totalPayment,
                    "pag_estado" => 'Pendiente',
                    "pag_entidad" => $paymentType,
                    "pag_concepto" => 'Suscripcion',
                    "pag_fecha_ven" => $endPaymentDate
                );
                $paymentId = $this->model_pagos->insert($payment);

                $pago_servicio = array(
                    "pag_id" => $pag_id,
                    "ser_id" => $id_service
                );
                $this->model_pago_servicio->insert($pago_servicio);


                if($_POST['paymentType'] === 'PAGOSNET') {

                    $Descripcion = "";
                    $Descripcion .= "SUSCRIPCION-".$service->ser_dias."DIAS";
                    $result = array();
                    $pago_id = $pag_id;
                    $MontoTotal = $totalPayment;
                    $fecha_vencimiento = $endPaymentDate;
                    $fecha = $paymentDate;
                    $nombre_fact = $userName;
                    $nit_fact = $userNit;
                    include BASE_DIRECTORY . DS . 'sintesis' . DS . 'servicios' . DS . 'clientePagosNet.php';
                    $data['errorSintesis'] = $errorSintesis;
                    if($errorSintesis == "No"){
                        $paymentUp = array(
                            "pag_cod_transaccion" => $result["cmeRegistroPlanResult"]["idTransaccion"],
                        );
                        $this->model_pagos->update_transaccion_id($pago_id, $paymentUp);
                    }

                }elseif($_POST['paymentType'] === 'TIGOMONEY') {
                    $_POST['pag_id'] = $pag_id;
                } elseif($_POST['paymentType'] === 'OFICINA') {
                } elseif($_POST['paymentType'] === 'BANCO') {}

                $suscription = array(
                    "usuario_id" => $this->user_id,
                    "fecha_inicio" => null,
                    "fecha_fin" => null,
                    "pago_id" => $pag_id,
                    "estado" => 'Inactivo'
                );

                $this->model_suscripcion->insert($suscription);

                $data['pagoId'] = $pag_id;
                $data['totalPayment'] = $totalPayment;
                $data['paymentType'] = $_POST['paymentType'];

                // Email notificacion de pagos al admin del sitio para seguimiento
                $concepto = "Suscripcion";
                $user_email = $this->model_usuario->get_user_by_id($this->user_id);
                $usuario = $user_email->usu_nombre . " " . $user_email->usu_apellido;
                $telefono = $user_email->usu_telefono . " - " . $user_email->usu_celular;
                $email = $user_email->usu_email;
                $monto = $totalPayment;
                $tipo = "Suscripcion a Paquete";
                $forma = $_POST['paymentType'];
                $facturacion_name = $_POST["userName"];
                $facturacion_nit = $_POST["userNit"];
                $this->send_mail_pago_notificacion($concepto,$usuario,$telefono,$email,$pag_id,$monto,$tipo,$forma,$facturacion_name,$facturacion_nit);

                if($_POST['paymentType'] === 'TIGOMONEY') {
                    $this->tigomoney();
                }

                $this->load_template_backend('backend/subscription/confirmation', $data);
            }else{
                $data['subscription_services'] = $this->model_servicios->get_services_by_type('Suscripcion');
                $this->load_template_backend('backend/subscription/register', $data);
            }
        }else{
            $data['subscription_services'] = $this->model_servicios->get_services_by_type('Suscripcion');
            $this->load_template_backend('backend/subscription/register', $data);
        }
    }

    // Esta funcion se utiliza cuando se verifica el pago de la suscripcion
    public function pay_publications_pending(){
        $payments = $this->model_pagos->get_payment_pending_subscription_by_user($this->user_id);
        if(count($payments) > 0){
            foreach($payments as $obj_pago){
                $values = array(
                    "pag_estado" => 'Pagado',
                    "pag_monto" => '0',
                    "pag_entidad" => 'Suscripcion',
                    "pag_fecha_pagado" => date('Y-m-d H:i:s')
                );
                $this->model_pagos->update($obj_pago->pag_id, $values);
                $value_pub = array(
                    "pub_estado" => 'Aprobado',
                    "pub_vig_fin" => date('Y-m-d', strtotime('+30 days'))
                );
                $this->model_publicacion->update($obj_pago->pag_pub_id, $value_pub);
            }
        }

    }


    public function send_mail_pago_notificacion($concepto, $usuario, $telefono, $email, $pag_id, $monto, $tipo, $forma,$facName,$facNit){
        require_once(BASE_DIRECTORY . DS . 'admin' . DS . 'clases' . DS. 'phpmailer' . DS . 'class.phpmailer.php');
        $parametros = $this->model_parametros->get_all();

        $template = base_url() . "assets/template_mail/mail_pago_notificacion_admin.html";
        $contenido = $this->obtener_contenido($template);
        $contenido = str_replace('@@concepto',$concepto,$contenido);
        $contenido = str_replace('@@usuario',$usuario,$contenido);
        $contenido = str_replace('@@telefono',$telefono,$contenido);
        $contenido = str_replace('@@email',$email,$contenido);
        $contenido = str_replace('@@pag_id',$pag_id,$contenido);
        $contenido = str_replace('@@monto',$monto,$contenido);
        $contenido = str_replace('@@tipo',$tipo,$contenido);
        $contenido = str_replace('@@forma',$forma,$contenido);
        $contenido = str_replace('@@fac_name',$facName,$contenido);
        $contenido = str_replace('@@fac_nit',$facNit,$contenido);

        $cue_nombre = $parametros->par_salida;
        $cue_pass = $parametros->par_pas_salida;

        $mail = new PHPMailer(true);
        $mail->SetLanguage('es');
        $mail->IsSMTP();
        $mail->SMTPAuth = true;
        $mail->Host = $parametros->par_smtp;
        $mail->Timeout=30;
        $mail->CharSet = 'utf-8';
        $mail->Subject = "Nuevo Pago #" . $pag_id;
        $body = $contenido;
        $mail->MsgHTML($body);
        $mail->AddReplyTo($email, $usuario);
        $mail->AddAddress("pagos@toqueeltimbre.com");
        //$mail->AddAddress("cristian.inarra@gmail.com");
        $mail->SetFrom($cue_nombre, $parametros->par_smtp);
        $mail->Username = $cue_nombre;
        $mail->Password = $cue_pass;

        $mail->Send();
    }

    private function obtener_contenido($url)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.3) Gecko/20070309 Firefox/2.0.0.3");
        curl_setopt($curl, CURLOPT_URL, $url);
        $contenido = curl_exec($curl);
        curl_close($curl);

        return $contenido;
    }

    public function tigomoney(){

        if($_POST){
            $order_pay = $this->model_pagos->get_payment_detail_tigomoney($_POST['pag_id']);
            $url_return = base_url() . "dashboard/payment/ipn";

            $pv_nroDocumento= "pv_nroDocumento=".$order_pay->pag_nit;
            $pv_orderId="pv_orderId=".$order_pay->pag_id;
            $monto = round($order_pay->pag_monto, 0);
            $pv_monto="pv_monto=".$monto;
            //$pv_monto="pv_monto=1";
            $pv_linea="pv_linea=".$_POST['linea'];
            $pv_nombre="pv_nombre=".$order_pay->pag_nombre;
            $pv_urlCorrecto = "pv_urlCorrecto=".$url_return;
            $pv_urlError = "pv_urlError=".$url_return;
            $pv_confirmacion = "pv_confirmacion=Toqueeltimbre";
            $pv_notificacion = "pv_notificacion=Orden de Pago ".$order_pay->pag_id;
            $pv_items= "pv_items=*i1|1|Servicios de Toqueeltimbre|".$order_pay->pag_monto."|".$order_pay->pag_monto;
            $pv_razonSocial= "pv_razonSocial=".$order_pay->nombre;
            $pv_nit= "pv_nit=".$order_pay->pag_nit;

            require BASE_DIRECTORY . DS . 'sintesis' . DS . 'tigomoney' . DS . 'CTripleDes.php';
            $montant = $pv_nroDocumento.";".$pv_orderId.";".$pv_monto.";".$pv_linea.";".$pv_nombre.";".$pv_urlCorrecto.";".$pv_urlError.";".$pv_confirmacion.";".$pv_notificacion.";".$pv_items.";".$pv_razonSocial.";".$pv_nit;

            $keyPublica = "2c522f105107418cc9d5a57632d676f66ee35a2b615e46c73c7cb293b0c7e2fb12725208ef1d2cfd9a360196ff41d095e170911460a56608df12d4514b8a538a";
            $keyPrivada = "7LJ4J18WS98FNVO7N01VFHX4";

            $tripleDes = new CTripleDes();
            $tripleDes->setMessage($montant);
            $tripleDes->setPrivateKey($keyPrivada);
            $encrypt = $tripleDes->encrypt();

            header("Location: http://190.129.208.178:96/vipagos/faces/payment.xhtml?key=".$keyPublica."&parametros=".$encrypt);
        }else{
            $this->load->view("backend/payment/iframe_tigomoney", array());
        }
    }


    public function ipn(){
        $data['user_info'] = $this->model_usuario->get_info_agent($this->user_id);

        $this->load->model('model_inmueble');
        $this->load->model('model_suscripcion');
        $values = $this->get_tigomoney_decrypt($_GET['r']);
        $sw_result = $values['codRes'];

        if($sw_result == 0){

            $id_pago = trim($values['orderId']);
            $obj_paid = $this->model_pagos->get_id_paid($id_pago);
            $sw_pago = $this->model_pagos->verify_id_paid($id_pago);

            if(!$sw_pago){
                $codigos_servicios = $this->model_pagos->get_services($id_pago);
                $email = $this->model_pagos->get_email_idpag($id_pago);
                $fecha_pagado = date("Y-m-d H:i:s");

                $pago_values = array(
                    "pag_estado" => 'Pagado',
                    'pag_entidad' => 'TigoMoney',
                    'pag_fecha_pagado' => $fecha_pagado
                );

                $this->model_pagos->update($id_pago, $pago_values);

                if($obj_paid->pag_concepto == "Suscripcion"){
                    $total = 0;
                    foreach($codigos_servicios as $row){
                        if($row->ser_id != ""){
                            $obj_servicio = $this->model_servicios->get_id($row->ser_id);
                            $fech_inicio = date("Y-m-d");
                            $fec_vencimi = date("Y-m-d", strtotime($fech_inicio. " + " . $obj_servicio->ser_dias . " days"));

                            $pag_service_values = array(
                                'fech_ini' => $fech_inicio,
                                'fech_fin' => $fec_vencimi
                            );

                            $this->model_pago_servicio->update($id_pago, $row->ser_id, $pag_service_values);
                        }
                    }
                    $obj_values = array(
                        'fecha_inicio' => $fech_inicio,
                        'fecha_fin' => $fec_vencimi,
                        'estado' => "Activo"
                    );
                    $this->model_suscripcion->update_by_pag_id($id_pago, $obj_values);
                    $this->pay_publications_pending_suscription($this->usu_id);

                    $total_pagado = $this->model_pagos->get_total_paid($id_pago);
                    $this->enviar_mail($total_pagado, $email, "Pago por suscripcion");
                }else{
                    $total = 0;
                    foreach($codigos_servicios as $row){
                        if($row->ser_id != ""){
                            $obj_servicio = $this->model_servicios->get_id($row->ser_id);
                            $id_pub = $this->model_pagos->get_id_publication($id_pago);

                            $fecha_fin_anterior_pago = $this->model_pagos->verify_exist_other_paids($id_pago, $id_pub, $obj_servicio->ser_id);
                            if(!empty($fecha_fin_anterior_pago)){
                                $fech_inicio = $fecha_fin_anterior_pago;
                            }else{
                                $fech_inicio = date("Y-m-d");
                            }

                            $fec_vencimi = date("Y-m-d", strtotime($fech_inicio. " + " . $obj_servicio->ser_dias . " days"));
                            $pag_service_values = array(
                                'fech_ini' => $fech_inicio,
                                'fech_fin' => $fec_vencimi
                            );
                            $this->model_pago_servicio->update($id_pago, $row->ser_id, $pag_service_values);

                            $total = $total + $obj_servicio->ser_precio;
                        }
                    }
                    $publicacion = $this->model_pagos->get_publication($id_pago);
                    $mayor_fecha = $this->model_pagos->get_max_date_service($publicacion->pub_id);
                    if($mayor_fecha > $publicacion->pub_vig_fin){
                        $pub_vig_fin = $mayor_fecha;
                        $pub_values = array(
                            'pub_estado' => 'Aprobado',
                            'pub_vig_fin' => $pub_vig_fin
                        );
                    }else{
                        $pub_values = array(
                            'pub_estado' => 'Aprobado'
                        );
                    }
                    $this->model_publicacion->update($publicacion->pub_id, $pub_values);
                    $obj_inmueble = $this->model_pagos->get_id_inmueble($publicacion->pub_id);
                    $this->model_inmueble->update($obj_inmueble->inm_id, array('inm_publicado' => 'Si'));

                    $total_pagado = $this->model_pagos->get_total_paid($id_pago);
                    $this->enviar_mail($total_pagado, $email, $obj_inmueble->inm_nombre);
                }

                $data['pagoId'] = $id_pago;
                $data['totalPayment'] = $total_pagado;
                $data['paymentType'] = "TIGOMONEY";
                $data['errorSintesis'] = "No";

                $this->load_template_backend('backend/payment/payment_confirmationtigo', $data);
            }else{
                $id_pago = trim($values['orderId']);
                $total_pagado = $this->model_pagos->get_total_paid($id_pago);

                $data['pagoId'] = $id_pago;
                $data['totalPayment'] = $total_pagado;
                $data['paymentType'] = "TIGOMONEY";
                $data['errorSintesis'] = "No";

                $this->load_template_backend('backend/payment/payment_confirmationtigo', $data);
            }
        }else{
			$data["result"] = $sw_result;
            $this->load_template_backend('backend/payment/payment_failed_tigomoney', $data);
        }

    }


    // Esta funcion se utiliza cuando se verifica el pago de la suscripcion
    public function pay_publications_pending_suscription($id_user){
        $payments = $this->model_pagos->get_payment_pending_subscription_by_user($id_user);

        if(count($payments) > 0){
            foreach($payments as $obj_pago){
                $id_pago = $obj_pago->pag_id;
                $codigos_servicios = $this->model_pagos->get_services($id_pago);
                $fecha_pagado = date("Y-m-d H:i:s");

                $pago_values = array(
                    "pag_estado" => 'Pagado',
                    'pag_monto' => '0',
                    'pag_entidad' => 'Suscripcion',
                    'pag_fecha_pagado' => $fecha_pagado
                );

                $this->model_pagos->update($id_pago, $pago_values);

                $total = 0;
                foreach($codigos_servicios as $row){
                    if($row->ser_id != ""){
                        $obj_servicio = $this->model_servicios->get_id($row->ser_id);
                        $id_pub = $this->model_pagos->get_id_publication($id_pago);

                        $fecha_fin_anterior_pago = $this->model_pagos->verify_exist_other_paids($id_pago, $id_pub, $obj_servicio->ser_id);
                        if(!empty($fecha_fin_anterior_pago)){
                            $fech_inicio = $fecha_fin_anterior_pago;
                        }else{
                            $fech_inicio = date("Y-m-d");
                        }

                        $fec_vencimi = date("Y-m-d", strtotime($fech_inicio. " + " . ($obj_servicio->ser_dias) . " days"));
                        $pag_service_values = array(
                            'fech_ini' => $fech_inicio,
                            'fech_fin' => $fec_vencimi
                        );
                        $this->model_pago_servicio->update($id_pago, $row->ser_id, $pag_service_values);

                        $total = $total + $obj_servicio->ser_precio;
                    }
                }
                $publicacion = $this->model_pagos->get_publication($id_pago);
                $mayor_fecha = $this->model_pagos->get_max_date_service($publicacion->pub_id);
                if($mayor_fecha > $publicacion->pub_vig_fin){
                    $pub_vig_fin = $mayor_fecha;
                    $pub_values = array(
                        'pub_estado' => 'Aprobado',
                        'pub_vig_fin' => $pub_vig_fin
                    );
                }else{
                    $pub_values = array(
                        'pub_estado' => 'Aprobado'
                    );
                }
                $this->model_publicacion->update($publicacion->pub_id, $pub_values);
                $obj_inmueble = $this->model_pagos->get_id_inmueble($publicacion->pub_id);
                $this->model_inmueble->update($obj_inmueble->inm_id, array('inm_publicado' => 'Si'));
            }
        }

    }


    function get_tigomoney_decrypt($value){
        require BASE_DIRECTORY . DS .'sintesis' . DS . 'tigomoney' . DS . 'CTripleDes.php';

        $keyPublica = "2c522f105107418cc9d5a57632d676f66ee35a2b615e46c73c7cb293b0c7e2fb12725208ef1d2cfd9a360196ff41d095e170911460a56608df12d4514b8a538a";
        $keyPrivada = "7LJ4J18WS98FNVO7N01VFHX4";

        $value = urldecode($value);
        $value = str_replace(" ", "+", $value);

        //desencriptacion
        $tripleDes2 = new CTripleDes();
        $tripleDes2->setMessage_to_decrypt($value);
        $tripleDes2->setPrivateKey($keyPrivada);
        $decrypt = $tripleDes2->decrypt();
        $decrypt = explode("&", $decrypt);

        foreach($decrypt as $value){
            $aux = explode("=", $value);
            $valores[$aux[0]] = $aux[1];
        }
        return $valores;
    }

    function enviar_mail($total,$email,$inmueble)	{
        require_once(BASE_DIRECTORY . DS . 'admin' . DS . 'clases' . DS. 'phpmailer' . DS . 'class.phpmailer.php');
        $parametros = $this->model_parametros->get_all();

        $template = base_url() . "assets/template_mail/confirmacion_pago.html";
        $contenido = $this->obtener_contenido($template);

        $contenido = str_replace('@@monto',$total,$contenido);
        $contenido = str_replace('@@inmueble',$inmueble,$contenido);

        $cue_nombre = $parametros->par_salida;
        $cue_pass = $parametros->par_pas_salida;

        $mail = new PHPMailer(true);
        $mail->SetLanguage('es');
        $mail->IsSMTP();
        $mail->SMTPAuth = true;
        $mail->Host = $parametros->par_smtp;
        $mail->Timeout=30;
        $mail->CharSet = 'utf-8';
        $mail->Subject = "Pago Confirmado";
        $body = $contenido;
        $mail->MsgHTML($body);
        $mail->AddAddress($email);
        //$mail->AddAddress("cristian.inarra@gmail.com");
        $mail->SetFrom($cue_nombre, $parametros->par_smtp);
        $mail->Username = $cue_nombre;
        $mail->Password = $cue_pass;

        $mail->Send();
    }

}