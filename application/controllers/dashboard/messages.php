<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Christian
 * Date: 03/12/2014
 * Time: 15:51
 */

class Messages extends Private_Controller{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('model_usuario');
        $this->load->model('model_parametros');
        $this->load->model('model_mensaje_proyecto');
        $this->load->model('model_real_state_message');
        $this->load->model('model_project_message');
        $this->breadcrumb['level2'] = array('name' => 'Mensajes', 'link' => base_url().'dashboard/messages');
        $this->page_title = 'Mis Mensajes';
        $this->page = 'inbox';
    }

    public function index()
    {
        $this->complementHandler->addViewComplement("tinymce");
        $this->complementHandler->addViewComplement("handlebars");
        $this->complementHandler->addPublicJs('handlerbars.custom.helpers');
        $this->complementHandler->addProjectJs("general.tinymce");
        $this->complementHandler->addProjectJs("message.index");

        $data['user_info'] = $this->model_usuario->get_info_agent($this->user_id);

        $num_projects = $this->model_proyecto->get_num_projects_by_user($this->user_id);
        if($num_projects > 0){
            $data['num_unread'] = $this->model_mensaje->get_num_messages_unread_project($this->user_id);
        }else{
            $data['num_unread'] = $this->model_mensaje->get_num_messages_unread($this->user_id);
        }
        $this->load_template_backend('backend/messages/list', $data);
    }

    public function ajax_inbox(){
        $search = $_POST['search'];
        $data['page'] = empty($_POST['page']) || $_POST['page'] == 0 ? 1 : $_POST['page'];
        $data['num_per_page'] = 50;

        $num_projects = $this->model_proyecto->get_num_projects_by_user($this->user_id);
        if($num_projects > 0){
            $data['num_inbox'] = $this->model_mensaje->get_num_messages_inbox_project($this->user_id, $search);
            $data['inbox'] = $this->model_mensaje->get_messages_projects_by_user($this->user_id, $data['page'], $data['num_per_page'], $search);
        }else{
            $data['num_inbox'] = $this->model_mensaje->get_num_messages_inbox($this->user_id, $search);
            $data['inbox'] = $this->model_mensaje->get_messages_by_user($this->user_id, $data['page'], $data['num_per_page'], $search);
        }
        $this->load->view('backend/messages/ajax_inbox', $data);
    }

    public function ajax_view(){
        $men_id = $_GET['message_id'];
        $messageType = $_GET['messageType'];
        $num_projects = $this->model_proyecto->get_num_projects_by_user($this->user_id);
        if($num_projects > 0){
            $this->model_mensaje_proyecto->update($men_id, array("men_leido" => "Si"));
            $data['message'] = $this->model_mensaje_proyecto->get_message_by_id($men_id);
        }
        elseif($messageType == "Mensaje inmobiliaria")
        {
            $realStateMessage = model_real_state_message::getById($men_id);
            $realStateMessage->setRead("Si");
            $realStateMessage->save();
            $data['message'] = (object)$realStateMessage->toArray();
        }
        elseif($messageType == "Mensaje_Proyecto")
        {
            $projectMessage = model_project_message::getById($men_id);
            $projectMessage->setRead("Si");
            $projectMessage->save();
            $data['message'] = (object)$projectMessage->toArray();
        }
        else{
            $this->model_mensaje->update($men_id, array("men_leido" => "Si"));
            $data['message'] = $this->model_mensaje->get_message_by_id($men_id);
        }
        $data['user_info'] = $this->model_usuario->get_info_agent($this->user_id);
        $data['messageType'] = $messageType;
        $this->load->view('backend/messages/ajax_inbox_view', $data);
    }

    public function ajax_trash(){
        $data['page'] = empty($_POST['page']) || $_POST['page'] == 0 ? 1 : $_POST['page'];
        $data['num_per_page'] = 50;
        $num_projects = $this->model_proyecto->get_num_projects_by_user($this->user_id);
        if($num_projects > 0){
            $data['num_inbox'] = $this->model_mensaje->get_num_messages_trash_project($this->user_id, $search);
            $data['inbox'] = $this->model_mensaje->get_messages_projects_trash_by_user($this->user_id, $data['page'], $data['num_per_page']);
        }else{
            $data['num_inbox'] = $this->model_mensaje->get_num_messages_trash($this->user_id, $search);
            $data['inbox'] = $this->model_mensaje->get_messages_trash_by_user($this->user_id, $data['page'], $data['num_per_page']);
        }
        $this->load->view('backend/messages/ajax_inbox', $data);
    }

    public function detail(){
        $data['user_info'] = $this->model_usuario->get_info_agent($this->user_id);
        $data['num_unread'] = $this->model_mensaje->get_num_messages_unread($this->user_id);

        $men_id = $this->uri->segment(3);
        $type_message = $this->input->get('t');
        if($type_message == "project"){
            $this->model_mensaje_proyecto->update($men_id, array("men_leido" => "Si"));
            $data['message'] = $this->model_mensaje_proyecto->get_message_by_id($men_id);
            $this->load_template_backend('backend/messages/detail', $data);
        }else{
            $this->model_mensaje->update($men_id, array("men_leido" => "Si"));
            $data['message'] = $this->model_mensaje->get_message_by_id($men_id);
            $this->load_template_backend('backend/messages/detail', $data);
        }
    }

    public function actions(){
        $ids = $_POST['messages'];
        $action = $_POST['action'];

        $num_projects = $this->model_proyecto->get_num_projects_by_user($this->user_id);
        if($num_projects > 0){
            if(!empty($ids)){
                if($action == 'delete'){
                    foreach($ids as $id){
                        $value = array(
                            'men_estado' => 'Novisible'
                        );
                        $this->model_mensaje_proyecto->update($id, $value);
                    }
                }

                if($action == 'change'){
                    foreach($ids as $id){
                        $value = array(
                            'men_leido' => 'Si'
                        );
                        $this->model_mensaje_proyecto->update($id, $value);
                    }
                }
                redirect('dashboard/messages');
            }
        }else{
            if(!empty($ids)){
                if($action == 'delete'){
                    foreach($ids as $id){
                        $value = array(
                            'men_estado' => 'Novisible'
                        );
                        $this->model_mensaje->update($id, $value);
                    }
                }

                if($action == 'change'){
                    foreach($ids as $id){
                        $value = array(
                            'men_leido' => 'Si'
                        );
                        $this->model_mensaje->update($id, $value);
                    }
                }
                redirect('dashboard/messages');
            }
        }
    }
}