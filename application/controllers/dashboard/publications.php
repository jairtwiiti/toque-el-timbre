<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Christian
 * Date: 04/12/2014
 * Time: 15:01
 */

class Publications extends Private_Controller{

    public $visible_publications;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_usuario');
        $this->load->model('model_publicacion');
        $this->load->model('model_categoria');
        $this->load->model('model_departamento');
        $this->load->model('model_ciudad');
        $this->load->model('model_zona');
        $this->load->model('model_forma');
        $this->load->model('model_moneda');
        $this->load->model('model_inmueble');
        $this->load->model('model_caracteristica');
        $this->load->model('model_inmueble_caracteristica');
        $this->load->model('model_inmueble_foto');
        $this->load->model('model_pagos');
        $this->load->model('model_payment');
        $this->load->model('model_pago_servicio');
        $this->load->model('model_servicios');
        $this->load->model('model_survey');
        $this->load->model('model_service');

        $this->load->library('excel');

        $this->breadcrumb['level2'] = array('name' => 'Publicaciones', 'link' => base_url().'dashboard/publications');
        $this->page_title = 'Mis Anuncios';
        $this->page = 'publication';
        $this->visible_publications = $this->model_publicacion->get_publications_visible_in_frontend($this->user_id);
    }

    public function index()
    {
	    $search = $_POST['searchPublication'];
        $data['total'] = $this->model_publicacion->get_num_publications_by_user($this->user_id, $search);

        $this->load->library('pagination');
        $this->complementHandler->addViewComplement("parsley");
        $this->complementHandler->addViewComplement("parsley.spanish");
        $this->complementHandler->addViewComplement("bootstrap-wizard");
        $this->complementHandler->addViewComplement("form-wizard");
        $this->complementHandler->addViewComplement("sweet-alerts");
        $this->complementHandler->addViewComplement('pricing');
        $this->complementHandler->addViewComplement('components');
        $this->complementHandler->addProjectCss('PaymentHandler', TRUE);
        $this->complementHandler->addProjectJs('PaymentHandler', TRUE);
        $this->complementHandler->addProjectCss('publications.index', TRUE);
        $this->complementHandler->addProjectJs('publications.index', TRUE);

        $from = ($this->uri->segment(4)) ? $this->uri->segment(4) : 1;
        $options['base_url'] = base_url(). 'dashboard/publications/p/';
        $options['first_url'] = base_url(). 'dashboard/publications/p/1';
        $options['per_page'] = 150;
        $options['uri_segment'] = 4;
        $options['first_link'] = '&laquo;';
        $options['last_link'] = '&raquo;';
        $options['next_link'] = '&rsaquo;';
        $options['prev_link'] = '&lsaquo;';
        $options['num_tag_open'] = '<li class="paginate_button">';
        $options['num_tag_close'] = '</li>';
        $options['cur_tag_open'] = '<li class="paginate_button active"><a href="javascript:;">';
        $options['cur_tag_close'] = '</a></li>';
        $options['next_link'] = '&gt;';
        $options['next_tag_open'] = '<li class="paginate_button next">';
        $options['next_tag_close'] = '</li>';
        $options['prev_link'] = '&lt;';
        $options['prev_tag_open'] = '<li class="paginate_button previous">';
        $options['prev_tag_close'] = '</li>';

        $options['total_rows'] = $data['total'];
        $choice = ceil($options["total_rows"] / $options["per_page"]);
        $options["num_links"] = $choice;
        //$options['suffix'] = '?' . http_build_query($_GET, '', "&");
        $options['use_page_numbers'] = TRUE;

        $this->pagination->initialize($options);
        $data['pagination'] = $this->pagination->create_links();
        $publications = $this->model_publicacion->get_publications_by_user($this->user_id, $from, $options['per_page'], $search);
        foreach($publications as $publication)
        {
            $publication->isPaymentPublication = $this->model_publicacion->is_payment_publication($publication->pub_id);
            $publication->hasPaymentImprove = $this->model_publicacion->has_payment_improve($publication->pub_id);
            $publication->hasPaymentPending = $this->model_publicacion->has_payment_pending($publication->pub_id);
            $publication->existPaymentPublication = $this->model_publicacion->exist_publication_payment($publication->pub_id);
            $publication->hasPaymentPendingRenovation = $this->model_publicacion->has_payment_pending_renovation($publication->pub_id);
            $publication->imageInmueble = $this->model_publicacion->get_first_image_publication($publication->inm_id);
        }
        $data['publications'] = $publications;
        $data['sw_subscription']= $this->sw_subscription;
        $data['limit_publications']= $this->limit_publications;

        $this->load_template_backend('backend/publications/list', $data);
    }

    public function update()
    {exit('not available');
        $inm_id = $this->uri->segment(4);
        $pub_id = $this->uri->segment(5);
        /** complements **/
        $this->complementHandler->addViewComplement("jquery.select2");
        $this->complementHandler->addViewComplement("jquery.select2.bootstrap");
        $this->complementHandler->addViewComplement("bootstrap.datepicker");
        $this->complementHandler->addViewComplement("parsley");
        $this->complementHandler->addViewComplement("parsley.spanish");
        $this->complementHandler->addViewComplement("tinymce");
        $this->complementHandler->addViewComplement("google.maps.api");
        $this->complementHandler->addViewComplement("gmaps");
        $this->complementHandler->addProjectJs("general.tinymce");
        $this->complementHandler->addProjectJs("PropertyFeatureHandler");
        $this->complementHandler->addProjectJs('property.edit');
        $this->complementHandler->addProjectCss('property.edit');
        $this->complementHandler->addProjectJs("initializeSelect2");
        if($this->form_validate("update")) {
            $images = $this->input->post('file_upload');
            $planos = $this->input->post('upload_planos');
            $status = $this->input->post('status');
            //var_dump($status, $status == "Si" && $this->sw_subscription);exit;
            if($status == "Si" && $this->sw_subscription){
                $num_paid = $this->model_publicacion->has_paid_publication($pub_id);

                if($num_paid == 0){
                    if( ($this->visible_publications + 1) > $this->limit_publications ){
                        $status = "No";
                        $error_limit = true;
                    }
                }
                if($num_paid > 0) {
                    //Si desactiva su anuncio y tiene mejora pagada, entonces se elimina la mejora y se alerta al usuario de esa accion
                    //$this->model_pagos->desactivate_mejora_publication($pub_id);
                    $sw_visible = $this->model_publicacion->ya_esta_publicado($pub_id);

                    // anuncios por suscripcion
                    if($sw_visible == 0){
                        $sw_visible_normal = $this->model_publicacion->ya_esta_publicado_normal($pub_id);

                        if($sw_visible_normal > 0){

                        }else{
                            if( ($this->visible_publications + 1) > $this->limit_publications ){
                                $status = "No";
                                $error_limit = true;
                            }
                        }
                    }
                }

            }

            $propertyData = array(
                'inm_nombre' => $this->input->post('title'),
                'inm_direccion' => $this->input->post('address'),
                'inm_precio' => $this->input->post('price'),
                'inm_superficie' => $this->input->post('area'),
                'inm_tipo_superficie' => $this->input->post('type_area'),
                'inm_detalle' => $this->input->post('description'),
                'inm_ciu_id' => $this->input->post('city'),
                'inm_latitud' => $this->input->post('latitude'),
                'inm_longitud' => $this->input->post('longitude'),
                'inm_cat_id' => $this->input->post('category'),
                'inm_zon_id' => $this->input->post('zone'),
                'inm_uv' => $this->input->post('uv'),
                'inm_manzano' => $this->input->post('manzano'),
                'inm_for_id' => $this->input->post('type'),
                'inm_tipo_superficie' => $this->input->post('type_area'),
                'inm_mon_id' => $this->input->post('currency')
                //'inm_publicado' => $status
            );
            $this->model_inmueble->update($inm_id, $propertyData);
            $propertyData["inm_id"] = $inm_id;
            $array_image = array();

            if(!empty($images))
            {
                foreach ($images as $img) {
                    $order = $this->model_inmueble_foto->get_order_max_foto();
                    $order = $order + 1;
                    $array_image[] = array(
                        'fot_archivo' => $img,
                        'fot_inm_id' => $inm_id,
                        'fot_order' => $order
                    );
                }
                model_publicacion::insertImages($array_image);
            }


            $images_feature = $this->input->post('images');
            if(!empty($images_feature)){
                foreach($images_feature['description'] as $key=>$value){
                    $aux_data = array(
                        'fot_descripcion' => $value
                    );
                    $this->model_inmueble_foto->update($key, $aux_data);
                }
                foreach($images_feature['sort_order'] as $key=>$value){
                    $aux_data = array(
                        'fot_order' => $value
                    );
                    $this->model_inmueble_foto->update($key, $aux_data);
                }
            }

            $sw_caracteristica = $this->input->post('sw_caracteristica');
            if(isset($sw_caracteristica) && $sw_caracteristica == 'true'){
                $features = $this->input->post('features');
                foreach($features as $key=>$feature){
                    $this->model_inmueble_caracteristica->update_features($inm_id,$key, array('eca_valor' => $feature));
                }
            }else{
                $this->model_inmueble_caracteristica->delete_features($inm_id);
                $this->insert_features($this->input->post('category'), $inm_id);
            }
            if(count($array_image)>0)
            {
                $propertyData = (array)model_inmueble::getById($inm_id);
                $publicationData = (array)model_publicacion::getById($pub_id);

                model_inmueble::emailNewImageNotification($publicationData, $propertyData, $array_image);
            }
            if($error_limit && $this->sw_subscription){
                //redirect('dashboard/publications?action=update&error=limit');
                redirect('dashboard/publications/pay/'.$pub_id);
            }else{
                redirect('dashboard/publications?action=update');
            }

        }

        $data['user_info'] = $this->model_usuario->get_info_agent($this->user_id);
        $data['inmueble'] = $this->model_inmueble->get_inmueble_by_id($inm_id);
        $data['categories'] = $this->model_categoria->get_all();
        $data['states'] = $this->model_departamento->get_all();
        $data['cities'] = $this->model_ciudad->get_cities_by_state($data['inmueble']->dep_id);
        $data['zones'] = $this->model_zona->get_zone_by_state($data['inmueble']->dep_id);
        $data['types'] = $this->model_forma->get_all();
        $data['currency'] = $this->model_moneda->get_all();
        $data['inmueble_fotos'] = $this->model_inmueble_foto->get_fotos($inm_id);
        $data['inmueble_planos'] = $this->model_inmueble_foto->get_planos($inm_id);
        $data['no_tien_pagos'] = $this->model_publicacion->no_tiene_pagos($pub_id);

        $data['features'] = $this->model_caracteristica->get_all_features($data['inmueble']->inm_cat_id);
        $data['feature_values'] = $this->model_inmueble_caracteristica->get_features_by_inmueble($inm_id);

        $this->load_template_backend('backend/publications/edit', $data);
    }

    public function register()
    {exit('not available');
        $data['user_info'] = $this->model_usuario->get_info_agent($this->user_id);
        $data['categories'] = $this->model_categoria->get_all();
        $data['states'] = $this->model_departamento->get_all();
        $data['types'] = $this->model_forma->get_all();
        $data['currency'] = $this->model_moneda->get_all();

        if($this->form_validate())
        {
            $url_seo = seo_url($this->input->post('title'));
            $url_seo = $this->exist_url_seo($url_seo);
            $images = $this->input->post('file_upload');
            $planos = $this->input->post('upload_planos');
            $status = $this->input->post('status');

            if($status == "Si"){
                if( ($this->visible_publications + 1) > $this->limit_publications ){
                    /*$status = "No";
                    $error_limit = true;*/
                    $status = "No";
                    $this->sw_subscription = false;
                }
            }

            $propertyData = array(
                'inm_nombre' => $this->input->post('title'),
                'inm_direccion' => $this->input->post('address'),
                'inm_precio' => $this->input->post('price'),
                'inm_superficie' => $this->input->post('area'),
                'inm_tipo_superficie' => $this->input->post('type_area'),
                'inm_detalle' => $this->input->post('description'),
                'inm_ciu_id' => $this->input->post('city'),
                'inm_orden' => 9999,
                'inm_latitud' => $this->input->post('latitude'),
                'inm_longitud' => $this->input->post('longitude'),
                'inm_cat_id' => $this->input->post('category'),
                'inm_zon_id' => $this->input->post('zone'),
                'inm_uv' => $this->input->post('uv'),
                'inm_manzano' => $this->input->post('manzano'),
                'inm_for_id' => $this->input->post('type'),
                'inm_tipo_superficie' => $this->input->post('type_area'),
                'inm_mon_id' => $this->input->post('currency'),
                'inm_visitas' => 0,
                'inm_publicado' => $status,
                'inm_seo' => $url_seo
            );

            $id_inm = $this->model_inmueble->insert($propertyData);
            $propertyData["imn_id"] = $propertyData;
            $this->insert_features($this->input->post('category'), $id_inm);
            $array_image = array();
            if(!empty($images)) {
                foreach ($images as $img) {
                    $array_image[] = array(
                        'fot_archivo' => $img,
                        'fot_inm_id' => $id_inm
                    );
                }
                model_publicacion::insertImages($array_image);

            }

            $desde = date('Y-m-d');
            $nuevafecha = strtotime ('+90 day', strtotime($desde));
            $hasta = date('Y-m-d', $nuevafecha);

            $publicationData = array(
                'pub_creado' => date('Y-m-d H:i:s'),
                'pub_vig_ini' => $desde,
                'pub_vig_fin' => $hasta,
                'pub_inm_id' => $id_inm,
                //'pub_estado' => $this->sw_subscription ? 'Aprobado' : 'No Aprobado',
                'pub_estado' => 'Aprobado',
                'pub_usu_id' => $this->user_id
            );
            $publicationId = $this->model_publicacion->insert($publicationData);
            $publicationData["pub_id"] = $publicationId;
            model_inmueble::emailNewImageNotification($publicationData, $propertyData, $array_image);
            //debemos colocar un estado al anuncion para ver si  ya esta creado

            $user = $data['user_info'];

            if(!$this->sw_subscription)
            {
                /*$paymentType   = NULL;
                $paymentDate    = date( "Y-m-d H:i:s" );
                $endPaymentDate = strtotime( '+30 days', strtotime( $paymentDate ) );
                $endPaymentDate = date( 'Y-m-d H:i:s', $endPaymentDate );
                $totalPayment = 30; // 30Bs
                $serviceId = 15;
                $estadoPago = 'Pendiente';
                $fechaPagado = NULL;

                $paymentData = array (
                    'paymentType' => $paymentType,
                    'paymentDate' => $paymentDate,
                    'endPaymentDate' => $endPaymentDate,
                    'totalPayment' => $totalPayment,
                    'service' => $serviceId,
                    'estado' => $estadoPago,
                    'concepto' => 'Anuncio',
                    'fechaPago' => $fechaPagado
                );

                $this->insert_payment($publicationId, $paymentData);*/
                $data['particularServices'] = $this->model_servicios->get_services_by_type('Anuncios');
                $data['mainServices']       = $this->model_servicios->get_services_by_type('Principal');
                $data['bannerServices']     = $this->model_servicios->get_services_by_type('Landing');
                $data['searchServices']     = $this->model_servicios->get_services_by_type('Busqueda');
                $data['similarServices']     = $this->model_servicios->get_services_by_type('Similares');
                $data['sw_subscription']    = $this->sw_subscription;
                $data['concepto'] = "Anuncio";
                $data['publicationId']      = $publicationId;

                $this->load_template_backend('backend/payment/payment_services_step1', $data);
            }
            else
            {
                $paymentType   = 'Suscripcion';
                $paymentDate    = date( "Y-m-d H:i:s" );
                $endPaymentDate = strtotime( '+30 days', strtotime( $paymentDate ) );
                $endPaymentDate = date( 'Y-m-d H:i:s', $endPaymentDate );
                $totalPayment = 0; // 30Bs
                $serviceId = 15;
                $estadoPago = 'Pagado';
                $fechaPagado = date( "Y-m-d H:i:s" );

                $paymentData = array (
                    'paymentType' => $paymentType,
                    'paymentDate' => $paymentDate,
                    'endPaymentDate' => $endPaymentDate,
                    'totalPayment' => $totalPayment,
                    'service' => $serviceId,
                    'estado' => $estadoPago,
                    'concepto' => 'Anuncio',
                    'fechaPago' => $fechaPagado
                );

                $this->insert_payment($publicationId, $paymentData);

                if($error_limit)
                {
                    redirect('dashboard/publications?action=register&error=limit');
                }
                else
                {
                    redirect('dashboard/publications?action=register');
                }
            }
        }
        else
        {
            $cat_id = $this->input->post('category');
            $data['features'] = $this->features($cat_id);
            $this->load_template_backend('backend/publications/register', $data);
        }
    }

    private function features($cat_id){
        if(!empty($cat_id)){
            $data['features'] = $this->model_caracteristica->get_all_features($cat_id);
            return $this->load->view('backend/publications/ajax_features', $data, TRUE);
        }else{
            return null;
        }
    }

    public function form_validate($action = "register") {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('title', 'Titulo', 'trim|required|max_length[150]');
        $this->form_validation->set_rules('description', 'Descripcion', 'trim|required|max_length[2000]');
        $this->form_validation->set_rules('type', 'Tipo', 'required');
        $this->form_validation->set_rules('area', 'Metros cuadrados', 'required');
        $this->form_validation->set_rules('price', 'Precio', 'required');
        $this->form_validation->set_rules('category', 'Categoria', 'required');
        $this->form_validation->set_rules('state', 'Departamento', 'required');
        $this->form_validation->set_rules('city', 'Ciudad', 'required');
        $this->form_validation->set_rules('zone', 'Zona', 'required');

        if($action == "register"){
            $this->form_validation->set_rules('file_upload', 'Imagen', 'required');
        }

        $this->form_validation->set_message('required', 'El campo %s es requerido.');
        if(strlen($this->input->post('title')) > 150){
            $this->form_validation->set_message('max_length', 'El titulo solo puede tener una longitud maxima de 150 caracteres.');
        }elseif(strlen($this->input->post('description')) > 2000){
            $this->form_validation->set_message('max_length', 'La descripcion solo puede tener una longitud maxima de 2000 caracteres.');
        }

        if($this->form_validation->run() == TRUE){
            return true;
        }else{
            return false;
        }
    }

    public function pay($concepto = 'Anuncio'){
        $this->complementHandler->addProjectJs('property.pay');
        $publicationId = $this->uri->segment(4);
        $url_concepto = $this->uri->segment(3);

        if($url_concepto == "pay"){
            $concepto = "Anuncio";
        }elseif($url_concepto == "renew"){
            $concepto = "Renovacion";
        }

        if( ($this->visible_publications + 1) > $this->limit_publications ){
            $variable_sw = $this->sw_subscription;
            $this->sw_subscription = false;
            //redirect('dashboard/publications?error=limit');
        }

        if(!$this->sw_subscription) {

            /*$paymentType   = NULL;
            $paymentDate    = date( "Y-m-d H:i:s" );
            $endPaymentDate = strtotime( '+3 days', strtotime( $paymentDate ) );
            $endPaymentDate = date( 'Y-m-d H:i:s', $endPaymentDate );
            $totalPayment = 30; // 30Bs
            $serviceId = 15;
            $estadoPago = 'Pendiente';
            $fechaPagado = NULL;

            $paymentData = array (
                'paymentType' => $paymentType,
                'paymentDate' => $paymentDate,
                'endPaymentDate' => $endPaymentDate,
                'totalPayment' => $totalPayment,
                'service' => $serviceId,
                'estado' => $estadoPago,
                'concepto' => $concepto,
                'fechaPago' => $fechaPagado
            );

            $pag_id = $this->insert_repay($publicationId, $paymentData);
            redirect('dashboard/publications/paymentinfo/'.$publicationId);*/

            $data['mainServices']       = $this->model_servicios->get_services_by_type('Principal');
            $data['bannerServices']     = $this->model_servicios->get_services_by_type('Landing');
            $data['searchServices']     = $this->model_servicios->get_services_by_type('Busqueda');
            $data['particularServices'] = $this->model_servicios->get_services_by_type('Anuncios');
            $data['similarServices']    = $this->model_servicios->get_services_by_type('Similares');
            $data['sw_subscription']    = $variable_sw;
            $data['concepto'] = $concepto;
            $data['publicationId']      = $publicationId;

            $this->load_template_backend('backend/payment/payment_services_step1', $data);
        } else {
            
            $values = array(
                "pub_estado" => 'Aprobado',
                "pub_vig_fin" => date('Y-m-d H:i:s', strtotime('+90 days'))
            );
            $this->model_publicacion->update($publicationId, $values);

            $inmuebleId = $this->model_publicacion->get_inmueble_by_pub_id($publicationId);
            $values = array(
                "inm_publicado" => 'Si',
            );
            $this->model_inmueble->update($inmuebleId, $values);

            $paymentType = 'Suscripcion';
            $paymentDate = date("Y-m-d H:i:s");
            $endPaymentDate = strtotime('+3 days', strtotime($paymentDate));
            $endPaymentDate = date('Y-m-d H:i:s', $endPaymentDate);
            $totalPayment = 0; // 30Bs
            $serviceId = 15;
            $estadoPago = 'Pagado';
            $fechaPagado = date("Y-m-d H:i:s");

            $paymentData = array(
                'paymentType' => $paymentType,
                'paymentDate' => $paymentDate,
                'endPaymentDate' => $endPaymentDate,
                'totalPayment' => $totalPayment,
                'service' => $serviceId,
                'estado' => $estadoPago,
                'concepto' => $concepto,
                'fechaPago' => $fechaPagado
            );

            $pag_id = $this->insert_repay($publicationId, $paymentData);
            redirect('dashboard/publications?action=register');
        }
    }

    private function insert_payment($publicationId, $paymentParams = array()) {
        if ($_POST) {

            $paymentType   = $paymentParams['paymentType'];
            $paymentDate    = $paymentParams['paymentDate'];
            $endPaymentDate = $paymentParams['endPaymentDate'];
            $totalPayment = $paymentParams['totalPayment'];
            $serviceId = $paymentParams['service'];
            $estado = $paymentParams['estado'];
            $concepto = $paymentParams['concepto'];
            $fechaPago = $paymentParams['fechaPago'];

            $sw   = true;
            while ($sw) {
                $pag_id = $this->codigoAlfanumerico(7);
                if (!$this->model_pagos->exist($pag_id)) {
                    $sw = false;
                }
            }

            $userName = "S/N";
            if ( isset( $_POST["userName"] ) && $_POST["userName"] != "" ) {
                $userName = $_POST["userName"];
            }

            $userNit = 0;
            if ( isset( $_POST["userNit"] ) && $_POST["userNit"] != "" ) {
                $userNit = $_POST["userNit"];
            }

            $payment = array(
                "pag_id"        => $pag_id,
                "pag_pub_id"    => $publicationId,
                "pag_fecha"     => $paymentDate,
                "pag_nombre"    => $userName,
                "pag_nit"       => $userNit,
                "pag_monto"     => $totalPayment,
                "pag_estado"    => $estado,
                "pag_entidad"   => $paymentType,
                "pag_concepto"   => $concepto,
                "pag_fecha_ven" => $endPaymentDate,
                "pag_fecha_pagado" => $fechaPago
            );

            $paymentId = $this->model_pagos->insert( $payment );

            $Descripcion = "PAGO ANUNCIO ESTANDAR";
            $result = array();
            $pago_id = $pag_id;
            $MontoTotal = $totalPayment;
            $fecha_vencimiento = $endPaymentDate;
            $fecha = $paymentDate;
            $nombre_fact = $userName;
            $nit_fact = $userNit;
            include BASE_DIRECTORY . DS . 'sintesis' . DS . 'servicios' . DS . 'clientePagosNet.php';
            $paymentUp = array(
                "pag_cod_transaccion" => $result["cmeRegistroPlanResult"]["idTransaccion"],
            );
            $this->model_pagos->update_transaccion_id($pago_id, $paymentUp);


            if (!$this->sw_subscription) {
                $pago_servicio = array(
                    "pag_id" => $pag_id,
                    "ser_id" => $serviceId
                );
            }else{
                $pago_servicio = array(
                    "pag_id" => $pag_id,
                    "ser_id" => $serviceId,
                    "fech_ini" => date('Y-m-d'),
                    "fech_fin" => date('Y-m-d', strtotime('+30 days'))
                );
            }

            $this->model_pago_servicio->insert($pago_servicio);

            $data = array();
            $data['pagoId']       = $pag_id;
            $data['totalPayment'] = $totalPayment;
            $data['paymentType']  = $_POST['paymentType'];

            if (!$this->sw_subscription) {
                $this->load_template_backend( 'backend/payment/payment_mandatory', $data );
            }
        }
    }

    private function insert_repay($publicationId, $paymentParams = array()) {
        $paymentType   = $paymentParams['paymentType'];
        $paymentDate    = $paymentParams['paymentDate'];
        $endPaymentDate = $paymentParams['endPaymentDate'];
        $totalPayment = $paymentParams['totalPayment'];
        $serviceId = $paymentParams['service'];
        $estado = $paymentParams['estado'];
        $concepto = $paymentParams['concepto'];
        $fechaPago = $paymentParams['fechaPago'];

        $sw   = true;
        while ($sw) {
            $pag_id = $this->codigoAlfanumerico(7);
            if (!$this->model_pagos->exist($pag_id)) {
                $sw = false;
            }
        }

        $userName = "S/N";
        $userNit = 0;

        $payment = array(
            "pag_id"        => $pag_id,
            "pag_pub_id"    => $publicationId,
            "pag_fecha"     => $paymentDate,
            "pag_nombre"    => $userName,
            "pag_nit"       => $userNit,
            "pag_monto"     => $totalPayment,
            "pag_estado"    => $estado,
            "pag_entidad"   => $paymentType,
            "pag_concepto"   => $concepto,
            "pag_fecha_ven" => $endPaymentDate,
            "pag_fecha_pagado" => $fechaPago
        );

        $paymentId = $this->model_pagos->insert( $payment );

        $Descripcion = "PAGO ANUNCIO ESTANDAR";
        $result = array();
        $pago_id = $pag_id;
        $MontoTotal = $totalPayment;
        $fecha_vencimiento = $endPaymentDate;
        $fecha = $paymentDate;
        $nombre_fact = $userName;
        $nit_fact = $userNit;
        include BASE_DIRECTORY . DS . 'sintesis' . DS . 'servicios' . DS . 'clientePagosNet.php';
        $paymentUp = array(
            "pag_cod_transaccion" => $result["cmeRegistroPlanResult"]["idTransaccion"],
        );
        $this->model_pagos->update_transaccion_id($pago_id, $paymentUp);


        if (!$this->sw_subscription) {
            $pago_servicio = array(
                "pag_id" => $pag_id,
                "ser_id" => $serviceId
            );
        }else{
            $pago_servicio = array(
                "pag_id" => $pag_id,
                "ser_id" => $serviceId,
                "fech_ini" => date('Y-m-d'),
                "fech_fin" => date('Y-m-d', strtotime('+30 days'))
            );
        }

        $this->model_pago_servicio->insert($pago_servicio);

        return $pag_id;

    }

    public function paymentinfo() {
        $publicationId = $this->uri->segment(4);
        $payment = $this->model_pagos->get_last_payment_by_publication($publicationId);

        $data['pagoId']       = $payment->pag_id;
        $data['totalPayment'] = $payment->pag_monto;
        $data['paymentType']  = $_POST['paymentType'];

        $this->load_template_backend( 'backend/payment/payment_mandatory', $data );
    }

    public function insert_features($cat_id, $inm_id){
        $features = $this->model_caracteristica->get_features_by_category($cat_id);

        foreach($features as $feature){
            $valor = $_POST[$feature->car_nombre];
            $values = array(
                'eca_inm_id' => $inm_id,
                'eca_car_id' => $feature->car_id,
                'eca_valor' => $valor
            );
            $this->model_inmueble_caracteristica->insert($values);
        }
    }

    private function exist_url_seo($seo){
        $sw = true;
        $i = 1;
        $aux = $seo;
        while($sw){
            $sw = $this->model_publicacion->exist_url_seo($aux);
            if($sw){
                $aux = $seo . "-".$i;
                $i++;
            }
        }
        return $aux;
    }

    public function delete(){
        $id = $this->uri->segment(4);
        $this->model_publicacion->delete_publication($id);
        redirect('dashboard/publications?action=delete');
    }

    public function pdf() {
    }

    public function export_excel() {
        $search = $_GET['searchText'];
        $total = $this->model_publicacion->get_num_publications_by_user($this->user_id, $search);
        $publications = $this->model_publicacion->get_publications_by_user($this->user_id, 0, $total, $search);

        //Set the titles Excel CELL and value
        $headerTitles = array(
            'A1'=> "NRO",
            'B1'=> "TITULO",
            'C1'=> "PRECIO",
            'D1'=> "VISTAS",
            'E1'=> "FECHA FIN",
            'F1'=> "PUBLICADO",
            'G1'=> "ESTADO"
        );

        $this->generate_excel_file($headerTitles, $publications);
    }

    private function generate_excel_file($titles, $data) {
        $objPHPExcel = $this->excel;
        //activate worksheet number 1
        $objPHPExcel->setActiveSheetIndex(0);
        //name the worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Lista de Anuncios TET');

        //write titles
        foreach($titles as $cell => $value) {
            //HEADER TITLES
            $objPHPExcel->getActiveSheet()->setCellValue($cell, $value);
            //SET STYLES
            $objPHPExcel->getActiveSheet()->getStyle($cell)->getFont()->setBold(true);
        }

        //write data
        $i = 1;
        foreach($data as $o) {
            $i++;
            $objPHPExcel->getActiveSheet()->setCellValue("A" . $i, $i-1);
            $objPHPExcel->getActiveSheet()->setCellValue("B" . $i, $o->inm_nombre);
            $objPHPExcel->getActiveSheet()->setCellValue("C" . $i, $o->inm_precio);
            $objPHPExcel->getActiveSheet()->setCellValue("D" . $i, $o->inm_visitas);
            $objPHPExcel->getActiveSheet()->setCellValue("E" . $i, date('d/m/Y', strtotime($o->finish)));
            $objPHPExcel->getActiveSheet()->setCellValue("F" . $i, $o->inm_publicado);

            // @TODO Generalmente los estado no deberian cambiar revisar esto.
            $status = 'Pendiente';
            if($o->pub_estado != 'No Aprobado') {
                $status = $o->pub_estado;
            }
            $objPHPExcel->getActiveSheet()->setCellValue("G" . $i, $status);
        }

        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $filename = "Toque El Timbre " . date('Y-m-d') . ".xls";
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
        //$objWriter->save('test.xlsx');

    }

    public function renew()
    {
        $publicationId = $this->uri->segment(4);
        $values = array(
            "pub_estado" => 'Aprobado',
            "pub_vig_fin" => date('Y-m-d H:i:s', strtotime('+90 days'))
        );
        $this->model_publicacion->update($publicationId, $values);

        $inmuebleId = $this->model_publicacion->get_inmueble_by_pub_id($publicationId);
        $values = array(
            "inm_publicado" => 'Si',
        );
        $this->model_inmueble->update($inmuebleId, $values);
        redirect(base_url('dashboard/publications'));
    }

    public function renew_old(){
        $pub_id = $this->uri->segment(4);
        //$data['user_info'] = $this->model_usuario->get_info_agent($this->user_id);

        $values_publication = array(
            'pub_renovado' => date('Y-m-d H:i:s'),
            "pub_enviado_1" => false
        );
        $publicationId = $this->model_publicacion->update($pub_id, $values_publication);

        $values_renew = array(
            'id_pub' => $pub_id,
            'fecha_ren_ini' => date('Y-m-d'),
            'fecha_ren_fin' => date('Y-m-d', strtotime('+90 days')),

        );
        $publicationId = $this->model_publicacion->insert_log_renew($values_renew);
        $this->pay('Renovacion');

        // if( tiene suscripcion) => renovamos gratis
        // if (no tiene suscripcion) => renovamos cobrando los 30bs
        //Obtener vista para pagar nuevamente los 30 bs, para habilitar nuevamente el anuncio.
    }

    public function register_form()
    {
        $inm_id = $this->uri->segment(4);
        $this->load->model('model_vendido');

        // insertar las respuestas de preguntas en la DB
        $values = array(
            'pregunta1' => ""
        );
        $this->model_vendido->insert($values);
        // Guardar como no publicado el inmueble

        $values = array(
            'inm_publicado' => "No"
        );
        $this->model_inmueble->update($inm_id, $values);

        // redireccionar a la lista
        redirect('dashboard/publications?action=vendido');
    }

}