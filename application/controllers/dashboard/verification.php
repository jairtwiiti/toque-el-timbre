<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Christian
 * Date: 19/11/2014
 * Time: 15:36
 */

class Verification extends Private_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->helper("url");
        $this->load->helper("form");
        $this->load->model('model_usuario');
    }

    public function index(){
        $data = array();
        $id = $this->uri->segment(2);
        $key = $this->uri->segment(3);

        $user = $this->model_usuario->get_id_user_verification($id, $key);
        if(!empty($user)){
            $status = $this->model_usuario->get_status_email(sha1($user->usu_email), $key);

            if(!empty($status)){
                $this->model_usuario->update($user->usu_id, array("usu_estado" => "Habilitado"));

                $logged_user = $this->model_usuario->authentication_redirect($user->usu_id);
                $this->session->set_userdata('logged_user', $logged_user);

                redirect('dashboard/?action=verify');
            }else{
                $data["error_verified"] = 'El email ya se encuentra habilitado.';
            }
        }else{
            $data["error_code"] = 'El codigo de confirmacion es erroneo, verifique si el enlace es igual al que le enviamos.';
        }
        $this->load->view('backend/verification', $data);
    }

    public function reset(){
        $data = array();
        $email = $this->uri->segment(2);
        $key = $this->uri->segment(3);

        $user = $this->model_usuario->get_email_reset_password($email, $key);
        if(!empty($user)){
            $data["change_password"] = TRUE;

            if($_POST){

                $password = $this->input->post('password');
                $rep_password = $this->input->post('rep_password');

                $this->form_validation->set_rules('password', 'Contraseña', 'required|matches[rep_password]');
                $this->form_validation->set_rules('rep_password', 'Repetir Contraseña', 'required');

                $this->form_validation->set_message('required', 'El campo <b>%s</b> es requerido.');
                $this->form_validation->set_message('matches', 'Las contraseñas deben ser iguales.');

                if($this->form_validation->run() == TRUE){
                    $data = array(
                        'usu_password' => sha1($password),
                        'usu_key' => NULL
                    );
                    $this->model_usuario->update($user->usu_id, $data);
                    $data["change_password"] = FALSE;
                    $data["change_ok"] = TRUE;
                }
            }

        }else{
            $data["change_password"] = FALSE;
            $data["error_code"] = TRUE;
        }
        $this->load->view('backend/change_password', $data);
    }

}