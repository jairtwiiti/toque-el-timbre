<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Christian
 * Date: 04/12/2014
 * Time: 15:01
 */

class Payment extends Private_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_usuario');
        $this->load->model('model_pagos');
        $this->load->model('model_suscripcion');
        $this->load->model('model_publicacion');
	    $this->load->model('model_servicios');
	    $this->load->model('model_pago_servicio');
        $this->load->model('model_publicacion');
        $this->load->model('model_parametros');
        $this->load->model('model_service');

        $this->breadcrumb['level2'] = array('name' => 'Pagos', 'link' => base_url().'dashboard/payments');
        $this->page_title = 'Mis Pagos';
        $this->page = 'payment';
    }

    public function index()
    {
        $data['user_info'] = $this->model_usuario->get_info_agent($this->user_id);
        $status = !empty($_GET['status']) ? $_GET['status'] : 'Pagado';
        $pub_id = !empty($_GET['uid']) ? $_GET['uid'] : "";
        $userId = !empty($_GET['userid']) ? $_GET['userid'] : "";
        $userId = $this->user->usu_tipo == "Admin"?$userId:$this->user_id;

        $data['total'] = $this->model_pagos->get_num_payment_by_user($userId, $status);
        $this->load->library('pagination');

        $from = ($this->uri->segment(4)) ? $this->uri->segment(4) : 1;
        $options['base_url'] = base_url(). 'dashboard/payments/p/';
        $options['first_url'] = base_url(). 'dashboard/payments/p/1';
        $options['per_page'] = 30;
        $options['uri_segment'] = 4;
        $options['first_link'] = '&laquo;';
        $options['last_link'] = '&raquo;';
        $options['next_link'] = '&rsaquo;';
        $options['prev_link'] = '&lsaquo;';
        $options['num_tag_open'] = '<li class="paginate_button">';
        $options['num_tag_close'] = '</li>';
        $options['cur_tag_open'] = '<li class="paginate_button active"><a href="javascript:;">';
        $options['cur_tag_close'] = '</a></li>';
        $options['next_link'] = '&gt;';
        $options['next_tag_open'] = '<li class="paginate_button next">';
        $options['next_tag_close'] = '</li>';
        $options['prev_link'] = '&lt;';
        $options['prev_tag_open'] = '<li class="paginate_button previous">';
        $options['prev_tag_close'] = '</li>';

        $options['total_rows'] = $data['total'];
        $choice = ceil($options["total_rows"] / $options["per_page"]);
        $options["num_links"] = $choice;
        //$options['suffix'] = '?' . http_build_query($_GET, '', "&");
        $options['use_page_numbers'] = TRUE;

        $this->pagination->initialize($options);
        $data['pagination'] = $this->pagination->create_links();
        $data['payments'] = $this->model_pagos->get_payment_by_user($userId, $from, $options['per_page'], $status, $pub_id);
        $this->load_template_backend('backend/payment/list', $data);
    }

    public function detail(){
        $data['user_info'] = $this->model_usuario->get_info_agent($this->user_id);
        $pag_id = $this->uri->segment(4);
        $pag_id = $pag_id;
        $data['detail'] = $this->model_pagos->get_payment_detail($pag_id);
        $data['hasSubscription']= $this->sw_subscription;
        $this->load_template_backend('backend/payment/detail', $data);
    }

    public function register() {
        $data['user_info'] = $this->model_usuario->get_info_agent($this->user_id);

	    if(!empty($_POST)) {

			if ($this->form_validate()) {
				$publicationId = $this->uri->segment(3);

				$paymentType = $_POST['paymentType'];
				$paymentDate = date("Y-m-d H:i:s");
				$endPaymentDate = strtotime ('+14 days', strtotime($paymentDate)) ;
				$endPaymentDate = date ('Y-m-d H:i:s', $endPaymentDate);

				$userName = "S/N";
				if(isset($_POST["userName"]) && $_POST["userName"] != ""){
					$userName = $_POST["userName"];
				}

				$userNit = 0;
				if(isset($_POST["userNit"]) && $_POST["userNit"] != ""){
					$userNit = $_POST["userNit"];
				}

				//$totalPayment = $_POST['paymentTotal'];
                $totalPayment = $this->validar_precios_mejoras_pago($_POST['principal'], $_POST['busqueda'], $_POST['similares']);

				$data = array();
				$sw = true;
				while($sw){
					$pag_id = $this->codigoAlfanumerico(7);
					if(!$this->model_pagos->exist($pag_id)){
						$sw = false;
					}
				}
				$payment = array(
					"pag_id" => $pag_id,
					"pag_pub_id" => $publicationId,
					"pag_fecha" => $paymentDate,
					"pag_nombre" =>$userName,
					"pag_nit" => $userNit,
					"pag_monto" => $totalPayment,
					"pag_estado" => 'Pendiente',
                    'pag_concepto' => 'Mejora',
					"pag_entidad" => $paymentType,
					"pag_fecha_ven" => $endPaymentDate,
                );

				$paymentId = $this->model_pagos->insert($payment);
				$Descripcion = "";
				if(isset($_POST['principal']) && $_POST['principal'] != ""){
					$Descripcion .= "PRIN-".$_POST['principal']."DIAS, ";

					$pago_servicio = array(
						"pag_id" => $pag_id,
						"ser_id" => $_POST['principal']
                    );
					$this->model_pago_servicio->insert($pago_servicio);
				}
				if(isset($_POST['banner']) && $_POST['banner'] != ""){
					$Descripcion .= "BANN-".$_POST['banner']."DIAS, ";

					$pago_servicio = array(
						"pag_id" => $pag_id,
						"ser_id" => $_POST['banner']
                    );
					$this->model_pago_servicio->insert($pago_servicio);
				}
				if(isset($_POST['busqueda']) && $_POST['busqueda'] != ""){
					$Descripcion .= "BUSQ-".$_POST['busqueda']."DIAS";

					$pago_servicio = array(
						"pag_id" => $pag_id,
						"ser_id" => $_POST['busqueda']
                    );
					$this->model_pago_servicio->insert($pago_servicio);
				}
                if(isset($_POST['similares']) && $_POST['similares'] != ""){
                    $Descripcion .= "BUSQ-".$_POST['similares']."DIAS";

                    $pago_servicio = array(
                        "pag_id" => $pag_id,
                        "ser_id" => $_POST['similares']
                    );
                    $this->model_pago_servicio->insert($pago_servicio);
                }
				if($_POST['paymentType'] === 'PAGOSNET') {
					$result = array();
					$pago_id = $pag_id;
					$MontoTotal = $totalPayment;
					$fecha_vencimiento = $endPaymentDate;
					$fecha = $paymentDate;
					$nombre_fact = $userName;
					$nit_fact = $userNit;
					include BASE_DIRECTORY . DS . 'sintesis' . DS . 'servicios' . DS . 'clientePagosNet.php';
                    $data['errorSintesis'] = $errorSintesis;
                    if($errorSintesis == "No"){
                        $paymentUp = array(
                            "pag_cod_transaccion" => $result["cmeRegistroPlanResult"]["idTransaccion"],
                        );
                        $this->model_pagos->update_transaccion_id($pago_id, $paymentUp);
                    }
				}elseif($_POST['paymentType'] === 'TIGOMONEY') {
                    $_POST['pag_id'] = $pag_id;
                } elseif($_POST['paymentType'] === 'OFICINA') {
				} elseif($_POST['paymentType'] === 'BANCO') {}

				$data['pagoId'] = $pag_id;
				$data['totalPayment'] = $totalPayment;
				$data['paymentType'] = $_POST['paymentType'];

                // Email notificacion de pagos al admin del sitio para seguimiento
                $aux_inm  = $this->model_publicacion->get_name_inmueble_by_pub_id($publicationId);
                $user_email = $this->model_usuario->get_user_by_publication($publicationId);
                $concepto = "#".$aux_inm->inm_id . " - ".$aux_inm->inm_nombre;
                $usuario = $user_email->nombre;
                $telefono = $user_email->usu_telefono . " - " . $user_email->usu_celular;
                $email = $user_email->usu_email;
                $monto = $totalPayment;
                $tipo = "Mejora de Anuncio";
                $forma = $_POST['paymentType'];
                $facturacion_name = $_POST["userName"];
                $facturacion_nit = $_POST["userNit"];
//                $this->send_mail_pago_notificacion($concepto,$usuario,$telefono,$email,$pag_id, $monto,$tipo,$forma,$facturacion_name,$facturacion_nit);
                static::send_mail_pago_notificacion($concepto,$usuario,$telefono,$email,$pag_id, $monto,$tipo,$forma,$facturacion_name,$facturacion_nit);

                // Email CODIGDO DE PAGO para que el cliente pase por PAGOSNET
//                $this->send_mail_codigo_pago_not_cliente($concepto,$email,$pag_id, $monto);
                static::send_mail_codigo_pago_not_cliente($concepto,$email,$pag_id, $monto);

                if($_POST['paymentType'] === 'TIGOMONEY') {
                    $this->tigomoney();
                }

				$this->load_template_backend('backend/payment/payment_confirmation', $data);
			}
            else
            {
                redirect();
            }
		}
        else
        {

		    $data['mainServices'] = $this->model_servicios->get_services_by_type('Principal');
		    $data['bannerServices'] = $this->model_servicios->get_services_by_type('Landing');
		    $data['searchServices'] = $this->model_servicios->get_services_by_type('Busqueda');
            $data['particularServices'] = $this->model_servicios->get_services_by_type('Anuncios');
            $data['similarsService'] = $this->model_servicios->get_services_by_type('Similares');
            $data['sw_subscription']= $this->sw_subscription;
            $user = $this->model_usuario->get_info_agent($this->user_id);
            $this->load_template_backend('backend/payment/register', $data);
        }
    }

    public function validar_precios_mejoras_pago($id_principal, $id_busqueda, $similarsId){
        $tiene_suscripcion = $this->sw_subscription;

        $id_busqueda_precio = $id_principal;
        $id_principal_precio = $id_busqueda;
        $similarsPriceId =  $similarsId;

        if($id_busqueda_precio != 0)
        {
            $busqueda = $this->model_servicios->get_id($id_busqueda_precio);
            $precio_busqueda = $busqueda->ser_precio;
        }
        else
        {
            $precio_busqueda = 0;
        }
        if($id_principal_precio != 0){
            $principal = $this->model_servicios->get_id($id_principal_precio);
            $precio_principal = $principal->ser_precio;
        }
        else
        {
            $precio_principal = 0;
        }

        $similarPrice = 0;
        if($similarsPriceId != 0){
            $similar = $this->model_servicios->get_id($similarsPriceId);
            $similarPrice = $similar->ser_precio;
        }

        if($tiene_suscripcion)
        {
            $precio_busqueda = $busqueda->ser_precio_suscriptor;
            $precio_principal = $principal->ser_precio_suscriptor;
            $similarPrice = $similar->ser_precio_suscriptor;
        }
        $total = $precio_busqueda + $precio_principal + $similarPrice;
        return $total;
    }

    public function register_publication()
    {
        $data['user_info'] = $this->model_usuario->get_info_agent($this->user_id);
        $this->complementHandler->addProjectJs("payment.register-publication");
        $publicationId = $this->uri->segment(4);

        if(!empty($_POST))
        {
            $this->form_validation->set_rules('anuncio', 'Anuncio', 'trim');
	        //$this->form_validation->set_message('required', 'Seleccionar los dias para su anuncio');

            if ($this->form_validation->run())
            {
                $cupon = $this->input->post('coupon');
                $is_valid = $this->validar_cupon_post($cupon);
                $is_valid = json_decode($is_valid);

                if($is_valid->error == 0 && !empty($cupon))
                {
                    $this->register_payment_publication($publicationId, $is_valid->data);
                }
                elseif($is_valid->error < 0 && !empty($cupon))
                {
                    $data['mainServices']       = $this->model_servicios->get_services_by_type('Principal');
                    $data['bannerServices']     = $this->model_servicios->get_services_by_type('Landing');
                    $data['searchServices']     = $this->model_servicios->get_services_by_type('Busqueda');
                    $data['particularServices'] = $this->model_servicios->get_services_by_type('Anuncios');
                    $data['similarServices']    = $this->model_servicios->get_services_by_type('Similares');
                    $data['sw_subscription']    = $this->sw_subscription;
                    $data['concepto'] = $_POST['concepto'];
                    $data['publicationId']      = $publicationId;
                    $data['cupon_error']      = $is_valid->error;
                    $this->load_template_backend('backend/payment/payment_services_step1', $data);
                }
                else
                {
                    $this->register_payment_publication($publicationId,'');
                }
            }
            else
            {
                $data['mainServices']       = $this->model_servicios->get_services_by_type('Principal');
                $data['bannerServices']     = $this->model_servicios->get_services_by_type('Landing');
                $data['searchServices']     = $this->model_servicios->get_services_by_type('Busqueda');
                $data['particularServices'] = $this->model_servicios->get_services_by_type('Anuncios');
                $data['similarServices']    = $this->model_servicios->get_services_by_type('Similares');
                $data['sw_subscription']    = $this->sw_subscription;
                $data['concepto'] = $_POST['concepto'];
                $data['publicationId']      = $publicationId;

                $this->load_template_backend('backend/payment/payment_services_step1', $data);
            }
        }
    }

    public function register_payment_publication($publicationId, $descuento){
        $data['user_info'] = $this->model_usuario->get_info_agent($this->user_id);

        $paymentType   = NULL;
        $paymentDate    = date( "Y-m-d H:i:s" );
        $endPaymentDate = strtotime( '+14 days', strtotime( $paymentDate ) );
        $endPaymentDate = date( 'Y-m-d H:i:s', $endPaymentDate );
        //$totalPayment = $_POST['paymentTotal'];

        $anuncio = $_POST['anuncio'];
        if(empty($anuncio) || $anuncio == null){
            //redirect('dashboard/publications'); // redireccionar cuando es cero por que es error
        }
//        $totalPayment = $this->validar_precios_anuncios_pago($anuncio, 0);
        $totalPayment = model_service::getTotalPaymentServiceByArrayIds($this->input->post('services'));
        if(!empty($descuento)){
            $descuento_id = $descuento->id;
            $descuento_valor = ($descuento->descuento / 100) * $totalPayment;
            $descuento_values = array(
                "cobrado" => "Si",
                "fecha_cobrado" => date("Y-m-d H:i:s")
            );
            $this->model_cupon_usuario->update($descuento_id, $descuento_values);
        }else{
            $descuento_id = 0;
            $descuento_valor = 0;
        }
        $totalPayment = model_service::getTotalPaymentServiceByArrayIds($this->input->post('services'));
        $estadoPago = 'Pendiente';
        $fechaPagado = NULL;

        if ($anuncio == 20) {
            $publicationAux = $this->model_publicacion->get_created_publication($publicationId);
            $values = array(
                "pub_vig_fin" => date('Y-m-d H:i:s', strtotime('+30 days', strtotime($publicationAux->pub_vig_fin)))
            );
            $this->model_publicacion->update($publicationId, $values);
        }

        $paymentParams = array (
            'paymentType' => $paymentType,
            'paymentDate' => $paymentDate,
            'endPaymentDate' => $endPaymentDate,
            'totalPayment' => $totalPayment,
            'services' => array($anuncio, $_POST['principal'], $_POST['banner'], $_POST['busqueda'], $_POST['similares']),
            'estado' => $estadoPago,
            'concepto' => $_POST['concepto'],
            'fechaPago' => $fechaPagado
        );

        $paymentType   = $paymentParams['paymentType'];
        $paymentDate    = $paymentParams['paymentDate'];
        $endPaymentDate = $paymentParams['endPaymentDate'];
        $totalPayment = $paymentParams['totalPayment'];
        $services = $paymentParams['services'];
        $estado = $paymentParams['estado'];
        $concepto = $paymentParams['concepto'];
        $fechaPago = $paymentParams['fechaPago'];

        $sw   = true;
        while ($sw) {
            $pag_id = $this->codigoAlfanumerico(7);
            if (!$this->model_pagos->exist($pag_id)) {
                $sw = false;
            }
        }

        $userName = "S/N";
        if ( isset( $_POST["userName"] ) && $_POST["userName"] != "" ) {
            $userName = $_POST["userName"];
        }

        $userNit = 0;
        if ( isset( $_POST["userNit"] ) && $_POST["userNit"] != "" ) {
            $userNit = $_POST["userNit"];
        }

        $payment = array(
            "pag_id"        => $pag_id,
            "pag_pub_id"    => $publicationId,
            "pag_fecha"     => $paymentDate,
            "pag_nombre"    => $userName,
            "pag_nit"       => $userNit,
            "pag_monto"     => $totalPayment,
            "pag_cupon_user"     => $descuento_id,
            "pag_monto_descuento"     => $descuento_valor,
            "pag_estado"    => $estado,
            "pag_entidad"   => $paymentType,
            "pag_concepto"   => $concepto,
            "pag_fecha_ven" => $endPaymentDate,
            "pag_fecha_pagado" => $fechaPago
        );
        $paymentId = $this->model_pagos->insert( $payment );

        foreach($services as $serviceId) {
            if($serviceId != 0) {
                if (!$this->sw_subscription) {
                    $pago_servicio = array(
                        "pag_id" => $pag_id,
                        "ser_id" => $serviceId
                    );
                }else{
                    $pago_servicio = array(
                        "pag_id" => $pag_id,
                        "ser_id" => $serviceId,
                        "fech_ini" => date('Y-m-d'),
                        "fech_fin" => date('Y-m-d', strtotime('+30 days'))
                    );
                }
                $this->model_pago_servicio->insert($pago_servicio);
            }
        }

        $Descripcion = "PAGO DE ANUNCIO";
        $result = array();
        $data = array();
        $pago_id = $pag_id;
        $MontoTotal = $totalPayment;
        $fecha_vencimiento = $endPaymentDate;
        $fecha = $paymentDate;
        $nombre_fact = $userName;
        $nit_fact = $userNit;
        include BASE_DIRECTORY . DS . 'sintesis' . DS . 'servicios' . DS . 'clientePagosNet.php';

        $data['errorSintesis'] = $errorSintesis;
        if($errorSintesis == "No"){
            $paymentUp = array(
                "pag_cod_transaccion" => $result["cmeRegistroPlanResult"]["idTransaccion"],
            );
            $this->model_pagos->update_transaccion_id($pago_id, $paymentUp);
        }

        $data['pagoId']       = $pag_id;
        $data['totalPayment'] = $totalPayment;
        $data['paymentType']  = $_POST['paymentType'];

        // Email notificacion de pagos al admin del sitio para seguimiento
        $aux_inm  = $this->model_publicacion->get_name_inmueble_by_pub_id($publicationId);
        $user_email = $this->model_usuario->get_user_by_publication($publicationId);
        $concepto = "#".$aux_inm->inm_id . " - ".$aux_inm->inm_nombre;
        $usuario = $user_email->nombre;
        $telefono = $user_email->usu_telefono . " - " . $user_email->usu_celular;
        $email = $user_email->usu_email;
        $monto = $totalPayment;
        $tipo = "Anuncio Estandar";
        $forma = $_POST['paymentType'];
        $facturacion_name = $_POST["userName"];
        $facturacion_nit = $_POST["userNit"];
//        $this->send_mail_pago_notificacion($concepto,$usuario,$telefono,$email,$pag_id,$monto,$tipo,$forma,$facturacion_name,$facturacion_nit);
        static::send_mail_pago_notificacion($concepto,$usuario,$telefono,$email,$pag_id,$monto,$tipo,$forma,$facturacion_name,$facturacion_nit);

        // Email CODIGDO DE PAGO para que el cliente pase por PAGOSNET
//        $this->send_mail_codigo_pago_not_cliente($concepto,$email,$pag_id, $monto);
        static::send_mail_codigo_pago_not_cliente($concepto,$email,$pag_id, $monto);
        $this->load_template_backend( 'backend/payment/payment_mandatory', $data);
    }

    public function validar_precios_anuncios_pago($anuncio, $descuento){
        $tiene_suscripcion = $this->sw_subscription;
        $formData = $this->input->post();
//        $totalPayment = model_service::getTotalPaymentServiceByArrayIds($formData['services']);
//        echo "<pre>";var_dump($totalPayment);exit;
        //The announces are free
        $id_anuncio_precio = $anuncio;
        $id_busqueda_precio = $this->input->post("busqueda");
        $id_principal_precio = $this->input->post("principal");
        $similarsIdPrice = $this->input->post("similares");
        if($anuncio != "")
        {
            $anuncio = $this->model_servicios->get_id($id_anuncio_precio);
        }
        else
        {
            //The announces are free
            $anuncio = (object) array('ser_precio' => 0, 'ser_precio_suscriptor' => 0);
        }
        $precio_anuncio = $anuncio->ser_precio;
        if($id_busqueda_precio != 0){
            $busqueda = $this->model_servicios->get_id($id_busqueda_precio);
            $precio_busqueda = $busqueda->ser_precio;
        }else{
            $precio_busqueda = 0;
        }
        if($id_principal_precio != 0){
            $principal = $this->model_servicios->get_id($id_principal_precio);
            $precio_principal = $principal->ser_precio;
        }else{
            $precio_principal = 0;
        }
        if($similarsIdPrice != 0){
            $similars = $this->model_servicios->get_id($similarsIdPrice);
            $similarsPrice = $similars->ser_precio;
        }else{
            $similarsPrice = 0;
        }
        if($tiene_suscripcion){
            $precio_anuncio = $anuncio->ser_precio_suscriptor;
            $precio_busqueda = $busqueda->ser_precio_suscriptor;
            $precio_principal = $principal->ser_precio_suscriptor;
            $similarsPrice = $similars->ser_precio_suscriptor;
        }
        $total = ($precio_anuncio + $precio_busqueda + $precio_principal + $similarsPrice) - $descuento;
        return $total;
    }

	public function form_validate() {

		$this->form_validation->set_rules('paymentType', 'Metodo de pago', 'trim|required');

		if(!$_POST['principal'] &&
            !$_POST['banner'] &&
            !$_POST['busqueda'] &&
            !$_POST['similares'] &&
            !$_POST['anuncio']) {

			$this->form_validation->set_rules('principal', 'Costo ', 'trim|required');
		}

		if($_POST['invoiced'] == 'on'){
			//$this->form_validation->set_rules('paymentType', 'Tipo de pago', 'trim|required|numeric');
		}

		//$this->form_validation->set_message('numeric', 'El Nit solo debe contener Numeros');
		$this->form_validation->set_message('required', 'Debe seleccionar al menos un Costo y metodo de pago');
        $response = FALSE;
		if($this->form_validation->run() == TRUE){
			$response = TRUE;
		}

        return $response;
	}

    public function delete(){
        // Eliminar pago pendiente
        $pag_id = $this->uri->segment(4);
        $values = array(
            "pag_estado" => "Eliminado"
        );
        $this->model_pagos->update($pag_id, $values);
        redirect('dashboard/payments?status=Pendiente&action=delete');
    }

    public function subscription(){
        $data['user_info'] = $this->model_usuario->get_info_agent($this->user_id);
        $this->page = 'payment_subscription';
        if($_POST){

            $this->form_validation->set_rules('subscription', 'Suscripcion', 'trim|required');
	        $this->form_validation->set_rules('paymentType', 'paymentType', 'trim|required');
            $this->form_validation->set_message('required', 'Seleccionar una de las siguientes suscripciones y una forma de pago');

            if ($this->form_validation->run()) {

                $cupon = $this->input->post('coupon');
                $is_valid = $this->validar_cupon_post($cupon);
                $is_valid = json_decode($is_valid);

                if($is_valid->error == 0 && !empty($cupon)){
                    $this->register_payment_subscription($is_valid->data);
                }elseif($is_valid->error < 0 && !empty($cupon)){
                    $data['cupon_error'] = $is_valid->error;
                    $data['subscription_services'] = $this->model_servicios->get_services_by_type('Suscripcion');
                    $this->load_template_backend('backend/subscription/register', $data);
                }else{
                    $this->register_payment_subscription('');
                }
            }else{
                $data['subscription_services'] = $this->model_servicios->get_services_by_type('Suscripcion');
                $this->load_template_backend('backend/subscription/register', $data);
            }
        }else{
            $data['subscription_services'] = $this->model_servicios->get_services_by_type('Suscripcion');
            $this->load_template_backend('backend/subscription/register', $data);
        }
    }

    public function register_payment_subscription($descuento){
        $data['user_info'] = $this->model_usuario->get_info_agent($this->user_id);
        $id_service = $this->input->post('subscription');
        $service = $this->model_servicios->get_id($id_service);
        $paymentType = $_POST['paymentType'];

        $paymentDate = date("Y-m-d H:i:s");
        $endPaymentDate = strtotime ('+14 days', strtotime($paymentDate)) ;
        $endPaymentDate = date ('Y-m-d H:i:s', $endPaymentDate);

        $userName = "S/N";
        if(isset($_POST["userName"]) && $_POST["userName"] != ""){
            $userName = $_POST["userName"];
        }

        $userNit = 0;
        if(isset($_POST["userNit"]) && $_POST["userNit"] != ""){
            $userNit = $_POST["userNit"];
        }

        //$totalPayment = $_POST["paymentTotal"];
        $totalPayment = $this->validar_precios_suscripcion_pago($id_service, 0);
        if(!empty($descuento)){
            $descuento_id = $descuento->id;
            $descuento_valor = ($descuento->descuento / 100) * $totalPayment;
            $totalPayment = $_POST['paymentTotalNew'];
            $this->model_cupon_usuario->update($descuento_id, array("cobrado" => "Si"));
        }else{
            $descuento_id = 0;
            $descuento_valor = 0;
        }
        $totalPayment = $this->validar_precios_suscripcion_pago($id_service, $descuento_valor);

        $data = array();
        $sw = true;
        while($sw){
            $pag_id = $this->codigoAlfanumerico(7);
            if(!$this->model_pagos->exist($pag_id)){
                $sw = false;
            }
        }
        $payment = array(
            "pag_id" => $pag_id,
            "pag_pub_id" => 0,
            "pag_fecha" => $paymentDate,
            "pag_nombre" =>$userName,
            "pag_nit" => $userNit,
            "pag_monto" => $totalPayment,
            "pag_cupon_user" => $descuento_id,
            "pag_monto_descuento" => $descuento_valor,
            "pag_estado" => 'Pendiente',
            "pag_entidad" => $paymentType,
            "pag_concepto" => 'Suscripcion',
            "pag_fecha_ven" => $endPaymentDate
        );
        $paymentId = $this->model_pagos->insert($payment);

        $pago_servicio = array(
            "pag_id" => $pag_id,
            "ser_id" => $id_service
        );
        $this->model_pago_servicio->insert($pago_servicio);


        if($_POST['paymentType'] === 'PAGOSNET') {

            $Descripcion = "";
            $Descripcion .= "SUSCRIPCION-".$service->ser_dias."DIAS";
            $result = array();
            $pago_id = $pag_id;
            $MontoTotal = $totalPayment;
            $fecha_vencimiento = $endPaymentDate;
            $fecha = $paymentDate;
            $nombre_fact = $userName;
            $nit_fact = $userNit;
            include BASE_DIRECTORY . DS . 'sintesis' . DS . 'servicios' . DS . 'clientePagosNet.php';
            $data['errorSintesis'] = $errorSintesis;
            if($errorSintesis == "No"){
                $paymentUp = array(
                    "pag_cod_transaccion" => $result["cmeRegistroPlanResult"]["idTransaccion"],
                );
                $this->model_pagos->update_transaccion_id($pago_id, $paymentUp);
            }

        }elseif($_POST['paymentType'] === 'TIGOMONEY') {
            $_POST['pag_id'] = $pag_id;
        } elseif($_POST['paymentType'] === 'OFICINA') {
        } elseif($_POST['paymentType'] === 'BANCO') {}

        $suscription = array(
            "usuario_id" => $this->user_id,
            "fecha_inicio" => null,
            "fecha_fin" => null,
            "pago_id" => $pag_id,
            "estado" => 'Inactivo'
        );

        $this->model_suscripcion->insert($suscription);

        $data['pagoId'] = $pag_id;
        $data['totalPayment'] = $totalPayment;
        $data['paymentType'] = $_POST['paymentType'];

        // Email notificacion de pagos al admin del sitio para seguimiento
        $concepto = "Suscripcion";
        $user_email = $this->model_usuario->get_user_by_id($this->user_id);
        $usuario = $user_email->usu_nombre . " " . $user_email->usu_apellido;
        $telefono = $user_email->usu_telefono . " - " . $user_email->usu_celular;
        $email = $user_email->usu_email;
        $monto = $totalPayment;
        $tipo = "Suscripcion a Paquete";
        $forma = $_POST['paymentType'];
        $facturacion_name = $_POST["userName"];
        $facturacion_nit = $_POST["userNit"];
//        $this->send_mail_pago_notificacion($concepto,$usuario,$telefono,$email,$pag_id,$monto,$tipo,$forma,$facturacion_name,$facturacion_nit);
        static::send_mail_pago_notificacion($concepto,$usuario,$telefono,$email,$pag_id,$monto,$tipo,$forma,$facturacion_name,$facturacion_nit);

        // Email CODIGDO DE PAGO para que el cliente pase por PAGOSNET
//        $this->send_mail_codigo_pago_not_cliente($concepto,$email,$pag_id, $monto);
        static::send_mail_codigo_pago_not_cliente($concepto,$email,$pag_id, $monto);

        if($_POST['paymentType'] === 'TIGOMONEY') {
            $this->tigomoney();
        }

        $this->load_template_backend('backend/subscription/confirmation', $data);
    }

    public function validar_precios_suscripcion_pago($anuncio, $descuento){
        $tiene_suscripcion = $this->sw_certification;

        $id_anuncio_precio = $anuncio;
        $anuncio = $this->model_servicios->get_id($id_anuncio_precio);
        $precio_anuncio = $anuncio->ser_precio;

        if($tiene_suscripcion){
            $precio_anuncio = $anuncio->ser_precio_suscriptor;
        }
        $total = ($precio_anuncio) - $descuento;
        return $total;
    }

    // Esta funcion se utiliza cuando se verifica el pago de la suscripcion
    ##################################################################################THIS METHOD SEEMS NOT USED
    public function pay_publications_pending(){
        $payments = $this->model_pagos->get_payment_pending_subscription_by_user($this->user_id);
        if(count($payments) > 0){
            foreach($payments as $obj_pago){
                $values = array(
                    "pag_estado" => 'Pagado',
                    "pag_monto" => '0',
                    "pag_entidad" => 'Suscripcion',
                    "pag_fecha_pagado" => date('Y-m-d H:i:s')
                );
                $this->model_pagos->update($obj_pago->pag_id, $values);
                $value_pub = array(
                    "pub_estado" => 'Aprobado',
                    "pub_vig_fin" => date('Y-m-d', strtotime('+30 days'))
                );
                $this->model_publicacion->update($obj_pago->pag_pub_id, $value_pub);
            }
        }

    }


    public function send_mail_pago_notificacion_deprecated($concepto, $usuario, $telefono, $email, $pag_id, $monto, $tipo, $forma,$facName,$facNit){
        require_once(BASE_DIRECTORY . DS . 'admin' . DS . 'clases' . DS. 'phpmailer' . DS . 'class.phpmailer.php');
        $parametros = $this->model_parametros->get_all();

        $template = base_url() . "assets/template_mail/mail_pago_notificacion_admin.html";
        $contenido = $this->obtener_contenido($template);
        $contenido = str_replace('@@concepto',$concepto,$contenido);
        $contenido = str_replace('@@usuario',$usuario,$contenido);
        $contenido = str_replace('@@telefono',$telefono,$contenido);
        $contenido = str_replace('@@email',$email,$contenido);
        $contenido = str_replace('@@pag_id',$pag_id,$contenido);
        $contenido = str_replace('@@monto',$monto,$contenido);
        $contenido = str_replace('@@tipo',$tipo,$contenido);
        $contenido = str_replace('@@forma',$forma,$contenido);
        $contenido = str_replace('@@fac_name',$facName,$contenido);
        $contenido = str_replace('@@fac_nit',$facNit,$contenido);

        $cue_nombre = $parametros->par_salida;
        $cue_pass = $parametros->par_pas_salida;

        $mail = new PHPMailer(true);
        $mail->SetLanguage('es');
        $mail->IsSMTP();
        $mail->SMTPAuth = true;
        $mail->Host = $parametros->par_smtp;
        $mail->Timeout=30;
        $mail->CharSet = 'utf-8';
        $mail->Subject = "Nuevo Pago #" . $pag_id;
        $body = $contenido;
        $mail->MsgHTML($body);
        $mail->AddReplyTo($email, $usuario);
        $paymentEmail = Private_Controller::getEmail('pagos');
        $mail->AddAddress($paymentEmail);
        $mail->SetFrom($cue_nombre, model_parametros::SITE_DOMAIN);
        $mail->Username = $cue_nombre;
        $mail->Password = $cue_pass;

        $mail->Send();
    }

    public static function send_mail_pago_notificacion($concepto, $usuario, $telefono, $userEmail, $pag_id, $monto, $tipo, $forma,$facName,$facNit)
    {
        $ci = &get_instance();
        $sendMessageResponse = array("response" => 0,"message" => 'Su mensaje no pudo ser enviado.');

        $template = base_url() . "assets/template_mail/mail_pago_notificacion_admin.html";
        $contenido = static::getContent($template);
        $contenido = str_replace('@@concepto',$concepto,$contenido);
        $contenido = str_replace('@@usuario',$usuario,$contenido);
        $contenido = str_replace('@@telefono',$telefono,$contenido);
        $contenido = str_replace('@@email',$userEmail,$contenido);
        $contenido = str_replace('@@pag_id',$pag_id,$contenido);
        $contenido = str_replace('@@monto',$monto,$contenido);
        $contenido = str_replace('@@tipo',$tipo,$contenido);
        $contenido = str_replace('@@forma',$forma,$contenido);
        $contenido = str_replace('@@fac_name',$facName,$contenido);
        $contenido = str_replace('@@fac_nit',$facNit,$contenido);
        $emailHandler = new EmailHandler();
        $email = $emailHandler->initialize();
        $email->from(EmailHandler::getSender(), 'ToqueElTimbre');
        $paymentEmail = Private_Controller::getEmail('pagos');
        $email->to(array($userEmail, $paymentEmail));
        $email->subject("Nuevo Pago #" . $pag_id);
        $email->message($contenido);
        try
        {
            if($email->Send())
            {
                $sendMessageResponse['response'] = 1;
                $sendMessageResponse['message'] = 'Su mensaje fue enviado correctamente';
            }
        }
        catch (Exception $e)
        {
            $sendMessageResponse['response'] = 0;
            $sendMessageResponse['message'] = $e->getMessage();
        }
        return $sendMessageResponse;
    }

    public function send_mail_codigo_pago_not_cliente_deprecated($inmueble, $email, $pag_id, $monto){
        require_once(BASE_DIRECTORY . DS . 'admin' . DS . 'clases' . DS. 'phpmailer' . DS . 'class.phpmailer.php');
        $parametros = $this->model_parametros->get_all();

        $template = base_url() . "assets/template_mail/codigo_pago_anuncios.html";
        $contenido = $this->obtener_contenido($template);
        $contenido = str_replace('@@inmueble',$concepto,$contenido);
        $contenido = str_replace('@@pag_id',$pag_id,$contenido);
        $contenido = str_replace('@@monto',$monto,$contenido);

        $cue_nombre = $parametros->par_salida;
        $cue_pass = $parametros->par_pas_salida;

        $mail = new PHPMailer(true);
        $mail->SetLanguage('es');
        $mail->IsSMTP();
        $mail->SMTPAuth = true;
        $mail->Host = $parametros->par_smtp;
        $mail->Timeout=30;
        $mail->CharSet = 'utf-8';
        $mail->Subject = "Nuevo Pago #" . $pag_id;
        $body = $contenido;
        $mail->MsgHTML($body);
        //$mail->AddReplyTo($email, $usuario);
        $mail->AddAddress($email);
        //$mail->AddAddress("cristian.inarra@gmail.com");
        $mail->SetFrom($cue_nombre, model_parametros::SITE_DOMAIN);
        $mail->Username = $cue_nombre;
        $mail->Password = $cue_pass;

        $mail->Send();
    }

    public static function send_mail_codigo_pago_not_cliente($inmueble, $userEmail, $pag_id, $monto)
    {
        $ci = &get_instance();
        $sendMessageResponse = array("response" => 0,"message" => 'Su mensaje no pudo ser enviado.');

        $template = base_url() . "assets/template_mail/codigo_pago_anuncios.html";
        $contenido = static::getContent($template);
        $contenido = str_replace('@@inmueble',$concepto,$contenido);
        $contenido = str_replace('@@pag_id',$pag_id,$contenido);
        $contenido = str_replace('@@monto',$monto,$contenido);

        $emailHandler = new EmailHandler();
        $email = $emailHandler->initialize();
        $email->from(EmailHandler::getSender(), 'ToqueElTimbre');
        $email->to($userEmail);
        $email->subject("Nuevo Pago #" . $pag_id);
        $email->message($contenido);
        try
        {
            if($email->Send())
            {
                $sendMessageResponse['response'] = 1;
                $sendMessageResponse['message'] = 'Su mensaje fue enviado correctamente';
            }
        }
        catch (Exception $e)
        {
            $sendMessageResponse['response'] = 0;
            $sendMessageResponse['message'] = $e->getMessage();
        }
        return $sendMessageResponse;
    }

    private function obtener_contenido($url)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.3) Gecko/20070309 Firefox/2.0.0.3");
        curl_setopt($curl, CURLOPT_URL, $url);
        $contenido = curl_exec($curl);
        curl_close($curl);

        return $contenido;
    }

    private static function getContent($url)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.3) Gecko/20070309 Firefox/2.0.0.3");
        curl_setopt($curl, CURLOPT_URL, $url);
        $contenido = curl_exec($curl);
        curl_close($curl);

        return $contenido;
    }

    public function tigomoney_deprecated(){

        if($_POST){
            $order_pay = $this->model_pagos->get_payment_detail_tigomoney($_POST['pag_id']);
            $url_return = base_url() . "dashboard/payment/ipn";

            $pv_nroDocumento= "pv_nroDocumento=".$order_pay->usu_ci;
            $pv_orderId="pv_orderId=".$order_pay->pag_id;
            $monto = round($order_pay->pag_monto, 0);
            $pv_monto="pv_monto=".$monto;
            //$pv_monto="pv_monto=1";
            $pv_linea="pv_linea=".$_POST['linea'];
            $pv_nombre="pv_nombre=".$order_pay->nombre_completo;
            $pv_urlCorrecto = "pv_urlCorrecto=".$url_return;
            $pv_urlError = "pv_urlError=".$url_return;
            $pv_confirmacion = "pv_confirmacion=Toqueeltimbre";
            $pv_notificacion = "pv_notificacion=Orden de Pago ".$order_pay->pag_id;
            $pv_items= "pv_items=*i1|1|Servicios de Toqueeltimbre|".$order_pay->pag_monto."|".$order_pay->pag_monto;
            $pv_razonSocial= "pv_razonSocial=".$order_pay->pag_nombre;
            $pv_nit= "pv_nit=".$order_pay->pag_nit;

            require BASE_DIRECTORY . DS . 'sintesis' . DS . 'tigomoney' . DS . 'CTripleDes.php';
            $montant = $pv_nroDocumento.";".$pv_orderId.";".$pv_monto.";".$pv_linea.";".$pv_nombre.";".$pv_urlCorrecto.";".$pv_urlError.";".$pv_confirmacion.";".$pv_notificacion.";".$pv_items.";".$pv_razonSocial.";".$pv_nit;

            $keyPublica = "cb6ce94b52d5fc21bfa35ad64baaff6301c154ac981180ec994d2ca64114e2899d21bb596011f52bb913b7ea3152d375490384bf7d99d3562dbf00021e826f1f";
            $keyPrivada = "PHF07Z8GAFBJTZAYBRGNCYN0";

            $tripleDes = new CTripleDes();
            $tripleDes->setMessage($montant);
            $tripleDes->setPrivateKey($keyPrivada);
            $encrypt = $tripleDes->encrypt();

            header("Location: https://pasarela.tigomoney.com.bo/vipagos/faces/payment.xhtml?key=".$keyPublica."&parametros=".$encrypt);
        }else{
            $this->load->view("backend/payment/iframe_tigomoney", array());
        }
    }


    public function tigomoney(){

        if($_POST){
            $order_pay = $this->model_pagos->get_payment_detail_tigomoney($_POST['pag_id']);
            $url_return = base_url() . "dashboard/payment/ipn";

            $pv_nroDocumento= "pv_nroDocumento=".$order_pay->usu_ci;
            $pv_orderId="pv_orderId=".$order_pay->pag_id;
            $monto = round($order_pay->pag_monto, 0);
            $pv_monto="pv_monto=".$monto;
            //$pv_monto="pv_monto=1";
            $pv_linea="pv_linea=".$_POST['linea'];
            $pv_nombre="pv_nombre=".$order_pay->nombre_completo;
            $pv_urlCorrecto = "pv_urlCorrecto=".$url_return;
            $pv_urlError = "pv_urlError=".$url_return;
            $pv_confirmacion = "pv_confirmacion=Toqueeltimbre";
            $pv_notificacion = "pv_notificacion=Orden de Pago ".$order_pay->pag_id;
            $pv_items= "pv_items=*i1|1|Servicios de Toqueeltimbre|".$order_pay->pag_monto."|".$order_pay->pag_monto;
            $pv_razonSocial= "pv_razonSocial=".$order_pay->pag_nombre;
            $pv_nit= "pv_nit=".$order_pay->pag_nit;

            //TODO:Here goes the new tigoMoney Library
            $orderId = $order_pay->pag_id;
            $amount = round($order_pay->pag_monto, 0);
            $cellNumber = $_POST['linea'];
            $confirmationMessage = "ToqueElTimbre";
            $notificationMessage = "Orden de Pago ".$order_pay->pag_id;
            $items = "*i1|1|Servicios de Toqueeltimbre|".$order_pay->pag_monto."|".$order_pay->pag_monto;
            $legalName = $order_pay->pag_nombre;
            $nit = $order_pay->pag_nit;
            require BASE_DIRECTORY . DS . 'application' . DS .'libraries'.DS. 'TigoMoneyHandler.php';
            $tigoMoneyHandler = new TigoMoneyHandler();
            $tigoMoneyHandler->requestPayment($orderId, $amount, $cellNumber, $confirmationMessage, $notificationMessage, $items, $legalName, $nit);
            $response =     $tigoMoneyHandler->getPaymentStatus($orderId);
            var_dump($response->getMessage(), $response->getData(), $response->isSuccessful(), $response->getTigoTransactionReference());exit;
            /*
            require BASE_DIRECTORY . DS . 'sintesis' . DS . 'tigomoney' . DS . 'CTripleDes.php';
            $montant = $pv_nroDocumento.";".$pv_orderId.";".$pv_monto.";".$pv_linea.";".$pv_nombre.";".$pv_urlCorrecto.";".$pv_urlError.";".$pv_confirmacion.";".$pv_notificacion.";".$pv_items.";".$pv_razonSocial.";".$pv_nit;

            $keyPublica = "cb6ce94b52d5fc21bfa35ad64baaff6301c154ac981180ec994d2ca64114e2899d21bb596011f52bb913b7ea3152d375490384bf7d99d3562dbf00021e826f1f";
            $keyPrivada = "PHF07Z8GAFBJTZAYBRGNCYN0";

            $tripleDes = new CTripleDes();
            $tripleDes->setMessage($montant);
            $tripleDes->setPrivateKey($keyPrivada);
            $encrypt = $tripleDes->encrypt();

            header("Location: https://pasarela.tigomoney.com.bo/vipagos/faces/payment.xhtml?key=".$keyPublica."&parametros=".$encrypt);
            */
        }else{
            $this->load->view("backend/payment/iframe_tigomoney", array());
        }
    }


    public function ipn(){
        $data['user_info'] = $this->model_usuario->get_info_agent($this->user_id);

        $this->load->model('model_inmueble');
        $this->load->model('model_suscripcion');
        $values = $this->get_tigomoney_decrypt($_GET['r']);
        $sw_result = $values['codRes'];

        if($sw_result == 0){

            $id_pago = trim($values['orderId']);
            $obj_paid = $this->model_pagos->get_id_paid($id_pago);
            $sw_pago = $this->model_pagos->verify_id_paid($id_pago);

            if(!$sw_pago){
                $codigos_servicios = $this->model_pagos->get_services($id_pago);
                $email = $this->model_pagos->get_email_idpag($id_pago);
                $fecha_pagado = date("Y-m-d H:i:s");

                $pago_values = array(
                    "pag_estado" => 'Pagado',
                    'pag_entidad' => 'TigoMoney',
                    'pag_fecha_pagado' => $fecha_pagado
                );

                $this->model_pagos->update($id_pago, $pago_values);

                if($obj_paid->pag_concepto == "Suscripcion"){
                    $total = 0;
                    foreach($codigos_servicios as $row){
                        if($row->ser_id != ""){
                            $obj_servicio = $this->model_servicios->get_id($row->ser_id);
                            $fech_inicio = date("Y-m-d");
                            $fec_vencimi = date("Y-m-d", strtotime($fech_inicio. " + " . $obj_servicio->ser_dias . " days"));

                            $pag_service_values = array(
                                'fech_ini' => $fech_inicio,
                                'fech_fin' => $fec_vencimi
                            );

                            $this->model_pago_servicio->update($id_pago, $row->ser_id, $pag_service_values);
                        }
                    }
                    $obj_values = array(
                        'fecha_inicio' => $fech_inicio,
                        'fecha_fin' => $fec_vencimi,
                        'estado' => "Activo"
                    );
                    $this->model_suscripcion->update_by_pag_id($id_pago, $obj_values);
                    $this->pay_publications_pending_suscription($this->usu_id);

                    $total_pagado = $this->model_pagos->get_total_paid($id_pago);
//                    $this->enviar_mail($total_pagado, $email, "Pago por suscripcion");
                    static::enviar_mail($total_pagado, $email, "Pago por suscripcion");
                }else{
                    $total = 0;
                    foreach($codigos_servicios as $row){
                        if($row->ser_id != ""){
                            $obj_servicio = $this->model_servicios->get_id($row->ser_id);
                            $id_pub = $this->model_pagos->get_id_publication($id_pago);

                            $fecha_fin_anterior_pago = $this->model_pagos->verify_exist_other_paids($id_pago, $id_pub, $obj_servicio->ser_id);
                            if(!empty($fecha_fin_anterior_pago)){
                                $fech_inicio = $fecha_fin_anterior_pago;
                            }else{
                                $fech_inicio = date("Y-m-d");
                            }

                            $fec_vencimi = date("Y-m-d", strtotime($fech_inicio. " + " . $obj_servicio->ser_dias . " days"));
                            $pag_service_values = array(
                                'fech_ini' => $fech_inicio,
                                'fech_fin' => $fec_vencimi
                            );
                            $this->model_pago_servicio->update($id_pago, $row->ser_id, $pag_service_values);

                            $total = $total + $obj_servicio->ser_precio;
                        }
                    }
                    $publicacion = $this->model_pagos->get_publication($id_pago);
                    $mayor_fecha = $this->model_pagos->get_max_date_service($publicacion->pub_id);
                    if($mayor_fecha > $publicacion->pub_vig_fin){
                        $pub_vig_fin = $mayor_fecha;
                        $pub_values = array(
                            'pub_estado' => 'Aprobado',
                            'pub_vig_fin' => $pub_vig_fin
                        );
                    }else{
                        $pub_values = array(
                            'pub_estado' => 'Aprobado'
                        );
                    }
                    $this->model_publicacion->update($publicacion->pub_id, $pub_values);
                    $obj_inmueble = $this->model_pagos->get_id_inmueble($publicacion->pub_id);
                    $this->model_inmueble->update($obj_inmueble->inm_id, array('inm_publicado' => 'Si'));

                    $total_pagado = $this->model_pagos->get_total_paid($id_pago);
//                    $this->enviar_mail($total_pagado, $email, $obj_inmueble->inm_nombre);
                    static::enviar_mail($total_pagado, $email, $obj_inmueble->inm_nombre);
                }

                $data['pagoId'] = $id_pago;
                $data['totalPayment'] = $total_pagado;
                $data['paymentType'] = "TIGOMONEY";
                $data['errorSintesis'] = "No";

                $this->load_template_backend('backend/payment/payment_confirmationtigo', $data);
            }else{
                $id_pago = trim($values['orderId']);
                $total_pagado = $this->model_pagos->get_total_paid($id_pago);

                $data['pagoId'] = $id_pago;
                $data['totalPayment'] = $total_pagado;
                $data['paymentType'] = "TIGOMONEY";
                $data['errorSintesis'] = "No";

                $this->load_template_backend('backend/payment/payment_confirmationtigo', $data);
            }

        }else{
            $data["result"] = $sw_result;
            $this->load_template_backend('backend/payment/payment_failed_tigomoney', $data);
        }

    }


    // Esta funcion se utiliza cuando se verifica el pago de la suscripcion
    public function pay_publications_pending_suscription($id_user){
        $payments = $this->model_pagos->get_payment_pending_subscription_by_user($id_user);

        if(count($payments) > 0){
            foreach($payments as $obj_pago){
                $id_pago = $obj_pago->pag_id;
                $codigos_servicios = $this->model_pagos->get_services($id_pago);
                $fecha_pagado = date("Y-m-d H:i:s");

                $pago_values = array(
                    "pag_estado" => 'Pagado',
                    'pag_monto' => '0',
                    'pag_entidad' => 'Suscripcion',
                    'pag_fecha_pagado' => $fecha_pagado
                );

                $this->model_pagos->update($id_pago, $pago_values);

                $total = 0;
                foreach($codigos_servicios as $row){
                    if($row->ser_id != ""){
                        $obj_servicio = $this->model_servicios->get_id($row->ser_id);
                        $id_pub = $this->model_pagos->get_id_publication($id_pago);

                        $fecha_fin_anterior_pago = $this->model_pagos->verify_exist_other_paids($id_pago, $id_pub, $obj_servicio->ser_id);
                        if(!empty($fecha_fin_anterior_pago)){
                            $fech_inicio = $fecha_fin_anterior_pago;
                        }else{
                            $fech_inicio = date("Y-m-d");
                        }

                        $fec_vencimi = date("Y-m-d", strtotime($fech_inicio. " + " . ($obj_servicio->ser_dias) . " days"));
                        $pag_service_values = array(
                            'fech_ini' => $fech_inicio,
                            'fech_fin' => $fec_vencimi
                        );
                        $this->model_pago_servicio->update($id_pago, $row->ser_id, $pag_service_values);

                        $total = $total + $obj_servicio->ser_precio;
                    }
                }
                $publicacion = $this->model_pagos->get_publication($id_pago);
                $mayor_fecha = $this->model_pagos->get_max_date_service($publicacion->pub_id);
                if($mayor_fecha > $publicacion->pub_vig_fin){
                    $pub_vig_fin = $mayor_fecha;
                    $pub_values = array(
                        'pub_estado' => 'Aprobado',
                        'pub_vig_fin' => $pub_vig_fin
                    );
                }else{
                    $pub_values = array(
                        'pub_estado' => 'Aprobado'
                    );
                }
                $this->model_publicacion->update($publicacion->pub_id, $pub_values);
                $obj_inmueble = $this->model_pagos->get_id_inmueble($publicacion->pub_id);
                $this->model_inmueble->update($obj_inmueble->inm_id, array('inm_publicado' => 'Si'));
            }
        }

    }


    function get_tigomoney_decrypt($value){
        require BASE_DIRECTORY . DS .'sintesis' . DS . 'tigomoney' . DS . 'CTripleDes.php';

        $keyPublica = "cb6ce94b52d5fc21bfa35ad64baaff6301c154ac981180ec994d2ca64114e2899d21bb596011f52bb913b7ea3152d375490384bf7d99d3562dbf00021e826f1f";
        $keyPrivada = "PHF07Z8GAFBJTZAYBRGNCYN0";

        $value = urldecode($value);
        $value = str_replace(" ", "+", $value);

        //desencriptacion
        $tripleDes2 = new CTripleDes();
        $tripleDes2->setMessage_to_decrypt($value);
        $tripleDes2->setPrivateKey($keyPrivada);
        $decrypt = $tripleDes2->decrypt();
        $decrypt = explode("&", $decrypt);

        foreach($decrypt as $value){
            $aux = explode("=", $value);
            $valores[$aux[0]] = $aux[1];
        }
        return $valores;
    }

    function enviar_mail_deprecated($total,$email,$inmueble)	{
        require_once(BASE_DIRECTORY . DS . 'admin' . DS . 'clases' . DS. 'phpmailer' . DS . 'class.phpmailer.php');
        $parametros = $this->model_parametros->get_all();

        $template = base_url() . "assets/template_mail/confirmacion_pago.html";
        $contenido = $this->obtener_contenido($template);

        $contenido = str_replace('@@monto',$total,$contenido);
        $contenido = str_replace('@@inmueble',$inmueble,$contenido);

        $cue_nombre = $parametros->par_salida;
        $cue_pass = $parametros->par_pas_salida;

        $mail = new PHPMailer(true);
        $mail->SetLanguage('es');
        $mail->IsSMTP();
        $mail->SMTPAuth = true;
        $mail->Host = $parametros->par_smtp;
        $mail->Timeout=30;
        $mail->CharSet = 'utf-8';
        $mail->Subject = "Pago Confirmado";
        $body = $contenido;
        $mail->MsgHTML($body);
        $mail->AddAddress($email);
        //$mail->AddAddress("cristian.inarra@gmail.com");
        $mail->SetFrom($cue_nombre, model_parametros::SITE_DOMAIN);
        $mail->Username = $cue_nombre;
        $mail->Password = $cue_pass;

        $mail->Send();
    }

    public static function enviar_mail($total,$userEmail,$inmueble)
    {
        $ci = &get_instance();
        $sendMessageResponse = array("response" => 0,"message" => 'Su mensaje no pudo ser enviado.');

        $template = base_url() . "assets/template_mail/confirmacion_pago.html";
        $contenido = static::getContent($template);
        $contenido = str_replace('@@monto',$total,$contenido);
        $contenido = str_replace('@@inmueble',$inmueble,$contenido);

        $emailHandler = new EmailHandler();
        $email = $emailHandler->initialize();
        $email->from(EmailHandler::getSender(), 'ToqueElTimbre');
        $email->to($userEmail);
        $email->subject("Pago Confirmado");
        $email->message($contenido);
        try
        {
            if($email->Send())
            {
                $sendMessageResponse['response'] = 1;
                $sendMessageResponse['message'] = 'Su mensaje fue enviado correctamente';
            }
        }
        catch (Exception $e)
        {
            $sendMessageResponse['response'] = 0;
            $sendMessageResponse['message'] = $e->getMessage();
        }
        return $sendMessageResponse;
    }

    public function validar_cupon(){
        $this->load->model('model_cupon');
        $this->load->model('model_cupon_usuario');
        $cupon = $this->input->post('cupon');

        $id_cupon = $this->model_cupon->is_valid($cupon);
        if(!empty($id_cupon)){
            $is_expired = $this->model_cupon->is_expired($cupon);
            if($is_expired){
                $is_assigned = $this->model_cupon_usuario->is_assigned($id_cupon, $this->user_id);
                if(!empty($is_assigned)){
                    $is_cobrado = $this->model_cupon_usuario->is_cobrado($id_cupon, $this->user_id);
                    if(!$is_cobrado){
                        $values = array("error" => 0, "data" => $is_assigned);
                    }else{
                        $values = array("error" => -4, "data" => "");
                    }
                }else{
                    // No esta asignado al usuario
                    $values = array("error" => -3, "data" => "");
                }
            }else{
                // Esta expirado
                $values = array("error" => -2, "data" => "");
            }
        }else{
            // Cupon no es valido
            $values = array("error" => -1, "data" => "");
        }
        echo json_encode($values);
    }

    public function validar_cupon_post($cupon){
        $this->load->model('model_cupon');
        $this->load->model('model_cupon_usuario');

        $id_cupon = $this->model_cupon->is_valid($cupon);
        if(!empty($id_cupon)){
            $is_expired = $this->model_cupon->is_expired($cupon);
            if($is_expired){
                $is_assigned = $this->model_cupon_usuario->is_assigned($id_cupon, $this->user_id);
                if(!empty($is_assigned)){
                    $is_cobrado = $this->model_cupon_usuario->is_cobrado($id_cupon, $this->user_id);
                    if(!$is_cobrado){
                        $values = array("error" => 0, "data" => $is_assigned);
                    }else{
                        $values = array("error" => -4, "data" => "");
                    }
                }else{
                    // No esta asignado al usuario
                    $values = array("error" => -3, "data" => "");
                }
            }else{
                // Esta expirado
                $values = array("error" => -2, "data" => "");
            }
        }else{
            // Cupon no es valido
            $values = array("error" => -1, "data" => "");
        }
        return json_encode($values);
    }

}