<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Christian
 * Date: 18/11/2014
 * Time: 10:24
 */

class Panel extends Private_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->model('model_usuario');
        $this->load->model('model_publicacion');
        $this->load->model('model_proyecto');
        $this->load->model('model_inmueble');
        $this->load->model('visitor');
        $this->load->model('model_categoria');

        $this->load->library('image_lib');

        $this->page = 'profile';
    }

    public function index()
    {
        $data['breadcrumb'] = $this->breadcrumb;
        $data['user_info'] = $this->model_usuario->get_info_agent($this->user_id);
        $this->breadcrumb['level2'] = array('name' => 'Panel', 'link' => base_url().'dashboard');
        $this->page_title = 'Panel de Administracion';
        $this->page = 'home';


        $user = $data['user_info'];
        $box1 = $box2 = $box3 = array(
            'info' => "0",
            'text' => "toque el timbre"
        );

        if($user->usu_tipo == 'Inmobiliaria' && $this->sw_subscription)
        {
            $publications = $this->model_publicacion->get_active_publications($user->usu_id);
            $publicationsActives = $publications;
            $data['publications'] = $publications;
            $x = count($publications);
            $box1 = array(
                'info' => $x,
                'text' => "Anuncios Publicados"
            );

            $y = $this->limit_publications - $x;
            $y = $y >= 0 ? $y : 0;
            $box2 = array(
                'info' => $y,
                'text' => "Anuncios Restantes"
            );

            $suscripcion = $this->model_suscripcion->get_suscription_by_userId($user->usu_id);

            $box3 = array(
                'info' => $suscripcion->fecha_fin,
                    'text' => "Expira tu suscripcion"
            );

            // bar chart total visits
            $totalVisits = $this->getTotalVisitors($publicationsActives);

            // publications created
            $totalCreatedByDay = $this->getTotalCreadedByDay($this->model_categoria->get_all_categories());

            // visits by day in the publications
            $visitors = $this->visitor->get_all_visitor_by_user_id('inmueble', $user->usu_id);

	        $allVisitorsData = $this->getGraphicalChartDays($visitors);
            // Pie chart
            $pieChartObjects = $this->getPieChartOffer($this->model_categoria->get_all_categories());
            $data['categoriesPieOffer'] = $pieChartObjects;
            $pieChartObjects = $this->getPieChartDemand($this->model_categoria->get_all_categories());
            $data['categoriesPieDemand'] = $pieChartObjects;

        }
        elseif($user->usu_tipo == 'Constructora')
        {
	        $x = $this->model_proyecto->get_project_by_userId($user->usu_id);
            $box1 = array(
                'info' => $x[0]->proy_visitas,
                'text' => "Visitas a tu(s) proyecto(s)"
            );

            $projects = $this->model_proyecto->get_project_by_userId($user->usu_id);
            $project = array_shift($projects);

            $project->proy_vig_fin = $project->proy_vig_fin != "0000-00-00" ? $project->proy_vig_fin : '-';
            $box2 = array(
                'info' => $project->proy_vig_fin,
                'text' => "Finaliza su proyecto"
            );
            // bar chart total visits
            $publicationsActives = $this->model_publicacion->get_active_publications($user->usu_id);
            $totalVisits = $this->getTotalVisitors($publicationsActives);

            // publications created
            $totalCreatedByDay = $this->getTotalCreadedByDay($this->model_categoria->get_all_categories());

            // visits by day in the publications
            $visitors = $this->visitor->get_all_visitor_by_user_id('Proyecto', $user->usu_id);
            $allVisitorsData = $this->getGraphicalChartDays($visitors);

            // Pie chart
            $pieChartObjects = $this->getPieChartOffer($this->model_categoria->get_all_categories());
            $data['categoriesPieOffer'] = $pieChartObjects;
            $pieChartObjects = $this->getPieChartDemand($this->model_categoria->get_all_categories());
            $data['categoriesPieDemand'] = $pieChartObjects;


        } else {
            $publications = $this->model_publicacion->get_active_publications($user->usu_id);
            $publicationsActives = $publications;
            $data['publications'] = $publications;

            $x = count($publications);
            $box1 = array(
                'info' => $x,
                'text' => "Anuncios Publicados"
            );

            $publications = $this->model_publicacion->get_publications_ended_this_week($user->usu_id);
            $x = count($publications);
            $box2 = array(
                'info' => $x,
                'text' => "Anuncios Finalizan esta semana"
            );

            // bar chart total visits
            $totalVisits = $this->getTotalVisitors($publicationsActives);

            // publications created
            $totalCreatedByDay = $this->getTotalCreadedByDay($this->model_categoria->get_all_categories());

            // visits by day in the publications
            $visitors = $this->visitor->get_all_visitor_by_user_id('inmueble', $user->usu_id);
            $allVisitorsData = $this->getGraphicalChartDays($visitors);
            // Pie chart
            /*if($user->usu_tipo == 'Inmobiliaria') {
				$pieChartObjects = $this->getPieChartOffer($this->model_categoria->get_all_categories());
                $data['categoriesPieOffer'] = $pieChartObjects;
                $pieChartObjects = $this->getPieChartDemand($this->model_categoria->get_all_categories());
                $data['categoriesPieDemand'] = $pieChartObjects;
            }*/
        }

        $data["box1"] = $box1;
        $data["box2"] = $box2;
        $data['box3'] = $box3;

        $data['suscripcion'] = $this->sw_subscription;

        $data['visitors'] = $allVisitorsData;
        $data['visitorsBar'] = $totalVisits;
        $data['publicationsCreated'] = $totalCreatedByDay;

        $this->load_template_backend('backend/panel', $data);
    }

    private function getPieChartOffer($categories = array()) {
        $pieChartCategories = array();
        foreach($categories as $category) {
            $pieObject = new stdClass();
            $pieObject->label = $category->cat_nombre;
            $pieObject->data = count($this->model_publicacion->get_publications_by_type($category->cat_nombre));
            $pieChartCategories[] = $pieObject;
        }

        return $pieChartCategories;
    }

    private function getPieChartDemand($categories = array()) {
        $pieChartCategories = array();
        foreach($categories as $category) {
            $pieObject = new stdClass();
            $pieObject->label = $category->cat_nombre;

            $publications = $this->model_publicacion->get_sum_total_by_type($category->cat_nombre);
            $total = 0;
            foreach ($publications as $publication) {
                $total += $publication->inm_visitas;
            }
            $pieObject->data = $total;
            $pieChartCategories[] = $pieObject;
        }
        // debug_pre($this->db->last_query());
        return $pieChartCategories;
    }

    private function getTotalCreadedByDay($categories = array()) {
        $totalCreated = array();
        foreach($categories as $category) {
            $data = $this->get_publications_created($category->cat_nombre);
            $createdChartObject = new stdClass();
            $createdChartObject->label = $category->cat_nombre;
            $createdChartObject->data = $data;

            $totalCreated[] = $createdChartObject;
        }
        return $totalCreated;
    }

    private function get_publications_created($category = "") {
        /*data:[["25",0],["26",0],["27",0],["28",0],["01",0],["02",0],["03",0],["04",0],["05",0],["06",0],["07",0],["08",0],["09","1"],["10","5"],["11",0],["12","21"]]*/

        $today = date('Y-m-d');
        $startDate = date('Y-m-d', strtotime ('-10 day', strtotime(date('Y-m-d'))));
        $arrayDays = array();
        while ($startDate <= $today) {
            $arrayDays[] = array($startDate, 0);
            $startDate = date('Y-m-d', strtotime ("+1 day", strtotime($startDate)));
        }

        $arrayDaysAux = array();
        foreach($arrayDays as $day) {
            $day[1] = count($this->model_publicacion->get_publications_created_by_Category($category, $day[0]));
            $day[0] = date("d", strtotime($day[0]));
            $arrayDaysAux[] = $day;
        }
        return $arrayDaysAux;
    }

    private function getTotalVisitors($publications = array()) {
        $totalVisits = array();
        foreach($publications as $publication) {
            $totalVisits[] = array(
                "id" => $publication->pub_id,
                "name" => $publication->inm_nombre,
                "visits" => $publication->inm_visitas
            );
        }

        return $totalVisits;
    }

    private function getGraphicalChartDays($visitors = array()) {
        $allVisitorsData = array();
        foreach($visitors as $visitor) {
            // $ejeX = date("d", strtotime($visitor->visitor_date));
            $ejeX = $visitor->visitor_date;
            $ejeY = $visitor->count;

            $allVisitorsData[$visitor->type_id][] = array($ejeX, $ejeY);
        }

        $typeofVisitor = $visitor->type;

        foreach($allVisitorsData as $obj_id => $arrayValue) {
            $arrayValueCopy = $arrayValue;

            $currentDate = date('Y-m-d');
            $startDate = date('Y-m-d', strtotime ('-14 day', strtotime(date('Y-m-d'))));
            $arrayDays = array();
            while ($startDate <= $currentDate) {
                $arrayDays[] = array($startDate, 0);
                $startDate = date('Y-m-d', strtotime ("+1 day", strtotime($startDate)));
            }

            foreach($arrayValueCopy as $value) {
                foreach($arrayDays as $key => $arrayDay) {
                    if($value[0] == $arrayDay[0]){
                        $arrayDay[1] = $value[1];
                        $arrayDays[$key] = $arrayDay;
                    }
                }
            }

            $last_array = array();
            foreach($arrayDays as $value) {
                $day = date("d", strtotime($value[0]));
                $value[0] = $day;
                $last_array[] = $value;
            }

            $label = "";
            if ($typeofVisitor == "Inmueble") {
                $inmu = $this->model_inmueble->get_all($obj_id);
                $label = $inmu['inm_nombre'];

            } elseif ($typeofVisitor == "Proyecto") {
                $proy = $this->model_proyecto->get_all($obj_id);
                $label = $proy['proy_nombre'];

            } elseif ($typeofVisitor == "User Inmobiliaria") {
                $label = "user inmobiliaria";
            }

            $l = 40;
            $label = strlen($label) >= $l ? substr($label, 0, $l)."...": $label;

            $info = new stdClass();
            $info->label = $label;
            $info->data = $last_array;

            $totalVisitors[] = $info;
        }
        $allVisitorsData = $totalVisitors;
        return $allVisitorsData;
    }

    public function profile(){
        $this->breadcrumb['level2'] = array('name' => 'Mi Cuenta', 'link' => base_url().'dashboard/edit-profile');
        $this->page_title = 'Mi Cuenta';

        if($_POST){
            $tab_position = $this->input->post('tab_position');
            switch($tab_position){
                case 'user':
                    $this->form_validation->set_rules('first_name', 'Nombre', 'required');
                    $this->form_validation->set_rules('last_name', 'Apellido', 'required');
                    $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
                    $this->form_validation->set_rules('phone', 'Telefono', 'numeric');
                    $this->form_validation->set_rules('cellphone', 'Celular', 'numeric');

                    $this->form_validation->set_message('required', 'El campo %s es requerido.');
                    $this->form_validation->set_message('valid_email', 'El email no es valido.');
                    $this->form_validation->set_message('numeric', 'Ingrese valores numericos en el campo %s.');

                    if ($this->form_validation->run() == TRUE) {
                        $email = $this->input->post('email');
                        $verify = $this->model_usuario->email_verify($email, $this->user_id);
                        if (!$verify) {
                            $this->edit_user();
                            $data['ok_update'] = 'Datos guardados correctamente.';
                        } else {
                            $data['error_update'] = 'El email ya se encuentra en uso.';
                        }
                    }
                break;
                case 'company':
                    $this->form_validation->set_rules('ci_nit', 'NIT', 'numeric');

                    $this->form_validation->set_message('numeric', 'Ingrese valores numericos en el campo %s.');

                    if ($this->form_validation->run() == TRUE) {
                        $this->edit_company();
                        $data['ok_update'] = 'Datos guardados correctamente.';
                    }
                break;
                case 'change_image':
                    $this->change_image();
                    $data['ok_update'] = 'Imagen guardada correctamente.';
                break;
                case 'change_password':
                    $this->form_validation->set_rules('password', 'Contraseña', 'matches[repassword]');
                    $this->form_validation->set_rules('repassword', 'Repetir Contraseña');
                    $this->form_validation->set_message('matches', 'Las contraseñas deben coincidir.');

                    if ($this->form_validation->run() == TRUE) {
                        $this->change_password();
                        $data['ok_update'] = 'Su contraseña fue modificada correctamente.';
                    }
                break;
                case 'notifications':
                    $this->notifications();
                    $data['ok_update'] = 'Datos guardados correctamente.';
                break;
            }
            $data['position'] = $tab_position;

        }

        $data['user_info'] = $this->model_usuario->get_info_agent($this->user_id);
        $data['breadcrumb'] = $this->breadcrumb;
        $this->load_template_backend('backend/profile/edit', $data);
    }

    private function delete_image($filename){
        $path = realpath(BASE_DIRECTORY. '/assets/uploads/users/' . $filename);
        //$path_small = realpath(BASE_DIRECTORY. '/assets/uploads/users/small/' . $filename);
        if(file_exists($path) && $filename != ""){
            unlink($path);
            //unlink($path_small);
        }
    }

    private function edit_user(){
        $nombre = $this->input->post('first_name');
        $apellido = $this->input->post('last_name');
        $email = $this->input->post('email');
        $telefono = $this->input->post('phone');
        $celular = $this->input->post('cellphone');
        $ciudad = $this->input->post('city');
        $tipo = $this->input->post('inmobiliaria');

        if(empty($tipo)){
            $values = array(
                'usu_nombre' => $nombre,
                'usu_apellido' => $apellido,
                'usu_email' => $email,
                'usu_telefono' => $telefono,
                'usu_celular' => $celular,
                'usu_ciu_id' => $ciudad
            );
        }else{
            $values = array(
                'usu_nombre' => $nombre,
                'usu_apellido' => $apellido,
                'usu_email' => $email,
                'usu_telefono' => $telefono,
                'usu_celular' => $celular,
                'usu_ciu_id' => $ciudad,
                'usu_tipo' => $tipo
            );
        }

        $this->model_usuario->update($this->user_id, $values);
    }

    private function edit_company(){
        $empresa = $this->input->post('company');
        $nit = $this->input->post('ci_nit');
        $direccion = $this->input->post('address');
        $descripcion = $this->input->post('description');
        $skype = $this->input->post('skype');
        $facebook = $this->input->post('facebook');
        $twitter = $this->input->post('twitter');

        $latitude = $this->input->post('latitude');
        $longitude = $this->input->post('longitude');

        $values = array(
            'usu_empresa' => $empresa,
            'usu_ci' => $nit,
            'usu_direccion' => $direccion,
            'usu_descripcion' => $descripcion,
            'usu_skype' => $skype,
            'usu_facebook' => $facebook,
            'usu_twitter' => $twitter,
            'usu_latitud' => $latitude,
            'usu_longitud' => $longitude
        );
        $this->model_usuario->update($this->user_id, $values);
    }

    private function change_image(){
        if(!empty($_FILES['userfile']['name'])) {
            $image = $this->do_upload();

            if($image["upload"] == FALSE){
                $data["error_profile_image"] = $image["error"];
            }else{
                $filename = $image["filename"];
                //$this->do_resize($filename);
            }
            $values = array(
                'usu_foto' => $filename
            );
            $this->model_usuario->update($this->user_id, $values);
        }
    }

    private function change_password(){
        $new_password = $this->input->post('password');
        $repeat_password = $this->input->post('repassword');

        $values = array(
            'usu_password' => sha1($new_password)
        );
        $this->model_usuario->update($this->user_id, $values);
    }

    private function notifications(){
        $datos_contacto = $this->input->post('datos_contacto');
        $email_reporte = $this->input->post('email_reporte');

        $values = array(
            'usu_dat_contacto' => $datos_contacto,
            'usu_email_reporte' => $email_reporte
        );
        $this->model_usuario->update($this->user_id, $values);
    }

    private function do_upload(){

        $filename = date("Y_m_d_H_i_s_").rand();
        $config['file_name'] = $filename;
        $config['upload_path'] = realpath(BASE_DIRECTORY. '/assets/uploads/users/');
        $config['overwrite'] = FALSE;
        $config['encrypt_name'] = FALSE;
        $config["allowed_types"] = 'jpg|jpeg|png|gif';
        $config["max_size"] = 4096;
        $this->load->library('upload', $config);

        if(!$this->upload->do_upload()) {
            $error = $this->upload->display_errors();
            return array(
                "upload" => FALSE,
                "filename" => "",
                "error" => $error
            );
        }else{
            $aux = $this->upload->data();
            return array(
                "upload" => TRUE,
                "filename" => $aux["file_name"],
                "error" => ""
            );
        }
    }

    private function do_resize($filename){
        $source_path = realpath(BASE_DIRECTORY. '/assets/uploads/users/'.$filename);
        $target_path = realpath(BASE_DIRECTORY. '/assets/uploads/users/small/');

        $config= array(
            'image_library' => 'gd2',
            'source_image' => $source_path,
            'new_image' => $target_path,
            'maintain_ratio' => TRUE,
            'thumb_marker' => '',
            'width' => 400,
            'height' => 400
        );

        $this->load->library('image_lib', $config);
        $this->image_lib->clear();
        if ( ! $this->image_lib->resize())
        {
            //echo $this->image_lib->display_errors();
        }
    }

}