<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Christian
 * Date: 20/11/2014
 * Time: 11:46
 */

class Auth extends Base_page
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('model_usuario');
        $this->load->library('session');
    }

    public function facebook()
    {
        $this->load->helper('url_helper');
        $this->load->spark('oauth2/0.4.0');

        $permisos = 'email';
        $provider = $this->oauth2->provider("facebook", array(
            'id' => '1589580387930226',
            'secret' => '2610e3afc83aedae025b58adeaf682bd',
            'scope' => $permisos
        ));
        $url_new_publication = base_url(). "dashboard/publications/register";
        if(!empty($_GET['url_redirect'])){
            $this->session->set_userdata('url_redirect', $url_new_publication);
        }else{
            $url_redirect = $this->session->userdata('url_redirect');
            if(empty($url_redirect)){
                $this->session->unset_userdata('url_redirect');
            }
        }

        if (!$this->input->get('code')) {
            redirect($provider->authorize());
        }else{
            try{
                $token = $provider->access($_GET['code']);
                $user = $provider->get_user_info($token);
                unset($_COOKIE['logged_user']);
                setcookie('logged_user', null, -1, '/');

                if(!empty($user)){
                    $login_user = $this->model_usuario->authentification_by_social_network($user['uid'], $user['email']);
                    if(!empty($login_user)){
                        //$this->session->set_userdata('logged_user', $login_user);

                        setcookie('logged_user', serialize($login_user), $this->expire_cookie,'/', 'toqueeltimbre.com');
                        $url_redirect = $this->session->userdata('url_redirect');
                        if(!empty($url_redirect)){
                            redirect($url_redirect);
                        }else{
                            redirect('dashboard');
                        }
                    }else{
                        // Registramos al usuario sino esta en la base de datos
                        $first_name = $user['first_name'];
                        $last_name = $user['last_name'];
                        $email = $user['email'];
                        if(empty($email)){
                            redirect('login?error=facebook');
                        }
                        $uid = $user['uid'];
                        //redirect('registrarse?first_name='.$first_name.'&last_name='.$last_name.'&uid='.$uid.'&email='.$email.'&social=Facebook');
                        $this->register_user($first_name, $last_name, $email, $uid, "Facebook");

                        $login_user = $this->model_usuario->authentification_by_social_network($uid, $email);
                        setcookie('logged_user', serialize($login_user), $this->expire_cookie,'/', 'toqueeltimbre.com');
                        redirect('dashboard');
                    }
                }else{
                    // Error no se pudo obtener usuario
                    redirect('login?error=facebook');
                }


            }catch (OAuth2_Exception $e){
                show_error('That didnt work: '.$e);
            }

        }
    }

    public function google()
    {
        $this->load->helper('url_helper');
        $this->load->spark('oauth2/0.4.0');

        $provider = $this->oauth2->provider("google", array(
            'id' => '551862188755-fvkp4kbk6v2bojela2ands6hrlu6bd8h.apps.googleusercontent.com',
            'secret' => 'm2_uU3dNG7jHtqSYWfZ0tD8g',
            'redirectUri' => base_url().'auth/google'
        ));

        $url_new_publication = base_url(). "dashboard/publications/register";
        if(!empty($_GET['url_redirect'])){
            $this->session->set_userdata('url_redirect', $url_new_publication);
        }else{
            $url_redirect = $this->session->userdata('url_redirect');
            if(empty($url_redirect)){
                $this->session->unset_userdata('url_redirect');
            }
        }

        if (!$this->input->get('code')) {
            header('Location:'.$provider->authorize());
        }else{
            try {
                $token = $provider->access($_GET['code']);
                $user = $provider->get_user_info($token);
                unset($_COOKIE['logged_user']);
                setcookie('logged_user', null, -1, '/');

                if(!empty($user)){
                    $login_user = $this->model_usuario->authentification_by_social_network($user['uid'], $user['email']);
                    if(!empty($login_user)){
                        //$this->session->set_userdata('logged_user', $login_user);

                        setcookie('logged_user', serialize($login_user), $this->expire_cookie,'/', 'toqueeltimbre.com');
                        $url_redirect = $this->session->userdata('url_redirect');
                        if(!empty($url_redirect)){
                            redirect($url_redirect);
                        }else{
                            redirect('dashboard');
                        }
                    }else{
                        // Registramos al usuario sino esta en la base de datos
                        $first_name = $user['first_name'];
                        $last_name = $user['last_name'];
                        $email = $user['email'];
                        if(empty($email)){
                            redirect('login?error=google');
                        }
                        $uid = $user['uid'];
                        //redirect('registrarse?first_name='.$first_name.'&last_name='.$last_name.'&uid='.$uid.'&email='.$email.'&social=Google');
                        $this->register_user($first_name, $last_name, $email, $uid, "Google");

                        $login_user = $this->model_usuario->authentification_by_social_network($uid, $email);
                        setcookie('logged_user', serialize($login_user), $this->expire_cookie,'/', 'toqueeltimbre.com');
                        redirect('dashboard');
                    }
                }else{
                    // Error no se pudo obtener usuario
                    redirect('login?error=google');
                }

            }catch (OAuth2_Exception $e){
                show_error('That didnt work: '.$e);
            }

        }
    }

    public function register_user($first_name, $last_name, $email, $uid, $social){
        $sw_email = $this->model_usuario->exits_email($email);
        if(empty($sw_email)){
            $key = $this->generate_key(9);
            $status = "Habilitado";
            $password = null;

            $values = array(
                "usu_nombre" => $first_name,
                "usu_apellido" => $last_name,
                "usu_email" => $email,
                "usu_ciu_id" => 0,
                "usu_estado" => $status,
                "usu_tipo" => "Particular",
                "usu_fecha_registro" => date("Y-m-d H:i:s"),
                "usu_uid" => $uid,
                "usu_social" => $social,
                "usu_key" => $key
            );
            $this->model_usuario->insert($values);
        }else{
            $user_values = array(
                "usu_uid" => $uid,
                "usu_social" => $social,
                "usu_estado" => "Habilitado"
            );
            $this->model_usuario->update($sw_email->usu_id, $user_values);
        }

    }

    private function generate_key($longitud) {
        $key = '';
        $pattern = '1234567890abcdefghijklmnopqrstuvwxyz';
        $max = strlen($pattern)-1;
        for($i=0;$i < $longitud;$i++) $key .= $pattern{mt_rand(0,$max)};
        return $key;
    }
}