<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: David
 * Date: 10/10/2014
 * Time: 10:38
 */
require_once FCPATH . '/application/libraries/facebook-php-ads-sdk/vendor/autoload.php';
use FacebookAds\Api;
use FacebookAds\Object\Lead;
use FacebookAds\Object\LeadgenForm;

class Home extends Base_page
{
    /**
    * Index Page for this controller.
    *
    * Maps to the following URL
    * 		http://example.com/index.php/welcome
    *	- or -
    * 		http://example.com/index.php/welcome/index
    *	- or -
    * Since this controller is set as the default controller in
    * config/routes.php, it's displayed at http://example.com/
    *
    * So any other public methods not prefixed with an underscore will
    * map to /index.php/welcome/<method_name>
    * @see http://codeigniter.com/user_guide/general/urls.html
    */

    function __construct()
    {
        parent::__construct();
        $this->load->model('model_departamento');
        $this->load->model('model_proyecto');
        $this->load->model('model_inmueble');
        $this->load->model('model_categoria');
        $this->load->model('model_slideshow');
        $this->load->model('model_forma');
        $this->load->model('model_publicacion');
        $this->load->helper('util_string');
    }

    public function index()
    {
        $this->complementHandler->addViewComplement("jquery");
        $this->complementHandler->addViewComplement("tinynav");
        $this->complementHandler->addViewComplement("parsley");
        $this->complementHandler->addViewComplement("parsley.spanish");
//        $this->complementHandler->addViewComplement("superfish");
//        $this->complementHandler->addViewComplement("hover-intent");
        $this->complementHandler->addViewComplement("jquery.ui.totop");
        $this->complementHandler->addViewComplement("owl.carousel");
//        $this->complementHandler->addViewComplement("jquery.content-panel-switcher");
//        $this->complementHandler->addViewComplement("jquery.cookies");
        $this->complementHandler->addViewComplement("bootstrapjs");
//        $this->complementHandler->addViewComplement("animate");
//        $this->complementHandler->addViewComplement("camera");
//        $this->complementHandler->addViewComplement("jquery.easing");
        $this->complementHandler->addViewComplement("jquery.ui");
        $this->complementHandler->addViewComplement("jquery.ui.touch-punch");
        $this->complementHandler->addViewComplement("jquery.switchButton");
//        $this->complementHandler->addViewComplement("numeral");
        $this->complementHandler->addViewComplement("jquery.colorbox");
//        $this->complementHandler->addViewComplement("jquery.print");

        $this->complementHandler->addViewComplement("bootbox");
        $this->complementHandler->addViewComplement("handlebars");
        $this->complementHandler->addViewComplement("line-height");
        $this->complementHandler->addProjectJs("general-scripts");
        $this->complementHandler->addProjectJs("facebook.login");
        $this->complementHandler->addProjectJs("quick-login");
        $this->complementHandler->addProjectCss("quick-login");
        $this->complementHandler->addPublicJs('handlerbars.custom.helpers');
        $this->complementHandler->addPublicJs("URL_Builder");
        $this->complementHandler->addPublicJs("home.index", TRUE);
        $this->complementHandler->addPublicCss("home.index", TRUE);
        $this->complementHandler->addPublicCss("publication-thumbnail");
//        $this->complementHandler->addViewComplement("dropdown");
//        $this->complementHandler->addViewComplement("dropdown.theme");
//        $this->complementHandler->addViewComplement("main");

        $data = array();
        $meta = array(
            "title" => "Casas, Departamentos, Lotes y Terrenos en Venta Bolivia",
            "keywords" => "casas, departamentos, terrenos, quintas, oficinas, propiedades, locales comerciales, inmuebles bolivia",
            "description" => "ToqueelTimbre.com el primer buscador de inmuebles en Bolivia encuentras casas, departamentos, lotes y terrenos en (venta anticretico alquiler)"
        );

        /******* google recaptcha*/
        $data["reCaptchaKeys"] = static::getReCaptchaKeys();
        /******* google recaptcha*/
        /****** facebook  */
        $facebookAppIdAndSecretKey = Base_page::getFacebookAppIdAndSecretKey();
        $data["facebookAppIdAndSecretKey"] = $facebookAppIdAndSecretKey;
        /****** facebook  */

        $this->loadHomePage('frontend/home-page', $meta["title"], $data, $meta);
    }

    public function change_city()
    {
        $city = $this->uri->segment(1);
        $city = ucwords(str_replace("-", " ", $city));
        $city = array("city" => $city);
        $this->geolocation = $city;
        //$this->session->set_userdata('cookie_city', $city);
        setcookie('cookie_city', serialize($city), $this->expire_cookie);
        $this->index();
    }

    public function change_bolivia()
    {
        $city = array("city" => "Bolivia");
        setcookie('cookie_city', serialize($city), $this->expire_cookie);
        redirect('/');
    }

    public function seoHandler($param1, $param2, $param3)
    {
        $this->load->model("model_publication");
        require_once(FCPATH."application/libraries/URL_Decoder.php");

        if($this->input->is_ajax_request())
        {
            $result = model_publication::getAllPublicResult(15,0);
            echo json_encode($result);exit;

        }
        else
        {
            $urlDecoder = new URL_Decoder($param1, $param2, $param3, $this->input->get());
            $urlDecoder->callController();
        }

    }

    public function debug($url)
    {
        $this->load->library("FacebookDebugger");
        $fb = new FacebookDebugger();
        echo "<pre>";$fb->reload(base_url("buscar/".$url));exit;
    }

    public function readXML($path)
    {
        $xmlFile = simplexml_load_file($path);
        $xmlAgent = simplexml_load_file("remaxFTP/Agents_120001_2017_10_1_Full.xml");
        $xmlOffice = simplexml_load_file("remaxFTP/Offices_120001_2017_9_10_Full.xml");

        $json = json_encode($xmlFile);
        $array = json_decode($json,TRUE);

        $jsonAgent = json_encode($xmlAgent);
        $arrayAgent = json_decode($jsonAgent,TRUE);

        $jsonOffices = json_encode($xmlOffice);
        $arrayOffices = json_decode($jsonOffices,TRUE);

        echo "<pre>";var_dump(/*"properties",$array,"agents",$arrayAgent,"offices",*/$arrayAgent);exit;
    }

    public function openFtp()
    {
        // connect and login to FTP server
        $ftp_server = "ftp.remax-europe.com";
        $ftp_username = "remaxbolivia_xml";
        $ftp_userpass = "oUSr4v";
        $year = date("Y");
        $month = date("m");
        $day = date("d");
        $ftp_conn = ftp_connect($ftp_server) or die("Could not connect to $ftp_server");
        $login = ftp_login($ftp_conn, $ftp_username, $ftp_userpass);
//        echo"<pre>";var_dump($login);exit;
        if($login)
        {
            $server_file = "Properties_120001_".$year."_".$month."_".$day."_Changes.xml";
            $local_file = "remaxFTP/Properties_120001_".$year."_".$month."_".$day."_Changes.xml";

            // download server file
            if (ftp_get($ftp_conn, $local_file, $server_file, FTP_ASCII))
            {
                echo "Successfully written to $local_file.";
            }
            else
            {
                echo "Error downloading $server_file.";
            }
            $this->readXML($local_file);
            // close connection
            ftp_close($ftp_conn);
        }
    }

    public function tetWebhook()
    {
        /*facebook webhook */
        $log = FCPATH."/assets/uploads/log.txt";
        $me = "desafio-tet";
        $content = file_get_contents("php://input");
        $content = json_decode($content, TRUE);
        if(isset($_GET['hub_mode']) && isset($_GET['hub_verify_token']) && $_GET['hub_verify_token'] == $me)
        {
            echo $_GET['hub_challenge'];

        }
        $content = is_null($content)?array():$content;
        $adManagement = new AdManagement($content);
        $adManagement->saveLeadgenOnSystem();
        ob_start();
        //
        print_r($content);
        //
        $data = ob_get_contents();
        ob_end_clean();
        file_put_contents($log,$data,FILE_APPEND);
    }

    public function facebookAdManagementLogin()
    {
        $keys = static::getFacebookAppIdAndSecretKey();     // Add to header of your file
        $data = array();
        $data["appId"] = $keys["appId"];
        $this->load->view("facebook-ad-management-login", $data);
    }

    public function adManagement()
    {
        $keys = static::getFacebookAppIdAndSecretKey();
        Api::init(
            $keys["appKey"], // App ID
            $keys["secretKey"],
            Base_page::getAccessTokenApiMarketing() // Your user access token
        );
        $form = new LeadgenForm("2047079508639525");
        $lead = new Lead("2046469648933291");

        echo"<pre>";var_dump("Form: ",$form->read()->getData(),"Lead: ",$lead->read()->getData());exit;
    }

    public function tetArray()
    {
        $test =
            array(
                array("name" => "consulta", "value" => "my consulta"),
                array("name" => "email", "value" => "jaironman_jcs@hotmail.com"),
                array("name" => "phone_number", "value" => "jaironman_jcs@hotmail.com")
            );
        $key1 = array_search("phone_number", array_column($test, 'name'));
        $key2 = array_search("full_name", array_column($test, 'name'));
        var_dump($key1, $key2);exit;
    }

    public function testLeadView()
    {
        $this->load->view("email_template/contact-message-facebook-lead");
    }

    function array_multi_key_exists( $arrNeedles, $arrHaystack){
        $Needle = array_shift($arrNeedles);
        if(count($arrNeedles) == 0){
            return(array_key_exists($Needle, $arrHaystack));
        }else{
            if(!array_key_exists($Needle, $arrHaystack)){
                return false;
            }else{
                return array_multi_key_exists($arrNeedles, $arrHaystack[$Needle]);
            }
        }
    }

    public function testLibrary()
    {
        $content = Array
                (
                "entry" => Array
                            (
                            "0" => Array
                                    (
                                        "changes" => Array
                                                (
                                                    "0" => Array
                                                            (
                                                                "field" => "leadgen",
                                                                "value" => Array
                                                                        (
                                                                            "created_time" => "1521471590",
                                                                            "page_id" => "288985344521207",
                                                                            "form_id" => "2047079508639525",
                                                                            "leadgen_id" => "2046469648933291"
                                                                        )

                                                            )

                                                ),

                                        "id" => "288985344521207",
                                        "time" => "1521471591"
                                    )

                            ),

                "object" => "page"
                );
        $adManagement = new AdManagement($content);
        $adManagement->saveLeadgenOnSystem();
    }

    public function testApi()
    {
        $path = FCPATH."/application/libraries/BitlyPHP-master/bitly.php";
        include_once($path);
        $params = array();
        $params['access_token'] = '6825b82423d7e7a9a82945e77c12c1ab71ef6e82';
        $params['longUrl'] = 'https://toqueeltimbre.com/buscar/casas-en-venta-en-santa-cruz/a-precio-de-terreno-casa-de-ocasion';
//        $params['longUrl'] = 'http://localhost/toque-el-timbre/buscar/casas-en-venta-en-santa-cruz/last-test---image-validation';
        $params['domain'] = 'bit.ly';
        $results = bitly_get('shorten', $params);
        echo"<pre>";var_dump($results);exit;
    }
}
