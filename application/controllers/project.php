<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Christian
 * Date: 06/11/2014
 * Time: 11:42
 */

class Project extends Base_page
{
    public $projects;
    public $config_project;

    function __construct()
    {
        parent::__construct();
        $this->load->model("model_proyecto");
        $this->load->model("model_mensaje_proyecto");
        $this->load->model("model_departamento");
        $this->load->model("model_parametros");
        $this->load->model("model_inmueble");
        $this->load->model("model_categoria");
        $this->load->model("model_tipologia");
        $this->load->model("model_forma");
        $this->load->model("model_user");
        $this->load->helper('url');

        $this->projects = $this->model_inmueble->get_main_pay_projects();
        $this->complementHandler->addViewComplement("sticky-element");
        $this->complementHandler->addProjectCss("project-sticky-form");
        $this->complementHandler->addProjectJs("project-sticky-form");
    }

    public function index()
    {
        $url = $this->uri->segment(1);
        $data["project_data"] = $this->model_proyecto->get_project_by_url($url);
        if(!empty($data["project_data"]["project"]))
        {
            $project = $data["project_data"]["project"];
            $data["project_data"]["acabado"] = $this->model_proyecto->getAllFinishingDetails($project->proy_id);
            $data["project_data"]["gallery"] = $this->model_proyecto->get_gallery($project->proy_id);
            $data['low_price'] = $this->model_proyecto->get_lower_price($project->proy_id);
        }

        $data['project_config'] = $this->model_proyecto->get_config_proyecto($project->proy_id);

        $title = !empty($project->proy_nombre) ? $project->proy_nombre : ".: ToqueElTimbre.com :.";
        $facebookMetaTags = $this->_getFacebookMetaTags($data["project_data"]["project"]);
        $data["facebookMetaTags"] = $facebookMetaTags;
        $facebookAppIdAndSecretKey = Base_page::getFacebookAppIdAndSecretKey();
        $data["facebookAppIdAndSecretKey"] = $facebookAppIdAndSecretKey;
        $data["reCaptchaKeys"] = static::getReCaptchaKeys();
        $url = "{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
        $this->session->set_userdata(array("lastPropertyViewed" => $url));
        $this->loadProjectTemplate("frontend/project/main", $title, $data);
    }

    private function _getFacebookMetaTags($project)
    {
        $facebookMetaTags = array();
        if(!empty($project))
        {
            $facebookMetaTags["url"] = current_url();
            $facebookMetaTags["title"] = $project->proy_nombre;
            $facebookMetaTags["description"] = strip_tags($project->proy_descripcion);
            $image = "";
            $projectSlideShowImage = $this->model_proyecto->get_project_slideshow($project->proy_id);
            if(isset($projectSlideShowImage[0]))
            {
                $fileHandler = new File_Handler($projectSlideShowImage[0]->imagen,"projectSlideShowImage");
                $thumbnail = $fileHandler->getThumbnail(527,389);
                $image = $thumbnail->getSource();
            }
            $facebookMetaTags["image"] = $image;
            $facebookMetaTags["site_name"] = "ToqueElTimbre";
            $facebookAppId = Base_page::getFacebookAppIdAndSecretKey();
            $facebookMetaTags["app_id"] = $facebookAppId["appId"];
        }
        return $facebookMetaTags;
    }

    public function prices(){
        $url = $this->uri->segment(1);
        $data["project_data"] = $this->model_proyecto->get_project_by_url($url);
        $cities = $this->model_departamento->get_all();

        $data['cities'] = json_decode(json_encode($cities));
        $data['view_name'] = "frontend/project/prices";
        $project = $data["project_data"]["project"];

        $data['low_price'] = $this->model_proyecto->get_lower_price($project->proy_id);

        $data['inmueble_projects'] = $this->projects;
        $categories = $this->model_categoria->get_all();
        $forma = $this->model_forma->get_all();
        $states = $this->model_departamento->get_all();
        $data['categories'] = json_decode(json_encode($categories), FALSE);
        $data['forma'] = json_decode(json_encode($forma), FALSE);
        $data['cities'] = json_decode(json_encode($states), FALSE);

        $data['project_config'] = $this->model_proyecto->get_config_proyecto($project->proy_id);

	    $project = $data["project_data"]['project'];
	    $this->breadcrumbs->push('Inicio', '/');
	    $this->breadcrumbs->push($project->proy_nombre, "/$project->proy_seo");
	    $this->breadcrumbs->push('Precio', "/$project->proy_seo/precio");

	    $data['breacrumbs'] = $this->breadcrumbs->show();

        $this->load_template("frontend/project/main", $project->proy_nombre, $data);
    }

    public function acabado(){
        $url = $this->uri->segment(1);
        $data["project_data"] = $this->model_proyecto->get_project_by_url($url);
        $data["project_data"]["acabado"] = $this->model_proyecto->get_acabado($data["project_data"]["project"]->proy_id);
        $cities = $this->model_departamento->get_all();
        $data['cities'] = json_decode(json_encode($cities));
        $data['view_name'] = "frontend/project/acabado";

        $project = $data["project_data"]["project"];
        $data['low_price'] = $this->model_proyecto->get_lower_price($project->proy_id);

        $data['inmueble_projects'] = $this->projects;
        $categories = $this->model_categoria->get_all();
        $forma = $this->model_forma->get_all();
        $states = $this->model_departamento->get_all();
        $data['categories'] = json_decode(json_encode($categories), FALSE);
        $data['forma'] = json_decode(json_encode($forma), FALSE);
        $data['cities'] = json_decode(json_encode($states), FALSE);

        $data['project_config'] = $this->model_proyecto->get_config_proyecto($project->proy_id);

	    $project = $data["project_data"]['project'];
	    $this->breadcrumbs->push('Inicio', '/');
	    $this->breadcrumbs->push($project->proy_nombre, "/$project->proy_seo");
	    $this->breadcrumbs->push('Acabado', "/$project->proy_seo/acabado");

	    $data['breacrumbs'] = $this->breadcrumbs->show();

        $this->load_template("frontend/project/main", $project->proy_nombre, $data);
    }

    public function gallery(){
        $url = $this->uri->segment(1);
        $data["project_data"] = $this->model_proyecto->get_project_by_url($url);
        $data["project_data"]["gallery"] = $this->model_proyecto->get_gallery($data["project_data"]["project"]->proy_id);
        $cities = $this->model_departamento->get_all();
        $data['cities'] = json_decode(json_encode($cities));
        $data['view_name'] = "frontend/project/gallery";

        $project = $data["project_data"]["project"];
        $data['low_price'] = $this->model_proyecto->get_lower_price($project->proy_id);

        $data['inmueble_projects'] = $this->projects;
        $categories = $this->model_categoria->get_all();
        $forma = $this->model_forma->get_all();
        $states = $this->model_departamento->get_all();
        $data['categories'] = json_decode(json_encode($categories), FALSE);
        $data['forma'] = json_decode(json_encode($forma), FALSE);
        $data['cities'] = json_decode(json_encode($states), FALSE);

        $data['project_config'] = $this->model_proyecto->get_config_proyecto($project->proy_id);

	    $project = $data["project_data"]['project'];
	    $this->breadcrumbs->push('Inicio', '/');
	    $this->breadcrumbs->push($project->proy_nombre, "/$project->proy_seo");
	    $this->breadcrumbs->push('Foto Galeria', "/$project->proy_seo/fotogaleria");

	    $data['breacrumbs'] = $this->breadcrumbs->show();

        $this->load_template("frontend/project/main", $project->proy_nombre, $data);
    }

    public function social(){
        $url = $this->uri->segment(1);
        $data["project_data"] = $this->model_proyecto->get_project_by_url($url);
        $data["project_data"]["social"] = $this->model_proyecto->get_area_social($data["project_data"]["project"]->proy_id);
        $cities = $this->model_departamento->get_all();
        $data['cities'] = json_decode(json_encode($cities));
        $data['view_name'] = "frontend/project/social";

        $project = $data["project_data"]["project"];
        $data['low_price'] = $this->model_proyecto->get_lower_price($project->proy_id);

        $data['inmueble_projects'] = $this->projects;
        $categories = $this->model_categoria->get_all();
        $forma = $this->model_forma->get_all();
        $states = $this->model_departamento->get_all();
        $data['categories'] = json_decode(json_encode($categories), FALSE);
        $data['forma'] = json_decode(json_encode($forma), FALSE);
        $data['cities'] = json_decode(json_encode($states), FALSE);

        $data['project_config'] = $this->model_proyecto->get_config_proyecto($project->proy_id);

	    $project = $data["project_data"]['project'];
	    $this->breadcrumbs->push('Inicio', '/');
	    $this->breadcrumbs->push($project->proy_nombre, "/$project->proy_seo");
	    $this->breadcrumbs->push('Area Social', "/$project->proy_seo/area-social");

	    $data['breacrumbs'] = $this->breadcrumbs->show();

        $this->load_template("frontend/project/main", $project->proy_nombre, $data);
    }

    public function tipologia_ajax(){
        $id_tipologia = $_POST["id_tipologia"];
        $data["tipologia"] = $this->model_tipologia->get_tipologia_id($id_tipologia);

        $this->load->view('frontend/project/tipologia_ajax', $data);
    }


    public function send_mail2()
    {
        $validationResponse = static::validateToken($_POST["g-recaptcha-response"]);
        $sendMessageResponse = array("response" => 0,"message" => '<div class="alert alert-danger">Su mensaje no pudo ser enviado.</div>');
        if($validationResponse->success)
        {
            require_once(BASE_DIRECTORY . DS . 'admin' . DS . 'clases' . DS. 'phpmailer' . DS . 'class.phpmailer.php');
            $parametros = $this->model_parametros->get_all();

            $template = base_url() . "assets/template_mail/mensaje_contacto_proyecto.html";
            $contenido = $this->obtener_contenido($template);
            $contenido = str_replace('@@nombre',$_POST['nombre'],$contenido);
            $contenido = str_replace('@@ciudad',$_POST['ciudad'],$contenido);
            $contenido = str_replace('@@telefono',$_POST['telefono'],$contenido);
            $contenido = str_replace('@@email',$_POST['email'],$contenido);
            $contenido = str_replace('@@mensaje',nl2br($_POST['mensaje']),$contenido);
            $contenido = str_replace('@@proy_nombre',$_POST['proy_nombre'],$contenido);

            $cue_nombre = $parametros->par_salida;
            $cue_pass = $parametros->par_pas_salida;

            $mail = new PHPMailer(true);
            $mail->SetLanguage('es');
            $mail->IsSMTP();
            $mail->SMTPAuth = true;
            $mail->Host = $parametros->par_smtp;
            $mail->Timeout=30;
            $mail->CharSet = 'utf-8';
            $mail->Subject = "Contacto Proyecto";
            $body = $contenido;
            $mail->MsgHTML($body);
            $mail->AddReplyTo($_POST['email'], $_POST['nombre']);
            $sendTo = strtolower($_POST['email']) == "toquetest@mailinator.com"?"toquetest@mailinator.com":$_POST['email_send'];
            $mail->AddAddress($sendTo);
            $mail->SetFrom($cue_nombre, model_parametros::SITE_DOMAIN);
            $mail->Username = $cue_nombre;
            $mail->Password = $cue_pass;

            if($mail->Send())
            {
                /** Create user after save his message */
                model_user::createUser('Buscador');

                $sendMessageResponse['response'] = 1;
                $sendMessageResponse['message'] = '<div class="alert alert-success">Su mensaje fue enviado correctamente</div>';
                $mensaje = array(
                    "men_nombre" => $_POST['nombre'],
                    "men_telefono" => $_POST['telefono'],
                    "men_email" => $_POST['email'],
                    "men_descripcion" => $_POST['mensaje'],
                    "men_proy_id" => $_POST['proy_id'],
                    "men_ciudad" => $_POST['ciudad'],
                    "men_fech" => date('Y-m-d')
                );
                if($sendTo != "toquetest@mailinator.com")
                    $this->model_mensaje_proyecto->insert($mensaje);
            }
        }
        echo json_encode($sendMessageResponse);
    }



    private function obtener_contenido($url)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.3) Gecko/20070309 Firefox/2.0.0.3");
        curl_setopt($curl, CURLOPT_URL, $url);
        $contenido = curl_exec($curl);
        curl_close($curl);

        return $contenido;
    }

    public function ajaxRegisterLogVisitor()
    {
        $formData = $this->input->post();
        $projectId = $formData["projectId"];
        $this->registerLogVisitor('Proyecto', $projectId);
    }
}