<?php
/**
 * Created by PhpStorm.
 * User: Christian
 * Date: 15/10/2014
 * Time: 15:58
 */

class Landing extends Base_page{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */

    function __construct() {
        parent::__construct();
        $this->load->model('model_departamento');
        $this->load->model('model_proyecto');
        $this->load->model('model_inmueble');
        $this->load->model('model_categoria');
        $this->load->model('model_landing');
        $this->load->model('model_forma');
        $this->load->model('model_publicacion');
        $this->load->helper('util_string');
        $this->load->helper('url');
        $this->load->library('Googlemaps');
    }

    public function index() {

    }

    public function comprar(){
        $data['inmueble_projects'] = $this->model_inmueble->get_main_pay_projects();
        $data['pay_publications'] = $this->model_publicacion->get_pay_publications('venta', 'Landing');
        $data['landing'] = $this->model_landing->get_landing();
        $states = $this->model_departamento->get_all();
        $categories = $this->model_categoria->get_all();
        $forma = $this->model_forma->get_all();
        $data["box_search"] = $this->load->view("template/box_search", array("categories" => $categories, "forma" => $forma, "states" => $states, "page" => "venta"), true);

        $data['publications_map'] = $this->model_publicacion->get_all_type_publications_map('venta');
        $data['buy_map'] = $this->createMap($data['publications_map']);

        $meta = array(
            "title" => "Comprar casas, departamentos, lotes y terrenos en Bolivia",
            "keywords" => "casas, departamentos, terrenos, bolivia",
            "description" => "Comprar casas, departamentos, lotes y terrenos"
        );

        $this->load_template('frontend/landing/comprar', $meta["title"], $data, $meta);
    }

    public function alquilar() {
        $data['inmueble_projects'] = $this->model_inmueble->get_main_pay_projects();
        $data['pay_publications'] = $this->model_publicacion->get_pay_publications('alquiler', 'Landing');
        $data['landing'] = $this->model_landing->get_landing();
        $states = $this->model_departamento->get_all();
        $categories = $this->model_categoria->get_all();
        $forma = $this->model_forma->get_all();
        $data["box_search"] = $this->load->view("template/box_search", array("categories" => $categories, "forma" => $forma, "states" => $states, "page" => "alquiler"), true);

        $data['publications_map'] = $this->model_publicacion->get_all_type_publications_map('alquiler');
        $data['rent_map'] = $this->createMap($data['publications_map']);

        $meta = array(
            "title" => "Alquilar casas, departamentos, lotes y terrenos en Bolivia",
            "keywords" => "casas, departamentos, terrenos, bolivia",
            "description" => "Alquilar casas, departamentos, lotes y terrenos"
        );

        $this->load_template('frontend/landing/alquilar', $meta["title"], $data, $meta);
    }

    public function anticretico(){
        $data['inmueble_projects'] = $this->model_inmueble->get_main_pay_projects();
        $data['pay_publications'] = $this->model_publicacion->get_pay_publications('anticretico', 'Landing');
        $data['landing'] = $this->model_landing->get_landing();
        $states = $this->model_departamento->get_all();
        $categories = $this->model_categoria->get_all();
        $forma = $this->model_forma->get_all();
        $data["box_search"] = $this->load->view("template/box_search", array("categories" => $categories, "forma" => $forma, "states" => $states, "page" => "anticretico"), true);

        $data['publications_map'] = $this->model_publicacion->get_all_type_publications_map('anticretico');
        $data['anticretico_map'] = $this->createMap($data['publications_map']);

        $meta = array(
            "title" => "Anticretico casas, departamentos, lotes y terrenos en Bolivia",
            "keywords" => "casas, departamentos, terrenos, bolivia",
            "description" => "Anticretico casas, departamentos, lotes y terrenos"
        );

        $this->load_template('frontend/landing/anticretico', $meta["title"], $data, $meta);
    }

    /**
     * Create a google map with exist places
     *
     * @param $places
     * @return []
     */
    private function createMap($publications = array()) {
        $curl = $this->getExternalUrlData();

        // Make map configuration
        $config = array();
        if ($curl != NULL || !empty($curl)) {
            $lat = $curl['latitud'];
            $lng = $curl['longitud'];
            //$config['center'] = "$city, $countryName";
            $config['center'] = "$lat, $lng";
            $config['zoom'] = 12;
            $config['infowindowMaxWidth'] = 320;
            $config['scrollwheel'] = FALSE;
        } else {
            $config['zoom'] = 5;
            $config['center'] = "-19.430799, -64.450275";
            $config['infowindowMaxWidth'] = 320;
            $config['scrollwheel'] = FALSE;
        }
        $config['cluster'] = TRUE;
        $config['clusterAverageCenter'] = TRUE;
        $config['clusterMinimumClusterSize'] = 6;

        $this->googlemaps->initialize($config);

        $marker = array();
        if (!empty($publications)) {
            foreach($publications as $publication) {
                $latitud = $publication->inm_latitud;
                $longitud = $publication->inm_longitud;
                $features = "Features";
                //$marker['position'] = '37.429, -122.1419';
                $marker['position'] = "$latitud, $longitud";
                $marker['icon'] = base_url() . "assets/img/icons/icono_map1.png";

                $url_detalle = base_url();
                $url_detalle .= seo_url($publication->ciu_nombre)."/";
                $url_detalle .= seo_url($publication->cat_nombre . " EN ". $publication->for_descripcion)."/";
                $url_detalle .= seo_url($publication->inm_seo).".html";

                $image = "";
                $imageX =  $publication->fot_archivo;
                $fileimage = BASE_DIRECTORY . DS . "admin" . DS . "imagenes" . DS . "inmueble" . DS . $imageX;
                if (!empty($imageX) && !is_null($imageX) && file_exists($fileimage)) {
                    $image = '<img style="float:left; margin-right:5px; margin-top:3px;" src="' . base_url(). 'librerias/timthumb.php?src=' . base_url(). 'admin/imagenes/inmueble/'.$publication->fot_archivo.'&h=94" />';
                }

                $marker['infowindow_content'] =
                    "<div style='height: 120px; line-height:20px;'>".
                    $image.
                    "$publication->inm_nombre".
                    "<br />".
                    "<b>Precio:</b> " .number_format($publication->inm_precio, 0, ",", ".") . " " . $publication->mon_abreviado.
                    "<p><a href='$url_detalle' target='_blank' style='color:#00aeef'><b>Ver Inmueble</b></a></p>".
                    "</div>";
                $this->googlemaps->add_marker($marker);
            }
        }
        return $this->googlemaps->create_map();
    }

    /**
     * Execute a curl php method, for get location data from external url.
     *
     * @param string $url
     * @return array
     */
    private function getExternalUrlData($url = "http://api.hostip.info/get_json.php?position=true") {

        //$ip = $_SERVER['REMOTE_ADDR'];
        $ip = '200.119.198.122';
        $url = 'http://ipinfo.io/'.$ip .'/geo';

        //  Initiate curl
        $ch = curl_init();
        // Disable SSL verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        // Will return the response, if false it print the response
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Set the url
        curl_setopt($ch, CURLOPT_URL, $url);
        // Execute
        $result=curl_exec($ch);
        // Closing
        curl_close($ch);

        $result = json_decode($result);

        // Ya no recoge de la api lo localizacion en el mapa, sino de la variable ciudad que selecciona el usuario
        $city = $this->geolocation['city'];
        //return $this->get_city_location($result->city);
        return $this->get_city_location($city);
    }

    private function get_city_location($city){
        switch($city){
            case "Santa Cruz":
                $loc = array(
                    'latitud' => '-17.7858707604867',
                    'longitud' => '-63.181906442683434'
                );
            break;
            case "Beni":
                $loc = array(
                    'latitud' => '-14.832512586191942',
                    'longitud' => '-64.90341024887084'
                );
            break;
            case "Pando":
                $loc = array(
                    'latitud' => '-11.026357967074226',
                    'longitud' => '-68.75968960794069'
                );
            break;
            case "Cochabamba":
                $loc = array(
                    'latitud' => '-17.383700584651177',
                    'longitud' => '-66.16220185157472'
                );
            break;
            case "La Paz":
                $loc = array(
                    'latitud' => '-16.503158914469847',
                    'longitud' => '-68.15112231314696'
                );
            break;
            case "Chuquisaca":
                $loc = array(
                    'latitud' => '-19.04207178593077',
                    'longitud' => '-65.25660642254638'
                );
            break;
            case "Tarija":
                $loc = array(
                    'latitud' => '-21.530393338903124',
                    'longitud' => '-64.73134376380007'
                );
            break;
            case "Oruro":
                $loc = array(
                    'latitud' => '-17.966217321421365',
                    'longitud' => '-67.10966354064328'
                );
            break;
            case "Potosi":
                $loc = array(
                    'latitud' => '-19.570802595728622',
                    'longitud' => '-65.757733447406'
                );
            break;
            default:
                $loc = array(
                    'latitud' => '-17.7858707604867',
                    'longitud' => '-63.181906442683434'
                );
            break;
        }
        return $loc;
    }
}