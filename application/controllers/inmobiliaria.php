<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Christian
 * Date: 04/11/2014
 * Time: 17:11
 */

class Inmobiliaria extends Base_page
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('model_usuario');
        $this->load->model('model_publicacion');
        $this->load->model('model_inmueble');
        $this->load->model('model_departamento');
        $this->load->model('model_parametros');
        $this->load->model('model_categoria');
        $this->load->model('model_forma');
        $this->load->model('model_user');
        $this->load->helper('url');
    }

    public function index()
    {
        $this->complementHandler->addViewComplement("bootbox");
        $this->complementHandler->addViewComplement("pagination");
        $this->complementHandler->addViewComplement("handlebars");
        $this->complementHandler->addPublicJs('handlerbars.custom.helpers');
        $this->complementHandler->addProjectJs("facebook.login");
        $this->complementHandler->addProjectJs("quick-login");
        $this->complementHandler->addProjectCss("quick-login");
        $this->complementHandler->addProjectJs("RegisterVisitor");
        $this->complementHandler->addProjectCss("inmobiliaria.index");
        $this->complementHandler->addProjectJs("inmobiliaria.index");
        $this->complementHandler->addPublicCss("publication-thumbnail");

        $data["reCaptchaKeys"] = static::getReCaptchaKeys();
        $inm_id = $this->uri->segment(2);
        $user = $this->model_usuario->get_info_agent($inm_id);
        $data["inmobiliaria"] = $user;

        $company = empty($user->usu_empresa) ? $user->usu_nombre : $user->usu_empresa;
        $description = !empty($user->usu_descripcion) ? crop_string($user->usu_descripcion, 200) : '';
        $keywords_array = explode(" ", $company);
        $keywords_array = implode(", ", $keywords_array);
        $keywords_array = strtolower($keywords_array);
        $meta = array(
            "title" => crop_string($company, 80),
            "keywords" => $keywords_array,
            "description" => $description
        );

	    $this->breadcrumbs->push('Inicio', '/');
	    $this->breadcrumbs->push('Detalle Inmueble', $this->session->userdata('url_inm_detalle'));
	    $this->breadcrumbs->push('Perfil', '/inmobiliaria/index');
	    $data['breacrumbs'] = $this->breadcrumbs->show();
        $this->load_template("frontend/inmobiliaria/detail", $meta["title"], $data, $meta);
    }

    public function send_mail2()
    {
        $validationResponse = static::validateToken($_POST["g-recaptcha-response"]);
        $sendMessageResponse = array("response" => 0,"message" => '<div class="alert alert-danger">Su mensaje no pudo ser enviado.</div>');
        if($validationResponse->success)
        {
            require_once(BASE_DIRECTORY . DS . 'admin' . DS . 'clases' . DS. 'phpmailer' . DS . 'class.phpmailer.php');
            $parametros = $this->model_parametros->get_all();

            $template = base_url() . "assets/template_mail/mensaje_contacto_inmobiliaria.html";
            $contenido = $this->obtener_contenido($template);
            $contenido = str_replace('@@nombre',$_POST['nombre'],$contenido);
            $contenido = str_replace('@@ciudad',$_POST['ciudad'],$contenido);
            $contenido = str_replace('@@telefono',$_POST['telefono'],$contenido);
            $contenido = str_replace('@@email',$_POST['email'],$contenido);
            $contenido = str_replace('@@mensaje',nl2br($_POST['mensaje']),$contenido);
            $contenido = str_replace('@@proy_nombre',$_POST['proy_nombre'],$contenido);

            $cue_nombre = $parametros->par_salida;
            $cue_pass = $parametros->par_pas_salida;

            $mail = new PHPMailer(true);
            $mail->SetLanguage('es');
            $mail->IsSMTP();
            $mail->SMTPAuth = true;
            $mail->Host = $parametros->par_smtp;
            $mail->Timeout=30;
            $mail->CharSet = 'utf-8';
            $mail->Subject = "Contacto inmobiliaria";
            $body = $contenido;
            $mail->MsgHTML($body);
            $sendTo = strtolower($_POST['email']) == "toquetest@mailinator.com"?"toquetest@mailinator.com":$_POST['email_send'];
            $mail->AddAddress($sendTo);
            $mail->AddReplyTo($_POST['email'], $_POST['nombre']);
            $mail->SetFrom($cue_nombre, model_parametros::SITE_DOMAIN);
            $mail->Username = $cue_nombre;
            $mail->Password = $cue_pass;

            try
            {
                if($mail->Send())
                {
                    /** Create user after save his message */
                    model_user::createUser('Buscador');

                    $sendMessageResponse['response'] = 1;
                    $sendMessageResponse['message'] = '<div class="alert alert-success">Su mensaje fue enviado correctamente</div>';
                }
            }
            catch (Exception $e)
            {
                $sendMessageResponse['response'] = 1;
                $sendMessageResponse['message'] = $e->getMessage();
            }
        }

        echo json_encode($sendMessageResponse);
    }

    private function obtener_contenido($url)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.3) Gecko/20070309 Firefox/2.0.0.3");
        curl_setopt($curl, CURLOPT_URL, $url);
        $contenido = curl_exec($curl);
        curl_close($curl);

        return $contenido;
    }
}
