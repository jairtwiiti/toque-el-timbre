﻿<?php 
	require_once('admin/config/constante.php');
	require_once('admin/config/database.conf.php');
	
	mysql_connect(_SERVIDOR_BASE_DE_DATOS,_USUARIO_BASE_DE_DATOS, _PASSWORD_BASE_DE_DATOS) or
			die("Could not connect: " . mysql_error());
	mysql_select_db(_BASE_DE_DATOS);
	
	function obtener_contenido($url)
	{
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_HEADER, 0);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);		
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.3) Gecko/20070309 Firefox/2.0.0.3");	
		curl_setopt($curl, CURLOPT_URL, $url);   		
		$contenido = curl_exec($curl); 
		curl_close($curl); 
		
		return $contenido;
	}
	
	function validaCorreo($valor)
	{
		if(eregi("([a-zA-Z0-9._-]{1,30})@([a-zA-Z0-9.-]{1,30})", $valor)) return TRUE;
		else return FALSE;
	}
	
	function fecha_latina($fecha)
	{
		$aux = explode("-",$fecha);
		return $aux[2]."/".$aux[1]."/".$aux[0];
	}
	
	function enviar_reporte($usu_id,$usu_nombre,$usu_email,$usu_tipo){
	
			require_once('clases/phpmailer/class.phpmailer.php');	
			include_once("clases/EnDecryptText.php");		 
			
			$EnDecryptText = new EnDecryptText();
			$enc = $EnDecryptText->Encrypt_Text($pub_id);			
			
			$sql="select * from ad_parametro limit 1";
		    $rs = mysql_query($sql);		    
			$fila=mysql_fetch_object($rs);
			
			$contenido = obtener_contenido(_URL_TEMPLATE_EMAIL."reporte_usuario.html");			
			
			$contenido = str_replace('@@usu_nombre', $usu_nombre ,$contenido);
			
			$enc_usu_id = $EnDecryptText->Encrypt_Text($usu_id);	
			$contenido = str_replace('@@link_baja',_URL_WEBSITE."index.php?dir=desactivar_mailing_reporte&k=".$enc_usu_id,$contenido);
			
		if( $usu_tipo != 'Constructora')	{
			$sql = "SELECT inm_id,inm_nombre,inm_visitas,inm_publicado,pub_vig_ini,pub_vig_fin,pub_estado,count(men_id) as cant_men
							         FROM bus_inmueble_1 LEFT JOIN mensaje ON (inm_id = men_inm_id)
 									 WHERE pub_usu_id =".$usu_id." and pub_estado='Aprobado' GROUP BY inm_id ";	
			$res = mysql_query($sql);
			$num = mysql_num_rows($res);	
			
			echo $num;
			
			if( $num > 0){
				$reporte = "";
				for($i=0; $i < $num ; $i++)
				{
					$objeto=mysql_fetch_object($res);				
					if (($i%2)==1){
						$reporte = $reporte . '<tr  BGCOLOR="#F1F1F1">';	
					}else{
						$reporte = $reporte . '<tr>';
					}
				
					$reporte = $reporte . '<td>';
					$reporte = $reporte . '<font face="Verdana" size="2" color="#00AEEF"><a style="color:#00AEEF;" href="http://www.toqueeltimbre.com/index.php?dir=detalle&id='. $objeto->inm_id .'" target="_blank">' . $objeto->inm_nombre . '</a></font>';
					$reporte = $reporte . '</td>';
					$reporte = $reporte . '<td style="vertical-align: top;"> <font face="Verdana" size="2" color="#474747">' . $objeto->inm_id . '</font></td>';
					$reporte = $reporte . '<td style="vertical-align: top;"> <font face="Verdana" size="2" color="#474747">' . $objeto->inm_visitas . '</font></td>';			 
					$reporte = $reporte . '<td style="vertical-align: top;"> <font face="Verdana" size="2" color="#474747">' . $objeto->cant_men . '</font></td>';		
					$reporte = $reporte . '<td style="vertical-align: top;"> <font face="Verdana" size="2" color="#474747">' . fecha_latina($objeto->pub_vig_fin) . '</font></td>';		
					$reporte = $reporte . '</tr>';
				
				}
			
				$contenido = str_replace('@@reporte', $reporte ,$contenido);
			
			
				$cue_nombre=$fila->par_salida; 
				
				$cue_pass=$fila->par_pas_salida;
				
				$mail = new PHPMailer(true);			
				$mail->SetLanguage('es');
				$mail->IsSMTP();
				$mail->SMTPAuth   = true;                              
				$mail->Host       = $fila->par_smtp;      		
				$mail->Timeout=30;
				$mail->CharSet = 'utf-8';							
				$mail->Subject = "Resumen del rendimiento de tus anuncios en Toqueeltimbre.com ";				
				$body =  $contenido;			
				$mail->MsgHTML($body);	
				$sw = validaCorreo($usu_email);		
				if( $sw ){
					$mail->AddAddress($usu_email);					
				}
				$mail->AddAddress('cristian.inarra@gmail.com');
				$mail->SetFrom($cue_nombre, $fila->par_smtp);							
				$mail->Username   = $cue_nombre;  
				$mail->Password   = $cue_pass;	
				if( $sw ){
					try {
						$mail->Send(); 	
						return true;					

					} catch (phpmailerException $e) {

							return  false;

					} catch (Exception $e) {

							return false;
					}
				}
			}else{
				return true;
			}
		}else{  // es constructora
			$sql = "SELECT proy_id,proy_nombre,proy_visitas,proy_vig_ini,proy_vig_fin,proy_estado,proy_seo 
							         FROM proyecto WHERE proy_usu_id =".$usu_id." and proy_estado='Aprobado' GROUP BY proy_id ";	
			$res = mysql_query($sql);
			$num = mysql_num_rows($res);	
			
			if( $num > 0){
				$reporte = "";
				for($i=0; $i < $num ; $i++)
				{
					$objeto=mysql_fetch_object($res);
					
					$sql1 = "SELECT count(*) as cant_men from mensaje_proyecto,proyecto where proy_id=". $objeto->proy_id ." and proy_id=men_proy_id";	
					$res1 = mysql_query($sql1);
					$objeto1=mysql_fetch_object($res1);	
				
								
					if (($i%2)==1){
						$reporte = $reporte . '<tr  BGCOLOR="#F1F1F1">';	
					}else{
						$reporte = $reporte . '<tr>';
					}
				
					$reporte = $reporte . '<td>';
					$reporte = $reporte . '<font face="Verdana" size="2" color="#00AEEF"><a style="color:#00AEEF;" href="http://www.toqueeltimbre.com/'. $objeto->proy_seo .'/" target="_blank">' . $objeto->proy_nombre . '</a></font>';
					$reporte = $reporte . '</td>';
					$reporte = $reporte . '<td style="vertical-align: top;"> <font face="Verdana" size="2" color="#474747">' . $objeto->proy_id . '</font></td>';
					$reporte = $reporte . '<td style="vertical-align: top;"> <font face="Verdana" size="2" color="#474747">' . $objeto->proy_visitas . '</font></td>';			 
					$reporte = $reporte . '<td style="vertical-align: top;"> <font face="Verdana" size="2" color="#474747">' . $objeto1->cant_men . '</font></td>';		
					$reporte = $reporte . '<td style="vertical-align: top;"> <font face="Verdana" size="2" color="#474747">' . fecha_latina($objeto->proy_vig_fin) . '</font></td>';		
					$reporte = $reporte . '</tr>';
				
				}
			
				$contenido = str_replace('@@reporte', $reporte ,$contenido);
			
			
				$cue_nombre=$fila->par_salida; 
				
				$cue_pass=$fila->par_pas_salida;
				
				$mail = new PHPMailer(true);			
				$mail->SetLanguage('es');
				$mail->IsSMTP();
				$mail->SMTPAuth   = true;                              
				$mail->Host       = $fila->par_smtp;      		
				$mail->Timeout=30;
				$mail->CharSet = 'utf-8';							
				$mail->Subject = "Resumen del rendimiento de tus proyectos en Toqueeltimbre.com ";				
				$body =  $contenido;			
				$mail->MsgHTML($body);			
				$sw = validaCorreo($usu_email);
				if($sw){
					$mail->AddAddress($usu_email);					
				}
				echo $sw;
				/*$mail->AddAddress('silvestre@twiiti.com');*/
				$mail->AddAddress('cristian.inarra@gmail.com');
				$mail->SetFrom($cue_nombre, $fila->par_smtp);							
				$mail->Username   = $cue_nombre;  
				$mail->Password   = $cue_pass;	
				if( $sw ){
					try {
						$mail->Send();
						return true;					

					} catch (phpmailerException $e) {

							return  false;

					} catch (Exception $e) {

							return false;
					}
				}
			}else{
				return true;
			}
		}
		return true;
	}
	
	//$sql="SELECT * FROM usuario where usu_estado='Habilitado' and usu_enviado=0 order by usu_id asc";		
	$sql="SELECT * FROM usuario where usu_estado='Habilitado' and usu_enviado=0 and usu_email_reporte='1' order by usu_id asc limit 20";		
	//$sql="SELECT * FROM publicacion where pub_estado='Cancelado' and pub_enviado='0' and pub_vig_fin>='$fecha_actual' and pub_vig_fin<='$fecha_actual_mas_2' and pub_usu_id=13 order by pub_vig_fin asc ";		
	$res = mysql_query($sql);
	$num = mysql_num_rows($res);
	
	echo $num . '<br>';
	
	if( $num>0 ){
		for($i=0 ; $i<1 && $i<$num ; $i++){
			$objeto=mysql_fetch_object($res);
			if( enviar_reporte( $objeto->usu_id , $objeto->usu_nombre , $objeto->usu_email , $objeto->usu_tipo ) == true ){
				echo $objeto->usu_nombre . '<br>' ;
				$fecha_actual = date("Y-m-d H:i:s");
				$sql="update usuario set usu_enviado='1' where usu_id=".$objeto->usu_id;		
				mysql_query($sql);	
				$sql="insert into usuario_reporte (rep_txt,rep_fecha) values('" . $objeto->usu_id . " - " . $objeto->usu_nombre ." ". $objeto->usu_apellido . "','". $fecha_actual ."')";		
				mysql_query($sql);
			}
		}
	}else{
		$sql = "update usuario set usu_enviado=0 where usu_id>0";
		mysql_query($sql);
	}
	
?>