﻿<?php 
	require_once('admin/config/constante.php');
	require_once('admin/config/database.conf.php');
	
	mysql_connect(_SERVIDOR_BASE_DE_DATOS,_USUARIO_BASE_DE_DATOS, _PASSWORD_BASE_DE_DATOS) or
			die("Could not connect: " . mysql_error());
	mysql_select_db(_BASE_DE_DATOS);
	
	function obtener_contenido($url)
	{
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_HEADER, 0);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);		
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.3) Gecko/20070309 Firefox/2.0.0.3");	
		curl_setopt($curl, CURLOPT_URL, $url);   		
		$contenido = curl_exec($curl); 
		curl_close($curl); 
		
		return $contenido;
	}
	
	function validaCorreo($valor)
	{
		if(eregi("([a-zA-Z0-9._-]{1,30})@([a-zA-Z0-9.-]{1,30})", $valor)) return TRUE;
		else return FALSE;
	}
	
	function enviar_notificacion($publicaciones ,$noti_obj){	
	
			require_once('clases/phpmailer/class.phpmailer.php');	
			include_once("clases/EnDecryptText.php");		 
			
			$EnDecryptText = new EnDecryptText();
			$enc = $EnDecryptText->Encrypt_Text($pub_id);			
			
			$sql="select * from ad_parametro limit 1";
		    $rs = mysql_query($sql);		    
			$fila=mysql_fetch_object($rs);
			
			$contenido = obtener_contenido(_URL_TEMPLATE_EMAIL."notificaciones_busqueda.html");			
			
			echo $publicaciones;
			$publicaciones = mysql_query($publicaciones);
			$num = mysql_num_rows($publicaciones);
			
			if( $num > 0){
				
				$contenido = str_replace('@@usu_nombre', $noti_obj->not_bus_nombre ,$contenido);
				$reporte = "";
				
				for($i=0 ; $i<$num ; $i++){
					$pub = mysql_fetch_object($publicaciones);	
					
					if (($i%2)==1){
						$reporte = $reporte . '<tr  BGCOLOR="#F1F1F1">';	
					}else{
						$reporte = $reporte . '<tr>';
					}
				
					$reporte = $reporte . '<td style="vertical-align: top;"> <font face="Verdana" size="2" color="#474747">' . $pub->inm_nombre . '</font></td>';
					$reporte = $reporte . '<td>';
					$reporte = $reporte . '<font face="Verdana" size="2" color="#00AEEF"><a style="color:#00AEEF;" href="http://toqueeltimbre.com/index.php?dir=detalle&id='. $pub->inm_id .'" target="_blank">' . 'Ver' . '</a></font>';
					$reporte = $reporte . '</td>';
					$reporte = $reporte . '</tr>';
				}
			
			$contenido = str_replace('@@reporte',$reporte,$contenido);
			/*$contenido = str_replace('@@url',_URL_WEBSITE."index.php?dir=reactivar_publicacion&k=".$enc,$contenido);
			$contenido = str_replace('@@aurl',_URL_WEBSITE."index.php?dir=reactivar_publicacion&<br>k=".$enc,$contenido);*/
			
			$cue_nombre=$fila->par_salida; 
				
			$cue_pass=$fila->par_pas_salida;
				
			$mail = new PHPMailer(true);			
			$mail->SetLanguage('es');
			$mail->IsSMTP();
			$mail->SMTPAuth = true;                              
			$mail->Host = $fila->par_smtp;      		
			$mail->Timeout=30;
			$mail->CharSet = 'utf-8';							
			$mail->Subject = "Toqueeltimbre.com consiguió información de tu interés";				
			$body =  $contenido;			
			$mail->MsgHTML($body);	
			$sw = validaCorreo($noti_obj->not_bus_email);
			if( $sw ){
				$mail->AddAddress($noti_obj->not_bus_email);				  
			}
            $mail->AddAddress("cristian.inarra@gmail.com");
			$mail->SetFrom($cue_nombre, $fila->par_smtp);							
			$mail->Username   = $cue_nombre;  
			$mail->Password   = $cue_pass;	
			  if( $sw ){
				try {
					$mail->Send(); 	
                    return true;					

				} catch (phpmailerException $e) {

						return  false;

				} catch (Exception $e) {

						return false;
				}	
			  }
			}else{
				return true;
			}
			return true;
	}
	
	$fecha_actual = date("Y-m-d");
	$fecha_actual_menos_1 = date("Y-m-d" ,time()-86400 ); //restamos 1 dia a la fecha actual
	
	$sql= "SELECT not_bus_id
				FROM notificaciones_busqueda
				where not_bus_vig_fin>='$fecha_actual' and not_bus_id NOT IN
					  (
					  SELECT nbp_not_bus_id from not_bus_pub where nbp_fecha='$fecha_actual'
					  )";
	
	$res = mysql_query($sql);
	$num = mysql_num_rows($res);
	
	echo $num . '<br>';
	
	for($i=0 ; $i<1 && $i<$num ; $i++){	
		$objeto=mysql_fetch_object($res);
		$nb_id = $objeto->not_bus_id;		
		
		$sql2 = "select *from notificaciones_busqueda where not_bus_id=$nb_id";
		$res2 = mysql_query($sql2);
		$noti_obj = mysql_fetch_object($res2);
		
		$cat_id = $noti_obj->not_bus_cat_id;
		$for_id = $noti_obj->not_bus_for_id;
		$dep_id = $noti_obj->not_bus_dep_id;
		
		$precio_val = $noti_obj->not_bus_precio;
		
		$sql3 = "SELECT *FROM publicacion,inmueble,ciudad,departamento 
				where pub_inm_id=inm_id and 
					  inm_ciu_id = ciu_id and					  
					  pub_estado='Aprobado' and 
					  DATE_FORMAT(pub_creado,'%Y-%m-%d')='$fecha_actual_menos_1' and 
					  inm_cat_id=$cat_id and
					  inm_for_id=$for_id and
					  ciu_dep_id=dep_id and
					  dep_id=$dep_id";
		
		if($precio_val != ""){
			$arr = explode("-",$precio_val);
			$min = $arr[0];
			$max = $arr[1];
			$sql3 = $sql3 . " and inm_precio>=$min and inm_precio<=$max ";
		}
		//$publicaciones = mysql_query($sql3);
		
		if( enviar_notificacion( $sql3 , $noti_obj ) == true ){
				$not_bus_id = $noti_obj->not_bus_id;
				echo $noti_obj->not_bus_nombre . '<br>';
				$sql="insert into not_bus_pub(nbp_not_bus_id,nbp_fecha) values ( $not_bus_id,'$fecha_actual')";		
				mysql_query($sql);	
		}		
	}
	
?>