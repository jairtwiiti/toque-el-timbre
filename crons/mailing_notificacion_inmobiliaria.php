<?php 
	require_once('../admin/config/constante.php');
	require_once('../admin/config/database.conf.php');
	
	mysql_connect(_SERVIDOR_BASE_DE_DATOS,_USUARIO_BASE_DE_DATOS, _PASSWORD_BASE_DE_DATOS) or
			die("Could not connect: " . mysql_error());
	mysql_select_db(_BASE_DE_DATOS);
	
	function obtener_contenido($url)
	{
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_HEADER, 0);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);		
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.3) Gecko/20070309 Firefox/2.0.0.3");	
		curl_setopt($curl, CURLOPT_URL, $url);   		
		$contenido = curl_exec($curl); 
		curl_close($curl); 
		
		return $contenido;
	}
	
	function transformar_precio($precio_val){
		$res = explode("-",$precio_val);
		return number_format($res[0], 0, '', '.').'$ a '.number_format($res[1], 0, '', '.').'$';
	}
	
	function validaCorreo($valor)
	{
		if(eregi("([a-zA-Z0-9._-]{1,30})@([a-zA-Z0-9.-]{1,30})", $valor)) return TRUE;
		else return FALSE;
	}
	
	function obtener_departamento($ciu_id){
		$sql = "SELECT dep_id FROM ciudad 
		INNER JOIN departamento ON dep_id = ciu_dep_id
		WHERE ciu_id=" . $ciu_id;
		$res = mysql_query($sql);
		$objeto = mysql_fetch_object($res);
		return $objeto->dep_id;
	}
	
	function enviar_notificaciones($notificaciones ,$usu_obj){	
	
			require_once('../admin/clases/phpmailer/class.phpmailer.php');
			include_once("../admin/clases/EnDecryptText.php");
			
			$sql = "SELECT *from usuario where usu_id=" . $usu_obj->usu_id;
			$res = mysql_query($sql);
			$usu_obj = mysql_fetch_object($res);
	
			
			$EnDecryptText = new EnDecryptText();
			$enc = $EnDecryptText->Encrypt_Text($usu_obj->usu_id);			
			
			$sql="select * from ad_parametro limit 1";
		    $rs = mysql_query($sql);		    
			$fila=mysql_fetch_object($rs);
			
			$contenido = obtener_contenido("http://toqueeltimbre.com/assets/template_mail/notificaciones_busqueda_inmobiliaria.html");
			
			$notificaciones = mysql_query($notificaciones);
			$num = mysql_num_rows($notificaciones);

			if( $num > 0){
				
				$contenido = str_replace('@@usu_nombre', $usu_obj->usu_nombre ,$contenido);
				$reporte = "";
				
				for($i=0 ; $i<$num ; $i++){
					$noti = mysql_fetch_object($notificaciones);	
					
					if (($i%2)==1){
						$reporte = $reporte . '<tr BGCOLOR="#F1F1F1">';
					}else{
						$reporte = $reporte . '<tr>';
					}
					
					
					$sql = "select *from categoria where cat_id=".$noti->not_bus_cat_id;
					$result = mysql_query($sql);
					$cat_obj = mysql_fetch_object($result);
					
					$sql = "select *from forma where for_id=".$noti->not_bus_for_id;
					$result = mysql_query($sql);
					$for_obj = mysql_fetch_object($result);
		
					$sql = "select *from departamento where dep_id=".$noti->not_bus_dep_id;
					$result = mysql_query($sql);
					$dep_obj = mysql_fetch_object($result);
					
					$filtro = strtolower($cat_obj->cat_nombre).' en '. strtolower($for_obj->for_descripcion).' en '. strtolower($dep_obj->dep_nombre);
					if($noti->not_bus_precio != ""){
						$filtro = $filtro . ' de ' . transformar_precio($noti->not_bus_precio);
					}
					
					
					$reporte = $reporte . '<td style="vertical-align: top; padding:4px 1px; line-height:16px;"> <font face="Verdana" size="2" color="#474747">' . $filtro  . '</font></td>';
					$reporte = $reporte . '<td style="vertical-align: top; padding:4px 1px; line-height:16px;"> <font face="Verdana" size="2" color="#474747">' . $noti->not_bus_descripcion  . '</font></td>';
					$reporte = $reporte . '<td style="padding:0 2px;">';
					//$reporte = $reporte . '<font face="Verdana" size="2" color="#00AEEF"><a style="color:#00AEEF;" href="http:'.'//www.toqueeltimbre.com/index.php?dir=lista-busqueda'.'&noti_bus_id='. $noti->not_bus_id .'&k='.$enc.'" target="_blank">' . 'Ver' . '</a></font>';
                    $reporte = $reporte . '<font face="Verdana" size="2" color="#00AEEF"><a style="color:#00AEEF;" href="http://toqueeltimbre.com/busqueda/detalle/'.$noti->not_bus_id.'" target="_blank">' . 'Ver' . '</a></font>';
					$reporte = $reporte . '</td>';
					$reporte = $reporte . '</tr>';
				}

				$contenido = str_replace('@@reporte',$reporte,$contenido);
				//$contenido = str_replace('@@link_baja',_URL_WEBSITE."index.php?dir=desactivar_mailing_noti_inmobiliaria&k=".$enc,$contenido);
				$cue_nombre=$fila->par_salida; 					
				$cue_pass=$fila->par_pas_salida;
					
				$mail = new PHPMailer(true);			
				$mail->SetLanguage('es');
				$mail->IsSMTP();
				$mail->SMTPAuth = true;                              
				$mail->Host = $fila->par_smtp;      		
				$mail->Timeout=30;
				$mail->CharSet = 'utf-8';							
				$mail->Subject = "Información de Interés de Toqueeltimbre.com";				
				$body =  $contenido;			
				$mail->MsgHTML($body);		
				$sw = validaCorreo($usu_obj->usu_email);	
				if( $sw ){
					$mail->AddAddress($usu_obj->usu_email);
                    //$mail->AddAddress("cristian.inarra@gmail.com");
				}
				$mail->SetFrom($cue_nombre, $fila->par_smtp);
				$mail->Username   = $cue_nombre;  
				$mail->Password   = $cue_pass;	
				  if( $sw ){
					try {
						$mail->Send();
						return true;					
	
					} catch (phpmailerException $e) {
	                        echo $e->errorMessage();
							return  false;
	
					} catch (Exception $e) {
                            echo $e->getMessage();
							return false;
					}	
				  }
			}else{
				return true;
			}
			return true;
	}
	
	$fecha_actual = date("Y-m-d");
	$fecha_actual_menos_1 = date("Y-m-d" ,time()-86400 ); //restamos 1 dia a la fecha actual
	
	/*$sql = "SELECT usu_id, usu_ciu_id from usuario
            where usu_tipo='Inmobiliaria' and usu_email_noti_inmobiliaria='1' and usu_estado='Habilitado' and usu_id NOT IN
                (
                    SELECT not_inm_usu_id from notificacion_inmobiliaria where not_inm_fecha='$fecha_actual'
                )
          ";
	*/
    $sql = "SELECT u.usu_id, u.usu_nombre, u.usu_email, u.usu_ciu_id FROM usuario u
            INNER JOIN suscripcion s ON (s.`usuario_id` = u.`usu_id` AND (CURDATE() >= s.`fecha_inicio` AND CURDATE() <= s.`fecha_fin`))
            WHERE u.usu_tipo='Inmobiliaria' AND u.usu_estado='Habilitado' AND u.usu_id NOT IN
                (
                    SELECT not_inm_usu_id from notificacion_inmobiliaria where not_inm_fecha='$fecha_actual'
                )
          ";
		
	$res = mysql_query($sql);
	$num = mysql_num_rows($res);
	
	for($i=0 ; $i<1 && $i<$num ; $i++){
	//for($i=0 ; $i<$num ; $i++){
		$usuario_obj=mysql_fetch_object($res);
		$usu_id = $objeto->usu_id;
		$departamento = obtener_departamento($usuario_obj->usu_ciu_id);
		
		//$sql3 = "SELECT * FROM notificaciones_busqueda where not_bus_vig_ini='$fecha_actual_menos_1' AND not_bus_dep_id = " . $departamento;
        $sql3 = "
        SELECT * FROM notificaciones_busqueda n
        WHERE TRIM(n.`not_bus_email`) <> ''
        AND TRIM(n.`not_bus_nombre`) <> ''
        AND n.not_bus_vig_ini = '$fecha_actual_menos_1'
        AND n.not_bus_dep_id = $departamento
        ";

		if( enviar_notificaciones( $sql3 , $usuario_obj ) == true ){
				$usu_id = $usuario_obj->usu_id;
				echo $usuario_obj->usu_nombre . '<br>';
				$sql="insert into notificacion_inmobiliaria(not_inm_usu_id,not_inm_fecha) values ( $usu_id ,'$fecha_actual')";
				mysql_query($sql);
		}		
	}
	
?>