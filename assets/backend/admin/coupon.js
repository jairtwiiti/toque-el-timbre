$(document).ready(function() {
    $("#canjear_cupon").click(function() {
        var url = $(this).attr('rel');
        var cupon = $('input[name="coupon"]').val();
        $.ajax({
            url: url,
            type: 'POST',
            data: {cupon: cupon},
            dataType: 'json',
            success: function(result) {
                switch(result.error) {
                    case -1:
                        $('.mensaje_cupon').html('<div class="alert alert-danger"><button class="close" aria-hidden="true" data-dismiss="alert" type="button"></button>El cupón es invalido.</div>');
                        return false;
                        break;
                    case -2:
                        $('.mensaje_cupon').html('<div class="alert alert-danger"><button class="close" aria-hidden="true" data-dismiss="alert" type="button"></button>El cupón ha expirado.</div>');
                        return false;
                        break;
                    case -3:
                        $('.mensaje_cupon').html('<div class="alert alert-danger"><button class="close" aria-hidden="true" data-dismiss="alert" type="button"></button>Este cupón no ha sido asignado a tu cuenta.</div>');
                        return false;
                        break;
                    case -4:
                        $('.mensaje_cupon').html('<div class="alert alert-danger"><button class="close" aria-hidden="true" data-dismiss="alert" type="button"></button>Este cupón ya ha sido utilizado.</div>');
                        return false;
                        break;
                    case 0:
                        var descuentoAjax = result.data;
                        descuentoAjax = descuentoAjax.descuento;
                        $('.mensaje_cupon').html('<div class="alert alert-success"><button class="close" aria-hidden="true" data-dismiss="alert" type="button"></button>El cupón valido.</div>');
                        $('#descuentoAjax').val(descuentoAjax);
                        $('#descuentoId').val(descuentoAjax.id);
                        var total = $('#paymentTotal').val();
                        var descuento = ((descuentoAjax / 100) * total);
                        $('#total_discount').html(descuento.toFixed(2));
                        $('#descuento').val(descuento.toFixed(2));
                        var new_total = total - descuento;
                        $('#total_payment').html(new_total);
                        $('#paymentTotalNew').val(new_total.toFixed(2));
                        break;
                }

            }
        });
    });
});