var MapsGoogle = function () {

    var mapGeocoding = function () {

        $("#check_mapa").click(function() {
            if($('input[name="marcar_mapa"]').is(':checked')) {
                $("#marcar_mapa_div").show();
                initialize();
            } else {
                $("#marcar_mapa_div").hide();
            }
        });

        var map;
        var geocoder;
        var marker;
        function initialize() {
            var latitude = $('#gmap_geocoding').attr("data-latitud");
            var longitude = $('#gmap_geocoding').attr("data-longitud");

            var marker_latitude = $('#gmap_geocoding').attr("data-marker-latitud");
            var marker_longitude = $('#gmap_geocoding').attr("data-marker-longitud");

            if(marker_latitude == "" && marker_longitude == "") {
                map = new google.maps.Map(document.getElementById('gmap_geocoding'), {
                    zoom: 14,
                    center: new google.maps.LatLng(latitude, longitude),
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                });
            }else{
                map = new google.maps.Map(document.getElementById('gmap_geocoding'), {
                    zoom: 14,
                    center: new google.maps.LatLng(marker_latitude, marker_longitude),
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                });
            }

            geocoder = new google.maps.Geocoder();
            if(marker_latitude != "" && marker_longitude != ""){
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(marker_latitude, marker_longitude),
                    map: map,
                    draggable:true
                });
            }else{
                marker = new google.maps.Marker({
                    draggable:true
                });
            }

            google.maps.event.addListener(map, 'click',function(event) {
                $('#latitude').attr("value", event.latLng.lat());
                $('#longitude').attr("value", event.latLng.lng());
                marker.setPosition(event.latLng);
                marker.setMap(map);
            });

            google.maps.event.addListener(marker, 'drag',function(event) {
                $('#latitude').attr("value", event.latLng.lat());
                $('#longitude').attr("value", event.latLng.lng());
            });

            google.maps.event.addListener(marker, 'dragend',function(event) {
                $('#latitude').attr("value", event.latLng.lat());
                $('#longitude').attr("value", event.latLng.lng());
            });

        }

        var handleAction2 = function () {
            var city = $('#gmap_geocoding').attr("data-city");
            var text = city + ", " + $.trim($('#gmap_geocoding_address').val());
            var address = {'address': text };
            geocoder.geocode(address, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    map.setCenter(results[0].geometry.location);

                    $('#latitude').attr("value", results[0].geometry.location.lat());
                    $('#longitude').attr("value", results[0].geometry.location.lng());
                    marker.setPosition(results[0].geometry.location);
                    marker.setMap(map);
                }
            });
        }
        google.maps.event.addDomListener(window, 'load', initialize);

        $('#gmap_geocoding_btn').click(function (e) {
            e.preventDefault();
            handleAction2();
        });

        $("#gmap_geocoding_address").keypress(function (e) {
            var keycode = (e.keyCode ? e.keyCode : e.which);
            if (keycode == '13') {
                e.preventDefault();
                handleAction2();
            }
        });


        $(document).ready(function(){
            $('.tab-pane').not(':first').hide();
            $('.nav-tabs li a').click(function (e) {
                $('.tab-pane').hide();
                $($(this).attr('href')).show();
                var center = map.getCenter();
                google.maps.event.trigger(map, "resize");
                map.setCenter(center);
            });
        });


        /*var map = new GMaps({
            div: '#gmap_geocoding',
            lat: $('#gmap_geocoding').attr("data-latitud"),
            lng: $('#gmap_geocoding').attr("data-longitud"),
            width: 600,
            height:400,
            click: function(e) {
                addMarkerAction(e);
            }
        });

        var addMarkerAction = function (e) {
            var add_latlng = e.latLng;
            $('#latitude').attr("value", add_latlng.lat());
            $('#longitude').attr("value", add_latlng.lng());

            map.addMarker({
                lat: add_latlng.lat(),
                lng: add_latlng.lng(),
                draggable: true,
                dragend: function(e) {
                    var new_latlng = e.latLng;
                    $('#latitude').attr("value", new_latlng.lat());
                    $('#longitude').attr("value", new_latlng.lng());
                }
            });
        }

        var handleAction = function () {
            var city = $('#gmap_geocoding').attr("data-city");
            var text = city + ", " + $.trim($('#gmap_geocoding_address').val());
            GMaps.geocode({
                address: text,
                callback: function (results, status) {
                    if (status == 'OK') {
                        var latlng = results[0].geometry.location;
                        map.setCenter(latlng.lat(), latlng.lng());
                        map.addMarker({
                            lat: latlng.lat(),
                            lng: latlng.lng(),
                            draggable: true,
                            dragend: function(e) {
                                var new_latlng = e.latLng;
                                //map.setCenter(new_latlng.lat(), new_latlng.lng());
                                $('#latitude').attr("value", new_latlng.lat());
                                $('#longitude').attr("value", new_latlng.lng());
                            }
                        });

                        Metronic.scrollTo($('#gmap_geocoding'));
                    }
                }
            });
        }*/

    }

    return {
        //main function to initiate map samples
        init: function () {
            mapGeocoding();
        }

    };

}();