var EcommerceProductsEdit = function () {
    //var url_ajax = "http://localhost/toqueeltimbre/ajax/upload_images";
    //var url_ajax = "http://toqueeltimbre.com/ajax/upload_images";
    var url_ajax = base_url+"ajax/upload_images";

    var handleImages = function() {
        var maxfiles = 20;
        // see http://www.plupload.com/
        var uploader = new plupload.Uploader({
            runtimes : 'html5,flash,silverlight,html4',
             
            browse_button : document.getElementById('tab_images_uploader_pickfiles'), // you can pass in id...
            container: document.getElementById('tab_images_uploader_container'), // ... or DOM Element itself
             
            url : url_ajax,
             
            filters : {
                max_file_size : '50mb',
                mime_types: [
                    {title : "Image files", extensions : "jpg,jpeg,gif,png"},
                    {title : "Zip files", extensions : "zip"}
                ]
            },
            multi_selection:false,
         
            // Flash settings
            flash_swf_url : 'assets/plugins/plupload/js/Moxie.swf',
     
            // Silverlight settings
            silverlight_xap_url : 'assets/plugins/plupload/js/Moxie.xap',             
         
            init: {
                PostInit: function() {
                    $('#tab_images_uploader_filelist').html("");
         
                    $('#tab_images_uploader_uploadfiles').click(function() {
                        uploader.start();
                        return false;
                    });

                    $('#tab_images_uploader_filelist').on('click', '.added-files .remove', function(){
                        uploader.removeFile($(this).parent('.added-files').attr("id"));    
                        $(this).parent('.added-files').remove();
                        $('#tab_images_uploader_container input[type="file"]').parent().show(); // provided there is only one #uploader_browse on page
                        $('#tab_images_uploader_pickfiles').show(); // provided there is only one #uploader_browse on page
                    });
                },
         
                FilesAdded: function(up, files) {
                    if (up.files.length >= maxfiles) {
                        $('#tab_images_uploader_container input[type="file"]').parent().hide(); // provided there is only one #uploader_browse on page
                        $('#tab_images_uploader_pickfiles').hide(); // provided there is only one #uploader_browse on page
                    }
                    plupload.each(files, function(file) {
                        $('#tab_images_uploader_filelist').append('<div class="alert alert-warning added-files" id="uploaded_file_' + file.id + '"> <span class="image_upload"></span>&nbsp;' + file.name + ' <span class="status label label-info"></span>&nbsp;<a href="javascript:;" class="remove label label-danger"><i class="fa fa-times"></i> Eliminar</a></div>');
                    });
                    uploader.start();
                    return false;
                },
         
                UploadProgress: function(up, file) {
                    $('#uploaded_file_' + file.id + ' > .status').html(file.percent + '%');
                },

                FileUploaded: function(up, file, response) {
                    var response = $.parseJSON(response.response);

                    if (response.result && response.status == 'OK') {
                        var filename = response.result; // uploaded file's unique name. Here you can collect uploaded file names and submit an jax request to your server side script to process the uploaded files and update the images tabke
                        var image = response.image;

                        $('#uploaded_file_' + file.id + ' > .status').removeClass("label-info").addClass("label-success").html('<i class="fa fa-check"></i> Subido ' + filename); // set successfull upload
                        $('#uploaded_file_' + file.id + ' > .image_upload').html(image);
                    } else {
                        $('#uploaded_file_' + file.id + ' > .status').removeClass("label-info").addClass("label-danger").html('<i class="fa fa-warning"></i> Error'); // set failed upload
                        Metronic.alert({type: 'danger', message: 'Una imagen no pudo ser subida. Intente nuevamente.', closeInSeconds: 10, icon: 'warning'});
                    }
                },
         
                Error: function(up, err) {
                    Metronic.alert({type: 'danger', message: err.message, closeInSeconds: 30, icon: 'warning'});
                }
            }
        });

        uploader.init();

    }


    var handleImagesPlanos = function() {
        var maxfiles = 2;
        // see http://www.plupload.com/
        var uploader = new plupload.Uploader({
            runtimes : 'html5,flash,silverlight,html4',

            browse_button : document.getElementById('tab_images_uploader_planos_pickfiles'), // you can pass in id...
            container: document.getElementById('tab_images_uploader_planos_container'), // ... or DOM Element itself

            url : url_ajax,

            filters : {
                max_file_size : '30mb',
                mime_types: [
                    {title : "Image files", extensions : "jpg,jpeg,gif,png"},
                    {title : "Zip files", extensions : "zip"}
                ]
            },
            multi_selection:false,

            // Flash settings
            flash_swf_url : 'assets/plugins/plupload/js/Moxie.swf',

            // Silverlight settings
            silverlight_xap_url : 'assets/plugins/plupload/js/Moxie.xap',

            init: {
                PostInit: function() {
                    $('#tab_images_uploader_planos_filelist').html("");

                    $('#tab_images_uploader_planos_uploadfiles').click(function() {
                        uploader.start();
                        return false;
                    });

                    $('#tab_images_uploader_planos_filelist').on('click', '.added-files .remove', function(){
                        uploader.removeFile($(this).parent('.added-files').attr("id"));
                        $(this).parent('.added-files').remove();
                        $('#tab_images_uploader_planos_container input[type="file"]').parent().show(); // provided there is only one #uploader_browse on page
                        $('#tab_images_uploader_planos_pickfiles').show(); // provided there is only one #uploader_browse on page
                    });
                },

                FilesAdded: function(up, files) {
                    if (up.files.length >= maxfiles) {
                        $('#tab_images_uploader_planos_container input[type="file"]').parent().hide(); // provided there is only one #uploader_browse on page
                        $('#tab_images_uploader_planos_pickfiles').hide(); // provided there is only one #uploader_browse on page
                    }
                    plupload.each(files, function(file) {
                        $('#tab_images_uploader_planos_filelist').append('<div class="alert alert-warning added-files" id="uploaded_file_' + file.id + '"> <span class="image_upload"></span>&nbsp;' + file.name + ' <span class="status label label-info"></span>&nbsp;<a href="javascript:;" class="remove label label-danger"><i class="fa fa-times"></i> Eliminar</a></div>');
                    });
                    uploader.start();
                    return false;
                },

                UploadProgress: function(up, file) {
                    $('#uploaded_file_' + file.id + ' > .status').html(file.percent + '%');
                },

                FileUploaded: function(up, file, response) {
                    var response = $.parseJSON(response.response);

                    if (response.result && response.status == 'OK') {
                        var filename = response.result; // uploaded file's unique name. Here you can collect uploaded file names and submit an jax request to your server side script to process the uploaded files and update the images tabke
                        var image = response.image;

                        $('#uploaded_file_' + file.id + ' > .status').removeClass("label-info").addClass("label-success").html('<i class="fa fa-check"></i> Subido ' + filename); // set successfull upload
                        $('#uploaded_file_' + file.id + ' > .image_upload').html(image);
                    } else {
                        $('#uploaded_file_' + file.id + ' > .status').removeClass("label-info").addClass("label-danger").html('<i class="fa fa-warning"></i> Error'); // set failed upload
                        Metronic.alert({type: 'danger', message: 'Una imagen no pudo ser subida. Intente nuevamente.', closeInSeconds: 10, icon: 'warning'});
                    }
                },

                Error: function(up, err) {
                    Metronic.alert({type: 'danger', message: err.message, closeInSeconds: 30, icon: 'warning'});
                }
            }
        });

        uploader.init();

    }

    var handleReviews = function () {

        var grid = new Datatable();

        grid.init({
            src: $("#datatable_reviews"),
            onSuccess: function (grid) {
                // execute some code after table records loaded
            },
            onError: function (grid) {
                // execute some code on network or other general error  
            },
            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options 

                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js). 
                // So when dropdowns used the scrollable div should be removed. 
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",

                "lengthMenu": [
                    [10, 20, 50, 100, 150, -1],
                    [10, 20, 50, 100, 150, "All"] // change per page values here
                ],
                "pageLength": 10, // default record count per page
                "ajax": {
                    "url": "demo/ecommerce_product_reviews.php", // ajax source
                },
                "columnDefs": [{ // define columns sorting options(by default all columns are sortable extept the first checkbox column)
                    'orderable': true,
                    'targets': [0]
                }],
                "order": [
                    [0, "asc"]
                ] // set first column as a default sort by asc
            }
        });
    }

    var handleHistory = function () {

        var grid = new Datatable();

        grid.init({
            src: $("#datatable_history"),
            onSuccess: function (grid) {
                // execute some code after table records loaded
            },
            onError: function (grid) {
                // execute some code on network or other general error  
            },
            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options 
                "lengthMenu": [
                    [10, 20, 50, 100, 150, -1],
                    [10, 20, 50, 100, 150, "All"] // change per page values here
                ],
                "pageLength": 10, // default record count per page
                "ajax": {
                    "url": "demo/ecommerce_product_history.php", // ajax source
                },
                "columnDefs": [{ // define columns sorting options(by default all columns are sortable extept the first checkbox column)
                    'orderable': true,
                    'targets': [0]
                }],
                "order": [
                    [0, "asc"]
                ] // set first column as a default sort by asc
            }
        });
    } 

    var initComponents = function () {
        //init datepickers
        $('.date-picker').datepicker({
            rtl: Metronic.isRTL(),
            autoclose: true
        });

        //init datetimepickers
        $(".datetime-picker").datetimepicker({
            isRTL: Metronic.isRTL(),
            autoclose: true,
            todayBtn: true,
            pickerPosition: (Metronic.isRTL() ? "bottom-right" : "bottom-left"),
            minuteStep: 10
        });

        //init maxlength handler
        $('.maxlength-handler').maxlength({
            limitReachedClass: "label label-danger",
            alwaysShow: true,
            threshold: 5
        });

    }

    var loadCitiesZone = function () {
        $('#state').on('change', function() {
            var url = $(this).attr("rel");
            $.ajax({
                type 		: 'POST',
                url 		: url + 'city',
                data 		: { 'departamento': $(this).val() },
                success 	: function(data) {
                    $('#city').html(data);
                }
            });

            $.ajax({
                type 		: 'POST',
                url 		: url + 'zone',
                data 		: { 'departamento': $(this).val() },
                success 	: function(data) {
                    $('#zone').html(data);
                }
            });
        });
    }

    var loadFeaturesAjax = function () {
        $('#categories').on('change', function() {
            var url = $(this).attr("rel");
            $.ajax({
                type 		: 'POST',
                url 		: url,
                data 		: { 'category': $(this).val() },
                success 	: function(data) {
                    $('#caracteristicas').html(data);
                }
            });
        });
    }

    var deleteImage = function () {
        $('.delete_image').click(function() {
            var obj = $(this);
            var url = obj.attr("href");
            var id_image = obj.attr("rel");
            $.ajax({
                type 		: 'POST',
                url 		: url,
                data 		: { 'image_id': id_image },
                success 	: function(data) {
                    obj.parent().parent().remove();
                }
            });

        });
    }

    var deletePlano = function () {
        $('.delete_plano').click(function() {
            var obj = $(this);
            var url = obj.attr("href");
            var id_image = obj.attr("rel");
            $.ajax({
                type 		: 'POST',
                url 		: url,
                data 		: { 'plano_id': id_image },
                success 	: function(data) {
                    obj.parent().remove();
                }
            });

        });
    }

    return {

        //main function to initiate the module
        init: function () {
            initComponents();

            handleImages();
            handleImagesPlanos();
            loadCitiesZone();
            loadFeaturesAjax();
            deleteImage();
            deletePlano();
            //handleReviews();
            //handleHistory();
        }

    };

}();