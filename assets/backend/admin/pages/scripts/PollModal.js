$(".row.response-2").slideUp();
$('.poll-modal').find(".modal-progress-bar").slideUp();

$('.deactivate-publication').on('change',function(){

    var isPublished = "Si";
    var propertyId = $(this).data('property-id');
    var $checkbox = $(this);
    if($(this).is(":checked") === false)
    {
        isPublished = "No";
        $('.poll-modal')
            .data('publication-id',$(this).data('publication-id'))
            .modal({
                backdrop: 'static',
                keyboard: false
            });
        $("form[name=poll-form]").find("input[name=publication-id]").val($(this).data('publication-id'));
    }
    $.ajax({
        method: "POST",
        url: base_url + "ajax/updatePublication",
        dataType: "json",
        data: {isPublished:isPublished,propertyId:propertyId},
        beforeSend:function()
        {
            $checkbox.closest(".checker").addClass("hide");
            $checkbox.prop("disabled",true);
            $checkbox.closest(".checkbox").find(".fa.fa-spinner.fa-pulse").removeClass("hide");
        },
        success:function()
        {
            setTimeout(function(){
                $checkbox.closest(".checker").removeClass("hide");
                $checkbox.prop("disabled",false);
                $checkbox.closest(".checkbox").find(".fa.fa-spinner.fa-pulse").addClass("hide");
            }, 2000);

        }
    });

});

$('input[name=property-sold]').on('change',function(){

    if($(this).val() == 0)
    {
        $(".row.response-1").slideUp();
        $(".row.response-2").slideDown();
    }
    else
    {
        $(".row.response-2").slideUp();
        $(".row.response-1").slideDown();
    }
});

$(".send-data").on("click",function()
{
    var propertySold = $("input[name=property-sold]:checked").val();
    var blockList = ["block-2", "block-1"];
    var blockToValidate = blockList[propertySold];
    var $form = $("form[name=poll-form]");
    if($form.parsley().isValid({group: blockToValidate}))
    {
        var formData = $("form[name=poll-form]").serialize();
        $.ajax({
            method: "POST",
            url: base_url + "ajax/saveSurvey",
            dataType: "json",
            data: formData,
            beforeSend:function()
            {
                $('.poll-modal').find(".modal-progress-bar").slideDown();
                $('.poll-modal').find("form[name=poll-form]").slideUp();
                $(".send-data").addClass("disabled");
                $(".send-data").text("Gracias por su cooperación..");

            },
            success:function()
            {
                setTimeout(function(){

                    $("input[name=sales-price]").val("");
                    $("input[name=property-sold]").closest("p").next().addClass("checked").next().removeClass("checked");
                    $('.poll-modal').modal('hide');
                    $('.poll-modal').find(".modal-progress-bar").slideUp();
                    $('.poll-modal').find("form[name=poll-form]").slideDown();
                    $("form[name=poll-form]").find("input[name=means-of-sale]:checked").parent().removeClass("checked");
                    $("form[name=poll-form]").find("input[name=means-of-sale]:checked").attr("checked",false);
                    $("form[name=poll-form]").find("textarea[name=no-sold-description]").val("");
                    $(".send-data").removeClass("disabled");
                    $(".send-data").text("Listo!");
                }, 2000);

            }
        });
    }
    else
    {
        $form.parsley().validate({group: blockToValidate});
    }
});