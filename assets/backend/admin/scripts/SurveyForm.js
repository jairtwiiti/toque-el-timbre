/**
 * Created by Jair on 05/07/2017.
 */

function SurveyForm()
{
    var id = Date.now();
    var primaryFilter = new PrimaryFilterBuilder();
    var secondaryFilter = new SecondaryFilterBuilder();

    var redirectTo = "";
    //getters
    this.getId = function()
    {
        return id;
    };

    this.eventHandler = function()
    {
        var formRequestHandler = this;
        $("#button_search_home, #advanced-search-form").on("click",function(){
            formRequestHandler.sendRequest();
        });
        $("#sort-type").on("change",function(){
            formRequestHandler.sendRequest();
        });
        $("a[data-layout]").on("click",function(e){
            e.preventDefault;
            $(this).data("layout", $(this).data("display"));
            formRequestHandler.sendRequest();
        });
    };
    this.parseToHttpQuery = function()
    {
        var filter1 = primaryFilter.getPrimaryFilter();
        var auxSecondaryFilter = secondaryFilter.getSecondaryFilter();
        var filter2 = auxSecondaryFilter!=""?"?"+auxSecondaryFilter:"";
        redirectTo = filter1+filter2;
    };

    this.sendRequest = function()
    {
        this.parseToHttpQuery();
        window.location = base_url+"buscar/"+redirectTo;
        //console.log(base_url+"buscar/"+redirectTo);

    };


}
function PrimaryFilterBuilder()
{
    var id = Date.now();
    var primaryFilter = "";
    this.getId = function()
    {
        return id;
    };
    var buildFilter = function()
    {
        var arrayObjectVariables = [];
        primaryFilter = "";
        arrayObjectVariables.push(new VariableHandler("type","inmueble"));
        arrayObjectVariables.push(new VariableHandler("in","transaccion"));
        arrayObjectVariables.push(new VariableHandler("city","ciudad"));

        $.each(arrayObjectVariables,function(index,variable)
        {
            var separator = "-en-";
            var value = typeof $("select[name="+variable.getKeyword()+"] option:selected").val() == "undefined"?"":$("select[name="+variable.getKeyword()+"] option:selected").val();
            variable.setValue(value);
            if(index == (arrayObjectVariables.length-1))
            {
                separator = "";
            }
            primaryFilter+= variable.getAsPrimaryFilter()+separator;
        });
    };

    this.getPrimaryFilter = function()
    {
        buildFilter();
        return primaryFilter;
    };

}

function SecondaryFilterBuilder()
{
    var id = Date.now();
    var secondaryFilter = "";

    this.getId = function()
    {
        return id;
    };
    var buildFilter = function()
    {
        var arrayObjectVariables = [];
        secondaryFilter = "";
        arrayObjectVariables.push(new VariableHandler("room","habitacion"));
        arrayObjectVariables.push(new VariableHandler("bathroom","bano"));
        arrayObjectVariables.push(new VariableHandler("parking","parqueo"));
        arrayObjectVariables.push(new VariableHandler("currency","moneda"));
        arrayObjectVariables.push(new VariableHandler("price_min","desde"));
        arrayObjectVariables.push(new VariableHandler("price_max","hasta"));
        arrayObjectVariables.push(new VariableHandler("sort","ordenar"));
        arrayObjectVariables.push(new VariableHandler("layout","tipo"));

        $.each(arrayObjectVariables,function(index,filter)
        {
            var separator = "&";
            var value = "";
            switch(filter.getKeyword())
            {
                case "currency":
                    value = $(".search_box .switch-button-label.on").text();
                    if(value != "")
                    {
                        value = value == "BOB"?"Bolivianos":"Dolares";
                    }
                    break;
                case "price_min":
                case "price_max":
                    value = typeof $("#"+filter.getKeyword()).val() == "undefined"?"":$("#"+filter.getKeyword()).val();
                    break;
                case "layout":
                    value = typeof $("a[data-layout]").data("layout") == "undefined"?"":$("a[data-layout]").data("layout");
                    break;
                default:
                    value = typeof $("select[name="+filter.getKeyword()+"] option:selected").val() == "undefined"?"":$("select[name="+filter.getKeyword()+"] option:selected").val();
            }
            filter.setValue(value);
            if(filter.getAsSecondaryFilter() != "")
            {
                secondaryFilter+= separator+filter.getAsSecondaryFilter();
            }
        });
        secondaryFilter = secondaryFilter.substr(1);
    };

    this.getSecondaryFilter = function()
    {
        buildFilter();
        return secondaryFilter;
    };

}

function VariableHandler(keyword, label)
{
    var id = Date.now();
    var keyword = keyword;
    var label = label;
    var value = "";

    this.setValue = function(val)
    {
        value = val;
    };
    this.getKeyword = function()
    {
        return keyword;
    };
    var removeBlankSpaces = function()
    {
        value = value.replace(/ /g,"-");
    };

    this.getAsPrimaryFilter = function()
    {
        removeBlankSpaces();
        return value;
    };
    this.getAsSecondaryFilter = function()
    {
        var response = "";
        if(value!="")
        {
            response = label+"="+value;
        }
        return response;
    };
}
var formRequestHandler = new FormRequestHandler();
formRequestHandler.eventHandler();