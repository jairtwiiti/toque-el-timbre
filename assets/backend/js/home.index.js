/**
 * Created by Jair on 12/09/2017.
 */
$(document).ready(function() {
    $('input.date-picker').datepicker();
    var initialParams = {
        startDate: "",
        endDate: "",
        userType: "",
        propertyTypeId: "",
        transactionTypeId: "",
        onlyVisiblePublications:"1",
        getDemand:"0",
        reportType:"0"
    };
    getData(initialParams,"admin/AjaxUser/getAllUsersRegisteredByDateRange","chart-users-registered","stackedColumns", "Reporte de usuarios");
    getData(initialParams,"admin/AjaxProperty/getAllPublicationsActivityByDateRange","chart-publications-activity","createMultipleValueAxes", "Reporte de publicaciones");
    getData(initialParams,"admin/AjaxPayment/getAllPaymentsSettlementByDateRange","chart-payments-settlement-registered","createSingleChart", "Reporte de pagos");
    getData(initialParams,"admin/AjaxProperty/getAllMessagesToPublicationsByDateRange","chart-messages-to-publications","stackedColumns", "Reporte de mensajes");
    //offer
    getData(initialParams,"admin/AjaxProperty/getPropertyOffersBasedOnUsers","chart-property-offers-based-on-users","createPieChart", "Oferta segun el tipo de usuario");
    getData(initialParams,"admin/AjaxProperty/getPropertyOffersBasedOnTransactionsType","chart-property-offers-based-on-transactions-type","createPieChart", "Oferta segun el tipo de transaccion");
    getData(initialParams,"admin/AjaxProperty/getPropertyOffersBasedOnPropertyTypes","chart-property-offers-based-on-property-types","createPieChart", "Oferta segun el tipo de inmueble");
    //demand
    initialParams.getDemand = "1";
    getData(initialParams,"admin/AjaxProperty/getPropertyOffersBasedOnUsers","chart-property-offers-based-on-users-demand","createPieChart", "Demanda segun el tipo de usuario");
    getData(initialParams,"admin/AjaxProperty/getPropertyOffersBasedOnTransactionsType","chart-property-offers-based-on-transactions-type-demand","createPieChart", "Demanda segun el tipo de transaccion");
    getData(initialParams,"admin/AjaxProperty/getPropertyOffersBasedOnPropertyTypes","chart-property-offers-based-on-property-types-demand","createPieChart", "Demanda segun el tipo de inmueble");

    //get a single chart
    $(document).on("click",".chart-search-data",function(e)
    {
        e.preventDefault();
        var contentData = $(this).data("content-data");
        var requestInfo = getRequestInfo(contentData);
        getData(requestInfo.params, requestInfo.sourceData, requestInfo.contentData, requestInfo.charType, requestInfo.reportName);
    });

    //get the offer that is composed by 3 charts
    $(document).on("click",".chart-search-offer",function(e){
        e.preventDefault();
        var startDate = $(this).closest(".portlet-title").find("input[name=start-date]").val().split('-');
        startDate = startDate[2]+"-"+startDate[1]+"-"+startDate[0];
        var endDate = $(this).closest(".portlet-title").find("input[name=end-date]").val().split('-');
        endDate = endDate[2]+"-"+endDate[1]+"-"+endDate[0];

        var requestInfo = getRequestInfo("chart-property-offers-based-on-property-types");
        requestInfo.params.startDate = startDate;
        requestInfo.params.endDate = endDate;
        getData(requestInfo.params, requestInfo.sourceData, requestInfo.contentData, requestInfo.charType, requestInfo.reportName);

        var requestInfo = getRequestInfo("chart-property-offers-based-on-transactions-type");
        requestInfo.params.startDate = startDate;
        requestInfo.params.endDate = endDate;
        getData(requestInfo.params, requestInfo.sourceData, requestInfo.contentData, requestInfo.charType, requestInfo.reportName);

        var requestInfo = getRequestInfo("chart-property-offers-based-on-users");
        requestInfo.params.startDate = startDate;
        requestInfo.params.endDate = endDate;
        getData(requestInfo.params, requestInfo.sourceData, requestInfo.contentData, requestInfo.charType, requestInfo.reportName);
    });

    //get the demand that is composed by 3 charts
    $(document).on("click",".chart-search-demand",function(e){
        e.preventDefault();
        var startDate = $(this).closest(".portlet-title").find("input[name=start-date]").val().split('-');
        startDate = startDate[2]+"-"+startDate[1]+"-"+startDate[0];
        var endDate = $(this).closest(".portlet-title").find("input[name=end-date]").val().split('-');
        endDate = endDate[2]+"-"+endDate[1]+"-"+endDate[0];

        var requestInfo = getRequestInfo("chart-property-offers-based-on-property-types-demand");
        requestInfo.params.startDate = startDate;
        requestInfo.params.endDate = endDate;
        requestInfo.params.getDemand = "1";
        getData(requestInfo.params, requestInfo.sourceData, requestInfo.contentData, requestInfo.charType, requestInfo.reportName);

        var requestInfo = getRequestInfo("chart-property-offers-based-on-transactions-type-demand");
        requestInfo.params.startDate = startDate;
        requestInfo.params.endDate = endDate;
        requestInfo.params.getDemand = "1";
        getData(requestInfo.params, requestInfo.sourceData, requestInfo.contentData, requestInfo.charType, requestInfo.reportName);

        var requestInfo = getRequestInfo("chart-property-offers-based-on-users-demand");
        requestInfo.params.startDate = startDate;
        requestInfo.params.endDate = endDate;
        requestInfo.params.getDemand = "1";
        getData(requestInfo.params, requestInfo.sourceData, requestInfo.contentData, requestInfo.charType, requestInfo.reportName);
    });

    $(document).on("click",".master-search-data",function(e){
        e.preventDefault();
        var masterStartDate = $("input[name=master-start-date]").val();
        var masterEndDate = $("input[name=master-end-date]").val();
        $("input[name=start-date]").val(masterStartDate);
        $("input[name=end-date]").val(masterEndDate);
        $(".chart-search-data").trigger("click");
        $(".chart-search-offer").trigger("click");
        $(".chart-search-demand").trigger("click");

    });
});
AmCharts.addInitHandler( function( chart ) {
    var _this = chart.export; // Chart export instance
    var toCSV = _this.toCSV; // toCSV method backup
    var toXLSX = _this.toXLSX; // toXLSX method backup
    /*
     ** REPLACE toXLSX METHOD TO MODIFY IT'S BEHAVIOUR
     */
    _this.toXLSX = function( options, callback ) {
        var requestInfo = getRequestInfo($(_this.setup.chart.div).attr("id"));
        requestInfo.params.reportType = "1";
        options.fileName = requestInfo.reportName;
        $.ajax({
            url : base_url + requestInfo.sourceData,
            dataType  :"json",
            type : "POST",
            data: requestInfo.params,
            success:function(data){
                var cfg = _this.deepMerge({
                    name: "Hoja 1",
                    dateFormat: _this.config.dateFormat || "dateObject",
                    SheetNames: [],
                    Sheets: {},
                    withHeader: true,
                    stringify: false,
                    data:data
                }, options || {}, true );
                exportXlsx(_this, cfg, callback);
            }
        });


    }

}, [ "pie", "serial", "xy", "funnel", "radar", "gauge", "stock", "map", "gantt" ] );
function getData(params, sourceData, content, chartType, reportName)
{
    var params = params;
    $.ajax({
        url : base_url + sourceData,
        dataType  :"json",
        type : "POST",
        data : params,
        beforeSend:function()
        {
            var html = "";
            html+="<div class='progress'>";
            html+="<div class='progress-bar progress-bar-striped active' role='progressbar' aria-valuenow='100' aria-valuemin='0' aria-valuemax='100' style='width: 100%'></div>";1
            html+="</div>";
            $("#"+content).html(html);
            $("#"+content).closest(".portlet-body").find(".total-register-box").html(html);

        },
        success:function(response){
            var totalRegisters = response.length;
            if(totalRegisters > 0)
            {
                var createChart = window[chartType];
                var totalRegisterBoxHtml = getTotalRegisterHtml(response,chartType,params);
                $("#"+content).closest(".portlet-body").find(".total-register-box").html(totalRegisterBoxHtml);
                createChart(response, content);
            }
            else
            {
                $("#"+content).text("No se encontraron registros!");
                $("#"+content).closest(".portlet-body").find(".total-register-box").html("");
            }
        }
    });
}
function createSingleChart(data, content)
{
    var jsonData = data;
    var dataContent = content;
    var chart = AmCharts.makeChart(dataContent, {
        "type": "serial",
        "theme": "light",
        "language": "es",
        "marginRight": 40,
        "marginLeft": 40,
        "autoMarginOffset": 20,
        "mouseWheelZoomEnabled":true,
        "dataDateFormat": "YYYY-MM-DD",
        "valueAxes": [{
            "id": "v1",
            "axisAlpha": 0,
            "position": "left",
            "ignoreAxisWidth":true
        }],
        "balloon": {
            "borderThickness": 1,
            "shadowAlpha": 0
        },
        "graphs": [{
            "id": "g1",
            "balloon":{
                "drop":true,
                "adjustBorderColor":false,
                "color":"#ffffff"
            },
            "bullet": "round",
            "bulletBorderAlpha": 1,
            "bulletColor": "#FFFFFF",
            "bulletSize": 5,
            "hideBulletsCount": 50,
            "lineThickness": 2,
            "title": "red line",
            "useLineColorForBulletBorder": true,
            "valueField": "value",
            "balloonText": "<span style='font-size:18px;'>[[value]]</span>"
        }],
        "chartScrollbar": {
            "graph": "g1",
            "oppositeAxis":false,
            "offset":30,
            "scrollbarHeight": 80,
            "backgroundAlpha": 0,
            "selectedBackgroundAlpha": 0.1,
            "selectedBackgroundColor": "#888888",
            "graphFillAlpha": 0,
            "graphLineAlpha": 0.5,
            "selectedGraphFillAlpha": 0,
            "selectedGraphLineAlpha": 1,
            "autoGridCount":true,
            "color":"#AAAAAA"
        },
        "chartCursor": {
            "pan": true,
            "valueLineEnabled": true,
            "valueLineBalloonEnabled": true,
            "cursorAlpha":1,
            "cursorColor":"#258cbb",
            "limitToGraph":"g1",
            "valueLineAlpha":0.2,
            "valueZoomable":true
        },
        "valueScrollbar":{
            "oppositeAxis":false,
            "offset":50,
            "scrollbarHeight":10
        },
        "categoryField": "date",
        "categoryAxis": {
            "parseDates": true,
            "dashLength": 1,
            "minorGridEnabled": true
        },
        "export": {
            "enabled": true,
            "fileName": "tessss",
        },
        "dataProvider": jsonData
    });

    chart.addListener("rendered", zoomChart);
    zoomChart();
    function zoomChart() {
        chart.zoomToIndexes(chart.dataProvider.length - 40, chart.dataProvider.length - 1);
    }
}

function createMultipleValueAxes(data, content)
{
    var jsonData = data;
    var dataContent = content;
    var chart = AmCharts.makeChart(dataContent, {
        "type": "serial",
        "theme": "light",
        "language": "es",
        "legend": {
            "useGraphSettings": true
        },
        "dataProvider": jsonData,
        "synchronizeGrid":true,
        "dataDateFormat": "YYYY-MM-DD",
        "valueAxes": [{
            "id":"v1",
            "axisColor": "#eea236",
            "axisThickness": 2,
            "axisAlpha": 1,
            "position": "left"
        }, {
            "id":"v2",
            "axisColor": "#2e6da4",
            "axisThickness": 2,
            "axisAlpha": 1,
            "position": "right"
        }/*, {
            "id":"v3",
            "axisColor": "#d43f3a",
            "axisThickness": 2,
            "gridAlpha": 0,
            "offset": 40,
            "axisAlpha": 1,
            "position": "left"
        }*/],
        "graphs": [{
            "valueAxis": "v1",
            "lineColor": "#eea236",
            "bullet": "round",
            "bulletBorderThickness": 1,
            "hideBulletsCount": 30,
            "title": "Actividad",
            "valueField": "Actividad",
            "fillAlphas": 0
        }, {
            "valueAxis": "v2",
            "lineColor": "#2e6da4",
            "bullet": "square",
            "bulletBorderThickness": 1,
            "hideBulletsCount": 30,
            "title": "Creadas",
            "valueField": "Creadas",
            "fillAlphas": 0
        }, {
            "valueAxis": "v2",
            "lineColor": "#d43f3a",
            "bullet": "triangleUp",
            "bulletBorderThickness": 1,
            "hideBulletsCount": 30,
            "title": "Vencidas",
            "valueField": "Vencidas",
            "fillAlphas": 0
        }],
        "chartScrollbar": {},
        "chartCursor": {
            "cursorPosition": "mouse"
        },
        "categoryField": "date",
        "categoryAxis": {
            "parseDates": true,
            "axisColor": "#DADADA",
            "minorGridEnabled": true
        },
        "export": {
            "enabled": true,
            "position": "bottom-right"
        }
    });
}
function createPieChart(data, content)
{
    var jsonData = data;
    var dataContent = content;
    var chart = AmCharts.makeChart( dataContent, {
        "type": "pie",
        "theme": "light",
        "language": "es",
        "dataProvider":jsonData,
        "valueField": "total",
        "titleField": "type",
        "outlineAlpha": 0.4,
        "depth3D": 15,
        "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
        "angle": 0,
        "radius":90,
        "export": {
            "enabled": true
        }
    } );
}

function stackedColumns(data, content)
{
    var jsonData = data;
    var dataContent = content;
    var chart = AmCharts.makeChart(dataContent, {
        "type": "serial",
        "theme": "light",
        "language": "es",
        "legend": {
            "autoMargins": false,
            "equalWidths": false,
            "horizontalGap": 10,
            "markerSize": 10,
            "useGraphSettings": true,
            "valueAlign": "left",
            "valueWidth": 0
        },
        "dataDateFormat": "YYYY-MM-DD",
        "dataProvider": jsonData,
        "valueAxes": [{
            "stackType": "regular",
            "axisAlpha": 0.3,
            "gridAlpha": 0
        }],
        "graphs": [{
            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
            "fillAlphas": 0.8,
            "labelText": "[[value]]",
            "lineAlpha": 0.3,
            "title": "Particular",
            "type": "column",
            "color": "#000000",
            "valueField": "Particular"
        }, {
            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
            "fillAlphas": 0.8,
            "labelText": "[[value]]",
            "lineAlpha": 0.3,
            "title": "Inmobiliaria",
            "type": "column",
            "color": "#000000",
            "valueField": "Inmobiliaria"
        }, {
            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
            "fillAlphas": 0.8,
            "labelText": "[[value]]",
            "lineAlpha": 0.3,
            "title": "Constructora",
            "type": "column",
            "color": "#000000",
            "valueField": "Constructora"
        }, {
            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
            "fillAlphas": 0.8,
            "labelText": "[[value]]",
            "lineAlpha": 0.3,
            "title": "Buscador",
            "type": "column",
            "color": "#000000",
            "valueField": "Buscador"
        }],
        "categoryField": "date",
        "categoryAxis": {
            "parseDates": true,
            "gridPosition": "start",
            "axisAlpha": 0,
            "gridAlpha": 0,
            "position": "left",
            "labelRotation": 45
        },
        "export": {
            "enabled": true,
            "dateFormat": "DD-MM-YY"
        }
    });
}

function getTotalRegisterHtml(response, chartType,params)
{
    var totalRegisters = 0;
    switch(chartType)
    {
        case "createPieChart":
            $.each(response, function(index, value)
            {
                totalRegisters += parseInt(response[index].total);
            });
            totalRegisters+=9;// 9 = total projects
            break;
        case "stackedColumns":
            $.each(response, function(index, value)
            {
                totalRegisters += parseInt(response[index].Particular) + parseInt(response[index].Inmobiliaria) + parseInt(response[index].Constructora)  + parseInt(response[index].Buscador === undefined?0:response[index].Buscador);
            });
            break;
        case "createMultipleValueAxes":
            totalRegisters = [];
            $.each(response, function(index, value)
            {
                var counter = 0;
                $.each(value,function(i,v){
                    if(counter > 0)
                    {
                        totalRegisters[i] = totalRegisters[i] == undefined?0:totalRegisters[i];
                        totalRegisters[i] += parseInt(v);
                    }
                    counter++;
                });

                //totalRegisters += parseInt(response[index].Particular) + parseInt(response[index].Inmobiliaria) + parseInt(response[index].Constructora);
            });
            totalRegisters["Actividad"] = parseInt(totalRegisters["Actividad"]/response.length);
            break;
        default:
            $.each(response, function(index, value)
            {
                totalRegisters += parseInt(response[index].value);
            });
            break;
    }
    var labelText = " Registros";
    if(params.getDemand == "1")
    {
        //labelText = " Visitias";
    }

    var totalRegisterBoxHtml = "";
    if(Array.isArray(totalRegisters))
    {
        //$.each(totalRegisters,function(index, value){
            totalRegisterBoxHtml += "<span class='label label-warning'>"+totalRegisters.Actividad+"<em>Activas</em></span>";
            totalRegisterBoxHtml += "<span class='label label-warning'>"+totalRegisters.Creadas+" <em>Creadas</em></span>";
            totalRegisterBoxHtml += "<span class='label label-warning'>"+totalRegisters.Vencidas+" <em>Vencidas</em></span>";
        //});
    }
    else
    {
        totalRegisterBoxHtml = "<span class='label label-warning'>"+totalRegisters+" <em>"+labelText+"</em></span>";
    }

    return totalRegisterBoxHtml;
}
function datenum( v, date1904 ) {
    if ( date1904 ) v += 1462;
    var epoch = Date.parse( v );
    return ( epoch - new Date( Date.UTC( 1899, 11, 30 ) ) ) / ( 24 * 60 * 60 * 1000 );
}

function sheet_from_array_of_arrays( data, opts ) {
    var ws = {};
    var range = {
        s: {
            c: 10000000,
            r: 10000000
        },
        e: {
            c: 0,
            r: 0
        }
    };
    for ( var R = 0; R != data.length; ++R ) {
        for ( var C = 0; C != data[ R ].length; ++C ) {
            if ( range.s.r > R ) range.s.r = R;
            if ( range.s.c > C ) range.s.c = C;
            if ( range.e.r < R ) range.e.r = R;
            if ( range.e.c < C ) range.e.c = C;
            var cell = {
                v: data[ R ][ C ]
            };
            if ( cell.v == null ) continue;
            var cell_ref = XLSX.utils.encode_cell( {
                c: C,
                r: R
            } );

            //if ( typeof eval(cell.v) === "number" ) cell.t = "n";
            var dat = new Date(cell.v);
            if ($.isNumeric(cell.v)) cell.t = "n";
            else if ( typeof cell.v === "boolean" ) cell.t = "b";
            // else if ( cell.v instanceof Date ) {
            else if ( dat != "Invalid Date") {
                cell.t = "n";
                cell.z = XLSX.SSF._table[ 14 ];
                cell.v = datenum( cell.v );
            } else cell.t = "s";

            ws[ cell_ref ] = cell;
        }
    }
    if ( range.s.c < 10000000 ) ws[ "!ref" ] = XLSX.utils.encode_range( range );
    return ws;
}
function exportXlsx(_this, cfg, callback)
{
    var data = "";
    var wb = {
        SheetNames: [],
        Sheets: {}
    }
    // GET CHART DATA
    cfg.data = cfg.data ? cfg.data : _this.getChartData( cfg );

    // ARRAY MODIFICATION
    if ( _this.config.postDataProcessor instanceof Function ) {
        cfg.data = _this.config.postDataProcessor( "ARRAY", cfg.data );

        // GATHER POSSIBLE NEW FIELDS
        cfg.data = _this.getChartData( {
            data: cfg.data
        } );

        // GIVEN SHEETS
    } else if ( _this.config.postDataProcessor instanceof Object ) {
        cfg.SheetNames = _this.config.postDataProcessor.SheetNames || [];
        cfg.Sheets = _this.config.postDataProcessor.Sheets || {};
    }

    // DEFINE DEFAULT DATA SHEET
    cfg.Sheets[ cfg.name ] = cfg.data;
    if ( cfg.SheetNames.indexOf( cfg.name ) == -1 ) {
        cfg.SheetNames.unshift( cfg.name );
    }

    if ( _this.config.postDataProcessor instanceof Function ) {
        cfg = _this.config.postDataProcessor( "SHEETS", cfg );
    }

    // CRAWL THROUGH GIVEN SHEETS
    for ( var i1 = 0; i1 < cfg.SheetNames.length; i1++ )
    {
        var sheetName = cfg.SheetNames[ i1 ];
        var sheetData = _this.toArray( {
            data: cfg.Sheets[ sheetName ],
            withHeader: true,
            stringify: false
        } );
        // PREPEND; APPEND
        if ( _this.config.postDataProcessor instanceof Object ) {

            if ( _this.config.postDataProcessor.prepend ) {
                sheetData.unshift([_this.config.postDataProcessor.prepend]);
            }

            if ( _this.config.postDataProcessor.append ) {
                sheetData.push([_this.config.postDataProcessor.append]);
            }

        }
        wb.SheetNames.push( sheetName );
        wb.Sheets[ sheetName ] = sheet_from_array_of_arrays( sheetData );
    }

    data = XLSX.write( wb, {
        bookType: "xlsx",
        bookSST: true,
        type: "base64"
    } );
    data = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," + data;
    _this.handleCallback( callback, data );
    return data;
}
function getRequestInfo(chartDivContent)
{
    var $chart =  $("#"+chartDivContent);
    var requestInfo = {};
    var date = getDateRange(chartDivContent);
    var sourceData = $chart.data("source-data");//got it
    var contentData = chartDivContent;
    var charType = $chart.data("chart-type");//got it
    var propertyType = $("select[name=property-type][data-chart="+chartDivContent+"]").select2("data").id;//got it
    var transactionType = $("select[name=transaction-type][data-chart="+chartDivContent+"]").select2("data").id;//got it
    var userType = $("select[name=user-type][data-chart="+chartDivContent+"]").select2("data").id;//got it
    var onlyVisiblePublications = $("select[name=only-visible-publications][data-chart="+chartDivContent+"]").select2("data").id;//got it
    var reportName = $chart.data("report-name");//got it
    var params = {
        startDate: date.start,
        endDate: date.end,
        userType: userType,
        propertyTypeId: propertyType,
        transactionTypeId: transactionType,
        onlyVisiblePublications: onlyVisiblePublications,
        getDemand:"0",
        reportType:"0"
    };
    requestInfo.params = params;
    requestInfo.sourceData = sourceData;
    requestInfo.contentData = contentData;
    requestInfo.charType = charType;
    requestInfo.reportName = reportName;
    return requestInfo;
}

function getDateRange(chartDivContent)
{
    var dateRange = {};
    var offerCharts = ["chart-property-offers-based-on-property-types","chart-property-offers-based-on-transactions-type","chart-property-offers-based-on-users"];
    var demandCharts = ["chart-property-offers-based-on-property-types-demand","chart-property-offers-based-on-transactions-type-demand","chart-property-offers-based-on-users-demand"];

    if(offerCharts.includes(chartDivContent))
    {
        chartDivContent = "chart-property-offers-based-on-property-types";
    }
    else if(demandCharts.includes(chartDivContent))
    {
        chartDivContent = "chart-property-offers-based-on-property-types-demand";
    }

    var $dateRangeContent =  $("div[data-date-range-chart="+chartDivContent+"]");
    var start = $dateRangeContent.find("input[name=start-date]").val().split('-');
    start = start[2]+"-"+start[1]+"-"+start[0];
    var end = $dateRangeContent.find("input[name=end-date]").val().split('-');
    end = end[2]+"-"+end[1]+"-"+end[0];
    dateRange.start = start;
    dateRange.end = end;
    return dateRange;
}
