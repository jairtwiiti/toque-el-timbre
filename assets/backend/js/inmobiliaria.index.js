/**
 * Created by Jair on 20/11/2017.
 */
$(document).ready(function($) {
    getProperties();

    var visitorTypeId = $("#real-state-id").val();
    if(visitorTypeId > 0)
    {
        var visitor = new Visitor("User Inmobiliaria", visitorTypeId);
        visitor.register();
    }


    if($( window ).width() >= 975)
    {
        $('.project-contact-form').followTo();
    }

    $("#form-real-state-contact input[type=submit]").click(function(e){
        e.preventDefault();
        if(!$("#form-real-state-contact").parsley().isValid())
        {
            $("#form-real-state-contact").parsley().validate();
        }
        else {
            grecaptcha.execute();
        }
    });


});

$.fn.followTo = function () {
    var $this = this,
        $window = $(window);
    $window.scroll(function(e){
        var pos = ($("#col-detail").height() - $(".project-contact-form").height()) + $("nav").height() - 9;
        if ($window.scrollTop() >= 141 && $window.scrollTop() <= pos)
        {
            // console.log("1",$window.scrollTop(),pos);
            $this.css({
                position: 'fixed',
                top: 10,
                width:"263px",
                marginRight:"104px",
                paddingBottom:"48px"
            });
        }
        else if($window.scrollTop() > pos)
        {
            // console.log("2",$window.scrollTop(),pos);
            $this.css({
                position: 'absolute',
                top: pos-141,//pos,
                marginRight:"15px",
                paddingBottom:"48px"
            });
        }
        else
        {
            // console.log("3",$window.scrollTop(),pos);
            $this.css({
                position: 'absolute',
                top: 0,
                marginRight:"15px",
                paddingBottom:"48px"
            });
        }
    });
};

function sendRequestRealEstate(token)
{
    var postData = $("#form-real-state-contact").serializeArray();
    var formURL = base_url + 'AjaxUser/sendContactMessage';
    var buttonSubmit = $("#form-real-state-contact").find("input[type=submit]");
    $.ajax({
        url : formURL,
        dataType: "json",
        type: "POST",
        data : postData,
        beforeSend: function()
        {
            $(buttonSubmit)
                .prop("disabled",true)
                .val("Enviando...");
        },
        success:function(data, textStatus, jqXHR)
        {
            if(data.response == 1)
            {
                $("#form-real-state-contact")[0].reset();
                $("#box_contact_agent .result").html(data.message);
                location.reload();
            }
            else if(data.response == 2)
            {
                launchQuickLoginForm();
            }
            $(buttonSubmit)
                .prop("disabled",false)
                .val("Enviar");
            $("#box_contact_agent .result").html(data.message);
            grecaptcha.reset();
        }
    });
}

function getProperties()
{
    var userId = $("input[name=user-id]").val();
    $(".paginations").pagination({
        dataSource: base_url + "AjaxPublication/getByRealStateId",
        locator: 'result',
        totalNumberLocator: function(response) {
            // you can return totalNumber by analyzing response content
            return response.totalResult;
        },
        pageSize: 6,
        ajax: {
            type:"POST",
            data:{userId:userId},
            dataType: "json",
            beforeSend: function() {
                // $(overlay).show();
            }
        },
        callback: function(data, pagination) {

            // template method of yourself
            $("#real-state-publications").html('');
            $(".real-state-total-properties").text(pagination.totalNumber);
            var htmlSource   = $("#ht-publication-quick-view").html();
            var template = Handlebars.compile(htmlSource);
            var searchResult = "";
            var thumbnailSettings = {
                colDefinition:"col-xs-12 col-sm-6 col-md-6 col-lg-4",
                hidePublicationName: ""
            };
            $.each(data,function(index, value){
                var data = {publication:value,thumbnailSettings:thumbnailSettings};
                var html    = template(data);
                searchResult += html;
            });
            $("#real-state-publications").html(searchResult);
        }
    });
}
