/**
 * Created by Jair on 01/03/2018.
 */
var reCaptchaWidget2 = "";

$(document).ready(function($) {
    $(document).on("click",".signin-signup-button",function(e){
        e.preventDefault();
        var $form = $("form.form-signin-signup");
        if(!$form.parsley().isValid())
        {
            $form.parsley().validate();
        }
        else {
            if($(".register-info").is(":visible"))
            {
                $(this).prop("disabled", true);
                if($("#signin-signup-recaptcha").html("") || reCaptchaWidget2 === "")
                {
                    reCaptchaWidget2 = grecaptcha.render('signin-signup-recaptcha', {
                        'sitekey' : $form.find('input[name=site-key]').val(),
                        'callback' : loginRegister,
                        'size' : 'invisible'
                    });
                }
                grecaptcha.execute(reCaptchaWidget2);
            }
            else
            {
                loginRegister();
            }
        }
    });

    $(document).on("click",".signin-signup-switch-button",function(e){
        e.preventDefault();
        var aux = "";
        var executeCounter = 0;
        var formButton = $(".signin-signup-button").html();
        var switchButton = $(".signin-signup-switch-button").html();
        var switchMessage = $.trim($(".signin-signup-switch-message").html());
        $(".register-info").find("input").prop("disabled", true);
        $(".register-info").slideToggle( "slow" ,function(){
            if($(".register-info").is(":visible") && executeCounter === 0)
            {
                executeCounter++;
                aux = formButton;
                formButton = switchButton;
                switchButton = aux;
                $(".signin-signup-switch-message span").toggle();
                $(".register-info").find("input").prop("disabled", false);
            }
            $(".signin-signup-button").html(formButton);
            $(".signin-signup-switch-button").html(switchButton);

        });
    });
    $(document).on("click","a.btn.btn-facebook",function(e){
        e.preventDefault();
        loginWithFacebook();
    })
});

function launchQuickLoginForm()
{
    var htmlSource   = $("#ht-quick-login-form").html();
    var template = Handlebars.compile(htmlSource);
    var emailToAttemptLogin = $("#form.form_contact input[name=email]").val();
    var defaultMessage = "Ingresa a tu cuenta para realizar esta accion";
    if(window.location.href === base_url)
    {
        defaultMessage = "";
    }
    var data = {emailToAttemptLogin:emailToAttemptLogin,defaultMessage:defaultMessage};
    var html    = template(data);
    bootbox.dialog({
        size: "small",
        message: html
    });
}

function loginRegister()
{
    var $loginForm = $("form.form-signin-signup");
    var postData = $loginForm.serializeArray();
    var formURL = base_url + 'AjaxUser/login';
    if($(".register-info").is(":visible"))
    {
        formURL = base_url + 'AjaxUser/register';
    }

    var buttonSubmit = $loginForm.find(".signin-signup-button");
    var buttonSwitchForm = $loginForm.find(".signin-signup-switch-button");
    var buttonSubmitText = $(buttonSubmit).html();
    $.ajax({
        url : formURL,
        dataType: "json",
        type: "POST",
        data : postData,
        beforeSend: function()
        {
            $("span.login-error-message").html('');
            $(buttonSubmit)
                .prop("disabled",true)
                .html("Enviando informacion..");
            $(buttonSwitchForm).prop("disabled",true);
        },
        success:function(data, textStatus, jqXHR)
        {
            if(data.response === 1)
            {
                $(buttonSubmit)
                    .html(data.message);
                $("span.login-error-message").html('');
                $("span.login-success-message").html(data.message);

                if(window.location.href === base_url)
                {
                    window.location = base_url + "admin/Home";
                }
                else
                {
                    location.reload();
                }
            }
            else
            {
                $(buttonSubmit)
                    .prop("disabled",false)
                    .html(buttonSubmitText);
                $(buttonSwitchForm).prop("disabled",false);
                $("span.login-error-message").html(data.message);
            }
            if(reCaptchaWidget2 !== "")
            {
                grecaptcha.reset(reCaptchaWidget2);
            }
        }
    });
}