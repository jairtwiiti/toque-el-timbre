/**
 * Created by Jair on 01/09/2017.
 */
$(document).ready(function() {
    $("button.facebook-connect-with").on("click",function(e){
        e.preventDefault();
        connectWithFacebook();
    });
    window.fbAsyncInit = function() {
        FB.init({
            appId: $("button.facebook-connect-with").data("app-id"),
            cookie: true, // This is important, it's not enabled by default
            version: 'v2.10'
        });
    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/es_ES/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk#xfbml=1&version=v2.10&appId='+$("input[name=app-id]").val()));
});
function connectWithFacebook()
{
    FB.login(function(response) {
            if (response.authResponse) {
                //console.log(response, response.authResponse);
                FB.api('/me', {fields: 'name'}, function(response) {
                    var userId = response.id;
                    $.ajax({
                        type:"post",
                        dataType:"json",
                        url:base_url+"AjaxUser/connectWithFacebook",
                        data:{facebookId:userId},
                        beforeSend:function()
                        {
                            var progressBar = '<span class="loading-account">Enlazando su cuenta.. </span><div class="progress">';
                            progressBar += '<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">';
                            progressBar += '</div></div>';
                            bootbox.dialog({
                                message: progressBar,
                                closeButton: false
                            });
                        },
                        success:function(response)
                        {
                            bootbox.hideAll();
                            var title = "";
                            if(response.userConnected == 0)
                            {
                                title ="Algo salio mal";
                            }
                            else
                            {
                                title ="Estas conectad@!";
                                $("button.facebook-connect-with").remove();
                            }
                            bootbox.alert({
                                title: title,
                                message: response.message
                            });
                        }
                    });
                });
            }/* else {
                alert('User cancelled login or did not fully authorize.');
            }*/
        },{scope: 'email'});
}