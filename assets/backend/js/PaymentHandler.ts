declare let Handlebars: any;
declare let blockArea: any;
declare let base_url: any;
declare let language: any;
declare let $: any;
declare let swal: any;
declare let moment: any;
class PaymentHandler
{
    private _formSelector: string;
    private _publicationId: number;

    constructor()
    {
        this._formSelector = "";
        this._publicationId = 0;
    }

    /**
     * Request the options to buy and submit the options selected
     * @param formData
     */
    public purchase(formData?)
    {
        let _this = this;
        let method = !formData?"GET":"POST";
        $.ajax({
            url : base_url + 'admin/AjaxPayment/purchase/'+_this._publicationId,
            dataType  :"json",
            method : method,
            data:formData,
            beforeSend:function()
            {
                let message = "Cargando formulario..";
                if(formData)
                {
                    message = "Procesando.."
                }
                swal.fire({
                    html: "<h3>"+message+"</h3>",
                    allowOutsideClick:false,
                    onBeforeOpen: () => {
                        swal.showLoading();
                    }
                });
            },
            success:function(response){
                if(response.success === 1 && !formData)
                {
                    _this.launchPurchaseForm(response, "Mejorar Anuncio");
                }
                else if(response.success === 1 && formData)
                {
                    swal.fire({ title:'', html:response.message, type:"success"});
                    _this.paymentOptions(response.data.payment.id);
                }
                else
                {
                    swal.fire({ title:'', html:response.message, type:"error"});
                }
            }
        });
    }

    private _sendPaymentToPagosNet(formData?)
    {
        let _this = this;
        let method = !formData?"GET":"POST";
        $.ajax({
            url : base_url + 'admin/AjaxPayment/sendPaymentToPagosNet/'+_this._publicationId,
            dataType  :"json",
            method : method,
            data:formData,
            beforeSend:function()
            {
                let message = "Cargando formulario..";
                if(formData)
                {
                    message = "Procesando.."
                }
                swal.fire({
                    html: "<h3>"+message+"</h3>",
                    allowOutsideClick:false,
                    onBeforeOpen: () => {
                        swal.showLoading();
                    }
                });
            },
            success:function(response){
                if(response.success === 1 && !formData)
                {
                    _this.launchPurchaseForm(response, "Mejorar Anuncio");
                }
                else if(response.success === 1 && formData)
                {
                    swal.fire({ title:'', html:response.message, type:"success"});
                }
                else
                {
                    swal.fire({ title:'', html:response.message, type:"error"});
                }
            }
        });
    }

    /**
     * Launch a modal form with options to buy
     * @param response
     * @param formTitle
     */
    private launchPurchaseForm (response, formTitle)
    {
        let htmlTemplate = response.data.template;
        let $template = $("<div>"+htmlTemplate+"</div>");
        let htServiceListItemPartial = $template.find("#ht-service-list-item").html();
        Handlebars.registerPartial("ht-service-list-item", htServiceListItemPartial);
        let htmlSource = $template.find(response.data.templateName).html();
        let template = Handlebars.compile(htmlSource);
        let data = {data:response.data};
        let html = template(data);
        let _this = this;
        swal.fire({
            title: "",
            html: html,
            showCancelButton: true,
            showConfirmButton: true,
            confirmButtonColor: '#E41C5E',
            cancelButtonColor: '#DDDDDD',
            confirmButtonText: "Comprar ahora!",
            cancelButtonText: "Lo haré después",
            allowOutsideClick:false,
            showLoaderOnConfirm: true,
            customClass:"payment-handler",
            width:600,
            preConfirm: () => {
                let $form = $("form[name=purchase-form]");
                if(!$form.parsley().isValid())
                {
                    $form.parsley().validate();
                    swal.showValidationMessage('Corrija los errores e intente nuevamente');
                }
            },
        }).then((result) => {
            if (result.value)
            {
                let $form = $("form[name=purchase-form]");
                _this.purchase($form.serialize());
            }
        });
    }

    public tigoMoneyRequestPayment(formData)
    {
        let _this = this;
        let method = !formData?"GET":"POST";
        $.ajax({
            url : base_url + 'admin/AjaxPayment/tigoMoneyRequestPayment',
            dataType  :"json",
            method : method,
            data:formData,
            beforeSend:function()
            {
                swal.showLoading();
            },
            success:function(response){
                if(response.success === 1 && !formData)
                {
                    _this.launchTigoMoneyRequestPaymentForm(response, "Pagar con Tigo Money");
                }
                else
                {
                    let htmlTemplate = response.data.template;
                    let $template = $("<div>"+htmlTemplate+"</div>");
                    let htmlSource = $template.find(response.data.templateName).html();
                    let template = Handlebars.compile(htmlSource);
                    let data = {data:response};
                    let html = template(data);
                    $("#tigo-money-response-message").html(html);
                    let tigoMoneyOrderId = response.data.tigoMoneyRequestPaymentResponse.orderId;
                    let tigoMoneyResponse = response.data.tigoMoneyRequestPaymentResponse.success;
                    if(tigoMoneyResponse == 1)
                    {
                        let nextYear = moment().add(10,'seconds');
                        $('.countdown').countdown(nextYear.toDate(), function(event) {
                            $(this).html(event.strftime('%S'));
                        }).on('finish.countdown', function(){
                            _this.tigoMoneyRequestStatus(tigoMoneyOrderId);
                        });
                    }
                }
            }
        });
    }

    /**
     * Launch a modal form with options to buy
     * @param response
     * @param formTitle
     */
    private launchTigoMoneyRequestPaymentForm(response, formTitle)
    {
        let htmlTemplate = response.data.template;
        let $template = $("<div>"+htmlTemplate+"</div>");
        let htmlSource = $template.find(response.data.templateName).html();
        let template = Handlebars.compile(htmlSource);
        let data = {data:response.data};
        let html = template(data);
        let _this = this;
        swal.fire({
            title: "",
            html: html,
            showCancelButton: true,
            showConfirmButton: true,
            confirmButtonColor: '#E41C5E',
            cancelButtonColor: '#DDDDDD',
            confirmButtonText: "Comprar ahora!",
            cancelButtonText: "Lo haré después",
            allowOutsideClick:false,
            showLoaderOnConfirm: true,
            customClass:"payment-handler",
            width:600,
            preConfirm: () => {
                let $form = $("form[name=tigo-money-request-payment-form]");
                if(!$form.parsley().isValid())
                {
                    $form.parsley().validate();
                    swal.showValidationMessage('Corrija los errores e intente nuevamente');
                }
            },
        }).then((result) => {
            if (result.value)
            {
                let $form = $("form[name=tigo-money-request-payment-form]");
                _this.tigoMoneyRequestPayment($form.serialize());
            }
        });
    }

    public paymentOptions(paymentId)
    {
        let _this = this;
        $.ajax({
            url : base_url + 'admin/AjaxPayment/paymentOptions/'+paymentId,
            dataType  :"json",
            method : "GET",
            data:{},
            beforeSend:function()
            {
                let $dataTable = $("#data-table.data-table-property");
                if($dataTable.length > 0)
                {
                    $dataTable.DataTable().ajax.reload(null, false);
                }
                let message = "Cargando opciones de pago..";
                swal.fire({
                    html: "<h3>"+message+"</h3>",
                    allowOutsideClick:false,
                    onBeforeOpen: () => {
                        swal.showLoading();
                    }
                });
            },
            success:function(response)
            {
                if(response.success == 1)
                {

                    _this.launchPaymentOptionsForm(response, "Opciones de pago");
                }
                else
                {
                    swal.fire({title:'', html:response.message, type:"error"});
                }
            }
        });
    }

    private launchPaymentOptionsForm(response, formTitle)
    {
        let htmlTemplate = response.data.template;
        let $template = $("<div>"+htmlTemplate+"</div>");
        let htmlSource = $template.find(response.data.templateName).html();
        let template = Handlebars.compile(htmlSource);
        let data = {data:response.data};
        let html = template(data);
        let _this = this;
        swal.fire({
            title: "",
            html: html,
            showCancelButton: false,
            showConfirmButton: false,
            confirmButtonColor: '#E41C5E',
            cancelButtonColor: '#3598DC',
            confirmButtonText: "Lo haré después",
            cancelButtonText: "",
            allowOutsideClick:false,
            showLoaderOnConfirm: true,
            customClass:"payment-handler",
            width:600,
            preConfirm: () => {
                let $form = $("form[name=payment-options-form]");
                if(!$form.parsley().isValid())
                {
                    $form.parsley().validate();
                    swal.showValidationMessage('Corrija los errores e intente nuevamente');
                }
            },
        }).then((result) => {
            if (result.value)
            {
                let $form = $("form[name=payment-options-form]");
                _this.paymentOptions($form.serialize());
            }
        });
    }
    //TODO: This method will update the section below input cellphone to show the payment status
    public tigoMoneyRequestStatus(orderId)
    {
        let _this = this;
        $.ajax({
            url : base_url + 'admin/AjaxPayment/tigoResponse/'+orderId,
            dataType  :"json",
            method : "GET",
            beforeSend:function()
            {
                // let message = "Verificando pago..";
                // console.log(message);
            },
            success:function(response)
            {
                switch(response.status)
                {
                    case 'EN PROGRESO':
                        let nextYear = moment().add(10,'seconds');
                        $('.countdown').countdown(nextYear.toDate(), function(event) {
                            $(this).html(event.strftime('%S'));
                        }).on('finish.countdown', function(){
                            _this.tigoMoneyRequestStatus(orderId);
                        });
                        break;
                    case 'CORRECTO':
                        swal.fire({ title: "Pago aprobado correctamente.", type:"success"});
                        break;
                    case 'INCORRECTO':
                        swal.fire({ title: response.message, type:"info"});
                        break;
                    case 'REVERTIDO':
                        swal.fire({ title: response.message, type:"info"});
                        break;
                }
            }
        });
    }

    public static updateTotalToPurchase()
    {
        let $totalToPurchaseContent = $("#total-to-purchase");
        let $checkboxes = $("input.checkbox-purchase-option:checked");
        let totalToPurchase = 0;
        $.each($checkboxes, function(index, value){
            totalToPurchase+= parseInt($(value).data("price"));
        });
        $totalToPurchaseContent.text(totalToPurchase.toFixed(2));
    }

    /**
     * Enable payment handler events
     */
    public loadEventHandler()
    {
        let _this = this;
        $(document).on("click", ".purchase", function(e){
            e.preventDefault();
            let publicationId = $(this).data("publication-id");
            publicationId = parseInt(publicationId);
            _this._publicationId = publicationId;
            _this.purchase();
        });

        $(document).on("click", ".pay-out", function(e){
            e.preventDefault();
        });

        $(document).on("change", ".checkbox-purchase-option", function(){
            // console.log($(this).val());
            PaymentHandler.updateTotalToPurchase();
        });

        $(document).on("click", ".tigo-money-request-payment", function(e){
            e.preventDefault();
            let $form = $("form[name=tigo-money-request-payment-form]");
            _this.tigoMoneyRequestPayment($form.serialize());
        });
    }
}