/**
 * Created by Jair on 09/05/2018.
 */
function Visitor(visitorType, visitorTypeId)
{
    var id = Date.now();
    this.register = function()
    {
        var url = base_url+"AjaxPublication/registerVisitor";
        var projectId = $("input[name=proy_id]").val();
        var postData = {visitorType:visitorType,visitorTypeId:visitorTypeId};
        if(projectId !== "")
        {
            $.ajax({
                url : url,
                dataType: "json",
                type: "POST",
                data : postData
            });
        }
    };
}