/**
 * Created by Jair on 28/7/2017.
 */
$.fn.dataTable.Buttons.swfPath = base_url + 'assets/backend/global/plugins/datatables/plugins/Buttons142/swf/flashExport.swf';
$(document).ready(function() {
    $('input.date-picker').datepicker({});

    var additionalParameter = new DTAdditionalParameterHandler("#extra-request-data","#data-table-messages");
    additionalParameter.addParameterObject('addresses','radio');
    additionalParameter.addParameterObject('start-date','text');
    additionalParameter.addParameterObject('end-date','text');
    additionalParameter.setButtonFilter('#send-filters');
    additionalParameter.setButtonRest('#remove-additional-parameters');
    additionalParameter.loadEventHandlers();

    var buttonCommon = {
        title: "Mensajes",
        exportOptions: {
            page: 'all',
            columns: [1, 2, 3, 4, 5, 6, 7, 8]
        }
    };
    //Horizontal Icons dataTable
    var oTable = $('#data-table-messages').dataTable({
        "processing" : true,
        "serverSide" : true,
        "order": [[ 6, "desc" ]],
        "ajax" : {
            url : base_url + 'admin/AjaxMessage/ajaxDtAllMessages',
            type : 'POST',
            data:function ( data ) {
                data.additionalParameters = additionalParameter.getList();
            }
        },
        "dom": "<'row'<'col-sm-6'Bl><'col-sm-6 text-right'f>>rt<'row'<'col-sm-6'i><'col-sm-6 text-right'p>>",
        "lengthMenu": [ [10, 25, 50, 100, 100000], [10, 25, 50,100, 100000] ],
        "columns" : [{
            "data" : "men_id"
        }, {
            "data" : "men_nombre"
        }, {
            "data" : "men_empresa"
        }, {
            "data" : "men_telefono"
        }, {
            "data" : "men_email"
        }, {
            "data" : "men_descripcion",
            "searchable" : false
        }, {
            "data" : "men_fech",
            "render" : function(data, type, row, meta) {
                var result = "";
                if(row.men_fech !== "" && row.men_fech !== null)
                {
                    var dateObject = new Date(row.men_fech+" 00:00:00");
                    var date = dateObject.getDate() < 10? "0"+dateObject.getDate():dateObject.getDate();
                    var month = (dateObject.getMonth()+1) < 10? "0"+(dateObject.getMonth()+1):(dateObject.getMonth()+1);
                    var year = dateObject.getFullYear();
                    result = date+"-"+month+"-"+year;
                }
                return result;
            }
        }, {
            "data" : "addresses",
            "render" : function(data, type, row, meta) {

                var url = "";
                switch(row.messageType)
                {
                    case "0":
                        url = sanitizeString(row.cat_nombre)+"-en-"+sanitizeString(row.for_descripcion)+"-en-"+sanitizeString(row.dep_nombre)+"/"+row.seo;
                        url = base_url +"buscar/"+ url.toLocaleLowerCase();
                        break;
                    case "1":
                        url = base_url + row.seo;
                        break;
                    default:
                        url = base_url +"perfil/"+ row.usu_id+"/"+sanitizeString(row.addresses);
                        break;
                }
                html = '<a href="'+url+'" target="_blank" class="addresses-link">'+row.addresses+'</a>';
                return html;
            }
        }, {
            "data" : "usu_tipo",
            "searchable" : false,
            "orderable" : false
        }, {
            "defaultContent" : " ",
            "searchable" : false,
            "orderable" : false,
            "render" : function(data, type, row, meta) {
                var html = '<a class="btn btn-primary btn-xs see-project" href="#" data-project-id="'+row.proy_id+'" title="" data-original-title="VER"  data-toggle="tooltip" data-placement="top"><i class="fa fa-search"></i></a>';
                    html += '<a target="_blank" class="btn btn-primary btn-xs" href="'+base_url + 'admin/User/edit/' +row.usu_id+'" title="" data-original-title="EDITAR"  data-toggle="tooltip" data-placement="top"><i class="fa fa-pencil"></i></a>';
                    html += '<a class="btn btn-primary btn-xs" href="'+base_url + 'admin/Project/publication/' +row.proy_id+'" title="" data-original-title="PUBLICACIONES"  data-toggle="tooltip" data-placement="top"><i class="fa fa-home"></i></a>';
                return html;
            }
        }],
        "drawCallback" : function(object) {
            $('[data-toggle="tooltip"]').tooltip();
            this.api().column(0).visible(false);
            this.api().column(9).visible(false);
        },
        "buttons": [
            $.extend( true, {}, buttonCommon,{
                extend: 'copy'
            } ),
            $.extend( true, {}, buttonCommon,{
                extend: 'csv'
            } ),
            $.extend( true, {}, buttonCommon,{
                extend: 'pdf',
            } ),
            $.extend( true, {}, buttonCommon,{
                extend: 'excel'
            } ),
            $.extend( true, {}, buttonCommon,{
                extend: 'print',
                customize: function ( win ){
                    $(win.document).find("link[href*='style.css']").remove();
                }
            } )
        ]
    });
    $('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Search');
    $('.dataTables_length select').addClass('form-control');
    oTable.fnSetFilteringDelay(1000);



    /*###$################################################################## BEGIN - SEE PAYMENT*/
    $('#data-table').on("click","a.see-project", function (e) {
        e.preventDefault();
        var projectId = $(this).parent().find(".see-project").data("project-id");
        getProjectDetail(projectId);

    });
    /*######################################################################## END - SEE PAYMENT*/
});
function getProjectDetail(id)
{
    var projectId = id;

    $.ajax({
        url : base_url + 'admin/AjaxProject/getDetail',
        dataType  :"json",
        type : "POST",
        data : {projectId : projectId},
        success:function(response){
            bootbox.hideAll();
            var htmlSource   = $("#ht-project-detail").html();
            var template = Handlebars.compile(htmlSource);
            var data = {detail: response};
            var html    = template(data);
            bootbox.alert({
                title:"Detalle del proyecto",
                message:html
            });
        }
    });
}
function sanitizeString(str)
{
    if(typeof str == "string")
    {
        str = str.replace(/[^a-z0-9áéíóúñü \.,_-]/gim,"");
        str = str.replace(/ /g,"-");
        return str.trim();
    }

}