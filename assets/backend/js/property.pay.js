/**
 * Created by Jair on 3/12/2018.
 */
$(document).ready(function() {
    $(document).on("click","input[type=radio]",function(){
        calculateTotalAmount();
    });

    $(document).on("submit","#checkout-form",function(){
       blockArea($(this));
    });
    // getServices();
});
function calculateTotalAmount()
{
    var totalChecked = $("input[type=checkbox]:checked");
    var couponDiscountAmount = 0;
    var totalAmount = 0;
    $.each(totalChecked, function(index, value){
        totalAmount += parseFloat($(value).parent().attr("cost"));
    });

    if(couponDiscountAmount > 0)
    {
        $('#total_discount').html(descuento.toFixed(2));
        $('#descuento').val(descuento.toFixed(2));
        $('input[name="paymentTotalNew"]').val(totalAmount);
        $('input[name="paymentTotal"]').val(totalWithoutDiscount);
    }else{
        $('input[name="paymentTotal"]').val(totalAmount);
    }

    toggleFormActionButtonAndCustomerInfo(totalAmount);
    $("#total_payment").text(totalAmount);
}
function toggleFormActionButtonAndCustomerInfo(totalAmount)
{
    if(totalAmount > 0)
    {
        $("input[type=submit]").removeClass("hide");
        $("a.see-my-publishments").addClass("hide");
        $("#customer-information").slideDown();
    }
    else
    {
        $("input[type=submit]").addClass("hide");
        $("a.see-my-publishments").removeClass("hide");
        $("#customer-information").slideUp();
    }
}

function getServices()
{
    $.ajax({
        url : base_url + 'admin/AjaxService/getServiceList',
        dataType  :"json",
        type : "POST",
        data : {},
        success:function(response)
        {
            var servicesList = [];
            $.each(response, function(index, value){
                servicesList.push(value);
            });
            //pre compile sub templates
            var serviceTypePartial = $("#ht-service-type").html();
            Handlebars.registerPartial("ht-service-type", serviceTypePartial);

            var serviceItemPartial = $("#ht-service-item").html();
            Handlebars.registerPartial("ht-service-item", serviceItemPartial);

            var htmlSource   = $("#ht-services-list").html();
            var template = Handlebars.compile(htmlSource);
            var html = template({servicesList:servicesList});
            $("#service-list").html(html);
            console.log(response);
        }
    });
}