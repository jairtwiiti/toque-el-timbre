/**
 * Created by Jair on 30/8/2017.
 */
$(document).ready(function() {

    $("a.facebook-login").on("click",function(e){
        e.preventDefault();
        //loginWithFacebook();
    });
//    var logInWithFacebook = function() {
//        FB.login(function(response) {
//            if (response.authResponse) {
//                //console.log(response, response.authResponse);
//                FB.api('/me', {fields: 'last_name'}, function(response) {
////                        user = response;
//                    console.log(response);
//                });
//                //window.location = base_url + "registrarse";
//                // Now you can redirect the user or do an AJAX request to
//                // a PHP script that grabs the signed request from the cookie.
//            } else {
//                alert('User cancelled login or did not fully authorize.');
//            }
//        },{scope: 'email'});
//        return false;
//    };
    window.fbAsyncInit = function() {
        FB.init({
            appId: $("input[name=app-id]").val(),
            cookie: true, // This is important, it's not enabled by default
            version: 'v2.10'
        });
    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/es_ES/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk#xfbml=1&version=v2.10&appId='+$("input[name=app-id]").val()));
});

function loginWithFacebook()
{
    FB.login(function(response) {
        if (response.authResponse) {
            //console.log(response, response.authResponse);
            findUser();
            //window.location = base_url + "registrarse";
            // Now you can redirect the user or do an AJAX request to
            // a PHP script that grabs the signed request from the cookie.
        } else {
            //alert('User cancelled login or did not fully authorize.');
        }
    },{scope: 'email'});
}
function findUser()
{
    if(FB.getAccessToken() !== null)
    {
        var accessToken = FB.getAccessToken();
        var quickLogin = window.location.href.search("login") > 0?0:1;
        var redirectTo = window.location.href.replace(base_url, "");
        //FB.api('/me', {fields: 'first_name, last_name, email'}, function(response) {
        //    var user = response;
        //    var facebookId = user.id;
        //    var userFullName = user.first_name+" "+user.last_name;
        //    var userEmail = user.email;
            $.ajax({
                type:"post",
                dataType:"json",
                url:base_url+"AjaxUser/loginFacebook",
                data:{accessToken:accessToken,quickLogin:quickLogin, redirectTo:redirectTo},
                beforeSend:function()
                {
                    var progressBar = '<span class="loading-account">Un momento por favor.. </span><div class="progress">';
                    progressBar += '<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">';
                    progressBar += '</div></div>';
                    bootbox.dialog({
                        message: progressBar,
                        closeButton: false
                    });
                },
                success:function(response)
                {
                    if(response.userFound == 0)
                    {
                        bootbox.hideAll();
                        var htmlSource   = $("#ht-welcome-to-tet-login-facebook").html();
                        var template = Handlebars.compile(htmlSource);
                        var data = {userEmail:response.userFacebookEmail};
                        var html    = template(data);
                        bootbox.confirm({
                            title: "Bienvenid@ a ToqueElTimbre",
                            message: html,
                            buttons: {
                                cancel: {
                                    label: '<i class="fa fa-times"></i> Cancelar'
                                },
                                confirm: {
                                    label: '<i class="fa fa-check"></i> Registrame con ese email'
                                }
                            },
                            callback: function (result)
                            {
                                var newUserEmail = $(".bootbox-confirm").find("input").val();
                                var isValidEmail = validateEmail(newUserEmail);
                                if(result && isValidEmail)
                                {
                                    $("em.invalid-email").text("");
                                    createAccount(accessToken, newUserEmail);
                                }
                                else {
                                    $("em.invalid-email").text("Debe ingresar un email valido");
                                    return false;
                                }
                            }
                        });
                    }
                    else
                    {
                        $(".loading-account").text("Inicializando cuenta");
                        window.location = response.url;
                    }
                }
            });
        //});
    }
}

function createAccount(accessToken, userEmail)
{
    var email = userEmail;
    var accessToken = accessToken;
    var quickLogin = window.location.href.search("login") > 0?0:1;
    var redirectTo = window.location.href.replace(base_url, "");
    $.ajax({
        type:"post",
        dataType:"json",
        url:base_url+"AjaxUser/signUpFacebook",
        data:{email:email,accessToken:accessToken, quickLogin:quickLogin, redirectTo:redirectTo},
        beforeSend:function()
        {
            var progressBar = '<span class="loading-account">Validando email.. </span><div class="progress">';
            progressBar += '<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">';
            progressBar += '</div></div>';
            bootbox.dialog({
                message: progressBar,
                closeButton: false
            });
        },
        success:function(response)
        {
            if(response.userRegistered == 0)
            {
                bootbox.hideAll();
                bootbox.alert({
                    title: "Email en uso",
                    message: response.message
                });
            }
            else
            {
                $(".loading-account").html(response.message);
                window.location = response.url;
            }
        }
    });
}
function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
