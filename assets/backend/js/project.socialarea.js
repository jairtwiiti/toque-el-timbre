/**
 * Created by Jair on 4/7/2017.
 */
$(document).ready(function() {
    $( ".sortable" ).sortable();
    $('[data-toggle="tooltip"]').tooltip();
    getImages();
    /**  begin - dropzone **/
    Dropzone.options.myDropzone = {
        addRemoveLinks: true,
        paramName: "file", // The name that will be used to transfer the file
        maxFilesize: 30, // MB
        acceptedFiles: "image/*",
        success: function(file, responseText) {
            var item = jQuery.parseJSON(responseText);
            addItem(item);
            this.removeFile(file);
        },
        addedfile: function(file) {
            file.previewElement = Dropzone.createElement(this.options.previewTemplate);
            // Create the remove button
            var removeButton = Dropzone.createElement('<a class="btn btn-danger btn-xs" href="#" data-payment-id="1" title="" data-original-title="ELIMINAR" data-toggle="tooltip" data-placement="top"><i class="fa fa-times"></i></a>');
            var _this = this;
            removeButton.addEventListener("click", function (e) {
                // Make sure the button click doesn't submit the form:
                e.preventDefault();
                e.stopPropagation();
                // Remove the file preview.
                _this.removeFile(file);
                // If you want to the delete the file on the server as well,
                // you can do the AJAX request here.
            });
            // Now attach this new element some where in your page
            $("form[name=upload-project-images]").append(file.previewElement);
            // Add the button to the file preview element.
            file.previewElement.appendChild(removeButton);
        }
    };
    /** end -dropzone **/
});
function getImages()
{
    var projectId = $("input[name=project-id]").val();
    $.ajax({
        url : base_url + 'admin/AjaxProject/getSocialAreas',
        dataType:"json",
        type : "POST",
        data : {projectId : projectId},
        success:function(response){
            var list = response;
            $(".list-group").html("");
            $.each(list,function(index,value){
                addItem(value);
            });
            bootbox.hideAll();
        }
    });
}
function addItem(item, replace)
{
    var htmlSource   = $("#ht-image-item").html();
    var template = Handlebars.compile(htmlSource);
    var data = {item: item};
    var html    = template(data);
    if(replace)
    {
        $(".list-group").find("div[data-project-image-id="+item.are_soc_id+"]").closest("div.image-item").replaceWith(html);
    }
    else
    {
        $(".list-group").append(html);
    }
    loadEventListener(item.are_soc_id);
    startImageThumbnailComponent(item.are_soc_id);
}
function loadEventListener(itemId)
{
    /*** begin - action buttons */

    $(".project-image-see-"+itemId).bind("click",function(e){
        e.preventDefault();
        var itemId = $(this).closest("div").data("project-image-id");
        seeResource(itemId);
    });
    $(".project-image-edit-"+itemId).bind("click",function(e){
        e.preventDefault();
        var itemId = $(this).closest("div").data("project-image-id");
        $.ajax({
            url : base_url + 'admin/AjaxProject/getSocialArea',
            dataType  :"json",
            type : "POST",
            data : {itemId : itemId},
            success:function(response){
                bootbox.hideAll();
                var htmlSource   = $("#ht-image-edit").html();
                var template = Handlebars.compile(htmlSource);
                var data = {detail: response};
                var html    = template(data);
                bootbox.confirm({
                    title:"Editar informacion",
                    message:html,
                    buttons: {
                        cancel: {
                            label: 'Cancelar'
                        },
                        confirm: {
                            label: 'Guardar'
                        }
                    },
                    callback: function (result) {
                        if(result)
                        {
                            var $form = $("form[name=image-edit-"+itemId+"]");
                            updateSocialAreaInformation($form);
                        }
                    }
                });
                startImageThumbnailComponent(itemId);
                initNormalTynimce(".tinymce");
            }
        });
    });
    $(".project-image-delete-"+itemId).on("click",function(e){
        e.preventDefault();
        var itemId = $(this).closest("div").data("project-image-id");
        bootbox.confirm({
            title:"Borrar imagen",
            message:"Borrar esta imagen permanentemente?",
            buttons: {
                cancel: {
                    label: 'Cancelar'
                },
                confirm: {
                    label: 'Aceptar'
                }
            },
            callback: function (result) {
                if(result)
                {
                    deleteResource(itemId);
                }
            }
        });
    });
    /*** end - action buttons */
}
function startImageThumbnailComponent(itemId)
{
    /** begin - Snippet to upload image **/
    $("div.avatar-upload-"+itemId+" img").bind("click",function()
    {
        $("#avatar-file-"+itemId).trigger("click");
    });

    $("#avatar-file-"+itemId).replaceWith("<input id=\"avatar-file-"+itemId+"\" type=\"file\" name=\"logo\" accept=\"image/*\" class=\"hide\">");
    $("#avatar-file-"+itemId).change(function() {
        readImage(this);
    });
    $("div.avatar-upload-"+itemId+" img").css( 'cursor', 'pointer' );
    function readImage(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#avatar-preview-'+itemId).attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    /** end - Snippet to upload image **/
}
function updateSocialAreaInformation(form)
{
    var data = new FormData();
    var itemId = form.find("input[name=item-id]").val();
    data.append("itemId", itemId);
    data.append("title",form.find("input[name=title]").val());
    data.append("description",tinyMCE.activeEditor.getContent());
    data.append("file",form.find("input[type=file]")[0].files[0]);
    $.ajax({
        url : base_url + 'admin/AjaxProject/updateSocialAreaInformation',
        dataType  :"json",
        type : "POST",
        contentType: false,
        processData: false,
        data : data,
        success:function(response){
            bootbox.hideAll();
            addItem(response,true);
        }
    });
}
function seeResource(id)
{
    var src = $("div[data-project-image-id="+id+"]").closest("div.image-item").find("img").prop("src");
    src = src.replace("h=100", "");
    src = src.replace("w=150", "w=850");
    bootbox.alert({
        title:"Imagen",
        message: "<p class='text-center'><img src='"+src+"' class='img-responsive'></p>",
        size: 'large'
    });
}
function deleteResource(id)
{
    var itemId = id;
    var itemModel = "model_socialarea";
    $.ajax({
        url : base_url + 'admin/AjaxProject/deleteResource',
        dataType  :"json",
        type : "POST",
        data : {itemId:itemId, itemModel:itemModel},
        success:function(){
            bootbox.hideAll();
            $("div[data-project-image-id="+itemId+"]").closest("div.image-item").remove();
        }
    });
}