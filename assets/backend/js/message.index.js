/**
 * Created by Jair on 31/10/2017.
 */
$(document).ready(function() {

    //when this button si submit then the modal will appears to fill the email body.
    $("div.inbox-content").on("click",".btn-response-message",function(e){
        e.preventDefault();
        launchModal();
    });

    var launchModal = function()
    {
        var addressee = $("div.inbox-view").find(".addressee").text();
        var propertyName = $("div.inbox-header").find(".property-name").text();
        var messageType = $("div.inbox-view").data("message-type");
        var messageEmail = $("input[type=hidden][name=message-email]").val();
        var projectId = $("input[type=hidden][name=project-id]").val();
        var propertyId = $("input[type=hidden][name=property-id]").val();
        var response = {
            addressee: addressee,
            subject: "Respuesta de "+ propertyName,
            messageType:messageType,
            messageEmail:messageEmail,
            projectId:projectId,
            propertyId:propertyId
        };
        var htmlSource   = $("#ht-response-message-form").html();
        var template = Handlebars.compile(htmlSource);
        var data = {data: response};
        var html    = template(data);

        bootbox.confirm({
            size:"small",
            title: "Responder mensaje",
            message: html,
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> Cancelar'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> Enviar'
                }
            },
            callback: function (result)
            {
                if(result)
                {
                    sendMessage(result)
                }
            }
        });
        initNormalTynimce(".tinymce");
    }

    var sendMessage = function(result)
    {
        if(result)
        {
            var messageId = $("div.inbox-view").data("message-id");
            var subject = $("input[name=subject]").val();
            var replyMessage = $("textarea[name=message]").val();
            var messageType = $("form[name=response-message]").find("input[name=message-type]").val();
            var email = $("form[name=response-message]").find("input[name=email]").val();
            var projectId = $("form[name=response-message]").find("input[name=project-id]").val();
            var propertyId = $("form[name=response-message]").find("input[name=property-id]").val();
            $.ajax({
                method: "POST",
                url : base_url + 'admin/AjaxUser/contactMessageResponse',
                dataType: "json",
                data: {messageId:messageId,subject:subject,replyMessage:replyMessage,messageType:messageType,email:email,projectId:projectId,propertyId:propertyId},
                beforeSend:function()
                {
                    bootbox.hideAll();
                    var progressBar = 'Enviando...<div class="progress">';
                    progressBar += '<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">';
                    progressBar += '</div></div>';
                    bootbox.dialog({
                        message: progressBar,
                        closeButton: false

                    });
                },
                success:function(response)
                {
                    bootbox.hideAll();
                    console.log(response);
                }
            });
        }
    }

});
