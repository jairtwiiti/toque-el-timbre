$(document).ready(function(){
//tinyMce
	$('.tinymce').prop('rows',5);
	tinymce.init({
        selector: ".tinymce",
		//theme: "modern",
        plugins: [
	                 "advlist autolink link image lists charmap print hr anchor pagebreak",
	                 "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
	                 "table contextmenu directionality emoticons paste textcolor",
	                 "code"
                 ],
       	toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect ",
       	toolbar2: "| link unlink anchor | image media | forecolor backcolor  | fontselect | fontsizeselect",
       	image_advtab: true,
       	relative_urls: false,
 		remove_script_host: false,
 		language : 'es'
 		//external_filemanager_path: baseUrl("/static/panel/t1/js/filemanager/"),
      	//filemanager_title:"Responsive Filemanager" ,
      	//external_plugins: { "filemanager" : "../filemanager/plugin.min.js"}
      });
});

function initNormalTynimce(selectorVar) {
	tinymce.init({
		selector: selectorVar,
		//theme: "modern",
		plugins: [
			"advlist autolink link image lists charmap print hr anchor pagebreak",
			"searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
			"table contextmenu directionality emoticons paste textcolor",
			"code"
		],
		toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect ",
		toolbar2: "| link unlink anchor | image media | forecolor backcolor  | fontselect | fontsizeselect",
		image_advtab: true,
		relative_urls: false,
		remove_script_host: false,
		language : 'es'
		//external_filemanager_path: baseUrl("/static/panel/t1/js/filemanager/"),
		//filemanager_title:"Responsive Filemanager" ,
		//external_plugins: { "filemanager" : "../filemanager/plugin.min.js"}
      });
}
