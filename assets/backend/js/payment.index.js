/**
 * Created by Jair on 21/7/2017.
 */
$(document).ready(function() {
    $('input.date-picker').datepicker({});

    var additionalParameter = new DTAdditionalParameterHandler("#extra-request-data","#data-table");
    additionalParameter.addParameterObject('start-date','text');
    additionalParameter.addParameterObject('end-date','text');
    additionalParameter.addParameterObject('date-to-filter','radio');
    additionalParameter.addParameterObject('payment-status','radio');
    additionalParameter.setButtonFilter('#send-filters');
    additionalParameter.setButtonRest('#remove-additional-parameters');
    additionalParameter.loadEventHandlers();

    var buttonCommon = {
        title: "Pagos",
        exportOptions: {
            page: 'all',
            columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        }
    };
    //Horizontal Icons dataTable
    var oTable = $('#data-table').dataTable({
        "processing" : true,
        "serverSide" : true,
        "ajax" : {
            url : base_url + 'admin/AjaxPayment/ajaxDtAllPayments',
            type : 'POST',
            data:function ( data ) {
                data.additionalParameters = additionalParameter.getList();
            }
        },
        "dom": "<'row'<'col-sm-6'Bl><'col-sm-6 text-right'f>>rt<'row'<'col-sm-6'i><'col-sm-6 text-right'p>>",
        "lengthMenu": [ [10, 25, 50, 100, 100000], [10, 25, 50,100, 100000] ],
        "order": [[ 2, "desc" ]],
        "columns" : [{
            "data" : "pag_id",
        }, {
            "searchable" : false,
            "data" : "inm_nombre",
        }, {
            "data" : "pag_fecha",
            "render" : function(data, type, row, meta) {
                var result = "";
                if(row.pag_fecha !== "" && row.pag_fecha !== null)
                {
                    var dateObject = new Date(row.pag_fecha);
                    var date = dateObject.getDate() < 10? "0"+dateObject.getDate():dateObject.getDate();
                    var month = (dateObject.getMonth()+1) < 10? "0"+(dateObject.getMonth()+1):(dateObject.getMonth()+1);
                    var year = dateObject.getFullYear();
                    result = date+"-"+month+"-"+year;
                }
                return result;
            }
        }, {
            "data" : "pag_fecha_pagado",
            "render" : function(data, type, row, meta) {
                var result = "";
                if(row.pag_fecha_pagado !== "" && row.pag_fecha_pagado !== null)
                {
                    var dateObject = new Date(row.pag_fecha_pagado);
                    var date = dateObject.getDate() < 10? "0"+dateObject.getDate():dateObject.getDate();
                    var month = (dateObject.getMonth()+1) < 10? "0"+(dateObject.getMonth()+1):(dateObject.getMonth()+1);
                    var year = dateObject.getFullYear();
                    result = date+"-"+month+"-"+year;
                }

                return result;
            }
        }, {
            "data" : "usu_nombre",
        }, {
            "data" : "usu_apellido",
        }, {
            "data" : "usu_email",
        }, {
            "data" : "usu_tipo",
        }, {
            "searchable" : false,
            "data" : "pag_monto",
        }, {
            "data" : "pag_entidad",
        }, {
            "data" : "pag_concepto",
        }, {
            "data" : "pag_estado",
        }, {
            "defaultContent" : " ",
            "searchable" : false,
            "orderable" : false,
            "render" : function(data, type, row, meta) {
                var html = '<a class="btn btn-primary btn-xs see-payment" href="#" data-payment-id="'+row.pag_id+'" title="" data-original-title="VER"  data-toggle="tooltip" data-placement="top"><i class="fa fa-search"></i></a>';
                    if(row.pag_estado == "Pendiente")
                    {
                        html += '<span class="payment-methods"></span><a class="btn btn-danger btn-xs approve-payment" href="#" data-payment-method= "Oficina" title="APROBAR" data data-toggle="tooltip"><i class="fa fa-check"></i></a>';
                        html += '<a class="btn btn-danger btn-xs approve-payment" href="#" data-payment-method= "Banco" title="DEPOSITO O TRANSFERENCIA BANCARIA" data-toggle="tooltip"><i class="fa fa-money"></i></a>';
                        html += '<a class="btn btn-danger btn-xs approve-payment" href="#" data-payment-method= "Toqueeltimbre" title="PAGO CORTESIA TOQUEELTIMBRE" data-toggle="tooltip"><i class="fa fa-home"></i></a></span>';
                    }

                return html;
            }
        }],
        // sorting: [[1,'asc']],
        "drawCallback" : function(object) {
            //this.api().column(0).visible(false);
            $('[data-toggle="tooltip"]').tooltip();
            this.api().columns.adjust();
        },
        "buttons": [
            $.extend( true, {}, buttonCommon,{
                extend: 'copy'
            } ),
            $.extend( true, {}, buttonCommon,{
                extend: 'csv'
            } ),
            $.extend( true, {}, buttonCommon,{
                extend: 'pdf',
                orientation: 'landscape'
            } ),
            $.extend( true, {}, buttonCommon,{
                extend: 'excel'
            } ),
            $.extend( true, {}, buttonCommon,{
                extend: 'print',
                customize: function ( win ){
                    $(win.document).find("link[href*='style.css']").remove();
                }
            } )
        ]
    });
    //$('#data-table thead th').each( function () {
    //    var title = $(this).text();
    //    $(this).html( '<input type="text" placeholder="'+title+'" />' );
    //} );
    //$('#data-table thead th').find("input[type=text]").on("click",function(e){
    //    e.stopPropagation();
    //});
    $('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Search');
    $('.dataTables_length select').addClass('form-control');
    oTable.fnSetFilteringDelay(1000);

    /*##################################################################### BEGIN - APPROVE PAYMENT*/

    $('#data-table').on("click","a.approve-payment", function (e) {
        e.preventDefault();
        let paymentId = $(this).parent().find(".see-payment").data("payment-id");
        let paymentMethod = $(this).data("payment-method");
        let paymentConfirmMessage = [];
        paymentConfirmMessage["banco"] = "Confirmar pago a traves de Deposito o Transferencia Bancaria?";
        paymentConfirmMessage["oficina"] = "Confirmar pago realizado directamente en la oficina?";
        paymentConfirmMessage["toqueeltimbre"] = "Confirmar pago de cortesia de ToqueElTimbre?";
        let paymentMessage = paymentConfirmMessage[paymentMethod.toLowerCase()];
        //select current row to update the data
        bootbox.confirm({
            title: "Aprobar pago",
            message: paymentMessage,
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> Cancelar'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> Aceptar'
                }
            },
            callback: function (result)
            {
                if(result)
                {
                    sendPaymentApprovalRequest(paymentId, paymentMethod);
                }
            }
        });
    });
    /*##################################################################### END - APPROVE PAYMENT*/

    /*###$################################################################## BEGIN - SEE PAYMENT*/
    $('#data-table').on("click","a.see-payment", function (e) {
        e.preventDefault();
        let paymentId = $(this).parent().find(".see-payment").data("payment-id");
        getPaymentDetail(paymentId);

    });
    /*######################################################################## END - SEE PAYMENT*/
});
function sendPaymentApprovalRequest(paymentId, method)
{
    $.ajax({
        url : base_url + 'admin/AjaxPayment/approve',
        type : "POST",
        data : {paymentId : paymentId, paymentMethod:method},
        success:function(response){
            let result = jQuery.parseJSON(response);
            let stringResponse = "";
            let successApproval = false;
            let title = "El pago se aprobo correctamente!";
            $.each(result,function(index,value){
                stringResponse += '<i class="fa fa-check"></i> '+value+"<br>";
                successApproval = true;
            });
            if(!successApproval)
            {
                title = "No se puede aprobar el pago!";
                stringResponse = '<i class="fa fa-times"></i> Ocurrio un error actualizando el pago y/o sus servicios, <br>por favor intente de nuevo o contacte al administador del sistema';
            }
            let $dataTable = $("#data-table");
            if($dataTable.length > 0)
            {
                $dataTable.DataTable().ajax.reload(null, false);
            }
            bootbox.hideAll();
            bootbox.alert({
                title:title,
                message: stringResponse
            });
        }
    });
}

function getPaymentDetail(id)
{
    $.ajax({
        url : base_url + 'admin/AjaxPayment/getDetail',
        dataType  :"json",
        type : "POST",
        data : {paymentId : id},
        success:function(response){
            bootbox.hideAll();
            let htmlSource   = $("#ht-payment-detail").html();
            let template = Handlebars.compile(htmlSource);
            let data = {detail: response};
            let html    = template(data);
            bootbox.alert({
                title:"Detalle del pago",
                message:html
            });
        }
    });
}