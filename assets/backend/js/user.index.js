/**
 * Created by Jair on 28/7/2017.
 */
$.fn.dataTable.Buttons.swfPath = base_url + 'assets/backend/global/plugins/datatables/plugins/Buttons142/swf/flashExport.swf';
$(document).ready(function() {
    $('input.date-picker').datepicker({});

    var additionalParameter = new DTAdditionalParameterHandler("#extra-request-data","#data-table");
    additionalParameter.addParameterObject('start-date','text');
    additionalParameter.addParameterObject('end-date','text');
    additionalParameter.addParameterObject('date-to-filter','radio');
    additionalParameter.addParameterObject('show-expired','checkbox');
    additionalParameter.addParameterObject('user','text');
    additionalParameter.setButtonFilter('#send-filters');
    additionalParameter.setButtonRest('#remove-additional-parameters');
    additionalParameter.loadEventHandlers();

    var buttonCommon = {
        title: "Usuarios",
        exportOptions: {
            page: 'all',
            columns: [0, 1, 2, 3, 4, 5, 6]
        }
    };
    //Horizontal Icons dataTable
    var oTable = $('#data-table').dataTable({
        "processing" : true,
        "serverSide" : true,
        "order": [[ 6, "desc" ]],
        "ajax" : {
            url : base_url + 'admin/AjaxUser/ajaxDtAllUsers',
            type : 'POST',
            data:function ( data ) {
                data.additionalParameters = additionalParameter.getList();
            }
        },
        "dom": "<'row'<'col-sm-6'Bl><'col-sm-6 text-right'f>>rt<'row'<'col-sm-6'i><'col-sm-6 text-right'p>>",
        "lengthMenu": [ [10, 25, 50, 100, 100000], [10, 25, 50,100, 100000] ],
        "columns" : [{
            "data" : "usu_id"
        }, {
            "data" : "usu_nombre"
        }, {
            "data" : "usu_apellido"
        }, {
            "data" : "usu_email"
        }, {
            "data" : "usu_tipo"
        }, {
            "data" : "usu_certificado",
            "searchable" : false,
            "orderable" : false,
            "render" : function(data, type, row, meta) {
                var checked = row.usu_certificado == 1?"checked":"";
                var html = '<div class="checkbox">';
                html += '<label>';
                html += '<input type="checkbox" class="certificate-user" value="" '+checked+' data-user-id="'+row.usu_id+'">';
                html += '<i class="fa fa-spinner fa-pulse fa-fw pull-left hide certificate-user-pulse" style="margin-top: 3px;margin-right: 5px;"></i>';
                html += '</label>';
                html += '</div>';
                return html;
            }
        }, {
            "data" : "usu_fecha_registro",
            "render" : function(data, type, row, meta) {
                var result = "";
                if(row.usu_fecha_registro !== "" && row.usu_fecha_registro !== null)
                {
                    var dateObject = new Date(row.usu_fecha_registro);
                    var date = dateObject.getDate() < 10? "0"+dateObject.getDate():dateObject.getDate();
                    var month = (dateObject.getMonth()+1) < 10? "0"+(dateObject.getMonth()+1):(dateObject.getMonth()+1);
                    var year = dateObject.getFullYear();
                    result = date+"-"+month+"-"+year;
                }
                return result;
            }
        }, {
            "data" : "total_publications",
            "searchable" : false,
        }, {
            "defaultContent" : " ",
            "searchable" : false,
            "orderable" : false,
            "render" : function(data, type, row, meta) {
                var html = '<a class="btn btn-primary btn-xs see-project" href="#" data-project-id="'+row.proy_id+'" title="" data-original-title="VER"  data-toggle="tooltip" data-placement="top"><i class="fa fa-search"></i></a>';
                    html += '<a target="_blank" class="btn btn-primary btn-xs" href="'+base_url + 'admin/User/edit/' +row.usu_id+'" title="" data-original-title="EDITAR"  data-toggle="tooltip" data-placement="top"><i class="fa fa-pencil"></i></a>';
                    html += '<a class="btn btn-primary btn-xs" href="'+base_url + 'admin/Project/publication/' +row.proy_id+'" title="" data-original-title="PUBLICACIONES"  data-toggle="tooltip" data-placement="top"><i class="fa fa-home"></i></a>';
                    if(row.usu_tipo == 'Inmobiliaria')
                    {
                        html += '<a class="btn btn-primary btn-xs" href="'+base_url + 'perfil/' +row.usu_id+'/'+row.usu_nombre+'" target="_blank" title="" data-original-title="PERFIL"  data-toggle="tooltip" data-placement="top"><i class="fa fa-globe"></i></a>';
                    }
                return html;
            }
        }],
        "drawCallback" : function(object) {
            $('[data-toggle="tooltip"]').tooltip();
            $('.certificate-user').on('change',function(){
                updateCertificationStatus($(this));
            });
        },
        "buttons": [
            $.extend( true, {}, buttonCommon,{
                extend: 'copy'
            } ),
            $.extend( true, {}, buttonCommon,{
                extend: 'csv'
            } ),
            $.extend( true, {}, buttonCommon,{
                extend: 'pdf',
            } ),
            $.extend( true, {}, buttonCommon,{
                extend: 'excel'
            } ),
            $.extend( true, {}, buttonCommon,{
                extend: 'print',
                customize: function ( win ){
                    $(win.document).find("link[href*='style.css']").remove();
                }
            } )
        ]
    });
    $('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Search');
    $('.dataTables_length select').addClass('form-control');
    oTable.fnSetFilteringDelay(1000);



    /*###$################################################################## BEGIN - SEE PAYMENT*/
    $('#data-table').on("click","a.see-project", function (e) {
        e.preventDefault();
        var projectId = $(this).parent().find(".see-project").data("project-id");
        getProjectDetail(projectId);

    });
    /*######################################################################## END - SEE PAYMENT*/
});
function updateCertificationStatus(checkbox)
{
    var $checkbox = checkbox;
    var userId = $checkbox.data('user-id');
    var isChecked = $checkbox.is(":checked");
    var isCertificated = isChecked?1:0;

    $.ajax({
        method: "POST",
        url : base_url + 'admin/AjaxUser/ajaxCertificateUser',
        dataType: "json",
        data: {isCertificated:isCertificated,userId:userId},
        beforeSend:function()
        {
            $checkbox.closest("tbody").find("input[type=checkbox]").addClass("hide");
            $checkbox.prop("disabled",true);
            $checkbox.closest("tbody").find(".certificate-user-pulse").removeClass("hide");
        },
        success:function()
        {
            $checkbox.closest("tbody").find("input[type=checkbox]").removeClass("hide");
            $checkbox.prop("disabled",false);
            $checkbox.closest("tbody").find(".certificate-user-pulse").addClass("hide");
            $('#data-table').DataTable().ajax.reload( null, false );
        }
    });
}
function getProjectDetail(id)
{
    var projectId = id;

    $.ajax({
        url : base_url + 'admin/AjaxProject/getDetail',
        dataType  :"json",
        type : "POST",
        data : {projectId : projectId},
        success:function(response){
            bootbox.hideAll();
            var htmlSource   = $("#ht-project-detail").html();
            var template = Handlebars.compile(htmlSource);
            var data = {detail: response};
            var html    = template(data);
            bootbox.alert({
                title:"Detalle del proyecto",
                message:html
            });
        }
    });
}

