/**
 * Created by Jair on 07/09/2017.
 */
$(document).ready(function() {
    $(".input-masked").inputmask();
    /** property feature handler**/
    var propertyTypeList = jQuery.parseJSON($("input[name=property-type-feature-list]").val());
    var propertyFeatureHandler = new PropertyFeatureHandler(propertyTypeList);
    //This method isn't required at least the features needs to be preset
    propertyFeatureHandler.drawFeatures();
    propertyFeatureHandler.loadEventHandlers();

    /** google maps **/
    if($("#maps").length > 0)
    {
        var mapHandler = new MapHandler();
        mapHandler.addInitialMarker();
        mapHandler.loadEventHandlers();
    }

    /** tooltips **/
    $('[data-toggle="tooltip"]').tooltip();

    /** begin - Snippet to upload image **/
    $(document).on("click","div.avatar-upload img",function()
    {
        $("#avatar-file").trigger("click");
    });

    $("#avatar-file").replaceWith("<input id=\"avatar-file\" type=\"file\" name=\"logo\" accept=\"image/*\">");
    $("#avatar-file").change(function() {
        readImage(this);
    });
    $("#avatar-file").hide();
    $("div.avatar-upload img").css( 'cursor', 'pointer' );
    function readImage(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#avatar-preview').attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    /** end - Snippet to upload image **/

    /** date picker **/
    $('input.date-picker').datepicker({});

    /** select2 ajax for users **/
    $('#ajax-get-users').select2({
        placeholder : "Buscar..",
        allowClear : true,
        ajax : {
            type : "post",
            dataType : "json",
            url : base_url + 'admin/AjaxUser/getAllUsers',
            quietMillis : 600,
            minimumInputLength : 4,
            data : function(term, page) {
                return {
                    term : term, //search term
                    limit : 5 // page size
                };
            },
            results : function(data) {
                return {
                    results : data
                };
            }
        },
        width : "100%",
        initSelection : function(element, callback) {
            var jsonResp = JSON.parse(element.val());
            callback(jsonResp);
            element.val(jsonResp.id);
        }
    });

    /** select2 ajax for cities **/
    $('#ajax-get-cities').select2({
        placeholder : "Buscar..",
        allowClear : true,
        ajax : {
            type : "post",
            dataType : "json",
            url : base_url + 'admin/AjaxCity/getAllCitiesByStateId',
            quietMillis : 600,
            minimumInputLength : 4,
            data : function(term, page) {
                var stateId = $("select[name=state]").val();
                return {
                    stateId : stateId, // search by state
                    term : term, //search term
                    limit : 5 // page size
                };
            },
            results : function(data) {
                return {
                    results : data
                };
            }
        },
        width : "100%",
        initSelection : function(element, callback) {
            var jsonResp = JSON.parse(element.val());
            callback(jsonResp);
            element.val(jsonResp.id);
        }
    });

    /** select2 ajax for zones **/
    $('#ajax-get-zones').select2({
        placeholder : "Buscar..",
        allowClear : true,
        ajax : {
            type : "post",
            dataType : "json",
            url : base_url + 'admin/AjaxZone/getAllZonesByStateId',
            quietMillis : 600,
            minimumInputLength : 4,
            data : function(term, page) {
                var stateId = $("select[name=state]").val();
                return {
                    stateId : stateId, // search by state
                    term : term, //search term
                    limit : 5 // page size
                };
            },
            results : function(data) {
                return {
                    results : data
                };
            }
        },
        width : "100%",
        initSelection : function(element, callback) {
            var jsonResp = JSON.parse(element.val());
            callback(jsonResp);
            element.val(jsonResp.id);
        }
    });
    $("select[name=state]").change(function() {
        $("#ajax-get-cities").select2("val", "");
        $("#ajax-get-zones").select2("val", "");
        $(".search-address-data-city").addClass("hide");
    });

    $("select[name=state]").on("change",function(){
        addStateOnSearch($(this));
    });

    function addStateOnSearch(select2)
    {
        var state = "";
        if(select2.select2("data") !=null)
        {
            state = select2.select2("data").text;
            $(".search-address-data-state").removeClass("hide");
            $(".search-address-data-state").text(state);
        }
        else
        {
            $(".search-address-data-state").addClass("hide");
        }
    }
    $("#ajax-get-cities").on("change",function(){
        var city = "";
        if($(this).select2("data") !=null)
        {
            city = $(this).select2("data").text;
            $(".search-address-data-city").removeClass("hide");
            $(".search-address-data-city").text(city);
        }
        else
        {
            $(".search-address-data-city").addClass("hide");
        }
    });
    $(document).on("submit","form[name=add-project-form]",function(){
        if($(this).parsley().isValid())
        {
            $("button[type=submit]").addClass("disabled");
            $("button[type=submit]").html("Guardando <i class='fa fa-spinner fa-pulse'></i>");
        }
    });
});
