/**
 * Created by Jair on 23/11/2017.
 */
var reCaptchaWidget1 = "";
$(document).ready(function($) {

    $(document).on("click","#test-whatsapp",function(e){
       e.preventDefault();
       sendToWhatsapp();
    });

    var visitorTypeId = $("#property-id").val();
    if(visitorTypeId > 0)
    {
        var visitor = new Visitor("Inmueble", visitorTypeId);
        visitor.register();
    }
    /** google maps **/
    if($("#maps").length > 0)
    {
        var mapHandler = new MapHandler();
        mapHandler.addInitialMarker();
    }

    if($( window ).width() >= 975)
    {
        $('.contact-form').followTo();
    }

    $("#form input[type=button]").click(function(e){
        e.preventDefault();

        if(!$("#form").parsley().isValid())
        {
            $("#form").parsley().validate();

        }
        else {
            if(reCaptchaWidget1 === "")
            {
                $("#form-contact-recaptcha").html("");
                reCaptchaWidget1 = grecaptcha.render('form-contact-recaptcha', {
                    'sitekey' : $("#form").find('input[name=site-key]').val(),
                    'callback' : sendRequest,
                    'size' : 'invisible'
                });
            }
            grecaptcha.execute(reCaptchaWidget1);
        }
    });
});

$.fn.followTo = function () {
    var $this = this,
    $window = $(window);
    $window.scroll(function(e){
        var pos = $("#col-detail").height() - $(".contact-form").height() + $("nav").height() - 9;
        if ($window.scrollTop() >= 120 && $window.scrollTop() <= pos)
        {
            // console.log("1",$window.scrollTop(),pos);
            $this.css({
                position: 'fixed',
                top: 10,
                width:"263px",
                marginRight:"104px",
                paddingBottom:"48px"
            });
        }
        else if($window.scrollTop() > pos)
        {
            // console.log("2",$window.scrollTop(),pos);
            $this.css({
                position: 'absolute',
                top: pos-155,
                marginRight:"15px",
                paddingBottom:"48px"
            });
        }
        else
        {
            // console.log("3",$window.scrollTop(),pos);
            $this.css({
                position: 'absolute',
                top: 0,
                marginRight:"15px",
                paddingBottom:"48px"
            });
        }
    });
};
function sendRequest(token)
{
    var postData = $("#form").serializeArray();
    var sendToWhatsapp = $("#send-to-whatsapp").is(":checked");
    postData.push({token:token});
    var formURL = base_url + 'AjaxProperty/sendContactMessage';
    var buttonSubmit = $("#form").find("input[type=button]");
    $.ajax({
        url : formURL,
        dataType: "json",
        type: "POST",
        data : postData,
        beforeSend: function()
        {
            $(buttonSubmit)
                .prop("disabled",true)
                .val("Enviando..");
        },
        success:function(data, textStatus, jqXHR)
        {
            if(data.response == 1)
            {
                //This code segment eval if send the message through whatsapp
                var $link = $("#send-whatsapp a");
                if(sendToWhatsapp && data.user.cellPhone !== "")
                {
                    var property = data.user.property;
                    var propertyLink = "http://bit.ly/"+property.inm_short_seo;
                    if(property.inm_short_seo === "" || property.inm_short_seo === null)
                    {
                        propertyLink = base_url + "buscar/inmuebles-en-bolivia/"+property.inm_seo;
                    }
                    var mainFilter = property.cat_nombre+" en "+property.for_descripcion+" en "+property.ciu_nombre;
                    mainFilter = mainFilter.toLowerCase().replace(/(\b\w)/gi,function(m){return m.toUpperCase();});
                    var price = property.inm_precio;
                    price = parseFloat(price).toLocaleString('en');
                    var currency = property.mon_abreviado;
                    //TODO:The comment user is arriving with <br> tag
                    var userQuery = data.user.comment;
                    var text = "Estoy viendo tu inmueble en \n" +
                                "ToqueElTimbre.com:\n" +
                                mainFilter+"\n" +
                                price+currency+"\n" +
                                "Tengo la siguiente consulta:\n" +
                                userQuery +"\n"+
                                "Mas informacion\n" +
                                propertyLink;
                    text = encodeURI(text);
                    var url = "https://api.whatsapp.com/send?phone=591"+data.user.cellPhone+"&text="+text;
                    // window.open(url);

                    $link.prop("href",url);
                    $link.prop("target","_blank");
                    // $link[0].click();
                }

                var thankYouPageEndPoint = $(location).attr('href')+"/thank-you";
                window.location = thankYouPageEndPoint;
                $link[0].click();
            }
            else if(data.response == 2)
            {
                launchQuickLoginForm();
                $(buttonSubmit)
                    .prop("disabled",false)
                    .val("Enviar");
            }
            else
            {
                $("#box_contact_agent .result").html(data.message);
                $(buttonSubmit)
                    .prop("disabled",false)
                    .val("Enviar");
            }
            grecaptcha.reset(reCaptchaWidget1);
        }
    });
}

function sendToWhatsapp()
{
    var text = "Buenas tardes\nAun esta disponible el inmueble \n\"Condominio Privado Colonial Norte\"\nque publicó en ToqueElTimbre.com?\nDetalle del inmueble\nhttp://toqueeltimbre.com/buscar/inmuebles-en-bolivia/condominio-privado-colonial-norte-1";
    var text = "Buenas tardes\nAun esta disponible el inmueble \n\"Condominio Privado Colonial Norte\"\nque publicó en ToqueElTimbre.com?\nDetalle del inmueble\nhttp://bit.ly/2IqD8og";
    // text = "UltraCasas.com%20te%20conecta%3A%0D%0AEstoy%20interesado%20en%20tu%20Casa%20en%20Venta%20en%207MO%20ANILLO%20ZONA%20NORTE%20en%20%24us%20195%2C000%0D%0APor%20favor%20m%C3%A1s%20informaci%C3%B3n%0D%0Ahttp%3A%2F%2Fultra.bo%2Fs1aeu";
    text = encodeURI(text);
    window.open("https://api.whatsapp.com/send?phone=59177017333&text="+text,"_blank");
    window.open(thankYouPageEndPoint,"_self");
    // var thankYouPageEndPoint = $(location).attr('href')+"/thank-you";
    // window.location = thankYouPageEndPoint;
}

function getGreeting()
{
    var d = new Date();
    var time = d.getHours();
    var greeting = "";

    if (time < 12)
    {
        greeting = "Buenos dias";
    }
    if (time >= 12)
    {
        greeting = "Buenas tardes";
    }
    if (time >= 19)
    {
        greeting = "Buenas noches";
    }
    return greeting;
}