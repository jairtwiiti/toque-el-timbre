/**
 * Created by Jair on 28/11/2018.
 */
$(document).ready(function() {
    loadTigoMoneyForm();
    $(document).on("click","#send-payment-request", function(e){
        paymentRequest();
    });
});

function loadTigoMoneyForm()
{
    let paymentId = window.location.href.substr(window.location.href.lastIndexOf('/') + 1);
    var htmlSource   = $('#ht-tigo-money-form').html();
    var template = Handlebars.compile(htmlSource);
    var data = {paymentId:paymentId};
    var html    = template(data);
    $("#tigo-money-form").html(html);
}

function paymentRequest()
{
    var formData = new FormData();
    var paymentId = $("#tigo-money-form").find("input[name=payment-id]").val();
    formData.append("paymentId", paymentId);

}