/**
 * Created by Jair on 4/7/2017.
 */
$(document).ready(function() {

    $( ".sortable" ).sortable();
    $('[data-toggle="tooltip"]').tooltip();
    getTypologies();
    /**  begin - dropzone **/
    Dropzone.options.myDropzone = {
        addRemoveLinks: true,
        paramName: "file", // The name that will be used to transfer the file
        maxFilesize: 30, // MB
        acceptedFiles: "image/*",
        /*accept: function(file, done) {
            if (file.name == "justinbieber.jpg") {
                done("Naha, you don't.");
            }
            else { done(); }
        },*/
        success: function(file, responseText) {
        // Handle the responseText here. For example, add the text to the preview element:
        //file.previewTemplate = Dropzone.createElement(this.options.previewTemplate);
        //file.previewTemplate.appendChild(document.createTextNode(responseText));
            //console.log(responseText);
            var item = jQuery.parseJSON(responseText);
            addItem(item);
            this.removeFile(file);
        },
        addedfile: function(file) {
           // console.log(file);
            file.previewElement = Dropzone.createElement(this.options.previewTemplate);
            // Create the remove button
            var removeButton = Dropzone.createElement('<a class="btn btn-danger btn-xs" href="#" data-payment-id="1" title="" data-original-title="ELIMINAR" data-toggle="tooltip" data-placement="top"><i class="fa fa-times"></i></a>');
            var _this = this;
            removeButton.addEventListener("click", function (e) {
                // Make sure the button click doesn't submit the form:
                e.preventDefault();
                e.stopPropagation();
                // Remove the file preview.
                _this.removeFile(file);
                // If you want to the delete the file on the server as well,
                // you can do the AJAX request here.
            });
            // Now attach this new element some where in your page
            $("form[name=upload-project-tipologies]").append(file.previewElement);
            // Add the button to the file preview element.
            file.previewElement.appendChild(removeButton);
        }
    };
    /** end -dropzone **/


});
function getTypologies()
{
    var projectId = $("input[name=project-id]").val();
    $.ajax({
        url : base_url + 'admin/AjaxProject/getTypologies',
        dataType:"json",
        type : "POST",
        data : {projectId : projectId},
        success:function(response){
            var list = response;
            $(".list-group").html("");
            $.each(list,function(index,value){
                addItem(value);
            })

            bootbox.hideAll();
        }
    });
}
function addItem(item, replace)
{
    var htmlSource   = $("#ht-typology-item").html();
    var template = Handlebars.compile(htmlSource);
    var data = {item: item};
    var html    = template(data);
    if(replace)
    {
        $(".list-group").find("div[data-project-typology-id="+item.tip_id+"]").closest("div.typology-item").replaceWith(html);
    }
    else
    {
        $(".list-group").append(html);
    }
    loadEventListener(item.tip_id);
    startImageThumbnailComponent(item.tip_id);
}
function loadEventListener(projectImageId)
{
    /*** begin - action buttons */

    $(".project-typology-see-"+projectImageId).bind("click",function(e){
        e.preventDefault();
        var resourceId = $(this).closest("div").data("project-typology-id");
        seeResource(resourceId)
    });
    $(".project-typology-edit-"+projectImageId).bind("click",function(e){
        e.preventDefault();
        var projectTypologyId = $(this).closest("div").data("project-typology-id");
        $.ajax({
            url : base_url + 'admin/AjaxProject/getTypology',
            dataType  :"json",
            type : "POST",
            data : {projectImageId : projectTypologyId},
            success:function(response){
                bootbox.hideAll();
                var htmlSource   = $("#ht-typology-edit").html();
                var template = Handlebars.compile(htmlSource);
                var data = {detail: response};
                var html    = template(data);
                bootbox.confirm({
                    title:"Editar informacion",
                    message:html,
                    buttons: {
                        cancel: {
                            label: 'Cancelar'
                        },
                        confirm: {
                            label: 'Guardar'
                        }
                    },
                    callback: function (result) {
                        var $form = $("form[name=typology-edit-"+projectTypologyId+"]");
                        //var isValid = $form.parsley().validate();
                        if(result)
                        {
                            updateImageInformation($form);
                        }
                    }
                });
                $(".input-masked").inputmask();
                startImageThumbnailComponent(projectTypologyId);
                startImageThumbnailComponent2(projectTypologyId);
                initNormalTynimce(".tinymce");
            }
        });
    });
    $(".project-typology-delete-"+projectImageId).on("click",function(e){
        e.preventDefault();
        var itemId = $(this).closest("div").data("project-typology-id");
        bootbox.confirm({
            title:"Borrar imagen",
            message:"Borrar esta imagen permanentemente?",
            buttons: {
                cancel: {
                    label: 'Cancelar'
                },
                confirm: {
                    label: 'Aceptar'
                }
            },
            callback: function (result) {
                if(result)
                {
                    deleteResource(itemId);
                }
            }
        });
    });
    /*** end - action buttons */
}
function startImageThumbnailComponent(projectTypologyId)
{
    /** begin - Snippet to upload image **/
    $("div.avatar-upload-"+projectTypologyId+" img").bind("click",function()
    {
        $("#avatar-file-"+projectTypologyId).trigger("click");
    });

    $("#avatar-file-"+projectTypologyId).replaceWith("<input id=\"avatar-file-"+projectTypologyId+"\" type=\"file\" name=\"logo\" accept=\"image/*\" class=\"hide\">");
    $("#avatar-file-"+projectTypologyId).change(function() {
        readImage(this);
    });
    $("div.avatar-upload-"+projectTypologyId+" img").css( 'cursor', 'pointer' );
    function readImage(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#avatar-preview-'+projectTypologyId).attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    /** end - Snippet to upload image **/
}
function startImageThumbnailComponent2(projectTypologyId)
{
    /** begin - Snippet to upload image **/
    $("div.avatar-upload-"+projectTypologyId+"-2 img").bind("click",function()
    {
        $("#avatar-file-"+projectTypologyId+"-2").trigger("click");
    });

    $("#avatar-file-"+projectTypologyId+"-2").replaceWith("<input id=\"avatar-file-"+projectTypologyId+"-2\" type=\"file\" name=\"logo2\" accept=\"image/*\" class=\"hide\">");
    $("#avatar-file-"+projectTypologyId+"-2").change(function() {
        readImage(this);
    });
    $("div.avatar-upload-"+projectTypologyId+"-2 img").css( 'cursor', 'pointer' );
    function readImage(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#avatar-preview-'+projectTypologyId+'-2').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    /** end - Snippet to upload image **/
}
function updateImageInformation(form)
{
    var data = new FormData();
    var typologyId = form.find("input[name=typology-id]").val();
    data.append("typologyId", typologyId);
    data.append("title",form.find("input[name=title]").val());
    data.append("titlePrice",form.find("input[name=title-price]").val());
    data.append("price",form.find("input[name=price]").val());
    data.append("builtSurface",form.find("input[name=built-surface]").val());
    data.append("totalSurface",form.find("input[name=total-surface]").val());
    data.append("availability",form.find("input[name=availability]").val());
    data.append("showPrice",form.find("input[name=show-price]:checked").val());
    data.append("showBuiltSurface",form.find("input[name=show-built-surface]:checked").val());
    data.append("showTotalSurface",form.find("input[name=show-total-surface]:checked").val());
    data.append("description",tinyMCE.activeEditor.getContent());
    data.append("file",form.find("input[type=file]")[0].files[0]);
    data.append("file2",form.find("input[type=file]")[1].files[0]);
    $.ajax({
        url : base_url + 'admin/AjaxProject/updateTypologyInformation',
        dataType  :"json",
        type : "POST",
        contentType: false,
        processData: false,
        data : data,
        success:function(response){
            bootbox.hideAll();
            addItem(response,true);
        }
    });
}
function deleteResource(id)
{
    var itemId = id;
    var itemModel = "model_typology";
    $.ajax({
        url : base_url + 'admin/AjaxProject/deleteResource',
        dataType  :"json",
        type : "POST",
        data : {itemId:itemId, itemModel:itemModel},
        success:function(){
            bootbox.hideAll();
            $("div[data-project-typology-id="+itemId+"]").closest("div.typology-item").remove();
        }
    });
}
function seeResource(id)
{
    var src = $("div[data-project-typology-id="+id+"]").closest("div.typology-item").find("img").prop("src");
    src = src.replace("h=100", "");
    src = src.replace("w=150", "w=850");
    bootbox.alert({
        title:"Imagen",
        message: "<p class='text-center'><img src='"+src+"' class='img-responsive'></p>",
        size: 'large'
    });
}