/**
 * Created by Jair on 26/7/2017.
 */
// var showProgressBar = true;
$(document).ready(function() {
    $( document ).ajaxSend(function(event, jqxhr, settings){
        var url = settings.url;
        var request = url.split("/");
        request = request[request.length-1];

        var invalidToShowProgressBar = [
                                            "ajaxDtAllPayments",
                                            "ajaxDtAllProjects",
                                            "getAllCitiesByStateId",
                                            "getAllZonesByStateId",
                                            "getAllUsers",
                                            "ajaxDtAllProperties",
                                            "getAllPublicationsRegisteredByDateRange",
                                            "getAllPaymentsSettlementByDateRange",
                                            "getAllPublicationsActivityByDateRange",
                                            "getUserTypeDetailByDateRange",
                                            "getPropertyOffersBasedOnUsers",
                                            "getPropertyOffersBasedOnTransactionsType",
                                            "getPropertyOffersBasedOnPropertyTypes",
                                            "getAllMessagesToPublicationsByDateRange",
                                            "ajaxDtAllUsers",
                                            "updatePublication",
                                            "renewPublication",
                                            "updateImageOrder",
                                            "getAllUsersRegisteredByDateRange",
                                            "emailNewImageNotification",
                                            "ajaxDtAllNotifies",
                                            "ajaxDtAllMessages",
                                            "ajaxCertificateUser",
                                            "ajaxDtAllLeads",
                                            "getAllForms",
                                            "AjaxPayment",
                                            "tigoMoneyRequestPayment"
                                        ];
        if($.inArray(request, invalidToShowProgressBar) < 0 && !$.isNumeric(request))
        {
            var progressBar = 'Processing...<div class="progress">';
            progressBar += '<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">';
            progressBar += '</div></div>';
            bootbox.dialog({
                message: progressBar,
                closeButton: false
            });
        }
    });

    $(document).ajaxError(function (request, status, error) {
        bootbox.hideAll();
        bootbox.alert({
            title:"Oops, algo salió mal",
            message: "Por favor intente de nuevo, si el problema persiste contacte al administrador del sistema."
        });
    });
});
