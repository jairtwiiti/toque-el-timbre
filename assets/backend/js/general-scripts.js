/**
 * Created by Jair on 28/11/2018.
 */
createCutText();
imageErrorHandler();
$(document).ajaxComplete(function(){
    imageErrorHandler();
});
$(document).ready(function() {

});
$(window).on('resize', function() {
    cutText();
});
function blockArea(content)
{
    content.block({
        message: '<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>',
        overlayCSS: {
            backgroundColor: '#fff',
            opacity: 0.8,
            cursor: 'wait'
        },
        css: {
            border: 0,
            padding: 0,
            backgroundColor: 'transparent'
        }
    });
}

function createCutText()
{
    $.fn.truncate = function (lines, readmore) {
        this.text(this.text().trim());
        lines = typeof lines !== 'undefined' ? lines : 1;
        readmore = typeof readmore !== 'undefined' ? readmore : '.readmore';
        let lineHeight = window.lineHeight(this[0]);
        if (this.attr('title')) {
            this.text(this.attr('title'));
        }
        if (!this.attr('data-link') && this.find('a'+readmore).length > 0) {
            this.attr('data-link', this.find('a'+readmore)[0].outerHTML);
        }
        let link = this.attr('data-link');
        if (this.height() > lines * lineHeight) {
            if (!this.attr("title")) {
                this.attr("title", this.html());
            }
            let words = this.attr("title").split(" ");
            let str = "";
            let prevstr = "";
            this.text("");
            for (let i = 0; i < words.length; i++) {
                if (this.height() > lines * lineHeight) {
                    this.html(prevstr.trim() + "&hellip; " + (typeof link !== 'undefined' ? ' ' + link : ''));
                    break;
                }
                prevstr = str;
                str += words[i] + " ";
                this.html(str.trim() + "&hellip;" + (typeof link !== 'undefined' ? ' ' + link : ''));
            }
            if (this.height() > lines * lineHeight) {
                this.html(prevstr.trim() + "&hellip; " + (typeof link !== 'undefined' ? ' ' + link : ''));
            }
        }
        return this;
    }
}

function cutText(content)
{
    content = content === undefined?"":content;
    $('.cut-text').each(function() {
        //@ts-ignore
        let lines = $(this).data('ct-lines') !== "" && $(this).data('ct-lines') !== undefined?$(this).data('ct-lines'):1;
        $(this).truncate(lines);
    });
}

function imageErrorHandler()
{
    $( "img" ).error(function() {
        let $image = $(this);
        let defaultImage = $image.data('default-image') != "" && $image.data('default-image') != undefined?$image.data('default-image'):"default.png";
        let width = $image.data('default-width') != "" && $image.data('default-width') !== undefined?"&w="+$image.data('default-width'):"";
        let height = $image.data('default-height') != "" && $image.data('default-height') !== undefined?"&h="+$image.data('default-height'):"";
        let src = base_url+"assets/img/default.png";
        if(width != "" || height != "")
        {
            src =  base_url + "librerias/timthumb.php?src="+base_url+"assets/img/"+defaultImage+width+height;
            // {{base_url}}timthumb/timthumb.php?src={{base_url}}assets/images/twiitiSmallLogo.png&h=50
        }
        $image.attr( "src", src);
    });
}