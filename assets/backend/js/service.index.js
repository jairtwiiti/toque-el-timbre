/**
 * Created by Jair on 28/03/2018.
 */
$.fn.dataTable.Buttons.swfPath = base_url + 'assets/backend/global/plugins/datatables/plugins/Buttons142/swf/flashExport.swf';
$(document).ready(function() {
    $('input.date-picker').datepicker({});

    var additionalParameter = new DTAdditionalParameterHandler("#extra-request-data","#service-index");
    additionalParameter.addParameterObject('facebook-form-id','text');
    additionalParameter.addParameterObject('start-date','text');
    additionalParameter.addParameterObject('end-date','text');
    additionalParameter.setButtonFilter('#send-filters');
    additionalParameter.setButtonRest('#remove-additional-parameters');
    additionalParameter.loadEventHandlers();

    var buttonCommon = {
        title: "Leads",
        exportOptions: {
            page: 'all',
            columns: [1, 2, 3, 6, 7, 8]
        }
    };
    //Horizontal Icons dataTable
    var oTable = $('#service-index').dataTable({
        "processing" : true,
        "serverSide" : true,
        "order": [[ 8, "desc" ]],
        "ajax" : {
            url : base_url + 'admin/AjaxService/ajaxDtAllServices',
            type : 'POST',
            data:function ( data ) {
                data.additionalParameters = additionalParameter.getList();
            }
        },
        "dom": "<'row'<'col-sm-6'Bl><'col-sm-6 text-right'f>>rt<'row'<'col-sm-6'i><'col-sm-6 text-right'p>>",
        "lengthMenu": [ [10, 25, 50, 100, 100000], [10, 25, 50,100, 100000] ],
        "columns" : [{
            "data" : "ser_id"
        }, {
            "data" : "ser_descripcion"
        }, {
            "data" : "ser_precio"
        }, {
            "data" : "ser_dias"
        }, {
            "data" : "ser_estado"
        }, {
            "data" : "ser_tipo"
        }, {
            "defaultContent" : " ",
            "searchable" : false,
            "orderable" : false,
            "render" : function(data, type, row, meta) {
                var html = '<a class="btn btn-primary btn-xs see-project" href="#" data-project-id="'+row.proy_id+'" title="" data-original-title="VER"  data-toggle="tooltip" data-placement="top"><i class="fa fa-search"></i></a>';
                html += '<a target="_blank" class="btn btn-primary btn-xs" href="'+base_url + 'admin/Project/edit/' +row.proy_id+'" title="" data-original-title="EDITAR"  data-toggle="tooltip" data-placement="top"><i class="fa fa-pencil"></i></a>';
                html += '<a target="_blank" class="btn btn-primary btn-xs" href="'+base_url + 'admin/Project/publication/' +row.proy_id+'" title="" data-original-title="PUBLICACIONES"  data-toggle="tooltip" data-placement="top"><i class="fa fa-home"></i></a>';
                html += '<a target="_blank" class="btn btn-primary btn-xs" href="'+base_url + 'admin/Project/images/' +row.proy_id+'" title="" data-original-title="FOTOS"  data-toggle="tooltip" data-placement="top"><i class="fa fa-file-image-o"></i></a>';
                html += '<a target="_blank" class="btn btn-primary btn-xs" href="'+base_url + 'admin/Project/typology/' +row.proy_id+'" title="" data-original-title="TIPOLOGIA"  data-toggle="tooltip" data-placement="top"><i class="fa fa-cubes"></i></a>';
                html += '<a target="_blank" class="btn btn-primary btn-xs" href="'+base_url + 'admin/Project/finishingDetail/' +row.proy_id+'" title="" data-original-title="ACABADO"  data-toggle="tooltip" data-placement="top"><i class="fa fa-paint-brush"></i></a>';
                html += '<a target="_blank" class="btn btn-primary btn-xs" href="'+base_url + 'admin/Project/socialArea/' +row.proy_id+'" title="" data-original-title="AREA SOCIAL"  data-toggle="tooltip" data-placement="top"><i class="fa fa-users"></i></a>';
                html += '<a target="_blank" class="btn btn-primary btn-xs" href="'+base_url + 'admin/Project/slideShow/' +row.proy_id+'" title="" data-original-title="SLIDE SHOW"  data-toggle="tooltip" data-placement="top"><i class="fa fa-caret-square-o-right"></i></a>';
                html += '<a target="_blank" class="btn btn-primary btn-xs" href="'+base_url + 'admin/Project/settings/' +row.proy_id+'" title="" data-original-title="CONFIGURACION"  data-toggle="tooltip" data-placement="top"><i class="fa fa-wrench"></i></a>';
                html += '<a target="_blank" class="btn btn-primary btn-xs" href="'+base_url + 'admin/Project/leads/' +row.proy_id+'" title="" data-original-title="LEADS"  data-toggle="tooltip" data-placement="top"><i class="fa fa-list-alt"></i></a>';

                return html;
            }
        }],
        "drawCallback" : function(object) {
            $('[data-toggle="tooltip"]').tooltip();
            this.api().column(0).visible(false);
            this.api().column(4).visible(false);
            this.api().column(5).visible(false);
            this.api().column(7).visible(false);
        },
        "buttons": [
            $.extend( true, {}, buttonCommon,{
                extend: 'copy'
            } ),
            $.extend( true, {}, buttonCommon,{
                extend: 'csv'
            } ),
            $.extend( true, {}, buttonCommon,{
                extend: 'pdf',
            } ),
            $.extend( true, {}, buttonCommon,{
                extend: 'excel'
            } ),
            $.extend( true, {}, buttonCommon,{
                extend: 'print',
                customize: function ( win ){
                    $(win.document).find("link[href*='style.css']").remove();
                }
            } )
        ]
    });
    $('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Search');
    $('.dataTables_length select').addClass('form-control');
    oTable.fnSetFilteringDelay(1000);

    //select2 ajax for facebook forms
    $('#ajax-get-facebook-forms').select2({
        placeholder : "Seleccione uno o mas formularios",
        allowClear : true,
        tags:true,
        ajax : {
            type : "post",
            dataType : "json",
            url : base_url + 'admin/AjaxFacebookForm/getAllForms',
            quietMillis : 600,
            minimumInputLength : 4,
            data : function(term, page) {
                return {
                    term : term, //search term
                    limit : 5 // page size
                };
            },
            results : function(data) {
                return {
                    results : data
                };
            }
        },
        width : "100%",
        initSelection : function(element, callback) {
            var jsonResp = JSON.parse(element.val());
            callback(jsonResp);
            element.val(jsonResp.id);
        }
    });

    /*###$################################################################## BEGIN - SEE PAYMENT*/
    $('#data-table').on("click","a.see-project", function (e) {
        e.preventDefault();
        var projectId = $(this).parent().find(".see-project").data("project-id");
        getProjectDetail(projectId);

    });
    /*######################################################################## END - SEE PAYMENT*/
});
function getProjectDetail(id)
{
    var projectId = id;

    $.ajax({
        url : base_url + 'admin/AjaxProject/getDetail',
        dataType  :"json",
        type : "POST",
        data : {projectId : projectId},
        success:function(response){
            bootbox.hideAll();
            var htmlSource   = $("#ht-project-detail").html();
            var template = Handlebars.compile(htmlSource);
            var data = {detail: response};
            var html    = template(data);
            bootbox.alert({
                title:"Detalle del proyecto",
                message:html
            });
        }
    });
}
function sanitizeString(str)
{
    if(typeof str == "string")
    {
        str = str.replace(/[^a-z0-9áéíóúñü \.,_-]/gim,"");
        str = str.replace(/ /g,"-");
        return str.trim();
    }

}