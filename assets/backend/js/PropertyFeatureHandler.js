/**
 * Created by Jair on 8/9/2017.
 */
function PropertyFeatureHandler(list)
{
    var featureList = list
    var listIndexed = [];
    var defaultDataIndexed = [];
    var targetContent = "#specific-features";

    this.drawFeatures = function(propertyTypeId)
    {
        this.indexList(featureList);

        if(propertyTypeId == undefined)
        {
            propertyTypeId = $("select[name=property-type] :selected").val();
        }
        var html = this.getFeaturesByPropertyTypeId(propertyTypeId);
        $(targetContent).html(html);
        $(".input-masked").inputmask();
    };

    this.getFeaturesByPropertyTypeId = function(propertyTypeId)
    {
        var html = "";
        var propertyFeatureHandler = this;
        var templateList = {input:"ht-feature-type-input", select:"ht-feature-type-select2", radio:"ht-feature-type-radio"};
        $.each(listIndexed[propertyTypeId],function(index,value){
            value = propertyFeatureHandler.addDefaultDataToFeature(value);
            html+= propertyFeatureHandler.getHtmlComponent(templateList[value.car_tipo],value);
        });
        return html;
    };

    this.getHtmlComponent = function(componentType,data)
    {
        var htmlSource   = $("#"+componentType).html();
        var template = Handlebars.compile(htmlSource);
        data.valores = data.valores != null && typeof data.valores == "string"?data.valores.split(","):data.valores;
        data.parsleyValidation = "abc";
        data.inputMasked = inputMasked;
        var data = {feature:data};
        var html = template(data);
        return html;
    };

    this.indexList = function(list)
    {
        if(listIndexed.length == 0)
        {
            $.each(list, function(index,value){
                if(listIndexed[value.cat_id] == undefined)
                {
                    listIndexed[value.cat_id] = new Array();
                }
                listIndexed[value.cat_id].push(value);
            });
        }
    };

    this.addDefaultDataToFeature = function(feature)
    {
        if(defaultDataIndexed.length > 0 && defaultDataIndexed[feature.car_id] != undefined)
        {
            feature.default = defaultDataIndexed[feature.car_id][0].eca_valor;
        }
        return feature;
    };

    this.setDefaultData = function(data)
    {
        if(defaultDataIndexed.length == 0)
        {
            $.each(data, function(index,value){
                if(defaultDataIndexed[value.car_id] == undefined)
                {
                    defaultDataIndexed[value.car_id] = new Array();
                }
                defaultDataIndexed[value.car_id].push(value);
            });
        }
    };

    this.loadEventHandlers = function()
    {
        var propertyFeatureHandler = this;
        $("select[name=property-type]").on("change",function(){
            var propertyTypeId = $(this).select2("data").id;
            propertyFeatureHandler.drawFeatures(propertyTypeId);
            propertyFeatureHandler.initializeSelect2();
        });
    };
    this.initializeSelect2 = function()
    {
        $(targetContent+" .select2").select2({
            width : '100%',
            placeholder: "Elija una opcion"
        });
    };
}