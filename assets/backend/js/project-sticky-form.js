/**
 * Created by Jair on 20/11/2017.
 */
$(document).ready(function($) {
    $('.project-contact-form').followTo();
});

$.fn.followTo = function () {

        var $this = this,
            $window = $(window);
        $window.scroll(function(e){
            if($( window ).width() >= 975 && $(".col-detail>div.row").height() > $(".project-contact-form").height())
            {
                var pos = $(".col-detail").height() - $(".project-contact-form").height() + $("nav").height() + $(".padding_top").height() + $(".project-banner").height()+ $(".project-menu").height()+20;
                if ($window.scrollTop() >= 783 && $window.scrollTop() <= pos)
                {
                    console.log("1",$window.scrollTop(),pos);
                    $this.css({
                        position: 'fixed',
                        top: 10,
                        width:"263px",
                        marginRight:"104px",
                        paddingBottom:"48px"
                    });
                }
                else if($window.scrollTop() > pos)
                {
                    console.log("2",$window.scrollTop(),pos);
                    $this.css({
                        position: 'absolute',
                        top: pos-783,//pos,
                        marginRight:"15px",
                        paddingBottom:"48px"
                    });
                }
                else
                {
                    console.log("3",$window.scrollTop(),pos);
                    $this.css({
                        position: 'absolute',
                        top: 0,
                        marginRight:"15px",
                        paddingBottom:"48px"
                    });
                }
            }
        });

};
