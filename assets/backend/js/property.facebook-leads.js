/**
 * Created by Jair on 22/3/2018.
 */
$.fn.dataTable.Buttons.swfPath = base_url + 'assets/backend/global/plugins/datatables/plugins/Buttons142/swf/flashExport.swf';
$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();

    startDataTable();

    $(document).on("change",".filter-leads",function () {
        var propertyId = $(this).data("property-id");
        var formId = $(this).val();
        getLeads(propertyId, formId);
    })
});
function getLeads(propertyId, formId)
{
    $.ajax({
        url : base_url + 'admin/AjaxFacebookLead/getByPropertyIdAndFormId',
        dataType  :"json",
        type : "POST",
        data : {propertyId:propertyId, formId : formId},
        success:function(response){
            bootbox.hideAll();
            var tr = "";
            var fullName = "";
            $.each(response,function(index, lead){

                fullName = lead.fle_full_name === ""?lead.fle_first_name+" "+lead.fle_last_name:lead.fle_full_name;
                tr += '<tr>' +
                    '<td>'+fullName+'</td>' +
                    '<td>'+lead.fle_email+'</td>' +
                    '<td>'+lead.fle_phone_number.replace("+591", "")+'</td>' +
                    '<td>'+lead.fle_consulta+'</td>' +
                    '<td>'+lead.fle_createdon+'</td>' +
                    '</tr>';
            });

            $("#project-facebook-leads").DataTable().destroy();
            $("#project-facebook-leads").find("tbody").html(tr);
            startDataTable();
        }
    });
}

function addFacebookForm(projectId, formId)
{
    $.ajax({
        url : base_url + 'admin/AjaxFacebookForm/AddForm',
        dataType  :"json",
        type : "POST",
        data : {publicationId:projectId, formId : formId, publicationType:2},
        success:function(response){
            window.location.reload()
        }
    });
}

function startDataTable()
{
    var propertyId = $("#property-facebook-leads").data("property-id");
    var isAdmin = $("input[name=is-admin]").val();
    var addFormButton =
        {
            text: 'Add form',
            action: function ( e, dt, node, config )
            {
                bootbox.prompt("Facebook form ID!",
                    function(result)
                    {
                        if(result !== null)
                        {
                            addFacebookForm(propertyId, result);
                        }
                    }
                );
            }
        };
    var buttons = [
        {
            extend: 'copy',
            title: 'Leads',
            exportOptions: {
                page: 'all'
            }
        },
        {
            extend: 'csv',
            title: 'Leads',
            exportOptions: {
                page: 'all'
            }
        },
        {
            extend: 'pdf',
            title: 'Leads',
            exportOptions: {
                page: 'all'
            }
        },
        {
            extend: 'excel',
            title: 'Leads',
            exportOptions: {
                page: 'all'
            }
        },
        {
            extend: 'print',
            title: 'Leads',
            exportOptions: {
                page: 'all'
            }
        }
    ];
    if(isAdmin === "1")
    {
        buttons.push(addFormButton);
    }
    var table = $("#property-facebook-leads").DataTable({
        dom: 'Bfrtip',
        buttons: buttons
    });
    table.order( [ 4, 'desc' ] ).draw();
}
