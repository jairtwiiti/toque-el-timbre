var itemToUpload = 0;
$(document).ready(function() {
    /**
     * Created by Jair on 4/7/2017.
     */

    $( ".sortable" ).sortable({
        update : function(event, ui) {
            var newOrder = $(".sortable").sortable('toArray');
            $.ajax({
                url : base_url + 'admin/AjaxProperty/updateImageOrder',
                type : "POST",
                data : {
                    "ids" : newOrder
                }
            });
        },
    });
    $('[data-toggle="tooltip"]').tooltip();
    getImages();
    /**  begin - dropzone **/
    var propertyImageArray = [];
    var rejectedImages = [];
    Dropzone.prototype.defaultOptions.dictMaxFilesExceeded = "Solo puedes cargar 15 imágenes al mismo tiempo";
    Dropzone.options.myDropzone = {
        addRemoveLinks: true,
        autoProcessQueue: false,
        parallelUploads: 15,
        maxFiles: 15,
        paramName: "file", // The name that will be used to transfer the file
        maxFilesize: 30, // MB
        acceptedFiles: "image/*",
       /*accept: function(file, done) {
           if (file.name == "justinbieber.jpg") {
               done("Naha, you don't.");
           }
           else { done(); }
       },*/
        success: function(file, responseText) {
       // Handle the responseText here. For example, add the text to the preview element:
       //file.previewTemplate = Dropzone.createElement(this.options.previewTemplate);
       //file.previewTemplate.appendChild(document.createTextNode(responseText));
           //console.log(responseText);
            try {
                item = jQuery.parseJSON(responseText);
            } catch (exception) {
                item = null;
                rejectedImages.push(file);
            }

            if (item)
            {
                propertyImageArray.push({fot_archivo:item.fot_archivo});
                addItem(item);
                this.removeFile(file);
            }
        },
        addedfile: function(file) {
            itemToUpload++;
          // console.log(file);
           file.previewElement = Dropzone.createElement(this.options.previewTemplate);
           // Create the remove button
           var removeButton = Dropzone.createElement('<a class="btn btn-danger btn-xs" href="#" data-payment-id="1" title="" data-original-title="ELIMINAR" data-toggle="tooltip" data-placement="top"><i class="fa fa-times"></i></a>');
           var _this = this;
           removeButton.addEventListener("click", function (e) {
               // Make sure the button click doesn't submit the form:
               e.preventDefault();
               e.stopPropagation();
               // Remove the file preview.
               _this.removeFile(file);
               // If you want to the delete the file on the server as well,
               // you can do the AJAX request here.
           });
           // Now attach this new element some where in your page
           $("form[name=upload-property-images]").append(file.previewElement);
           // Add the button to the file preview element.
           file.previewElement.appendChild(removeButton);
        },
        maxfilesreached: function(files)
        {
            var _this = this;
            $.each(files,function(index,file)
            {
               if(file.accepted === false)
               {
                   _this.removeFile(file);
               }
            });
        },
        complete: function (file) {
            if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0)
            {
                throwAd();
                $('.dropzone-start-queue').removeClass("disabled");
                $('.dropzone-start-queue').html("Guardar <i class='fa fa-upload' aria-hidden='true'></i>");
                validateButtonToDeleteImage();
                //TODO: send quantity of filed that were uploaded
                checkIfPendingToApproval(itemToUpload);
                newImageNotification(propertyImageArray,rejectedImages);
                propertyImageArray = [];
                rejectedImages = [];
                itemToUpload = 0;
            }
        },
        error: function(param1, param2)
        {
            console.log(param1, param2);
        }
    };

    /** end -dropzone **/
    $('.dropzone-start-queue').on("click",function(e){
       e.preventDefault();
        if(Dropzone.instances[0].getQueuedFiles().length >= 1)
        {
            Dropzone.instances[0].processQueue();
            $(this).addClass("disabled");
            $(this).html("Guardando <i class='fa fa-spinner fa-pulse'></i>");
        }
    });

    $('.dropzone-add-to-queue').on("click",function(e){
        e.preventDefault();
        $("form[name=upload-property-images]").trigger("click");
    });
});
function throwAd()
{
    var showModal = $("input[name=throw-ad]").val();
    var publicationId = $("input[name=publication-id]").val();
    if(showModal == "1")
    {
        $("input[name=throw-ad]").val("");
        var html = "";
        html += "<p>Te gustaria mejorar el alcance de tu anuncio?</p>";
        html += "<p><i class='fa fa-check-square-o' aria-hidden='true'></i> Aparecer en pagina principal.<br>";
        html += "<i class='fa fa-check-square-o' aria-hidden='true'></i> Primeros lugares en resultados de búsqueda.<br>";
        html += "<i class='fa fa-check-square-o' aria-hidden='true'></i> Primeros lugares en anuncios similares.<br>";
        html += "<p>Tambien lo puedes hacer después desde tu lista de anuncios.</p>";
        bootbox.confirm({
            title:"<b>Mejorar anuncio <i class='fa fa-arrow-up' aria-hidden='true'></i></b>",
            message:html,
            buttons: {
                cancel: {
                    label: 'Lo hare después'
                },
                confirm: {
                    label: 'Si me gustaría'
                }
            },
            callback: function (result) {
                if(result)
                {
                    window.location = base_url + "dashboard/publications/pay/"+publicationId;
                }
            }
        });
    }
}
function newImageNotification(propertyImageArray, rejectedImages)
{
    var propertyId = $("input[name=property-id]").val();
    $.ajax({
        url : base_url + 'admin/AjaxProperty/emailNewImageNotification',
        dataType:"json",
        type : "POST",
        data : {propertyId : propertyId, propertyImageArray: propertyImageArray},
        success:function(response){

            if(rejectedImages.length > 0)
            {
                var html = "<p>";
                $.each(rejectedImages,function(index, image){
                    html+="<i class='fa fa-times'> "+image.name+"</i><br>";
                });
                html = html.substring(0,html.length-4);
                html += "<p>";
                bootbox.alert({
                    title: "Error al cargar algunas imágenes",
                    message:html
                });
            }
        }
    });
}
function getImages()
{
    var propertyId = $("input[name=property-id]").val();
    $.ajax({
        url : base_url + 'admin/AjaxProperty/getImages',
        dataType:"json",
        type : "POST",
        data : {propertyId : propertyId},
        success:function(response){
            var list = response;
            $(".list-group").html("");
            $.each(list,function(index,value){
                addItem(value);
            });
            bootbox.hideAll();
            throwLastStepExplanation();
            validateButtonToDeleteImage();
        }
    });
}
function addItem(item, replace)
{
    var htmlSource   = $("#ht-image-item").html();
    var template = Handlebars.compile(htmlSource);
    var data = {item: item};
    var html    = template(data);
    if(replace)
    {
        $(".list-group").find("div[data-project-image-id="+item.fot_id+"]").closest("div.image-item").replaceWith(html);
    }
    else
    {
        $(".list-group").append(html);
    }
    loadEventListener(item.fot_id);
    startImageThumbnailComponent(item.fot_id);
}
function loadEventListener(propertyImageId)
{
    /*** begin - action buttons */

    $(".property-image-see-"+propertyImageId).bind("click",function(e){
        e.preventDefault();
        var propertyImageId = $(this).closest("div").data("property-image-id");
        seeResource(propertyImageId);
    });

    $(".property-image-delete-"+propertyImageId).on("click",function(e){
        e.preventDefault();
        var itemId = $(this).closest("div").data("property-image-id");
        var itemsRemain = $(".list-group").children().length;
        if(itemsRemain > 1)
        {
            bootbox.confirm({
                title:"Borrar imagen",
                message:"Borrar esta imagen permanentemente?",
                buttons: {
                    cancel: {
                        label: 'Cancelar'
                    },
                    confirm: {
                        label: 'Aceptar'
                    }
                },
                callback: function (result) {
                    if(result)
                    {
                        deleteResource(itemId);
                    }
                }
            });
        }
        else
        {
            bootbox.alert({
                title:"<h3>Ups!!!</h3>",
                message: "<h5>Lo sentimos pero no puedes dejar sin imagenes tu publicacion.</h5>",
                size:"medium"
            });
        }

    });
    /*** end - action buttons */
}
function startImageThumbnailComponent(propertyImageId)
{
    /** begin - Snippet to upload image **/
    $("div.avatar-upload-"+propertyImageId+" img").bind("click",function()
    {
        $("#avatar-file-"+propertyImageId).trigger("click");
    });

    $("#avatar-file-"+propertyImageId).replaceWith("<input id=\"avatar-file-"+propertyImageId+"\" type=\"file\" name=\"logo\" accept=\"image/*\" class=\"hide\">");
    $("#avatar-file-"+propertyImageId).change(function() {
        readImage(this);
    });
    $("div.avatar-upload-"+propertyImageId+" img").css( 'cursor', 'pointer' );
    function readImage(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#avatar-preview-'+propertyImageId).attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    /** end - Snippet to upload image **/
}

function deleteResource(id)
{
    var itemId = id;
    var propertyId = $("input[name=property-id]").val();
    $.ajax({
        url : base_url + 'admin/AjaxProperty/deleteResource',
        dataType  :"json",
        type : "POST",
        data : {itemId:itemId,propertyId:propertyId},
        success:function(response){
            bootbox.hideAll();
            if(response.response === 1)
            {
                $("div[data-property-image-id="+itemId+"]").closest("div.image-item").remove();
            }
            else
            {
                bootbox.alert({
                    title:"<h3>No es no.</h3>",
                    message: "<h5>"+response.message+"</h5>",
                    size:"medium"
                });
            }
            validateButtonToDeleteImage();
        }
    });
}

function seeResource(id)
{
    var src = $("div[data-property-image-id="+id+"]").closest("div.image-item").find("img").prop("src");
    src = src.replace("h=100", "");
    src = src.replace("w=150", "w=850");
    bootbox.alert({
        title:"Imagen",
        message: "<p class='text-center'><img src='"+src+"' class='img-responsive'></p>",
        size: 'large'
    });
}

function throwLastStepExplanation()
{
    var showExplanation = $("input[name=throw-last-step-explanation]").val();
    if(showExplanation === "1")
    {
        bootbox.alert({
            title:"<h3>Felicidades!!!</h3>",
            message: "<h5>Tu publicacion esta casi lista... <br>Solo debes AGREGAR y GUARDAR al menos 1 imagen para que sea aprobada. Puedes hacerlo despues.. pero mientras tanto tu publicacion no sera visible para los visitantes de Toque El Timbre</h5>",
            size:"medium"
        });
    }
}

function validateButtonToDeleteImage()
{
    var itemsRemain = $(".list-group").children().length;
    if(itemsRemain == 1)
    {
        $(".list-group").find("a[class*=property-image-delete-]").addClass("hide");
    }
    else if(itemsRemain > 1)
    {
        $(".list-group").find("a[class*=property-image-delete-]").removeClass("hide");
    }
}

function checkIfPendingToApproval(currentItemsLoaded)
{
    var propertyId = $("input[name=property-id]").val();
    $.ajax({
        url : base_url + 'admin/AjaxProperty/checkIfPendingToApproval',
        type : "POST",
        dataType  :"json",
        data : {
            currentItemsUploaded : currentItemsLoaded,
            propertyId:propertyId
        },
        success:function(response)
        {
            if(response.response === 1)
            {
                bootbox.alert({
                    title:"<h4>Tu publicacion ha sido aprobada!!!</h4>",
                    message: "<h4>Ahora es visible por todos los visitantes de Toque El Timbre</h4>",
                    size:"medium"
                });
            }
        }
    });
}