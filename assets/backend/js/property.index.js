/**
 * Created by Jair on 28/7/2017.
 */
$(document).ready(function() {
    let paymentHandler = new PaymentHandler();
    paymentHandler.loadEventHandler();
    // paymentHandler.paymentOptions('2176209008');
    //end - Snippet to upload image
    $('input.date-picker').datepicker({});

    var additionalParameter = new DTAdditionalParameterHandler("#extra-request-data","#data-table");
    additionalParameter.addParameterObject('start-date','text');
    additionalParameter.addParameterObject('end-date','text');
    additionalParameter.addParameterObject('date-to-filter','radio');
    additionalParameter.addParameterObject('show-expired','checkbox');
    additionalParameter.addParameterObject('user','text');
    additionalParameter.setButtonFilter('#send-filters');
    additionalParameter.setButtonRest('#remove-additional-parameters');
    additionalParameter.loadEventHandlers();


    var buttonCommon = {
        title: "Inmuebles",
        exportOptions: {
            page: 'all',
            columns: [0, 1, 2, 3, 5]
        }
    };
    //Horizontal Icons dataTable
    var oTable = $('#data-table').dataTable({
        "processing" : true,
        "serverSide" : true,
        "ajax" : {
            url : base_url + 'admin/AjaxProperty/ajaxDtAllProperties',
            type : 'POST',
            data:function ( data ) {
                data.additionalParameters = additionalParameter.getList();
            }
        },
        "dom": "<'row'<'col-sm-6'Bl><'col-sm-6 text-right'f>>rt<'row'<'col-sm-6'i><'col-sm-6 text-right'p>>",
        "lengthMenu": [ [10, 25, 50, 100, 100000], [10, 25, 50,100, 100000] ],
        "order": [[ 5, "desc" ]],
        "columns" : [{
            "data" : "pub_id",
        }, {
            "data" : "inm_nombre",
            // "searchable" : false,
            "orderable" : false,
            "render" : function(data, type, row, meta) {
                var url = base_url + "buscar/"+row.cat_nombre+"-en-"+row.for_descripcion+"-en-"+row.dep_nombre+"/"+row.inm_seo;
                url = findAndReplace(url, " ", "-");
                html = "<a class='property-name-as-link' href='"+url.toLocaleLowerCase()+"' target='_blank'>"+row.inm_nombre+"</a>";
                return html;
            }
        }, {
            "data" : "inm_visitas",
        }, {
            "data" : "inm_publicado",
            "visible":false
        }, {
            "defaultContent" : " ",
            "searchable" : false,
            "orderable" : false,
            "render" : function(data, type, row, meta) {
                var currentDate = new Date();
                var expirationDate = new Date(row.pub_vig_fin + " 23:59:59");
                var html = "";
                if(currentDate < expirationDate)
                {
                    var checked = row.inm_publicado == 'Si'?"checked":"";
                    html += '<div class="checkbox">';
                    html += '<label>';
                    html += '<input type="checkbox" class="deactivate-publication" value="" '+checked+' data-property-id="'+row.inm_id+'">';
                    html += '<i class="fa fa-spinner fa-pulse fa-fw pull-left hide deactivate-publication-pulse"style="margin-top: 3px;margin-right: 5px;"></i>';
                    html += '</label>';
                    html += '</div>';
                }
                else
                {
                    html += '<span class="label label-danger">Expirado</span>';
                }

                return html;
            }
        }, {
            "data" : "pub_vig_ini",
            "render" : function(data, type, row, meta) {
                var result = "";
                if(row.pub_vig_ini !== "" && row.pub_vig_ini !== null)
                {
                    var dateObject = new Date(row.pub_vig_ini+" 00:00:00");
                    var date = dateObject.getDate() < 10? "0"+dateObject.getDate():dateObject.getDate();
                    var month = (dateObject.getMonth()+1) < 10? "0"+(dateObject.getMonth()+1):(dateObject.getMonth()+1);
                    var year = dateObject.getFullYear();
                    result = date+"-"+month+"-"+year;
                }
                return result;
            }
        }, {
            "data" : "pub_vig_fin",
            "render" : function(data, type, row, meta) {
                var result = "";
                if(row.pub_vig_fin !== "" && row.pub_vig_fin !== null)
                {
                    var dateObject = new Date(row.pub_vig_fin+" 00:00:00");
                    var date = dateObject.getDate() < 10? "0"+dateObject.getDate():dateObject.getDate();
                    var month = (dateObject.getMonth()+1) < 10? "0"+(dateObject.getMonth()+1):(dateObject.getMonth()+1);
                    var year = dateObject.getFullYear();
                    result = date+"-"+month+"-"+year;
                }
                return result;
            }
        }, {
            "defaultContent" : " ",
            "searchable" : false,
            "orderable" : false,
            "render" : function(data, type, row, meta) {

                var currentDate = new Date();
                currentDate.setHours(23);
                currentDate.setMinutes(59);
                currentDate.setSeconds(59);
                currentDate.setDate(currentDate.getDate()+3);
                var expirationDate = new Date(row.pub_vig_fin + " 23:59:59");
                var renewButton = "";
                if(currentDate >= expirationDate)
                {
                    renewButton = '<a class="btn btn-warning btn-xs renew-publication" data-publication-id="'+row.pub_id+'" title="" data-original-title="RENOVAR"  data-toggle="tooltip" data-placement="top"><i class="fa fa-recycle"></i></a>';
                    renewButton += '<i class="fa fa-spinner fa-pulse fa-fw pull-left hide renew-publication-pulse"style="margin-top: 3px;margin-right: 5px;"></i>';
                }

                let html = '<a class="btn btn-primary btn-xs see-record" href="#" data-record-id="'+row.pub_id+'" title="" data-original-title="VER"  data-toggle="tooltip" data-placement="top"><i class="fa fa-search"></i></a>';
                    html += '<a target="_blank" class="btn btn-primary btn-xs" href="'+base_url + 'admin/Property/edit/' +row.inm_id+'" title="" data-original-title="EDITAR"  data-toggle="tooltip" data-placement="top"><i class="fa fa-pencil"></i></a>';
                    html += '<a target="_blank" class="btn btn-primary btn-xs" href="'+base_url + 'admin/Property/images/' +row.inm_id+'" title="" data-original-title="IMAGENES"  data-toggle="tooltip" data-placement="top"><i class="fa fa-file-image-o"></i></a>';
                    html += '<a target="_blank" class="btn btn-primary btn-xs" href="'+base_url + 'admin/Property/leads/' +row.inm_id+'" title="" data-original-title="LEADS"  data-toggle="tooltip" data-placement="top"><i class="fa fa-list-alt"></i></a>';
                    if(currentDate < expirationDate && row.total_pending_payments === "0")
                    {
                        html += '<a target="_blank" class="btn btn-primary purple-studio btn-xs purchase" href="#" title="" data-publication-id="'+row.pub_id+'" data-original-title="MEJORAR ANUNCIO"  data-toggle="tooltip" data-placement="top"><i class="fa fa-arrow-up"></i></a>';
                    }
                    else if(currentDate < expirationDate && row.total_pending_payments !== "0")
                    {
                        html += '<a target="_blank" class="btn btn-primary purple-studio btn-xs" href="'+base_url+'dashboard/payments?status=Pendiente&uid='+row.pub_id+'&userid='+row.usu_id+'" title="" data-original-title="PAGOS PENDIENTES"  data-toggle="tooltip" data-placement="top"><i class="fa fa-money"></i></a>';
                    }

                    html += renewButton;
                    html += '<a class="btn btn-danger btn-xs delete-record" data-publication-id="'+row.pub_id+'" href="#" title="" data-original-title="ELIMINAR"  data-toggle="tooltip" data-placement="top"><i class="fa fa-times"></i></a>';
                return html;
            }
        }],
        "drawCallback" : function(object) {
            //this.api().columns.adjust();
            $('[data-toggle="tooltip"]').tooltip();
            $('.deactivate-publication').on('change',function(){
                updatePublicationStatus($(this));
            });

            $('a.renew-publication').on("click",function(){
                var button = $(this);
                var id = $(this).closest("tr").find("td")[0];
                var id = $(id).text();
                var name = $(this).closest("tr").find("td")[1];
                var name = $(name).text();
                var expirationDate = $(this).closest("tr").find("td")[5];
                var expirationDate = $(expirationDate).text();

                html = "<h4>Renovar esta publication?</h4>";
                html += "<p><b>ID:</b>"+id+"<br>";
                html += "<b>Nombre:</b>"+name+"<br>";
                html += "<b>Fecha fin:</b>"+expirationDate+"</p>";
                bootbox.confirm({
                    title: "Renovar Publicacion",
                    message: html,
                    buttons: {
                        cancel: {
                            label: '<i class="fa fa-times"></i> Cancelar'
                        },
                        confirm: {
                            label: '<i class="fa fa-check"></i> Aceptar'
                        }
                    },
                    callback: function (result)
                    {
                        if(result)
                        {
                            renewPublication(button);
                        }
                    }
                });

            });
        },
        "buttons": [
            $.extend( true, {}, buttonCommon,{
                extend: 'copy'
            } ),
            $.extend( true, {}, buttonCommon,{
                extend: 'csv'
            } ),
            $.extend( true, {}, buttonCommon,{
                extend: 'pdf'
            } ),
            $.extend( true, {}, buttonCommon,{
                extend: 'excel'
            } ),
            $.extend( true, {}, buttonCommon,{
                extend: 'print',
                customize: function ( win ){
                    $(win.document).find("link[href*='style.css']").remove();
                }
            } )
        ]
    });
    $('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Buscar');
    $('.dataTables_length select').addClass('form-control');
    oTable.fnSetFilteringDelay(1000);



    /*###$################################################################## BEGIN - SEE RECORD*/
    $('#data-table').on("click","a.see-record", function (e) {
        e.preventDefault();
        var recordId = $(this).parent().find(".see-record").data("record-id");
        getRecordDetail(recordId);

    });
    $(document).on("click","a.delete-record", function (e) {
        e.preventDefault();
        let recordId = $(this).data("publication-id");
        $.ajax({
            method: "GET",
            url : base_url + 'AjaxPublication/delete/'+recordId,
            dataType: "json",
            // beforeSend:function()
            // {
            //     $button.closest("tbody").find(".renew-publication").addClass("hide");
            //     // $button.closest("tbody").find(".renew-publication-pulse").removeClass("hide");
            // },
            success:function(response)
            {
                if(response.success == 1)
                {
                    swal.fire({ title:'', html:response.message, type:"success"});
                    $('#data-table').DataTable().ajax.reload( null, false );
                }
                else
                {
                    swal.fire({ title:'', html:response.message, type:"error"});
                }
            }
        });

    });
    /*######################################################################## END - SEE PAYMENT*/
    //select2 ajax for users
    $('#ajax-get-users').select2({
        placeholder : "Seleccione un usuario",
        allowClear : true,
        ajax : {
            type : "post",
            dataType : "json",
            url : base_url + 'admin/AjaxUser/getAllUsers',
            quietMillis : 600,
            minimumInputLength : 4,
            data : function(term, page) {
                return {
                    term : term, //search term
                    limit : 5 // page size
                };
            },
            results : function(data) {
                return {
                    results : data
                };
            }
        },
        width : "100%",
        initSelection : function(element, callback) {
            var jsonResp = JSON.parse(element.val());
            callback(jsonResp);
            element.val(jsonResp.id);
        }
    });
});

function updatePublicationStatus(checkbox)
{
    var isPublished = "Si";
    var $checkbox = checkbox;
    var propertyId = $checkbox.data('property-id');
    var isChecked = $checkbox.is(":checked");

    if(isChecked === false)
    {
        isPublished = "No";
        $('.poll-modal')
            .data('publication-id',$checkbox.data('publication-id'))
            .modal({
                backdrop: 'static',
                keyboard: false
            });
        $("form[name=poll-form]").find("input[name=publication-id]").val($checkbox.data('publication-id'));
    }
    $.ajax({
        method: "POST",
        url : base_url + 'admin/AjaxProperty/updatePublication',
        dataType: "json",
        data: {isPublished:isPublished,propertyId:propertyId},
        beforeSend:function()
        {
            $checkbox.closest("tbody").find("input[type=checkbox]").addClass("hide");
            $checkbox.prop("disabled",true);
            $checkbox.closest("tbody").find(".deactivate-publication-pulse").removeClass("hide");
        },
        success:function()
        {
            // setTimeout(function(){
                $checkbox.closest("tbody").find("input[type=checkbox]").removeClass("hide");
                $checkbox.prop("disabled",false);
                $checkbox.closest("tbody").find(".deactivate-publication-pulse").addClass("hide");
                $('#data-table').DataTable().ajax.reload( null, false );
            // }, 2000);

        }
    });
}

function renewPublication(button)
{
    var $button = button;
    var publicationId = $button.data("publication-id");
    $.ajax({
        method: "POST",
        url : base_url + 'admin/AjaxProperty/renewPublication',
        dataType: "json",
        data: {publicationId:publicationId},
        beforeSend:function()
        {
            $button.closest("tbody").find(".renew-publication").addClass("hide");
            // $button.closest("tbody").find(".renew-publication-pulse").removeClass("hide");
        },
        success:function()
        {
            // setTimeout(function(){
            // $button.closest("tbody").find(".renew-publication-pulse").addClass("hide");
            $('#data-table').DataTable().ajax.reload( null, false );
            // }, 2000);
        }
    });
}

function getRecordDetail(id)
{
    var recordId = id;

    $.ajax({
        url : base_url + 'admin/AjaxProperty/getDetail',
        dataType  :"json",
        type : "POST",
        data : {recordId : recordId},
        success:function(response){
            bootbox.hideAll();
            var htmlSource   = $("#ht-record-detail").html();
            var template = Handlebars.compile(htmlSource);
            var data = {detail: response};
            var html    = template(data);
            bootbox.alert({
                title:"Detalle de la publicacion",
                message:html
            });
        }
    });
}

function findAndReplace(string, target, replacement) {

    var i = 0, length = string.length;

    for (i; i < length; i++) {

        string = string.replace(target, replacement);

    }

    return string;

}
