/**
 * Created by Jair on 28/7/2017.
 */
$(document).ready(function() {
    var buttonCommon = {
        title: "Proyectos",
        exportOptions: {
            page: 'all',
            columns: [0, 1, 2, 3, 4]
        }
    };
    //Horizontal Icons dataTable
    var oTable = $('#data-table').dataTable({
        "processing" : true,
        "serverSide" : true,
        "order": [[ 3, "desc" ]],
        "ajax" : {
            url : base_url + 'admin/AjaxProject/ajaxDtAllProjects',
            type : 'POST'
        },
        "dom": "<'row'<'col-sm-6'Bl><'col-sm-6 text-right'f>>rt<'row'<'col-sm-6'i><'col-sm-6 text-right'p>>",
        "lengthMenu": [ [10, 25, 50, 100, 100000], [10, 25, 50,100, 100000] ],
        "columns" : [{
            "data" : "proy_id",
        }, {
            "data" : "proy_nombre",
        }, {
            "data" : "proy_estado",
        }, {
            "data" : "proy_creado",
            "render" : function(data, type, row, meta) {
                var result = "";
                if(row.proy_creado !== "" && row.proy_creado !== null)
                {
                    var dateObject = new Date(row.proy_creado);
                    var date = dateObject.getDate() < 10? "0"+dateObject.getDate():dateObject.getDate();
                    var month = (dateObject.getMonth()+1) < 10? "0"+(dateObject.getMonth()+1):(dateObject.getMonth()+1);
                    var year = dateObject.getFullYear();
                    result = date+"-"+month+"-"+year;
                }

                return result;
            }
        }, {
            "data" : "proy_visitas",
        }, {
            "defaultContent" : " ",
            "searchable" : false,
            "orderable" : false,
            "render" : function(data, type, row, meta) {
                var html = '<a class="btn btn-primary btn-xs see-project" href="#" data-project-id="'+row.proy_id+'" title="" data-original-title="VER"  data-toggle="tooltip" data-placement="top"><i class="fa fa-search"></i></a>';
                    html += '<a target="_blank" class="btn btn-primary btn-xs" href="'+base_url + 'admin/Project/edit/' +row.proy_id+'" title="" data-original-title="EDITAR"  data-toggle="tooltip" data-placement="top"><i class="fa fa-pencil"></i></a>';
                    html += '<a target="_blank" class="btn btn-primary btn-xs" href="'+base_url + 'admin/Project/publication/' +row.proy_id+'" title="" data-original-title="PUBLICACIONES"  data-toggle="tooltip" data-placement="top"><i class="fa fa-home"></i></a>';
                    html += '<a target="_blank" class="btn btn-primary btn-xs" href="'+base_url + 'admin/Project/images/' +row.proy_id+'" title="" data-original-title="FOTOS"  data-toggle="tooltip" data-placement="top"><i class="fa fa-file-image-o"></i></a>';
                    html += '<a target="_blank" class="btn btn-primary btn-xs" href="'+base_url + 'admin/Project/typology/' +row.proy_id+'" title="" data-original-title="TIPOLOGIA"  data-toggle="tooltip" data-placement="top"><i class="fa fa-cubes"></i></a>';
                    html += '<a target="_blank" class="btn btn-primary btn-xs" href="'+base_url + 'admin/Project/finishingDetail/' +row.proy_id+'" title="" data-original-title="ACABADO"  data-toggle="tooltip" data-placement="top"><i class="fa fa-paint-brush"></i></a>';
                    html += '<a target="_blank" class="btn btn-primary btn-xs" href="'+base_url + 'admin/Project/socialArea/' +row.proy_id+'" title="" data-original-title="AREA SOCIAL"  data-toggle="tooltip" data-placement="top"><i class="fa fa-users"></i></a>';
                    html += '<a target="_blank" class="btn btn-primary btn-xs" href="'+base_url + 'admin/Project/slideShow/' +row.proy_id+'" title="" data-original-title="SLIDE SHOW"  data-toggle="tooltip" data-placement="top"><i class="fa fa-caret-square-o-right"></i></a>';
                    html += '<a target="_blank" class="btn btn-primary btn-xs" href="'+base_url + 'admin/Project/settings/' +row.proy_id+'" title="" data-original-title="CONFIGURACION"  data-toggle="tooltip" data-placement="top"><i class="fa fa-wrench"></i></a>';
                    html += '<a target="_blank" class="btn btn-primary btn-xs" href="'+base_url + 'admin/Project/leads/' +row.proy_id+'" title="" data-original-title="LEADS"  data-toggle="tooltip" data-placement="top"><i class="fa fa-list-alt"></i></a>';

                return html;
            }
        }],
        "drawCallback" : function(object) {
            $('[data-toggle="tooltip"]').tooltip();
            //this.api().columns.adjust();
        },
        "buttons": [
            $.extend( true, {}, buttonCommon,{
                extend: 'copy'
            } ),
            $.extend( true, {}, buttonCommon,{
                extend: 'csv'
            } ),
            $.extend( true, {}, buttonCommon,{
                extend: 'pdf'
            } ),
            $.extend( true, {}, buttonCommon,{
                extend: 'excel'
            } ),
            $.extend( true, {}, buttonCommon,{
                extend: 'print',
                customize: function ( win ){
                    $(win.document).find("link[href*='style.css']").remove();
                }
            } )
        ]
    });
    $('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Search');
    $('.dataTables_length select').addClass('form-control');
    oTable.fnSetFilteringDelay(1000);



    /*###$################################################################## BEGIN - SEE PAYMENT*/
    $('#data-table').on("click","a.see-project", function (e) {
        e.preventDefault();
        var projectId = $(this).parent().find(".see-project").data("project-id");
        getProjectDetail(projectId);

    });
    /*######################################################################## END - SEE PAYMENT*/
});
function getProjectDetail(id)
{
    var projectId = id;

    $.ajax({
        url : base_url + 'admin/AjaxProject/getDetail',
        dataType  :"json",
        type : "POST",
        data : {projectId : projectId},
        success:function(response){
            bootbox.hideAll();
            var htmlSource   = $("#ht-project-detail").html();
            var template = Handlebars.compile(htmlSource);
            var data = {detail: response};
            var html    = template(data);
            bootbox.alert({
                title:"Detalle del proyecto",
                message:html
            });
        }
    });
}