/**
/**
 * Created by Jair on 8/9/2017.
 */
function MapHandler(mapSelector)
{

    var latitudeSelector = "input[name=latitude]";
    var longitudeSelector = "input[name=longitude]";
    var mapContentSelector = mapSelector === undefined?"#maps":mapSelector;
    var map = new GMaps({
        div: mapContentSelector,
        lat: -17.784146,
        lng: -63.181738,
        zoom:13,
        enableNewStyle: true
    });

    this.addInitialMarker = function()
    {
        var latitude = $(latitudeSelector).val();
        var longitude = $(longitudeSelector).val();
        if(latitude != "")
        {
            map.setCenter(latitude, longitude);
            map.addMarker({
                lat: latitude,
                lng: longitude
            });
        }
    };
    this.addMarkers = function(list)
    {
        var htmlSource   = $("#ht-publication-map-quick-view").html();
        var template = Handlebars.compile(htmlSource);
        var infoWindowList = [];
        // A new Info Window is created and set content
        var infoWindow = new google.maps.InfoWindow({
            // Assign a maximum value for the width of the infowindow allows
            // greater control over the various content elements
            maxWidth: 350
        });
        $.each(list, function (index, value) {
            if(value.inm_latitud !== "")
            {
                // marker options
                var marker = map.addMarker({
                    lat: value.inm_latitud,
                    lng: value.inm_longitud,
                    title:value.inm_nombre,
                    icon: base_url + 'assets/img/icons/icono_map1.png'
                });

                //Attach click event to the marker.
                (function (marker, value) {
                    //build url to property
                    if(value.Service == "Project")
                        value.seo = base_url + value.seo;
                    else
                        value.seo = location.protocol + '//' + location.host + location.pathname + "/" + value.seo;
                    //build url to image
                    if(value.Service == "Project")
                        value.imageUrl = base_url + "librerias/timthumb.php?src="+base_url+"admin/imagenes/proyecto/"+value.publicationImage+"&w=150&h=90";
                    else
                        value.imageUrl = base_url + "librerias/timthumb.php?src="+base_url+"admin/imagenes/inmueble/"+value.publicationImage+"&w=150&h=90";
                    //formating price
                    value.inm_precio = parseFloat(value.inm_precio).toLocaleString();
                    var data = {publication:value};
                    var html    = template(data);
                    google.maps.event.addListener(marker, "click", function (e) {
                        //Wrap the content inside an HTML DIV in order to set height and width of InfoWindow.
                        infoWindow.setContent(html);
                        infoWindow.open(map, marker);
                    });
                })(marker, value);

                // This event expects a click on a marker
                // When this event is fired the Info Window is opened.
                google.maps.event.addListener(marker, 'click', function() {
                    infoWindow.open(map,marker);
                });
                // Event that closes the Info Window with a click on the map
                google.maps.event.addListener(map, 'click', function() {
                    infoWindow.close();
                });

                // *
                // START INFOWINDOW CUSTOMIZE.
                // The google.maps.event.addListener() event expects
                // the creation of the infowindow HTML structure 'domready'
                // and before the opening of the infowindow, defined styles are applied.
                // *
                google.maps.event.addListener(infoWindow, 'domready', function() {

                    // Reference to the DIV that wraps the bottom of infowindow
                    var iwOuter = $('.gm-style-iw');

                    /* Since this div is in a position prior to .gm-div style-iw.
                     * We use jQuery and create a iwBackground variable,
                     * and took advantage of the existing reference .gm-style-iw for the previous div with .prev().
                    */
                    var iwBackground = iwOuter.prev();

                    // Removes background shadow DIV
                    iwBackground.children(':nth-child(2)').css({'display' : 'none'});

                    // Removes white background DIV
                    iwBackground.children(':nth-child(4)').css({'display' : 'none'});

                    // Moves the infowindow 115px to the right.
                    iwOuter.parent().parent().css({left: '115px'});

                    // Moves the shadow of the arrow 76px to the left margin.
                    iwBackground.children(':nth-child(1)').attr('style', function(i,s){ return s + 'left: 76px !important;'});

                    // Moves the arrow 76px to the left margin.
                    iwBackground.children(':nth-child(3)').attr('style', function(i,s){ return s + 'left: 76px !important;'});

                    // Changes the desired tail shadow color.
                    iwBackground.children(':nth-child(3)').find('div').children().css({'box-shadow': 'rgba(72, 181, 233, 0.6) 0px 1px 6px', 'z-index' : '1'});

                    // Reference to the div that groups the close button elements.
                    var iwCloseBtn = iwOuter.next();

                    // Apply the desired effect to the close button
                    iwCloseBtn.css({width:"27px",height:"27px",opacity: '1', right: '38px', top: '3px', border: '7px solid #48b5e9', 'border-radius': '13px', 'box-shadow': '0 0 5px #3990B9'});

                    // If the content of infowindow not exceed the set maximum height, then the gradient is removed.
                    if($('.iw-content').height() < 140){
                        $('.iw-bottom-gradient').css({display: 'none'});
                    }

                    // The API automatically applies 0.7 opacity to the button after the mouseout event. This function reverses this event to the desired value.
                    iwCloseBtn.mouseout(function(){
                        $(this).css({opacity: '1'});
                    });
                });
            }
        });
    };
    var addMarkerBySearchResult = function(data)
    {
        var addressData = data;
        GMaps.geocode({
            address: addressData,
            callback: function(results, status) {
                if (status == 'OK')
                {
                    $(".map-search-message").text("");
                    var Coordinates = results[0].geometry.location;
                    var latitude = Coordinates.lat();
                    var longitude = Coordinates.lng();
                    var index = map.markers.length;
                    map.setCenter(latitude, longitude);
                    if(index == 0)
                    {
                        map.addMarker({
                            lat: latitude,
                            lng: longitude
                            //title: 'Hello map'
                        });
                    }
                    else
                    {
                        map.markers[0].setPosition(new google.maps.LatLng(latitude, longitude));
                        $(latitudeSelector).val(latitude);
                        $(longitudeSelector).val(longitude);
                    }
                }
                else
                {
                    $(".map-search-message").text("No se encontro la ubicacion...");
                }
            }
        });
    };
    this.loadEventHandlers = function()
    {
        var _this = this;
        //Add marker manually
        GMaps.on('click', map.map, function(event) {
            var index = map.markers.length;
            var latitude = event.latLng.lat();
            var longitude = event.latLng.lng();
            if(index == 0)
            {
                map.addMarker({
                    lat: latitude,
                    lng: longitude
                    //title: 'Hello map'
                });
            }
            else
            {
                map.markers[0].setPosition(new google.maps.LatLng(latitude, longitude));
                $("input[name=latitude]").val(latitude);
                $("input[name=longitude]").val(longitude);
            }
        });
        //Add marker by search result
        $(".search-address-button").on("click",function(e){
            e.preventDefault();
            var addressData = $.trim($(".search-address-data").val());
            var addressState = $.trim($(".search-address-data-state").text())!=""?", "+$.trim($(".search-address-data-state").text()):"";
            var addressCity = $.trim($(".search-address-data-city").text())!=""?", "+$.trim($(".search-address-data-city").text()):"";
            addressData += addressCity + addressState +", Bolivia";
            if(addressData != "")
            {
                addMarkerBySearchResult(addressData);
            }
        });
        //When a marker is added performance this
        GMaps.on('marker_added', map, function(marker) {
            $("input[name=latitude]").val(marker.getPosition().lat());
            $("input[name=longitude]").val(marker.getPosition().lng());
        });
        $(".search-address-data").on("keypress",function(e){
            $(".map-search-message").text("");
            if (e.which == 13)
            {
                e.preventDefault();
                $(".map-search-message").text("Buscando...");
                var addressData = $.trim($(this).val());
                var addressState = $.trim($(".search-address-data-state").text())!=""?", "+$.trim($(".search-address-data-state").text()):"";
                var addressCity = $.trim($(".search-address-data-city").text())!=""?", "+$.trim($(".search-address-data-city").text()):"";
                addressData += addressCity + addressState +", Bolivia";
                if (addressData != "") {
                    addMarkerBySearchResult(addressData);
                }
                return false;
            }
        });
    };
}
