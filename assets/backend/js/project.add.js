/**
 * Created by Jair on 28/7/2017.
 */
$(document).ready(function() {

    /*############################################## BEGIN - GOOGLE MAPS */
    var map = new GMaps({
        div: '#maps',
        lat: -17.784146,
        lng: -63.181738,
        enableNewStyle: true
    });
    //Add marker manually
    GMaps.on('click', map.map, function(event) {
        var index = map.markers.length;
        var latitude = event.latLng.lat();
        var longitude = event.latLng.lng();
        if(index == 0)
        {
            map.addMarker({
                lat: latitude,
                lng: longitude,
                title: 'Hello map'
            });
        }
        else
        {
            map.markers[0].setPosition(new google.maps.LatLng(latitude, longitude));
            $("input[name=latitude]").val(latitude);
            $("input[name=longitude]").val(longitude);
        }
    });
    //Add marker by search result
    $(".search-address-button").on("click",function(e){
        e.preventDefault();
        var addressData = $.trim($(".search-address-data").val());
        var addressState = $.trim($(".search-address-data-state").text())!=""?", "+$.trim($(".search-address-data-state").text()):"";
        var addressCity = $.trim($(".search-address-data-city").text())!=""?", "+$.trim($(".search-address-data-city").text()):"";
        addressData += addressCity + addressState +", Bolivia";
        if(addressData != "")
        {
            addMarkerBySearchResult(addressData);
        }
    });
    //When a marker is added performance this
    GMaps.on('marker_added', map, function(marker) {
        $("input[name=latitude]").val(marker.getPosition().lat());
        $("input[name=longitude]").val(marker.getPosition().lng());
    });
    $(".search-address-data").on("keypress",function(e){
        $(".map-search-message").text("");
        if (e.which == 13)
        {
            e.preventDefault();
            $(".map-search-message").text("Buscando...");
            var addressData = $.trim($(this).val());
            var addressState = $.trim($(".search-address-data-state").text())!=""?", "+$.trim($(".search-address-data-state").text()):"";
            var addressCity = $.trim($(".search-address-data-city").text())!=""?", "+$.trim($(".search-address-data-city").text()):"";
            addressData += addressCity + addressState +", Bolivia";
            if (addressData != "") {
                addMarkerBySearchResult(addressData);
            }
            return false;
        }
    });
    function addMarkerBySearchResult(data)
    {
        var addressData = data;
        GMaps.geocode({
            address: addressData,
            callback: function(results, status) {
                if (status == 'OK')
                {
                    $(".map-search-message").text("");
                    var Coordinates = results[0].geometry.location;
                    var latitude = Coordinates.lat();
                    var longitude = Coordinates.lng();
                    var index = map.markers.length;
                    map.setCenter(latitude, longitude);
                    if(index == 0)
                    {
                        map.addMarker({
                            lat: latitude,
                            lng: longitude,
                            title: 'Hello map'
                        });
                    }
                    else
                    {
                        map.markers[0].setPosition(new google.maps.LatLng(latitude, longitude));
                        $("input[name=latitude]").val(latitude);
                        $("input[name=longitude]").val(longitude);
                    }
                }
                else
                {
                    $(".map-search-message").text("No se encontro la ubicacion...");
                }
            }
        });
    }

    /*############################################## END - GOOGLE MAPS */
    //begin - Snippet to upload image
    $(document).on("click","div.avatar-upload img",function()
    {
        $("#avatar-file").trigger("click");
    });

    $("#avatar-file").replaceWith("<input id=\"avatar-file\" type=\"file\" name=\"logo\" accept=\"image/*\">");
    $("#avatar-file").change(function() {
        readImage(this);
    });
    $("#avatar-file").hide();
    $("div.avatar-upload img").css( 'cursor', 'pointer' );
    function readImage(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#avatar-preview').attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    /*********************************/
    $(document).on("click","div.avatar-upload-2 img",function()
    {
        $("#avatar-file-2").trigger("click");
    });

    $("#avatar-file-2").replaceWith("<input id=\"avatar-file-2\" type=\"file\" name=\"logo-2\" accept=\"image/*\">");
    $("#avatar-file-2").change(function() {
        readImage2(this);
    });
    $("#avatar-file-2").hide();
    $("div.avatar-upload-2 img").css( 'cursor', 'pointer' );
    function readImage2(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#avatar-preview-2').attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    //end - Snippet to upload image
    $('input.date-picker').datepicker({});
    //select2 ajax for users
    $('#ajax-get-users').select2({
        placeholder : "Seleccione un usuario",
        allowClear : true,
        ajax : {
            type : "post",
            dataType : "json",
            url : base_url + 'admin/AjaxUser/getAllUsers',
            quietMillis : 600,
            minimumInputLength : 4,
            data : function(term, page) {
                return {
                    term : term, //search term
                    limit : 5 // page size
                };
            },
            results : function(data) {
                return {
                    results : data
                };
            }
        },
        width : "100%",
        initSelection : function(element, callback) {
            var jsonResp = JSON.parse(element.val());
            callback(jsonResp);
            element.val(jsonResp.id);
        }
    });
    //select2 ajax for cities
    $('#ajax-get-cities').select2({
        placeholder : "Seleccione una ciudad",
        allowClear : true,
        ajax : {
            type : "post",
            dataType : "json",
            url : base_url + 'admin/AjaxCity/getAllCitiesByStateId',
            quietMillis : 600,
            minimumInputLength : 4,
            data : function(term, page) {
                var stateId = $("select[name=state]").val();
                return {
                    stateId : stateId, // search by state
                    term : term, //search term
                    limit : 5 // page size
                };
            },
            results : function(data) {
                return {
                    results : data
                };
            }
        },
        width : "100%",
        initSelection : function(element, callback) {
            var jsonResp = JSON.parse(element.val());
            callback(jsonResp);
            element.val(jsonResp.id);
        }
    });
    $('#ajax-get-zones').select2({
        placeholder : "Seleccione una Zona",
        allowClear : true,
        ajax : {
            type : "post",
            dataType : "json",
            url : base_url + 'admin/AjaxZone/getAllZonesByStateId',
            quietMillis : 600,
            minimumInputLength : 4,
            data : function(term, page) {
                var stateId = $("select[name=state]").val();
                return {
                    stateId : stateId, // search by state
                    term : term, //search term
                    limit : 5 // page size
                };
            },
            results : function(data) {
                return {
                    results : data
                };
            }
        },
        width : "100%",
        initSelection : function(element, callback) {
            var jsonResp = JSON.parse(element.val());
            callback(jsonResp);
            element.val(jsonResp.id);
        }
    });
    $("select[name=state]").change(function() {
        $("#ajax-get-cities").select2("val", "");
        $("#ajax-get-zones").select2("val", "");
        $(".search-address-data-city").addClass("hide");
    });

    $("select[name=state]").on("change",function(){
        addStateOnSearch($(this));
    });

    function addStateOnSearch(select2)
    {
        var state = "";
        if(select2.select2("data") !=null)
        {
            state = select2.select2("data").text;
            $(".search-address-data-state").removeClass("hide");
            $(".search-address-data-state").text(state);
        }
        else
        {
            $(".search-address-data-state").addClass("hide");
        }
    }
    $("#ajax-get-cities").on("change",function(){
        var city = "";
        if($(this).select2("data") !=null)
        {
            city = $(this).select2("data").text;
            $(".search-address-data-city").removeClass("hide");
            $(".search-address-data-city").text(city);
        }
        else
        {
            $(".search-address-data-city").addClass("hide");
        }
    });
});
