/**
 * Created by Jair on 23/11/2017.
 */
$(document).ready(function($) {
    /** google maps **/
    if($("#maps").length > 0)
    {
        var mapHandler = new MapHandler();
        mapHandler.addInitialMarker();
    }
});
$("#form-contact-agent input[type=submit]").click(function(e){
    e.preventDefault();

    if(!$("#form-contact-agent").parsley().isValid())
    {
        $("#form-contact-agent").parsley().validate();

    }
    else {
        grecaptcha.execute();
    }
});
function sendRequestContactAgentForm(token)
{
    $("#form-contact-agent").submit();
}