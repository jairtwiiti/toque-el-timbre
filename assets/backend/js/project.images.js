/**
 * Created by Jair on 4/7/2017.
 */
$(document).ready(function() {
    $( ".sortable" ).sortable({
        update : function(event, ui) {
            var newOrder = $(".sortable").sortable('toArray');
            $.ajax({
                url : base_url + 'admin/AjaxProject/updateImageOrder',
                type : "POST",
                data : {
                    "ids" : newOrder
                }
            });
        },
    });
    $('[data-toggle="tooltip"]').tooltip();
    getImages();
    /**  begin - dropzone **/
    Dropzone.options.myDropzone = {
        addRemoveLinks: true,
        paramName: "file", // The name that will be used to transfer the file
        maxFilesize: 30, // MB
        acceptedFiles: "image/*",
        /*accept: function(file, done) {
            if (file.name == "justinbieber.jpg") {
                done("Naha, you don't.");
            }
            else { done(); }
        },*/
        success: function(file, responseText) {
        // Handle the responseText here. For example, add the text to the preview element:
        //file.previewTemplate = Dropzone.createElement(this.options.previewTemplate);
        //file.previewTemplate.appendChild(document.createTextNode(responseText));
            //console.log(responseText);
            var item = jQuery.parseJSON(responseText);
            addItem(item);
            this.removeFile(file);
        },
        addedfile: function(file) {
           // console.log(file);
            file.previewElement = Dropzone.createElement(this.options.previewTemplate);
            // Create the remove button
            var removeButton = Dropzone.createElement('<a class="btn btn-danger btn-xs" href="#" data-payment-id="1" title="" data-original-title="ELIMINAR" data-toggle="tooltip" data-placement="top"><i class="fa fa-times"></i></a>');
            var _this = this;
            removeButton.addEventListener("click", function (e) {
                // Make sure the button click doesn't submit the form:
                e.preventDefault();
                e.stopPropagation();
                // Remove the file preview.
                _this.removeFile(file);
                // If you want to the delete the file on the server as well,
                // you can do the AJAX request here.
            });
            // Now attach this new element some where in your page
            $("form[name=upload-project-images]").append(file.previewElement);
            // Add the button to the file preview element.
            file.previewElement.appendChild(removeButton);
        }
    };
    /** end -dropzone **/


});
function getImages()
{
    var projectId = $("input[name=project-id]").val();
    $.ajax({
        url : base_url + 'admin/AjaxProject/getImages',
        dataType:"json",
        type : "POST",
        data : {projectId : projectId},
        success:function(response){
            var list = response;
            $(".list-group").html("");
            $.each(list,function(index,value){
                addItem(value);
            })

            bootbox.hideAll();
        }
    });
}
function addItem(item, replace)
{
    var htmlSource   = $("#ht-image-item").html();
    var template = Handlebars.compile(htmlSource);
    var data = {item: item};
    var html    = template(data);
    if(replace)
    {
        $(".list-group").find("div[data-project-image-id="+item.proy_fot_id+"]").closest("div.image-item").replaceWith(html);
    }
    else
    {
        $(".list-group").append(html);
    }
    loadEventListener(item.proy_fot_id);
    startImageThumbnailComponent(item.proy_fot_id);
}
function loadEventListener(projectImageId)
{
    /*** begin - action buttons */

    $(".project-image-see-"+projectImageId).bind("click",function(e){
        e.preventDefault();
        var projectImageId = $(this).closest("div").data("project-image-id");
        seeResource(projectImageId);
    });
    $(".project-image-edit-"+projectImageId).bind("click",function(e){
        e.preventDefault();
        var projectImageId = $(this).closest("div").data("project-image-id");
        $.ajax({
            url : base_url + 'admin/AjaxProject/getImage',
            dataType  :"json",
            type : "POST",
            data : {projectImageId : projectImageId},
            success:function(response){
                bootbox.hideAll();
                var htmlSource   = $("#ht-image-edit").html();
                var template = Handlebars.compile(htmlSource);
                var data = {detail: response};
                var html    = template(data);
                bootbox.confirm({
                    title:"Editar informacion",
                    message:html,
                    buttons: {
                        cancel: {
                            label: 'Cancelar'
                        },
                        confirm: {
                            label: 'Guardar'
                        }
                    },
                    callback: function (result) {
                        if(result)
                        {
                            var $form = $("form[name=image-edit-"+projectImageId+"]");


                            updateImageInformation($form);
                        }
                    }
                });
                startImageThumbnailComponent(projectImageId);
                initNormalTynimce(".tinymce");
            }
        });
    });
    $(".project-image-delete-"+projectImageId).on("click",function(e){
        e.preventDefault();
        var itemId = $(this).closest("div").data("project-image-id");
        bootbox.confirm({
            title:"Borrar imagen",
            message:"Borrar esta imagen permanentemente?",
            buttons: {
                cancel: {
                    label: 'Cancelar'
                },
                confirm: {
                    label: 'Aceptar'
                }
            },
            callback: function (result) {
                if(result)
                {
                    deleteResource(itemId);
                }
            }
        });
    });
    /*** end - action buttons */
}
function startImageThumbnailComponent(projectImageId)
{
    /** begin - Snippet to upload image **/
    $("div.avatar-upload-"+projectImageId+" img").bind("click",function()
    {
        $("#avatar-file-"+projectImageId).trigger("click");
    });

    $("#avatar-file-"+projectImageId).replaceWith("<input id=\"avatar-file-"+projectImageId+"\" type=\"file\" name=\"logo\" accept=\"image/*\" class=\"hide\">");
    $("#avatar-file-"+projectImageId).change(function() {
        readImage(this);
    });
    $("div.avatar-upload-"+projectImageId+" img").css( 'cursor', 'pointer' );
    function readImage(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#avatar-preview-'+projectImageId).attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    /** end - Snippet to upload image **/
}
function updateImageInformation(form)
{
    var data = new FormData();
    var projectImageId = form.find("input[name=project-image-id]").val();
    data.append("projectImageId", projectImageId);
    data.append("coverImage",form.find("input[name=cover-image]:checked").val());
    data.append("adImage",form.find("input[name=ad-image]:checked").val());
    data.append("description",tinyMCE.activeEditor.getContent());
    data.append("file",form.find("input[type=file]")[0].files[0]);
    $.ajax({
        url : base_url + 'admin/AjaxProject/updateImageInformation',
        dataType  :"json",
        type : "POST",
        contentType: false,
        processData: false,
        data : data,
        success:function(response){
            bootbox.hideAll();
            addItem(response,true);
        }
    });
}

function deleteResource(id)
{
    var itemId = id;
    var itemModel = "model_projectimage";
    $.ajax({
        url : base_url + 'admin/AjaxProject/deleteResource',
        dataType  :"json",
        type : "POST",
        data : {itemId:itemId, itemModel:itemModel},
        success:function(){
            bootbox.hideAll();
            $("div[data-project-image-id="+itemId+"]").closest("div.image-item").remove();
        }
    });
}

function seeResource(id)
{
    var src = $("div[data-project-image-id="+id+"]").closest("div.image-item").find("img").prop("src");
    src = src.replace("h=100", "");
    src = src.replace("w=150", "w=850");
    bootbox.alert({
        title:"Imagen",
        message: "<p class='text-center'><img src='"+src+"' class='img-responsive'></p>",
        size: 'large'
    });
}