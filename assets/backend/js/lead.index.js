/**
 * Created by Jair on 28/03/2018.
 */
$.fn.dataTable.Buttons.swfPath = base_url + 'assets/backend/global/plugins/datatables/plugins/Buttons142/swf/flashExport.swf';
$(document).ready(function() {
    $('input.date-picker').datepicker({});

    var additionalParameter = new DTAdditionalParameterHandler("#extra-request-data","#data-table-messages");
    additionalParameter.addParameterObject('facebook-form-id','text');
    additionalParameter.addParameterObject('start-date','text');
    additionalParameter.addParameterObject('end-date','text');
    additionalParameter.setButtonFilter('#send-filters');
    additionalParameter.setButtonRest('#remove-additional-parameters');
    additionalParameter.loadEventHandlers();

    var buttonCommon = {
        title: "Leads",
        exportOptions: {
            page: 'all',
            columns: [1, 2, 3, 6, 7, 8]
        }
    };
    //Horizontal Icons dataTable
    var oTable = $('#data-table-messages').dataTable({
        "processing" : true,
        "serverSide" : true,
        "order": [[ 8, "desc" ]],
        "ajax" : {
            url : base_url + 'admin/AjaxLead/ajaxDtAllLeads',
            type : 'POST',
            data:function ( data ) {
                data.additionalParameters = additionalParameter.getList();
            }
        },
        "dom": "<'row'<'col-sm-6'Bl><'col-sm-6 text-right'f>>rt<'row'<'col-sm-6'i><'col-sm-6 text-right'p>>",
        "lengthMenu": [ [10, 25, 50, 100, 100000], [10, 25, 50,100, 100000] ],
        "columns" : [{
            "data" : "fle_id"
        }, {
            "data" : "fle_consulta"
        }, {
            "data" : "fle_email"
        }, {
            "data" : "fle_full_name",
            "render" : function(data, type, row, meta) {
                var fullName = row.fle_full_name;
                if(fullName == "")
                {
                    fullName = row.fle_first_name+" "+row.fle_last_name;
                }
                return fullName;
            }
        }, {
            "data" : "fle_first_name"
        }, {
            "data" : "fle_last_name"
        }, {
            "data" : "fle_phone_number",
            "render" : function(data, type, row, meta) {
               return row.fle_phone_number.replace("+591","");
            }
        }, {
            "data" : "fle_city"
        }, {
            "data" : "fle_createdon",
            "render" : function(data, type, row, meta) {
                var result = "";
                if(row.fle_createdon !== "" && row.fle_createdon !== null)
                {
                    var dateObject = new Date(row.fle_createdon);
                    var date = dateObject.getDate() < 10? "0"+dateObject.getDate():dateObject.getDate();
                    var month = (dateObject.getMonth()+1) < 10? "0"+(dateObject.getMonth()+1):(dateObject.getMonth()+1);
                    var year = dateObject.getFullYear();
                    result = date+"-"+month+"-"+year;
                }

                return result;
            }
        }, {
            "data" : "proy_nombre",
            "render" : function(data, type, row, meta) {
                var html = "";
                if(row.proy_nombre !== null)
                {
                    var url = base_url + row.proy_seo;
                    html = "<a class='property-name-as-link' href='"+url.toLocaleLowerCase()+"' target='_blank'>"+row.proy_nombre+"</a>";
                }
                return html;
            }
        }, {
            "data" : "inm_nombre",
            "render" : function(data, type, row, meta) {
                var html = "";
                if(row.inm_nombre !== null)
                {
                    var url = base_url +"buscar/inmuebles-en-venta-en-bolivia/"+ row.inm_seo;
                    html = "<a class='property-name-as-link' href='"+url.toLocaleLowerCase()+"' target='_blank'>"+row.inm_nombre+"</a>";
                }

                return html;
            }
        }],
        "drawCallback" : function(object) {
            $('[data-toggle="tooltip"]').tooltip();
            this.api().column(0).visible(false);
            this.api().column(4).visible(false);
            this.api().column(5).visible(false);
            this.api().column(7).visible(false);
        },
        "buttons": [
            $.extend( true, {}, buttonCommon,{
                extend: 'copy'
            } ),
            $.extend( true, {}, buttonCommon,{
                extend: 'csv'
            } ),
            $.extend( true, {}, buttonCommon,{
                extend: 'pdf',
            } ),
            $.extend( true, {}, buttonCommon,{
                extend: 'excel'
            } ),
            $.extend( true, {}, buttonCommon,{
                extend: 'print',
                customize: function ( win ){
                    $(win.document).find("link[href*='style.css']").remove();
                }
            } )
        ]
    });
    $('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Search');
    $('.dataTables_length select').addClass('form-control');
    oTable.fnSetFilteringDelay(1000);

    //select2 ajax for facebook forms
    $('#ajax-get-facebook-forms').select2({
        placeholder : "Seleccione uno o mas formularios",
        allowClear : true,
        tags:true,
        ajax : {
            type : "post",
            dataType : "json",
            url : base_url + 'admin/AjaxFacebookForm/getAllForms',
            quietMillis : 600,
            minimumInputLength : 4,
            data : function(term, page) {
                return {
                    term : term, //search term
                    limit : 5 // page size
                };
            },
            results : function(data) {
                return {
                    results : data
                };
            }
        },
        width : "100%",
        initSelection : function(element, callback) {
            var jsonResp = JSON.parse(element.val());
            callback(jsonResp);
            element.val(jsonResp.id);
        }
    });

    /*###$################################################################## BEGIN - SEE PAYMENT*/
    $('#data-table').on("click","a.see-project", function (e) {
        e.preventDefault();
        var projectId = $(this).parent().find(".see-project").data("project-id");
        getProjectDetail(projectId);

    });
    /*######################################################################## END - SEE PAYMENT*/
});
function getProjectDetail(id)
{
    var projectId = id;

    $.ajax({
        url : base_url + 'admin/AjaxProject/getDetail',
        dataType  :"json",
        type : "POST",
        data : {projectId : projectId},
        success:function(response){
            bootbox.hideAll();
            var htmlSource   = $("#ht-project-detail").html();
            var template = Handlebars.compile(htmlSource);
            var data = {detail: response};
            var html    = template(data);
            bootbox.alert({
                title:"Detalle del proyecto",
                message:html
            });
        }
    });
}
function sanitizeString(str)
{
    if(typeof str == "string")
    {
        str = str.replace(/[^a-z0-9áéíóúñü \.,_-]/gim,"");
        str = str.replace(/ /g,"-");
        return str.trim();
    }

}