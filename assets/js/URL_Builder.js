/**
 * Created by Jair on 15/5/2017.
 */

function FormRequestHandler()
{
    var id = Date.now();
    var primaryFilter = new PrimaryFilterBuilder();
    var secondaryFilter = new SecondaryFilterBuilder();
    var responseContent = "#publications-content";
    var paginationContent = '.publication-pagination';
    var overlay = "#overlay-publications";
    var resultData = [];
    var pageSize = 15;
    var onlyProjects = 0;
    var storeSearchOnHistory = 1;

    var mapMode = 0;

    var redirectTo = "";
    //getters
    this.getId = function()
    {
        return id;
    };

    this.setResponseContent = function(content)
    {
        responseContent = content;
    };

    this.setPaginationContent = function (content)
    {
        paginationContent = content;
    };

    this.setOnlyProjects = function(showOnlyProjects)
    {
        onlyProjects = showOnlyProjects;
    };

    this.setStoreSearchOnHistory = function(store)
    {
        storeSearchOnHistory = store;
    };

    this.setOverlay = function(selector)
    {
        overlay = selector;
    };

    this.setPageSize = function(size)
    {
        pageSize = size;
    };
    this.setResultData = function(data)
    {
        resultData = data;
    };
    this.setMapMode = function(isMapMode)
    {
        mapMode = isMapMode;
    };

    this.eventHandler = function()
    {
        var _this = this;
        $("#button_search_home, #advanced-search-form").on("click",function(){
            _this.sendRequest();
        });
        $("#sort-type").on("change",function(){
            _this.sendRequest();
        });
        $("a[data-layout]").on("click",function(e){
            e.preventDefault;
            $(this).data("layout", $(this).data("display"));
            _this.sendRequest();
        });

    };
    this.parseToHttpQuery = function()
    {
        var filter1 = primaryFilter.getPrimaryFilter();
        var auxSecondaryFilter = secondaryFilter.getSecondaryFilter();
        var filter2 = auxSecondaryFilter!=""?"?"+auxSecondaryFilter:"";
        redirectTo = filter1+filter2;
    };

    this.buildMap = function(list)
    {
        //NOTE: before to call this method make sure the maps library are loaded previously
        /** begin - google maps **/

        if($("#maps").length > 0)
        {
            var mapHandler = new MapHandler();
            mapHandler.addMarkers(list);
        }

        /** end - google maps **/
    };
    this.sendRequest = function(httpQuery)
    {
        var url = "";

        var zoneStringArray = [];
        if(httpQuery !== undefined)
        {
            url = httpQuery.toLocaleLowerCase();
            var position = url.search("buscar") + 7;
            url = url.substr(position,url.length);
        }
        else
        {
            this.parseToHttpQuery();
            url = redirectTo;
            if($("#ajax-get-zones").length > 0)
            {
                zoneStringArray = $("#ajax-get-zones").select2("data");
            }
        }

        if($(responseContent).length <= 0)
        {
            window.location = base_url + "buscar/" + url;
        }
        else if(storeSearchOnHistory === 1)
        {
            //visualizer initalized on search.index.js
            if(visualizer == 0)
            {
                history.pushState("home", "home", base_url);
            }
            history.pushState("search", "search", base_url + "buscar/" + url);
        }
        visualizer++;
        var _this = this;

        $(paginationContent).pagination({
            dataSource: base_url + "AjaxPublication/getAll",
            locator: 'result',
            totalNumberLocator: function(response) {
                // you can return totalNumber by analyzing response content
                return response.totalResult;
            },
            pageSize: pageSize,
            ajax: {
                type:"POST",
                data:{urlRequested:url,showOnlyProjects:onlyProjects, zoneStringArray:zoneStringArray},
                dataType: "json",
                beforeSend: function() {
                    $(overlay).show();
                }
            },
            callback: function(data, pagination) {

                if(mapMode === 1)
                {
                    _this.buildMap(data);
                }
                else
                {
                    // template method of yourself
                    $(responseContent).html('');
                    var htmlSource   = $("#ht-publication-quick-view").html();
                    var template = Handlebars.compile(htmlSource);
                    var searchResult = "";
                    var thumbnailSettings = {
                        colDefinition:"col-xs-12 col-sm-6 col-md-6 col-lg-4",
                        hidePublicationName: ""
                    };
                    var html = "";
                    $.each(data,function(index, value){
                        var data = {publication:value,thumbnailSettings:thumbnailSettings};
                        html = template(data);
                        searchResult += html;
                    });
                    $(responseContent).html(searchResult);
                    cutText();
                    //scroll page to top
                    $('.content_info.content_info_search.mobile-full-width').goTo()
                }
                $(overlay).hide();
            }
        });
    };

    this.imageExist = function (url)
    {
        var xhr = new XMLHttpRequest();
        xhr.open('HEAD', url, false);
        xhr.send();

        if (xhr.status == "404") {
            return false;
        } else {
            return true;
        }
    }
}

function PrimaryFilterBuilder()
{
    var id = Date.now();
    var primaryFilter = "";
    this.getId = function()
    {
        return id;
    };
    var buildFilter = function()
    {
        var arrayObjectVariables = [];
        primaryFilter = "";
        arrayObjectVariables.push(new VariableHandler("type","inmueble"));
        arrayObjectVariables.push(new VariableHandler("in","transaccion"));
        arrayObjectVariables.push(new VariableHandler("city","ciudad"));

        $.each(arrayObjectVariables,function(index,variable)
        {
            var separator = "-en-";
            var value = "";
            if(typeof $("input[type=radio][name="+variable.getKeyword()+"]:checked").val() == "undefined")
            {
                value = typeof $("select[name="+variable.getKeyword()+"] option:selected").val() == "undefined"?"":$("select[name="+variable.getKeyword()+"] option:selected").val();
            }
            else
            {
                value = typeof $("input[type=radio][name="+variable.getKeyword()+"]:checked").val() == "undefined"?"":$("input[type=radio][name="+variable.getKeyword()+"]:checked").val();
            }
            variable.setValue(value);
            if(index == (arrayObjectVariables.length-1))
            {
                separator = "";
            }
            primaryFilter+= variable.getAsPrimaryFilter()+separator;
        });
    };

    this.getPrimaryFilter = function()
    {
        buildFilter();
        return primaryFilter;
    };
}

function SecondaryFilterBuilder()
{
    var id = Date.now();
    var secondaryFilter = "";

    this.getId = function()
    {
        return id;
    };
    var buildFilter = function()
    {
        var arrayObjectVariables = [];
        secondaryFilter = "";
        arrayObjectVariables.push(new VariableHandler("room","habitacion"));
        arrayObjectVariables.push(new VariableHandler("bathroom","bano"));
        arrayObjectVariables.push(new VariableHandler("parking","parqueo"));
        arrayObjectVariables.push(new VariableHandler("currency","moneda"));
        arrayObjectVariables.push(new VariableHandler("price_min","desde"));
        arrayObjectVariables.push(new VariableHandler("price_max","hasta"));
        arrayObjectVariables.push(new VariableHandler("sort","ordenar"));
        arrayObjectVariables.push(new VariableHandler("layout","tipo"));
        arrayObjectVariables.push(new VariableHandler("zone","zona"));
        arrayObjectVariables.push(new VariableHandler("status","estado"));
        arrayObjectVariables.push(new VariableHandler("published-by","publicado-por"));
        arrayObjectVariables.push(new VariableHandler("condominium","condominio"));
        arrayObjectVariables.push(new VariableHandler("social-housing","vivienda-social"));
        arrayObjectVariables.push(new VariableHandler("amenities","amenidades"));

        $.each(arrayObjectVariables,function(index,filter)
        {
            var separator = "&";
            var value = "";
            switch(filter.getKeyword())
            {
                case "price_min":
                case "price_max":
                    value = typeof $("#"+filter.getKeyword()).val() == "undefined"?"":$("#"+filter.getKeyword()).val();
                    value = value.replace(/,/g,"");
                    break;
                case "layout":
                    value = typeof $("a[data-layout]").data("layout") == "undefined"?"":$("a[data-layout]").data("layout");
                    break;
                case "zone":
                    value = typeof $("input[name="+filter.getKeyword()+"]").val() == "" || typeof $("input[name="+filter.getKeyword()+"]").val() == "undefined"?"":$("input[name="+filter.getKeyword()+"]").val();
                    break;
                case "sort":
                    value = typeof $("select[name="+filter.getKeyword()+"] option:selected").val() == "undefined"?"":$("select[name="+filter.getKeyword()+"] option:selected").val();
                    break;
                case "amenities":
                    $('input[name="'+filter.getKeyword()+'[]"]:checked').each(function() {
                        value += this.value;
                    });
                    break;
                default:
                    value = typeof $("input[type=radio][name="+filter.getKeyword()+"]:checked").val() == "undefined"?"":$("input[type=radio][name="+filter.getKeyword()+"]:checked").val();
            }
            filter.setValue(value);
            if(filter.getAsSecondaryFilter() != "")
            {
                secondaryFilter+= separator+filter.getAsSecondaryFilter();
            }
        });
        secondaryFilter = secondaryFilter.substr(1);
    };

    this.getSecondaryFilter = function()
    {
        buildFilter();
        return secondaryFilter;
    };

}

function VariableHandler(keyword, label)
{
    var id = Date.now();
    var keyword = keyword;
    var label = label;
    var value = "";

    this.setValue = function(val)
    {
        value = val;
    };
    this.getKeyword = function()
    {
        return keyword;
    };
    var removeBlankSpaces = function()
    {
        value = value.replace(/ /g,"-");
    };

    this.getAsPrimaryFilter = function()
    {
        removeBlankSpaces();
        return value;
    };
    this.getAsSecondaryFilter = function()
    {
        var response = "";
        if(value!="")
        {
            response = label+"="+value;
        }
        return response;
    };
}
