(function ( $ ) {

    $( ".item_property" ).hover(
        function() {
            $(this).find("a h5").css("right", "-20em");
            $(this).find(".av-portfolio-overlay").css("bottom", "0em");
            $(this).find(".av-portfolio-info").css("top", "0em");
        }, function() {
            $(this).find("a h5").css("right", "0em");
            $(this).find(".av-portfolio-overlay").css("bottom", "-13em");
            $(this).find(".av-portfolio-info").css("top", "-6em");
        }
    );

}( jQuery ));


