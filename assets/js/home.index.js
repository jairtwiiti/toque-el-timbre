/**
 * Created by Jair on 20/12/2017.
 */
var formRequestHandler = new FormRequestHandler();
formRequestHandler.eventHandler();
getProjects();
getPaidPublications();
$(document).ready(function($) {
    startTestimonialCarousel();
    window.onpopstate = function(event)
    {
        if(event.state === "search")
        {
            window.location = window.location.href;
        }
    };
    if(window.innerWidth <= 800 && window.innerHeight <= 700) {
        $(".busqueda_usuario").colorbox({iframe:true, width:"100%", height:"100%", fixed:true});
    } else {
        $(".busqueda_usuario").colorbox({iframe:true, width:"40%", height:"70%"});
    }

    $(document).on("click",".publish-it-now-button",function(e){
        e.preventDefault();
        if($(".desea_publicar.my-account-button").is(":visible"))
        {
            window.open(base_url + "admin/Property/add",'_blank');
        }
        else
        {
            launchQuickLoginForm();
        }
    });

    //launch quick login form
    $(document).on("click",".login.my-account-button",function (e) {
        e.preventDefault();
        launchQuickLoginForm();
    })
//    begin - Dropdown script

    $(document).on("click","button[data-transaction-type]",function(e)
    {
        e.preventDefault();
        if(!$(this).hasClass("active"))
        {
            $(this).addClass("active");
            $(this).siblings().removeClass("active");
        }
    });

    $(document).on("click","a[data-property-type]",function(e){
        e.preventDefault();
        var $currentSelection = $("span[data-property-type]");
        $currentSelection.text($(this).text());
        $currentSelection.data("property-type",$(this).data("property-type"));
    });

    $(document).on("click","a[data-state]",function(e){
        e.preventDefault();
        var $currentSelection = $("span[data-state]");
        $currentSelection.text($(this).text());
        $currentSelection.data("state",$(this).data("state"));
    });
    $(document).on("submit","form[name=search-form]",function (e) {
        e.preventDefault();
        var propertyType = "";
        var transactionType = $("button[data-transaction-type].active").data("transaction-type");
        var state = "";

        if($("span[data-property-type]").is(":visible"))
        {
            propertyType = $("span[data-property-type]").data("property-type");
            state = $("span[data-state]").data("state");
        }
        else if($("select[data-state]").is(":visible"))
        {
            propertyType = $("select[data-property-type] option:selected").val();
            state = $("select[data-state] option:selected").val();
        }
        url = base_url + "buscar/"+propertyType+"-en-"+transactionType+"-en-"+state;
        window.location = url;
        // alert(url);
    })

//    end - Dropdown script
});

function getProjects()
{
    $.ajax({
        url : base_url + 'AjaxPublication/getAll',
        dataType  :"json",
        type : "POST",
        data : {
                urlRequested: "inmuebles-en-venta-en-santa-cruz",
                showOnlyProjects: 1,
                pageSize: 40,
                pageNumber: 1
        },
        success:function(response){
            $("#properties-carousel").html('');
            var htmlSource   = $("#ht-publication-quick-view").html();
            var template = Handlebars.compile(htmlSource);
            var searchResult = "";
            var thumbnailSettings = {
                colDefinition: "col-xs-12 col-sm-12 col-md-12 col-lg-12",
                hidePublicationName: ""
            };
            $.each(response.result,function(index, value){
                var data = {publication:value,thumbnailSettings:thumbnailSettings};
                var html    = template(data);
                searchResult += html;
            });
            $("#properties-carousel").html(searchResult);
            startCarousel();
        }
    });
}

function getPaidPublications()
{
    $.ajax({
        url : base_url + 'AjaxPublication/getPublicationsOnMainPage',
        dataType  :"json",
        type : "POST",
        data : {},
        success:function(response){
            $("#paid-publications").html('');
            var htmlSource   = $("#ht-publication-quick-view").html();
            var template = Handlebars.compile(htmlSource);
            var searchResult = "";
            var thumbnailSettings = {
                colDefinition: "col-xs-12 col-sm-6 col-md-4 col-lg-4",
                hidePublicationName: ""
            };
            $.each(response,function(index, value){
                var data = {publication:value,thumbnailSettings:thumbnailSettings};
                var html = template(data);
                searchResult += html;
            });
            $("#paid-publications").html(searchResult);
            $( ".item_property" ).hover(
                function() {
                    $(this).find("a h5").css("right", "-20em");
                    $(this).find(".av-portfolio-overlay").css("bottom", "0em");
                    $(this).find(".av-portfolio-info").css("top", "0em");
                }, function() {
                    $(this).find("a h5").css("right", "0em");
                    $(this).find(".av-portfolio-overlay").css("bottom", "-13em");
                    $(this).find(".av-portfolio-info").css("top", "-6em");
                }
            );
        }
    });
}

function startCarousel()
{
    $(".on-carousel").owlCarousel({
        items:4,
        loop:true,
        navigation:true,
        dots:false,
        margin:30,
        autoplay:true,
        autoplayTimeout:3000,
        responsive:{
            170:{
                items:1
            },
            500:{
                items:2
            },
            678:{
                items:2
            },
            768:{
                items:3
            },
            960:{
                items:3
            }
        }
    });
    cutText();
}
function startTestimonialCarousel()
{
    $('.testimonial-carousel').owlCarousel({
        items:1,
        loop:true,
        margin:10,
        autoplay:true,
        autoplayTimeout:3000,
        responsive:{
            480:{
                items:1
            },
            678:{
                items:1
            },
            960:{
                items:1
            }
        }
    });
}
