/**
 * Created by Jair on 18/12/2017.
 */

(function($) {
    $.fn.goTo = function() {
        $('html, body').animate({
            scrollTop: $(this).offset().top + 'px'
        }, 'fast');
        return this; // for chaining...
    }
})(jQuery);

var visualizer = 0;
var formRequestHandler = new FormRequestHandler();
formRequestHandler.sendRequest(window.location.href);
formRequestHandler.eventHandler();

var formRequestHandler2 = new FormRequestHandler();
formRequestHandler2.setResponseContent("#projects-content");
formRequestHandler2.setPaginationContent(".project-pagination");
formRequestHandler2.setOnlyProjects(1);
formRequestHandler2.setStoreSearchOnHistory(0);
formRequestHandler2.setOverlay("#overlay-projects");
formRequestHandler2.sendRequest(window.location.href);
formRequestHandler2.eventHandler();

var formRequestHandler3 = new FormRequestHandler();
formRequestHandler3.setPaginationContent(".map-pagination");
formRequestHandler3.setPageSize(1000);//the map view needs to show all properties found
formRequestHandler3.setStoreSearchOnHistory(0);
formRequestHandler3.setOverlay("#overlay-map");
formRequestHandler3.setMapMode(1);
window.onpopstate = function(event)
{
    console.log(event.state);
    if(event.state === "home")
    {
        window.location = window.location.href;
    }
    else
    {
        formRequestHandler.sendRequest(window.location.href);
        formRequestHandler.setStoreSearchOnHistory(0);
        formRequestHandler2.sendRequest(window.location.href);
        formRequestHandler2.setStoreSearchOnHistory(0);
        formRequestHandler3.sendRequest(window.location.href);
        formRequestHandler3.setStoreSearchOnHistory(0);
    }
};

$(document).on("click","a[href=#tab-map]",function(){
    if($.trim($("#maps").html()) === "")
    {
        formRequestHandler3.sendRequest(window.location.href);
        formRequestHandler3.eventHandler();
    }
});

$("div.search_box").on("change","input[type=radio]",function(){
    formRequestHandler.sendRequest();
    formRequestHandler2.sendRequest();
    formRequestHandler3.sendRequest();
});
$("#ajax-get-zones").on("change",function(){
    formRequestHandler.sendRequest();
    formRequestHandler2.sendRequest();
    formRequestHandler3.sendRequest();
});
$("div.search_box").on("click",".accept-price-range",function(e){
   e.preventDefault();
    formRequestHandler.sendRequest();
    formRequestHandler2.sendRequest();
    formRequestHandler3.sendRequest();
});

$("div.search_box").on("change","input[name='amenities[]']",function () {
    formRequestHandler.sendRequest();
    formRequestHandler2.sendRequest();
    formRequestHandler3.sendRequest();
})

$("input[type=radio][name=city]").on("change",function(){
    $("#ajax-get-zones").select2("val", "");

    if($(this).val() == "Bolivia")
    {
        $(".choose-state-label").slideDown("slow");
    }
    else if($(".choose-state-label").is(":visible"))
    {
        $(".choose-state-label").slideUp("slow");
    }
});

$(document).ready(function(){
    $(".input-masked").inputmask();
    /** select2 ajax for zones **/
    $('#ajax-get-zones').select2({
        placeholder : "Buscar..",
        allowClear : true,
        tags:true,
        ajax : {
            type : "post",
            dataType : "json",
            url : base_url + 'admin/AjaxZone/getAllZonesByStateId',
            quietMillis : 600,
            minimumInputLength : 4,
            data : function(term, page) {
                var stateId = $("input[type=radio][name=city]:checked").data("state-id");
                return {
                    stateId : stateId, // search by state
                    term : term, //search term
                    limit : 5 // page size
                };
            },
            results : function(data) {
                return {
                    results : data
                };
            }
        },
        width : "100%",
        initSelection : function(element, callback) {
            var jsonResp = JSON.parse(element.val());
            callback(jsonResp);
            element.val(jsonResp.id);
        }
    });

    $(document).on("click",".enhance-search",function(e){
        e.preventDefault();
        $(".search_box").slideToggle();
    });
});

$( ".show-more-options" ).click(function(e) {
    e.preventDefault();
    $(this).parent().find(".more-options").slideToggle( "slow" );
    $(this).parent().find(".show-more-options").slideToggle( "slow" );
});


$(".new-group-buttons label").on("click",function(e){
    e.preventDefault();
    var $radioToCheck = $(this).find("input[type=radio]");
    if($radioToCheck.is(":checked") && !$(this).parent().hasClass("currency"))
    {
        $(this).removeClass('active');
        $radioToCheck.prop("checked", false);
    }
    else
    {
        $(this).addClass('active');
        $(this).parent().children('label').not(this).removeClass('active');
        $radioToCheck.prop( "checked", true );
        $(this).parent().children('input[type=radio]').not($radioToCheck).prop( "checked", false );
    }
    formRequestHandler.sendRequest();
    formRequestHandler2.sendRequest();
    formRequestHandler3.sendRequest();
});

$(document).on('shown.bs.tab', 'a[data-toggle="tab"]', function (e) {
    cutText()
})