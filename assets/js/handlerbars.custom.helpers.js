    Handlebars.registerHelper('ifCond', function (v1, operator, v2, options) {

        switch (operator) {
            case '==':
                return (v1 == v2) ? options.fn(this) : options.inverse(this);
            case '===':
                return (v1 === v2) ? options.fn(this) : options.inverse(this);
            case '!=':
                return (v1 != v2) ? options.fn(this) : options.inverse(this);
            case '!==':
                return (v1 !== v2) ? options.fn(this) : options.inverse(this);
            case '<':
                return (v1 < v2) ? options.fn(this) : options.inverse(this);
            case '<=':
                return (v1 <= v2) ? options.fn(this) : options.inverse(this);
            case '>':
                return (v1 > v2) ? options.fn(this) : options.inverse(this);
            case '>=':
                return (v1 >= v2) ? options.fn(this) : options.inverse(this);
            case '&&':
                return (v1 && v2) ? options.fn(this) : options.inverse(this);
            case '||':
                return (v1 || v2) ? options.fn(this) : options.inverse(this);
            default:
                return options.inverse(this);
        }
    });
    Handlebars.registerHelper('each', function(context, options) {
        var out = "", data;

        if (options.data) {
            data = Handlebars.createFrame(options.data);
        }

        for (var i=0; i<context.length; i++) {
            if (data) {
                data.index = i;
            }

            out += options.fn(context[i], { data: data });
        }
        return out;
    });
    Handlebars.registerHelper('toLowerCase', function (str) {
        if(str && typeof str === "string") {
            return str.toLowerCase();
        }
        return '';
    });
    Handlebars.registerHelper('var',function(name, value, context){
        this[name] = value;
    });
    Handlebars.registerHelper('listSelectOptions', function(items, defaultValue) {
        var out = "";
        var selected = '';
        for(var i=0, l=items.length; i<l; i++) {
            selected = defaultValue != undefined && defaultValue.toLowerCase() == items[i].toLowerCase()?' selected ': '';
            out = out + "<option "+selected+" >" + items[i] + "</option>";
        }

        return out;
    });
    // Handlebars.registerHelper("switch", function(value, options) {
    //     this._switch_value_ = value;
    //     var html = options.fn(this); // Process the body of the switch block
    //     delete this._switch_value_;
    //     return html;
    // });
    // Handlebars.registerHelper("case", function(value, options) {
    //     if (value == this._switch_value_) {
    //         return options.fn(this);
    //     }
    // });
    Handlebars.registerHelper("switch", function(value, options) {
        this._switch_value_ = value;
        var html = options.fn(this); // Process the body of the switch block
        delete this._switch_value_;
        return html;
    });

    Handlebars.registerHelper("case", function() {
        // Convert "arguments" to a real array - stackoverflow.com/a/4775938
        var args = Array.prototype.slice.call(arguments);

        var options    = args.pop();
        var caseValues = args;

        if (caseValues.indexOf(this._switch_value_) === -1) {
            return '';
        } else {
            return options.fn(this);
        }
    });

    Handlebars.registerHelper("striptags", function( input ){
        // this prevents any overhead from creating the object each time
        var element = document.createElement('div');

        function decodeHTMLEntities (input) {
            if(input && typeof input === 'string') {
                // strip script/html tags
                input = input.replace(/<script[^>]*>([\S\s]*?)<\/script>/gmi, '');
                input = input.replace(/<\/?\w(?:[^"'>]|"[^"]*"|'[^']*')*>/gmi, '');
                element.innerHTML = input;
                input = element.textContent;
                element.textContent = '';
            }

            return input;
        }
        return decodeHTMLEntities(input);
    });

    Handlebars.registerHelper('base_url',function(value, context){
        return base_url;
    });

    Handlebars.registerHelper('var',function(name, value, context){
        this[name] = value;
    });

    Handlebars.registerHelper('numberFormat', function (value, options) {
        // Helper parameters
        var dl = options.hash['decimalLength'] || 2;
        var ts = options.hash['thousandsSep'] || ',';
        var ds = options.hash['decimalSep'] || '.';

        // Parse to float
        var value = parseFloat(value);

        // The regex
        var re = '\\d(?=(\\d{3})+' + (dl > 0 ? '\\D' : '$') + ')';

        // Formats the number with the decimals
        var num = value.toFixed(Math.max(0, ~~dl));

        // Returns the formatted number
        return (ds ? num.replace('.', ds) : num).replace(new RegExp(re, 'g'), '$&' + ts);
    });

    Handlebars.registerHelper('seoFormat', function (seo, options) {
        // Helper parameters
        var groupOrder = options.hash['groupOrder'];
        var propertyType = options.hash['propertyType'];
        var transactionType = options.hash['transactionType'];
        var stateName = options.hash['stateName'];
        var response = "";

        if(parseInt(groupOrder) === 1)
        {
            response = base_url + seo;
        }
        else
        {
            response = base_url+ "buscar/" +
                propertyType.replace(/ /g,"-").toLowerCase()+
                "-en-"+transactionType.replace(/ /g,"-").toLowerCase()+
                "-en-"+stateName.replace(/ /g,"-").toLowerCase()+"/"+
                seo;
        }

        return response;
    });

    Handlebars.registerHelper('cutWord', function (value, options) {
        // Helper parameters
        var maxLength = options.hash['maxLength'] || 20;
        var stripTag = options.hash['stripTag'] || 1;

        value = value.replace(/(<([^>]+)>)/ig,"");

        var parser=new DOMParser();
        var htmlDoc=parser.parseFromString(value, "text/html");
        value = $(htmlDoc).find("body").text();

        if(value.length > maxLength)
            value = value.substring(0, (maxLength -3))+"...";
        return value;
    });

    Handlebars.registerHelper('ucFirst', function (value, options) {
        // Helper parameters
        var maxLength = options.hash['maxLength'] || 20;
        var stripTag = options.hash['stripTag'] || 1;
        return text.toLowerCase().replace(/(\b\w)/gi,function(m){return m.toUpperCase();});
    });
    Handlebars.registerHelper('publicationQuickViewBgInfo', function (value, options) {
        // Helper parameters
        var backgroundPropertyInfo = "";
        switch (value)
        {
            case "1":
                backgroundPropertyInfo = userRole === "Admin"?"stand_out_project":"";
                break;
            case "2":
            case "3":
                backgroundPropertyInfo = userRole === "Admin"?"stand_out":"";
                break;
            default:
                backgroundPropertyInfo = "";
        }
        return backgroundPropertyInfo;
    });