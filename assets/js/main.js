// MAIN.JS
//--------------------------------------------------------------------------------------------------------------------------------
//This is main JS file that contains custom JS scipts and initialization used in this template*/
// -------------------------------------------------------------------------------------------------------------------------------
// 	- Template Name: Full State - Real State Template 
//	- Autor: Iwthemes
//	- Email: support@iwthemes.com
//	- Name File: main.js
//	- Version 1.4 - Updated on 14/08/2014
//	- Website: http://www.iwthemes.com 
//	- Copyright: (C) 2014
// -------------------------------------------------------------------------------------------------------------------------------


$(document).ready(function($) {

	'use strict';
    //var base_url = "http://localhost/toqueeltimbre/";
    //var base_url = "http://localhost/toque-el-timbre/";


    //$('.icon_certificado data-toggle="tooltip"').tooltip();

 	//=================================== Nav Responsive ===============================//
    /*$('#menu').tinyNav({
       active: 'selected'
    });*/

    //=================================== efect_switcher  ===================================//
		
	jcps.slider(500, '.switcher-panel', '.set2');

    //=================================== Google Maps Iframe =================================//

    if(window.innerWidth <= 800 && window.innerHeight <= 700) {
        $(".iframe_mapa").click(function() {
            $(this).attr("target", "_blank");
        });
    } else {
        $(".iframe_mapa").colorbox({iframe:true, width:"60%", height:"60%"});
    }

    //=================================== Tipologia =================================//

    if(window.innerWidth <= 800 && window.innerHeight <= 700) {
        $(".tipologia_zoom").colorbox({rel:'group1', width:"100%", height:"100%", current:"", fixed:true});
    } else {
        $(".tipologia_zoom").colorbox({rel:'group1', height:"90%", current:""});
    }

    //=================================== Precios =================================//

    if(window.innerWidth <= 1024) {
        $(".precio_zoom").colorbox({rel:'group1', width:"100%", height:"100%", current:"", fixed:true});
    } else {
        $(".precio_zoom").colorbox({rel:'group1', height:"auto", current:""});
    }

    //=================================== Project Slideshow Colorbox =================================//

    if(window.innerWidth <= 800 && window.innerHeight <= 700) {
        $("#project_detail .camera_link, .play_video").click(function() {
            $(this).attr("target", "_blank");
        });
    } else {
        $("#project_detail .camera_link, .play_video").colorbox({iframe:true, innerWidth:640, innerHeight:390});
    }

    //=================================== Planos =================================//

    if(window.innerWidth <= 800 && window.innerHeight <= 700) {
        $(".planos_zoom").colorbox({rel:'group1', width:"100%", height:"100%", current:"", fixed:true});
    } else {
        $(".planos_zoom").colorbox({rel:'group1', height:"90%", current:""});
    }

    //=================================== Volver Atras =================================//

    $(".volver_atras").click(function() {
        var url = $(this).attr("rel");
        location.href = url;
    });

    //=================================== Busqueda Usuarios =================================//

    if(window.innerWidth <= 800 && window.innerHeight <= 700) {
        $(".busqueda_usuario").colorbox({iframe:true, width:"100%", height:"100%", fixed:true});
    } else {
        $(".busqueda_usuario").colorbox({iframe:true, width:"40%", height:"70%"});
    }

    //=================================== Form Ordenamiento =================================//

    if(window.innerWidth <= 1024) {
        $("#sort-type_old").change(function() {
            $("#form-sort").submit();
        });
    } else {
        /*$("#sort-type option").click(function() {
            $("#form-sort").submit();
        });*/
        $("#sort-type_old").change(function() {
            $("#form-sort").submit();
        });
    }



    //=================================== Contact Form Property =================================//

    $("#form-contact input[type=button]").click(function(e){
        e.preventDefault();
        if(!$("#form-contact").parsley().isValid())
        {
            $("#form-contact").parsley().validate();

        }
        else {
            grecaptcha.execute();
        }
    });


    //=================================== Print Favorites =================================//

    $(".print").click(function () {
        $("#print-content").print({
            globalStyles : false,
            mediaPrint : false,
            iframe : true
        });
    });

    //=================================== Search Button Home =================================//

    //$("#button_search_home").click(function () {
    //    $("#form_search_home").submit();
    //});

    //=================================== Add favorite =================================//

    $(".btn_favorite").click(function() {
        var $btn = $(this);
        var inm_id = $(this).attr("rel");
        $.ajax({
            type: 'POST',
            url: base_url + "inmueble/favorito",
            data: { id_inmueble: inm_id },
            beforeSend: function() {
                // loading
            },
            success: function(data) {
                if (data == "true") {
                    $btn.addClass("check_favorite");
                }else{
                    $btn.removeClass("check_favorite");
                }
            }
        });
    });

    //=================================== Agregar intento de llamada estadistica =================================//

    $(".btn_phone_call").click(function(event) {
        var $btn = $(this);
        var inm_id = $(this).attr("rel");
        var ruta = $(this).attr("href");
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: base_url + "ajax/register_phone_action",
            data: { id_inmueble: inm_id },
            beforeSend: function() {},
            success: function(data) {
                location.href = ruta;
            }
        });
    });

    //=================================== Ajax Tipologia =================================//

    $(".tipologia_ajax").click(function() {
        var tipologia_id = $(this).attr("rel");
        $("body").css("overflow-y", "hidden");
        $.ajax({
            type: 'POST',
            url: base_url + "project/tipologia_ajax",
            data: { id_tipologia: tipologia_id },
            beforeSend: function() {
                $(".tipologia_detail_modal").show();
                var loading = base_url + "assets/img/loading.gif";
                $(".tipologia_detail_modal .modal_content").html("<center style='padding-top:20px;'><img src='" + loading + "' /></center>");
            },
            success: function(data) {
                $(".tipologia_detail_modal .modal_close").show();
                $(".tipologia_detail_modal .modal_content").html(data);
            }
        });
    });

    $(".tipologia_detail_modal .modal_close").click(function() {
        $(".tipologia_detail_modal").hide();
        $(".tipologia_detail_modal .modal_content").html("");
        $("body").css("overflow-y", "scroll");
        $(".tipologia_detail_modal .modal_close").hide();
    });

    //=================================== Delete favorite =================================//

    $(".btn_delete_favorite").click(function() {
        var $btn = $(this);
        var inm_id = $(this).attr("rel");
        $.ajax({
            type: 'POST',
            url: base_url + "inmueble/delete_favorite",
            data: { id_inmueble: inm_id },
            success: function(data) {
                $btn.parent().parent().hide();
            }
        });
    });

    //=================================== Send favorite =================================//

    $(".btn_submit_email").click(function() {
        $("#form-favorite-email-2").submit();
    });


    //=================================== Totop  ===================================//
	$().UItoTop({ 		
		scrollSpeed:500,
		easingType:'linear'
	});	

	//=================================== Subtmit Form  =================================//

	/*$('#form').submit(function(event) {
	  event.preventDefault();  
	  var url = $(this).attr('action');  
	  var datos = $(this).serialize();  
	  $.get(url, datos, function(resultado) {  
	    $('#result').html(resultado);  
	  });  
	});*/

	//=================================== Subtmit Calculator  =================================//

	$('.form_calculator').submit(function(event) {
        event.preventDefault();
        var url = $(this).attr('action');
        var datos = $(this).serialize();
        $.post(url, datos, function(resultado) {
            $('#result_calculator').html(resultado);
        });
	});

	//=================================== Form Newslleter  =================================//

  	/*$('#newsletterForm').submit(function(event) {
       event.preventDefault();  
       var url = $(this).attr('action');  
       var datos = $(this).serialize();  
        $.get(url, datos, function(resultado) {  
        $('#result-newsletter').html(resultado);  
	    });  
	});  */

	//=================================== jBar  ===================================//
	
	/*$('.jBar').hide();
	$('.jRibbon').show().removeClass('up', 500);
	$('.login').click(function(){
        $('.jRibbon').hide("fade");
		$('.jBar').slideToggle();
	});

    $('.downarrow').click(function(){
        $('.jRibbon').show("fade");
        $('.jBar').slideToggle();
    });*/


    //=================================== Switch button  ===================================//

    $(".search_advance input[type=checkbox].switch-button").switchButton({
        width: 80,
        height: 22,
        button_width: 40,
        on_label: 'USD',
        off_label: 'BOB',
        /*on_callback: function(){
            $("#currency").attr("value", "Dolares");
            $("#currency").attr("value", "Bolivianos");
        },
        off_callback: function(){
            $("#currency").attr("value", "Bolivianos");
        }*/
    });

	//=================================== Accordion  =================================//

	$('.accordion-container').hide(); 
	$('.accordion-trigger:first').addClass('active').next().show();
	$('.accordion-trigger').click(function(){
		if( $(this).next().is(':hidden') ) { 
			$('.accordion-trigger').removeClass('active').next().slideUp();
			$(this).toggleClass('active').next().slideDown();
		}
		return false;
	});

	//=================================== Sponsors Carousel  =================================// 

	$('.sponsors').owlCarousel({
		items:6,
		loop:true,
		margin:10,
		autoplayTimeout:4000,
		autoplay:true,
		responsive:{
			320:{
				items:1
			},	
			480:{
				items:2
			},
			678:{
				items:3
			},
			960:{
				items:5
			},
			1280:{
				items:6
			}
		}
	});

	//=================================== Testimonial Carousel  =================================// 

	$('.testimonial-carousel').owlCarousel({
		items:1,
		loop:true,
		margin:10,
		autoplay:true,
		autoplayTimeout:3000,
		responsive:{	
			480:{
				items:1
			},
			678:{
				items:1
			},
			960:{
				items:1
			}
		}
	});

	//=================================== Testimonial Carousel  =================================// 

	$('.on-carousel').owlCarousel({
		items:4,
		loop:true,
		navigation:true,
		dots:false,
		margin:30,
		autoplay:true,
		autoplayTimeout:3000,
		responsive:{
			170:{
				items:1
			},
			500:{
				items:2
			},
			678:{
				items:2
			},
			768:{
				items:3
			},
			960:{
				items:4
			}
		}
	});

    //=================================== Tooltips ==================================//

	/*$('.sponsors, .social, .tooltip_hover').tooltip({
      selector: "[data-toggle=tooltip]",
      container: "body"
   	});*/

   	 //=================================== Slide Home =====================================//
        if($('#slide').length>0)
        {
            $('#slide').camera({
                height: '650px',
                navigation: false
            });
        }


    //=================================== begin - Scroll Contact form =====================================//
    // if($( window ).width()>=975)
    // {
    //     var navBarHeight = $("nav").height();
    //     var bodyContentHeight = $(".padding_top").height();
    //     var contacFormHeight = $(".contact-form").height();
    //     var margins = 13;
    //     $('.contact-form').followTo();
    // }
    //=================================== end - Scroll Contact form =====================================//
    $(document).on("click","#search_button",function()
    {
        $("#search_container").toggleClass("hidden-xs");
    });
});
    if($('#slide_projects').length != 0) {
        $('#slide_projects').camera({
            height: '42%',
            navigation: true,
            navigationHover: false,
            mobileNavHover: false,
            autoAdvance: false,
            mobileAutoAdvance: true
        });
    }

    if($('#slide_details').length != 0) {
        $('#slide_details').camera({
            height: '55.5%',
            navigation: true,
            mobileAutoAdvance: true
        });
    }

function sendRequestContactForm(token)
{
    $("#form-contact").submit();
}