var map = new google.maps.Map(document.getElementById('map'), {
    zoom: zoom,
    center: new google.maps.LatLng(dep_latitud, dep_longitud),
    mapTypeId: google.maps.MapTypeId.ROADMAP
});

var infowindow = new google.maps.InfoWindow();

var marker, i;

for (i = 0; i < locations.length; i++) {
    marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map,
        icon: base_url + "assets/img/icons/icono_map1.png"
    });

    google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
            var html_marker = '<div class="inmueble">';
            html_marker = html_marker + '<div class="col-left-6"><div class="foto-lista"><a href="' + locations[i][9] + '" target="_blank"><img width="130" height="100" src="' + locations[i][3] + '"></a><div class="oferta"><i class="fa fa-home"></i> ' + locations[i][4] + ' en ' + locations[i][5] + '</div></div></div>';
            html_marker = html_marker + '<div class="col-right-6">';
            html_marker = html_marker + '<div class="detalles"><div class="direccion">' + locations[i][0] + '</div><div class="acciones"><div class="precio">' + locations[i][7] + ' ' + locations[i][8] + '</div></div><div class="zona">' + locations[i][6] + '</div><div class="enlace"><a href="' + locations[i][9] + '" class="button" style="padding:4px 8px; font-style: normal; font-size:11px;" target="_blank">Ver Inmueble</a></div></div>';
            html_marker = html_marker + '</div>';
            html_marker = html_marker + '</div>';


            infowindow.setContent('<div class="search-result div-marker">' + html_marker + '</div>');
            //infowindow.setContent(locations[i][0]);
            infowindow.open(map, marker);
        }
    })(marker, i));
}