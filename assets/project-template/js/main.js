(function ($) {
"use strict";
/*--
	Counter UP
-----------------------------------*/
$('.counter').counterUp();
/*--
	Menu Sticky
-----------------------------------*/
$(window).on('scroll', function() {
	var scroll = $(window).scrollTop();
    var sticky = $('.header-bottom');
	if (scroll < 400) {
		sticky.removeClass('stick');
	}else{
		sticky.addClass('stick');
	}
});
/*--
	Search Toggle
-----------------------------------*/
$('.search-toggle').on('click', function(){
    $('.header-search-form').toggleClass('active');
})
/*--
	One Page Menu
-----------------------------------*/
var TopOffsetId = $('.header-bottom').height() - -19;
$('.onepage-nav nav').onePageNav({
    currentClass: 'active',
    scrollThreshold: 0.2,
    scrollSpeed: 1000,
    scrollOffset: TopOffsetId,
});
/*--
	Bootstrap Menu Fix For Mobile
-----------------------------------*/
$(document).on('click','.navbar-collapse.in',function(e) {
    if( $(e.target).is('a') ) {
        $(this).collapse('hide');
    }
});
/*--
	Hero Slider
-----------------------------------*/
$('.hero-slider').slick({
    arrows: false,
    infinite: true,
    slidesToShow: 1,
    dots: true,
    fade: true,
    customPaging : function(slider, i) {
        var thumb = $(slider.$slides[i]).data('thumb');
        return '<button class="overlay"><img src="'+thumb+'"></button>';
    },
    responsive: [
        {
          breakpoint: 600,
          settings: {
              dots: false,
              autoplay: true,
              autoplaySpeed: 5000,
          }
        }
    ]
});
/*--
	Amenities Border Line
-----------------------------------*/
$('.amenities-content ul li').each(function(){

    var aLBorder = $(this).find('.left').width();
    var aRBorder = $(this).find('.right').width();

    $(this).find('.border').css({
        left: (aLBorder + 10),
        right: (aRBorder + 10),
    })
}) 
/*--
	Testimonial Slider
-----------------------------------*/
$('.testimonial-slider').slick({
    arrows: false,
    infinite: true,
    slidesToShow: 1,
});
/*--
	Blog Slider
-----------------------------------*/
$('.blog-slider').slick({
    arrows: false,
    infinite: true,
    slidesToShow: 3,
    responsive: [
        {
          breakpoint: 950,
          settings: {
              slidesToShow: 2,
          }
        },
        {
          breakpoint: 750,
          settings: {
              slidesToShow: 1,
          }
        }
    ]
});
/*--
	Magnific Popup
-----------------------------------*/
/*-- Video --*/
$('.video-popup').magnificPopup({
	type: 'iframe',
	mainClass: 'mfp-fade',
	removalDelay: 160,
	preloader: false,
	zoom: {
		enabled: true,
	}
});
/*-- Image --*/
$('.image-popup').magnificPopup({
	type: 'image',
});
/*-- Gallery --*/
$('.gallery-popup').magnificPopup({
	type: 'image',
	gallery:{
		enabled:true
	}	
});
/*-- Video Gallery --*/
$('.video-gallery-popup').magnificPopup({
	type: 'iframe',
	mainClass: 'mfp-fade',
	removalDelay: 160,
	preloader: false,
	zoom: {
		enabled: true,
	},
	gallery:{
		enabled:true
	}	
});
/*--
	Smooth Scroll
-----------------------------------*/
$('[data-scroll]').on('click', function(e) {
	e.preventDefault();
	var link = this;
	$.smoothScroll({
        speed: 1000,
        scrollTarget: link.hash,
        offset: -90,
	});
});
/*--
	Scroll Up
-----------------------------------*/
$.scrollUp({
	easingType: 'linear',
	scrollSpeed: 900,
	animation: 'fade',
	scrollText: '<i class="zmdi zmdi-chevron-up"></i>',
});

/*--
    Gallery filter
-----------------------------------*/
$(document).on("click",".filter-images",function(e)
{
   e.preventDefault();
   var imageType = $(this).data("image-type");
    $(".gallery-image-content div").show();
    if(imageType != "all")
    {
        $(".gallery-image-content div").not("."+imageType).hide();
    }

});
    /*--
            contact form
        -----------------------------------*/
    $(".form_contact_project input[type=submit]").click(function(e){
        e.preventDefault(); //STOP default action

        if(!$(".form_contact_project").parsley().isValid())
        {
            $(".form_contact_project").parsley().validate();

        }
        else {
            grecaptcha.execute();
        }
    });
})(jQuery);

function sendRequest(token)
{
    var postData = $(".form_contact_project").serializeArray();
    postData.push({token:token});
    var formURL = base_url + 'AjaxProject/sendContactMessage';
    $.ajax({
        url : formURL,
        dataType: "json",
        type: "POST",
        data : postData,
        beforeSend: function()
        {
            $("div.input-box.submit-box").find("input[type=submit]").addClass("hide");
            $("div.input-box.submit-box").find("img").removeClass("hide");
            $("div.input-box.submit-box").find(".content-result-message").html("");
        },
        success:function(data, textStatus, jqXHR)
        {
            if(data.response == 1)
            {
                var thankYouPageEndPoint = $(location).attr('href')+"/thank-you";
                window.location = thankYouPageEndPoint;
            }
            else if(data.response == 2)
            {
                launchQuickLoginForm();
                $("div.input-box.submit-box").find("input[type=submit]").removeClass("hide");
                $("div.input-box.submit-box").find("img").addClass("hide");
            }
            else
            {
                $("div.input-box.submit-box").find(".content-result-message").html(data.message);
                $("div.input-box.submit-box").find("input[type=submit]").removeClass("hide");
                $("div.input-box.submit-box").find("img").addClass("hide");
            }
            grecaptcha.reset();
        }
    });
}