var reCaptchaProjectContactFormModal = "";
$(document).ready(function() {
    startCarousel();
    registerLogVisitor();
    $(document).on("click","#test-whatsapp",function(e){
        e.preventDefault();
        sendToWhatsapp();
    });
     /** google maps **/
    if($("#maps").length > 0)
    {
        var mapHandler = new MapHandler();
        mapHandler.addInitialMarker();
        // mapHandler.loadEventHandlers();
    }
    $(document).on("click","a#open-modal-form-message",function(e){
        e.preventDefault();
        var htmlSource   = $("#ht-project-contact-message-modal-form").html();
        var template = Handlebars.compile(htmlSource);
        var data = {};
        var html    = template(data);
        bootbox.confirm({
            title:"CONSULTA",
            message: html,
            className: 'modal-project-contact-message',
            buttons: {
                confirm: {
                    label: 'ENVIAR CONSULTA',
                    className: 'btn-default modal-form-contact-send-message'
                },
                cancel: {
                    label: 'Cancelar',
                    className: 'hide'
                }
            },
            callback: function (result) {
                if(result)
                {
                    var $form = $("form.project-contact-message-modal-form");
                    if(!$form.parsley().isValid())
                    {
                        $form.parsley().validate();
                    }
                    else
                    {
                        exeRecaptchaProjectContactFormModal();
                    }
                    return false;
                }
            }
        });
    });
});

function exeRecaptchaProjectContactFormModal()
{
    $(this).prop("disabled", true);
    if($("#project-contact-message-modal-form-recaptcha").html("") || reCaptchaProjectContactFormModal === "")
    {
        reCaptchaProjectContactFormModal = grecaptcha.render('project-contact-message-modal-form-recaptcha', {
            'sitekey' : $(".project-contact-message-modal-form").find('input[name=site-key]').val(),
            'callback' : sendMessageContactFormModal,
            'size' : 'invisible'
        });
    }
    grecaptcha.execute(reCaptchaProjectContactFormModal);
}

function sendMessageContactFormModal()
{
    var postData = $(".project-contact-message-modal-form").serializeArray();
    var $sendButton = $(".btn.btn-default.modal-form-contact-send-message");
    var sendToWhatsapp = $("#send-to-whatsapp").is(":checked");
    postData.push({name:"proy_id","value":$("input[name=proy_id]").val()});
    console.log(postData);
    var formURL = base_url + 'AjaxProject/sendContactMessage';
    $.ajax({
        url : formURL,
        dataType: "json",
        type: "POST",
        data : postData,
        beforeSend: function()
        {
            $sendButton.addClass(".disabled");
            $sendButton.text("ENVIANDO..");
        },
        success:function(data, textStatus, jqXHR)
        {
            if(data.response === 1)
            {
                //This code segment eval if send the message through whatsapp
                if(sendToWhatsapp && data.user.cellPhone !== "")
                {
                    var project = data.user.project;
                    var projectName = project.proy_nombre;
                    var projectLink = "http://bit.ly/"+project.proy_short_seo;
                    if(project.proy_short_seo === "" || project.proy_short_seo === null)
                    {
                        projectLink = base_url + project.proy_seo;
                    }
                    var greeting = getGreeting();
                    var text = greeting+"..\nQuisiera por favor mas informacion del proyecto \n\""+projectName+"\"\nque publicó en ToqueElTimbre.com\nDetalle del proyecto\n"+projectLink;
                    text = encodeURI(text);
                    window.open("https://api.whatsapp.com/send?phone=591"+data.user.cellPhone+"&text="+text,"_blank");
                }
                var thankYouPageEndPoint = $(location).attr('href')+"/thank-you";
                window.location = thankYouPageEndPoint;
            }
            else if(data.response === 2)
            {
                bootbox.hideAll();
                launchQuickLoginForm();
            }
            grecaptcha.reset(reCaptchaProjectContactFormModal);
        }
    });
}

function startCarousel()
{
    $(".on-carousel").owlCarousel({
        items:4,
        loop:true,
        navigation:true,
        dots:false,
        margin:30,
        autoplay:true,
        autoplayTimeout:3000,
        responsive:{
            170:{
                items:1
            },
            500:{
                items:2
            },
            678:{
                items:2
            },
            768:{
                items:3
            },
            960:{
                items:3
            }
        }
    });
}

function registerLogVisitor()
{
    var url = "project/ajaxRegisterLogVisitor";
    var projectId = $("input[name=proy_id]").val();
    var postData = {projectId:projectId};
    if(projectId !== "")
    {
        $.ajax({
            url : url,
            dataType: "json",
            type: "POST",
            data : postData
        });
    }
}
function getGreeting()
{
    var d = new Date();
    var time = d.getHours();
    var greeting = "";

    if (time < 12)
    {
        greeting = "Buenos dias";
    }
    if (time >= 12)
    {
        greeting = "Buenas tardes";
    }
    if (time >= 19)
    {
        greeting = "Buenas noches";
    }
    return greeting;
}
function sendToWhatsapp()
{
    var text = "Buenas tardes\nAun esta disponible el inmueble \n\"Condominio Privado Colonial Norte\"\nque publicó en ToqueElTimbre.com?\nDetalle del inmueble\nhttp://toqueeltimbre.com/buscar/inmuebles-en-bolivia/condominio-privado-colonial-norte-1";
    var text = "Buenas tardes\nAun esta disponible el inmueble \n\"Condominio Privado Colonial Norte\"\nque publicó en ToqueElTimbre.com?\nDetalle del inmueble\n"+base_url+"nano-by-smart-studio";
    // text = "UltraCasas.com%20te%20conecta%3A%0D%0AEstoy%20interesado%20en%20tu%20Casa%20en%20Venta%20en%207MO%20ANILLO%20ZONA%20NORTE%20en%20%24us%20195%2C000%0D%0APor%20favor%20m%C3%A1s%20informaci%C3%B3n%0D%0Ahttp%3A%2F%2Fultra.bo%2Fs1aeu";
    text = encodeURI(text);
    window.open("https://api.whatsapp.com/send?phone=59177017333&text="+text);
    // var thankYouPageEndPoint = $(location).attr('href')+"/thank-you";
    // window.location = thankYouPageEndPoint;
}