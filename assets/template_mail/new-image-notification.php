<?php
$propertyTypeArray = array(
    "1" => "CASAS",
    "2" => "DEPARTAMENTOS",
    "3" => "OFICINAS",
    "4"	=> "LOCALES COMERCIALES",
    "13" =>	"TERRENOS",
    "14" =>	"QUINTAS Y PROPIEDADES"
);

$transactionType = array(
    "1" => "VENTA",
    "2"	=> "ALQUILER",
    "3"	=> "ANTICRETICO"
);
$city = model_ciudad::getById($property["inm_ciu_id"]);
$department = model_departamento::getById($city->ciu_dep_id);

?>
<html>
<head></head>
<body style="background-color:#f3f3f3; color: #4a4a4a; font-family: Helvetica,Arial,sans-serif;">
<table width="100%" cellpadding="0" cellspacing="0" style="border-spacing: 0; line-height: 20px; margin: 0 auto;">
    <tr>
        <td style="padding-left: 10px; padding-right: 10px;">
            <table width="100%" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" style="margin: 0 auto; max-width: 600px; width: 100%; margin-top: 3%;">
                <tr>
                    <td style="padding-top:2%; border-top: 4px #19bdf4 solid; text-align: center">
                        <a href="http://toqueeltimbre.com" target="_blank">
                            <img src="http://toqueeltimbre.com/assets/img/logo.png" style="width: 100%; max-width: 300px;"/>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #ffffff; padding: 6% 9%;">
                        <table width="100%" cellpadding="0" cellspacing="0" style="padding-bottom: 20px; width: 100%;">
                            <tr>
                                <td style="font-size: 14px; line-height: 22px;" colspan="2">
                                    <p style="color:#474747">Se han cargado nuevas imagenes(<?=count($propertyImageArray)?>) a la publicación con el siguiente detalle:</p>
                                    <p>
                                        <b>Usuario: </b><?=$userInfo["usu_nombre"]." ".$userInfo["usu_apellido"]?><br>
                                        <b>Tipo de usuario: </b><?=$userInfo["usu_tipo"]?><br>
                                        <b>E-mail: </b><?=$userInfo["usu_email"]?><br>
                                        <b>Id de publicacion: </b><?=$publication["pub_id"]?><br>
                                        <b>Tipo de inmueble: </b><?=$propertyTypeArray[$property["inm_cat_id"]]?><br>
                                        <b>Tipo de transaccion: </b><?=$transactionType[$property["inm_for_id"]]?><br>
                                        <b>Precio: </b><?=number_format($property["inm_precio"],0)?><br>
                                        <b>Superficie: </b><?=$property["inm_superficie"]?><br>
                                        <b>Precio m2: </b><?=$property["inm_superficie"]>0?round($property["inm_precio"]/$property["inm_superficie"],2):""?><br>
                                        <b>Superficie construida: </b><?=$propertyCharacteristics->eca_valor?><br>
                                        <b>precio m2 construido: </b><?=$propertyCharacteristics->eca_valor>0?round($property["inm_precio"]/$propertyCharacteristics->eca_valor,2):""?><br>
                                        <b>Ubicacion: </b><?=$property["inm_direccion"]?><br>
                                    </p>
                                    <p style="text-align: center;">
                                        <a
                                            href="<?=base_url("buscar/".seo_url($propertyTypeArray[$property["inm_cat_id"]]."-en-".$transactionType[$property["inm_for_id"]]."-en-".$department->dep_nombre)."/".$property["inm_seo"]);?>"
                                            style="
                                                    margin-right: 20px;
                                                    text-align: right;
                                                    margin-bottom: 20px;
                                                    font-size: 16px;
                                                    padding: 8px 14px;
                                                    border-color: #337ab7;
                                                    BACKGROUND-COLOR: #2e6da4;
                                                    color: #ffffff;
                                                    text-decoration: none;
                                                    ">
                                            Ver publicacion
                                        </a>
                                        <a
                                            href="<?=base_url("inmueble/deactivate/".$property["inm_id"])?>"
                                            style="
                                                    text-align: right;
                                                    margin-bottom: 20px;
                                                    font-size: 16px;
                                                    padding: 8px 14px;
                                                    border-color: #d43f3a;
                                                    BACKGROUND-COLOR: #d9534f;
                                                    color: #ffffff;
                                                    text-decoration: none;
                                                    ">
                                            Desactivar publicacion
                                        </a>
                                        <a
                                            href="<?=base_url("admin/Property/edit/".$property["inm_id"])?>"
                                            style="
                                                    text-align: right;
                                                    margin-bottom: 20px;
                                                    font-size: 16px;
                                                    padding: 8px 14px;
                                                    border-color: #d43f3a;
                                                    BACKGROUND-COLOR: #d9534f;
                                                    color: #ffffff;
                                                    text-decoration: none;
                                                    ">
                                            Editar publicacion
                                        </a>
                                    </p>

                                    <p style="color:#474747">Estas son las imagenes que se han cargado</p>
                                </td>
                            </tr>
                            <?php
                            foreach($propertyImageArray as $image)
                            {
                                $fileHandler = new File_Handler($image["fot_archivo"]);
                                $thumbnail = $fileHandler->getThumbnail("300","300");
                                ?>
                                <tr>
                                    <td style="vertical-align: top;text-align: right;width: 50px;">
                                        <p style="margin-top: 19px;">
                                            <a href="<?=base_url("inmueble/removeImage/".$property["inm_id"]."/".$image["fot_archivo"])?>" style="text-align: right;margin-bottom: 20px;font-size: 16px;padding: 8px 14px;border: solid 1px #dedede;BACKGROUND-COLOR: #018Aba;color: #ffffff;text-decoration: none;background-color: #d9534f;border-color: #d43f3a;" target="_blank" rel="noreferrer">X</a>
                                        </p>
                                    </td>
                                    <td style="vertical-align: top;text-align: left;width: 300px;">
                                        <p>
                                            <img src="<?=$thumbnail->getSource()?>" alt="<?=$thumbnail->getAlternateText()?>"><br><br>
                                        </p>
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #555759; border-top:5px solid #00aeef; padding: 2% 9%;">
                        &nbsp;
                        <!--<table width="100%" cellpadding="0" cellspacing="0" style="padding-bottom: 20px; width: 100%;">
                            <tr>
                                <td style="color: #fff; text-align: center; line-height: 20px;">
                                    Avenida Beni Calle Murur&eacute; 2010<br />
                                    Telf. (591) 3-3437530<br />
                                    info@toqueeltimbre.com<br />
                                    Santa Cruz - Bolivia
                                </td>
                            </tr>
                        </table>-->
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td width="100%" style="padding:20px 0" cellpadding="0" cellspacing="0" border="0">
            <table width="100%" cellspacing="0" cellpadding="0" border="0" style="border-collapse:collapse">
                <tr>
                    <td width="100%" style="padding:20px">
                        <div style="margin:0;font-size:13px;text-align:center;color:#a8a8a8">&copy; <?=date("Y")?> ToqueElTimbre.com</div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
<html>
		

					