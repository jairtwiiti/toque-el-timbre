<?php

//require_once("clases/phpmailer/class.phpmailer.php");

class CARACTERISTICA extends BUSQUEDA
{
	var $formulario;
	var $mensaje;
	
	function CARACTERISTICA()
	{
		//permisos
		$this->ele_id=134;
		
		$this->busqueda();
		
		if(!($this->verificar_permisos('AGREGAR')))
		{
			$this->ban_agregar=false;
		}
		//fin permisos
		
		$this->num_registros=14;
		
		$this->coneccion= new ADO();
		
			
		
		$this->arreglo_campos[0]["nombre"]="car_descripcion";
		$this->arreglo_campos[0]["texto"]="DESCRIPCION";
		$this->arreglo_campos[0]["tipo"]="cadena";
		$this->arreglo_campos[0]["tamanio"]=25;
		
		
		
		$this->link='gestor.php';
		
		$this->modulo='caracteristica';
		
		$this->formulario = new FORMULARIO();
		
		$this->formulario->set_titulo('CARACTERISTICAS');
	}
	
	
	
	function dibujar_busqueda()
	{
		?>
		<script>		
		function ejecutar_script(id,tarea){
				var txt = 'Esta seguro de eliminar la Caracteristica?';
				
				$.prompt(txt,{ 
					buttons:{Si:true, No:false},
					callback: function(v,m,f){
						
						if(v){
								location.href='gestor.php?mod=caracteristica&tarea='+tarea+'&id='+id;
						}
												
					}
				});
			}

		</script>
		<?php
		$this->formulario->dibujar_cabecera();
		
		$this->dibujar_listado();
	}
	
	function set_opciones()
	{
		$nun=0;
		
		if($this->verificar_permisos('VER'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='VER';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_search.png';
			$this->arreglo_opciones[$nun]["nombre"]='VER';
			$nun++;
		}
		
		if($this->verificar_permisos('MODIFICAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='MODIFICAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_edit.png';
			$this->arreglo_opciones[$nun]["nombre"]='MODIFICAR';
			$nun++;
		}
		
		
		if($this->verificar_permisos('ELIMINAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='ELIMINAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_drop.png';
			$this->arreglo_opciones[$nun]["nombre"]='ELIMINAR';
			$this->arreglo_opciones[$nun]["script"]="ok";
			$nun++;
		}
		
		
	}
	
	function dibujar_listado()
	{
		$sql="SELECT 
				car_id,car_nombre,car_descripcion,car_tipo,car_tamano
			  FROM 
				caracteristica		
				";
		
		$this->set_sql($sql,' order by car_id desc ');
		
		$this->set_opciones();
		
		$this->dibujar();
		
	}
	
	function dibujar_encabezado()
	{
		?>
			<tr>
			    <th>Descripcion</th>
				<th>Nombre</th>
				<th>Tipo</th>
				<th>Tama�o</th>	        			
	            <th class="tOpciones" width="150px">Opciones</th>
			</tr>
		<?PHP
	}
	
	function mostrar_busqueda()
	{
				
		for($i=0;$i<$this->numero;$i++)
			{
				
				$objeto=$this->coneccion->get_objeto();
				echo '<tr>';
					echo "<td>";
						echo $objeto->car_descripcion;
					echo "</td>";					
					echo "<td>";
						echo $objeto->car_nombre;
					echo "</td>";
					echo "<td>";
						echo  $objeto->car_tipo;
					echo "</td>";
					echo "<td>";
						echo  $objeto->car_tamano;
					echo "</td>";		
					echo "<td>";
						echo $this->get_opciones($objeto->car_id);
					echo "</td>";
				echo "</tr>";
				
				$this->coneccion->siguiente();
			}
	}
	
	function cargar_datos()
	{
		$conec=new ADO();
		
		$sql="select 
			*  
		from caracteristica
				where car_id = '".$_GET['id']."'";
		
		$conec->ejecutar($sql);
		
		$objeto=$conec->get_objeto();
		
		$_POST['car_id']=$objeto->car_id;
		$_POST['car_nombre']=$objeto->car_nombre;
		$_POST['car_descripcion']=$objeto->car_descripcion;
		$_POST['car_tamano']=$objeto->car_tamano;		
		$_POST['car_tipo']=$objeto->car_tipo;						
	}
	
	function datos()
	{
		if($_POST)
		{
			//texto,  numero,  real,  fecha,  mail.
			$num=0;
			
			$valores[$num]["etiqueta"]="Nombre";
			$valores[$num]["valor"]=$_POST['car_nombre'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			
			$valores[$num]["etiqueta"]="Descripcion";
			$valores[$num]["valor"]=$_POST['car_descripcion'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			
			$valores[$num]["etiqueta"]="Tipo";
			$valores[$num]["valor"]=$_POST['car_tipo'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			// $num++;
			
			// $valores[$num]["etiqueta"]="Tama�o";
			// $valores[$num]["valor"]=$_POST['car_tamano'];
			// $valores[$num]["tipo"]="numero";
			// $valores[$num]["requerido"]=true;
			
			$val=NEW VALIDADOR;
			
			$this->mensaje="";
			
			if($val->validar($valores))
			{
				return true;
			}
				
			else
			{
				$this->mensaje=$val->mensaje;
				return false;
			}
		}
			return false;
		
	}
	
	function formulario_tcp($tipo)
	{
				
		switch ($tipo)
		{
			case 'ver':{
						$ver=true;
						break;
						}
					
			case 'cargar':{
						$cargar=true;
						break;
						}
		}
		
		$url=$this->link.'?mod='.$this->modulo;
		
		$red=$url.'&tarea=ACCEDER';
		
		if(!($ver))
		{
			$url.="&tarea=".$_GET['tarea'];
		}
		
		if($cargar)
		{
			$url.='&id='.$_GET['id'];
		}
		
		$this->formulario->dibujar_tarea();
		
		if($this->mensaje<>"")
		{
			$this->formulario->mensaje('Error',$this->mensaje);
		}
		$fun=NEW FUNCIONES;		
			?>
			
			
			<script type="text/javascript">
			
			$().ready(function(){
			   $('.botonagr').click(function(){
					var contextoClick = this;			
					var nuevo_indice_detcomp = $("#detcar tbody tr").length;
					var data = "<tr><td>"+(nuevo_indice_detcomp+1)+"<input type='hidden' name='orden[]' value='"+(nuevo_indice_detcomp+1)+"'></td><td><input type='text' name='key[]' size='10'></td><td><input type='text' name='valor[]' size='30'></td><td><img src='images/b_drop.png' class='borrar-detalle' style='cursor:pointer'></td></tr>";
					$('#detcar tbody').append(data);		
				});	
				
				

               $('#detcar').delegate('.borrar-detalle','click',function(){
					if (confirm('Esta usted seguro de eliminar esta fila?')){				   
						var con=1;       					
						var $link = $(this);					
						$link.closest('tr').remove();						
						$("#detcar tbody tr").each(function(){						
							 $(this).children('td').eq(0).text(con);
							 con++;
						});						
					}
				});				
				
				 $('#car_tipo').change(function(){
				 
				     var $tipo = $(this);
					 
					 if ($tipo.attr('value')=="select")
					    $(".valores").css("display", "block"); 
					 else
                        $(".valores").css("display", "none");  					 
				        
						
				});	
				
				
			});
			
				
				
			function enviar_formulario(){					
				
				if ($("#car_tipo").attr('value')=="select")
				{
					var numcar=$("#detcar tbody tr").length;
					if (numcar>0)
					{
						  document.frm_sentencia.submit();
					}
					else
					{					  		
					   if (numcar==0)
						  $.prompt('Agregue al menos un valor',{ opacity: 0.8 });				  
					}
				}
				else
				{
				   document.frm_sentencia.submit();
				}					
			}
				</script>
			<script type="text/javascript" src="js/ajax.js"></script>
			<div id="Contenedor_NuevaSentencia" >
			<form id="frm_sentencia" name="frm_sentencia" action="<?php echo $url;?>" method="POST" enctype="multipart/form-data">  
				<div id="FormSent" style="width:100%;">
				  
						<div class="Subtitulo">Datos</div>
						<div id="ContenedorSeleccion">						   
						
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">*</span>Nombre</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto"  name="car_nombre" id="car_nombre"  maxlength="200" size="40"  value="<?php echo $_POST['car_nombre'];?>">
							   </div>
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">*</span>Descripcion</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto"  name="car_descripcion" id="car_descripcion"  maxlength="200" size="40"  value="<?php echo $_POST['car_descripcion'];?>">
							   </div>
							</div>
							<!--Fin-->							
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">*</span>Tipo</div>
							   <div id="CajaInput">
								    <select name="car_tipo" id="car_tipo" class="caja_texto">
									<option value="" >Seleccione</option>
									<option value="input" <?php if($_POST['car_tipo']=='input') echo 'selected="selected"'; ?>>Input</option>
									<option value="select" <?php if($_POST['car_tipo']=='select') echo 'selected="selected"'; ?>>Select</option>
									<option value="textarea" <?php if($_POST['car_tipo']=='textarea') echo 'selected="selected"'; ?>>Textarea</option>
									</select>
							   </div>
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Tama�o</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto"  name="car_tamano" id="car_tamano"  maxlength="200" size="5"  value="<?php echo $_POST['car_tamano'];?>">
							   </div>
							</div>
							<!--Fin-->	
							<!--Inicio-->
							
						
							
						
							
							
							<div id="ContenedorDiv" class="valores" style="display:<?php if($_POST['car_tipo']=='select') echo 'block'; else echo 'none'; ?>">
							    <div class="Etiqueta" ><span class="flechas1">*</span>Valores:</div>
								<div id="CajaInput">
									<input type="button" class="boton botonagr" name="" value="Agregar"  onclick="">
								</div>
							   	<div id="CajaInput" style="margin:0px 0px 0px 10px;">							
								<table class="tablaReporte" width="400" cellpadding="0" cellspacing="0" border="0" id="detcar">
								<thead>
									<tr>
										<th width="5%">&nbsp;#&nbsp;</th>
										<th width="15%">key</th>
										<th width="50%">Valor</th>
										<th width="5%"><?php echo "&nbsp;";?></th>
									</tr>
								</thead>  
								<tbody>
								<?php
									if($_GET['tarea']=='MODIFICAR' || $_GET['tarea']=='VER')
										$this->cargar_filas_detalle($_GET['id']);
									?>
									
									 
								</tbody>    
								</table> 
								</div>
							</div>
						</div>
					
						
					
						<div id="ContenedorDiv">
						   <div id="CajaBotones">
								<center>
								<?php
								if(!($ver))
								{
									?>
									<input type="button" class="boton" name="" value="Guardar"  onclick="javascript:enviar_formulario()">
									<input type="reset" class="boton" name="" value="Cancelar">
									<input type="button" class="boton" name="" value="Volver" onclick="javascript:location.href='<?php echo $red;?>';">
									<?php
								}
								else
								{
									?>
									<input type="button" class="boton" name="" value="Volver" onclick="javascript:location.href='<?php echo $red;?>';">
									<?php
								}
								?>
								</center>
						   </div>
						</div>
				</div>
			</form>
		</div>
		<?php
	}
	
	function insertar_tcp()
	{
		
		$conec= new ADO();
		
		
		$sql="insert into caracteristica (
		car_nombre,
		car_descripcion,
		car_tipo,
		car_tamano		
		) 
		values (
		'".$_POST['car_nombre']."',
		'".$_POST['car_descripcion']."',
		'".$_POST['car_tipo']."',
		'".$_POST['car_tamano']."'		
		)";
		
	
	
		$conec->ejecutar($sql,false);
		
		$llave=mysql_insert_id();
				
		if ($_POST['car_tipo'] == 'select' )
		{		
			$keys=$_POST['key'];
			$i=0;			
			
			foreach($keys as $key)
			{				
				$sql="insert into caracteristica_detalle(det_car_id,det_orden,det_key,det_valor) values 
								('".$llave."','".$_POST['orden'][$i]."','".$key."','".$_POST['valor'][$i]."')";

				$conec->ejecutar($sql);
				$i++;
			}		
		}
		
		$mensaje='Caracteristica Agregada Correctamente.';
		
		$tipo='Correcto';
		
		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER'.'�'.$this->link.'?mod='.$this->modulo.'&tarea=AGREGAR','Ir al listado�Continuar Agregando',$tipo);
	}
	
	function modificar_tcp()
	{	
	
			$conec= new ADO();	
			
			   $sql="update caracteristica set 
									car_nombre='".$_POST['car_nombre']."',
									car_descripcion='".$_POST['car_descripcion']."',
                                    car_tipo='".$_POST['car_tipo']."',
									car_tamano='".$_POST['car_tamano']."'									
								where car_id='".$_GET['id']."'";
			
					
			
			
			
			$conec->ejecutar($sql);
			
			$sql="delete from caracteristica_detalle where det_car_id='".$_GET['id']."'";
			$conec->ejecutar($sql);
			
			if ($_POST['car_tipo'] == 'select' )
			{		
			    
				
				
				$keys=$_POST['key'];
				$i=0;			
				
				foreach($keys as $key)
				{				
					$sql="insert into caracteristica_detalle(det_car_id,det_orden,det_key,det_valor) values 
									('".$_GET['id']."','".$_POST['orden'][$i]."','".$key."','".$_POST['valor'][$i]."')";

					$conec->ejecutar($sql);
					$i++;
				}		
			}
						
			$mensaje='Caracteristica Modificado Correctamente';
			
			$tipo='correcto';
		
			$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER','',$tipo);
	}
	
	function eliminar_tcp()
	{	    
		$conec= new ADO();
		
		$sql="delete FROM caracteristica where car_id='".$_GET['id']."'";

		$conec->ejecutar($sql);
		
		$sql="delete from caracteristica_detalle where det_car_id='".$_GET['id']."'";
		 
		$conec->ejecutar($sql);		
		
		$mensaje='Caracteristica Eliminada Correctamente.';
		
		$tipo='Correcto';
		
		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER','',$tipo);
	}
	
	function cargar_filas_detalle($id)
	{
		$conec= new ADO();
	
		$sql="select * from caracteristica_detalle where det_car_id='$id' order by det_orden";		

		$conec->ejecutar($sql);
		
		$num=$conec->get_num_registros();
		
		$cad="";
		
		$fun=NEW FUNCIONES;		
		
		for($i=0;$i<$num;$i++)
		{
			
			$objeto=$conec->get_objeto();
			
			?>
			<tr>
			   <td width='5%'><? echo $objeto->det_orden;?><input type='hidden' name='orden[]' value='<? echo $objeto->det_orden;?>'></td>
			   <td><input type='text' name='key[]' size='10' value='<? echo $objeto->det_key;?>'></td>
			   <td><input type='text' name='valor[]' size='30' value='<? echo $objeto->det_valor;?>'></td>		   
			   <td><img src='images/b_drop.png' class='borrar-detalle' style='cursor:pointer'></td>
			</tr>
				
			
			<?php
			
			$conec->siguiente();
		}
		
	}
	
	
}
?>