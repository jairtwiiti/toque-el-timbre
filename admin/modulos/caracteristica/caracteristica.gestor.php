<?php
	require_once('caracteristica.class.php');
	
	$caracteristica = new CARACTERISTICA();
	
	if(!($caracteristica->verificar_permisos($_GET['tarea'])))
	{
		?>
		<script>
			location.href="log_out.php";
		</script>
		<?php
	}
	
	switch ($_GET['tarea'])
	{
		case 'AGREGAR':{
						
							if($caracteristica->datos())
							{
								$caracteristica->insertar_tcp();
							}
							else 
							{
								$caracteristica->formulario_tcp('blanco');
							}
							
							break;
		}
		case 'VER':{
							$caracteristica->cargar_datos();
													
							$caracteristica->formulario_tcp('ver');
							
							break;
		}
		case 'MODIFICAR':{
		
		                if($_GET['acc']=='Imagen')
						{
							$caracteristica->eliminar_imagen();
						}
						else
						{
							if($caracteristica->datos())
							{
								$caracteristica->modificar_tcp();
							}
							else 
							{
								if(!($_POST))
								{
									$caracteristica->cargar_datos();
								}
								$caracteristica->formulario_tcp('cargar');
							}
							break;
						}
		}					
		case 'ELIMINAR':{
							
							$caracteristica->eliminar_tcp();
								
							break;
		}
		case 'ACCEDER':{
							$caracteristica->dibujar_busqueda();
							break;
		}
		
	}
?>