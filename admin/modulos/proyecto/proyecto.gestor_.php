<?php
	
	require_once('proyecto.class.php');
	
	$proyecto = new PROYECTO();
	
	if($_GET['tarea']<>"")
	{
		if(!($proyecto->verificar_permisos($_GET['tarea'])))
		{
			?>
			<script>
				location.href="log_out.php";
			</script>
			<?php
		}
	}
	
	switch ($_GET['tarea'])
	{
		case 'AGREGAR':{
						if($_GET['acc']=='Emergente')
						{
							$proyecto->emergente();							
						}
						else{	
						
							if($proyecto->datos())
							{
								$proyecto->insertar_tcp();
							}
							else 
							{
								$proyecto->formulario_tcp('blanco');
							}
						}

						break;}
		case 'VER':{
						$proyecto->cargar_datos();
												
						$proyecto->formulario_tcp('ver');
						
						break;}
		case 'MODIFICAR':{
							
							if($proyecto->datos())
							{
								$proyecto->modificar_tcp();
							}
							else 
							{
								if(!($_POST))
								{
									$proyecto->cargar_datos();
								}
								$proyecto->formulario_tcp('cargar');
							}
							
						break;}
		
		
						
		case 'DISPONIBILIDAD':{				
			
			
			if($_GET['acc']=='MODIFICAR_DISPONIBILIDAD')
			{
				if($proyecto->datos_disponibilidad())
				{
					$proyecto->modificar_disponibilidad();
				}
				else
				{
					if(!($_POST))
					{
						$proyecto->cargar_datos_disponibilidad();
					}
					$proyecto->formulario_tcp_disponibilidad('cargar');
				}
			}
			else						
			{
				if($_GET['acc']=='ELIMINAR')
					{
						$proyecto->eliminar_disponibilidad($_GET['dis_id']);
					}
					
					if($_GET['acc']=='s'||$_GET['acc']=='b'){
						$proyecto->orden_publicacion($_GET['pro'],$_GET['acc'],$_GET['or'],$_GET['id']);
					}								
					
					if($proyecto->datos_disponibilidad())
					{
						
						$proyecto->guardar_disponibilidad();
					}
					
					$proyecto->formulario_tcp_disponibilidad('cargar');
					
					$proyecto->dibujar_encabezado_disponibilidad();
					
					$proyecto->mostrar_busqueda_disponibilidad();
			}
			
			
			
		break;}
		
		
		case 'ELIMINAR':{
							
								if(isset($_POST['proy_id']))
								{
									if(trim($_POST['proy_id'])<>"")
									{
										$proyecto->eliminar_tcp();
									}
									else 
									{
										$proyecto->dibujar_busqueda();
									}
								}
								else 
								{
									$proyecto->formulario_confirmar_eliminacion();
								}
							
							
						break;}
		
		/*case 'PRODUCTOS':{
						if($_GET['acc']=='MODIFICAR_PRODUCTO')
						{
							if($proyecto->datos2())
							{
								$proyecto->modificar_producto();
							}
							else
							{
								if(!($_POST))
								{
									$proyecto->cargar_datos2();
								}
								$proyecto->formulario_tcp2('cargar');
							}
						}
						else						
						{
							if($_GET['acc']=='ELIMINAR')
								{
									$proyecto->eliminar_producto($_GET['pro_id']);
								}
								
								if($_GET['acc']=='s'||$_GET['acc']=='b'){
									$proyecto->orden2($_GET['pro'],$_GET['acc'],$_GET['or'],$_GET['id']);
								}								
								
								if($proyecto->datos2())
								{
									$proyecto->guardar_producto();
								}
								
								$proyecto->formulario_tcp2('cargar');
								
								$proyecto->dibujar_encabezado2();
								
								$proyecto->mostrar_busqueda2();
						}
								
								
						break;
					}*/
					
		case 'PUBLICACIONES':{
						if($_GET['acc']=='MODIFICAR_PUBLICACION')
						{
							if($proyecto->datos_publicacion())
							{
								$proyecto->modificar_publicacion();
							}
							else
							{
								if(!($_POST))
								{
									$proyecto->cargar_datos_publicacion();
								}
								$proyecto->formulario_tcp_publicacion('cargar');
							}
						}
						else						
						{
							if($_GET['acc']=='ELIMINAR')
								{
									$proyecto->eliminar_publicacion($_GET['inm_id']);
								}
								
								if($_GET['acc']=='s'||$_GET['acc']=='b'){
									$proyecto->orden_publicacion($_GET['pro'],$_GET['acc'],$_GET['or'],$_GET['id']);
								}								
								
								if($proyecto->datos_publicacion())
								{
									$proyecto->guardar_publicacion();
								}
								
								$proyecto->formulario_tcp_publicacion('cargar');
								
								$proyecto->dibujar_encabezado_publicacion();
								
								$proyecto->mostrar_busqueda_publicacion();
						}
								
								
						break;
					}
					
		case 'TIPOLOGIA':{
						if($_GET['acc']=='MODIFICAR_TIPOLOGIA')
						{
							if($proyecto->datos_tipologia())
							{
								$proyecto->modificar_tipologia();
							}
							else
							{
								if(!($_POST))
								{
									$proyecto->cargar_datos_tipologia();
								}
								$proyecto->formulario_tcp_tipologia('cargar');
							}
						}
						else						
						{
							if($_GET['acc']=='ELIMINAR')
								{
									$proyecto->eliminar_tipologia($_GET['tip_id']);
								}
								
								if($_GET['acc']=='s'||$_GET['acc']=='b'){
									$proyecto->orden_publicacion($_GET['pro'],$_GET['acc'],$_GET['or'],$_GET['id']);
								}								
								
								if($proyecto->datos_tipologia())
								{
									$proyecto->guardar_tipologia();
								}
								
								$proyecto->formulario_tcp_tipologia('cargar');
								
								$proyecto->dibujar_encabezado_tipologia();
								
								$proyecto->mostrar_busqueda_tipologia();
						}
								
								
						break;
					}
					
		case 'ACABADO':{
						if($_GET['acc']=='MODIFICAR_ACABADO')
						{
							if($proyecto->datos_acabado())
							{
								$proyecto->modificar_acabado();
							}
							else
							{
								if(!($_POST))
								{
									$proyecto->cargar_datos_acabado();
								}
								$proyecto->formulario_tcp_acabado('cargar');
							}
						}
						else						
						{
							if($_GET['acc']=='ELIMINAR')
								{
									$proyecto->eliminar_acabado($_GET['aca_id']);
								}
								
								if($_GET['acc']=='s'||$_GET['acc']=='b'){
									$proyecto->orden_publicacion($_GET['pro'],$_GET['acc'],$_GET['or'],$_GET['id']);
								}								
								
								if($proyecto->datos_acabado())
								{
									$proyecto->guardar_acabado();
								}
								
								$proyecto->formulario_tcp_acabado('cargar');
								
								$proyecto->dibujar_encabezado_acabado();
								
								$proyecto->mostrar_busqueda_acabado();
						}
								
								
						break;
					}
					
		case 'FOTOS':{
					if($_GET['acc']=='MODIFICAR_FOTO'){

                        if($proyecto->datos_foto())
                        {
                            $proyecto->modificar_foto();
                        }
                        else
                        {
                            if(!($_POST))
                            {
                                $proyecto->cargar_datos_foto();
                            }
                            $proyecto->formulario_tcp_foto('cargar');
                        }
                    }else{
						if($_GET['acc']=='ELIMINAR')
						{
							$proyecto->eliminar_foto($_GET['proy_fot_id']);
						}
								
						if($_GET['acc']=='s'||$_GET['acc']=='b'){
							$proyecto->orden_publicacion($_GET['pro'],$_GET['acc'],$_GET['or'],$_GET['id']);
						}								
								
						if($proyecto->datos_foto())
						{
							$proyecto->guardar_foto();
						}
								
						$proyecto->formulario_tcp_foto('cargar');
								
						$proyecto->dibujar_encabezado_foto();
								
						$proyecto->mostrar_busqueda_foto();					
					}
				
								
								
		break;
		}
					
		case 'AREASOCIAL':{
						if($_GET['acc']=='MODIFICAR_AREASOCIAL')
						{
							if($proyecto->datos_areasocial())
							{
								$proyecto->modificar_areasocial();
							}
							else
							{
								if(!($_POST))
								{
									$proyecto->cargar_datos_areasocial();
								}
								$proyecto->formulario_tcp_areasocial('cargar');
							}
						}
						else						
						{
							if($_GET['acc']=='ELIMINAR')
								{
									$proyecto->eliminar_areasocial($_GET['are_soc_id']);
								}
								
								if($_GET['acc']=='s'||$_GET['acc']=='b'){
									$proyecto->orden_publicacion($_GET['pro'],$_GET['acc'],$_GET['or'],$_GET['id']);
								}								
								
								if($proyecto->datos_areasocial())
								{
									$proyecto->guardar_areasocial();
								}
								
								$proyecto->formulario_tcp_areasocial('cargar');
								
								$proyecto->dibujar_encabezado_areasocial();
								
								$proyecto->mostrar_busqueda_areasocial();
						}
								
								
						break;
					}

        case 'SLIDESHOW':{
            if($_GET['acc']=='MODIFICAR_FOTO'){

                if($proyecto->datos_foto())
                {
                    $proyecto->modificar_foto_slideshow();
                }
                else
                {
                    if(!($_POST))
                    {
                        $proyecto->cargar_datos_foto_slideshow();
                    }

                    $proyecto->formulario_tcp_foto_slideshow('cargar');
                }
            }else {
                if ($_GET['acc'] == 'ELIMINAR') {
                    $proyecto->eliminar_foto_slideshow($_GET['proy_fot_id']);
                }

                /*if ($_GET['acc'] == 's' || $_GET['acc'] == 'b') {
                    $proyecto->orden_publicacion_slideshow($_GET['pro'], $_GET['acc'], $_GET['or'], $_GET['id']);
                }*/

                if ($proyecto->datos_foto()) {
                    $proyecto->guardar_foto_slideshow();
                }

                $proyecto->formulario_tcp_foto_slideshow('ver');
                $proyecto->dibujar_encabezado_foto_slideshow();
                $proyecto->mostrar_busqueda_foto_slideshow();
            }
            break;
        }
					
		case 'ACCEDER':{	
						
						if($_GET['acc']<>"")
						{
							$proyecto->orden($_GET['enl'],$_GET['acc'],$_GET['or']);
						}
						$proyecto->dibujar_busqueda();
						break;
						}					
		
		//default: $producto->dibujar_busqueda();break;
	}
		
?>