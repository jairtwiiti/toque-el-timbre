<?php

class SLIDESHOW extends BUSQUEDA
{
	var $formulario;
	var $mensaje;

	function SLIDESHOW()
	{
		//permisos
		$this->ele_id = 170;
		$this->busqueda();

		if(!($this->verificar_permisos('AGREGAR')))
		{
			$this->ban_agregar=false;
		}
		//fin permisos

		$this->num_registros=14;
		$this->coneccion= new ADO();

		$this->link='gestor.php';
		$this->modulo='slideshow';
		$this->formulario = new FORMULARIO();
		$this->formulario->set_titulo('SLIDESHOW');
	}

	function dibujar_busqueda()
	{
		$this->formulario->dibujar_cabecera();
		$this->dibujar_listado();
	}

	function set_opciones()
	{
		$nun=0;

        if($this->verificar_permisos('VER'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='VER';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_search.png';
			$this->arreglo_opciones[$nun]["nombre"]='VER';
			$nun++;
		}

		if($this->verificar_permisos('MODIFICAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='MODIFICAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_edit.png';
			$this->arreglo_opciones[$nun]["nombre"]='MODIFICAR';
			$nun++;
		}

		if($this->verificar_permisos('ELIMINAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='ELIMINAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_drop.png';
			$this->arreglo_opciones[$nun]["nombre"]='ELIMINAR';
			$nun++;
		}

	}

	function dibujar_listado()
	{
		$sql="select * from slideshow ";
		$this->set_sql($sql, "order by orden desc");
		$this->set_opciones();
		$this->dibujar();
	}

	function dibujar_encabezado()
	{
		?>
			<tr>
				<th>Nombre</th>
				<th>Orden</th>
	            <th class="tOpciones" width="100px">Opciones</th>
			</tr>
		<?PHP
	}

	function mostrar_busqueda()
	{
		$conversor = new convertir();

		for($i=0;$i<$this->numero;$i++)
			{
				$objeto=$this->coneccion->get_objeto();
				echo '<tr>';
					echo "<td>";
						echo $objeto->nombre;
					echo "</td>";
					echo "<td>";
    ?>
						<center><a href="<?php echo $this->link.'?mod='.$this->modulo.'&tarea=ACCEDER&vi='.$objeto->id.'&acc=s&or='.$objeto->orden;?>"><img src="images/subir.png" border="0"></a><a href="<?php echo $this->link.'?mod='.$this->modulo.'&tarea=ACCEDER&vi='.$objeto->id.'&acc=b&or='.$objeto->orden;?>"><img src="images/bajarr.png" border="0"></a></center>
    <?php
					echo "</td>";
					echo "<td>";
						echo $this->get_opciones($objeto->id);
					echo "</td>";
				echo "</tr>";
				$this->coneccion->siguiente();
			}
	}

    function orden($banner,$accion,$ant_orden)
    {
        $conec= new ADO();

        if($accion=='s')
            $cad=" where orden > $ant_orden order by orden asc";
        else
            $cad=" where orden < $ant_orden order by orden desc";

        $consulta = "
		select
			id,orden
		from
			slideshow
		$cad
		limit 0,1
		";

        $conec->ejecutar($consulta);

        $num = $conec->get_num_registros();

        if($num > 0)
        {
            $objeto=$conec->get_objeto();

            $nu_orden=$objeto->orden;

            $id=$objeto->id;

            $consulta = "update slideshow set orden='$nu_orden' where id='$banner'";

            $conec->ejecutar($consulta);

            $consulta = "update slideshow set orden='$ant_orden' where id='$id'";

            $conec->ejecutar($consulta);
        }
    }

    function cargar_datos()
	{
		$conec=new ADO();
		$sql="select
                  *
			  from
			      slideshow
              where
				  id = '".$_GET['id']."'";

		$conec->ejecutar($sql);
		$objeto=$conec->get_objeto();

        $_POST['id']=$objeto->id;
		$_POST['nombre']=$objeto->nombre;
		$_POST['imagen']=$objeto->imagen;
		$_POST['orden']=$objeto->orden;
        $_POST['fecha']=$objeto->fecha;
	}

	function datos()
	{
		if($_POST)
		{
			//texto,  numero,  real,  fecha,  mail.
			$num=0;
			$valores[$num]["etiqueta"]="Titulo";
			$valores[$num]["valor"]=$_POST['titulo'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;

			$val=NEW VALIDADOR;
			$this->mensaje="";
			if($val->validar($valores))
			{
				return true;
			}else{
				$this->mensaje=$val->mensaje;
				return false;
			}
		}
        return false;
	}

	function formulario_tcp($tipo)
	{
		switch ($tipo)
		{
			case 'ver':{
						$ver=true;
						break;
						}
			case 'cargar':{
						$cargar=true;
						break;
						}
		}

        $url=$this->link.'?mod='.$this->modulo;
		$red=$url.'&tarea=ACCEDER';

        if(!($ver))
		{
			$url.="&tarea=".$_GET['tarea'];
		}

		if($cargar)
		{
			$url.='&id='.$_GET['id'];
		}

		$this->formulario->dibujar_tarea();

		if($this->mensaje<>"")
		{
            $this->formulario->mensaje('Error',$this->mensaje);
		}
        ?>

            <div id="Contenedor_NuevaSentencia">
			<form id="frm_siniestro" name="frm_siniestro" action="<?php echo $url;?>" method="POST"enctype="multipart/form-data">
            <div id="FormSent">
						<div class="Subtitulo">Datos</div>
						<div id="ContenedorSeleccion">

                            <!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">*</span>Titulo</div>
							   <div id="CajaInput">
								  <input type="text" class="caja_texto" name="titulo" id="titulo" size="40" maxlength="250"  value="<?php echo $_POST['nombre'];?>">
							   </div>
							</div>
							<!--Fin-->

                            <!--Inicio-->
                            <div id="ContenedorDiv">
                                <div class="Etiqueta" ><span class="flechas1">*</span>Imagen</div>
                                <div id="CajaInput">
                                    <img src="imagenes/slideshow/<?php echo $_POST['imagen']; ?>" width="150" /><br />
                                    <input type="file" name="fot_archivo" />
                                </div>
                            </div>
                            <!--Fin-->

							</div>

                        <div id="ContenedorDiv">
						   <div id="CajaBotones">
								<center>
								<?php
								if((!($ver)))
								{
									?>
									<input type="submit" class="boton" name="" value="Guardar">
									<input type="reset" class="boton" name="" value="Cancelar">
									<input type="button" class="boton" name="" value="Volver" onclick="javascript:location.href='<?php echo $red;?>';">
									<?php
								}
								else
								{
									?>
									<input type="button" class="boton" name="" value="Volver" onclick="javascript:location.href='<?php echo $red;?>';">
									<?php
								}
								?>
								</center>
						   </div>
						</div>
				</div>
			</form>
		</div>
		<?php
	}

	

	function insertar_tcp()
	{
		$conec= new ADO();

		$sql="select max(orden) as ultimo from slideshow";

		$conec->ejecutar($sql);
		$objeto=$conec->get_objeto();
		$orden=$objeto->ultimo + 1;



        $result=$this->subir_imagen($nombre_archivo,$_FILES['fot_archivo']['name'],$_FILES['fot_archivo']['tmp_name']);

        if(trim($result)<>'')
        {
            $this->formulario->ventana_volver($result,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER','Ir al listado','Error');
        }
        else
        {

            $sql="insert into slideshow(nombre,orden,fecha,imagen)
            values (
            '".$_POST['titulo']."',
            ".$orden.",
            '".date("Y-m-d H:i:s")."',
            '".$nombre_archivo."'
            )";

            $conec->ejecutar($sql);
            $mensaje='Slideshow Agregado Correctamente';
            $this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
        }
	}

	

	function modificar_tcp()
	{
		$conec= new ADO();

        $result=$this->subir_imagen($nombre_archivo,$_FILES['fot_archivo']['name'],$_FILES['fot_archivo']['tmp_name']);

        if(trim($result)<>'')
        {
            $this->formulario->ventana_volver($result,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER','Volver','Error');
        }
        else
        {

            $sql="update slideshow set
            nombre='".$_POST['titulo']."',
            imagen='".$nombre_archivo."'
            where id='".$_GET['id']."'";

            $conec->ejecutar($sql);
            $mensaje='Modificado Correctamente';
            $this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
        }


	}

	function formulario_confirmar_eliminacion()
	{
		$mensaje='Esta seguro de eliminar esta imagen?';
		$this->formulario->ventana_confirmacion($mensaje,$this->link."?mod=$this->modulo".'&tarea=ELIMINAR','id');
	}

	function eliminar_tcp(){
		$conec= new ADO();
		$sql="delete from slideshow where id='".$_POST['id']."'";
		$conec->ejecutar($sql);
		$mensaje='Eliminado Correctamente';
		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
	}

    function subir_imagen(&$nombre_imagen,$name,$tmp)
    {
        require_once('clases/upload.class.php');

        $nn=date('d_m_Y_H_i_s_').rand();
        $upload_class = new Upload_Files();
        $upload_class->temp_file_name = trim($tmp);
        $upload_class->file_name = $nn.substr(trim($name), -4, 4);
        $nombre_imagen=$upload_class->file_name;
        $upload_class->upload_dir = "imagenes/slideshow/";
        $upload_class->upload_log_dir = "imagenes/slideshow/upload_logs/";
        $upload_class->max_file_size = 1048576;
        $upload_class->ext_array = array(".jpg",".gif",".png");
        $upload_class->crear_thumbnail=false;
        $valid_ext = $upload_class->validate_extension();
        $valid_size = $upload_class->validate_size();
        $valid_user = $upload_class->validate_user();
        $max_size = $upload_class->get_max_size();
        $file_size = $upload_class->get_file_size();
        $file_exists = $upload_class->existing_file();

        if (!$valid_ext) {
            $result = "La Extension de este Archivo es invalida, Intente nuevamente por favor!";
        }elseif (!$valid_size){
            $result = "El Tama�o de este archivo es invalido, El maximo tama�o permitido es: $max_size y su archivo pesa: $file_size";
        }elseif ($file_exists){
            $result = "El Archivo Existe en el Servidor, Intente nuevamente por favor.";
        }else{
            $upload_file = $upload_class->upload_file_with_validation();

            if (!$upload_file) {

                $result = "Su archivo no se subio correctamente al Servidor.";
            }else{
                $result = "";
                require_once('clases/class.upload.php');
                $mifile='imagenes/slideshow/'.$upload_class->file_name;
                $handle = new upload($mifile);

                if ($handle->uploaded)
                {
                    /*$handle->image_resize          = true;
                    $handle->image_ratio           = true;
                    $handle->image_y               = 300;
                    $handle->image_x               = 300;
                    $handle->process('imagenes/slideshow/chica/');
                    if (!($handle->processed))
                    {
                        echo 'error : ' . $handle->error;
                    }*/
                }
            }
        }
        return $result;
    }



}

?>