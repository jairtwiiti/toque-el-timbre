<?php

class CERTIFICACION extends BUSQUEDA
{
	var $formulario;
	var $mensaje;
	
	function CERTIFICACION()
	{
		//permisos
		$this->ele_id=173;
		
		$this->busqueda();
		
		if(!($this->verificar_permisos('AGREGAR')))
		{
			$this->ban_agregar=false;
		}
		//fin permisos
		
		$this->num_registros=100;
		
		$this->coneccion= new ADO();
		
		/*$this->arreglo_campos[0]["nombre"]="usu_id";
		$this->arreglo_campos[0]["texto"]="ID Usuario";
		$this->arreglo_campos[0]["tipo"]="cadena";
		$this->arreglo_campos[0]["tamanio"]=40;*/

        $this->arreglo_campos[0]["nombre"]="usu_nombre";
        $this->arreglo_campos[0]["texto"]="Nombre de Usuario";
        $this->arreglo_campos[0]["tipo"]="cadena";
        $this->arreglo_campos[0]["tamanio"]=40;

        $this->arreglo_campos[1]["nombre"]="usu_email";
        $this->arreglo_campos[1]["texto"]="Email";
        $this->arreglo_campos[1]["tipo"]="cadena";
        $this->arreglo_campos[1]["tamanio"]=40;
		
		$this->link='gestor.php';
		
		$this->modulo='inmueble_certificacion';
		
		$this->formulario = new FORMULARIO();
		
		$this->formulario->set_titulo('CERTIFICACION DE INMOBILIARIAS');
		
		
	}
	
	
	function dibujar_busqueda()
	{
		
		$this->formulario->dibujar_cabecera();
		
		$this->dibujar_listado();
	}
	
		
	function set_opciones()
	{
				
		$nun=0;
		
		if($this->verificar_permisos('VER'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='VER';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_search.png';
			$this->arreglo_opciones[$nun]["nombre"]='VER';
			$nun++;
		}
		
		if($this->verificar_permisos('MODIFICAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='MODIFICAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_edit.png';
			$this->arreglo_opciones[$nun]["nombre"]='MODIFICAR';
			$nun++;
		}
		
		if($this->verificar_permisos('ELIMINAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='ELIMINAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_drop.png';
			$this->arreglo_opciones[$nun]["nombre"]='ELIMINAR';
			$nun++;
		}
		
		if($this->verificar_permisos('HABILITAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='HABILITAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/ok.png';
			$this->arreglo_opciones[$nun]["nombre"]='HABILITAR';
			$this->arreglo_opciones[$nun]["script"]="ok";
			$nun++;
		}
		
		if($this->verificar_permisos('APROBAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='APROBAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/ok.png';
			$this->arreglo_opciones[$nun]["nombre"]='APROBAR';
			$this->arreglo_opciones[$nun]["script"]="ok";
			$nun++;
		}
	}
	

	
	function aprobar(){
		$conec= new ADO();

		$id_user = $_GET['id'];
        $sql = "UPDATE usuario SET usu_certificado = 'Si' WHERE usu_id = " . $id_user;
        $conec->ejecutar($sql);
	}
	
	function dibujar_listado()
	{
		?>
		<script>
		function ejecutar_script(id,tarea){
				var txt = 'Esta seguro que desea certificar esta inmobiliaria?';
				
				$.prompt(txt,{ 
					buttons:{Aceptar:true, Cancelar:false},
					callback: function(v,m,f){
						
						if(v){
								location.href='gestor.php?mod=inmueble_certificacion&tarea='+tarea+'&id='+id;
						}
												
					}
				});
			}

		</script>
		<?php

		$sql="
		SELECT * FROM usuario
		where usu_tipo = 'Inmobiliaria' and usu_estado = 'Habilitado' and usu_certificado = 'No'
		";
		
		$this->set_sql($sql, ' ORDER BY usu_id DESC');
		$this->set_opciones();
		
		$this->dibujar();
		
	}
	
	function dibujar_encabezado()
	{
		?>
			<tr>
	        	<th>ID</th>
				<th>Nombre</th>
				<th>Empresa</th>
                <th>Telefono</th>
				<th>Email</th>
				<th>Certificado</th>
				<th class="tOpciones" width="100px">Opciones</th>
			</tr>
			
		<?PHP
	}
	
	function mostrar_busqueda()
	{
		$conversor = new convertir();
		
		for($i=0;$i<$this->numero;$i++)
			{
				
				$objeto=$this->coneccion->get_objeto();
				echo '<tr>';
									
					echo "<td>";
						echo $objeto->usu_id;
					echo "&nbsp;</td>";
					
					echo "<td>";
						echo $objeto->usu_nombre . " " . $objeto->usu_apellido;
					echo "&nbsp;</td>";
					
					echo "<td>";
						echo $objeto->usu_empresa;
					echo "&nbsp;</td>";
					
					echo "<td>";
						echo $objeto->usu_telefono . " - " . $objeto->usu_celular;
					echo "&nbsp;</td>";
					
					echo "<td>";
						echo $objeto->usu_email;
					echo "&nbsp;</td>";
					
					echo "<td>";
						echo $objeto->usu_certificado;
					echo "&nbsp;</td>";

					echo "<td>";
						echo $this->get_opciones($objeto->usu_id);
					echo "</td>";
				echo "</tr>";
				
				$this->coneccion->siguiente();
			}
	}
	
	function mostrar_detalle_pago(){
		$id_pago = $_GET['id'];
		
		$conec=new ADO();		
		$sql="
		SELECT s.ser_descripcion, s.ser_precio, ps.fech_ini, ps.fech_fin FROM pago_servicio ps 
		INNER JOIN servicios s ON s.ser_id = ps.ser_id
		WHERE ps.pag_id = '".$_GET['id']."'
		";
		$result = mysql_query($sql);
		
		?>
		
		<div id="Contenedor_NuevaSentencia">
			<div id="FormSent" style="width:650px">
				<div class="Subtitulo">Detalle de Pago - <?php echo $_GET['id']; ?></div>
				<div id="ContenedorSeleccion">
				
				<table width="100%" class="tablaLista">
				<thead>
				<tr>
					<th>Servicio</th>
					<th>Fecha Inicio</th>
					<th>Fecha Fin</th>
					<th>Precio</th>
				</tr>
				</thead>
		
		<?php 
		while($row = mysql_fetch_array($result))
		{
			
			?>
			
			<tr>
				<td><?php echo $row['ser_descripcion']?></td>
				<td><?php echo $row['fech_ini']?></td>
				<td><?php echo $row['fech_fin']?></td>
				<td><?php echo $row['ser_precio']; ?></td>
			</tr>
			
			<?php
			$total = $total + $row['ser_precio'];
		}
		?>
				</table>
				
				
				<h2>
					Total: BS. <?php echo $total; ?>
				</h2>
				
				
				</div>
			</div>
		</div>
		
	<?php 
	}
	
	
}
?>