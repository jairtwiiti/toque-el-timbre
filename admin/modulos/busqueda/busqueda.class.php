<?php

class CONTENIDO extends BUSQUEDA 
{
	var $formulario;
	var $mensaje;
	
	function CONTENIDO()
	{
		//permisos
		$this->ele_id=154;
		
		$this->busqueda();
		
		if(!($this->verificar_permisos('AGREGAR')))
		{
			$this->ban_agregar=false;
		}
		//fin permisos
		
		$this->num_registros=14;
		
		$this->coneccion= new ADO();
		
		$this->arreglo_campos[0]["nombre"]="con_titulo";
		$this->arreglo_campos[0]["texto"]="T�tulo";
		$this->arreglo_campos[0]["tipo"]="cadena";
		$this->arreglo_campos[0]["tamanio"]=40;
		
		$this->link='gestor.php';
		
		$this->modulo='busqueda';
		
		$this->formulario = new FORMULARIO();
		
		$this->formulario->set_titulo('BUSQUEDA');
		
		
	}
	
	
	function dibujar_busqueda()
	{
		
		$this->formulario->dibujar_cabecera();
		
		$this->dibujar_listado();
	}
	
		
	function set_opciones()
	{
				
		$nun=0;
		
		if($this->verificar_permisos('VER'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='VER';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_search.png';
			$this->arreglo_opciones[$nun]["nombre"]='VER';
			$nun++;
		}
		
		if($this->verificar_permisos('MODIFICAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='MODIFICAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_edit.png';
			$this->arreglo_opciones[$nun]["nombre"]='MODIFICAR';
			$nun++;
		}
		
		if($this->verificar_permisos('ELIMINAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='ELIMINAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_drop.png';
			$this->arreglo_opciones[$nun]["nombre"]='ELIMINAR';
			$nun++;
		}
	}
	
	function dibujar_listado()
	{
		$sql="SELECT * FROM busqueda_definida";
		
		$this->set_sql($sql,'');
		
		$this->set_opciones();
		
		$this->dibujar();
		
	}
	
	function dibujar_encabezado()
	{
		?>
			<tr>
	        	<th>Busqueda</th>
				<th>Titulo</th>
				<th>URL</th>
				<th class="tOpciones" width="100px">Opciones</th>
			</tr>
			
		<?PHP
	}
	
	function mostrar_busqueda()
	{
		$conversor = new convertir();
		
		for($i=0;$i<$this->numero;$i++)
			{
				
				$objeto=$this->coneccion->get_objeto();
				echo '<tr>';
									
					echo "<td>";
						echo $objeto->bus_criterio;
					echo "&nbsp;</td>";
					
					echo "<td>";
						echo $objeto->bus_titulo;
					echo "&nbsp;</td>";
					
					echo "<td>";
						echo $objeto->bus_seo;
					echo "&nbsp;</td>";
					
					echo "<td>";
						echo $this->get_opciones($objeto->bus_id);
					echo "</td>";
				echo "</tr>";
				
				$this->coneccion->siguiente();
			}
	}
	
	
	function cargar_datos()
	{
		$conec=new ADO();
		
		$sql="select * from busqueda_definida
				where bus_id = '".$_GET['id']."'";
		
		$conec->ejecutar($sql);
		
		$objeto=$conec->get_objeto();
		 	 	 	 	 	 	 	 	 
		$_POST['bus_criterio']=$objeto->bus_criterio;
		$_POST['bus_titulo']=$objeto->bus_titulo;
		
	}
	
	function datos()
	{
		if($_POST)
		{
			//texto,  numero,  real,  fecha,  mail.
			$num=0;
			$valores[$num]["etiqueta"]="Busqueda";
			$valores[$num]["valor"]=$_POST['bus_criterio'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			
			$valores[$num]["etiqueta"]="Busqueda";
			$valores[$num]["valor"]=$_POST['bus_titulo'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			
			
			$val=NEW VALIDADOR;
			
			$this->mensaje="";
			
			if($val->validar($valores))
			{
				return true;
			}
				
			else
			{
				$this->mensaje=$val->mensaje;
				return false;
			}
		}
			return false;
	}
	
	function formulario_tcp($tipo)
	{
				switch ($tipo)
				{
					case 'ver':{
								$ver=true;
								break;
								}
							
					case 'cargar':{
								$cargar=true;
								break;
								}
				}
				
				$url=$this->link.'?mod='.$this->modulo;
				
				$red=$url.'&tarea=ACCEDER';
				
				if(!($ver))
				{
					$url.="&tarea=".$_GET['tarea'];
				}
				
				if($cargar)
				{
					$url.='&id='.$_GET['id'];
				}

		
		    $this->formulario->dibujar_tarea('BUSQUEDA');
		
			if($this->mensaje<>"")
			{
				$this->formulario->dibujar_mensaje($this->mensaje);
			}
			?>
		<div id="Contenedor_NuevaSentencia">
			<form id="frm_sentencia" name="frm_sentencia" action="<?php echo $url;?>" method="POST" enctype="multipart/form-data">  
				<div id="FormSent">
				  
					<div class="Subtitulo">Datos</div>
						<div id="ContenedorSeleccion">
							
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">* </span>Busqueda</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto" name="bus_criterio" id="bus_criterio" maxlength="250"  size="60" value="<?php echo $_POST['bus_criterio'];?>">
							   </div>
							</div>
							<!--Fin-->
							
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">* </span>Titulo</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto" name="bus_titulo" id="bus_titulo" maxlength="250"  size="60" value="<?php echo $_POST['bus_titulo'];?>">
							   </div>
							</div>
							<!--Fin-->
							
						</div>
					
						<div id="ContenedorDiv">
						   <div id="CajaBotones">
								<center>
								<?php
								if(!($ver))
								{
									?>
									<input type="submit" class="boton" name="" value="Guardar">
									<input type="reset" class="boton" name="" value="Cancelar">
									<input type="button" class="boton" name="" value="Volver" onclick="javascript:location.href='<?php echo $red;?>';">
									<?php
								}
								else
								{
									?>
									<input type="button" class="boton" name="" value="Volver" onclick="javascript:location.href='<?php echo $red;?>';">
									<?php
								}
								?>
								</center>
						   </div>
						</div>
				</div>
			</form>
		</div>
		<?php
	}
	
	function verificar_urlseo($url_seo,$id=0){
		//echo 'el valor de id es: ' . $id . '<br>';
		$sql = "select * from busqueda_definida where bus_seo = '$url_seo'";
		if($id!=0){
			$sql = $sql . " and bus_id!=$id";
		}
		$conec= new ADO();
		$conec->ejecutar($sql,false);
		$rows = $conec->get_datos();
		$contador = count($rows) > 0 ? count($rows) : 0;
		
		/*echo $sql;
		exit(0);*/
		
		return $contador;  
	}
	
	function insertar_tcp()
	{
		include '../class.SEO.php';
		
		$seo = new SEO();
		$url_seo = $seo->scapeURL($_POST['bus_titulo']);
		$contador = $this->verificar_urlseo($url_seo);
		
		if($contador > 0){
			$url_seo = $url_seo . "-" . $contador;
		}
		
		$conec= new ADO();
		
		$sql="insert into busqueda_definida(bus_criterio,bus_titulo,bus_seo) values 
							('".$_POST['bus_criterio']."','". $_POST['bus_titulo'] ."','".$url_seo."')";

		$conec->ejecutar($sql);
		
		$mensaje='Busqueda Agregado Correctamente!!!';

		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
		
	}
	
	function modificar_tcp()
	{
		include '../class.SEO.php';
		$seo = new SEO();
		$url_seo = $seo->scapeURL($_POST['bus_criterio']);
			
		$id = $_GET['id'];
		$contador = $this->verificar_urlseo($url_seo,$id);
			
		if($contador > 0){
			$url_seo = $url_seo . "-" . $contador;
		}
		
		$conec= new ADO();	
		
		$sql="update busqueda_definida set 
						bus_criterio='".$_POST['bus_criterio']."',
						bus_titulo='".$_POST['bus_titulo']."',
						bus_seo='".$url_seo."'
						where bus_id = '".$_GET['id']."'";

		//echo $sql;
		$conec->ejecutar($sql);

		$mensaje='Busqueda Modificado Correctamente!!!';

		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
		
	}
	
	function formulario_confirmar_eliminacion()
	{
		
		$mensaje='Esta seguro de eliminar la busqueda?';
		
		$this->formulario->ventana_confirmacion($mensaje,$this->link."?mod=$this->modulo".'&tarea=ELIMINAR','bus_id');
	}
	
	function eliminar_tcp()
	{
		$conec= new ADO();
		
		$sql="delete from busqueda_definida where bus_id='".$_POST['bus_id']."'";
		
		$conec->ejecutar($sql);
		
		$mensaje='Busqueda Eliminada Correctamente!!!';
			
		
		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
	}
	
}
?>