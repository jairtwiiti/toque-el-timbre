<?php

class USU extends BUSQUEDA 
{
	var $formulario;
	var $mensaje;
	
	function USU()
	{
		//permisos
		$this->ele_id=22;
		
		$this->busqueda();
		
		if(!($this->verificar_permisos('AGREGAR')))
		{
			$this->ban_agregar=false;
		}
		//fin permisos
		
		$this->num_registros=14;
		
		$this->coneccion= new ADO();
		
		$this->arreglo_campos[0]["nombre"]="gru_descripcion";
		$this->arreglo_campos[0]["texto"]="Grupo";
		$this->arreglo_campos[0]["tipo"]="cadena";
		$this->arreglo_campos[0]["tamanio"]=25;
		
		$this->arreglo_campos[1]["nombre"]="per_nombre";
		$this->arreglo_campos[1]["texto"]="Nombre";
		$this->arreglo_campos[1]["tipo"]="cadena";
		$this->arreglo_campos[1]["tamanio"]=25;
		
		$this->arreglo_campos[2]["nombre"]="per_apellido";
		$this->arreglo_campos[2]["texto"]="Apellido";
		$this->arreglo_campos[2]["tipo"]="cadena";
		$this->arreglo_campos[2]["tamanio"]=25;
		
		$this->arreglo_campos[3]["nombre"]="usu_id";
		$this->arreglo_campos[3]["texto"]="Usuario";
		$this->arreglo_campos[3]["tipo"]="cadena";
		$this->arreglo_campos[3]["tamanio"]=25;
		
		$this->link='gestor.php';
		
		$this->modulo='usuario';
		
		$this->formulario = new FORMULARIO();
		
		$this->formulario->set_titulo('USUARIO');
	}
	
	function dibujar_busqueda()
	{
		
		$this->formulario->dibujar_cabecera();
		
		$this->dibujar_listado();
	}
	
	function set_opciones()
	{
		$nun=0;
		
		if($this->verificar_permisos('VER'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='VER';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_search.png';
			$this->arreglo_opciones[$nun]["nombre"]='VER';
			$nun++;
		}
		
		if($this->verificar_permisos('MODIFICAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='MODIFICAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_edit.png';
			$this->arreglo_opciones[$nun]["nombre"]='MODIFICAR';
			$nun++;
		}
		
		if($this->verificar_permisos('ELIMINAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='ELIMINAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_drop.png';
			$this->arreglo_opciones[$nun]["nombre"]='ELIMINAR';
			$nun++;
		}
		
	}
	
	function dibujar_listado()
	{
		$sql="SELECT 
				usu_id,per_nombre,per_apellido,gru_descripcion,usu_estado 
			  FROM 
				ad_usuario inner join gr_persona on(usu_per_id=per_id)
				inner join ad_grupo on (usu_gru_id=gru_id)
				";
		
		$this->set_sql($sql);
		
		$this->set_opciones();
		
		$this->dibujar();
		
	}
	
	function dibujar_encabezado()
	{
		?>
			<tr>
	        	<th>Grupo</th>
				<th>Persona</th>
				<th>Usuario</th>
				<th>Estado</th>
	            <th class="tOpciones" width="100px">Opciones</th>
			</tr>
		<?PHP
	}
	
	function mostrar_busqueda()
	{
		$conversor = new convertir();
		
		for($i=0;$i<$this->numero;$i++)
			{
				
				$objeto=$this->coneccion->get_objeto();
				echo '<tr>';
									
					echo "<td>";
						echo $objeto->gru_descripcion;
					echo "</td>";
					echo "<td>";
						echo $objeto->per_nombre." ".$objeto->per_apellido;
					echo "</td>";
					echo "<td>";
						echo $objeto->usu_id;
					echo "</td>";
					echo "<td>";
						if($objeto->usu_estado=='1') echo 'Habilitado'; else echo 'Deshabilitado';
					echo "</td>";
					echo "<td>";
						echo $this->get_opciones($objeto->usu_id);
					echo "</td>";
				echo "</tr>";
				
				$this->coneccion->siguiente();
			}
	}
	
	function cargar_datos()
	{
		$conec=new ADO();
		
		$sql="select * from ad_usuario
				where usu_id = '".$_GET['id']."'";
		
		$conec->ejecutar($sql);
		
		$objeto=$conec->get_objeto();
		
		$_POST['usu_id']=$objeto->usu_id;
		
		$_POST['usu_password']='123456789';
		
		$_POST['usu_per_id']=$objeto->usu_per_id;
		
		$_POST['usu_estado']=$objeto->usu_estado;
		
		$_POST['usu_gru_id']=$objeto->usu_gru_id;
		
		$fun=NEW FUNCIONES;		
		
		$_POST['usu_nombre_persona']=$fun->nombre($objeto->usu_per_id);
		
		
	}
	
	function datos()
	{
		if($_POST)
		{
			//texto,  numero,  real,  fecha,  mail.
			$num=0;
			$valores[$num]["etiqueta"]="Persona";
			$valores[$num]["valor"]=$_POST['usu_per_id'];
			$valores[$num]["tipo"]="numero";
			$valores[$num]["requerido"]=true;
			$num++;
			$valores[$num]["etiqueta"]="Grupo";
			$valores[$num]["valor"]=$_POST['usu_gru_id'];
			$valores[$num]["tipo"]="texto";
			$valores[$num]["requerido"]=true;
			$num++;
			$valores[$num]["etiqueta"]="Usuario";
			$valores[$num]["valor"]=$_POST['usu_id'];
			$valores[$num]["tipo"]="texto";
			$valores[$num]["requerido"]=true;
			$num++;
			$valores[$num]["etiqueta"]="Contraseņa";
			$valores[$num]["valor"]=$_POST['usu_password'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			$valores[$num]["etiqueta"]="Estado";
			$valores[$num]["valor"]=$_POST['usu_estado'];
			$valores[$num]["tipo"]="numero";
			$valores[$num]["requerido"]=true;
			
			$val=NEW VALIDADOR;
			
			$this->mensaje="";
			
			if($val->validar($valores))
			{
				return true;
			}
				
			else
			{
				$this->mensaje=$val->mensaje;
				return false;
			}
		}
			return false;
		
	}
	
	function formulario_tcp($tipo)
	{
				
		switch ($tipo)
		{
			case 'ver':{
						$ver=true;
						break;
						}
					
			case 'cargar':{
						$cargar=true;
						break;
						}
		}
		
		$url=$this->link.'?mod='.$this->modulo;
		
		$red=$url;
		
		if(!($ver))
		{
			$url.="&tarea=".$_GET['tarea'];
		}
		
		if($cargar)
		{
			$url.='&id='.$_GET['id'];
		}
		$page="'gestor.php?mod=usuario&tarea=AGREGAR&acc=Emergente'";
		$extpage="'persona'";
		$features="'left=325,width=600,top=200,height=420'";
		
		$this->formulario->dibujar_tarea('USUARIO');
		
		if($this->mensaje<>"")
		{
			$this->formulario->dibujar_mensaje($this->mensaje);
		}
		
			?>
			<div id="Contenedor_NuevaSentencia">
			<form id="frm_usuario" name="frm_usuario" action="<?php echo $url;?>" method="POST" enctype="multipart/form-data">  
				<div id="FormSent">
				  
					<div class="Subtitulo">Datos</div>
						<div id="ContenedorSeleccion">
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">*</span>Persona</div>
							   <div id="CajaInput">
								    <input name="usu_per_id" id="usu_per_id" readonly="readonly" type="hidden" class="caja_texto" value="<?php echo $_POST['usu_per_id']?>" size="2">
									<input name="usu_nombre_persona" id="usu_nombre_persona" readonly="readonly"  type="text" class="caja_texto" value="<?php echo $_POST['usu_nombre_persona']?>" size="40">
									<?php
									if($_GET['tarea']=='AGREGAR')
									{
									?>
										<img src="images/ir.png"  onclick="javascript:window.open(<?php echo $page;?>,<?php echo $extpage;?>,<?php echo $features;?>);">
									<?php
									}
									?>	
							   </div>

							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">*</span>Grupo</div>
							   <div id="CajaInput">
							   <select name="usu_gru_id" class="caja_texto">
							   <option value="">Seleccione</option>
							   <?php 		
								$fun=NEW FUNCIONES;		
								$fun->combo("select gru_id as id,gru_descripcion as nombre from ad_grupo",$_POST['usu_gru_id']);		
								?>
							   </select>
							   </div>
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">* </span>Usuario</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto" <?php if($_GET['tarea']=="MODIFICAR") echo 'readonly="readonly"';?>  name="usu_id" id="usu_id" size="25" value="<?php echo $_POST['usu_id'];?>">
							   </div>
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">* </span>Contraseņa</div>
							   <div id="CajaInput">
							   <input type="password" class="caja_texto" name="usu_password" id="usu_password" size="25" value="<?php echo $_POST['usu_password'];?>">
							   </div>
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">* </span>Estado</div>
							   <div id="CajaInput">
								    <select name="usu_estado" class="caja_texto">
									<option value="" >Seleccione</option>
									<option value="1" <?php if($_POST['usu_estado']=='1') echo 'selected="selected"'; ?>>Habilitado</option>
									<option value="0" <?php if($_POST['usu_estado']=='0') echo 'selected="selected"'; ?>>Deshabilitado</option>
									</select>
							   </div>
							</div>
							<!--Fin-->
							
						</div>
					
						<div id="ContenedorDiv">
						   <div id="CajaBotones">
								<center>
								<?php
								if(!($ver))
								{
									?>
									<input type="submit" class="boton" name="" value="Guardar">
									<input type="reset" class="boton" name="" value="Cancelar">
									<input type="button" class="boton" name="" value="Volver" onclick="javascript:location.href='<?php echo $red;?>';">
									<?php
								}
								else
								{
									?>
									<input type="button" class="boton" name="" value="Volver" onclick="javascript:location.href='<?php echo $red;?>';">
									<?php
								}
								?>
								</center>
						   </div>
						</div>
				</div>
			</form>
		</div>
		<?php
	}
	
	function emergente()
	{
		$this->formulario->dibujar_cabecera();
		
		$valor=trim($_POST['valor']);
	?>
			
			<script>
				function poner(id,valor)
				{
					opener.document.frm_usuario.usu_per_id.value=id;
					opener.document.frm_usuario.usu_nombre_persona.value=valor;
					window.close();
				}			
			</script>
			<br><center><form name="form" id="form" method="POST" action="gestor.php?mod=usuario&tarea=AGREGAR&acc=Emergente">
				<table align="center">
					<tr>
						<td class="txt_contenido" colspan="2" align="center">
							<input name="valor" type="text" class="caja_texto" size="30" value="<?php echo $valor;?>">
							<input name="Submit" type="submit" class="boton" value="Buscar">
						</td>
					</tr>
				</table>
			</form><center>
			<?php
			
			$conec= new ADO();
		
			if($valor<>"")
			{
				$sql="select per_id,per_nombre,per_apellido from gr_persona where per_nombre like '%$valor%' or per_apellido like '%$valor%'";
			}
			else
			{
				$sql="select per_id,per_nombre,per_apellido from gr_persona";
			}
			
			$conec->ejecutar($sql);
			
			$num=$conec->get_num_registros();
			
			echo '<table class="tablaLista" cellpadding="0" cellspacing="0">
					<thead>
					<tr>
						<th>
							Nombre
						</th>
						<th>
							Apellido
						</th>
						<th width="80" class="tOpciones">
							Seleccionar
						</th>
				</tr>
				</thead>
				<tbody>
			';
			
			for($i=0;$i<$num;$i++)
			{
				$objeto=$conec->get_objeto();
				
				echo '<tr>
						 <td>'.$objeto->per_nombre.'</td>
						 <td>'.$objeto->per_apellido.'</td>
						 <td><a href="javascript:poner('."'".$objeto->per_id."'".','."'".$objeto->per_nombre.' '.$objeto->per_apellido."'".');"><center><img src="images/select.png" border="0" width="20px" height="20px"></center></a></td>
					   </tr>	 
				';
				
				$conec->siguiente();
			}
			
			?>
			</tbody></table>
			<?php
	}
	
	function insertar_tcp()
	{
		$verificar=NEW VERIFICAR;
		
		$parametros[0]=array('usu_id');
		$parametros[1]=array($_POST['usu_id']);
		$parametros[2]=array('ad_usuario');
		
		if($verificar->validar($parametros))
		{
			$conec= new ADO();
		
			$sql="insert into ad_usuario (usu_id,usu_password,usu_per_id,usu_estado,usu_gru_id) values ('".$_POST['usu_id']."','".md5($_POST['usu_password'])."','".$_POST['usu_per_id']."','".$_POST['usu_estado']."','".$_POST['usu_gru_id']."')";
			
			$conec->ejecutar($sql);
			
			$mensaje='Usuario Agregado Correctamente';
		}
		else
		{
			$mensaje='El usuario no puede ser agregado, por que ya existe una persona con ese nombre de usuario.';
		}
		
		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo);
	}
	
	function modificar_tcp()
	{
		$conec= new ADO();
		
		$codigo="";
	 	 	 	 	
		if($_POST['usu_password']<>"123456789")
		{
			$sql="update ad_usuario set 
								usu_password='".md5($_POST['usu_password'])."',
								usu_per_id='".$_POST['usu_per_id']."',
								usu_estado='".$_POST['usu_estado']."',
								usu_gru_id='".$_POST['usu_gru_id']."'
								where usu_id='".$_GET['id']."'";
		}
		else
		{
			$sql="update ad_usuario set 
								usu_per_id='".$_POST['usu_per_id']."',
								usu_estado='".$_POST['usu_estado']."',
								usu_gru_id='".$_POST['usu_gru_id']."'
								where usu_id='".$_GET['id']."'";
		}
		
		
		
		$conec->ejecutar($sql);
		
		$mensaje='Usuario Modificado Correctamente';
		
		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo);
	}
	
	function formulario_confirmar_eliminacion()
	{
		
		$mensaje='Esta seguro de eliminar el usuario?';
		
		$this->formulario->ventana_confirmacion($mensaje,$this->link."?mod=$this->modulo",'usu_id');
	}
	
	function eliminar_tcp()
	{
		$verificar=NEW VERIFICAR;
		
		$parametros[0]=array('log_usu_id');
		$parametros[1]=array($_POST['usu_id']);
		$parametros[2]=array('ad_logs');
		
		if($verificar->validar($parametros))
		{
			$conec= new ADO();
		
			$sql="delete from ad_usuario where usu_id='".$_POST['usu_id']."'";
			 
			$conec->ejecutar($sql);
			
			$mensaje='Usuario Eliminado Correctamente.';
		}
		else
		{
			$mensaje='El usuario no puede ser eliminado, por que ya realizo varias acciones en el sistema.';
		}
		
		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo);
	}

	
}
?>