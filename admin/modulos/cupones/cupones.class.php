<?php

class CONTENIDO extends BUSQUEDA 
{
	var $formulario;
	var $mensaje;
	var $obj_languages;
	
	function CONTENIDO()
	{
		//$this->obj_languages = new MOD_LANGUAGE();
		
		//permisos
		$this->ele_id=178;
		
		$this->busqueda();
		
		if(!($this->verificar_permisos('AGREGAR')))
		{
			$this->ban_agregar=false;
		}
		//fin permisos
		
		$this->num_registros=30;
		
		$this->coneccion= new ADO();
		
		//$this->arreglo_campos[0]["nombre"]="con_title";
		//$this->arreglo_campos[0]["texto"]="Title";
		//$this->arreglo_campos[0]["tipo"]="cadena";
		//$this->arreglo_campos[0]["tamanio"]=40;
		
		$this->link='gestor.php';
		
		$this->modulo='cupones';
		
		$this->formulario = new FORMULARIO();
		
		$this->formulario->set_titulo('CUPONES DE DESCUENTO');
		
		
	}
	
	
	function dibujar_busqueda()
	{
		
		$this->formulario->dibujar_cabecera();
		
		$this->dibujar_listado();
	}
	
		
	function set_opciones()
	{
				
		$nun=0;
		
		if($this->verificar_permisos('VER'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='VER';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_search.png';
			$this->arreglo_opciones[$nun]["nombre"]='VIEW';
			$nun++;
		}
		
		if($this->verificar_permisos('MODIFICAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='MODIFICAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_edit.png';
			$this->arreglo_opciones[$nun]["nombre"]='UPDATE';
			$nun++;
		}
		
		if($this->verificar_permisos('FOTOS'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='FOTOS';
			$this->arreglo_opciones[$nun]["imagen"]='images/fotos.png';
			$this->arreglo_opciones[$nun]["nombre"]='PICTURES';
			$nun++;
		}
		
		if($this->verificar_permisos('ELIMINAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='ELIMINAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_drop.png';
			$this->arreglo_opciones[$nun]["nombre"]='DELETE';
			$nun++;
		}
	}
	
	function dibujar_listado()
	{
		$sql = "
		SELECT * FROM cupon
		";
		$this->set_sql($sql,'');
		
		$this->set_opciones();
		
		$this->dibujar();
		
	}
	
	function dibujar_encabezado()
	{
		?>
			<tr>
	        	<th>Title</th>
				<th class="tOpciones" width="100px">Options</th>
			</tr>
			
		<?PHP
	}
	
	function mostrar_busqueda()
	{
		$conversor = new convertir();
		//$languages = $this->obj_languages->obtener_idiomas();
		
		
		for($i=0;$i<$this->numero;$i++)
			{
				
				$objeto=$this->coneccion->get_objeto();
				echo '<tr>';
									
					echo "<td>";
						//echo $this->obj_languages->obtener_traduccion($objeto->con_title, $objeto->con_lang);
						echo $objeto->titulo;
					echo "&nbsp;</td>";
					echo "<td>";
						echo $this->get_opciones($objeto->id);
					echo "</td>";
				echo "</tr>";
				
				$this->coneccion->siguiente();
			}
	}
	
	
	function cargar_datos()
	{
		$conec=new ADO();
		
		$sql="select * from content
				where con_id = '".$_GET['id']."'";
		
		$conec->ejecutar($sql);
		
		$objeto=$conec->get_objeto();
		 	 	 	 	 	 	 	 	 
		$_POST['con_id']=$objeto->con_id;
		$_POST['con_title'] = $objeto->con_title;
		$_POST['con_description'] = $objeto->con_description;
		$_POST['con_lang']=$objeto->con_lang;
		$_POST['con_seo']=$objeto->con_seo;
	}
	
	function datos()
	{
		if($_POST)
		{
			//texto,  numero,  real,  fecha,  mail.
			$num=0;
			$valores[$num]["etiqueta"]="Title";
			$valores[$num]["valor"]=$_POST['con_title'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			
			
			$val=NEW VALIDADOR;
			
			$this->mensaje="";
			
			if($val->validar($valores))
			{
				return true;
			}
				
			else
			{
				$this->mensaje=$val->mensaje;
				return false;
			}
		}
			return false;
	}
	
	function formulario_tcp($tipo)
	{
				switch ($tipo)
				{
					case 'ver':{
								$ver=true;
								break;
								}
							
					case 'cargar':{
								$cargar=true;
								break;
								}
				}
				
				$url=$this->link.'?mod='.$this->modulo;
				
				$red=$url.'&tarea=ACCEDER';
				
				if(!($ver))
				{
					$url.="&tarea=".$_GET['tarea'];
				}
				
				if($cargar)
				{
					$url.='&id='.$_GET['id'];
				}

		
		    $this->formulario->dibujar_tarea('CONTENIDO');
		
			if($this->mensaje<>"")
			{
				$this->formulario->dibujar_mensaje($this->mensaje);
			}
			?>
		<div id="Contenedor_NuevaSentencia">
			<form id="frm_sentencia" name="frm_sentencia" action="<?php echo $url;?><?php echo $_GET["acc"] != "" ? "&acc=".$_GET["acc"] : ""; ?>" method="POST" enctype="multipart/form-data">  
				<div id="FormSent">
				  
					<div class="Subtitulo">Datos</div>
						<div id="ContenedorSeleccion">
							
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">* </span>Title</div>
							   <div id="CajaInput">
							   <?php 
							   $idioma = $_GET["tarea"] == "TRANSLATE" ? $_GET["acc"] : $_POST['con_lang'];
							   ?>
							   <input type="text" class="caja_texto" name="con_title" id="con_title" maxlength="250"  size="60" value="<?php echo $this->obj_languages->obtener_traduccion($_POST['con_title'], $idioma); ?>">
							   </div>
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Description</div>
							   <div id="CajaInput">
							   <!-- <textarea class="area_texto" name="con_description" id="con_description" cols="55" rows="4"><?php echo $this->obj_languages->obtener_traduccion($_POST['con_description'], $idioma); ?></textarea> -->
							   
							   <?php
							    include_once("js/fckeditor/fckeditor.php");
								$pagina = $this->obj_languages->obtener_traduccion($_POST['con_description'], $idioma);
								$oFCKeditor = new FCKeditor('con_description') ;
								$oFCKeditor->BasePath = 'js/fckeditor/';
								$oFCKeditor->Width  = '700' ;
								$oFCKeditor->Height = '270' ;
								//$oFCKeditor->ToolbarSet = 'Basic';
								$oFCKeditor->Value = $pagina;
								$oFCKeditor->Create() ;
							   ?>
							   
							   
							   </div>
							</div>
							<!--Fin-->
							<!--Inicio-->
							<?php if($_GET["tarea"] == "TRANSLATE" or $_GET["tarea"] == "MODIFICAR"){ ?>
							<input type="hidden" name="con_lang" value="<?php echo $_GET['acc'] != "" ? $_GET['acc'] : $_POST["con_lang"]; ?>" />							
                            <?php }else{ ?>
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">*</span>Translate</div>
							   <div id="CajaInput">
							   <select name="con_lang" class="caja_texto">
							   <option value="">Seleccione</option>
							   <?php 		
								$fun=NEW FUNCIONES;		
								$fun->combo("select lan_id as id,lan_name as nombre from language where lan_state = 'Active'",$_POST['con_lang']);		
								?>
							   </select>
							   </div>
							</div>							
							<?php } ?>
							<!--Fin-->								
							<input type="hidden" name="con_title_key" value="<?php echo $_POST["con_title"]; ?>" />
							<input type="hidden" name="con_description_key" value="<?php echo $_POST["con_description"]; ?>" />
							<input type="hidden" name="con_seo_key" value="<?php echo $_POST["con_seo"]; ?>" />
							
						</div>
					
						<div id="ContenedorDiv">
						   <div id="CajaBotones">
								<center>
								<?php
								if(!($ver))
								{
									?>
									<input type="submit" class="boton" name="" value="Save">
									<input type="button" class="boton" name="" value="Volver" onclick="javascript:location.href='<?php echo $red;?>';">
									<?php
								}
								else
								{
									?>
									<input type="button" class="boton" name="" value="Back" onclick="javascript:location.href='<?php echo $red;?>';">
									<?php
								}
								?>
								</center>
						   </div>
						</div>
				</div>
			</form>
		</div>
		<?php
	}
	
	function insertar_tcp()
	{
		$conec= new ADO();
		
		/*$sql="insert into content(con_title,con_description) values 
							('".$_POST['con_title']."','".$_POST['con_description']."')";

		$conec->ejecutar($sql);*/
		
		$title_key = $this->obj_languages->generar_keys_traduccion("content");
		$content_key = $this->obj_languages->generar_keys_traduccion("content");
		$seo_key = $this->obj_languages->generar_keys_traduccion("content");
		
		$sql="insert into content(con_title,con_description,con_lang,con_seo) values 
							('".$title_key."','".$content_key."','".$_POST['con_lang']."','".$seo_key."')";
		
		mysql_query($sql);
		$id_key = mysql_insert_id();
		
		
		$this->obj_languages->insertar_traduccion($_POST['con_title'], $title_key, $id_key, $_POST['con_lang'], 2);
		$this->obj_languages->insertar_traduccion($_POST['con_description'], $content_key, $id_key, $_POST['con_lang'], 2);
		$this->obj_languages->insertar_traduccion($this->seo_url($_POST['con_title']).".html", $seo_key, $id_key, $_POST['con_lang'], 2);		
		
		$mensaje='Content added successfully!!!';

		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
		
	}
	
	function insertar_translate_tcp()
	{
		$conec= new ADO();
		
		/*$sql="insert into content(con_title,con_description) values 
							('".$_POST['con_title']."','".$_POST['con_description']."')";

		$conec->ejecutar($sql);*/
		
		$title_key = $_POST["con_title_key"];
		$content_key = $_POST["con_description_key"];
		$seo_key = $_POST["con_seo_key"];
		
		$id_key = $_GET["id"];
		
		$this->obj_languages->insertar_traduccion($_POST['con_title'], $title_key, $id_key, $_POST['con_lang'], 2);
		$this->obj_languages->insertar_traduccion($_POST['con_description'], $content_key, $id_key, $_POST['con_lang'], 2);	
		$this->obj_languages->insertar_traduccion($this->seo_url($_POST['con_title']).".html", $seo_key, $id_key, $_POST['con_lang'], 2);		
		
		$mensaje='Added successfully!!!';

		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
		
	}
	
	function modificar_tcp()
	{
		$conec= new ADO();	
		
		/*$sql="update content set 
						con_title='".$_POST['con_title']."',
						con_description='".$_POST['con_description']."'
						where con_id = '".$_GET['id']."'";*/
		//$conec->ejecutar($sql);
		
		$this->obj_languages->actualizar_traduccion($_POST['con_title'], $_POST['con_title_key'], $_GET['id'], $_POST["con_lang"]);
		$this->obj_languages->actualizar_traduccion($_POST['con_description'], $_POST['con_description_key'], $_GET['id'], $_POST["con_lang"]);
		//$this->obj_languages->actualizar_traduccion($this->seo_url($_POST['con_title']).".html", $_POST['con_seo_key'], $_GET['id'], $_POST["con_lang"]);
		
		$mensaje='Content updated correctly!!!';
		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
		
	}
	
	function formulario_confirmar_eliminacion()
	{
		$mensaje='Really remove this content?';
		$this->formulario->ventana_confirmacion($mensaje,$this->link."?mod=$this->modulo".'&tarea=ELIMINAR','con_id');
	}
	
	function eliminar_tcp()
	{
		$conec= new ADO();
		// ID MODULO CONTENT = 2
		//$this->eliminar_traducciones_contentido($_POST["con_id"]);
		
		$sql="
		UPDATE content SET
		con_state = 'Removed'
		WHERE con_id = ".$_POST['con_id'];
		$conec->ejecutar($sql);
		
		$mensaje='Content removed correctly!!!';
		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
	}
	
	
	function seo_url($texto){
		$spacer = "-";
		$texto = trim($texto);
		$texto = strtolower($texto);
		$texto = trim(ereg_replace("[^ A-Za-z0-9_]", " ", $texto));
		$texto = ereg_replace("[ \t\n\r]+", "-", $texto);
		$texto = str_replace(" ", $spacer, $texto);
		$texto = ereg_replace("[ -]+", "-",$texto);
		return $texto;
	}
	
	function insertar_cupon_usuario($id_cupon, $id_usuario){
		$conec= new ADO();
		
		$sql_cupon_usuario = "
			insert into cupon_usuario(id_cupon,id_usuario,cobrado) values(
			".$id_cupon.",
			".$id_usuario.",
			'No'
			)
		";
		$conec->ejecutar($sql_cupon_usuario);
	}

    function generar_cupon(){
        $conec = new ADO();
        $sw_cupon = true;

        while($sw_cupon){
            $cupon = $this->codigoAlfanumerico(7,true);
            $sql="
            SELECT c.id FROM cupon c
            where c.cupon = '$cupon'
            ";
            $conec->ejecutar($sql);
            $num = $conec->get_num_registros();
            $sw_cupon = $num > 0 ? true : false;
        }
        return $cupon;
    }

    public function codigoAlfanumerico($length=10,$uc=FALSE,$n=TRUE,$sc=FALSE,$min=FALSE) {
        $source = '';
        if($min==1) $source .= 'abcdefghijklmnopqrstuvwxyz';;
        if($uc==1) $source .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        if($n==1) $source .= '1234567890';
        if($sc==1) $source .= '|@#~$%()=^*+[]{}-_';
        if($length>0){
            $rstr = "";
            $source = str_split($source,1);
            for($i=1; $i<=$length; $i++){
                mt_srand((double)microtime() * 1000000);
                $num = mt_rand(1,count($source));
                $rstr .= $source[$num-1];
            }
        }
        return $rstr;
    }
	
	function insertar_cupon($titulo,$cupon,$descuento,$fecha_ini,$fecha_fin){
		$conec = new ADO();
        $conec2 = new ADO();
        $fecha_ini = date('Y-m-d', strtotime($fecha_ini));
        $fecha_fin = date('Y-m-d', strtotime($fecha_fin));

        $sql_cupon = "
			insert into cupon(titulo,cupon,descuento,fecha_ini,fecha_fin,estado) values(
			'".$titulo."',
			'".$cupon."',
			".$descuento.",
			'".$fecha_ini."',
			'".$fecha_fin."',
			'Activo'
			)
		";
        $conec2->ejecutar($sql_cupon);

		$sql="
		SELECT c.id FROM cupon c
		ORDER BY c.id desc
		LIMIT 1
		";
		$conec->ejecutar($sql);
		$row = $conec->get_objeto();
		return !empty($row->id) ? $row->id : 0;
	}

	function list_select_packages(){
		$conec= new ADO();
		
		if(!empty($_POST['ids'])){

            $titulo = $_POST['titulo'];
            $cupon = $this->generar_cupon();
            $descuento = $_POST['descuento'];
            $fecha_ini = $_POST['start_date'];
            $fecha_fin = $_POST['end_date'];
            $id_cupon = $this->insertar_cupon($titulo,$cupon,$descuento,$fecha_ini,$fecha_fin);
            foreach($_POST['ids'] as $id_user){
                $this->insertar_cupon_usuario($id_cupon,$id_user);
            }
            $message = "Cupon <b>$cupon</b> registrado y asignado correctamente.";
		}
		
		$this->formulario->dibujar_tarea('');
		?>
		<script>
		var iframe = window.parent.document.getElementById("contenido").contentWindow;
		var height_content = 0;
		$( document ).ready(function() {
			$(".select_package option").click(function() {
				var valor = $(this).val();
				$.ajax({
					data:  {"grupo" : valor},
					url:   'gestor.php?mod=cupones&tarea=AGREGAR',
					type:  'post',
					beforeSend: function () {},
					success:  function (response) {
						$(".iframeContenido", window.parent.document).attr("scrolling", "Si");
						$(".list_all_subscription_per_module").html(response);
					}
				});
				setInterval(function(){
					height_content = $(".list_all_subscription_per_module").height();
					$(".iframeContenido", window.parent.document).css({height: height_content + 120});
				},300);
			});

			<?php if($_GET['module'] != "" || $_POST['select_package'] != ""): ?>
			<?php $_GET['module'] = !empty($_POST['select_package']) ? $_POST['select_package'] : $_GET['module']; ?>
				$.ajax({
					data:  {"grupo" : "<?php echo $_GET['module']; ?>"},
					url:   'gestor.php?mod=cupones&tarea=AGREGAR',
					type:  'post',
					beforeSend: function () {},
					success:  function (response) {
						$(".iframeContenido", window.parent.document).attr("scrolling", "Si");
						$(".list_all_subscription_per_module").html(response);
					}
				});
				setInterval(function(){
					height_content = $(".list_all_subscription_per_module").height();
					$(".iframeContenido", window.parent.document).css({height: height_content + 120});
				},300);
			<?php endif; ?>
			
		});
		</script>
		<script>
		$(function() {
		    $(document).on('click', '.id_checkbox', function() {
		    	var valor = $(this).is(':checked');
				  if(valor){
					  $(this).parent().parent().css("background", "#ccc");
				  }else{
					  $(this).parent().parent().css("background", "none");
				  }
		    });
		});
		</script>
		<style>
		td{
			border-right:1px #999 solid;
			border-bottom:1px #999 solid;
			padding:4px 8px;
			font-size:13px;
		}
		table{
			border-left:1px #999 solid;
			border-top:1px #999 solid;
		}
		</style>
		<div id="Contenedor_NuevaSentencia" style="padding-top:30px;">
			<div style="font-size:14px; padding:0px 0 20px 0;"><?php echo $message; ?></div>
			<form action="#" method="post">
				<div>
					<!--Inicio-->
					<div id="ContenedorDiv">
					   <div class="Etiqueta" ><span class="flechas1">* </span>Grupo de Descuento</div>
					   <div id="CajaInput">
							<select class="select_package" name="select_package">
								<option>Seleccionar</option>
								<option value="Vencidos">Anuncios Vencidos</option>
								<option value="Por_Vencer">Auncios por vencer</option>
							</select>
					   </div>
					</div>
					<!--Fin-->

                    <!--Inicio-->
                    <div id="ContenedorDiv">
                        <div class="Etiqueta" > <span class="flechas1">* </span>Titulo</div>
                        <div id="CajaInput">
                            <input class="caja_texto" name="titulo" id="titulo" style="width: 300px;" type="text">
                        </div>
                    </div>
                    <!--Fin-->

                    <!--Inicio-->
                    <div id="ContenedorDiv">
                        <div class="Etiqueta" > <span class="flechas1">* </span>Descuento</div>
                        <div id="CajaInput">
                            <input class="caja_texto" name="descuento" id="descuento" type="text">
                        </div>
                    </div>
                    <!--Fin-->

					<!--Inicio-->
					<div id="ContenedorDiv">
					   <div class="Etiqueta" > <span class="flechas1">* </span>Fecha Inicio</div>
						 <div id="CajaInput">
							<input readonly="readonly" class="caja_texto" name="start_date" id="start_date" size="12" type="text">
						</div>		
					</div>
					<!--Fin-->

                    <!--Inicio-->
                    <div id="ContenedorDiv">
                        <div class="Etiqueta" > <span class="flechas1">* </span>Fecha Fin</div>
                        <div id="CajaInput">
                            <input readonly="readonly" class="caja_texto" name="end_date" id="end_date" size="12" type="text">
                        </div>
                    </div>
                    <!--Fin-->
					
					<!--Inicio-->
					<div id="ContenedorDiv">
					   <div class="Etiqueta" ></div>
					   <div id="CajaInput">
							<input type="submit" value="Asignar Cupones" />
					   </div>
					</div>
					<!--Fin-->
				</div>
				<script>
				$(function() {
					var dateFormat = 'dd.mm.yy';
					$("#start_date").datepicker({
						minDate: new Date(), 
						showButtonPanel: true,
						dateFormat: dateFormat 
					});
                    $("#end_date").datepicker({
                        minDate: new Date(),
                        showButtonPanel: true,
                        dateFormat: dateFormat
                    });
					
				});
				</script>
				
				<div class="list_all_subscription_per_module"></div>
			</form>
		</div>
		<?php 
		
	}
	
	function lista_de_anuncios(){
		$conec= new ADO();
        $grupo = $_POST['grupo'];
        switch($grupo){
            case "Vencidos":
                $this->get_anuncios_vencidos_no_vencidos($grupo);
                break;
            case "Por_Vencer":
                $this->get_anuncios_vencidos_no_vencidos($grupo);
                break;
        }
	}

    function get_anuncios_vencidos_no_vencidos($grupo){
        $conec= new ADO();
        if($grupo == "Vencidos") {
            $sql = "
            SELECT inm_id, inm_nombre, inm_precio, dep_nombre, cat_nombre, for_descripcion, COUNT(inm_id) as anuncios_vencidos, pub_vig_ini, pub_vig_fin, usu_id, usu_email, CONCAT(usu_nombre,' ',usu_apellido) AS usuario, usu_tipo, usu_telefono FROM inmueble
            INNER JOIN ciudad ON (inm_ciu_id = ciu_id)
            INNER JOIN departamento ON (ciu_dep_id = dep_id)
            INNER JOIN categoria ON (inm_cat_id = cat_id)
            INNER JOIN forma ON (inm_for_id = for_id)
            INNER JOIN publicacion ON (pub_inm_id = inm_id)
            INNER JOIN usuario ON (usu_id = pub_usu_id)
            INNER JOIN moneda ON (inm_mon_id = mon_id)
            INNER JOIN pagos p ON (p.`pag_pub_id` = pub_id AND p.pag_estado = 'Pagado')
            INNER JOIN pago_servicio ps ON (ps.`pag_id` = p.`pag_id`)
            where pub_estado = 'Aprobado'
            AND CURDATE() > pub_vig_fin
            GROUP BY usu_id
            order by pub_vig_fin desc
            ";
        }elseif($grupo == "Por_Vencer"){
            $sql="
            SELECT inm_id, inm_nombre, inm_precio, dep_nombre, cat_nombre, for_descripcion, COUNT(inm_id) as anuncios_vencidos, pub_vig_ini, pub_vig_fin, usu_id, usu_email, CONCAT(usu_nombre,' ',usu_apellido) AS usuario, usu_tipo, usu_telefono FROM inmueble
            INNER JOIN ciudad ON (inm_ciu_id = ciu_id)
            INNER JOIN departamento ON (ciu_dep_id = dep_id)
            INNER JOIN categoria ON (inm_cat_id = cat_id)
            INNER JOIN forma ON (inm_for_id = for_id)
            INNER JOIN publicacion ON (pub_inm_id = inm_id)
            INNER JOIN usuario ON (usu_id = pub_usu_id)
            INNER JOIN moneda ON (inm_mon_id = mon_id)
            INNER JOIN pagos p ON (p.`pag_pub_id` = pub_id AND p.pag_estado = 'Pagado')
            INNER JOIN pago_servicio ps ON (ps.`pag_id` = p.`pag_id`)
            where pub_estado = 'Aprobado'
            AND CURDATE() <= pub_vig_fin
            GROUP BY usu_id
            order by pub_vig_fin asc
            ";
        }

        $conec->ejecutar($sql);
        $result = $conec->get_datos();
        ?>
        <br style="clear:both" />
        <br style="clear:both" />
        <table width="100%" cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td width="4%"><strong>Select</strong></td>
                <td width="12%"><strong>Ultimo Vencido</strong></td>
                <td><strong>Anuncios</strong></td>
                <td><strong>Usuario</strong></td>
                <td><strong>Email</strong></td>
                <td><strong>Tipo</strong></td>
                <td><strong>Opciones</strong></td>
            </tr>
            <?php foreach($result as $row): ?>
                <tr>
                    <td style="text-align:center;"><input type="checkbox" name="ids[]" class="id_checkbox" value="<?php echo $row['usu_id']; ?>" /></td>
                    <td><?php echo !empty($row['pub_vig_fin']) ? date("d/m/Y", strtotime($row['pub_vig_fin'])) : '-'; ?></td>
                    <td><?php
                        echo $row['anuncios_vencidos']." Anuncios vencidos";
                    ?></td>
                    <td><?php echo utf8_encode($row['usuario']); ?></td>
                    <td><?php echo $row['usu_email']; ?></td>
                    <td><?php echo $row['usu_tipo']; ?></td>
                    <td><a href="<?php echo $this->link.'?mod='.$this->modulo.'&tarea=VER&id='.$row['usu_id'];?>">Cupones</a></td>
                </tr>
            <?php endforeach; ?>
        </table>

        <br style="clear:both" />
        <br style="clear:both" />
    <?php
    }
	
	function list_coupons($user_id){
		$conec= new ADO();
		
		$sql="
		SELECT CONCAT(u.`usu_nombre`,' ',u.`usu_apellido`) AS usuario, c.`cupon`, c.`descuento`, c.`fecha_ini`, c.`fecha_fin`, cu.`cobrado`, cu.`fecha_cobrado` FROM usuario u
        INNER JOIN cupon_usuario cu ON (cu.`id_usuario` = u.`usu_id`)
        INNER JOIN cupon c ON (c.`id` = cu.`id_cupon`)
        WHERE u.`usu_id` = '$user_id'
        ORDER BY cu.`cobrado` ASC
		";
		$conec->ejecutar($sql);
		$result = $conec->get_datos();
		
		$this->formulario->dibujar_tarea('');
		?>
		<style>
		td{
			border-right:1px #999 solid;
			border-bottom:1px #999 solid;
			padding:4px 8px;
			font-size:13px;
		}
		table{
			border-left:1px #999 solid;
			border-top:1px #999 solid;
		}
		</style>
		<div id="Contenedor_NuevaSentencia" style="padding-top:30px;">
				<br style="clear:both" />
				<br style="clear:both" />
				<h1 style="margin-bottom:20px;">Cupones Asignados a Usuario</h1>
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td><strong>Usuario</strong></td>
				    <td><strong>Cupon</strong></td>
					<td><strong>Descuento</strong></td>
				    <td><strong>Fecha Inicio</strong></td>
				    <td><strong>Fecha Fin</strong></td>
                    <td><strong>Cobrado</strong></td>
                    <td><strong>Fecha Cobrado</strong></td>
				</tr>
				<?php foreach($result as $row): ?>
				<tr>
                    <td><?php echo $row['usuario']; ?></td>
                    <td><?php echo $row['cupon']; ?></td>
                    <td><?php echo $row['descuento']; ?>%</td>
                    <td><?php echo !empty($row['fecha_ini']) ? date("d/m/Y", strtotime($row['fecha_ini'])) : '-'; ?></td>
				    <td><?php echo !empty($row['fecha_fin']) ? date("d/m/Y", strtotime($row['fecha_fin'])) : '-'; ?></td>
                    <td><?php echo $row['cobrado']; ?>%</td>
                    <td><?php echo !empty($row['fecha_cobrado']) ? date("d/m/Y", strtotime($row['fecha_cobrado'])) : '-'; ?></td>
				</tr>
				<?php endforeach; ?>
				</table>
				<br style="clear:both" />
				<br style="clear:both" />
				
				<div id="ContenedorDiv">
				   <div id="CajaBotones">
						<center>
							<?php 
							$url=$this->link.'?mod='.$this->modulo;
							$red=$url.'&tarea=ACCEDER&module='.$_GET['module'];
							?>
							<input type="button" class="boton" name="" value="Volver" onclick="javascript:location.href='<?php echo $red;?>';">
						</center>
				   </div>
				</div>
				
		</div>
		<?php 
		
	}
	
	/*function eliminar_traducciones_contentido($id)
	{
		$conec= new ADO();
		$sql="
		SELECT con_title, con_description FROM content
		WHERE con_id = ".$id;
		$conec->ejecutar($sql);

		$datos = $conec->get_datos();
		foreach ($datos as $reg){
			$this->obj_languages->eliminar_traducciones($reg["con_title"],2,$id);
			$this->obj_languages->eliminar_traducciones($reg["con_description"],2,$id);
		}		
	}*/
	
}
?>