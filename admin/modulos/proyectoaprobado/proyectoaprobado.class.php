<?php

class PROYECTO extends BUSQUEDA 
{
	var $formulario;
	var $mensaje;
	
	function PROYECTO()
	{
		//permisos
		$this->ele_id=160;
		
		$this->busqueda();
		
		if(!($this->verificar_permisos('AGREGAR')))
		{
			$this->ban_agregar=false;
		}
		//fin permisos
		
		$this->num_registros=20;
		
		$this->coneccion= new ADO();
		
		$num=0;
		$this->arreglo_campos[$num]["nombre"]="fam_titulo";
		$this->arreglo_campos[$num]["texto"]="Titulo";
		$this->arreglo_campos[$num]["tipo"]="cadena";
		$this->arreglo_campos[$num]["tamanio"]=20;
		
		
		$this->link='gestor.php';
		
		$this->modulo='proyectoaprobado';
		
		$this->formulario = new FORMULARIO();
		
		$this->formulario->set_titulo('Proyectos');
		
		
	}
		
	function dibujar_busqueda()
	{
		
		$this->formulario->dibujar_cabecera();
		
		$this->dibujar_listado();
	}
	
		
	function set_opciones()
	{
				
		$nun=0;
		
		if($this->verificar_permisos('VER'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='VER';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_search.png';
			$this->arreglo_opciones[$nun]["nombre"]='VER';
			$nun++;
		}
		
		if($this->verificar_permisos('MODIFICAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='MODIFICAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_edit.png';
			$this->arreglo_opciones[$nun]["nombre"]='MODIFICAR';
			$nun++;
		}
		
		if($this->verificar_permisos('ELIMINAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='ELIMINAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_drop.png';
			$this->arreglo_opciones[$nun]["nombre"]='ELIMINAR';
			$nun++;
		}
		
		if($this->verificar_permisos('PUBLICACIONES'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='PUBLICACIONES';
			$this->arreglo_opciones[$nun]["imagen"]='images/publicaciones.jpg';
			$this->arreglo_opciones[$nun]["nombre"]='PUBLICACIONES';
			$nun++;
		}
		
		if($this->verificar_permisos('FOTOS'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='FOTOS';
			$this->arreglo_opciones[$nun]["imagen"]='images/fotos.png';
			$this->arreglo_opciones[$nun]["nombre"]='FOTOS';
			$nun++;
		}


		if($this->verificar_permisos('TIPOLOGIA'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='TIPOLOGIA';
			$this->arreglo_opciones[$nun]["imagen"]='images/tipologia.jpg';
			$this->arreglo_opciones[$nun]["nombre"]='TIPOLOGIA';
			$nun++;
		}
		
		if($this->verificar_permisos('ACABADO'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='ACABADO';
			$this->arreglo_opciones[$nun]["imagen"]='images/acabado.jpg';
			$this->arreglo_opciones[$nun]["nombre"]='ACABADO';
			$nun++;
		}
		
		if($this->verificar_permisos('DISPONIBILIDAD'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='DISPONIBILIDAD';
			$this->arreglo_opciones[$nun]["imagen"]='images/disponibilidad.jpg';
			$this->arreglo_opciones[$nun]["nombre"]='DISPONIBILIDAD';
			$nun++;
		}

        if($this->verificar_permisos('SLIDESHOW'))
        {
            $this->arreglo_opciones[$nun]["tarea"]='SLIDESHOW';
            $this->arreglo_opciones[$nun]["imagen"]='images/fotos.png';
            $this->arreglo_opciones[$nun]["nombre"]='SLIDESHOW';
            $nun++;
        }
		
		
	}
	
	function dibujar_listado()
	{
		$sql="SELECT *from proyecto where proy_estado='Aprobado' ";
		
		$this->set_sql($sql,' order by proy_id desc ');
		
		$this->set_opciones();
		
		$this->dibujar();
		
	}
	
	function orden($enlaces,$accion,$ant_orden)
	{
		$conec= new ADO();
		
		if($accion=='s')
			$cad=" where fam_orden < $ant_orden order by fam_orden desc";
		else

			$cad=" where fam_orden > $ant_orden order by fam_orden asc";			

		$consulta = "
		select 
			fam_id,fam_orden 
		from 
			familia_aplicaciones
		$cad
		limit 0,1
		";	

		$conec->ejecutar($consulta);

		$num = $conec->get_num_registros();   

		if($num > 0)
		{
			$objeto=$conec->get_objeto();
			
			$nu_orden=$objeto->fam_orden;
			
			$id=$objeto->fam_id;
			
			$consulta = "update familia_aplicaciones set fam_orden='$nu_orden' where fam_id='$enlaces'";	

			$conec->ejecutar($consulta);
			
			$consulta = "update familia_aplicaciones set fam_orden='$ant_orden' where fam_id='$id'";	

			$conec->ejecutar($consulta);
		}	
	}	
	
	function dibujar_encabezado()
	{
		?>
			<tr>
			
				<th>Proyecto</th>
				<th>Logo</th>			
	            <th class="tOpciones" width="100px">Opciones</th>
                
			</tr>
			
		<?PHP
	}
	
	function mostrar_busqueda()
	{
		$conversor = new convertir();
		
		for($i=0;$i<$this->numero;$i++)
			{
				
				$objeto=$this->coneccion->get_objeto();
				echo '<tr>';
									
					echo "<td>";
						echo $objeto->proy_nombre;
					echo "&nbsp;</td>";
					echo "<td>";
						echo '<img width="100" height="100" src="imagenes/proyecto/'.$objeto->proy_logo.'" />';
					echo "&nbsp;</td>";																
					echo "<td>";
						echo $this->get_opciones($objeto->proy_id);
					echo "</td>";
				echo "</tr>";
				
				$this->coneccion->siguiente();
			}
	}

    function mostrar_busqueda_foto_slideshow()
    {

        $conec=new ADO();

        $sql="select *
		from
		proyecto_slideshow
		where
		proyecto_id='".$_GET['id']."'  order by id asc";

        $conec->ejecutar($sql);

        $num=$conec->get_num_registros();

        for($i=0;$i<$num;$i++)
        {
            $objeto=$conec->get_objeto();

            echo '<tr class="busqueda_campos">';

            ?>

            <td align="left">
                <img width="100" height="100" src="imagenes/proyecto_slideshow/<?php echo $objeto->imagen; ?>">
            </td>
            <td align="left">
                &nbsp;<?php echo $objeto->youtube; ?>
            </td>
            <td>
                <center>
                    <a href="gestor.php?mod=proyectoaprobado&tarea=SLIDESHOW&acc=ELIMINAR&proy_fot_id=<?php echo $objeto->id;?>&id=<?php echo $_GET['id'];?>"><img src="images/b_drop.png" alt="ELIMINAR" title="ELIMINAR" border="0"></a><br />
                    <a href="gestor.php?mod=proyectoaprobado&tarea=SLIDESHOW&acc=MODIFICAR_FOTO&proy_fot_id=<?php echo $objeto->id;?>&id=<?php echo $_GET['id'];?>"><img src="images/b_edit.png" alt="MODIFICAR" title="MODIFICAR" border="0"></a>
                </center>
            </td>

            <?php

            echo "</tr>";





            $conec->siguiente();

        }

        echo "</tbody></table></center><br>";
    }
	
	function cargar_datos()
	{
		$conec=new ADO();
		
		$sql="select * from proyecto
				where proy_id = '".$_GET['id']."'";
		
		$conec->ejecutar($sql);
		
		$objeto=$conec->get_objeto();
		
		$_POST['proy_nombre']=$objeto->proy_nombre;		
		$_POST['proy_logo']=$objeto->proy_logo;		
		$_POST['proy_video']=$objeto->proy_video;	
		$_POST['proy_descripcion']=$objeto->proy_descripcion;	
		$_POST['proy_latitud']=$objeto->proy_latitud;
		$_POST['proy_longitud']=$objeto->proy_longitud;
		$_POST['proy_ubicacion']=$objeto->proy_ubicacion;
		$_POST['proy_estado']=$objeto->proy_estado;		
		$_POST['proy_creado']=$objeto->proy_creado;
        $_POST['proy_vig_ini']=$objeto->proy_vig_ini;		
		$_POST['proy_vig_fin']=$objeto->proy_vig_fin;	
		$_POST['inm_ciu_id']=$objeto->proy_ciu_id;	
		$_POST['inm_zon_id']=$objeto->proy_zon_id;	
		$_POST['proy_contacto']=$objeto->proy_contacto;
		
		
		$sql="select * from ciudad
				where ciu_id = '".$_POST['inm_ciu_id']."'";
		
		$conec->ejecutar($sql);
		
		$objeto=$conec->get_objeto();
		
		$_POST['dep_id'] = $objeto->ciu_dep_id;
	}
	
	function datos()
	{
		if($_POST)
		{
			//texto,  numero,  real,  fecha,  mail.
			$num=0;
			
			$valores[$num]["etiqueta"]="Nombre";
			$valores[$num]["valor"]=$_POST['proy_nombre'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			
			/*$valores[$num]["etiqueta"]="Logo";
			$valores[$num]["valor"]=$_FILES['proy_logo']['name'];;
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;*/
			
			$valores[$num]["etiqueta"]="Descripci�n";
			$valores[$num]["valor"]=$_POST['proy_descripcion'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			
			$valores[$num]["etiqueta"]="Estado";
			$valores[$num]["valor"]=$_POST['proy_estado'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			
			$valores[$num]["etiqueta"]="Latitud";
			$valores[$num]["valor"]=$_POST['proy_latitud'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			
			$valores[$num]["etiqueta"]="Longitud";
			$valores[$num]["valor"]=$_POST['proy_longitud'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
						
			$valores[$num]["etiqueta"]="Contacto";
			$valores[$num]["valor"]=$_POST['proy_contacto'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			
			/*$valores[$num]["etiqueta"]="Fecha Inicio";
			$valores[$num]["valor"]=$_POST['proy_vig_ini'];
			$valores[$num]["tipo"]="fecha";
			$valores[$num]["requerido"]=true;
			$num++;
			
			$valores[$num]["etiqueta"]="Fecha Fin";
			$valores[$num]["valor"]=$_POST['proy_vig_fin'];
			$valores[$num]["tipo"]="fecha";
			$valores[$num]["requerido"]=true;
			$num++;*/
			
			/*$valores[$num]["etiqueta"]="Categoria";
			$valores[$num]["valor"]=$_POST['categoria'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;*/
			
			$valores[$num]["etiqueta"]="Ubicaci�n";
			$valores[$num]["valor"]=$_POST['proy_ubicacion'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			
			$valores[$num]["etiqueta"]="Ciudad";
			$valores[$num]["valor"]=$_POST['inm_ciu_id'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			
			$valores[$num]["etiqueta"]="Zona";
			$valores[$num]["valor"]=$_POST['inm_zon_id'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			
			
			
			$val=NEW VALIDADOR;
			
			$this->mensaje="";
			
			if($val->validar($valores))
			{
				return true;
			}
				
			else
			{
				$this->mensaje=$val->mensaje;
				return false;
			}
		}
			return false;
	}
	
	function formulario_tcp($tipo)
	{
				switch ($tipo)
				{
					case 'ver':{
								$ver=true;
								break;
								}
							
					case 'cargar':{
								$cargar=true;
								break;
								}
				}
				
				$url=$this->link.'?mod='.$this->modulo;
				
				$red=$url."&tarea=ACCEDER";
				
				if(!($ver))
				{
					$url.="&tarea=".$_GET['tarea'];
				}
				
				if($cargar)
				{
					$url.='&id='.$_GET['id'];
				}

		
		    $this->formulario->dibujar_tarea('PROYECTOS');
		
			if($this->mensaje<>"")
			{
				$this->formulario->mensaje('Error',$this->mensaje);
			}
			?>

		<script type="text/javascript">
		   var map;
		   var markers = [];
		   var geocoder;
		   var zooms = 5;		   
		   function initialize() {
				geocoder = new google.maps.Geocoder();				
				var myLatlng = new google.maps.LatLng(-16.930705,-64.782716); 		
				
				var myOptions = {
					zoom: zooms,
					center: myLatlng,
					mapTypeId: google.maps.MapTypeId.ROADMAP
				}
				
				map = new google.maps.Map(document.getElementById("map"), myOptions);						
				
				<?php
				    if (isset($_POST['proy_latitud']) && isset($_POST['proy_longitud']))
				    {				       
				?>
				       var mypoint = new google.maps.LatLng(<? echo $_POST['proy_latitud']; ?>,<? echo $_POST['proy_longitud']; ?>);					 
					   createMarker(mypoint);	
					   map.setCenter(mypoint);			
					   map.setZoom(15);	
				<?php
				    }
				?>
				
				google.maps.event.addListener(map, 'click', function(event) {
				
					if  (markers.length < 1)
					{	
						createMarker(event.latLng);	  
						
					}   
					else
					{	
						markers[0].setPosition(event.latLng);
						actualizar_punto(markers[0]);
					}	
						
				});
				
				
		   }
		   
		   function actualizar_punto(point)
		   {
		        document.getElementById("latitud").value  =	point.getPosition().lat();	
				document.getElementById("longitud").value = point.getPosition().lng();	
		   }
		   
		   function createMarker(point) {
				
				var clickedLocation = new google.maps.LatLng(point);
				
				markers.push(new google.maps.Marker({
				  position: point,
				  map: map,
				  draggable: true,
				  animation: google.maps.Animation.DROP
				}));  
				
				actualizar_punto(markers[0]);
					
				google.maps.event.addListener(markers[0], 'dragend', function() {	
					
				actualizar_punto(markers[0]);});
			}

			function codeAddress() {
				
				var pais    = document.getElementById("map_pais").value;		
				var ciudad  = document.getElementById("map_ciudad").value;
				var tipo  = document.getElementById("map_tipo").value;
				var direccion = document.getElementById("map_direccion").value;
				var address;
				
				if (direccion != "")		   
				   address = tipo+","+direccion+","+ciudad+","+pais;	
				else
				   address = ciudad+","+pais;					
				
				geocoder.geocode( { 'address': address}, function(results, status) {
				  if (status == google.maps.GeocoderStatus.OK) {
					map.setCenter(results[0].geometry.location);			
					map.setZoom(12);
					if (direccion != "")
					   map.setZoom(15);
					
					if  (markers.length < 1)
					{	
						createMarker(results[0].geometry.location);
					}
					else
					{
						markers[0].setPosition(results[0].geometry.location);
					}			
					
				  } else {
					alert("Geocode was not successful for the following reason: " + status);
				  }
				});
			}	
			
		  
			function loadScript() {
			  var script = document.createElement("script");
			  script.type = "text/javascript";
			  script.src = "http://maps.google.com/maps/api/js?sensor=false&callback=initialize";
			  document.body.appendChild(script);
			}
			  
			window.onload = loadScript;
			
			$(document).ready(function() {
				$('.cerrarToogle').hide();				
				$('.cerrarToogle:first').show();
				$('.prueba').click(function(){
					var cat = $(this).parent().find('.cerrarToogle');
					if(cat.is(':visible')){
						$(cat).hide();
					} else {
						$(cat).slideDown('normal');
					};
				});	
			});	

			function cargar_ciudad(id)
			{
				var	valores="thid="+id;			
				ejecutar_ajax('ajax.php','ciudad',valores,'POST');
				var	valores="tzid="+id;			
				ejecutar_ajax('ajax.php','zona',valores,'POST');	
			}
			
			function cargar_caracteristica(id)
			{
				var	valores="tcid="+id;			
				ejecutar_ajax('ajax.php','caracteristica',valores,'POST');				
			}			
			
		</script>				
			
			
		<?php include_once("js/fckeditor/fckeditor.php"); ?>	  
		<div id="Contenedor_NuevaSentencia">
			<form id="frm_sentencia" name="frm_sentencia" action="<?php echo $url;?>" method="POST" enctype="multipart/form-data">  
				<div id="FormSent">
				  
					<div class="Subtitulo">Datos</div>
						<div id="ContenedorSeleccion">
							
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">* </span>Nombre</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto" name="proy_nombre" id="proy_nombre" size="60" maxlength="250" value="<?php echo $_POST['proy_nombre'];?>">
							   </div>
							</div>
							<!--Fin-->
							
							
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Logo</div>
							   <div id="CajaInput">
							   <?php
								if($_POST['proy_logo']<>"")
								{	$foto=$_POST['proy_logo'];
									$b=true;
								}	
								else
								{	$foto='sin_foto.gif';
									$b=false;
								}
								if(($ver)||($cargar))
								{	?>
									<img src="imagenes/proyecto/<?php echo $foto;?>" border="0" width="50"><br>
									<input   name="proy_logo" type="file" id="proy_logo" /><span class="flechas1"></span>
									<?php
								}
								else 
								{
									?>
									<input  name="proy_logo" type="file" id="proy_logo" /><span class="flechas1"></span>
									<?php
								}
								?>
								<input   name="fotooculta" type="hidden" id="fotooculta" value="<?php echo $_POST['proy_logo'].$_POST['fotooculta'];?>"/>
							   </div>
							</div>
							<!--Fin-->
							
							
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1"> </span>Video</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto" name="proy_video" id="proy_video" size="60" maxlength="250" value="<?php echo $_POST['proy_video'];?>">
							   </div>
							</div>
							<!--Fin-->
							
							
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">*</span>Descripci�n</div>
							   <div id="CajaInput">
							   <textarea name="proy_descripcion" id="proy_descripcion"><?php echo $_POST['proy_descripcion']; ?> </textarea>
								<?php
								/*$pagina=$_POST['proy_descripcion'];
								$oFCKeditor = new FCKeditor('proy_descripcion') ;
								$oFCKeditor->BasePath = 'js/fckeditor/';
								$oFCKeditor->Width  = '650' ;
								$oFCKeditor->Height = '200' ;
								//$oFCKeditor->ToolbarSet = 'Basic' ;
								$oFCKeditor->Value = $pagina;
								$oFCKeditor->Create() ;*/
								?>     
							   </div>
							</div>
							<!--Fin-->   
							
								<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">*</span>Ubicaci�n</div>
							   <div id="CajaInput">
							   <textarea name="proy_ubicacion" id="proy_ubicacion"><?php echo $_POST['proy_ubicacion']; ?> </textarea>
								<?php
						/*$pagina=$_POST['proy_ubicacion'];
						$oFCKeditor = new FCKeditor('proy_ubicacion') ;
						$oFCKeditor->BasePath = 'js/fckeditor/';
						$oFCKeditor->Width  = '650' ;
						$oFCKeditor->Height = '200' ;
						//$oFCKeditor->ToolbarSet = 'Basic' ;
						$oFCKeditor->Value = $pagina;
						$oFCKeditor->Create() ;*/
						?>     
							   </div>
							</div>
							<!--Fin-->   
							
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">*</span>Contacto</div>
							   <div id="CajaInput">
							   <textarea name="proy_contacto" id="proy_contacto"><?php echo $_POST['proy_contacto']; ?> </textarea>
								<?php
								/*$pagina=$_POST['proy_contacto'];
								$oFCKeditor = new FCKeditor('proy_contacto') ;
								$oFCKeditor->BasePath = 'js/fckeditor/';
								$oFCKeditor->Width  = '650' ;
								$oFCKeditor->Height = '200' ;
								//$oFCKeditor->ToolbarSet = 'Basic' ;
								$oFCKeditor->Value = $pagina;
								$oFCKeditor->Create() ;*/
								?>     
							   </div>
							</div>
							<!--Fin-->   				
							
							
							<!--Inicio-->
								<div id="ContenedorDiv">
								   <div class="Etiqueta" ><span class="flechas1">* </span>Estado</div>
								   <div id="CajaInput">
										<select name="proy_estado" class="caja_texto">
										<option value="" >Seleccione</option>
										<option value="Aprobado" <?php if($_POST['proy_estado']=='Aprobado') echo 'selected="selected"'; ?>>Aprobado</option>
										<option value="No Aprobado" <?php if($_POST['proy_estado']=='No Aprobado') echo 'selected="selected"'; ?>>No Aprobado</option>
										<option value="Cancelado" <?php if($_POST['proy_estado']=='Cancelado') echo 'selected="selected"'; ?>>Cancelado</option>
										</select>
								   </div>
								</div>
							<!--Fin-->
							
							
								<!--Inicio-->
							<div id="ContenedorDiv">
					            <div class="Etiqueta" >Fecha Inicio</div>
					            <div id="CajaInput">
									<input readonly="readonly" class="caja_texto" name="proy_vig_ini" id="proy_vig_ini" size="12" value="<?php echo $_POST['proy_vig_ini'];?>" type="text">
									<input name="but_fecha_pago2" id="but_fecha_pago2" class="boton_fecha" value="..." type="button">
									<script type="text/javascript">
																		Calendar.setup({inputField     : "proy_vig_ini"
																						,ifFormat     :     "%Y-%m-%d",
																						button     :    "but_fecha_pago2"
																						});
									</script>
								</div>		
					        </div>
											
							<div id="ContenedorDiv">
					            <div class="Etiqueta" >Fecha Fin</div>
					            <div id="CajaInput">
									<input readonly="readonly" class="caja_texto" name="proy_vig_fin" id="proy_vig_fin" size="12" value="<?php echo $_POST['proy_vig_fin'];?>" type="text">
									<input name="but_fecha_pago" id="but_fecha_pago" class="boton_fecha" value="..." type="button">
									<script type="text/javascript">
																		Calendar.setup({inputField     : "proy_vig_fin"
																						,ifFormat     :     "%Y-%m-%d",
																						button     :    "but_fecha_pago"
																						});
									</script>
								</div>		
					        </div>	
							<!--Fin-->
							
								
							
							
								<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Departamento</div>
							   <div id="CajaInput">
							   <select name="dep_id" class="caja_texto" onchange="cargar_ciudad(this.value);">
							   <option value="">Seleccione</option>
							   <?php 		
								$fun=NEW FUNCIONES;		
								$fun->combo("select dep_id as id,dep_nombre as nombre from departamento",$_POST['dep_id']);				
								?>
							   </select>
							   </div>
							   
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Ciudad</div>
							   <div id="CajaInput">
							   <div id="ciudad">
							   <select name="inm_ciu_id" class="caja_texto">
							   <option value="">Seleccione</option>
							   <?php 	
                                 if ($_POST["dep_id"])							   
								 {							   
									$fun=NEW FUNCIONES;		
									$fun->combo("select ciu_id as id,ciu_nombre as nombre from ciudad",$_POST['inm_ciu_id']);				
								}
								?>
							   </select>
							   </div>
							   </div>
							   
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Zona</div>
							   <div id="CajaInput">		
                                <div id="zona">		   
							   <select name="inm_zon_id" id="inm_zon_id" class="caja_texto">
							   <option value="">Seleccione</option>
							   <?php 	
                                 							   
									$fun=NEW FUNCIONES;		
									$fun->combo("select zon_id as id,zon_nombre as nombre from zona",$_POST['inm_zon_id']);				
								
								?>
							   </select>	
                               </div>							   
							   </div>
							   
							</div>
							
								
						
							
						
							
							<!--Inicio-->
							<!-- <div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1"> </span>Categorias</div>
								<div id="CajaInput">
									<table style="font-family: tahoma; font-size: 11px;">
										<tr>
											<td><input type="checkbox" name="categoria[]" value="casa"></td>
											<td>Casa</td>
										</tr>
										<tr>
											<td><input type="checkbox" name="categoria[]" value="departamento"> </td>
											<td>Departamento</td>
										</tr>
										<tr>
											<td><input type="checkbox" name="categoria[]" value="terreno"></td>
											<td>Terreno</td>
										</tr>
									</table>									 
								</div>
									
							</div> -->
							<!--Fin-->
							
							
							<!--Inicio-->
							<!--<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1"> </span>Habilitar</div>
							   <div id="CajaInput">
									<table>
										<tr>
											<td><input type="checkbox" name="habilitar" value=""></td>
										</tr>
									</table>
							   </div>
							</div>-->
							<!--Fin-->
							
							
							<!--Inicio-->
							<!--<div id="ContenedorDiv">
					            <div class="Etiqueta" >Fecha Inicio</div>
					            <div id="CajaInput">
									<input readonly="readonly" class="caja_texto" name="proy_vig_ini" id="proy_vig_ini" size="12" value="<?php echo $_POST['proy_vig_ini'];?>" type="text">
									<input name="but_fecha_pago2" id="but_fecha_pago2" class="boton_fecha" value="..." type="button">
									<script type="text/javascript">
																		Calendar.setup({inputField     : "proy_vig_ini"
																						,ifFormat     :     "%Y-%m-%d",
																						button     :    "but_fecha_pago2"
																						});
									</script>
								</div>		
					        </div>
											
							<div id="ContenedorDiv">
					            <div class="Etiqueta" >Fecha Fin</div>
					            <div id="CajaInput">
									<input readonly="readonly" class="caja_texto" name="proy_vig_fin" id="proy_vig_fin" size="12" value="<?php echo $_POST['proy_vig_fin'];?>" type="text">
									<input name="but_fecha_pago" id="but_fecha_pago" class="boton_fecha" value="..." type="button">
									<script type="text/javascript">
																		Calendar.setup({inputField     : "proy_vig_fin"
																						,ifFormat     :     "%Y-%m-%d",
																						button     :    "but_fecha_pago"
																						});
									</script>
								</div>		
					        </div>			-->			
							<!--Fin-->
							


							<!--Inicio-->
							<br/>
							<fieldset style=" margin-left: 159px; width: 610px;">
							<legend class='prueba' id='mapaCargar'>&nbsp;Ubicaci�n&nbsp;</legend>
							<div class='cerrarToogle'>
							
							<div id="ContenedorDiv">							  
							    <div id="CajaInput">
							   <div style="padding:10px 0px 10px 15px">						   
							        <input id="latitud" name="proy_latitud" type="hidden" value="<?php echo $_POST['proy_latitud'];?>">
									<input id="longitud" name="proy_longitud" type="hidden" value = "<?php echo $_POST['proy_longitud'];?>">									
									<input id="map_pais" name="map_pais" type="hidden" value="Bolivia">
									<table border="0" cellspacing="2">
									    <tr>
										    <td><div style="color: #005C89; font-size:12px" >Ciudad</div></td>
											<td><div style="color: #005C89; font-size:12px" >Tipo</div></td>
											<td><div style="color: #005C89; font-size:12px" >Direccion</div></td>
											<td>&nbsp;</td>
										</tr>
										<tr>
										    <td>
												<select id="map_ciudad" name="map_ciudad" style="width:100px; ">
													 <option value="Santa Cruz de la Sierra">Santa Cruz</option>
													 <option value="La Paz">La Paz</option>	
													 <option value="Cochabamba">Cochabamba</option>	
													 <option value="Tarija">Tarija</option>	
													 <option value="Sucre">Sucre</option>	
													 <option value="Oruro">Oruro</option>	
													 <option value="Potosi">Potosi</option>	
													 <option value="Trinidad">Beni</option>	
													 <option value="Cobija">Pando</option>	
												</select>
											</td>
											<td>
												<select id="map_tipo" name="map_tipo" style="width:100px; ">
													 <option value="Calle">Calle</option>
													 <option value="Av.">Avenida</option>											 
												</select>
											</td>
											<td><input id="map_direccion" type="textbox" size="35" ></td>
											<td>
												<input type="button" class="boton" style="width:100px" value="Buscar Direcci�n" onclick="codeAddress()">
											</td>
										</tr>
									</table>
							  </div>
								<div id="map" style="width: 600px; height: 600px;"></div>
							   </div>
							</div>							
							</div>								
							</fieldset>							
							<!--Fin-->


							
						</div>
						
						<div id="ContenedorDiv">
						   <div id="CajaBotones">
								<center>
								<?php
								if(!($ver))
								{
									?>
									<input type="submit" class="boton" name="" value="Guardar">
									<input type="reset" class="boton" name="" value="Cancelar">
									<input type="button" class="boton" name="" value="Volver" onclick="javascript:location.href='<?php echo $red;?>';">
									<?php
								}
								else
								{
									?>
									<input type="button" class="boton" name="" value="Volver" onclick="javascript:location.href='<?php echo $red;?>';">
									<?php
								}
								?>
								</center>
						   </div>
						</div>
				</div>
			</form>
		</div>
		<?php
	}
	
	function insertar_tcp()
	{
		$conec= new ADO();
				
		if($_FILES['proy_logo']['name']<>"")
		{
			$result=$this->subir_imagen_proyecto($nombre_archivo,$_FILES['proy_logo']['name'],$_FILES['proy_logo']['tmp_name']);
			
		
		
			
		/*	$sql="insert into proyecto(proy_nombre,proy_logo,proy_video,proy_descripcion,proy_latitud,proy_longitud,proy_ubicacion,proy_estado,proy_creado,proy_vig_ini,proy_vig_fin,proy_ciu_id,proy_zon_id) values 
							('". $_POST['proy_nombre'] ."','". $nombre_archivo  ."','". $_POST['proy_video'] ."','". $_POST['proy_descripcion'] ."','". $_POST['proy_latitud'] ."','" . $_POST['proy_longitud'] ."','" .
							$_POST['proy_ubicacion'] ."','". $estado ."','" .  date('Y-m-d') ."','". $_POST['proy_vig_ini'] ."','" . $_POST['proy_vig_fin'] ."','". $_POST['proy_ciu_id'] ."','". $_POST['proy_zon_id'] ."')";*/
			
			
			$sql="insert into proyecto(proy_nombre,proy_contacto,proy_logo,proy_video,proy_descripcion,proy_latitud,proy_longitud,proy_ubicacion,proy_estado,proy_vig_ini,proy_vig_fin,proy_ciu_id,proy_zon_id) values 
							('". $_POST['proy_nombre'] ."','".$_POST['proy_contacto'] ."','". $nombre_archivo  ."','". $_POST['proy_video'] ."','". $_POST['proy_descripcion'] ."','". $_POST['proy_latitud'] ."','" . $_POST['proy_longitud'] ."','" .
							$_POST['proy_ubicacion'] ."','". $_POST['proy_estado'] ."','" . $_POST['proy_vig_ini'] ."','". $_POST['proy_vig_fin'] ."','". $_POST['inm_ciu_id'] ."','". $_POST['inm_zon_id'] ."')";
							
											
			
			
			
			if(trim($result)<>'')
			{
				$this->formulario->ventana_volver($result,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
			}
			else 
			{
				$conec->ejecutar($sql);
				
				
				$mensaje='Proyecto Agregado Correctamente!!!';
				
				$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
			}
		}
		else
		{
			$sql="insert into proyecto(proy_nombre,proy_contacto,proy_video,proy_descripcion,proy_latitud,proy_longitud,proy_ubicacion,proy_estado,proy_creado,proy_vig_ini,proy_vig_fin) values 
							('". $_POST['proy_nombre'] ."','".$_POST['proy_contacto'] ."','". $_POST['proy_video'] ."','". $_POST['descripcion'] ."','". $_POST['proy_latitud'] ."','" . $_POST['proy_longitud'] ."','" .
							$_POST['proy_ubicacion'] ."','". $estado ."','" . $_POST['proy_vig_ini'] ."','" . $_POST['proy_vig_fin'] ."')";

			$conec->ejecutar($sql);

			$mensaje='Proyecto Agregado Correctamente!!!';

			$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
		}
	}
	
	
	function modificar_tcp()
	{
		$conec= new ADO();
		
		if($_FILES['proy_logo']['name']<>"")
		{	 	 	 	 	 	 	
			 	 	 	 	
			$result=$this->subir_imagen_proyecto($nombre_archivo,$_FILES['proy_logo']['name'],$_FILES['proy_logo']['tmp_name']);
			
			$sql="update proyecto set 
								proy_nombre='".$_POST['proy_nombre']."',
								proy_video='".$_POST['proy_video']."',
								proy_descripcion='".$_POST['proy_descripcion']."',
								proy_logo='".$nombre_archivo."',
								proy_latitud='".$_POST['proy_latitud']."',
								proy_longitud='".$_POST['proy_longitud']."',
								proy_vig_ini='".$_POST['proy_vig_ini']."',
								proy_vig_fin='".$_POST['proy_vig_fin']."',
								proy_contacto='".$_POST['proy_contacto']."',
								proy_estado='".$_POST['proy_estado']."',
								proy_ubicacion='".$_POST['proy_ubicacion']."',
								proy_ciu_id='".$_POST['inm_ciu_id']."',
								proy_zon_id='".$_POST['inm_zon_id']."'
								where proy_id = '".$_GET['id']."'";
			
			if(trim($result)<>'')
			{
					
				$this->formulario->ventana_volver($result,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
					
			}
			else
			{
				$llave=$_GET['id'];
				
				$mi=trim($_POST['fotooculta']);
				
				if($mi<>"")
				{
					$mifile="imagenes/proyecto/$mi";
				
					@unlink($mifile);
				
				}
				
				
				$conec->ejecutar($sql);
				
				$consulta = "select *from inmueble,publicacion where inm_proy_id=".$_GET['id'] ." and inm_id = pub_inm_id ";
				$conec->ejecutar($consulta);
				$num = $conec->get_num_registros();   
				
				for($i=0; $i<$num ; $i++){
				
						$objeto=$conec->get_objeto();
						
						$sql ="update publicacion set
								pub_estado='". $_POST['proy_estado'] ."',
								pub_vig_ini='". $_POST['proy_vig_ini'] ."',
								pub_vig_fin='". $_POST['proy_vig_fin'] ."'
								 where pub_id =". $objeto->pub_id;
								 
						$conec_inm = new ADO();								 
						$conec_inm->ejecutar($sql);
						
						if($_POST['proy_estado'] == 'Aprobado'){
							$is_pub = 'Si';
						}else{
							$is_pub = 'No';
						}
						$sql ="update inmueble set
								inm_publicado='$is_pub' 
								 where inm_id =". $objeto->inm_id;
						$conec_inm->ejecutar($sql);
								 
						$conec->siguiente();						
				}
				
			
				$mensaje='Proyecto Modificado Correctamente!!!';
					
				$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
			}
		}
		else
		{
			
			$sql="update proyecto set 
								proy_nombre='".$_POST['proy_nombre']."',
								proy_video='".$_POST['proy_video']."',
								proy_descripcion='".$_POST['proy_descripcion']."',						
								proy_latitud='".$_POST['proy_latitud']."',
								proy_longitud='".$_POST['proy_longitud']."',
								proy_ubicacion='".$_POST['proy_ubicacion']."',
								proy_contacto='".$_POST['proy_contacto']."',
								proy_vig_ini='".$_POST['proy_vig_ini']."',
								proy_vig_fin='".$_POST['proy_vig_fin']."',
								proy_estado='".$_POST['proy_estado']."',
								proy_ciu_id='".$_POST['inm_ciu_id']."',
								proy_zon_id='".$_POST['inm_zon_id']."'
								where proy_id = '".$_GET['id']."'";

			$conec->ejecutar($sql);
			
			$consulta = "select *from inmueble,publicacion where inm_proy_id=".$_GET['id'] ." and inm_id = pub_inm_id ";
				$conec->ejecutar($consulta);
				$num = $conec->get_num_registros();   
				
				for($i=0; $i<$num ; $i++){
				
						$objeto=$conec->get_objeto();
						
						$sql ="update publicacion set
								pub_estado='". $_POST['proy_estado'] ."',
								pub_vig_ini='". $_POST['proy_vig_ini'] ."',
								pub_vig_fin='". $_POST['proy_vig_fin'] ."'
								 where pub_id =". $objeto->pub_id;
								 
						$conec_inm = new ADO();								 
						$conec_inm->ejecutar($sql);
						
						if($_POST['proy_estado'] == 'Aprobado'){
							$is_pub = 'Si';
						}else{
							$is_pub = 'No';
						}
						$sql ="update inmueble set 
								inm_publicado='$is_pub' 
								 where inm_id =". $objeto->inm_id;
						$conec_inm->ejecutar($sql);
						
						$conec->siguiente();						
				}

			$mensaje='Proyecto Modificado Correctamente!!!';

			$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
		}
	}
	
	function formulario_confirmar_eliminacion()
	{
		
		$mensaje='Esta seguro de eliminar el Proyecto?';
		
		$this->formulario->ventana_confirmacion($mensaje,$this->link."?mod=$this->modulo",'proy_id');
	}
	
	function nombre_imagen_proyecto($id)
	{
		$conec= new ADO();
		
		$sql="select proy_logo as imagen from proyecto where proy_id='".$id."'";
		
		$conec->ejecutar($sql);
		
		$objeto=$conec->get_objeto();
		
		return $objeto->imagen;
	}
	
	function eliminar_tcp()
	{
		
			$conec= new ADO();
			
			$llave=$_POST['proy_id'];
			
			$mi=$this->nombre_imagen_proyecto($llave);
			
			if(trim($mi)<>"")
			{
				$mifile="imagenes/proyecto/$mi";
			
				@unlink($mifile);
				
			}
			
			$sql="delete from proyecto where proy_id='".$_POST['proy_id']."'";
			
			$conec->ejecutar($sql);
			
			$mensaje='Proyecto Eliminado Correctamente!!!';
			
		
		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
	}
	
	
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    function cargar_datos_foto_slideshow()
    {
        $conec=new ADO();

        $sql="select * from proyecto_slideshow where id='".$_GET['proy_fot_id']."'";

        $conec->ejecutar($sql);

        $objeto=$conec->get_objeto();

        $_POST['proy_fot_id']=$objeto->id;
        $_POST['proy_fot_imagen']=$objeto->imagen;
        $_POST['youtube']=$objeto->youtube;
        $_POST['tipo']=$objeto->tipo;
    }
	
	function guardar_publicacion()
	{		
		$conec= new ADO();
		
		$sql="select * from proyecto
				where proy_id = '".$_GET['id']."'";
		
		$conec->ejecutar($sql);
		$objeto=$conec->get_objeto();
		
		
		$sql="insert into inmueble(inm_nombre,inm_foto,inm_direccion,inm_precio,inm_superficie,inm_detalle,inm_ciu_id,inm_orden,inm_latitud,inm_longitud,inm_cat_id,inm_zon_id,inm_for_id,inm_tipo_superficie,inm_mon_id,inm_proy_id) values 
									('".$objeto->proy_nombre."','".$objeto->proy_logo."','".$objeto->proy_ubicacion."','".$_POST['inm_precio']."','".$_POST['inm_superficie']."','".$_POST['inm_detalle']."','".$objeto->proy_ciu_id."','0','".$objeto->proy_latitud."','".$objeto->proy_longitud."','".$_POST['inm_cat_id']."','".$objeto->proy_zon_id."','".$_POST['inm_for_id']."','".$_POST['inm_tipo_superficie']."','".$_POST['inm_mon_id']."','".$_GET['id']."')";
		
		$conec->ejecutar($sql);
		
		$sql="select max(inm_id) as id from inmueble;";
		$conec->ejecutar($sql);
		
		$objeto=$conec->get_objeto();
		$insert_id = $objeto->id;
		//$insert_id = mysql_insert_id();
		
		$sql="insert into publicacion(pub_creado,pub_vig_ini,pub_vig_fin,pub_monto,pub_inm_id,pub_estado) values 
									 (NOW(),'','','0','".$insert_id."','No Aprobado')";
									 
		$conec->ejecutar($sql);		
		
		$this->limpiar_publicacion();		
	}
	
	function guardar_foto()
	{		
		$conec= new ADO();		
		
		$this->subir_imagen_foto($nombre_imagen,$_FILES['proy_fot_imagen']['name'],$_FILES['proy_fot_imagen']['tmp_name']);
		
		$sql="insert into proy_foto(proy_fot_imagen,proy_id,proy_fot_descripcion) values 
									('".$nombre_imagen."','".$_GET['id']."','".$_POST["proy_fot_descripcion"]."')";
		
		$conec->ejecutar($sql);	
		
		$this->limpiar_foto();		
	}

    function guardar_foto_slideshow()
    {
        $conec= new ADO();

        $this->subir_imagen_foto_slideshow($nombre_imagen,$_FILES['proy_fot_imagen']['name'],$_FILES['proy_fot_imagen']['tmp_name']);

        $tipo = !empty($_POST["youtube"])? "Video" : "Imagen";

        $sql="insert into proyecto_slideshow(imagen,proyecto_id,youtube,tipo) values
									('".$nombre_imagen."','".$_GET['id']."','".$_POST["youtube"]."', '".$tipo."')";

        $conec->ejecutar($sql);

        $this->limpiar_foto();
    }
	
	function guardar_tipologia()
	{		
		$conec= new ADO();		
		
		$this->subir_imagen_foto($nombre_imagen,$_FILES['tip_imagen']['name'],$_FILES['tip_imagen']['tmp_name']);
		
		$sql="insert into tipologia(tip_imagen,tip_titulo,tip_disponibilidad,tip_descripcion,tip_precio,tip_mts,tip_terreno_mts,tip_proy_id) values 
									('".$nombre_imagen."','". $_POST['tip_titulo'] ."','". $_POST['tip_disponibilidad'] ."','". $_POST['tip_descripcion'] ."','". $_POST['tip_precio']."','". $_POST['tip_mts']."','". $_POST['tip_terreno_mts']."','".$_GET['id']."')";
		
		$conec->ejecutar($sql);	
		
		$this->limpiar_tipologia();		
	}

    function modificar_foto_slideshow()
    {
        $conec= new ADO();

        if($_FILES['proy_fot_imagen']['name']<>"")
        {

            $result = $this->subir_imagen_foto_slideshow($nombre_imagen,$_FILES['proy_fot_imagen']['name'],$_FILES['proy_fot_imagen']['tmp_name']);

            if(trim($result)<>'')
            {

                $this->formulario->ventana_volver($result,$this->link.'?mod='.$this->modulo.'&tarea=SLIDESHOW&id='.$_GET['id']);

            }
            else
            {
                $tipo = !empty($_POST['youtube']) ? "Video" : "Imagen";
                $sql="update proyecto_slideshow set
                            imagen='".$nombre_imagen."',
							youtube='".$_POST['youtube']."',
							tipo='".$tipo."'
                            where id = '".$_GET['proy_fot_id']."'";
                $conec->ejecutar($sql);

                $mensaje='Foto Modificada Correctamente!!!';

                $this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=SLIDESHOW&id='.$_GET['id']);
            }
        }
        else
        {
            $tipo = !empty($_POST['youtube']) ? "Video" : "Imagen";
            $sql="update proyecto_slideshow set
							youtube='".$_POST['youtube']."',
							tipo='".$tipo."'
                            where id = '".$_GET['proy_fot_id']."'";
            $conec->ejecutar($sql);

            $mensaje='Foto Modificada Correctamente!!!';

            $this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=SLIDESHOW&id='.$_GET['id']);
        }

    }
	
	function subir_imagen_foto(&$nombre_imagen,$name,$tmp)
	{	
		 require_once('clases/upload.class.php');

		 $nn=date('d_m_Y_H_i_s_').rand();

		 $upload_class = new Upload_Files();

		 $upload_class->temp_file_name = trim($tmp); 	
		 
		 $upload_class->file_name = $nn.substr(trim($name), -4, 4);

		 $nombre_imagen=$upload_class->file_name;		 		 

		 $upload_class->upload_dir = "imagenes/proyecto/";

		 $upload_class->upload_log_dir = "imagenes/proyecto/upload_logs/";

		 $upload_class->max_file_size = 1048576; 	

		 $upload_class->ext_array = array(".jpg",".gif",".png");

         $upload_class->crear_thumbnail=false;

		 $valid_ext = $upload_class->validate_extension(); 

		 $valid_size = $upload_class->validate_size(); 

		 $valid_user = $upload_class->validate_user(); 

		 $max_size = $upload_class->get_max_size(); 

		 $file_size = $upload_class->get_file_size(); 

		 $file_exists = $upload_class->existing_file(); 		

		if (!$valid_ext) { 				   

			$result = "La Extension de este Archivo es invalida, Intente nuevamente por favor!"; 

		} 

		elseif (!$valid_size) { 

			$result = "El Tama�o de este archivo es invalido, El maximo tama�o permitido es: $max_size y su archivo pesa: $file_size"; 

		}    

		elseif ($file_exists) { 

			$result = "El Archivo Existe en el Servidor, Intente nuevamente por favor."; 

		} 

		else 
		{		    
			$upload_file = $upload_class->upload_file_with_validation(); 

			if (!$upload_file) { 

				$result = "Su archivo no se subio correctamente al Servidor."; 
			}

			else 
			{ 
					$result = "";
			} 
		} 	

		return $result;	

	}

    function subir_imagen_foto_slideshow(&$nombre_imagen,$name,$tmp)
    {
        require_once('clases/upload.class.php');

        $nn=date('d_m_Y_H_i_s_').rand();

        $upload_class = new Upload_Files();

        $upload_class->temp_file_name = trim($tmp);

        $upload_class->file_name = $nn.substr(trim($name), -4, 4);

        $nombre_imagen=$upload_class->file_name;

        $upload_class->upload_dir = "imagenes/proyecto_slideshow/";

        $upload_class->upload_log_dir = "imagenes/proyecto_slideshow/upload_logs/";

        $upload_class->max_file_size = 1048576;

        $upload_class->ext_array = array(".jpg",".gif",".png");

        $upload_class->crear_thumbnail=false;

        $valid_ext = $upload_class->validate_extension();

        $valid_size = $upload_class->validate_size();

        $valid_user = $upload_class->validate_user();

        $max_size = $upload_class->get_max_size();

        $file_size = $upload_class->get_file_size();

        $file_exists = $upload_class->existing_file();

        if (!$valid_ext) {

            $result = "La Extension de este Archivo es invalida, Intente nuevamente por favor!";

        }

        elseif (!$valid_size) {

            $result = "El Tama�o de este archivo es invalido, El maximo tama�o permitido es: $max_size y su archivo pesa: $file_size";

        }

        elseif ($file_exists) {

            $result = "El Archivo Existe en el Servidor, Intente nuevamente por favor.";

        }

        else
        {
            $upload_file = $upload_class->upload_file_with_validation();

            if (!$upload_file) {

                $result = "Su archivo no se subio correctamente al Servidor.";
            }

            else
            {
                $result = "";
            }
        }

        return $result;

    }
	
	function subir_imagen_proyecto(&$nombre_imagen,$name,$tmp)
	{	
		 require_once('clases/upload.class.php');

		 $nn=date('d_m_Y_H_i_s_').rand();

		 $upload_class = new Upload_Files();

		 $upload_class->temp_file_name = trim($tmp); 	
		 
		 $upload_class->file_name = $nn.substr(trim($name), -4, 4);

		 $nombre_imagen=$upload_class->file_name;		 		 

		 $upload_class->upload_dir = "imagenes/proyecto/"; 

		 $upload_class->upload_log_dir = "imagenes/proyecto/upload_logs/"; 

		 $upload_class->max_file_size = 1048576; 	

		 $upload_class->ext_array = array(".jpg",".gif",".png");

         $upload_class->crear_thumbnail=false;

		 $valid_ext = $upload_class->validate_extension(); 

		 $valid_size = $upload_class->validate_size(); 

		 $valid_user = $upload_class->validate_user(); 

		 $max_size = $upload_class->get_max_size(); 

		 $file_size = $upload_class->get_file_size(); 

		 $file_exists = $upload_class->existing_file(); 		

		if (!$valid_ext) { 				   

			$result = "La Extension de este Archivo es invalida, Intente nuevamente por favor!"; 

		} 

		elseif (!$valid_size) { 

			$result = "El Tama�o de este archivo es invalido, El maximo tama�o permitido es: $max_size y su archivo pesa: $file_size"; 

		}    

		elseif ($file_exists) { 

			$result = "El Archivo Existe en el Servidor, Intente nuevamente por favor."; 

		} 

		else 
		{		    
			$upload_file = $upload_class->upload_file_with_validation(); 

			if (!$upload_file) { 

				$result = "Su archivo no se subio correctamente al Servidor."; 
			}

			else 
			{ 
					$result = "";
			} 
		} 	

		return $result;	

	}
	
	function subir_imagen_pdf(&$nombre_imagen,$name,$tmp)
	{	
		 require_once('clases/upload.class.php');

		 $nn=date('d_m_Y_H_i_s_').rand();

		 $upload_class = new Upload_Files();

		 $upload_class->temp_file_name = trim($tmp); 	
		 
		 $upload_class->file_name = $nn.substr(trim($name), -4, 4);

		 $nombre_imagen=$upload_class->file_name;		 		 

		 $upload_class->upload_dir = "imagenes/familia_aplicaciones/"; 

		 $upload_class->upload_log_dir = "imagenes/familia_aplicaciones/upload_logs/"; 

		 $upload_class->max_file_size = 1048576; 	

		 $upload_class->ext_array = array(".pdf",".gif");

         $upload_class->crear_thumbnail=false;

		 $valid_ext = $upload_class->validate_extension(); 

		 $valid_size = $upload_class->validate_size(); 

		 $valid_user = $upload_class->validate_user(); 

		 $max_size = $upload_class->get_max_size(); 

		 $file_size = $upload_class->get_file_size(); 

		 $file_exists = $upload_class->existing_file(); 		

		if (!$valid_ext) { 				   

			$result = "La Extension de este Archivo es invalida, Intente nuevamente por favor!"; 

		} 

		elseif (!$valid_size) { 

			$result = "El Tama�o de este archivo es invalido, El maximo tama�o permitido es: $max_size y su archivo pesa: $file_size"; 

		}    

		elseif ($file_exists) { 

			$result = "El Archivo Existe en el Servidor, Intente nuevamente por favor."; 

		} 

		else 
		{		    
			$upload_file = $upload_class->upload_file_with_validation(); 

			if (!$upload_file) { 

				$result = "Su archivo no se subio correctamente al Servidor."; 
			}

			else 
			{ 
					$result = "";
			} 
		} 	

		return $result;	

	}
	
	function subir_imagen(&$nombre_imagen,$name,$tmp)
	{	
		 require_once('clases/upload.class.php');

		 $nn=date('d_m_Y_H_i_s_').rand();

		 $upload_class = new Upload_Files();

		 $upload_class->temp_file_name = trim($tmp); 	
		 
		 $upload_class->file_name = $nn.substr(trim($name), -4, 4);

		 $nombre_imagen=$upload_class->file_name;		 		 

		 $upload_class->upload_dir = "imagenes/aplicaciones_productos/"; 

		 $upload_class->upload_log_dir = "imagenes/aplicaciones_productos/upload_logs/"; 

		 $upload_class->max_file_size = 1048576; 	

		 $upload_class->ext_array = array(".jpg",".gif",".png");

         $upload_class->crear_thumbnail=false;

		 $valid_ext = $upload_class->validate_extension(); 

		 $valid_size = $upload_class->validate_size(); 

		 $valid_user = $upload_class->validate_user(); 

		 $max_size = $upload_class->get_max_size(); 

		 $file_size = $upload_class->get_file_size(); 

		 $file_exists = $upload_class->existing_file(); 		

		if (!$valid_ext) { 				   

			$result = "La Extension de este Archivo es invalida, Intente nuevamente por favor!"; 

		} 

		elseif (!$valid_size) { 

			$result = "El Tama�o de este archivo es invalido, El maximo tama�o permitido es: $max_size y su archivo pesa: $file_size"; 

		}    

		elseif ($file_exists) { 

			$result = "El Archivo Existe en el Servidor, Intente nuevamente por favor."; 

		} 

		else 
		{		    
			$upload_file = $upload_class->upload_file_with_validation(); 

			if (!$upload_file) { 

				$result = "Su archivo no se subio correctamente al Servidor."; 
			}

			else 
			{ 
					$result = "";
			} 
		} 	

		return $result;	

	}

	function limpiar_publicacion()
	{
		$_POST['inm_cat_id']="";
		$_POST['inm_for_id']="";
		$_POST['inm_superficie']="";
		$_POST['inm_precio']="";
		$_POST['inm_mon_id']="";
		$_POST['inm_detalle']="";
		$_POST['inm_tipo_superficie']="";
		
	}
	
	function limpiar_tipologia()
	{
		$_POST['tip_descripcion']="";
		$_POST['tip_precio']="";
		$_POST['tip_imagen']="";
		
	}
	
	function limpiar_foto()
	{
		$_FILES['proy_fot_imagen']="";
		
	}

	

	function datos_publicacion()
	{

		if($_POST)
		{
			require_once('clases/validar.class.php');

			$num=0;
			
			$valores[$num]["etiqueta"]="Categoria";
			$valores[$num]["valor"]=$_POST['inm_cat_id'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			$valores[$num]["etiqueta"]="Forma";
			$valores[$num]["valor"]=$_POST['inm_for_id'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			$valores[$num]["etiqueta"]="Superficie";
			$valores[$num]["valor"]=$_POST['inm_superficie'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			$valores[$num]["etiqueta"]="Precio";
			$valores[$num]["valor"]=$_POST['inm_precio'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			$valores[$num]["etiqueta"]="Moneda";
			$valores[$num]["valor"]=$_POST['inm_mon_id'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			$valores[$num]["etiqueta"]="Tipo superficie";
			$valores[$num]["valor"]=$_POST['inm_tipo_superficie'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;

			$val=NEW VALIDADOR;

			$this->mensaje="";

			if($val->validar($valores))
			{
				return true;
			}
			else
			{
				$this->mensaje=$val->mensaje;

				return false;

			}

		}

			return false;

	}
	
	function datos_tipologia()
	{

		if($_POST)
		{
			require_once('clases/validar.class.php');

			$num=0;
			
			/*$valores[$num]["etiqueta"]="Descripcion";
			$valores[$num]["valor"]=$_POST['tip_descripcion'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;*/
			
			$valores[$num]["etiqueta"]="Precio";
			$valores[$num]["valor"]=$_POST['tip_precio'];
			$valores[$num]["tipo"]="real";
			$valores[$num]["requerido"]=true;
			$num++;
			

			$val=NEW VALIDADOR;

			$this->mensaje="";

			if($val->validar($valores))
			{
				return true;
			}
			else
			{
				$this->mensaje=$val->mensaje;

				return false;

			}

		}

			return false;

	}
	
	function datos_foto()
	{

		if($_POST)
		{
			require_once('clases/validar.class.php');

			$num=0;
			
			$val=NEW VALIDADOR;

			$this->mensaje="";

			if($val->validar($valores))
			{
				return true;
			}
			else
			{
				$this->mensaje=$val->mensaje;

				return false;

			}

		}

			return false;

	}


	

	function formulario_tcp_publicacion($tipo)

	{

		$url=$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER';

		$re=$url;

		$enlace="<a href='$url'>Proyectos</a>";

		echo "<tr>
					<td class='busqueda_enlace' colspan='3'>
						Volver a $enlace
					</td>
				</tr>"; 		

		switch ($tipo)
		{

			case 'ver':{
						$ver=true;
						
						break;
						}

			case 'cargar':{

						$cargar=true;

						break;
						}
		}

		if(!($ver))
		{
			$url.="&tarea=".$_GET['tarea'];
		}

		if($cargar)
		{
			$url.='&id='.$_GET['id'];
		}
		
		if($_GET['acc']=='MODIFICAR_PUBLICACION')
		{
			$url.='&acc=MODIFICAR_PUBLICACION';
		}
		
		$this->formulario->dibujar_tarea('PUBLICACION');
		
		if($this->mensaje<>"")
		{
			$this->formulario->mensaje('Error',$this->mensaje);
		}

		?>


			<div id="Contenedor_NuevaSentencia">

               <form id="frm_sentencia" name="frm_sentencia" action="<?php echo $url.'&inm_id='.$_GET['inm_id'];?>" method="POST" enctype="multipart/form-data">  

				<div id="FormSent" style="width:900px;">

                    

					<div class="Subtitulo">Publicacion</div>

                    <div id="ContenedorSeleccion">
						
						<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Categoria</div>
							   <div id="CajaInput">							   
							   <select name="inm_cat_id" class="caja_texto" onchange="">
							   <option value="">Seleccione</option>
							   <?php 	                                 							   
									$fun=NEW FUNCIONES;		
									$fun->combo("select cat_id as id,cat_nombre as nombre from categoria",$_POST['inm_cat_id']);												
								?>
							   </select>							   
							   </div>
							   
							</div>
						<!--Fin-->
						
						<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Tipo</div>
							   <div id="CajaInput">
							   
							   <select name="inm_for_id" class="caja_texto">
							   <option value="">Seleccione</option>
							   <?php 	
                                 							   
									$fun=NEW FUNCIONES;		
									$fun->combo("select for_id as id,for_descripcion as nombre from forma",$_POST['inm_for_id']);				
								
								?>
							   </select>
							  
							   </div>
							   
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Superficie</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto" name="inm_superficie" id="inm_superficie" maxlength="250"  size="10" value="<?php echo $_POST['inm_superficie'];?>">
							   <select name="inm_tipo_superficie" class="caja_texto">
									<option value="" >Seleccione</option>
									<option value="Metros Cuadrados" <?php if($_POST['inm_tipo_superficie']=='Metros Cuadrados') echo 'selected="selected"'; ?>>Metros Cuadrados</option>
									<option value="Hectareas" <?php if($_POST['inm_tipo_superficie']=='Hectareas') echo 'selected="selected"'; ?>>Hectareas</option>
									</select>
							   </div>
							</div>
							<!--Fin-->
							
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Precio</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto" name="inm_precio" id="inm_precio" maxlength="250"  size="40" value="<?php echo $_POST['inm_precio'];?>">
							   </div>
							</div>
							<!--Fin-->	
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Moneda</div>
							   <div id="CajaInput">							   
							   <select name="inm_mon_id" class="caja_texto">
							   <option value="">Seleccione</option>
							   <?php 	                                 							   
									$fun=NEW FUNCIONES;		
									$fun->combo("select mon_id as id,mon_descripcion as nombre from moneda",$_POST['inm_mon_id']);												
								?>
							   </select>							   
							   </div>
							   
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">*</span>Detalle</div>
							   <div id="CajaInput">
							        <textarea rows="4" cols="40" name="inm_detalle" id="inm_detalle"><?php echo $_POST['inm_detalle'];?></textarea>									
							   </div>
							</div>
							<!--Fin-->	
						
                    </div>

					<div id="ContenedorDiv">

                           <div id="CajaBotones">

								<center>

								<input type="submit" class="boton" name="" value="Enviar">

							    <input type="reset" class="boton" name="" value="Cancelar">

								<input type="button" class="boton" name="" value="Volver" onclick="location.href='<?php if($_GET['acc']=="MODIFICAR_PRODUCTO") echo $this->link.'?mod='.$this->modulo."&tarea=PRODUCTOS&id=".$_GET['id']; else echo $this->link.'?mod='.$this->modulo.'&tarea=ACCEDER';?>';">

								</center>

						   </div>

                        </div>

                </div>

            </div>					

		<?php
	}
	
	
	function formulario_tcp_foto($tipo)

	{

		$url=$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER';

		$re=$url;

		$enlace="<a href='$url'>Proyectos</a>";

		echo "<tr>
					<td class='busqueda_enlace' colspan='3'>
						Volver a $enlace
					</td>
				</tr>"; 		

		switch ($tipo)
		{

			case 'ver':{
						$ver=true;
						
						break;
						}

			case 'cargar':{

						$cargar=true;

						break;
						}
		}

		if(!($ver))
		{
			$url.="&tarea=".$_GET['tarea'];
		}

		if($cargar)
		{
			$url.='&id='.$_GET['id'];
		}
	
		
		$this->formulario->dibujar_tarea('FOTO');
		
		if($this->mensaje<>"")
		{
			$this->formulario->mensaje('Error',$this->mensaje);
		}

		?>


			<div id="Contenedor_NuevaSentencia">

               <form id="frm_sentencia" name="frm_sentencia" action="<?php echo $url.'&proy_fot_id='.$_GET['proy_fot_id'];?>" method="POST" enctype="multipart/form-data">  

				<div id="FormSent" style="width:900px;">

                    

					<div class="Subtitulo">Fotos</div>

                    <div id="ContenedorSeleccion">
						
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Imagen </div>
							   <div id="CajaInput">
							   <?php
								if($_POST['proy_fot_imagen']<>"")
								{	$foto=$_POST['proy_fot_imagen'];
									$b=true;
								}	
								else
								{	$foto='sin_foto.gif';
									$b=false;
								}
								if(($ver)||($cargar))
								{	?>
									<img src="imagenes/proyecto/<?php echo $foto;?>" border="0" width="50"><br>
									<input   name="proy_fot_imagen" type="file" id="proy_fot_imagen" /><span class="flechas1"></span>
									<?php
								}
								else 
								{
									?>
									<input  name="proy_fot_imagen" type="file" id="proy_fot_imagen" /><span class="flechas1"></span>
									<?php
								}
								?>
								<input   name="fotooculta" type="hidden" id="fotooculta" value="<?php echo $_POST['proy_fot_imagen'].$_POST['fotooculta'];?>"/>
							   </div>
							</div>
							<!--Fin-->
                            
                            <!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Descripcion</div>
							   <div id="CajaInput">
							   <textarea  class="caja_texto" name="proy_fot_descripcion" id="proy_fot_descripcion"><?php echo $_POST['proy_fot_descripcion'];?></textarea>   
							   </div>
							</div>
							<!--Fin-->	
							
						
                    </div>

					<div id="ContenedorDiv">

                           <div id="CajaBotones">

								<center>

								<input type="submit" class="boton" name="" value="Enviar">

							    <input type="reset" class="boton" name="" value="Cancelar">

								<input type="button" class="boton" name="" value="Volver" onclick="location.href='<?php if($_GET['acc']=="MODIFICAR_PRODUCTO") echo $this->link.'?mod='.$this->modulo."&tarea=FOTOS&id=".$_GET['id']; else echo $this->link.'?mod='.$this->modulo.'&tarea=ACCEDER';?>';">

								</center>

						   </div>

                        </div>

                </div>

            </div>					

		<?php
	}

    function formulario_tcp_foto_slideshow($tipo)
    {

        $url=$this->link.'?mod='.$this->modulo.'&tarea=SLIDESHOW';
        $re=$url;
        $enlace="<a href='$url'>Proyectos</a>";
        echo "<tr>
					<td class='busqueda_enlace' colspan='3'>
						Volver a $enlace
					</td>
				</tr>";

        switch ($tipo)
        {

            case 'ver':{
                $ver=true;

                break;
            }

            case 'cargar':{

                $cargar=true;

                break;
            }
        }

        if(!($ver))
        {
            //$url.="&id=".$_GET['id'];
        }

        if($cargar)
        {
            $url.='&acc=MODIFICAR_FOTO&id='.$_GET['id'];
        }


        $this->formulario->dibujar_tarea('FOTO');

        if($this->mensaje<>"")
        {
            $this->formulario->mensaje('Error',$this->mensaje);
        }

        ?>


        <div id="Contenedor_NuevaSentencia">

            <form id="frm_sentencia" name="frm_sentencia" action="<?php echo $url.'&proy_fot_id='.$_GET['proy_fot_id'];?>" method="POST" enctype="multipart/form-data">

                <div id="FormSent" style="width:900px;">



                    <div class="Subtitulo">Fotos</div>

                    <div id="ContenedorSeleccion">

                        <!--Inicio-->
                        <div id="ContenedorDiv">
                            <div class="Etiqueta" >Imagen </div>
                            <div id="CajaInput">
                                <?php
                                if($_POST['proy_fot_imagen']<>"")
                                {	$foto=$_POST['proy_fot_imagen'];
                                    $b=true;
                                }
                                else
                                {	$foto='sin_foto.gif';
                                    $b=false;
                                }
                                if(($ver)||($cargar))
                                {	?>
                                    <img src="imagenes/proyecto_slideshow/<?php echo $foto;?>" border="0" width="50"><br>
                                    <input   name="proy_fot_imagen" type="file" id="proy_fot_imagen" /><span class="flechas1"></span>
                                <?php
                                }
                                else
                                {
                                    ?>
                                    <input  name="proy_fot_imagen" type="file" id="proy_fot_imagen" /><span class="flechas1"></span>
                                <?php
                                }
                                ?>
                            </div>
                        </div>
                        <!--Fin-->

                        <!--Inicio-->
                        <div id="ContenedorDiv">
                            <div class="Etiqueta" >Youtube (Code)</div>
                            <div id="CajaInput">
                                <input type="text" name="youtube" id="youtube" value="<?php echo $_POST['youtube'];?>">
                            </div>
                        </div>
                        <!--Fin-->


                    </div>

                    <div id="ContenedorDiv">

                        <div id="CajaBotones">

                            <center>

                                <input type="submit" class="boton" name="" value="Enviar">

                                <input type="reset" class="boton" name="" value="Cancelar">

                                <input type="button" class="boton" name="" value="Volver" onclick="location.href='<?php if($_GET['acc']=="MODIFICAR_PRODUCTO") echo $this->link.'?mod='.$this->modulo."&tarea=FOTOS&id=".$_GET['id']; else echo $this->link.'?mod='.$this->modulo.'&tarea=ACCEDER';?>';">

                            </center>

                        </div>

                    </div>

                </div>

        </div>

    <?php
    }

	
	function formulario_tcp_tipologia($tipo)

	{

		$url=$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER';

		$re=$url;

		$enlace="<a href='$url'>Proyectos</a>";

		echo "<tr>
					<td class='busqueda_enlace' colspan='3'>
						Volver a $enlace
					</td>
				</tr>"; 		

		switch ($tipo)
		{

			case 'ver':{
						$ver=true;
						
						break;
						}

			case 'cargar':{

						$cargar=true;

						break;
						}
		}

		if(!($ver))
		{
			$url.="&tarea=".$_GET['tarea'];
		}

		if($cargar)
		{
			$url.='&id='.$_GET['id'];
		}
	
		if($_GET['acc']=='MODIFICAR_TIPOLOGIA')
		{
			$url.='&acc=MODIFICAR_TIPOLOGIA';
		}
		
		$this->formulario->dibujar_tarea('TIPOLOGIA');
		
		if($this->mensaje<>"")
		{
			$this->formulario->mensaje('Error',$this->mensaje);
		}

		?>


			<div id="Contenedor_NuevaSentencia">

               <form id="frm_sentencia" name="frm_sentencia" action="<?php echo $url.'&tip_id='.$_GET['tip_id'];?>" method="POST" enctype="multipart/form-data">  

				<div id="FormSent" style="width:900px;">

                    

					<div class="Subtitulo">Publicacion</div>

                    <div id="ContenedorSeleccion">
						
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Imagen </div>
							   <div id="CajaInput">
							   <?php
								if($_POST['tip_imagen']<>"")
								{	$foto=$_POST['tip_imagen'];
									$b=true;
								}	
								else
								{	$foto='sin_foto.gif';
									$b=false;
								}
								if(($ver)||($cargar))
								{	?>
									<img src="imagenes/proyecto/<?php echo $foto;?>" border="0" width="50"><br>
									<input   name="tip_imagen" type="file" id="tip_imagen" /><span class="flechas1"></span>
									<?php
								}
								else 
								{
									?>
									<input  name="tip_imagen" type="file" id="tip_imagen" /><span class="flechas1"></span>
									<?php
								}
								?>
								<input   name="fotooculta" type="hidden" id="fotooculta" value="<?php echo $_POST['tip_imagen'].$_POST['fotooculta'];?>"/>
							   </div>
							</div>
							<!--Fin-->
							
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Precio</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto" name="tip_precio" id="tip_precio" maxlength="250"  size="40" value="<?php echo $_POST['tip_precio'];?>">
							   </div>
							</div>
							<!--Fin-->	
							
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Descripcion</div>
							   <div id="CajaInput">
							   <textarea  class="caja_texto" name="tip_descripcion" id="tip_descripcion"><?php echo $_POST['tip_descripcion'];?></textarea>
							   </div>
							</div>
							<!--Fin-->	
							
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Disponibilidad</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto" name="tip_disponibilidad" id="tip_disponibilidad" maxlength="250"  size="40" value="<?php echo $_POST['tip_disponibilidad'];?>">
							   </div>
							</div>
							<!--Fin-->	
							
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Superficie Construida</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto" name="tip_mts" id="tip_mts" maxlength="250"  size="40" value="<?php echo $_POST['tip_mts'];?>">
							   </div>
							</div>
							<!--Fin-->	
							
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Superficie Total</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto" name="tip_terreno_mts" id="tip_terreno_mts" maxlength="250"  size="40" value="<?php echo $_POST['tip_terreno_mts'];?>">
							   </div>
							</div>
							<!--Fin-->	
							
						
                    </div>

					<div id="ContenedorDiv">

                           <div id="CajaBotones">

								<center>

								<input type="submit" class="boton" name="" value="Enviar">

							    <input type="reset" class="boton" name="" value="Cancelar">

								<input type="button" class="boton" name="" value="Volver" onclick="location.href='<?php if($_GET['acc']=="MODIFICAR_TIPOLOGIA") echo $this->link.'?mod='.$this->modulo."&tarea=TIPOLOGIA&id=".$_GET['id']; else echo $this->link.'?mod='.$this->modulo.'&tarea=ACCEDER';?>';">

								</center>

						   </div>

                        </div>

                </div>

            </div>					

		<?php
	}
	

	function dibujar_encabezado_publicacion()

	{

		?><div style="clear:both;"></div><center>

		<table class="tablaLista" cellpadding="0" cellspacing="0" width="60%">
			<thead>
				<tr>

					<th >

						Nro

					</th>
					
					<th >

						Precio

					</th>
					
					<th >

						Detalle

					</th>
								
					

					<th class="tOpciones" width="100px">

						Opciones

					</th>

				</tr>
			</thead>
			<tbody>
		<?PHP

	}
	
	function dibujar_encabezado_tipologia()

	{

		?><div style="clear:both;"></div><center>

		<table class="tablaLista" cellpadding="0" cellspacing="0" width="60%">
			<thead>
				<tr>
					
					<th >

						Descripcion

					</th>
					
					<th >

						Precio

					</th>
					
					<th >

						Imagen

					</th>
								
					

					<th class="tOpciones" width="100px">

						Opciones

					</th>

				</tr>
			</thead>
			<tbody>
		<?PHP

	}
	
	function dibujar_encabezado_foto()

	{

		?><div style="clear:both;"></div><center>

		<table class="tablaLista" cellpadding="0" cellspacing="0" width="60%">
			<thead>
				<tr>

					<th >

						Imagen

					</th>
                    <th >

						Descripcion

					</th>
					

					<th class="tOpciones" width="100px">

						Opciones

					</th>

				</tr>
			</thead>
			<tbody>
		<?PHP

	}

    function dibujar_encabezado_foto_slideshow()
    {
        ?><div style="clear:both;"></div><center>
    <table class="tablaLista" cellpadding="0" cellspacing="0" width="60%">
        <thead>
        <tr>
            <th>
                Imagen
            </th>
            <th>
                Video
            </th>
            <th class="tOpciones" width="100px">
                Opciones
            </th>
        </tr>
        </thead>
    <tbody>
    <?PHP

    }


	

	function mostrar_busqueda_tipologia()
	{

		$conec=new ADO();

		
		
		$sql="select * 
		from 
		tipologia 
		where 
		tip_proy_id='".$_GET['id']."'  order by tip_id desc";
		
		$conec->ejecutar($sql);

		$num=$conec->get_num_registros();

		for($i=0;$i<$num;$i++)
		{
			$objeto=$conec->get_objeto();

			echo '<tr class="busqueda_campos">';

			?>
				
				<td align="left">

					<?php echo $objeto->tip_descripcion; ?>

				</td>
				
				<td align="left">

					<?php echo $objeto->tip_precio; ?>
				
				</td>		
				
				<td align="left">

						<img width="100" height="100" src="imagenes/proyecto/<?php echo $objeto->tip_imagen; ?>" />

				</td>
						

				<td>

					<center>

					<a href="gestor.php?mod=proyecto&tarea=TIPOLOGIA&acc=ELIMINAR&tip_id=<?php echo $objeto->tip_id;?>&id=<?php echo $_GET['id'];?>"><img src="images/b_drop.png" alt="ELIMINAR" title="ELIMINAR" border="0"></a>

					<a href="gestor.php?mod=proyecto&tarea=TIPOLOGIA&acc=MODIFICAR_TIPOLOGIA&tip_id=<?php echo $objeto->tip_id;?>&id=<?php echo $_GET['id'];?>"><img src="images/b_edit.png" alt="MODIFICAR" title="MODIFICAR" border="0"></a>

					</center>

				</td>

			<?php

			echo "</tr>";

			

			

			$conec->siguiente();

		}

		echo "</tbody></table></center><br>";
	}
	
	function mostrar_busqueda_publicacion()
	{

		$conec=new ADO();

		
		$sql="select * 
		from 
		inmueble 
		where 
		inm_proy_id='".$_GET['id']."'  order by inm_id desc";
		
		$conec->ejecutar($sql);

		$num=$conec->get_num_registros();

		for($i=0;$i<$num;$i++)
		{
			$objeto=$conec->get_objeto();

			echo '<tr class="busqueda_campos">';

			?>
				
				<td align="left">

					<?php echo $i+1;?>

				</td>
				
				<td align="left">

					<?php echo $objeto->inm_precio; ?>
				
				</td>		
				
				<td align="left">

					&nbsp;<?php echo $objeto->inm_detalle;?>

				</td>
						

				<td>

					<center>

					<a href="gestor.php?mod=proyecto&tarea=PUBLICACIONES&acc=ELIMINAR&inm_id=<?php echo $objeto->inm_id;?>&id=<?php echo $_GET['id'];?>"><img src="images/b_drop.png" alt="ELIMINAR" title="ELIMINAR" border="0"></a>

					<a href="gestor.php?mod=proyecto&tarea=PUBLICACIONES&acc=MODIFICAR_PUBLICACION&inm_id=<?php echo $objeto->inm_id;?>&id=<?php echo $_GET['id'];?>"><img src="images/b_edit.png" alt="MODIFICAR" title="MODIFICAR" border="0"></a>

					</center>

				</td>

			<?php

			echo "</tr>";

			

			

			$conec->siguiente();

		}

		echo "</tbody></table></center><br>";
	}
	
	function mostrar_busqueda_foto()
	{

		$conec=new ADO();

		$sql="select * 
		from 
		proy_foto 
		where 
		proy_id='".$_GET['id']."'  order by proy_fot_id desc";
		
		$conec->ejecutar($sql);

		$num=$conec->get_num_registros();

		for($i=0;$i<$num;$i++)
		{
			$objeto=$conec->get_objeto();

			echo '<tr class="busqueda_campos">';

			?>
				
				<td align="left">

					<img width="100" height="100" src="imagenes/proyecto/<?php echo $objeto->proy_fot_imagen; ?>">
				
				</td>
                <td align="left">
					<?php echo $objeto->proy_fot_descripcion; ?>				
				</td>		
			
				<td>

					<center>

					<a href="gestor.php?mod=proyecto&tarea=FOTOS&acc=ELIMINAR&proy_fot_id=<?php echo $objeto->proy_fot_id;?>&id=<?php echo $_GET['id'];?>"><img src="images/b_drop.png" alt="ELIMINAR" title="ELIMINAR" border="0"></a><br />
                    <a href="gestor.php?mod=proyecto&tarea=FOTOS&acc=MODIFICAR_FOTO&proy_fot_id=<?php echo $objeto->proy_fot_id;?>&id=<?php echo $_GET['id'];?>"><img src="images/b_edit.png" alt="MODIFICAR" title="MODIFICAR" border="0"></a>

					</center>

				</td>

			<?php

			echo "</tr>";

			

			

			$conec->siguiente();

		}

		echo "</tbody></table></center><br>";
	}
	
	
	function eliminar_publicacion($id_producto)
	{
		$conec= new ADO();

		$llave=$id_producto;
				
		/*$mi=$this->nombre_imagen($llave);*/
		
		$sql="delete from inmueble where inm_id='".$id_producto."'";

		$conec->ejecutar($sql);
		
		$this->mensaje='Inmueble eliminado correctamente';
	}
	
	
	function eliminar_tipologia($id_producto)
	{
		$conec= new ADO();

		$llave=$id_producto;
				
		/*$mi=$this->nombre_imagen($llave);*/
		
		$sql="delete from tipologia where tip_id='".$id_producto."'";

		$conec->ejecutar($sql);
		
		$this->mensaje='Tipologia eliminada correctamente';
	}
	
	
	function eliminar_foto($id_producto)
	{
		$conec= new ADO();

		$llave=$id_producto;
				
		/*$mi=$this->nombre_imagen($llave);*/
		
		$sql="delete from proy_foto where proy_fot_id='".$id_producto."'";

		$conec->ejecutar($sql);
		
		$this->mensaje='Foto eliminada correctamente';
	}
	
	function nombre_imagen($id)
	{
		$conec= new ADO();
		
		$sql="select pro_foto from aplicaciones_productos where pro_id='".$id."'";
		
		$conec->ejecutar($sql);
		
		$objeto=$conec->get_objeto();
		
		return $objeto->pro_foto;
	}
		
	function modificar_publicacion()
	{		
		$conec= new ADO();

		
		$mensaje='Publicacion Modificada Correctamente';

		$sql="update inmueble set 

		inm_cat_id='".$_POST['inm_cat_id']."',
		inm_for_id='".$_POST['inm_for_id']."',
		inm_superficie='".$_POST['inm_superficie']."',
		inm_precio='".$_POST['inm_precio']."',
		inm_mon_id='".$_POST['inm_mon_id']."',
		inm_detalle='".$_POST['inm_detalle']."',
		inm_tipo_superficie='".$_POST['inm_tipo_superficie']."'
		where inm_id= '".$_GET['inm_id']."'";

		$conec->ejecutar($sql);
		
		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=PUBLICACIONES&id='.$_GET['id']);

	}
	
	function modificar_tipologia()
	{		
		$conec= new ADO();
		
		if($_FILES['tip_imagen']['name']<>"")
		{	 	 	 	 	 	 	
			 	 	 	 	
			$result=$this->subir_imagen_proyecto($nombre_archivo,$_FILES['tip_imagen']['name'],$_FILES['tip_imagen']['tmp_name']);
			
			$sql="update tipologia set 
								tip_descripcion='".$_POST['tip_descripcion']."',
								tip_precio='".$_POST['tip_precio']."',								
								tip_titulo='".$_POST['tip_titulo']."',								
								tip_disponibilidad='".$_POST['tip_disponibilidad']."',	
								tip_mts='".$_POST['tip_mts']."',
								tip_terreno_mts='".$_POST['tip_terreno_mts']."',
								tip_imagen='".$nombre_archivo."'
								where tip_id = '".$_GET['tip_id']."'";
			
			if(trim($result)<>'')
			{
					
				$this->formulario->ventana_volver($result,$this->link.'?mod='.$this->modulo.'&tarea=TIPOLOGIA&id='.$_GET['id']);
					
			}
			else
			{
				$llave=$_GET['id'];
				
				$mi=trim($_POST['fotooculta']);
				
				if($mi<>"")
				{
					$mifile="imagenes/proyecto/$mi";
				
					@unlink($mifile);
				
				}
				
				
				$conec->ejecutar($sql);
			
				$mensaje='Tipologia Modificada Correctamente!!!';
					
				$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=TIPOLOGIA&id='.$_GET['id']);
			}
		}
		else
		{
			
			$sql="update tipologia set 
								tip_descripcion='".$_POST['tip_descripcion']."',
								tip_titulo='".$_POST['tip_titulo']."',
								tip_disponibilidad='".$_POST['tip_disponibilidad']."',	
								tip_mts='".$_POST['tip_mts']."',		
								tip_terreno_mts='".$_POST['tip_terreno_mts']."',
								tip_precio='".$_POST['tip_precio']."' 
								where tip_id = '".$_GET['tip_id']."'";

			$conec->ejecutar($sql);

			$mensaje='Tipologia Modificada Correctamente!!!';

			$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=TIPOLOGIA&id='.$_GET['id']);
		}

	}
	
	function cargar_datos_publicacion()
	{
		$conec=new ADO();

		$sql="select * from inmueble where inm_id='".$_GET['inm_id']."'";

		$conec->ejecutar($sql);

		$objeto=$conec->get_objeto();
		
		$_POST['inm_cat_id']=$objeto->inm_cat_id;
		$_POST['inm_for_id']=$objeto->inm_for_id;
		$_POST['inm_superficie']=$objeto->inm_superficie;
		$_POST['inm_precio']=$objeto->inm_precio;
		$_POST['inm_mon_id']=$objeto->inm_mon_id;
		$_POST['inm_detalle']=$objeto->inm_detalle;
		$_POST['inm_tipo_superficie']=$objeto->inm_tipo_superficie;
		
	}
	
	function cargar_datos_tipologia()
	{
		$conec=new ADO();

		$sql="select * from tipologia where tip_id='".$_GET['tip_id']."'";

		$conec->ejecutar($sql);

		$objeto=$conec->get_objeto();
		
		$_POST['tip_descripcion']=$objeto->tip_descripcion;
		$_POST['tip_imagen']=$objeto->tip_imagen;
		$_POST['tip_precio']=$objeto->tip_precio;
		$_POST['tip_titulo']=$objeto->tip_titulo;
		$_POST['tip_disponibilidad']=$objeto->tip_disponibilidad;		
		$_POST['tip_mts']=$objeto->tip_mts;
		$_POST['tip_terreno_mts']=$objeto->tip_terreno_mts;
		 	
		
	}
	
	function orden_publicacion($proyecto,$accion,$ant_orden,$ida)
	{
		$conec= new ADO();
		
		if($accion=='s')
			$cad=" where pro_fam_id='".$ida."' and  pro_orden < $ant_orden order by pro_orden desc";
		else
			$cad=" where pro_fam_id='".$ida."' and  pro_orden > $ant_orden order by pro_orden asc";			

		$consulta = "
		select 
			pro_id,pro_orden 
		from 
			aplicaciones_productos
		$cad
		limit 0,1
		";	
		$conec->ejecutar($consulta);
		$num = $conec->get_num_registros();   

		if($num > 0)
		{
			$objeto=$conec->get_objeto();
			
			$nu_orden=$objeto->pro_orden;
			
			$id=$objeto->pro_id;
			
			$consulta = "update aplicaciones_productos set pro_orden='$nu_orden' where pro_id='$proyecto'";	

			$conec->ejecutar($consulta);
			
			$consulta = "update aplicaciones_productos set pro_orden='$ant_orden' where pro_id='$id'";	

			$conec->ejecutar($consulta);
		}	
	}	
	
	
	function datos_acabado()
	{

		if($_POST)
		{
			require_once('clases/validar.class.php');

			$num=0;
			
			$valores[$num]["etiqueta"]="Descripcion";
			$valores[$num]["valor"]=$_POST['aca_descripcion'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;

			$val=NEW VALIDADOR;

			$this->mensaje="";

			if($val->validar($valores))
			{
				return true;
			}
			else
			{
				$this->mensaje=$val->mensaje;

				return false;

			}

		}

			return false;

	}
	
	
	function modificar_acabado()
	{		
		$conec= new ADO();
		
		if($_FILES['aca_imagen']['name']<>"")
		{	 	 	 	 	 	 	
			 	 	 	 	
			$result=$this->subir_imagen_proyecto($nombre_archivo,$_FILES['aca_imagen']['name'],$_FILES['aca_imagen']['tmp_name']);
			
			$sql="update acabado set 
								aca_descripcion='".$_POST['aca_descripcion']."',					
								aca_imagen='".$nombre_archivo."'
								where aca_id = '".$_GET['aca_id']."'";
			
			if(trim($result)<>'')
			{
					
				$this->formulario->ventana_volver($result,$this->link.'?mod='.$this->modulo.'&tarea=ACABADO&id='.$_GET['id']);
					
			}
			else
			{
				$llave=$_GET['id'];
				
				$mi=trim($_POST['fotooculta']);
				
				if($mi<>"")
				{
					$mifile="imagenes/proyecto/$mi";
				
					@unlink($mifile);
				
				}
				
				
				$conec->ejecutar($sql);
			
				$mensaje='Acabado Modificado Correctamente!!!';
					
				$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACABADO&id='.$_GET['id']);
			}
		}
		else
		{
			
				$sql="update acabado set 
								aca_descripcion='".$_POST['aca_descripcion']."' 						
								where aca_id = '".$_GET['aca_id']."'";

			$conec->ejecutar($sql);

			$mensaje='Acabado Modificado Correctamente!!!';

			$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACABADO&id='.$_GET['id']);
		}

	}


function cargar_datos_acabado()
	{
		$conec=new ADO();

		$sql="select * from acabado where aca_id='".$_GET['aca_id']."'";

		$conec->ejecutar($sql);

		$objeto=$conec->get_objeto();
		
		$_POST['aca_descripcion']=$objeto->aca_descripcion;
		$_POST['aca_imagen']=$objeto->aca_imagen;
		
		
	}
	
function formulario_tcp_acabado($tipo)

	{

		$url=$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER';

		$re=$url;

		$enlace="<a href='$url'>Proyectos</a>";

		echo "<tr>
					<td class='busqueda_enlace' colspan='3'>
						Volver a $enlace
					</td>
				</tr>"; 		

		switch ($tipo)
		{

			case 'ver':{
						$ver=true;
						
						break;
						}

			case 'cargar':{

						$cargar=true;

						break;
						}
		}

		if(!($ver))
		{
			$url.="&tarea=".$_GET['tarea'];
		}

		if($cargar)
		{
			$url.='&id='.$_GET['id'];
		}
	
		if($_GET['acc']=='MODIFICAR_ACABADO')
		{
			$url.='&acc=MODIFICAR_ACABADO';
		}
		
		$this->formulario->dibujar_tarea('ACABADO');
		
		if($this->mensaje<>"")
		{
			$this->formulario->mensaje('Error',$this->mensaje);
		}

		?>


			<div id="Contenedor_NuevaSentencia">

               <form id="frm_sentencia" name="frm_sentencia" action="<?php echo $url.'&aca_id='.$_GET['aca_id'];?>" method="POST" enctype="multipart/form-data">  

				<div id="FormSent" style="width:900px;">

                    

					<div class="Subtitulo">Publicacion</div>

                    <div id="ContenedorSeleccion">
						
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Imagen </div>
							   <div id="CajaInput">
							   <?php
								if($_POST['aca_imagen']<>"")
								{	$foto=$_POST['aca_imagen'];
									$b=true;
								}	
								else
								{	$foto='sin_foto.gif';
									$b=false;
								}
								if(($ver)||($cargar))
								{	?>
									<img src="imagenes/proyecto/<?php echo $foto;?>" border="0" width="50"><br>
									<input   name="aca_imagen" type="file" id="aca_imagen" /><span class="flechas1"></span>
									<?php
								}
								else 
								{
									?>
									<input  name="aca_imagen" type="file" id="aca_imagen" /><span class="flechas1"></span>
									<?php
								}
								?>
								<input   name="fotooculta" type="hidden" id="fotooculta" value="<?php echo $_POST['aca_imagen'].$_POST['fotooculta'];?>"/>
							   </div>
							</div>
							<!--Fin-->							
						
							
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Descripcion</div>
							   <div id="CajaInput">
							   <textarea  class="caja_texto" name="aca_descripcion" id="aca_descripcion"><?php echo $_POST['aca_descripcion'];?></textarea>
							   </div>
							</div>
							<!--Fin-->	
							
						
                    </div>

					<div id="ContenedorDiv">

                           <div id="CajaBotones">

								<center>

								<input type="submit" class="boton" name="" value="Enviar">

							    <input type="reset" class="boton" name="" value="Cancelar">

								<input type="button" class="boton" name="" value="Volver" onclick="location.href='<?php if($_GET['acc']=="MODIFICAR_ACABADO") echo $this->link.'?mod='.$this->modulo."&tarea=ACABADO&id=".$_GET['id']; else echo $this->link.'?mod='.$this->modulo.'&tarea=ACCEDER';?>';">

								</center>

						   </div>

                        </div>

                </div>

            </div>					

		<?php
	}
	
	
	function eliminar_acabado($id_producto)
	{
		$conec= new ADO();

		$llave=$id_producto;
				
		/*$mi=$this->nombre_imagen($llave);*/
		
		$sql="delete from acabado where aca_id='".$id_producto."'";

		$conec->ejecutar($sql);
		
		$this->mensaje='Acabado eliminado correctamente';
	}

    function eliminar_foto_slideshow($id_producto)
    {
        $conec= new ADO();

        $llave=$id_producto;

        $sql="delete from proyecto_slideshow where id='".$id_producto."'";

        $conec->ejecutar($sql);

        $this->mensaje='Foto eliminada correctamente';
    }
	
	
	function guardar_acabado()
	{		
		$conec= new ADO();		
		
		$this->subir_imagen_foto($nombre_imagen,$_FILES['aca_imagen']['name'],$_FILES['aca_imagen']['tmp_name']);
		
		$sql="insert into acabado(aca_imagen,aca_descripcion,aca_proy_id) values 
									('".$nombre_imagen."','". $_POST['aca_descripcion'] ."','".$_GET['id']."')";
		
		$conec->ejecutar($sql);	
		
		$this->limpiar_acabado();		
	}
	
	function limpiar_acabado()
	{
		$_POST['aca_descripcion']="";		
		$_POST['aca_imagen']="";
		
	}
	
	function dibujar_encabezado_acabado()

	{

		?><div style="clear:both;"></div><center>

		<table class="tablaLista" cellpadding="0" cellspacing="0" width="60%">
			<thead>
				<tr>
					
					<th >

						Descripcion

					</th>					
					
					<th >

						Imagen

					</th>
								
					

					<th class="tOpciones" width="100px">

						Opciones

					</th>

				</tr>
			</thead>
			<tbody>
		<?PHP

	}
	

	function mostrar_busqueda_acabado()
	{

		$conec=new ADO();

		
		
		$sql="select * 
		from 
		acabado 
		where 
		aca_proy_id='".$_GET['id']."'  order by aca_id desc";
		
		$conec->ejecutar($sql);

		$num=$conec->get_num_registros();

		for($i=0;$i<$num;$i++)
		{
			$objeto=$conec->get_objeto();

			echo '<tr class="busqueda_campos">';

			?>
				
				<td align="left">

					<?php echo $objeto->aca_descripcion; ?>

				</td>
				
				<td align="left">

						<img width="100" height="100" src="imagenes/proyecto/<?php echo $objeto->aca_imagen; ?>" />

				</td>
						

				<td>

					<center>

					<a href="gestor.php?mod=proyecto&tarea=ACABADO&acc=ELIMINAR&aca_id=<?php echo $objeto->aca_id;?>&id=<?php echo $_GET['id'];?>"><img src="images/b_drop.png" alt="ELIMINAR" title="ELIMINAR" border="0"></a>

					<a href="gestor.php?mod=proyecto&tarea=ACABADO&acc=MODIFICAR_ACABADO&aca_id=<?php echo $objeto->aca_id;?>&id=<?php echo $_GET['id'];?>"><img src="images/b_edit.png" alt="MODIFICAR" title="MODIFICAR" border="0"></a>

					</center>

				</td>

			<?php

			echo "</tr>";

			

			

			$conec->siguiente();

		}

		echo "</tbody></table></center><br>";
	}
	
}
?>