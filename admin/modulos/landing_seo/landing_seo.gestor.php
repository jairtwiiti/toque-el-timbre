<?php
	
	require_once('landing_seo.class.php');
	//require_once('images.class.php');
	
	$objeto = new LANDING_SEO();
	//$imagenes = new IMAGENES();
	
	if(!($objeto->verificar_permisos($_GET['tarea'])))
	{
		?>
		<script>
			location.href="log_out.php";
		</script>
		<?php
	}
	
	switch ($_GET['tarea'])
	{
		case 'AGREGAR':{
						
							if($objeto->datos())
							{
								$objeto->insertar_tcp();
							}
							else 
							{
								$objeto->formulario_tcp('blanco');
							}
					
						
						
						break;}
		case 'VER':{
						$objeto->cargar_datos();
												
						$objeto->formulario_tcp('ver');
						
						break;}
		case 'MODIFICAR':{

							if($objeto->datos())
							{
								$objeto->modificar_tcp();
							}
							else 
							{
								if(!($_POST))
								{
									$objeto->cargar_datos();
								}
								$objeto->formulario_tcp('cargar');
							}
							
						break;}
		case 'ELIMINAR':{
							
								if(isset($_POST['id']))
								{
									if(trim($_POST['id'])<>"")
									{
										$objeto->eliminar_tcp();
									}
									else 
									{
										$objeto->dibujar_busqueda();
									}
								}
								else 
								{
									$objeto->formulario_confirmar_eliminacion();
								}
							
							
						break;}
		case 'ACCEDER':{
							if($_GET['acc']<>"")
							{
								$objeto->orden($_GET['cli'],$_GET['acc'],$_GET['or']);
							}
			
							$objeto->dibujar_busqueda();
							break;
						}
		
		case 'ESTADO':{
								if(isset($_POST['id']))
								{
									if(trim($_POST['id'])<>"")
									{
										$objeto->estado();
									}
									else 
									{
										$objeto->dibujar_busqueda();
									}
								}else 
								{
									$objeto->estado();
								}
							break;
						}

		
	}
		
?>