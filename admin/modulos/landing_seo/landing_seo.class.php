<?php
class LANDING_SEO extends BUSQUEDA
{
	var $formulario;
	var $mensaje;
	var $obj_languages;
	
	function LANDING_SEO()
	{
		//permisos
		$this->ele_id=171;
		
		$this->busqueda();
		
		if(!($this->verificar_permisos('AGREGAR')))
		{
			$this->ban_agregar=false;
		}
		//fin permisos
		
		$this->num_registros=14;
		
		$this->coneccion= new ADO();
		
		/*$this->arreglo_campos[0]["nombre"]="title";
		$this->arreglo_campos[0]["texto"]="Title";
		$this->arreglo_campos[0]["tipo"]="cadena";
		$this->arreglo_campos[0]["tamanio"]=40;*/
		//$this->arreglo_campos[0]["sql"]="select cat_id as codigo,cat_nombre as descripcion from categoria";
		
		$this->link='gestor.php';
		
		$this->modulo='landing_seo';
		
		$this->formulario = new FORMULARIO();
		
		$this->formulario->set_titulo('SEO');
		
		
	}
	
	
	function dibujar_busqueda()
	{
		
		$this->formulario->dibujar_cabecera();
		
		$this->dibujar_listado();
	}
	
		
	function set_opciones()
	{
				
		$nun=0;
		
		if($this->verificar_permisos('VER'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='VER';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_search.png';
			$this->arreglo_opciones[$nun]["nombre"]='VIEW';
			$nun++;
		}
		
		if($this->verificar_permisos('MODIFICAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='MODIFICAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_edit.png';
			$this->arreglo_opciones[$nun]["nombre"]='UPDATE';
			$nun++;
		}
				
		if($this->verificar_permisos('ELIMINAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='ELIMINAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_drop.png';
			$this->arreglo_opciones[$nun]["nombre"]='DELETE';
			$nun++;
		}
		
		if($this->verificar_permisos('ESTADO'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='ESTADO';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_active.png';
			$this->arreglo_opciones[$nun]["imagen_inactive"]='images/b_inactive.png';
			$this->arreglo_opciones[$nun]["nombre"]='ACTIVE STATE';
			$this->arreglo_opciones[$nun]["nombre_inactive"]='INACTIVE STATE';
			$nun++;
		}
			
		
	}
	
	function dibujar_listado()
	{
		$sql="SELECT * FROM landing_seo ";

		$this->set_sql($sql);
		
		$this->set_opciones();
		
		$this->dibujar();
		
	}
	
	function dibujar_encabezado()
	{
		?>
			<tr>
	        	<th >Title</th>
                <th width="20%">Precio desde</th>
                <th width="20%">Categoria</th>
				<th class="tOpciones" width="10%">Opciones</th>
			</tr>
			
		<?PHP
	}
	
	function mostrar_busqueda()
	{
		$conversor = new convertir();
		
		for($i=0;$i<$this->numero;$i++)
			{
				
				$objeto=$this->coneccion->get_objeto();
				echo '<tr>';
					echo "<td>";
						echo $objeto->titulo;
					echo "&nbsp;</td>";
                    echo "<td>";
                        echo $objeto->precio;
                    echo "&nbsp;</td>";
                    echo "<td>";
                        echo $objeto->categoria;
                    echo "&nbsp;</td>";
					echo "<td>";
						echo $this->get_opciones($objeto->id);
					echo "</td>";
				echo "</tr>";
				
				$this->coneccion->siguiente();
			}
	}
	

	function cargar_datos()
	{
		$conec=new ADO();
		
		$sql="select * from landing_seo
				where id = '".$_GET['id']."'";
		
		$conec->ejecutar($sql);		
		$objeto=$conec->get_objeto();
		 	 	 	 	 	 	 	 	 
		$_POST['titulo'] = $objeto->titulo;
		$_POST['categoria'] = $objeto->categoria;
		$_POST['imagen'] = $objeto->imagen;
        $_POST['precio'] = $objeto->precio;
		$_POST['seo'] = $objeto->seo;
        $_POST['config'] = $objeto->config;
	}

	function datos()
	{
		if($_POST)
		{
			//texto,  numero,  real,  fecha,  mail.
			$num=0;
			$valores[$num]["etiqueta"]="Titulo";
			$valores[$num]["valor"]=$_POST['titulo'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;

			$val=NEW VALIDADOR;
			
			$this->mensaje="";
			
			if($val->validar($valores))
			{
				return true;
			}
				
			else
			{
				$this->mensaje=$val->mensaje;
				return false;
			}
		}
			return false;
	}
	
	function datos_aux()
	{
		if($_POST)
		{
			//texto,  numero,  real,  fecha,  mail.
			$num=0;
			/*$valores[$num]["etiqueta"]="Title";
			$valores[$num]["valor"]=$_POST['title'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;*/

			$val=NEW VALIDADOR;
			
			$this->mensaje="";
			
			if($val->validar($valores))
			{
				return true;
			}
				
			else
			{
				$this->mensaje=$val->mensaje;
				return false;
			}
		}
			return false;
	}
	
	
	function formulario_tcp($tipo)
	{
				switch ($tipo)
				{
					case 'ver':{
								$ver=true;
								break;
								}
							
					case 'cargar':{
								$cargar=true;
								break;
								}
				}
				
				$url=$this->link.'?mod='.$this->modulo;
				
				$red=$url.'&tarea=ACCEDER';
				
				if(!($ver))
				{
					$url.="&tarea=".$_GET['tarea'];
				}
				
				if($cargar)
				{
					$url.='&id='.$_GET['id'];
				}

		
		    $this->formulario->dibujar_tarea('CONTENIDO');
		
			if($this->mensaje<>"")
			{
                $this->formulario->mensaje('Error',$this->mensaje);
			}
			
			?>
		<div id="Contenedor_NuevaSentencia">
			<form id="frm_sentencia" name="frm_sentencia" action="<?php echo $url;?><?php echo $_GET["acc"] != "" ? "&acc=".$_GET["acc"] : ""; ?>" method="POST" enctype="multipart/form-data">  
				<div id="FormSent">
				  
					<div class="Subtitulo">Data</div>
						<div id="ContenedorSeleccion">
							
							
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">* </span>Titutlo</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto" name="titulo" id="titulo" maxlength="250"  size="60" value="<?php echo $_POST['titulo']; ?>">
							   </div>
							</div>
							<!--Fin-->

                            <!--Inicio-->
                            <div id="ContenedorDiv">
                                <div class="Etiqueta" ><span class="flechas1">* </span>Categoria</div>
                                <div id="CajaInput">
                                    <select name="categoria">
                                        <option value="Compra">Compra</option>
                                        <option value="Alquiler">Alquiler</option>
                                        <option value="Anticretico">Anticretico</option>
                                    </select>
                                </div>
                            </div>
                            <!--Fin-->

                            <!--Inicio-->
                            <div id="ContenedorDiv">
                                <div class="Etiqueta" ><span class="flechas1">* </span>Precio</div>
                                <div id="CajaInput">
                                    <input type="text" class="caja_texto" name="precio" id="precio" maxlength="250"  size="60" value="<?php echo $_POST['precio']; ?>">
                                </div>
                            </div>
                            <!--Fin-->

							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Imagen</div>
							   <div id="CajaInput">
							   		<?php 
							   		if($_POST["imagen"] != ""){
							   			echo $_POST["imagen"] != "" ? '<p><a href="imagenes/landing_seo/'.$_POST["imagen"].'" target="_blank"><img src="imagenes/landing_seo/'.$_POST["imagen"].'" width="150" /></a></p>' : '';
							   		}
							   		?>
									<input  name="file" type="file" id="file" /> Dimensions: 199 x 176 px
							   </div>
							</div>							
							<!--Fin-->


                            <?php
                            if(empty($_POST["config"])){
                                $parametros = array(
                                    "search" => "",
                                    "type" => null,
                                    "in" => null,
                                    "city" => null,
                                    "price_min" => "",
                                    "price_max" => ""
                                );
                                $_POST["config"] = json_encode($parametros);
                            }
                            $config = $_POST["config"];
                            $config = json_decode($config);
                            ?>

                            <!--Inicio-->
                            <div id="ContenedorDiv">
                                <div class="Etiqueta" ><span class="flechas1">* </span>Palabra Clave</div>
                                <div id="CajaInput">
                                    <input type="text" class="caja_texto" name="search" id="search" maxlength="250"  size="60" value="<?php echo $config->search; ?>">
                                </div>
                            </div>
                            <!--Fin-->

                            <!--Inicio-->
                            <div id="ContenedorDiv">
                                <div class="Etiqueta" ><span class="flechas1">* </span>Tipo</div>
                                <div id="CajaInput">
                                    <select name="tipo">
                                        <option value="">Seleccione</option>
                                        <?php
                                        $fun=NEW FUNCIONES;
                                        $fun->combo("SELECT cat_nombre as id, cat_nombre as nombre FROM categoria ",strtoupper($config->type));
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <!--Fin-->

                            <!--Inicio-->
                            <div id="ContenedorDiv">
                                <div class="Etiqueta" ><span class="flechas1">* </span>Forma</div>
                                <div id="CajaInput">
                                    <select name="in">
                                        <option value="">Seleccione</option>
                                        <?php
                                        $fun=NEW FUNCIONES;
                                        $fun->combo("SELECT for_descripcion as id, for_descripcion as nombre FROM forma ",strtoupper($config->in));
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <!--Fin-->

                            <!--Inicio-->
                            <div id="ContenedorDiv">
                                <div class="Etiqueta" ><span class="flechas1">* </span>Ciudad</div>
                                <div id="CajaInput">
                                    <select name="city">
                                        <option value="">Seleccione</option>
                                        <?php
                                        $fun=NEW FUNCIONES;
                                        $fun->combo("SELECT dep_nombre as id, dep_nombre as nombre FROM departamento ",$config->city);
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <!--Fin-->

                            <!--Inicio-->
                            <div id="ContenedorDiv">
                                <div class="Etiqueta" ><span class="flechas1">* </span>Precio Minimo</div>
                                <div id="CajaInput">
                                    <input type="text" class="caja_texto" name="price_min" id="price_min" maxlength="250"  size="60" value="<?php echo $config->price_min; ?>">
                                </div>
                            </div>
                            <!--Fin-->

                            <!--Inicio-->
                            <div id="ContenedorDiv">
                                <div class="Etiqueta" ><span class="flechas1">* </span>Precio Maximo</div>
                                <div id="CajaInput">
                                    <input type="text" class="caja_texto" name="price_max" id="price_max" maxlength="250"  size="60" value="<?php echo $config->price_max; ?>">
                                </div>
                            </div>
                            <!--Fin-->


                            <?php if(false): ?>
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta">Seo Url</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto" name="seo_url" id="new_title" maxlength="250"  size="60" value="<?php echo $_POST['seo']; ?>">
							   </div>
							</div>
							<!--Fin-->
                            <?php endif; ?>

														
						</div>
					
						<div id="ContenedorDiv">
						   <div id="CajaBotones">
								<center>
								<?php
								if(!($ver))
								{
									?>
									<input type="submit" class="boton" name="" value="Guardar">
									<input type="button" class="boton" name="" value="Atras" onclick="javascript:location.href='<?php echo $red;?>';">
									<?php
								}
								else
								{
									?>
									<input type="button" class="boton" name="" value="Atras" onclick="javascript:location.href='<?php echo $red;?>';">
									<?php
								}
								?>
								</center>
						   </div>
						</div>
				</div>
			</form>
		</div>
		<?php
	}
	
	function insertar_tcp()
	{
		$result=$this->subir_imagen($nombre_archivo,$_FILES['file']['name'],$_FILES['file']['tmp_name']);
		
		$conec= new ADO();		

		/*$sql="select max(orden) as ultimo from landing_seo";
		$conec->ejecutar($sql);
		$objeto=$conec->get_objeto();
		$orden=$objeto->ultimo + 1;*/

        $parametros = array(
            "search" => $_POST["search"],
            "type" => ucwords(strtolower($_POST["tipo"])),
            "in" => ucwords(strtolower($_POST["in"])),
            "city" => $_POST["city"],
            "room" => null,
            "bathroom" => null,
            "parking" => null,
            "currency" => "USD",
            "price_min" => $_POST["price_min"],
            "price_max" => $_POST["price_max"]
        );
        $config = json_encode($parametros);


		$sql="insert into landing_seo(titulo,categoria,imagen,seo,precio,fecha,config) values(
			'".$_POST["titulo"]."',
			'".$_POST["categoria"]."',
			'".$nombre_archivo."',
			'".$this->seo_url($_POST["titulo"])."',
			'".$_POST["precio"]."',
			'".date("Y-m-d H:i:s")."',
			'".$config."'
		)";
		
		if(trim($result)<>'' && $_FILES['file']['name']<>"")
		{
			$this->formulario->ventana_volver($result,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
		}
		else 
		{
            $conec->ejecutar($sql);
            $mensaje='Agregado Correctamente';
			$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
		}
	}
	

	function modificar_tcp()
	{
		$conec= new ADO();		
		$result=$this->subir_imagen($nombre_archivo,$_FILES['file']['name'],$_FILES['file']['tmp_name']);
		
		if(trim($result)<>'' && $_FILES['file']['name']<>"")
		{
			$this->formulario->ventana_volver($result,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
		}
		else
		{
            $parametros = array(
                "search" => $_POST["search"],
                "type" => ucwords(strtolower($_POST["tipo"])),
                "in" => ucwords(strtolower($_POST["in"])),
                "city" => $_POST["city"],
                "room" => null,
                "bathroom" => null,
                "parking" => null,
                "currency" => "USD",
                "price_min" => $_POST["price_min"],
                "price_max" => $_POST["price_max"]
            );
            $config = json_encode($parametros);
			
			if($_FILES['file']['name'] != ""){
				$this->eliminar_archivo($_GET['id']);
				
				$sql="update landing_seo set
				imagen = '".$nombre_archivo."',
				titulo = '".$_POST["titulo"]."',
				seo = '".$this->seo_url($_POST["titulo"])."',
				categoria = '".$_POST["categoria"]."',
				precio = '".$_POST["precio"]."',
				config = '".$config."'
				where id = '".$_GET['id']."'";
			}else{
                $sql="update landing_seo set
				titulo = '".$_POST["titulo"]."',
				seo = '".$this->seo_url($_POST["titulo"])."',
				categoria = '".$_POST["categoria"]."',
				precio = '".$_POST["precio"]."',
				config = '".$config."'
				where id = '".$_GET['id']."'";
			}
			$conec->ejecutar($sql);
            $mensaje = 'Modificado Correctamente';
			$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
		}
		
	}
	

	function formulario_confirmar_eliminacion()
	{
		$mensaje='Really remove this content?';		
		$this->formulario->ventana_confirmacion($mensaje,$this->link."?mod=$this->modulo".'&tarea=ELIMINAR','id');
	}
	
	function eliminar_tcp()
	{
		$conec= new ADO();
		
		$archivo = $this->obtener_nombre_archivo($_POST['id']);
		
		if(file_exists("./imagenes/landing_seo/".$archivo)){
			unlink("./imagenes/landing_seo/".$archivo);
		}
		
		$sql="
		delete from landing_seo WHERE id = ".$_POST['id'];
		$conec->ejecutar($sql);
		
		$mensaje='Eliminado correctamente!!!';
		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
	}
		
	function eliminar_archivo($id)
	{
		$archivo = $this->obtener_nombre_archivo($id);
		
		if(file_exists("../../imagenes/landing_seo/".$archivo)){
			unlink("../../imagenes/landing_seo/".$archivo);
		}
		
	}
	
	function obtener_nombre_archivo($id)
	{
		$conec= new ADO();
		
		$sql="select imagen from landing_seo where id = ".$id;
		$conec->ejecutar($sql);
		$nombre = $conec->get_objeto();
		return $nombre->imagen;
	}
	
	/*----------------------------------------------------------------------------*/
	/*--FUNCIONES DEL MODULO--*/
	
	function seo_url($texto){
		$spacer = "-";
		$texto = trim($texto);
		$texto = strtolower($texto);
		$texto = trim(ereg_replace("[^ A-Za-z0-9_]", " ", $texto));
		$texto = ereg_replace("[ \t\n\r]+", "-", $texto);
		$texto = str_replace(" ", $spacer, $texto);
		$texto = ereg_replace("[ -]+", "-",$texto);
		return $texto;
	}
	
	function subir_imagen(&$nombre_imagen,$name,$tmp)
	{	
		 require_once('clases/upload.class.php');

		 $nn=date('d_m_Y_H_i_s_').rand();
		 $upload_class = new Upload_Files();
		 $upload_class->temp_file_name = trim($tmp); 	
		 $upload_class->file_name = $nn.substr(trim($name), -4, 4);
		 $nombre_imagen=$upload_class->file_name;		 		 
		 $upload_class->upload_dir = "imagenes/landing_seo";
		 $upload_class->upload_log_dir = "imagenes/landing_seo/upload_logs/";
		 $upload_class->max_file_size = 1048576; 	
		 $upload_class->ext_array = array(".jpg",".jpeg",".gif",".png", ".JPG",".JPEG",".GIF",".PNG");
         $upload_class->crear_thumbnail=false;
		 $valid_ext = $upload_class->validate_extension(); 
		 $valid_size = $upload_class->validate_size(); 
		 $valid_user = $upload_class->validate_user(); 
		 $max_size = $upload_class->get_max_size(); 
		 $file_size = $upload_class->get_file_size(); 
		 $file_exists = $upload_class->existing_file();

		if (!$valid_ext) { 				
			   
			$result = "La Extension de este Archivo es invalida, Intente nuevamente por favor!"; 
		}elseif (!$valid_size) {
			
			$result = "El Tama�o de este archivo es invalido, El maximo tama�o permitido es: $max_size y su archivo pesa: $file_size"; 
		}elseif ($file_exists) {
			
			$result = "El Archivo Existe en el Servidor, Intente nuevamente por favor.";
		}else{		    
			
			$upload_file = $upload_class->upload_file_with_validation(); 

			if (!$upload_file) {
				$result = "Su archivo no se subio correctamente al Servidor.";
			}else{ 
					$result = "";

					require_once('clases/class.upload.php');

					$handle = new upload($mifile);

					if ($handle->uploaded) 
					{
					    /*$handle->image_resize          = true;
						$handle->image_ratio           = true;
						$handle->image_y               = 1100;
						$handle->image_x               = 283;
						$handle->process('imagenes/landing_seo_education/small/');*/

						if (!($handle->processed)) 
						{
					       echo 'error : ' . $handle->error;
					    }

					}
			} 
		} 	

		return $result;	

	}
	
	
}
?>