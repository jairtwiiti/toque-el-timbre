<?php
	
	require_once('equipo.class.php');
	
	$equipo = new EQUIPO();
	
	if(!($equipo->verificar_permisos($_GET['tarea'])))
	{
		?>
		<script>
			location.href="log_out.php";
		</script>
		<?php
	}
	
	switch ($_GET['tarea'])
	{
		case 'AGREGAR':{
						
							if($equipo->datos())
							{
								$equipo->insertar_tcp();
							}
							else 
							{
								$equipo->formulario_tcp('blanco');
							}
					
						
						
						break;}
		case 'VER':{
						$equipo->cargar_datos();
												
						$equipo->formulario_tcp('ver');
						
						break;}
		case 'MODIFICAR':{
							if($_GET['acc']=='Imagen')
							{
								$equipo->eliminar_imagen();
							}
							else
							{
								if($equipo->datos())
								{
									$equipo->modificar_tcp();
								}
								else 
								{
									if(!($_POST))
									{
										$equipo->cargar_datos();
									}
									$equipo->formulario_tcp('cargar');
								}
							}
						break;}
		case 'ELIMINAR':{
							
								if(isset($_POST['equ_id']))
								{
									if(trim($_POST['equ_id'])<>"")
									{
										$equipo->eliminar_tcp();
									}
									else 
									{
										$equipo->dibujar_busqueda();
									}
								}
								else 
								{
									$equipo->formulario_confirmar_eliminacion();
								}
							
							
						break;}
		case 'ACCEDER':{	
						
							$equipo->dibujar_busqueda();
							break;
						}
		
	}
		
?>