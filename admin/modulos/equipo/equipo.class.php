<?php

class EQUIPO extends BUSQUEDA 
{
	var $formulario;
	var $mensaje;
	
	function EQUIPO()
	{
		//permisos
		$this->ele_id=140;
		
		$this->busqueda();
		
		if(!($this->verificar_permisos('AGREGAR')))
		{
			$this->ban_agregar=false;
		}
		//fin permisos
		
		$this->num_registros=14;
		
		$this->coneccion= new ADO();
		
		$this->arreglo_campos[0]["nombre"]="equ_nombre";
		$this->arreglo_campos[0]["texto"]="Nombre";
		$this->arreglo_campos[0]["tipo"]="cadena";
		$this->arreglo_campos[0]["tamanio"]=30;
		
		$this->link='gestor.php';
		
		$this->modulo='equipo';
		
		$this->formulario = new FORMULARIO();
		
		$this->formulario->set_titulo('EQUIPO');
		
		
	}
	
	
	function dibujar_busqueda()
	{
		
		$this->formulario->dibujar_cabecera();
		
		$this->dibujar_listado();
	}
	
		
	function set_opciones()
	{
				
		$nun=0;
		
		if($this->verificar_permisos('VER'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='VER';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_search.png';
			$this->arreglo_opciones[$nun]["Nombre (es)"]='VER';
			$nun++;
		}
		
		if($this->verificar_permisos('MODIFICAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='MODIFICAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_edit.png';
			$this->arreglo_opciones[$nun]["Nombre (es)"]='MODIFICAR';
			$nun++;
		}
		
		if($this->verificar_permisos('ELIMINAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='ELIMINAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_drop.png';
			$this->arreglo_opciones[$nun]["Nombre (es)"]='ELIMINAR';
			$nun++;
		}
	}
	
	function dibujar_listado()
	{
		$sql="SELECT equ_nombre,mar_nombre,mod_nombre,equ_chasis,equ_ano FROM equipo INNER JOIN modelo ON (equ_mod_id = mod_id)
                                   INNER JOIN marca ON (mod_mar_id = mar_id)
								   INNER JOIN categoria ON (equ_cat_id = cat_id)";
		
		$this->set_sql($sql,'');
		
		$this->set_opciones();
		
		$this->dibujar();
		
	}
	
	function dibujar_encabezado()
	{
		?>
			<tr>
				<th>Nombre</th>
				<th>Marca</th>
				<th>Modelo</th>
				<th>Chasis</th>
				<th>A�o</th>
	            <th class="tOpciones" width="100px">Opciones</th>
			</tr>
			
		<?PHP
	}
	
	function mostrar_busqueda()
	{
		$conversor = new convertir();
		
		for($i=0;$i<$this->numero;$i++)
			{
				
				$objeto=$this->coneccion->get_objeto();
				echo '<tr>';
					echo "<td>";
						echo $objeto->equ_nombre;
					echo "&nbsp;</td>";
					echo "<td>";
						echo $objeto->mar_nombre;
					echo "&nbsp;</td>";
					echo "<td>";
						echo $objeto->mod_nombre;
					echo "&nbsp;</td>";
					echo "<td>";
						echo $objeto->equ_chasis;
					echo "&nbsp;</td>";
					echo "<td>";
						echo $objeto->equ_ano;
					echo "&nbsp;</td>";
					echo "<td>";
						echo $this->get_opciones($objeto->equ_id);
					echo "</td>";
				echo "</tr>";
				
				$this->coneccion->siguiente();
			}
	}
	
	function cargar_datos()
	{
		$conec=new ADO();
		
		$sql="select * from equipo
				where equ_id = '".$_GET['id']."'";
		
		$conec->ejecutar($sql);
		
		$objeto=$conec->get_objeto();
		 	 	 	 	 	 	 	 	 
		$_POST['equ_nombre']=$objeto->equ_nombre;		
		$_POST['equ_foto']=$objeto->equ_foto;		
		$_POST['equ_mod_id']=$objeto->equ_mod_id;		
		$_POST['equ_chasis']=$objeto->equ_chasis;		
		$_POST['equ_ano']=$objeto->equ_ano;		
		$_POST['equ_poliza']=$objeto->equ_poliza;		
		$_POST['equ_detalle']=$objeto->equ_detalle;
		$_POST['equ_cat_id']=$objeto->equ_cat_id;
	}
	
	function datos()
	{
		if($_POST)
		{
			//texto,  numero,  real,  fecha,  mail.
			$num=0;
			$valores[$num]["etiqueta"]="Nombre";
			$valores[$num]["valor"]=$_POST['equ_nombre'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			$valores[$num]["etiqueta"]="Marca";
			$valores[$num]["valor"]=$_POST['equ_mar_id'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			
			$valores[$num]["etiqueta"]="Modelo";
			$valores[$num]["valor"]=$_POST['equ_mod_id'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			
			$valores[$num]["etiqueta"]="Chasis";
			$valores[$num]["valor"]=$_POST['equ_chasis'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			
			$valores[$num]["etiqueta"]="A�o";
			$valores[$num]["valor"]=$_POST['equ_ano'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			
			$val=NEW VALIDADOR;
			
			$this->mensaje="";
			
			if($val->validar($valores))
			{
				return true;
			}
				
			else
			{
				$this->mensaje=$val->mensaje;
				return false;
			}
		}
			return false;
	}
	
	function formulario_tcp($tipo)
	{
				switch ($tipo)
				{
					case 'ver':{
								$ver=true;
								break;
								}
							
					case 'cargar':{
								$cargar=true;
								break;
								}
				}
				
				$url=$this->link.'?mod='.$this->modulo;
				
				$red=$url.'&tarea=ACCEDER';
				
				if(!($ver))
				{
					$url.="&tarea=".$_GET['tarea'];
				}
				
				if($cargar)
				{
					$url.='&id='.$_GET['id'];
				}

		
		    $this->formulario->dibujar_tarea('equipo');
		
			if($this->mensaje<>"")
			{
				$this->formulario->dibujar_mensaje($this->mensaje);
			}
			?>
			
			<script>
			      function cargar_modelo(id)
					{
						var	valores="tid="+id;			
						ejecutar_ajax('ajax.php','modelo',valores,'POST');									
					}
			</script>
		<div id="Contenedor_NuevaSentencia">
			<form id="frm_sentencia" name="frm_sentencia" action="<?php echo $url;?>" method="POST" enctype="multipart/form-data">  
				<div id="FormSent">
				  
					<div class="Subtitulo">Datos</div>
						<div id="ContenedorSeleccion">
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">*</span>Nombre</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto" name="equ_nombre" id="equ_nombre" maxlength="250"  size="30" value="<?php echo $_POST['equ_nombre'];?>">
							   </div>
							</div>
							<!--Fin-->							
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">*</span>Marca</div>
							   <div id="CajaInput">
							   <select name="equ_mar_id" class="caja_texto" onchange="cargar_modelo(this.value);">
							   <option value="">Seleccione</option>
							   <?php 		
								$fun=NEW FUNCIONES;		
								$fun->combo("select mar_id as id,mar_nombre as nombre from marca",$_POST['equ_mar_id']);				
								?>
							   </select>
							   </div>
							   
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">*</span>Modelo</div>
							   <div id="CajaInput">
							   <div id="ciudad">
							   <select name="equ_ciu_id" class="caja_texto">
							   <option value="">Seleccione</option>
							   <?php 
                                if ($_POST["equ_dep_id"])							   
								 {							   
									$fun=NEW FUNCIONES;		
									$fun->combo("select ciu_id as id,ciu_nombre as nombre from ciudad",$_POST['equ_ciu_id']);				
								}
								?>
							   </select>
							   </div>
							   </div>
							   
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Banner</div>
							   <div id="CajaInput">
							   <?php
								if($_POST['equ_banner']<>"")
								{	$foto=$_POST['equ_banner'];
									$b=true;
								}	
								else
								{	$foto='sin_foto.gif';
									$b=false;
								}
								if(($ver)||($cargar))
								{	?>
									<img src="multimedia/<?php echo $foto;?>" border="0"><?php if($b and $_GET['tarea']=='MODIFICAR') echo '<a href="'.$this->link.'?mod='.$this->modulo.'&tarea=MODIFICAR&id='.$_GET['id'].'&img='.$foto.'&acc=Imagen&c=equ_banner"><img src="images/b_drop.png" border="0"></a>';?><br>
									<input  name="equ_banner" type="file" id="equ_banner" /><span class="flechas1">&nbsp;&nbsp;Tama�o (645 X 100)</span>
									<?php
								}
								else 
								{
									?>
									<input  name="equ_banner" type="file" id="equ_banner" /><span class="flechas1">&nbsp;&nbsp;Tama�o (645 X 100)</span>
									<?php
								}
								?>
								<input   name="fotooculta2" type="hidden" id="fotooculta2" value="<?php echo $_POST['equ_banner'].$_POST['fotooculta2'];?>"/>
							   </div>
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">* </span>Descripci�n (es)</div>
							   <div id="CajaInput">
							   <textarea class="caja_texto" name="equ_descripcion_es" id="equ_descripcion_es" cols="50" rows="3"><?php echo $_POST['equ_descripcion_es']?></textarea>
							   </div>
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Descripci�n (en)</div>
							   <div id="CajaInput">
							   <textarea class="caja_texto" name="equ_descripcion_en" id="equ_descripcion_en" cols="50" rows="3"><?php echo $_POST['equ_descripcion_en']?></textarea>
							   </div>
							</div>
							<!--Fin-->
						</div>
					
						<div id="ContenedorDiv">
						   <div id="CajaBotones">
								<center>
								<?php
								if(!($ver))
								{
									?>
									<input type="submit" class="boton" name="" value="Guardar">
									<input type="reset" class="boton" name="" value="Cancelar">
									<input type="button" class="boton" name="" value="Volver" onclick="javascript:location.href='<?php echo $red;?>';">
									<?php
								}
								else
								{
									?>
									<input type="button" class="boton" name="" value="Volver" onclick="javascript:location.href='<?php echo $red;?>';">
									<?php
								}
								?>
								</center>
						   </div>
						</div>
				</div>
			</form>
		</div>
		<?php
	}
	
	function insertar_tcp()
	{
		$conec= new ADO();
				
		if($_FILES['equ_banner']['name']<>"")
		{
			$nombre1="";
			$nombre2="";
			
			if($_FILES['equ_banner']['name']<>"")
			{
				$result2=$this->subir_imagen($nombre2,$_FILES['equ_banner']['name'],$_FILES['equ_banner']['tmp_name']);
			}
			
			$sql="insert into equipo (equ_nombre_es,equ_nombre_en,equ_banner,equ_descripcion_es,equ_descripcion_en,equ_dep_id,equ_ciu_id) values 
								('".$_POST['equ_nombre_es']."','".$_POST['equ_nombre_en']."','".$nombre2."','".$_POST['equ_descripcion_es']."','".$_POST['equ_descripcion_en']."','".$_POST['equ_dep_id']."','".$_POST['equ_ciu_id']."')";
			
			if(trim($result1)<>'' || trim($result2)<>'')
			{
				$this->formulario->ventana_volver($result,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
			}
			else 
			{
			    
				$conec->ejecutar($sql);
				
				$mensaje='Lugar Popular Agregado Correctamente!!!';
				
				$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
			}
		}
		else
		{
			$sql="insert into equipo (equ_nombre_es,equ_nombre_en,equ_descripcion_es,equ_descripcion_en,equ_dep_id,equ_ciu_id) values 
								('".$_POST['equ_nombre_es']."','".$_POST['equ_nombre_en']."','".$_POST['equ_descripcion_es']."','".$_POST['equ_descripcion_en']."','".$_POST['equ_dep_id']."','".$_POST['equ_ciu_id']."')";
								
			$conec->ejecutar($sql);

			$mensaje='Lugar Popular Agregado Correctamente!!!';

			$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
		}
	}
	
	function subir_imagen(&$nombre,$name,$tmp)
	{	
		 require_once('clases/upload.class.php');

		 $nn=date('d_m_Y_H_i_s_').rand();

		 $upload_class = new Upload_Files();

		 $upload_class->temp_file_name = trim($tmp); 	
		 
		 $upload_class->file_name = $nn.substr(trim($name), -4, 4);

		 $nombre=$upload_class->file_name;		 		 

		 $upload_class->upload_dir = "multimedia/"; 

		 $upload_class->upload_log_dir = "multimedia/upload_logs/"; 

		 $upload_class->max_file_size = 1048576; 	

		 $upload_class->ext_array = array(".jpg",".gif",".png");

         $upload_class->crear_thumbnail=false;

		 $valid_ext = $upload_class->validate_extension(); 

		 $valid_size = $upload_class->validate_size(); 

		 $valid_user = $upload_class->validate_user(); 

		 $max_size = $upload_class->get_max_size(); 

		 $file_size = $upload_class->get_file_size(); 

		 $file_exists = $upload_class->existing_file(); 		

		if (!$valid_ext) { 				   

			$result = "La Extension de este Archivo es invalida, Intente nuevamente por favor!"; 

		} 

		elseif (!$valid_size) { 

			$result = "El Tama�o de este archivo es invalido, El maximo tama�o permitido es: $max_size y su archivo pesa: $file_size"; 

		}    

		elseif ($file_exists) { 

			$result = "El Archivo Existe en el Servidor, Intente nuevamente por favor."; 

		} 

		else 
		{		    
			$upload_file = $upload_class->upload_file_with_validation(); 

			if (!$upload_file) { 

				$result = "Su archivo no se subio correctamente al Servidor."; 
			}

			else 
			{ 
					$result = "";
			} 
		} 	

		return $result;	

	}
	
	function modificar_tcp()
	{
		$conec= new ADO();
		
		if($_FILES['equ_banner']['name']<>"")
		{	 	 	 	 	 	 	
			$sql="update equipo set 
							equ_nombre_es='".$_POST['equ_nombre_es']."',
							equ_nombre_en='".$_POST['equ_nombre_en']."',
							equ_descripcion_es='".$_POST['equ_descripcion_es']."',
							equ_dep_id='".$_POST['equ_dep_id']."',
							equ_ciu_id='".$_POST['equ_ciu_id']."',
							equ_descripcion_en='".$_POST['equ_descripcion_en']."'
							where equ_id = '".$_GET['id']."'";
						
			$conec->ejecutar($sql); 	 	 	 	
			
			if($_FILES['equ_banner']['name']<>"")
			{
				$result=$this->subir_imagen($nombre,$_FILES['equ_banner']['name'],$_FILES['equ_banner']['tmp_name']);
				
				if(trim($result)<>'')
				{
						
					$this->formulario->ventana_volver($result,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
						
				}
				else
				{
					$mi=trim($_POST['fotooculta2']);
					
					if($mi<>"")
					{
						$mifile="multimedia/$mi";
						
						@unlink($mifile);
					}
		
					$sql="update equipo set 
							equ_banner='".$nombre."'
							where equ_id = '".$_GET['id']."'";
					
					$conec->ejecutar($sql);
					
				}
			}
			
			$mensaje='Lugar Popular Modificado Correctamente!!!';
				
			$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
			
		}
		else
		{
			
			$sql="update equipo set 
						equ_nombre_es='".$_POST['equ_nombre_es']."',
						equ_nombre_en='".$_POST['equ_nombre_en']."',
						equ_dep_id='".$_POST['equ_dep_id']."',
						equ_ciu_id='".$_POST['equ_ciu_id']."',
						equ_descripcion_es='".$_POST['equ_descripcion_es']."',
						equ_descripcion_en='".$_POST['equ_descripcion_en']."'
						where equ_id = '".$_GET['id']."'";
			
			$conec->ejecutar($sql);

			$mensaje='Lugar Popular Modificado Correctamente!!!';

			$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
		}
	}
	
	function formulario_confirmar_eliminacion()
	{
		
		$mensaje='Esta seguro de eliminar el lugar?';
		
		$this->formulario->ventana_confirmacion($mensaje,$this->link."?mod=$this->modulo".'&tarea=ELIMINAR','equ_id');
	}
	
	function eliminar_tcp()
	{
	
		$llave=$_POST['equ_id'];
		
		$mi2=$this->nombre_imagen($llave,'equ_banner');
		
		if(trim($mi2)<>"")
		{
			$mifile="multimedia/$mi2";
		
			@unlink($mifile);
			
		}
		
		
		$conec= new ADO();
		
		$sql="delete from equipo where equ_id='".$_POST['equ_id']."'";
		
		$conec->ejecutar($sql);
		
		$mensaje='Lugar Popular Eliminado Correctamente!!!';
			
		
		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
	}
	
	function nombre_imagen($id,$campo)
	{
		$conec= new ADO();
		
		$sql="select $campo as archivo from equipo where equ_id='".$id."'";
		
		$conec->ejecutar($sql);
		
		$objeto=$conec->get_objeto();
		
		return $objeto->archivo;
	}
	
	function eliminar_imagen()
	{
		$conec= new ADO();
		
		$mi=$_GET['img'];
		
		$mifile="multimedia/$mi";
				
		@unlink($mifile);
		
		$conec= new ADO();
		
		$sql="update equipo set 
						".$_GET['c']."=''
						where equ_id = '".$_GET['id']."'";
						
		$conec->ejecutar($sql);
		
		$mensaje='Banner Eliminado Correctamente!';
			
		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=MODIFICAR&id='.$_GET['id']);
		
	}
}
?>