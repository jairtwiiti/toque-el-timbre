<?php
	
	require_once('marca.class.php');
	
	$marca = new MARCA();
	
	if($_GET['tarea']<>"")
	{
		if(!($marca->verificar_permisos($_GET['tarea'])))
		{
			?>
			<script>
				location.href="log_out.php";
			</script>
			<?php
		}
	}
	
	switch ($_GET['tarea'])
	{
		case 'AGREGAR':{
					
							if($marca->datos())
							{
								$marca->insertar_tcp();
							}
							else 
							{
								$marca->formulario_tcp('blanco');
							}

						break;}
		case 'VER':{
						$marca->cargar_datos();
												
						$marca->formulario_tcp('ver');
						
						break;}
		case 'MODIFICAR':{
						 
                        if($_GET['acc']=='Imagen')
						{
							$marca->eliminar_imagen();
						}
						else
						{						 
							if($marca->datos())
							{
								$marca->modificar_tcp();
							}
							else 
							{
								if(!($_POST))
								{
									$marca->cargar_datos();
								}
								$marca->formulario_tcp('cargar');
							}
						 }	
						break;}
		case 'ELIMINAR':{
							
								if(isset($_POST['mar_id']))
								{
									if(trim($_POST['mar_id'])<>"")
									{
										$marca->eliminar_tcp();
									}
									else 
									{
										$marca->dibujar_busqueda();
									}
								}
								else 
								{
									$marca->formulario_confirmar_eliminacion();
								}
							
							
						break;}
		
		case 'MODELO':{
						if($_GET['acc']=='MODIFICAR_PRODUCTO')
						{
							if($marca->datos2())
							{
								$marca->modificar_producto();
							}
							else
							{
								if(!($_POST))
								{
									$marca->cargar_datos2();
								}
								$marca->formulario_tcp2('cargar');
							}
						}
						else						
						{
							if($_GET['acc']=='ELIMINAR')
								{
									$marca->eliminar_producto($_GET['mod_id']);
								}
								
								if($marca->datos2())
								{
									$marca->guardar_producto();
								}
								
								$marca->formulario_tcp2('cargar');
								
								$marca->dibujar_encabezado2();
								
								$marca->mostrar_busqueda2();
						}
								
								
						break;
					}
		
		default: $marca->dibujar_busqueda();break;
	}
		
?>