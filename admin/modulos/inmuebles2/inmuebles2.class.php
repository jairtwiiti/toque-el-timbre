<?php

class INMUEBLE extends BUSQUEDA 
{
	var $formulario;
	var $mensaje;
	
	function INMUEBLE()
	{
		//permisos
		$this->ele_id=150;
		
		$this->busqueda();
		
		if(!($this->verificar_permisos('AGREGAR')))
		{
			$this->ban_agregar=false;
		}
		//fin permisos
		
		$this->num_registros=14;
		
		$this->coneccion= new ADO();
		
		$this->arreglo_campos[0]["nombre"]="inm_nombre";
		$this->arreglo_campos[0]["texto"]="Titulo Inmueble";
		$this->arreglo_campos[0]["tipo"]="cadena";
		$this->arreglo_campos[0]["tamanio"]=40;
		
		$this->arreglo_campos[1]["nombre"]="usu_nombre";
		$this->arreglo_campos[1]["texto"]="Nombre del Usuario";
		$this->arreglo_campos[1]["tipo"]="cadena";
		$this->arreglo_campos[1]["tamanio"]=50;
		
		$this->arreglo_campos[2]["nombre"]="usu_email";
		$this->arreglo_campos[2]["texto"]="Email del Usuario";
		$this->arreglo_campos[2]["tipo"]="cadena";
		$this->arreglo_campos[2]["tamanio"]=50;
		
		$this->arreglo_campos[3]["nombre"]="pub_id";
		$this->arreglo_campos[3]["texto"]="ID de la Publicacion";
		$this->arreglo_campos[3]["tipo"]="cadena";
		$this->arreglo_campos[3]["tamanio"]=50;
				
		
		$this->arreglo_campos[4]["nombre"]="inm_cat_id";
		$this->arreglo_campos[4]["texto"]="Tipo";
		$this->arreglo_campos[4]["tipo"]="combosql";
		$this->arreglo_campos[4]["sql"]="select cat_id as codigo,cat_nombre as descripcion from categoria";
		
		$this->arreglo_campos[5]["nombre"]="inm_ciu_id";
		$this->arreglo_campos[5]["texto"]="Ciudad";
		$this->arreglo_campos[5]["tipo"]="combosql";
		$this->arreglo_campos[5]["sql"]="select ciu_id as codigo,ciu_nombre as descripcion from ciudad";
		
		$this->arreglo_campos[6]["nombre"]="inm_for_id";
		$this->arreglo_campos[6]["texto"]="Forma";
		$this->arreglo_campos[6]["tipo"]="combosql";
		$this->arreglo_campos[6]["sql"]="select for_id as codigo,for_descripcion as descripcion from forma";
		
		$this->arreglo_campos[7]["nombre"]="pub_estado";
		$this->arreglo_campos[7]["texto"]="Estado";
		$this->arreglo_campos[7]["tipo"]="comboarray";
		$this->arreglo_campos[7]["valores"]='Aprobado,No Aprobado,Cancelado:Aprobado,No Aprobado,Cancelado';
		
		
		
		$this->link='gestor.php';
		
		$this->modulo='inmuebles2';
		
		$this->formulario = new FORMULARIO();
		
		$this->formulario->set_titulo('INMUEBLE');
		
		
	}
	
	
	function dibujar_busqueda()
	{
		
		$this->formulario->dibujar_cabecera();

        $this->buscador_fecha();
		
		$this->dibujar_listado();
	}
	
		
	function set_opciones()
	{
				
		$nun=0;
		
		if($this->verificar_permisos('VER'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='VER';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_search.png';
			$this->arreglo_opciones[$nun]["nombre"]='VER';
			$nun++;
		}
		
		if($this->verificar_permisos('MODIFICAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='MODIFICAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_edit.png';
			$this->arreglo_opciones[$nun]["nombre"]='MODIFICAR';
			$nun++;
		}
		
		if($this->verificar_permisos('ELIMINAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='ELIMINAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_drop.png';
			$this->arreglo_opciones[$nun]["nombre"]='ELIMINAR';
			$nun++;
		}
		
		if($this->verificar_permisos('FOTOS'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='FOTOS';
			$this->arreglo_opciones[$nun]["imagen"]='images/fotos.png';
			$this->arreglo_opciones[$nun]["nombre"]='FOTO';
			$nun++;
		}
	}
	
	function dibujar_listado()
	{
		$sql="
        SELECT inmueble.*,ciu_nombre,dep_nombre,cat_nombre,for_descripcion,pub_creado,pub_vig_ini,pub_vig_fin,pub_estado,mon_abreviado,pub_id,usu_email, CONCAT(usu_nombre,' ',usu_apellido) AS usu_nombre, usu_tipo, p.pag_entidad, p.pag_fecha_pagado FROM inmueble
        INNER JOIN ciudad ON (inm_ciu_id = ciu_id)
        INNER JOIN departamento ON (ciu_dep_id = dep_id)
        INNER JOIN categoria ON (inm_cat_id = cat_id)

        INNER JOIN forma ON (inm_for_id = for_id)
        INNER JOIN publicacion ON (pub_inm_id = inm_id)
        INNER JOIN usuario ON (usu_id = pub_usu_id)
        INNER JOIN moneda ON (inm_mon_id = mon_id)
        INNER JOIN pagos p ON (p.`pag_pub_id` = pub_id AND p.pag_estado = 'Pagado')
        INNER JOIN pago_servicio ps ON (ps.`pag_id` = p.`pag_id`)
        where 	pub_estado = 'Aprobado'
        ";

        $this->set_sql($sql,' GROUP BY inm_id order by p.pag_fecha_pagado desc');
		
		$this->set_opciones();
		
		$this->dibujar();
		
	}
	
	function dibujar_encabezado()
	{
		?>
			<tr>
	        	<th>Titulo</th>				
				<th>Tipo</th>
				<th>Departamento</th>
				<th>Precio</th>
				<th>Forma</th>
                <th>Direccion</th>
				<th>Entidad</th>
                <th>Fecha Creado</th>
                <th>Fecha Pagado</th>
                <th>Fecha Fin</th>
                <th>Usuario</th>
                <th>Email</th>
                <th>Tipo</th>
                <th>URL</th>
	            <th class="tOpciones" width="100px">Opciones</th>
			</tr>
			
		<?PHP
	}

    function buscador_fecha(){
        ?>
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
        <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
        <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
        <script>
            $(function() {
                $( "#fecha_inicio" ).datepicker();
                $( "#fecha_fin" ).datepicker();
            });
        </script>
        <div style="font-size:12px; padding:8px 10px;">
            <form method="post" action="reporte/excel_anuncios_publicados.php">
                <table>
                    <tr>
                        <td>
                            <label style="font-size:12px;">Fecha de Inicio</label>
                            <input style="font-size:12px;" type="text" name="fecha_inicio" id="fecha_inicio" />
                        </td>
                        <td>
                            <label style="font-size:12px;">Fecha de Fin</label>
                            <input style="font-size:12px;" type="text" name="fecha_fin" id="fecha_fin" />
                        </td>
                        <td><input type="submit"  value="Generar Reporte" /></td>
                    </tr>
                </table>
            </form>
        </div>
    <?php
    }
	
	function mostrar_busqueda()
	{
		$conversor = new convertir();
		
		for($i=0;$i<$this->numero;$i++)
			{
				
				$objeto=$this->coneccion->get_objeto();
				echo '<tr>';
									
					echo "<td>";
						echo $objeto->inm_nombre;
					echo "&nbsp;</td>";
					echo "<td>";
						echo $objeto->cat_nombre;
					echo "&nbsp;</td>";					
					echo "<td>";
						echo $objeto->dep_nombre;
					echo "&nbsp;</td>";
					echo "<td>";
						echo round($objeto->inm_precio,0)." ".$objeto->mon_abreviado;
					echo "&nbsp;</td>";
				
					echo "<td>";
						echo $objeto->for_descripcion;
					echo "&nbsp;</td>";

                    echo "<td>";
                        echo $objeto->inm_direccion;
                    echo "&nbsp;</td>";

					echo "<td>";
						echo $objeto->pag_entidad;
					echo "&nbsp;</td>";
                    echo "<td>";
                        echo $conversor->get_fecha_latina($objeto->pub_creado);
                    echo "&nbsp;</td>";
                    echo "<td>";
                        echo $conversor->get_fecha_latina($objeto->pag_fecha_pagado);
                    echo "&nbsp;</td>";
                    echo "<td>";
						echo $conversor->get_fecha_latina($objeto->pub_vig_fin);
					echo "&nbsp;</td>";
                    echo "<td>";
                        echo $objeto->usu_nombre;
                    echo "&nbsp;</td>";
                    echo "<td>";
                        echo $objeto->usu_email;
                    echo "&nbsp;</td>";
                    echo "<td>";
                        echo $objeto->usu_tipo;
                    echo "&nbsp;</td>";
                    echo "<td>";
                        $url_detalle = $this->generarURL($objeto->inm_id);
                        echo "<a href='".$url_detalle."' target='_blank'>Detalle</a>";
                    echo "&nbsp;</td>";
					echo "<td>";
						echo $this->get_opciones($objeto->inm_id);
					echo "</td>";
				echo "</tr>";
				
				$this->coneccion->siguiente();
			}
	}
	
	function orden($proyecto,$accion,$ant_orden)
	{
		$conec= new ADO();
		
		if($accion=='s')
			$cad=" where inm_orden > $ant_orden order by inm_orden asc";
		else
			$cad=" where inm_orden < $ant_orden order by inm_orden desc";

		$consulta = "
		select 
			inm_id,inm_orden 
		from 
			inmueble
		$cad
		limit 0,1
		";	

		$conec->ejecutar($consulta);

		$num = $conec->get_num_registros();   

		if($num > 0)
		{
			$objeto=$conec->get_objeto();
			
			$nu_orden=$objeto->inm_orden;
			
			$id=$objeto->inm_id;
			
			$consulta = "update inmueble set inm_orden='$nu_orden' where inm_id='$proyecto'";	

			$conec->ejecutar($consulta);
			
			$consulta = "update inmueble set inm_orden='$ant_orden' where inm_id='$id'";	

			$conec->ejecutar($consulta);
		}	
	}
	
	function cargar_datos()
	{
		$conec=new ADO();
		
		$sql="select inmueble.*,ciu_dep_id as dep_id,pub_estado,pub_vig_ini,pub_vig_fin,pub_usu_id,pub_obs,pub_monto,usu_email,usu_nombre,usu_apellido,usu_id
		       from inmueble inner join ciudad on (inm_ciu_id = ciu_id)
                             inner join publicacion on (pub_inm_id = inm_id)
                             left join usuario on (pub_usu_id = usu_id) 							 
				where inm_id = '".$_GET['id']."'";
		
		$conec->ejecutar($sql);
		
		$objeto=$conec->get_objeto();
		 	 	 	 	 	 	 	 	 
		$_POST['usu_id']=$objeto->usu_id;		
		$_POST['inm_id']=$objeto->inm_id;		
		$_POST['inm_nombre']=$objeto->inm_nombre;		
		$_POST['inm_foto']=$objeto->inm_foto;
        $_POST['inm_ciu_id']=$objeto->inm_ciu_id;			
		$_POST['inm_precio']=$objeto->inm_precio;		
		$_POST['inm_direccion']=$objeto->inm_direccion;				
		$_POST['inm_superficie']=$objeto->inm_superficie;
		$_POST['inm_tipo_superficie']=$objeto->inm_tipo_superficie;		
		$_POST['inm_detalle']=$objeto->inm_detalle;		
		$_POST['inm_cat_id']=$objeto->inm_cat_id;	
		$_POST['inm_zon_id']=$objeto->inm_zon_id;			
		$_POST['inm_for_id']=$objeto->inm_for_id;		
		$_POST['inm_latitud']=$objeto->inm_latitud;		
		$_POST['inm_longitud']=$objeto->inm_longitud;
		$_POST['inm_mon_id']=$objeto->inm_mon_id;
		//$_POST['inm_password']=$objeto->inm_password;
		$_POST['pub_estado']=$objeto->pub_estado;
		$_POST['pub_vig_ini']=$objeto->pub_vig_ini;
		$_POST['pub_vig_fin']=$objeto->pub_vig_fin;
		$_POST['pub_obs']=$objeto->pub_obs;
		$_POST['pub_usu_id']=$objeto->pub_usu_id;
		$_POST['pub_monto']=$objeto->pub_monto;
		
		$_POST['usu_email']=$objeto->usu_email;
		$_POST['usu_nombre']=$objeto->usu_nombre;
		$_POST['usu_apellido']=$objeto->usu_apellido;
		
        $_POST['dep_id']=$objeto->dep_id;		
	}

    function generarURL($inm_id)
    {
        $conec=new ADO();

        $sql="
        SELECT inm_seo, cat_nombre, ciu_nombre, for_descripcion
        FROM inmueble
        INNER JOIN ciudad ON (inm_ciu_id = ciu_id)
        INNER JOIN forma ON (for_id = inm_for_id)
        INNER JOIN categoria ON (cat_id = inm_cat_id)
        INNER JOIN publicacion ON (pub_inm_id = inm_id)
        where inm_id = '".$inm_id."'";

        $conec->ejecutar($sql);

        $objeto=$conec->get_objeto();

        $url_detalle = "http://toqueeltimbre.com/";
        $url_detalle .= $this->seo_url($objeto->ciu_nombre)."/";
        $url_detalle .= $this->seo_url($objeto->cat_nombre . " EN ". $objeto->for_descripcion)."/";
        $url_detalle .= $this->seo_url($objeto->inm_seo).".html";

        return $url_detalle;
    }

    function seo_url($texto, $lower = true){
        $spacer = "-";
        $texto = trim($texto);
        if($lower){
            $texto = strtolower($texto);
        }

        $texto = str_replace("á", "a", $texto);
        $texto = str_replace("é", "e", $texto);
        $texto = str_replace("í", "i", $texto);
        $texto = str_replace("ó", "o", $texto);
        $texto = str_replace("ú", "u", $texto);
        $texto = str_replace("ñ", "n", $texto);

        $texto = trim(ereg_replace("[^ A-Za-z0-9_]", " ", $texto));
        $texto = ereg_replace("[ \t\n\r]+", "-", $texto);
        $texto = str_replace(" ", $spacer, $texto);
        $texto = ereg_replace("[ -]+", "-",$texto);
        return $texto;
    }
	
	function datos()
	{
		if($_POST)
		{
			//texto,  numero,  real,  fecha,  mail.
			$num=0;
			$valores[$num]["etiqueta"]="Nombre";
			$valores[$num]["valor"]=$_POST['inm_nombre'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			
			$valores[$num]["etiqueta"]="Categoria";
			$valores[$num]["valor"]=$_POST['inm_cat_id'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			
			$valores[$num]["etiqueta"]="Departamento";
			$valores[$num]["valor"]=$_POST['dep_id'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			
			$valores[$num]["etiqueta"]="Ciudad";
			$valores[$num]["valor"]=$_POST['inm_ciu_id'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			
			$valores[$num]["etiqueta"]="Zona";
			$valores[$num]["valor"]=$_POST['inm_zon_id'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			$valores[$num]["etiqueta"]="Direccion";
			$valores[$num]["valor"]=$_POST['inm_direccion'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			$valores[$num]["etiqueta"]="Forma";
			$valores[$num]["valor"]=$_POST['inm_for_id'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			$valores[$num]["etiqueta"]="Superficie";
			$valores[$num]["valor"]=$_POST['inm_superficie'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			$valores[$num]["etiqueta"]="Precio";
			$valores[$num]["valor"]=$_POST['inm_precio'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			$valores[$num]["etiqueta"]="Moneda";
			$valores[$num]["valor"]=$_POST['inm_mon_id'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			$valores[$num]["etiqueta"]="Tipo superficie";
			$valores[$num]["valor"]=$_POST['inm_tipo_superficie'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			
			$val=NEW VALIDADOR;
			
			$this->mensaje="";
			
			if($val->validar($valores))
			{
				return true;
			}
				
			else
			{
				$this->mensaje=$val->mensaje;
				return false;
			}
		}
			return false;
	}
	
	function formulario_tcp($tipo)
	{
				switch ($tipo)
				{
					case 'ver':{
								$ver=true;
								break;
								}
							
					case 'cargar':{
								$cargar=true;
								break;
								}
				}
				
				$url=$this->link.'?mod='.$this->modulo;
				
				$red=$url;
				
				if(!($ver))
				{
					$url.="&tarea=".$_GET['tarea'];
				}
				
				if($cargar)
				{
					$url.='&id='.$_GET['id'];
				}

		
		    $this->formulario->dibujar_tarea('CLIENTE');
		
			if($this->mensaje<>"")
			{
				$this->formulario->mensaje('Error',$this->mensaje);
			}
			?>
		<script type="text/javascript">
		   var map;
		   var markers = [];
		   var geocoder;
		   var zooms = 5;		   
		   function initialize() {
				geocoder = new google.maps.Geocoder();				
				var myLatlng = new google.maps.LatLng(-16.930705,-64.782716); 		
				
				var myOptions = {
					zoom: zooms,
					center: myLatlng,
					mapTypeId: google.maps.MapTypeId.ROADMAP
				}
				
				map = new google.maps.Map(document.getElementById("map"), myOptions);						
				
				<?php
				    if (isset($_POST['inm_latitud']) && isset($_POST['inm_longitud']))
				    {				       
				?>
				       var mypoint = new google.maps.LatLng(<? echo $_POST['inm_latitud']; ?>,<? echo $_POST['inm_longitud']; ?>);					 
					   createMarker(mypoint);	
					   map.setCenter(mypoint);			
					   map.setZoom(15);	
				<?php
				    }
				?>
				
				google.maps.event.addListener(map, 'click', function(event) {
				
					if  (markers.length < 1)
					{	
						createMarker(event.latLng);	  
						
					}   
					else
					{	
						markers[0].setPosition(event.latLng);
						actualizar_punto(markers[0]);
						//document.getElementById("latitud").value = 	markers[0].getPosition().lat();	
						//document.getElementById("longitud").value = markers[0].getPosition().lng();
						//var contentString = "<p>"+event.latLng.lat()+"<br/>"+event.latLng.lng()+"</p>";		 
					}	
						
				});
				
				
		   }
		   
		   function actualizar_punto(point)
		   {
		        document.getElementById("latitud").value  =	point.getPosition().lat();	
				document.getElementById("longitud").value = point.getPosition().lng();	
		   }
		   
		   function createMarker(point) {
				
				var clickedLocation = new google.maps.LatLng(point);
				
				markers.push(new google.maps.Marker({
				  position: point,
				  map: map,
				  draggable: true,
				  animation: google.maps.Animation.DROP
				}));  
				
				actualizar_punto(markers[0]);
					
				google.maps.event.addListener(markers[0], 'dragend', function() {	
					
				actualizar_punto(markers[0]);	
						//document.getElementById("latitud").value = 	markers[0].getPosition().lat();	
						//document.getElementById("longitud").value = markers[0].getPosition().lng();	
						
						 // var contentString = "<p>"+markers[0].getPosition().lat()+"<br/>"+markers[0].getPosition().lng()+"</p>";
							
						  // var infowindow = new google.maps.InfoWindow({content: contentString});	
							
							// infowindow.open(map,markers[0]);
				});	
				//map.setCenter(point);       
			}

			function codeAddress() {
				
				var pais    = document.getElementById("map_pais").value;		
				var ciudad  = document.getElementById("map_ciudad").value;
				var tipo  = document.getElementById("map_tipo").value;
				var direccion = document.getElementById("map_direccion").value;
				var address;
				
				if (direccion != "")		   
				   address = tipo+","+direccion+","+ciudad+","+pais;	
				else
				   address = ciudad+","+pais;					
				
				geocoder.geocode( { 'address': address}, function(results, status) {
				  if (status == google.maps.GeocoderStatus.OK) {
					map.setCenter(results[0].geometry.location);			
					map.setZoom(12);
					if (direccion != "")
					   map.setZoom(15);
					
					if  (markers.length < 1)
					{	
						createMarker(results[0].geometry.location);
					}
					else
					{
						markers[0].setPosition(results[0].geometry.location);
					}			
					
				  } else {
					alert("Geocode was not successful for the following reason: " + status);
				  }
				});
			}	
			
		  
			function loadScript() {
			  var script = document.createElement("script");
			  script.type = "text/javascript";
			  script.src = "http://maps.google.com/maps/api/js?sensor=false&callback=initialize";
			  document.body.appendChild(script);
			}
			  
			window.onload = loadScript;
			
			$(document).ready(function() {
				//$('.cerrarToogle').hide();				
				//$('.cerrarToogle:first').show();
				$('.prueba').click(function(){
					var cat = $(this).parent().find('.cerrarToogle');
					if(cat.is(':visible')){
						$(cat).hide();
					} else {
						$(cat).slideDown('normal');
					};
				});	
			});
			
			function cargar_ciudad(id)
			{
				var	valores="thid="+id;			
				ejecutar_ajax('ajax.php','ciudad',valores,'POST');
				var	valores="tzid="+id;			
				ejecutar_ajax('ajax.php','zona',valores,'POST');	
			}
			
			function cargar_caracteristica(id)
			{
				var	valores="tcid="+id;			
				ejecutar_ajax('ajax.php','caracteristica',valores,'POST');				
			}
	
</script>	
		<div id="Contenedor_NuevaSentencia">
			<form id="frm_sentencia" name="frm_sentencia" action="<?php echo $url;?>" method="POST" enctype="multipart/form-data">  
				<div id="FormSent" style="width:650px">
				  
					<div class="Subtitulo">Datos</div>
						<div id="ContenedorSeleccion">
							
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">* </span>Titulo</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto" name="inm_nombre" id="inm_nombre" maxlength="250"  size="40" value="<?php echo $_POST['inm_nombre'];?>">
							   </div>
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Categoria</div>
							   <div id="CajaInput">							   
							   <select name="inm_cat_id" class="caja_texto" onchange="cargar_caracteristica(this.value);">
							   <option value="">Seleccione</option>
							   <?php 	                                 							   
									$fun=NEW FUNCIONES;		
									$fun->combo("select cat_id as id,cat_nombre as nombre from categoria",$_POST['inm_cat_id']);												
								?>
							   </select>							   
							   </div>
							   
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Departamento</div>
							   <div id="CajaInput">
							   <select name="dep_id" class="caja_texto" onchange="cargar_ciudad(this.value);">
							   <option value="">Seleccione</option>
							   <?php 		
								$fun=NEW FUNCIONES;		
								$fun->combo("select dep_id as id,dep_nombre as nombre from departamento",$_POST['dep_id']);				
								?>
							   </select>
							   </div>
							   
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Ciudad</div>
							   <div id="CajaInput">
							   <div id="ciudad">
							   <select name="inm_ciu_id" class="caja_texto">
							   <option value="">Seleccione</option>
							   <?php 	
                                 if ($_POST["dep_id"])							   
								 {							   
									$fun=NEW FUNCIONES;		
									$fun->combo("select ciu_id as id,ciu_nombre as nombre from ciudad",$_POST['inm_ciu_id']);				
								}
								?>
							   </select>
							   </div>
							   </div>
							   
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Zona</div>
							   <div id="CajaInput">		
                                <div id="zona">		   
							   <select name="inm_zon_id" id="inm_zon_id" class="caja_texto">
							   <option value="">Seleccione</option>
							   <?php 	
                                 							   
									$fun=NEW FUNCIONES;		
									$fun->combo("select zon_id as id,zon_nombre as nombre from zona",$_POST['inm_zon_id']);				
								
								?>
							   </select>	
                               </div>							   
							   </div>
							   
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Direccion</div>
							   <div id="CajaInput">
							   <textarea class="area_texto" name="inm_direccion" id="inm_direccion" cols="31" rows="3"><?php echo $_POST['inm_direccion']?></textarea>
							   </div>
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Tipo</div>
							   <div id="CajaInput">
							   
							   <select name="inm_for_id" class="caja_texto">
							   <option value="">Seleccione</option>
							   <?php 	
                                 							   
									$fun=NEW FUNCIONES;		
									$fun->combo("select for_id as id,for_descripcion as nombre from forma",$_POST['inm_for_id']);				
								
								?>
							   </select>
							  
							   </div>
							   
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Superficie</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto" name="inm_superficie" id="inm_superficie" maxlength="250"  size="10" value="<?php echo $_POST['inm_superficie'];?>">
							   <select name="inm_tipo_superficie" class="caja_texto">
									<option value="" >Seleccione</option>
									<option value="Metros Cuadrados" <?php if($_POST['inm_tipo_superficie']=='Metros Cuadrados') echo 'selected="selected"'; ?>>Metros Cuadrados</option>
									<option value="Hectareas" <?php if($_POST['inm_tipo_superficie']=='Hectareas') echo 'selected="selected"'; ?>>Hectareas</option>
									</select>
							   </div>
							</div>
							<!--Fin-->
							
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Precio</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto" name="inm_precio" id="inm_precio" maxlength="250"  size="40" value="<?php echo $_POST['inm_precio'];?>">
							   </div>
							</div>
							<!--Fin-->	
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Moneda</div>
							   <div id="CajaInput">							   
							   <select name="inm_mon_id" class="caja_texto">
							   <option value="">Seleccione</option>
							   <?php 	                                 							   
									$fun=NEW FUNCIONES;		
									$fun->combo("select mon_id as id,mon_descripcion as nombre from moneda",$_POST['inm_mon_id']);												
								?>
							   </select>							   
							   </div>
							   
							</div>
							<!--Fin-->
							<!--Inicio-->
							<!--<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">*</span>Detalle</div>
							   <div id="CajaInput">
							        <textarea rows="4" cols="40" name="inm_detalle" id="inm_detalle"><?php echo $_POST['inm_detalle'];?></textarea>									
							   </div>
							</div>-->
							<!--Fin-->
                            
                             <?php include_once("js/fckeditor/fckeditor.php"); ?>	
                           <!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Detalle</div>
							   <div id="CajaInput">
									<?php
										$pagina=$_POST['inm_detalle'];
										$oFCKeditor = new FCKeditor('inm_detalle') ;
										$oFCKeditor->BasePath = 'js/fckeditor/';
										$oFCKeditor->Width  = '450' ;
										$oFCKeditor->Height = '250' ;
										//$oFCKeditor->ToolbarSet = 'Basic' ;
										$oFCKeditor->Value = $pagina;
										$oFCKeditor->Create() ;
									?>
							   </div>
							 

							</div>                            
							<!--Fin-->
							
							
							<br/>
							<fieldset style='overflow:hidden; display: none;' >
							<legend class='prueba' id='mapaCargar'>&nbsp;Caracteristicas&nbsp;</legend>
								<div class='cerrarToogle' id="caracteristica">								    
								    <?php  
									    if (isset($_POST['inm_cat_id']))
									          $this->cargar_caracteristicas($_POST['inm_cat_id']); 
									   
									 ?> 
									
								</div>							
							</fieldset>	
							<br/>
							<fieldset style="display:none;">
							<legend class='prueba' id='mapaCargar'>&nbsp;Publicacion&nbsp;</legend>
							<div class='cerrarToogle'>
							     <!--Inicio-->
								<div id="ContenedorDiv">
								   <div class="Etiqueta" >Vigente Desde</div>
								   <div id="CajaInput">
								   <?php echo $_POST['pub_vig_ini'];?>
								   </div>
								</div>
								<!--Fin-->	   
								 <!--Inicio-->
								<div id="ContenedorDiv">
								   <div class="Etiqueta" >Vigente Hasta</div>
								   <div id="CajaInput">
								   <?php echo $_POST['pub_vig_fin'];?>
								   </div>
								</div>
								<!--Fin-->
								
								 <!--Inicio-->
								<div id="ContenedorDiv">
								   <div class="Etiqueta" >Monto</div>
								   <div id="CajaInput">
								   <input type="text" class="caja_texto" name="pub_monto" id="pub_monto" maxlength="250"  size="40" value="<?php echo $_POST['pub_monto'];?>">								  
								   </div>
								</div>
								<!--Fin-->
								
								 <!--Inicio-->
								<div id="ContenedorDiv">
								   <div class="Etiqueta" >Usuario</div>
								   <div id="CajaInput">
								   <?php echo $_POST['usu_nombre']." ".$_POST['usu_apellido'];?>
								   </div>
								</div>
								<!--Fin-->
								 <!--Inicio-->
								<div id="ContenedorDiv">
								   <div class="Etiqueta" >Email</div>
								   <div id="CajaInput">
								   <?php echo $_POST['usu_email'];?>
								     <input type="hidden" name="usu_email" value="<? echo $_POST['usu_email'];?>">
								   </div>
								</div>
								<!--Fin-->
								<!--Inicio-->
								<div id="ContenedorDiv">
								   <div class="Etiqueta" ><span class="flechas1">* </span>Estado</div>
								   <div id="CajaInput">
										<select name="pub_estado" class="caja_texto">
										<option value="" >Seleccione</option>
										<option value="Aprobado" <?php if($_POST['pub_estado']=='Aprobado') echo 'selected="selected"'; ?>>Aprobado</option>
										<option value="No Aprobado" <?php if($_POST['pub_estado']=='No Aprobado') echo 'selected="selected"'; ?>>No Aprobado</option>
										<option value="Cancelado" <?php if($_POST['pub_estado']=='Cancelado') echo 'selected="selected"'; ?>>Cancelado</option>
										</select>
								   </div>
								</div>
								<!--Fin-->
								
								<!--Inicio-->
								<div id="ContenedorDiv">
								   <div class="Etiqueta" >Observaciones</div>
								   <div id="CajaInput">
										<textarea rows="4" cols="40" name="pub_obs" id="pub_obs"><?php echo $_POST['pub_obs'];?></textarea>									
								   </div>
								</div>
								<!--Fin-->
							</div>							
							</fieldset>					
							<!--Inicio-->
							<br/>
							<fieldset style="display:none;">
							<legend class='prueba' id='mapaCargar'>&nbsp;Ubicaci�n&nbsp;</legend>
							<div class='cerrarToogle'>
							
							<div id="ContenedorDiv">							  
							    <div id="CajaInput">
							   <div style="padding:10px 0px 10px 15px">						   
							        <input id="latitud" name="inm_latitud" type="hidden" value="<?php echo $_POST['inm_latitud'];?>">
									<input id="longitud" name="inm_longitud" type="hidden" value = "<?php echo $_POST['inm_longitud'];?>">									
									<input id="map_pais" name="map_pais" type="hidden" value="Bolivia">
									<table border="0" cellspacing="2">
									    <tr>
										    <td><div style="color: #005C89; font-size:12px" >Ciudad</div></td>
											<td><div style="color: #005C89; font-size:12px" >Tipo</div></td>
											<td><div style="color: #005C89; font-size:12px" >Direccion</div></td>
											<td>&nbsp;</td>
										</tr>
										<tr>
										    <td>
												<select id="map_ciudad" name="map_ciudad" style="width:100px; ">
													 <option value="Santa Cruz de la Sierra">Santa Cruz</option>
													 <option value="La Paz">La Paz</option>	
													 <option value="Cochabamba">Cochabamba</option>	
													 <option value="Tarija">Tarija</option>	
													 <option value="Sucre">Sucre</option>	
													 <option value="Oruro">Oruro</option>	
													 <option value="Potosi">Potosi</option>	
													 <option value="Trinidad">Beni</option>	
													 <option value="Cobija">Pando</option>	
												</select>
											</td>
											<td>
												<select id="map_tipo" name="map_tipo" style="width:100px; ">
													 <option value="Calle">Calle</option>
													 <option value="Av.">Avenida</option>											 
												</select>
											</td>
											<td><input id="map_direccion" type="textbox" size="35" ></td>
											<td>
												<input type="button" class="boton" style="width:100px" value="Buscar Direcci�n" onclick="codeAddress()">
											</td>
										</tr>
									</table>
																		
									
									
									
									
							  </div>
								<div id="map" style="width: 600px; height: 600px;"></div>
							   </div>
							</div>
							<!--Fin-->	
							</div>	
							</fieldset>
							
								<!-- Inicio Fotos -->
							<style>
								.thumbs {
									min-height: 194px;
									padding-left: 79px;
									padding-right: 0px;
									padding-top: 22px;
								}
								.thumbs .photo-detalle {
									background-color: #CACCCE;
									float: left;
									height: 81px;
									margin-bottom: 10px;	
									margin-right: 15px;
									overflow: hidden;
									position: relative;
									width: 142px;
								}
							</style>
								<?php 
									require_once('config/constante.php');
									$inm_id = $_POST['inm_id'];
									$conec= new ADO();
									$consulta = "select *from inmueble_foto where fot_inm_id=" . $inm_id;	

									$conec->ejecutar($consulta);

									$num = $conec->get_num_registros(); 
									echo '<div class="thumbs">';
									for($i=0;$i<$num;$i++)
									{				
										$objeto=$conec->get_objeto();													
										?>
					                    	<div class="photo-detalle">
                    		                    <div class="image-content"><img alt="<?php echo $objeto->fot_archivo;?>" src="<?php echo _URL_WEBSITE;?>librerias/timthumb.php?src=<?php echo _URL_WEBSITE;?>admin/imagenes/inmueble/<?php echo $objeto->fot_archivo;?>&amp;w=142&amp;h=81"></div>                        	                    
											</div>
										<?php
										$conec->siguiente();
									}
									echo '</div>';
									
								?>
								
								
							<!-- Fin Fotos -->							
							
						</div>
					
						<div id="ContenedorDiv">
						   <div id="CajaBotones">
								<center>
								<?php
								if(!($ver))
								{
									?>
									<input type="submit" class="boton" name="" value="Guardar">
									<input type="reset" class="boton" name="" value="Cancelar">
									<input type="button" class="boton" name="" value="Volver" onclick="javascript:location.href='<?php echo $red;?>';">
									<?php
								}
								else
								{
									?>
									<input type="button" class="boton" name="" value="Volver" onclick="javascript:location.href='<?php echo $red;?>';">
									<?php
								}
								?>
								</center>
						   </div>
						</div>
				</div>
			</form>
		</div>
		<?php
	}
	
	function insertar_tcp()
	{
		
		// $verificar=NEW VERIFICAR;
		
		// $parametros[0]=array('inm_nombre','inm_apellido');
		// $parametros[1]=array($_POST['inm_nombre'],$_POST['inm_apellido']);
		// $parametros[2]=array('inmueble');

		// if($verificar->validar($parametros))
		// {
		    $conec= new ADO();
		
			$sql=" select max(inm_orden) as ultimo from inmueble ";
			
			$conec->ejecutar($sql);
			
			$objeto=$conec->get_objeto();
			
			$orden=$objeto->ultimo + 1;
		
		
				
			if($_FILES['inm_foto']['name']<>"")
			{
				$result=$this->subir_imagen($nombre_archivo,$_FILES['inm_foto']['name'],$_FILES['inm_foto']['tmp_name']);
				
				$sql="insert into inmueble(inm_nombre,inm_foto,inm_direccion,inm_precio,inm_superficie,inm_detalle,inm_ciu_id,inm_orden,inm_latitud,inm_longitud,inm_cat_id,inm_zon_id,inm_for_id,inm_tipo_superficie,inm_mon_id) values 
									('".$_POST['inm_nombre']."','".$nombre_archivo."','".$_POST['inm_direccion']."','".$_POST['inm_precio']."','".$_POST['inm_superficie']."','".$_POST['inm_detalle']."','".$_POST['inm_ciu_id']."','".$orden."','".$_POST['inm_latitud']."','".$_POST['inm_longitud']."','".$_POST['inm_cat_id']."','".$_POST['inm_zon_id']."','".$_POST['inm_for_id']."','".$_POST['inm_tipo_superficie']."','".$_POST['inm_mon_id']."')";
				
				if(trim($result)<>'')
				{					
					$this->formulario->ventana_volver($result,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
				}
				else 
				{
					$conec->ejecutar($sql);
					
					$insert_id = mysql_insert_id();
					
					$this->guardar_caracteristicas($_POST['inm_cat_id'],$insert_id);					
					
					$desde = date('Y-m-d');
					$hasta_aux = mktime(0, 0, 0, date("m")  , date("d")+10, date("Y"));
					$hasta = date('Y-m-d',$hasta_aux);
					
					$sql="insert into publicacion(pub_creado,pub_vig_ini,pub_vig_fin,pub_monto,pub_inm_id,pub_estado,pub_obs) values 
									 (NOW(),'".$desde."','".$hasta."','".$_POST['pub_monto']."','".$insert_id."','".$_POST['pub_estado']."','".$_POST['pub_obs']."')";
									 
					$conec->ejecutar($sql);		

				
					
					
					$mensaje='Inmueble Agregada Correctamente!!!';
					
					$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
				}
			}
			else
			{
				$sql="insert into inmueble(inm_nombre,inm_direccion,inm_precio,inm_superficie,inm_detalle,inm_ciu_id,inm_orden,inm_latitud,inm_longitud,inm_cat_id,inm_zon_id,inm_for_id,inm_tipo_superficie,inm_mon_id) values 
									('".$_POST['inm_nombre']."','".$_POST['inm_direccion']."','".$_POST['inm_precio']."','".$_POST['inm_superficie']."','".$_POST['inm_detalle']."','".$_POST['inm_ciu_id']."','".$orden."','".$_POST['inm_latitud']."','".$_POST['inm_longitud']."','".$_POST['inm_cat_id']."','".$_POST['inm_zon_id']."','".$_POST['inm_for_id']."','".$_POST['inm_tipo_superficie']."','".$_POST['inm_mon_id']."')";
				
				$conec->ejecutar($sql,false);
				//echo $sql;
				$insert_id = mysql_insert_id();
				
				$this->guardar_caracteristicas($_POST['inm_cat_id'],$insert_id);				
				
				$desde = date('Y-m-d');
				$hasta_aux = mktime(0, 0, 0, date("m")  , date("d")+10, date("Y"));
				$hasta = date('Y-m-d',$hasta_aux);
				
				$sql="insert into publicacion(pub_creado,pub_vig_ini,pub_vig_fin,pub_monto,pub_inm_id,pub_estado,pub_obs) values 
								 (NOW(),'".$desde."','".$hasta."','".$_POST['pub_monto']."','".$insert_id."','".$_POST['pub_estado']."','".$_POST['pub_obs']."')";
				//echo $sql;				 
				$conec->ejecutar($sql);	

				$mensaje='Inmueble Agregado Correctamente!!!';

				$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
			}
		// }
		// else
		// {
			// $mensaje='La inmueble no puede ser agregada, por que existe una inmueble con ese nombre y apellido.';
			
			// $this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
		// }
	
		
	}
	
	function guardar_caracteristicas($cat_id,$inm_id)
	{
	    $conec= new ADO();
	
		$sql="select caracteristica.* from categoria_caracteristica as catcar INNER JOIN caracteristica ON (catcar.car_id = caracteristica.car_id) 
		where catcar.cat_id='$cat_id' order by catcar.orden";		

		$conec->ejecutar($sql);
		
		$num=$conec->get_num_registros();
					
		$conec2 = new ADO();
		for($i=0;$i<$num;$i++)
		{
			
			$objeto=$conec->get_objeto();
			
			$valor = $_POST[$objeto->car_nombre];
			
			$sql = "INSERT inmueble_caracteristica (eca_inm_id,eca_car_id,eca_valor) VALUE ('".$inm_id."','".$objeto->car_id."','".$valor."')";
			echo $sql;
			$conec2->ejecutar($sql);
			
			$conec->siguiente();
		}
	
	
	}
	
	function obtener_valor($inm_id,$car_id)
	{
	    $conec= new ADO();
	
		$sql="select eca_valor from inmueble_caracteristica	where eca_inm_id='$inm_id' and eca_car_id = '$car_id'";		

		$conec->ejecutar($sql);
	    
		if ($conec->get_num_registros() > 0)
        {		
		   $objeto=$conec->get_objeto();
		      if ($objeto->eca_valor!="")
		          return $objeto->eca_valor;
			   else
			      return "-1";
		}
		else
		   return "-1";	
		
	}
	
	function cargar_caracteristicas($cat_id)
	{
	    $conec= new ADO();
	
		$sql="select caracteristica.* from categoria_caracteristica as catcar INNER JOIN caracteristica ON (catcar.car_id = caracteristica.car_id) 
		where catcar.cat_id='$cat_id' order by catcar.orden";		

		$conec->ejecutar($sql);
		
		$num=$conec->get_num_registros();
		
		$cad="";
		
		$fun=NEW FUNCIONES;		
		
		for($i=0;$i<$num;$i++)
		{
			
			$objeto=$conec->get_objeto();
			
			switch ($objeto->car_tipo)
			{
			    case 'select':{		             
					 ?>
					 
					    <!--Inicio-->
						<div id="ContenedorDiv">
						   <div class="Etiqueta" ><? echo $objeto->car_descripcion;?></div>
						   <div id="CajaInput">							   
						   <select name="<? echo $objeto->car_nombre;?>" class="caja_texto">
						   <option value="">Seleccione</option>
						   <?php 	                                 							   
								$fun=NEW FUNCIONES;
								$aux = $this->obtener_valor($_GET['id'],$objeto->car_id);
								if ($aux != -1)	
								    $_POST[$objeto->car_nombre] = $aux;
									
								$fun->combo("select det_key as id,det_valor as nombre from caracteristica_detalle where det_car_id=".$objeto->car_id." order by det_orden",$_POST[$objeto->car_nombre]);												
							?>
						   </select>							   
						   </div>
						   
						</div>
						<!--Fin-->
					 
					 
					 
					 <?php
					 break;
				}
				
				case 'input':{
				       $aux = $this->obtener_valor($_GET['id'],$objeto->car_id);
								if ($aux != -1)	
								    $_POST[$objeto->car_nombre] = $aux;
				    ?>
					       <!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><? echo $objeto->car_descripcion;?></div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto" name="<? echo $objeto->car_nombre;?>" id="<? echo $objeto->car_nombre;?>" maxlength="250"  size="<? echo $objeto->car_tamano;?>" value="<?php echo $_POST[$objeto->car_nombre];?>">
							   </div>
							</div>
							<!--Fin-->	
					<?
		            break;
				}
				
				case 'textarea':{
				        $aux = $this->obtener_valor($_GET['id'],$objeto->car_id);
								if ($aux != -1)	
								    $_POST[$objeto->car_nombre] = $aux;
				    ?>
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><? echo $objeto->car_descripcion;?></div>
							   <div id="CajaInput">
									<textarea rows="<? echo $objeto->car_tamano;?>" cols="40" name="<? echo $objeto->car_nombre;?>" id="<? echo $objeto->car_nombre;?>"><?php echo $_POST[$objeto->car_nombre];?></textarea>									
							   </div>
							</div>
							<!--Fin-->
				  
					
				    <?php    
		             break;
				}
			}
					
			?>
			
				
			
			<?php
			
			$conec->siguiente();
		}
	
	
	}
	
	function subir_imagen(&$nombre_imagen,$name,$tmp)
	{	
		 require_once('clases/upload.class.php');

		 $nn=date('d_m_Y_H_i_s_').rand();

		 $upload_class = new Upload_Files();

		 $upload_class->temp_file_name = trim($tmp); 	
		 
		 $upload_class->file_name = $nn.substr(trim($name), -4, 4);

		 $nombre_imagen=$upload_class->file_name;		 		 

		 $upload_class->upload_dir = "imagenes/inmueble/"; 

		 $upload_class->upload_log_dir = "imagenes/inmueble/upload_logs/"; 

		 $upload_class->max_file_size = 1048576; 	

		 $upload_class->ext_array = array(".jpg",".gif",".png");

         $upload_class->crear_thumbnail=false;

		 $valid_ext = $upload_class->validate_extension(); 

		 $valid_size = $upload_class->validate_size(); 

		 $valid_user = $upload_class->validate_user(); 

		 $max_size = $upload_class->get_max_size(); 

		 $file_size = $upload_class->get_file_size(); 

		 $file_exists = $upload_class->existing_file(); 		

		if (!$valid_ext) { 				   

			$result = "La Extension de este Archivo es invalida, Intente nuevamente por favor!"; 

		} 

		elseif (!$valid_size) { 

			$result = "El Tama�o de este archivo es invalido, El maximo tama�o permitido es: $max_size y su archivo pesa: $file_size"; 

		}    

		elseif ($file_exists) { 

			$result = "El Archivo Existe en el Servidor, Intente nuevamente por favor."; 

		} 

		else 
		{		    
			$upload_file = $upload_class->upload_file_with_validation(); 

			if (!$upload_file) { 

				$result = "Su archivo no se subio correctamente al Servidor."; 
			}

			else 
			{ 
					$result = "";

					require_once('clases/class.upload.php');

					$mifile='imagenes/inmueble/'.$upload_class->file_name;

					$handle = new upload($mifile);

					if ($handle->uploaded) 
					{
					    $handle->image_resize          = true;

						$handle->image_ratio           = true;

						$handle->image_y               = 100;

						$handle->image_x               = 100;

						$handle->process('imagenes/inmueble/chica/');

						if (!($handle->processed)) 
						{
					        echo 'error : ' . $handle->error;
					    }

					}
			} 
		} 	

		return $result;	

	}
	
	function obtener_estado_actual($id)
	{
		$conec= new ADO();	
		$sql="select pub_estado from publicacion where pub_inm_id='".$id."'";
		$conec->ejecutar($sql);		
		$objeto=$conec->get_objeto();
		
		return $objeto->pub_estado;
	}
	
	function obtener_contenido($url)
	{
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_HEADER, 0);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);		
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.3) Gecko/20070309 Firefox/2.0.0.3");	
		curl_setopt($curl, CURLOPT_URL, $url);   		
		$contenido = curl_exec($curl); 
		curl_close($curl); 
		
		return $contenido;
	}
	
	function enviar_mail_estado($estadoactual,$estado)
	{
	    require_once('clases/phpmailer/class.phpmailer.php');	
		require_once('config/constante.php');
		
		include_once("clases/EnDecryptText.php");		
		$EnDecryptText = new EnDecryptText();	     
		
		$enviar = false;
						
	     if (($estadoactual == 'No Aprobado') && ($estado == 'Aprobado'))
		 {
		    //enviar mail aprobado
		    $contenido = $this->obtener_contenido(_URL_TEMPLATE_EMAIL."anuncio_activado.html");		
            //echo $contenido; 			
			$contenido = str_replace('@@inm_id',$_GET['id'], $contenido);
			$contenido = str_replace('@@inm_titulo',$_POST['inm_nombre'], $contenido);	
            $subject = "Tu anuncio ha sido Activado";
			$email = $_POST['usu_email'];	
			$enviar = true;
		 }
		 else
		 {
			if (($estadoactual == 'No Aprobado') && ($estado== 'Cancelado')) 
			{
		        $contenido = $this->obtener_contenido(_URL_TEMPLATE_EMAIL."anuncio_cancelado.html");			 
				$contenido = str_replace('@@inm_id',$_GET['id'],$contenido);
				$contenido = str_replace('@@inm_titulo',$_POST['inm_nombre'],$contenido);
				$contenido = str_replace('@@obs',$_POST['pub_obs'],$contenido);				
				$contenido = str_replace('@@url', _URL_WEBSITE.'index.php?dir=autolog_editar&k='.$EnDecryptText->Encrypt_Text($POST['sus_id']).'&id='.$_GET['id'],$contenido);				
				$subject = "Tu anuncio ha sido Rechazado";
				$email = $_POST['usu_email'];	
				$enviar = true;
			}
			else
			{
			    if (($estadoactual == 'Aprobado') && ($estado== 'Cancelado')) 
				{
					$contenido = $this->obtener_contenido(_URL_TEMPLATE_EMAIL."anuncio_cancelado.html");			 
					$contenido = str_replace('@@inm_id',$_GET['id'],$contenido);
					$contenido = str_replace('@@inm_titulo',$_POST['inm_nombre'],$contenido);
					$contenido = str_replace('@@obs',$_POST['pub_obs'],$contenido);				
					$contenido = str_replace('@@url', _URL_WEBSITE.'index.php?dir=autolog_editar&k='.$EnDecryptText->Encrypt_Text($POST['sus_id']).'&id='.$_GET['id'],$contenido);				
					$subject = "Tu anuncio ha sido Rechazado";
					$email = $_POST['usu_email'];	
					$enviar = true;
				}
				else
				{
					if (($estadoactual == 'Cancelado') && ($estado== 'Aprobado')) 
					{
						$contenido = $this->obtener_contenido(_URL_TEMPLATE_EMAIL."anuncio_activado.html");			 
						$contenido = str_replace('@@inm_id',$_GET['id'],$contenido);
						$contenido = str_replace('@@inm_titulo',$_POST['inm_nombre'],$contenido);							
						$subject = "Tu anuncio ha sido Activado";
						$email = $_POST['usu_email'];
						$enviar = true;
					}
				}
			}			
		 }
		
		if ($enviar)
		{
		
			$conec= new ADO();
			
			$sql="select * from ad_parametro limit 1";
			$conec->ejecutar($sql);		
			$fila=$conec->get_objeto();
			
			$cue_nombre=$fila->par_salida; 
					
			$cue_pass=$fila->par_pas_salida;
				
			$mail = new PHPMailer(true);			
			$mail->SetLanguage('es');
			$mail->IsSMTP();
			$mail->SMTPAuth   = true;                              
			$mail->Host       = $fila->par_smtp;      		
			$mail->Timeout=30;
			$mail->CharSet = 'utf-8';							
			$mail->Subject = $subject;				
			$body =  $contenido;			
			$mail->MsgHTML($body);			
			$mail->AddAddress($email);				  
			$mail->SetFrom($cue_nombre, $fila->par_smtp);							
			$mail->Username   = $cue_nombre;  
			$mail->Password   = $cue_pass;	

				try {
					//echo "envio";
					$mail->Send(); 	
					return "el mensaje fue enviado correctamente";					

				} catch (phpmailerException $e) {

						return  "el mensaje no puedo ser enviado";

				} catch (Exception $e) {

						return "el mensaje no puedo ser enviado";
				}
		}
	}
	
	
	function modificar_tcp()
	{
		$conec= new ADO();
			
	        $estado_actual = $this->obtener_estado_actual($_GET['id']);
		
		
			$sql="update inmueble set 
					inm_nombre='".$_POST['inm_nombre']."',
					inm_ciu_id='".$_POST['inm_ciu_id']."',
					inm_precio='".$_POST['inm_precio']."',
					inm_mon_id='".$_POST['inm_mon_id']."',
					inm_superficie='".$_POST['inm_superficie']."',
					inm_direccion='".$_POST['inm_direccion']."',
					inm_detalle='".$_POST['inm_detalle']."',
					inm_cat_id ='".$_POST['inm_cat_id']."',
					inm_zon_id='".$_POST['inm_zon_id']."',
					inm_for_id='".$_POST['inm_for_id']."',
					inm_tipo_superficie='".$_POST['inm_tipo_superficie']."'
					
					where inm_id = '".$_GET['id']."'";
				
				$conec->ejecutar($sql);
				
				/*$sql="update publicacion set
					pub_monto='".$_POST['pub_monto']."',
					pub_estado='".$_POST['pub_estado']."',
					pub_obs='".$_POST['pub_obs']."'
					
					where pub_inm_id = '".$_GET['id']."'";
				
				$conec->ejecutar($sql);
				
				$men_mail = $this->enviar_mail_estado($estado_actual,$_POST['pub_estado']);
				
				$sql="Delete from inmueble_caracteristica where eca_inm_id = '".$_GET['id']."'";
					
				$conec->ejecutar($sql);
				
				$this->guardar_caracteristicas($_POST['inm_cat_id'],$_GET['id']);*/
				
				
				$mensaje='Inmueble Modificado Correctamente!! <br/>'.$men_mail;

				$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
			
		
	}
	
	function formulario_confirmar_eliminacion()
	{
		
		$mensaje='Esta seguro de eliminar el inmueble?';
		
		$this->formulario->ventana_confirmacion($mensaje,$this->link."?mod=$this->modulo",'inm_id');
	}
	
	function eliminar_tcp()
	{
		// $verificar=NEW VERIFICAR;
		
		// $parametros[0]=array('inm_inm_id');
		// $parametros[1]=array($_POST['inm_id']);
		// $parametros[2]=array('ad_inmueble');
		
		// if($verificar->validar($parametros))
		// {
			
			/* Eliminar inmueble */
			$conec= new ADO();			
			$sql="delete from inmueble where inm_id='".$_POST['inm_id']."'";			
			$conec->ejecutar($sql);
			/* Eliminar  publicacion */
			$conec= new ADO();			
			$sql="delete from publicacion where pub_inm_id='".$_POST['inm_id']."'";			
			$conec->ejecutar($sql);
			/* Eliminar inmueble caracteristica */
			$conec= new ADO();			
			$sql="delete from inmueble_caracteristica where eca_inm_id='".$_POST['inm_id']."'";			
			$conec->ejecutar($sql);
			/* Eliminar inmueble fotos */
			
			$sql="select * from inmueble_foto where fot_inm_id='".$_POST['inm_id']."'";
			$conec->ejecutar($sql);		
			$num=$conec->get_num_registros();						
			
			for($i=0;$i<$num;$i++)
			{				
				$objeto=$conec->get_objeto();							
				$mi = trim($objeto->fot_archivo);
				if ($mi != "")
				{
					$mifile="imagenes/inmueble/$mi";			
					@unlink($mifile);				
					$mifile2="imagenes/inmueble/chica/$mi";					
					@unlink($mifile2);
				}
				$conec->siguiente();
			}			
			
			$conec= new ADO();			
			$sql="delete from inmueble_foto where fot_inm_id='".$_POST['inm_id']."'";			
			$conec->ejecutar($sql);
			
			
			
			
			$mensaje='Cliente Eliminada Correctamente!!!';
		// }
		// else
		// {
			// $mensaje='La inmueble no puede ser eliminada, por que esta siendo utilizada en el modulo de Usuarios.';
		// }		
		
		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
	}
	
	function nombre_imagen($id)
	{
		$conec= new ADO();
		
		$sql="select inm_foto from inmueble where inm_id='".$id."'";
		
		$conec->ejecutar($sql);
		
		$objeto=$conec->get_objeto();
		
		return $objeto->inm_foto;
	}
	
	function eliminar_imagen()
	{
		$conec= new ADO();
		
		$mi=$_GET['img'];
		
		$mifile="imagenes/inmueble/$mi";
				
		@unlink($mifile);
		
		$mifile2="imagenes/inmueble/chica/$mi";
		
		@unlink($mifile2);
		
		$conec= new ADO();
		
		$sql="update inmueble set 
						inm_foto=''
						where inm_id = '".$_GET['id']."'";
						
		$conec->ejecutar($sql);
		
		$mensaje='Imagen Eliminada Correctamente!';
			
		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=MODIFICAR&id='.$_GET['id']);
		
	}
}
?>