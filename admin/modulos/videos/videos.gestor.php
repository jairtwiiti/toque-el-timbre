<?php
	
	require_once('videos.class.php');
	
	$videos = new VIDEOS();
	

	if(!($videos->verificar_permisos($_GET['tarea'])))
	{
		?>
		<script>
			location.href="log_out.php";
		</script>
		<?php
	}
	
	
	switch ($_GET['tarea'])
	{
		
		case 'AGREGAR':{
						
							if($videos->datos())
							{
								$videos->insertar_tcp();
							}
							else 
							{
								$videos->formulario_tcp('blanco');
							}
						
						break;}
		case 'VER':{
						$videos->cargar_datos();
												
						$videos->formulario_tcp('ver');
						
						break;}
		case 'MODIFICAR':{
						if($videos->datos())
						{
							$videos->modificar_tcp();
						}
						else 
						{
							if(!($_POST))
							{
								$videos->cargar_datos();
							}
							$videos->formulario_tcp('cargar');
						}
						break;}
								
		case 'ELIMINAR':{
						if(isset($_POST['vi_id']))
						{
							if(trim($_POST['vi_id'])<>"")
							{
								$videos->eliminar_tcp();
							}
							else 
							{
								$videos->dibujar_busqueda();
							}
						}
						else 
						{
							$videos->formulario_confirmar_eliminacion();
						}
							
						break;}
						
		case 'FOTOS':{
							
							if($_GET['acc']=='CARATULA')
							{
								$videos->caratula($_GET['img_id']);
							}
							
							if($_GET['acc']=='ELIMINAR')
							{
								$videos->eliminar_imagen($_GET['img_id']);
							}
							
							if($videos->datos2())
							{
								$videos->guardar_foto();
							}
							
							$videos->formulario_tcp2('cargar');
							
							$videos->dibujar_encabezado2();
							
							$videos->mostrar_busqueda2();
						

						break;
					}
						
		case 'ACCEDER':{
						if($_GET['acc']<>"")
						{
							$videos->orden($_GET['vi'],$_GET['acc'],$_GET['or']);
						}
						$videos->dibujar_busqueda();
						break;
						}
	}
	
?>