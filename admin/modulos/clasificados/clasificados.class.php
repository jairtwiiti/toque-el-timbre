<?php

class CLASIFICADOS extends BUSQUEDA 
{
	var $formulario;
	var $mensaje;
	
	function CLASIFICADOS()
	{
		//permisos
		$this->ele_id=169;
		
		$this->busqueda();
		
		if(!($this->verificar_permisos('AGREGAR')))
		{
			$this->ban_agregar=false;
		}
		//fin permisos
		
		$this->num_registros=50;
		
		$this->coneccion= new ADO();
		
		$this->link='gestor.php';
		
		$this->modulo='clasificados';
		
		$this->formulario = new FORMULARIO();
		
		$this->formulario->set_titulo('CLASIFICADOS');
		
	}
	
	
	function dibujar_busqueda()
	{
		//echo phpinfo();
		
		$this->formulario->dibujar_cabecera();
		
		$this->dibujar_listado();
	}
	
		
	function set_opciones()
	{
				
		$nun=0;
		
		if($this->verificar_permisos('VER'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='VER';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_search.png';
			$this->arreglo_opciones[$nun]["nombre"]='VER';
			$nun++;
		}
		
		if($this->verificar_permisos('MODIFICAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='MODIFICAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_edit.png';
			$this->arreglo_opciones[$nun]["nombre"]='MODIFICAR';
			$nun++;
		}
		
		if($this->verificar_permisos('ELIMINAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='ELIMINAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_drop.png';
			$this->arreglo_opciones[$nun]["nombre"]='ELIMINAR';
			$nun++;
		}
	}
	
	function dibujar_listado()
	{
		$sql="		SELECT ic.*, ciudad.ciu_nombre, m.`mon_abreviado`		FROM inmueble_clasificados ic		INNER JOIN ciudad ON (ciu_id = ic.city_id)		INNER JOIN departamento ON (dep_id = ciudad.ciu_dep_id)		INNER JOIN moneda ON (mon_id = ic.currency_id)		INNER JOIN forma ON (for_id = ic.forma_id)		INNER JOIN categoria ON (cat_id = ic.category_id)		INNER JOIN moneda m ON (m.mon_id = ic.`currency_id`)		where ic.status <> 'Removed'		";		$this->set_sql($sql,' order by ic.id desc');		$this->set_opciones();		$this->dibujar();
		
	}
	
	function dibujar_encabezado()
	{
		?>
		<tr>        	<th>Titulo</th>							<th>Telefono</th>			<th>Ciudad</th>							<th>Precio</th>			<th>Estado</th>            <th class="tOpciones" width="100px">Opciones</th>		</tr>
			
		<?PHP
	}
	
	function mostrar_busqueda()
	{
		$conversor = new convertir();
		
		for($i=0;$i<$this->numero;$i++)
			{
				
				$objeto=$this->coneccion->get_objeto();
				echo '<tr>';					echo "<td>";						echo $objeto->title;					echo "&nbsp;</td>";					echo "<td>";						echo $objeto->phone;					echo "&nbsp;</td>";					echo "<td>";						echo $objeto->ciu_nombre;					echo "&nbsp;</td>";					echo "<td>";						echo $objeto->price." ".$objeto->mon_abreviado;					echo "&nbsp;</td>";					echo "<td>";						echo $objeto->status;					echo "&nbsp;</td>";					echo "<td>";						echo $this->get_opciones($objeto->id);					echo "</td>";				echo "</tr>";
				
				$this->coneccion->siguiente();
			}
	}
	
	function cargar_datos()
	{
		$conec=new ADO();		$sql="		SELECT * FROM inmueble_clasificados		WHERE id = '".$_GET['id']."'";		$conec->ejecutar($sql);		$objeto=$conec->get_objeto();				$_POST['title']=$objeto->title;				$_POST['description']=$objeto->description;        $_POST['phone']=$objeto->phone;					$_POST['price']=$objeto->price;				$_POST['category_id']=$objeto->category_id;						$_POST['inm_ciu_id']=$objeto->city_id;		$_POST['forma_id']=$objeto->forma_id;				$_POST['end_date']=$objeto->end_date;		$_POST['currency_id']=$objeto->currency_id;        $_POST['status']=$objeto->status;
		
	}
	
	function datos()
	{
		if($_POST)
		{
			//texto,  numero,  real,  fecha,  mail.
			$num=0;
			$valores[$num]["etiqueta"]="Nombre";			$valores[$num]["valor"]=$_POST['title'];			$valores[$num]["tipo"]="todo";			$valores[$num]["requerido"]=true;			$num++;
			
			$val=NEW VALIDADOR;
			
			$this->mensaje="";
			
			if($val->validar($valores))
			{
				return true;
			}
				
			else
			{
				$this->mensaje=$val->mensaje;
				return false;
			}
		}
			return false;
	}
	
	function formulario_tcp($tipo)
	{
				switch ($tipo)
				{
					case 'ver':{
								$ver=true;
								break;
								}
							
					case 'cargar':{
								$cargar=true;
								break;
								}
				}
				
				$url=$this->link.'?mod='.$this->modulo;
				
				$red=$url.'&tarea=ACCEDER';
				
				if(!($ver))
				{
					$url.="&tarea=".$_GET['tarea'];
				}
				
				if($cargar)
				{
					$url.='&id='.$_GET['id'];
				}

		
		    $this->formulario->dibujar_tarea('PREGUNTA');
		
			if($this->mensaje<>"")
			{
				$this->formulario->dibujar_mensaje2($this->mensaje);
			}
			?>
		<script type="text/javascript">			function cargar_ciudad(id)			{				var	valores="thid="+id;							ejecutar_ajax('ajax.php','ciudad',valores,'POST');				var	valores="tzid="+id;							ejecutar_ajax('ajax.php','zona',valores,'POST');				}					</script>			<div id="Contenedor_NuevaSentencia">			<form id="frm_sentencia" name="frm_sentencia" action="<?php echo $url;?>" method="POST" enctype="multipart/form-data">  				<div id="FormSent" style="width:650px">					<div class="Subtitulo">Datos</div>						<div id="ContenedorSeleccion">							<!--Inicio-->							<div id="ContenedorDiv">							   <div class="Etiqueta" ><span class="flechas1">* </span>Titulo</div>							   <div id="CajaInput">							   <input type="text" class="caja_texto" name="title" id="title" maxlength="250"  size="40" value="<?php echo $_POST['title'];?>">							   </div>							</div>							<!--Fin-->														<!--Inicio-->							<div id="ContenedorDiv">							   <div class="Etiqueta" >Descripcion</div>							   <div id="CajaInput">							   <textarea class="area_texto" name="description" maxlength="232" id="description" cols="61" rows="5"><?php echo $_POST['description']?></textarea>							   </div>							</div>							<!--Fin-->														<!--Inicio-->							<div id="ContenedorDiv">							   <div class="Etiqueta" >Telefono</div>							   <div id="CajaInput">							   <input type="text" class="caja_texto" name="phone" id="phone" size="40" value="<?php echo $_POST['phone'];?>">							   </div>							</div>							<!--Fin-->														<!--Inicio-->							<div id="ContenedorDiv">							   <div class="Etiqueta" >Precio</div>							   <div id="CajaInput">							   <input type="text" class="caja_texto" name="price" id="price" maxlength="250"  size="40" value="<?php echo $_POST['price'];?>">							   </div>							</div>							<!--Fin-->														<!--Inicio-->							<div id="ContenedorDiv">							   <div class="Etiqueta" >Moneda</div>							   <div id="CajaInput">							   							   <select name="currency_id" class="caja_texto">							   <option value="">Seleccione</option>							   <?php									$fun=NEW FUNCIONES;											$fun->combo("select mon_id as id,mon_descripcion as nombre from moneda",$_POST['currency_id']);																				?>							   </select>							   							   </div>							</div>							<!--Fin-->							<!--Inicio-->							<div id="ContenedorDiv">							   <div class="Etiqueta" >Categoria</div>							   <div id="CajaInput">							   							   <select name="category_id" class="caja_texto">							   <option value="">Seleccione</option>							   <?php 	                                 							   									$fun=NEW FUNCIONES;											$fun->combo("select cat_id as id,cat_nombre as nombre from categoria", $_POST['category_id']);																				?>							   </select>							   </div>							</div>							<!--Fin-->							<!--Inicio-->							<div id="ContenedorDiv">							   <div class="Etiqueta" >Departamento</div>							   <div id="CajaInput">							   <select name="dep_id" class="caja_texto" onchange="cargar_ciudad(this.value);">							   <option value="">Seleccione</option>							   <?php								$fun=NEW FUNCIONES;								$fun->combo("select dep_id as id,dep_nombre as nombre from departamento",'');												?>							   </select>							   </div>							</div>							<!--Fin-->														<!--Inicio-->							<div id="ContenedorDiv">							   <div class="Etiqueta" >Ciudad</div>							   <div id="CajaInput">							   <div id="ciudad">							   <select name="inm_ciu_id" class="caja_texto">							   <option value="">Seleccione</option>							   <?php 	                                if ($_POST["inm_ciu_id"] != ""){							   									$fun=NEW FUNCIONES;											$fun->combo("select ciu_id as id,ciu_nombre as nombre from ciudad",$_POST['inm_ciu_id']);												}								?>							   </select>							   </div>							   </div>							</div>							<!--Fin-->							<!--Inicio-->							<div id="ContenedorDiv">							   <div class="Etiqueta" >Tipo</div>							   <div id="CajaInput">							   <select name="forma_id" class="caja_texto">							   <option value="">Seleccione</option>							   <?php 										$fun=NEW FUNCIONES;											$fun->combo("select for_id as id,for_descripcion as nombre from forma",$_POST['forma_id']);												?>							   </select>							   </div>							</div>							<!--Fin-->																					<!--Inicio-->							<div id="ContenedorDiv">							   <div class="Etiqueta" >Estado</div>							   <div id="CajaInput">							   <select name="status" class="caja_texto">								   <option value="">Seleccione</option>								   <option value="Active" <?php echo $_POST["status"] == "Active" ? 'selected':'' ;?> >Activo</option>								   <option value="Active" <?php echo $_POST["status"] == "Inactive" ? 'selected':'' ;?> >Inactivo</option>							   </select>							   </div>							</div>							<!--Fin-->						</div>												<div id="ContenedorDiv">						   <div id="CajaBotones">								<center>								<?php								if(!($ver)){									?>									<input type="submit" class="boton" name="" value="Guardar">									<input type="reset" class="boton" name="" value="Cancelar">									<input type="button" class="boton" name="" value="Volver" onclick="javascript:location.href='<?php echo $red;?>';">									<?php								}else{									?>									<input type="button" class="boton" name="" value="Volver" onclick="javascript:location.href='<?php echo $red;?>';">									<?php								}								?>								</center>						   </div>						</div>				</div>			</form>		</div>
		<?php
	}
	
	function insertar_tcp()
	{
		$conec= new ADO();
		$sql="insert into inmueble_clasificados(title,description,phone,price,category_id,city_id,forma_id,end_date,currency_id,status)values(
		'".$_POST['title']."',
		'".$_POST['description']."',
		'".$_POST['phone']."',
		".$_POST['price'].",
		".$_POST['category_id'].",
		".$_POST['inm_ciu_id'].",
		".$_POST['forma_id'].",
		'".date("Y-m-d", strtotime("+1 month"))."',
		".$_POST['currency_id'].",
		'".$_POST['status']."'
		)";
		$conec->ejecutar($sql,false);
		$mensaje='Clasificado Agregado Correctamente!!!';
		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
	}
	
	function modificar_tcp()	{

		$conec= new ADO();		
		$sql="update inmueble_clasificados set
		title='".$_POST['title']."',
		description='".$_POST['description']."',
		phone='".$_POST['phone']."',
		price = ".$_POST['price'].",
		category_id = ".$_POST['category_id'].",
		city_id = ".$_POST['inm_ciu_id'].",
		forma_id = ".$_POST['forma_id'].",
		currency_id = '".$_POST['currency_id']."'
		where id = '".$_GET['id']."'";
		$conec->ejecutar($sql);
		$mensaje='Clasificado Modificado Correctamente!!';
		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
			
	}
	function formulario_confirmar_eliminacion(){		$mensaje='Esta seguro de eliminar el inmueble?';		$this->formulario->ventana_confirmacion($mensaje,$this->link."?mod=$this->modulo",'id');	}	function eliminar_tcp(){		$conec= new ADO();				echo $_POST['id']."-------------";				$sql="delete from inmueble_clasificados where id = ".$_POST['id'];		$conec->ejecutar($sql);				$mensaje='Clasificado Eliminado Correctamente!!!';		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');	}
	

	
}
?>