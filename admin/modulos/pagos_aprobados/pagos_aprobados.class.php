﻿<?php

class CONTENIDO extends BUSQUEDA 
{
	var $formulario;
	var $mensaje;
	
	function CONTENIDO()
	{
		//permisos
		$this->ele_id=165;
		
		$this->busqueda();
		
		if(!($this->verificar_permisos('AGREGAR')))
		{
			$this->ban_agregar=false;
		}
		//fin permisos
		
		$this->num_registros=25;
		
		$this->coneccion= new ADO();
		
		$this->arreglo_campos[0]["nombre"]="con_titulo";
		$this->arreglo_campos[0]["texto"]="T�tulo";
		$this->arreglo_campos[0]["tipo"]="cadena";
		$this->arreglo_campos[0]["tamanio"]=40;
		
		$this->link='gestor.php';
		
		$this->modulo='pagos_aprobados';
		
		$this->formulario = new FORMULARIO();
		
		$this->formulario->set_titulo('PAGOS APROBADOS');
		
		
	}
	
	
	function dibujar_busqueda()
	{
		
		$this->formulario->dibujar_cabecera();
		
		$this->buscador_fecha();
		
		$this->dibujar_listado();
	}
	
		
	function set_opciones()
	{
				
		$nun=0;
		
		if($this->verificar_permisos('VER'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='VER';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_search.png';
			$this->arreglo_opciones[$nun]["nombre"]='VER';
			$nun++;
		}
		
		if($this->verificar_permisos('MODIFICAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='MODIFICAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_edit.png';
			$this->arreglo_opciones[$nun]["nombre"]='MODIFICAR';
			$nun++;
		}
		
		if($this->verificar_permisos('ELIMINAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='ELIMINAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_drop.png';
			$this->arreglo_opciones[$nun]["nombre"]='ELIMINAR';
			$nun++;
		}
		
		if($this->verificar_permisos('HABILITAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='HABILITAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/ok.png';
			$this->arreglo_opciones[$nun]["nombre"]='HABILITAR';
			$this->arreglo_opciones[$nun]["script"]="ok";
			$nun++;
		}
	}
	
	/*function aprobar(){
		$conec= new ADO();		
		$pub_vig_fin = date("Y-m-d" ,  mktime(0,0,0,date('m'),date('d'),date('Y')) +  2592000 ); //a�adimos 30 dias mas
			
		$sql="UPDATE publicacion SET pub_enviado='0',pub_vig_fin='$pub_vig_fin' WHERE pub_id=".$_GET['id'];			
		$conec->ejecutar($sql);	
	}*/
	
	function dibujar_listado()
	{
		
		$fecha_actual = date("Y-m-d");
	
		$sql="
		SELECT pag_id, pub_id, inm_nombre, pag_fecha, pag_fecha_pagado, pag_monto, pag_entidad, pag_concepto, pag_estado, CONCAT(usu_nombre,' ',usu_apellido) AS nombre, usu_email, usu_tipo FROM pagos
		LEFT JOIN publicacion ON pub_id = pag_pub_id
		LEFT JOIN inmueble ON inm_id = pub_inm_id
		LEFT JOIN suscripcion ON pago_id = pag_id
		LEFT JOIN usuario ON (usu_id = pub_usu_id OR usu_id = usuario_id)
		where pag_estado = 'Pagado'	AND pag_monto <> 0
		";
		
		
		$this->set_sql($sql,' ORDER BY pag_fecha_pagado DESC');
		
		$this->set_opciones();
		
		$this->dibujar();
		
	}
	
	function dibujar_encabezado()
	{
		?>
			<tr>
	        	<th>ID TRANSACCION</th>
				<th>Nombre Inmueble</th>
				<th>Fecha Pago</th>
				<th>Nombre Usuario</th>
                <th>Email</th>
                <th>Tipo</th>
                <th>Monto</th>
                <th>Metodo</th>
                <th>Concepto</th>
				<th class="tOpciones" width="100px">Opciones</th>
			</tr>
			
		<?PHP
	}
	
	function mostrar_busqueda()
	{
		$conversor = new convertir();
		
		for($i=0;$i<$this->numero;$i++)
			{
				
				$objeto=$this->coneccion->get_objeto();
				echo '<tr>';
									
					echo "<td>";
						echo $objeto->pag_id;
					echo "&nbsp;</td>";
					
					echo "<td>";
						echo $objeto->inm_nombre;
					echo "&nbsp;</td>";
					
					echo "<td>";
						echo $objeto->pag_fecha_pagado;
					echo "&nbsp;</td>";
					
					echo "<td>";
						echo $objeto->nombre;
					echo "&nbsp;</td>";
					
					echo "<td>";
						echo $objeto->usu_email;
					echo "&nbsp;</td>";

                    echo "<td>";
                        echo $objeto->usu_tipo;
                    echo "&nbsp;</td>";
					
					echo "<td>";
						echo $objeto->pag_monto;
					echo "&nbsp;</td>";
					
					echo "<td>";
						echo $objeto->pag_entidad;
					echo "&nbsp;</td>";
					
					echo "<td>";
						echo $objeto->pag_concepto;
					echo "&nbsp;</td>";
					
					echo "<td>";
						echo $this->get_opciones($objeto->pag_id);
					echo "</td>";
				echo "</tr>";
				
				$this->coneccion->siguiente();
			}
	}
	
	function buscador_fecha(){
		?>
        	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
			<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
            <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
            <script>
			$(function() {
				$( "#fecha_inicio" ).datepicker();
				$( "#fecha_fin" ).datepicker();
			});
			</script>
            <div style="font-size:12px; padding:8px 10px;">
			<form method="post" action="reporte/excel.php">
            <table>
            <tr>
            	<td>
                <label style="font-size:12px;">Fecha de Inicio</label>
                <input style="font-size:12px;" type="text" name="fecha_inicio" id="fecha_inicio" />
                </td>
                <td>
                <label style="font-size:12px;">Fecha de Fin</label>
                <input style="font-size:12px;" type="text" name="fecha_fin" id="fecha_fin" />
                </td>
                <td>
                <label style="font-size:12px;">Entidad</label>
                <select style="font-size:12px;" name="entidad">
                	<option value="Oficina">Oficina</option>
                    <option value="Pagosnet">Pagosnet</option>
                    <option value="Banco">Banco</option>
                    <option value="TigoMoney">TigoMoney</option>
                    <option value="Toqueeltimbre">ToqueElTimbre</option>
                    <option value="All">Todos</option>
                </select>
                </td>
                <td>
                <label style="font-size:12px;">Pagados/Pendientes</label>
                <select style="font-size:12px;" name="estado">
                	<option value="Pagado">Pagados</option>
                    <option value="Pendiente">Pendientes</option>
                    <option value="All">Todos</option>
                </select>
                </td>
                <td><input type="submit"  value="Generar Reporte" /></td>
            </tr>
            </table>            
     		</form>
            </div>
    <?php
	}
	
	
	
	function mostrar_detalle_pago(){
		$id_pago = $_GET['id'];
		
		$conec=new ADO();		
		$sql="
		SELECT s.ser_descripcion, s.ser_precio, ps.fech_ini, ps.fech_fin FROM pago_servicio ps 
		INNER JOIN servicios s ON s.ser_id = ps.ser_id
		WHERE ps.pag_id = '".$_GET['id']."'
		";
		$result = mysql_query($sql);
		
		?>
		
		<div id="Contenedor_NuevaSentencia">
			<div id="FormSent" style="width:650px">
				<div class="Subtitulo">Detalle de Pago - <?php echo $_GET['id']; ?></div>
				<div id="ContenedorSeleccion">
				
				<table width="100%" class="tablaLista">
				<thead>
				<tr>
					<th>Servicio</th>
					<th>Fecha Inicio</th>
					<th>Fecha Fin</th>
					<th>Precio</th>
				</tr>
				</thead>
		
		<?php 
		while($row = mysql_fetch_array($result))
		{
			
			?>
			
			<tr>
				<td><?php echo $row['ser_descripcion']?></td>
				<td><?php echo $row['fech_ini']?></td>
				<td><?php echo $row['fech_fin']?></td>
				<td><?php echo $row['ser_precio']; ?></td>
			</tr>
			
			<?php
			$total = $total + $row['ser_precio'];
		}
		?>
				</table>
				
				
				<h2>
					Total: BS. <?php echo $total; ?>
				</h2>
				
				
				</div>
			</div>
		</div>
		
	<?php 
	}
}
?>