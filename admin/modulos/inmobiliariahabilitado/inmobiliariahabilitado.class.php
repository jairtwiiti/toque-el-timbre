<?php

class SUSCRIPTOR extends BUSQUEDA 
{
	var $formulario;
	var $mensaje;
	
	function SUSCRIPTOR()
	{
		//permisos
		$this->ele_id=148;
		
		$this->busqueda();
		
		if(!($this->verificar_permisos('AGREGAR')))
		{
			$this->ban_agregar=false;
		}
		//fin permisos
		
		$this->num_registros=14;
		
		$this->coneccion= new ADO();
		
		$this->arreglo_campos[0]["nombre"]="usu_nombre";
		$this->arreglo_campos[0]["texto"]="Nombre/Razon Social";
		$this->arreglo_campos[0]["tipo"]="cadena";
		$this->arreglo_campos[0]["tamanio"]=40;
		
		$this->arreglo_campos[1]["nombre"]="usu_apellido";
		$this->arreglo_campos[1]["texto"]="Apellido";
		$this->arreglo_campos[1]["tipo"]="cadena";
		$this->arreglo_campos[1]["tamanio"]=25;
		
		$this->arreglo_campos[2]["nombre"]="usu_ci";
		$this->arreglo_campos[2]["texto"]="CI/NIT";
		$this->arreglo_campos[2]["tipo"]="cadena";
		$this->arreglo_campos[2]["tamanio"]=25;	
		
		$this->arreglo_campos[3]["nombre"]="usu_ciu_id";
		$this->arreglo_campos[3]["texto"]="Ciudad";
		$this->arreglo_campos[3]["tipo"]="combosql";
		$this->arreglo_campos[3]["sql"]="select ciu_id as codigo,ciu_nombre as descripcion from ciudad";
		
		$this->arreglo_campos[3]["nombre"]="usu_estado";
		$this->arreglo_campos[3]["texto"]="Estado";
		$this->arreglo_campos[3]["tipo"]="comboarray";
		$this->arreglo_campos[3]["valores"]='Habilitado,Deshabilitado:Habilitado,Deshabiitado';
		
		$this->arreglo_campos[4]["nombre"]="usu_tipo";
		$this->arreglo_campos[4]["texto"]="Tipo";
		$this->arreglo_campos[4]["tipo"]="comboarray";
		$this->arreglo_campos[4]["valores"]='Particular,Inmobiliaria:Particular,Inmobiliaria';
		
		$this->link='gestor.php';
		
		$this->modulo='suscriptor';
		
		$this->formulario = new FORMULARIO();
		
		$this->formulario->set_titulo('USUARIO/SUSCRIPTOR');
		
		
	}
	
	
	function dibujar_busqueda()
	{
		
		$this->formulario->dibujar_cabecera();
		
		$this->dibujar_listado();
	}
	
		
	function set_opciones()
	{
				
		$nun=0;
		
		if($this->verificar_permisos('VER'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='VER';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_search.png';
			$this->arreglo_opciones[$nun]["nombre"]='VER';
			$nun++;
		}
		
		if($this->verificar_permisos('MODIFICAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='MODIFICAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_edit.png';
			$this->arreglo_opciones[$nun]["nombre"]='MODIFICAR';
			$nun++;
		}
		
		if($this->verificar_permisos('ELIMINAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='ELIMINAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_drop.png';
			$this->arreglo_opciones[$nun]["nombre"]='ELIMINAR';
			$nun++;
		}
	}
	
	function dibujar_listado()
	{
		$sql="SELECT usuario.*,ciu_nombre,dep_nombre FROM usuario INNER JOIN ciudad ON (usu_ciu_id = ciu_id)
		                            INNER JOIN departamento ON (ciu_dep_id = dep_id) where usuario.usu_tipo='Inmobiliaria' and usuario.usu_estado='Habilitado' ";
		
		$this->set_sql($sql,' order by usu_orden desc');
		
		$this->set_opciones();
		
		$this->dibujar();
		
	}
	
	function dibujar_encabezado()
	{
		?>
			<tr>
	        	<th>Nombre/Razon Social</th>
				<th>Apellido</th>								<th>Email</th>
				<th>Tipo</th>
				<th>Departamento</th>
				<th>Ciudad</th>				
				<th>CI/NIT</th>
				<th>Celular</th>
				<th>Telefono</th>
				<th>Estado</th>
				<th>Orden</th>
	            <th class="tOpciones" width="100px">Opciones</th>
			</tr>
			
		<?PHP
	}
	
	function mostrar_busqueda()
	{
		$conversor = new convertir();
		
		for($i=0;$i<$this->numero;$i++)
			{
				
				$objeto=$this->coneccion->get_objeto();
				echo '<tr>';
									
					echo "<td>";
						echo $objeto->usu_nombre;
					echo "&nbsp;</td>";
					echo "<td>";
						echo $objeto->usu_apellido;
					echo "&nbsp;</td>";										echo "<td>";						echo $objeto->usu_email;					echo "&nbsp;</td>";
					echo "<td>";
						echo $objeto->usu_tipo;
					echo "&nbsp;</td>";
					echo "<td>";
						echo $objeto->dep_nombre;
					echo "&nbsp;</td>";
					echo "<td>";
						echo $objeto->ciu_nombre;
					echo "&nbsp;</td>";
					echo "<td>";
						echo $objeto->usu_ci;
					echo "&nbsp;</td>";
						echo "<td>";
						echo $objeto->usu_celular;
					echo "&nbsp;</td>";
						echo "<td>";
						echo $objeto->usu_telefono;
					echo "&nbsp;</td>";
					echo "&nbsp;</td>";
						echo "<td>";
						echo $objeto->usu_estado;
					echo "&nbsp;</td>";
					echo "<td>";
						?>
						<center><a href="<?php echo $this->link.'?mod='.$this->modulo.'&tarea=ACCEDER&pro='.$objeto->usu_id.'&acc=s&or='.$objeto->usu_orden;?>"><img src="images/subir.png" border="0"></a><a href="<?php echo $this->link.'?mod='.$this->modulo.'&tarea=ACCEDER&pro='.$objeto->usu_id.'&acc=b&or='.$objeto->usu_orden;?>"><img src="images/bajarr.png" border="0"></a></center>
						<?php
					echo "</td>";
					echo "<td>";
						echo $this->get_opciones($objeto->usu_id);
					echo "</td>";
				echo "</tr>";
				
				$this->coneccion->siguiente();
			}
	}
	
	function orden($proyecto,$accion,$ant_orden)
	{
		$conec= new ADO();
		
		if($accion=='s')
			$cad=" where usu_orden > $ant_orden order by usu_orden asc";
		else
			$cad=" where usu_orden < $ant_orden order by usu_orden desc";

		$consulta = "
		select 
			usu_id,usu_orden 
		from 
			usuario
		$cad
		limit 0,1
		";	

		$conec->ejecutar($consulta);

		$num = $conec->get_num_registros();   

		if($num > 0)
		{
			$objeto=$conec->get_objeto();
			
			$nu_orden=$objeto->usu_orden;
			
			$id=$objeto->usu_id;
			
			$consulta = "update usuario set usu_orden='$nu_orden' where usu_id='$proyecto'";	

			$conec->ejecutar($consulta);
			
			$consulta = "update usuario set usu_orden='$ant_orden' where usu_id='$id'";	

			$conec->ejecutar($consulta);
		}	
	}
	
	function cargar_datos()
	{
		$conec=new ADO();
		
		$sql="select usuario.*,ciu_dep_id as dep_id from usuario inner join ciudad on (usu_ciu_id = ciu_id)		                            
				where usu_id = '".$_GET['id']."'";
		
		$conec->ejecutar($sql);
		
		$objeto=$conec->get_objeto();
		 	 	 	 	 	 	 	 	 
		$_POST['usu_nombre']=$objeto->usu_nombre;		
		$_POST['usu_apellido']=$objeto->usu_apellido;		
		$_POST['usu_email']=$objeto->usu_email;		
		$_POST['usu_sexo']=$objeto->usu_sexo;		
		$_POST['usu_foto']=$objeto->usu_foto;		
		$_POST['usu_telefono']=$objeto->usu_telefono;		
		$_POST['usu_celular']=$objeto->usu_celular;		
		$_POST['usu_direccion']=$objeto->usu_direccion;		
		$_POST['usu_ci']=$objeto->usu_ci;		
		$_POST['usu_ciu_id']=$objeto->usu_ciu_id;		
		$_POST['usu_fecha_nacimiento']=$objeto->usu_fecha_nacimiento;		
		$_POST['usu_latitud']=$objeto->usu_latitud;		
		$_POST['usu_longitud']=$objeto->usu_longitud;
		$_POST['usu_tipo']=$objeto->usu_tipo;
		//$_POST['usu_password']=$objeto->usu_password;
		$_POST['usu_estado']=$objeto->usu_estado;
        $_POST['dep_id']=$objeto->dep_id;		
	}
	
	function datos()
	{
		if($_POST)
		{
			//texto,  numero,  real,  fecha,  mail.
			$num=0;
			$valores[$num]["etiqueta"]="Nombre";
			$valores[$num]["valor"]=$_POST['usu_nombre'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			
			
			$val=NEW VALIDADOR;
			
			$this->mensaje="";
			
			if($val->validar($valores))
			{
				return true;
			}
				
			else
			{
				$this->mensaje=$val->mensaje;
				return false;
			}
		}
			return false;
	}
	
	function formulario_tcp($tipo)
	{
				switch ($tipo)
				{
					case 'ver':{
								$ver=true;
								break;
								}
							
					case 'cargar':{
								$cargar=true;
								break;
								}
				}
				
				$url=$this->link.'?mod='.$this->modulo;
				
				$red=$url;
				
				if(!($ver))
				{
					$url.="&tarea=".$_GET['tarea'];
				}
				
				if($cargar)
				{
					$url.='&id='.$_GET['id'];
				}

		
		    $this->formulario->dibujar_tarea('CLIENTE');
		
			if($this->mensaje<>"")
			{
				$this->formulario->dibujar_mensaje($this->mensaje);
			}
			?>
		<script type="text/javascript">
		   var map;
		   var markers = [];
		   var geocoder;
		   var zooms = 5;		   
		   function initialize() {
				geocoder = new google.maps.Geocoder();				
				var myLatlng = new google.maps.LatLng(-16.930705,-64.782716); 		
				
				var myOptions = {
					zoom: zooms,
					center: myLatlng,
					mapTypeId: google.maps.MapTypeId.ROADMAP
				}
				
				map = new google.maps.Map(document.getElementById("map"), myOptions);						
				
				<?php
				    if (isset($_POST['usu_latitud']) && isset($_POST['usu_longitud']))
				    {				       
				?>
				       var mypoint = new google.maps.LatLng(<? echo $_POST['usu_latitud']; ?>,<? echo $_POST['usu_longitud']; ?>);					 
					   createMarker(mypoint);	
					   map.setCenter(mypoint);			
					   map.setZoom(15);	
				<?php
				    }
				?>
				
				google.maps.event.addListener(map, 'click', function(event) {
				
					if  (markers.length < 1)
					{	
						createMarker(event.latLng);	  
						
					}   
					else
					{	
						markers[0].setPosition(event.latLng);
						actualizar_punto(markers[0]);
						//document.getElementById("latitud").value = 	markers[0].getPosition().lat();	
						//document.getElementById("longitud").value = markers[0].getPosition().lng();
						//var contentString = "<p>"+event.latLng.lat()+"<br/>"+event.latLng.lng()+"</p>";		 
					}	
						
				});
				
				
		   }
		   
		   function actualizar_punto(point)
		   {
		        document.getElementById("latitud").value  =	point.getPosition().lat();	
				document.getElementById("longitud").value = point.getPosition().lng();	
		   }
		   
		   function createMarker(point) {
				
				var clickedLocation = new google.maps.LatLng(point);
				
				markers.push(new google.maps.Marker({
				  position: point,
				  map: map,
				  draggable: true,
				  animation: google.maps.Animation.DROP
				}));  
				
				actualizar_punto(markers[0]);
					
				google.maps.event.addListener(markers[0], 'dragend', function() {	
					
				actualizar_punto(markers[0]);	
						//document.getElementById("latitud").value = 	markers[0].getPosition().lat();	
						//document.getElementById("longitud").value = markers[0].getPosition().lng();	
						
						 // var contentString = "<p>"+markers[0].getPosition().lat()+"<br/>"+markers[0].getPosition().lng()+"</p>";
							
						  // var infowindow = new google.maps.InfoWindow({content: contentString});	
							
							// infowindow.open(map,markers[0]);
				});	
				//map.setCenter(point);       
			}

			function codeAddress() {
				
				var pais    = document.getElementById("map_pais").value;		
				var ciudad  = document.getElementById("map_ciudad").value;
				var tipo  = document.getElementById("map_tipo").value;
				var direccion = document.getElementById("map_direccion").value;
				var address;
				
				if (direccion != "")		   
				   address = tipo+","+direccion+","+ciudad+","+pais;	
				else
				   address = ciudad+","+pais;					
				
				geocoder.geocode( { 'address': address}, function(results, status) {
				  if (status == google.maps.GeocoderStatus.OK) {
					map.setCenter(results[0].geometry.location);			
					map.setZoom(12);
					if (direccion != "")
					   map.setZoom(15);
					
					if  (markers.length < 1)
					{	
						createMarker(results[0].geometry.location);
					}
					else
					{
						markers[0].setPosition(results[0].geometry.location);
					}			
					
				  } else {
					alert("Geocode was not successful for the following reason: " + status);
				  }
				});
			}	
			
		  
			function loadScript() {
			  var script = document.createElement("script");
			  script.type = "text/javascript";
			  script.src = "http://maps.google.com/maps/api/js?sensor=false&callback=initialize";
			  document.body.appendChild(script);
			}
			  
			window.onload = loadScript;
			
			$(document).ready(function() {
				$('.cerrarToogle').hide();				
				$('.cerrarToogle:first').show();
				$('.prueba').click(function(){
					var cat = $(this).parent().find('.cerrarToogle');
					if(cat.is(':visible')){
						$(cat).hide();
					} else {
						$(cat).slideDown('normal');
					};
				});	
			});
			
			function cargar_ciudad(id)
			{
				var	valores="tid="+id;			
				ejecutar_ajax('ajax.php','ciudad',valores,'POST');									
			}
	
</script>	
		<div id="Contenedor_NuevaSentencia">
			<form id="frm_sentencia" name="frm_sentencia" action="<?php echo $url;?>" method="POST" enctype="multipart/form-data">  
				<div id="FormSent" style="width:650px">
				  
					<div class="Subtitulo">Datos</div>
						<div id="ContenedorSeleccion">
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Foto</div>
							   <div id="CajaInput">
							   <?php
								if($_POST['usu_foto']<>"")
								{	$foto=$_POST['usu_foto'];
									$b=true;
								}	
								else
								{	$foto='sin_foto.gif';
									$b=false;
								}
								if(($ver)||($cargar))
								{	?>
									<img src="imagenes/usuario/chica/<?php echo $foto;?>" border="0" ><?php if($b and $_GET['tarea']=='MODIFICAR') echo '<a href="'.$this->link.'?mod='.$this->modulo.'&tarea=MODIFICAR&id='.$_GET['id'].'&img='.$foto.'&acc=Imagen"><img src="images/b_drop.png" border="0"></a>';?><br>
									<input   name="usu_foto" type="file" id="usu_foto" />
									<?php
								}
								else 
								{
									?>
									<input  name="usu_foto" type="file" id="usu_foto" />
									<?php
								}
								?>
								<input   name="fotooculta" type="hidden" id="fotooculta" value="<?php echo $_POST['usu_foto'].$_POST['fotooculta'];?>"/>
							   </div>
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">* </span>Nombre/Razon Social</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto" name="usu_nombre" id="usu_nombre" maxlength="250"  size="40" value="<?php echo $_POST['usu_nombre'];?>">
							   </div>
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Apellido</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto" name="usu_apellido" id="usu_apellido" maxlength="250"  size="40" value="<?php echo $_POST['usu_apellido'];?>">
							   </div>
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">* </span>Tipo Usuario</div>
							   <div id="CajaInput">
								    <select name="usu_tipo" class="caja_texto">
									<option value="" >Seleccione</option>
									<option value="Particular" <?php if($_POST['usu_tipo']=='Particular') echo 'selected="selected"'; ?>>Particular</option>
									<option value="Inmobiliaria" <?php if($_POST['usu_tipo']=='Inmobiliaria') echo 'selected="selected"'; ?>>Inmobiliaria</option>
									</select>
							   </div>
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Sexo</div>
							   <div id="CajaInput">
								    <select name="usu_sexo" class="caja_texto">
									<option value="" >Seleccione</option>
									<option value="Masculino" <?php if($_POST['usu_sexo']=='Masculino') echo 'selected="selected"'; ?>>Masculino</option>
									<option value="Femenino" <?php if($_POST['usu_sexo']=='Femenino') echo 'selected="selected"'; ?>>Femenino</option>
									</select>
							   </div>
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >CI</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto" name="usu_ci" id="usu_ci" size="20" maxlength="250"  value="<?php echo $_POST['usu_ci'];?>">
							   </div>
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Departamento</div>
							   <div id="CajaInput">
							   <select name="usu_dep_id" class="caja_texto" onchange="cargar_ciudad(this.value);">
							   <option value="">Seleccione</option>
							   <?php 		
								$fun=NEW FUNCIONES;		
								$fun->combo("select dep_id as id,dep_nombre as nombre from departamento",$_POST['dep_id']);				
								?>
							   </select>
							   </div>
							   
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Ciudad</div>
							   <div id="CajaInput">
							   <div id="ciudad">
							   <select name="usu_ciu_id" class="caja_texto">
							   <option value="">Seleccione</option>
							   <?php 	
                                 if ($_POST["dep_id"])							   
								 {							   
									$fun=NEW FUNCIONES;		
									$fun->combo("select ciu_id as id,ciu_nombre as nombre from ciudad",$_POST['usu_ciu_id']);				
								}
								?>
							   </select>
							   </div>
							   </div>
							   
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Fecha de Nacimiento</div>
								 <div id="CajaInput">
									<input readonly="readonly" class="caja_texto" name="usu_fecha_nacimiento" id="usu_fecha_nacimiento" size="12" value="<?php echo $_POST['usu_fecha_nacimiento'];?>" type="text">
										<input name="but_fecha_pago" id="but_fecha_pago" class="boton_fecha" value="..." type="button">
										<script type="text/javascript">
														Calendar.setup({inputField     : "usu_fecha_nacimiento"
																		,ifFormat     :     "%Y-%m-%d",
																		button     :    "but_fecha_pago"
																		});
										</script>
								</div>		
							</div>
							<!--Fin-->							
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Email</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto" name="usu_email" id="usu_email" size="40" value="<?php echo $_POST['usu_email'];?>">
							   </div>
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Telefono</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto" name="usu_telefono" id="usu_telefono" size="15" value="<?php echo $_POST['usu_telefono'];?>">
							   </div>
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">*</span>Celular</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto" name="usu_celular" id="usu_celular" size="15" value="<?php echo $_POST['usu_celular'];?>">
							   </div>
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Direccion</div>
							   <div id="CajaInput">
							   <textarea class="area_texto" name="usu_direccion" id="usu_direccion" cols="31" rows="3"><?php echo $_POST['usu_direccion']?></textarea>
							   </div>
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Password</div>
							   <div id="CajaInput">
							   <input type="password" class="caja_texto" name="usu_password" id="usu_password" maxlength="250"  size="20" value="<?php echo $_POST['usu_password'];?>">
							   </div>
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">* </span>Estado</div>
							   <div id="CajaInput">
								    <select name="usu_estado" class="caja_texto">
									<option value="" >Seleccione</option>
									<option value="Habilitado" <?php if($_POST['usu_estado']=='Habilitado') echo 'selected="selected"'; ?>>Habilitado</option>
									<option value="Deshabilitado" <?php if($_POST['usu_estado']=='Deshabilitado') echo 'selected="selected"'; ?>>Deshabilitado</option>
									</select>
							   </div>
							</div>
							<!--Fin-->
							<!--Inicio-->
							<br/>
							<fieldset style='overflow:hidden;'>
							<legend class='prueba' id='mapaCargar'>&nbsp;Ubicaci�n&nbsp;</legend>
							<div class='cerrarToogle'>
							
							<div id="ContenedorDiv">							  
							    <div id="CajaInput">
							   <div style="padding:10px 0px 10px 15px">						   
							        <input id="latitud" name="usu_latitud" type="hidden" value="<?php echo $_POST['usu_latitud'];?>">
									<input id="longitud" name="usu_longitud" type="hidden" value = "<?php echo $_POST['usu_longitud'];?>">									
									<input id="map_pais" name="map_pais" type="hidden" value="Bolivia">
									<table border="0" cellspacing="2">
									    <tr>
										    <td><div style="color: #005C89; font-size:12px" >Ciudad</div></td>
											<td><div style="color: #005C89; font-size:12px" >Tipo</div></td>
											<td><div style="color: #005C89; font-size:12px" >Direccion</div></td>
											<td>&nbsp;</td>
										</tr>
										<tr>
										    <td>
												<select id="map_ciudad" name="map_ciudad" style="width:100px; ">
													 <option value="Santa Cruz de la Sierra">Santa Cruz</option>
													 <option value="La Paz">La Paz</option>	
													 <option value="Cochabamba">Cochabamba</option>	
													 <option value="Tarija">Tarija</option>	
													 <option value="Sucre">Sucre</option>	
													 <option value="Oruro">Oruro</option>	
													 <option value="Potosi">Potosi</option>	
													 <option value="Trinidad">Beni</option>	
													 <option value="Cobija">Pando</option>	
												</select>
											</td>
											<td>
												<select id="map_tipo" name="map_tipo" style="width:100px; ">
													 <option value="Calle">Calle</option>
													 <option value="Av.">Avenida</option>											 
												</select>
											</td>
											<td><input id="map_direccion" type="textbox" size="35" ></td>
											<td>
												<input type="button" class="boton" style="width:100px" value="Buscar Direcci�n" onclick="codeAddress()">
											</td>
										</tr>
									</table>
																		
									
									
									
									
							  </div>
								<div id="map" style="width: 600px; height: 600px;"></div>
							   </div>
							</div>
							<!--Fin-->	
							</div>	
							</fieldset>
						</div>
					
						<div id="ContenedorDiv">
						   <div id="CajaBotones">
								<center>
								<?php
								if(!($ver))
								{
									?>
									<input type="submit" class="boton" name="" value="Guardar">
									<input type="reset" class="boton" name="" value="Cancelar">
									<input type="button" class="boton" name="" value="Volver" onclick="javascript:location.href='<?php echo $red;?>';">
									<?php
								}
								else
								{
									?>
									<input type="button" class="boton" name="" value="Volver" onclick="javascript:location.href='<?php echo $red;?>';">
									<?php
								}
								?>
								</center>
						   </div>
						</div>
				</div>
			</form>
		</div>
		<?php
	}
	
	function insertar_tcp()
	{
		
		// $verificar=NEW VERIFICAR;
		
		// $parametros[0]=array('usu_nombre','usu_apellido');
		// $parametros[1]=array($_POST['usu_nombre'],$_POST['usu_apellido']);
		// $parametros[2]=array('usuario');

		// if($verificar->validar($parametros))
		// {
		    $conec= new ADO();
		
			$sql=" select max(usu_orden) as ultimo from usuario ";
			
			$conec->ejecutar($sql);
			
			$objeto=$conec->get_objeto();
			
			$orden=$objeto->ultimo + 1;
		
		
				
			if($_FILES['usu_foto']['name']<>"")
			{
				$result=$this->subir_imagen($nombre_archivo,$_FILES['usu_foto']['name'],$_FILES['usu_foto']['tmp_name']);
				
				$sql="insert into usuario(usu_nombre,usu_apellido,usu_email,usu_foto,usu_telefono,usu_celular,usu_direccion,usu_ci,usu_fecha_nacimiento,usu_sexo,usu_ciu_id,usu_orden,usu_latitud,usu_longitud,usu_estado,usu_tipo,usu_fecha_registro) values 
									('".$_POST['usu_nombre']."','".$_POST['usu_apellido']."','".$_POST['usu_email']."','".$nombre_archivo."','".$_POST['usu_telefono']."','".$_POST['usu_celular']."','".$_POST['usu_direccion']."','".$_POST['usu_ci']."','".$_POST['usu_fecha_nacimiento']."','".$_POST['usu_sexo']."','".$_POST['usu_ciu_id']."','".$orden."','".$_POST['usu_latitud']."','".$_POST['usu_longitud']."','".$_POST['usu_estado']."','".$_POST['usu_tipo']."',NOW())";
				
				if(trim($result)<>'')
				{					
					$this->formulario->ventana_volver($result,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
				}
				else 
				{
					$conec->ejecutar($sql);
					
					$mensaje='Cliente Agregada Correctamente!!!';
					
					$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
				}
			}
			else
			{
				$sql="insert into usuario(usu_nombre,usu_apellido,usu_email,usu_telefono,usu_celular,usu_direccion,usu_ci,usu_fecha_nacimiento,usu_sexo,usu_ciu_id,usu_orden,usu_latitud,usu_longitud,usu_estado,usu_tipo,usu_fecha_registro) values 
									('".$_POST['usu_nombre']."','".$_POST['usu_apellido']."','".$_POST['usu_email']."','".$_POST['usu_telefono']."','".$_POST['usu_celular']."','".$_POST['usu_direccion']."','".$_POST['usu_ci']."','".$_POST['usu_fecha_nacimiento']."','".$_POST['usu_sexo']."','".$_POST['usu_ciu_id']."','".$orden."','".$_POST['usu_latitud']."','".$_POST['usu_longitud']."','".$_POST['usu_estado']."','".$_POST['usu_tipo']."',NOW())";

				$conec->ejecutar($sql);

				$mensaje='Cliente Agregado Correctamente!!!';

				$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
			}
		// }
		// else
		// {
			// $mensaje='La usuario no puede ser agregada, por que existe una usuario con ese nombre y apellido.';
			
			// $this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
		// }
	
		
	}
	
	function subir_imagen(&$nombre_imagen,$name,$tmp)
	{	
		 require_once('clases/upload.class.php');

		 $nn=date('d_m_Y_H_i_s_').rand();

		 $upload_class = new Upload_Files();

		 $upload_class->temp_file_name = trim($tmp); 	
		 
		 $upload_class->file_name = $nn.substr(trim($name), -4, 4);

		 $nombre_imagen=$upload_class->file_name;		 		 

		 $upload_class->upload_dir = "imagenes/usuario/"; 

		 $upload_class->upload_log_dir = "imagenes/usuario/upload_logs/"; 

		 $upload_class->max_file_size = 1048576; 	

		 $upload_class->ext_array = array(".jpg",".gif",".png");

         $upload_class->crear_thumbnail=false;

		 $valid_ext = $upload_class->validate_extension(); 

		 $valid_size = $upload_class->validate_size(); 

		 $valid_user = $upload_class->validate_user(); 

		 $max_size = $upload_class->get_max_size(); 

		 $file_size = $upload_class->get_file_size(); 

		 $file_exists = $upload_class->existing_file(); 		

		if (!$valid_ext) { 				   

			$result = "La Extension de este Archivo es invalida, Intente nuevamente por favor!"; 

		} 

		elseif (!$valid_size) { 

			$result = "El Tama�o de este archivo es invalido, El maximo tama�o permitido es: $max_size y su archivo pesa: $file_size"; 

		}    

		elseif ($file_exists) { 

			$result = "El Archivo Existe en el Servidor, Intente nuevamente por favor."; 

		} 

		else 
		{		    
			$upload_file = $upload_class->upload_file_with_validation(); 

			if (!$upload_file) { 

				$result = "Su archivo no se subio correctamente al Servidor."; 
			}

			else 
			{ 
					$result = "";

					require_once('clases/class.upload.php');

					$mifile='imagenes/usuario/'.$upload_class->file_name;

					$handle = new upload($mifile);

					if ($handle->uploaded) 
					{
					    $handle->image_resize          = true;

						$handle->image_ratio           = true;

						$handle->image_y               = 100;

						$handle->image_x               = 100;

						$handle->process('imagenes/usuario/chica/');

						if (!($handle->processed)) 
						{
					        echo 'error : ' . $handle->error;
					    }

					}
			} 
		} 	

		return $result;	

	}
	
	function modificar_tcp()
	{
		
			$conec= new ADO();
		
			if($_FILES['usu_foto']['name']<>"")
			{	 	 	 	 	 	 	
				 	 	 	 	
				$result=$this->subir_imagen($nombre_archivo,$_FILES['usu_foto']['name'],$_FILES['usu_foto']['tmp_name']);
				
				
				if (trim($_POST['usu_password'])=="")
				{
				
					$sql="update usuario set 
									usu_nombre='".$_POST['usu_nombre']."',
									usu_apellido='".$_POST['usu_apellido']."',
									usu_email='".$_POST['usu_email']."',
									usu_foto='".$nombre_archivo."',
									usu_telefono='".$_POST['usu_telefono']."',
									usu_celular='".$_POST['usu_celular']."',
									usu_direccion='".$_POST['usu_direccion']."',
									usu_ci='".$_POST['usu_ci']."',
									usu_fecha_nacimiento='".$_POST['usu_fecha_nacimiento']."',
									usu_sexo='".$_POST['usu_sexo']."',
									usu_ciu_id='".$_POST['usu_ciu_id']."',
									usu_latitud='".$_POST['usu_latitud']."',
									usu_longitud='".$_POST['usu_longitud']."',	
									usu_estado='".$_POST['usu_estado']."',
									usu_tipo='".$_POST['usu_tipo']."'
									where usu_id = '".$_GET['id']."'";
							
				}
				else
				{
					$sql="update usuario set 
									usu_nombre='".$_POST['usu_nombre']."',
									usu_apellido='".$_POST['usu_apellido']."',
									usu_email='".$_POST['usu_email']."',
									usu_foto='".$nombre_archivo."',
									usu_telefono='".$_POST['usu_telefono']."',
									usu_celular='".$_POST['usu_celular']."',
									usu_direccion='".$_POST['usu_direccion']."',
									usu_ci='".$_POST['usu_ci']."',
									usu_fecha_nacimiento='".$_POST['usu_fecha_nacimiento']."',
									usu_sexo='".$_POST['usu_sexo']."',
									usu_ciu_id='".$_POST['usu_ciu_id']."',
									usu_latitud='".$_POST['usu_latitud']."',
									usu_longitud='".$_POST['usu_longitud']."',	
									usu_estado='".$_POST['usu_estado']."',
									usu_password='".$_POST['usu_password']."',
									usu_tipo='".$_POST['usu_tipo']."'
									where usu_id = '".$_GET['id']."'";
				
				}
				
				if(trim($result)<>'')
				{
						
					$this->formulario->ventana_volver($result,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
						
				}
				else
				{
					$llave=$_GET['id'];
					
					$mi=trim($_POST['fotooculta']);
					
					if($mi<>"")
					{
						$mifile="imagenes/usuario/$mi";
					
						@unlink($mifile);
						
						$mifile2="imagenes/usuario/chica/$mi";
						
						@unlink($mifile2);
					
					}
					
					
					$conec->ejecutar($sql);
				
					$mensaje='Cliente Modificada Correctamente!!!';
						
					$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
				}
			}
			else
			{
				
				 if (trim($_POST['usu_password'])=="")
				{
					$sql="update usuario set 
									usu_nombre='".$_POST['usu_nombre']."',
									usu_apellido='".$_POST['usu_apellido']."',
									usu_email='".$_POST['usu_email']."',
									usu_telefono='".$_POST['usu_telefono']."',
									usu_celular='".$_POST['usu_celular']."',
									usu_direccion='".$_POST['usu_direccion']."',
									usu_ci='".$_POST['usu_ci']."',
									usu_fecha_nacimiento='".$_POST['usu_fecha_nacimiento']."',
									usu_sexo='".$_POST['usu_sexo']."',
									usu_ciu_id='".$_POST['usu_ciu_id']."',
									usu_latitud='".$_POST['usu_latitud']."',
									usu_longitud='".$_POST['usu_longitud']."',
									usu_estado='".$_POST['usu_estado']."',
									usu_tipo='".$_POST['usu_tipo']."'	
									where usu_id = '".$_GET['id']."'";
				}
				else
				{
				    $sql="update usuario set 
									usu_nombre='".$_POST['usu_nombre']."',
									usu_apellido='".$_POST['usu_apellido']."',
									usu_email='".$_POST['usu_email']."',
									usu_telefono='".$_POST['usu_telefono']."',
									usu_celular='".$_POST['usu_celular']."',
									usu_direccion='".$_POST['usu_direccion']."',
									usu_ci='".$_POST['usu_ci']."',
									usu_fecha_nacimiento='".$_POST['usu_fecha_nacimiento']."',
									usu_sexo='".$_POST['usu_sexo']."',
									usu_ciu_id='".$_POST['usu_ciu_id']."',
									usu_latitud='".$_POST['usu_latitud']."',
									usu_longitud='".$_POST['usu_longitud']."',
									usu_estado='".$_POST['usu_estado']."',
									usu_password='".$_POST['usu_password']."',
									usu_tipo='".$_POST['usu_tipo']."'	
									where usu_id = '".$_GET['id']."'";
				
				
				
				}
				$conec->ejecutar($sql);

				$mensaje='Cliente Modificado Correctamente!!!';

				$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
			}
		
	}
	
	function formulario_confirmar_eliminacion()
	{
		
		$mensaje='Esta seguro de eliminar el usuario?';
		
		$this->formulario->ventana_confirmacion($mensaje,$this->link."?mod=$this->modulo",'usu_id');
	}
	
	function eliminar_tcp()
	{
		// $verificar=NEW VERIFICAR;
		
		// $parametros[0]=array('usu_usu_id');
		// $parametros[1]=array($_POST['usu_id']);
		// $parametros[2]=array('ad_usuario');
		
		// if($verificar->validar($parametros))
		// {
			$llave=$_POST['usu_id'];
					
			$mi=$this->nombre_imagen($llave);
			
			if(trim($mi)<>"")
			{
				$mifile="imagenes/usuario/$mi";
			
				@unlink($mifile);
				
				$mifile2="imagenes/usuario/chica/$mi";
					
				@unlink($mifile2);
			}
			
			$conec= new ADO();
			
			$sql="delete from usuario where usu_id='".$_POST['usu_id']."'";
			
			$conec->ejecutar($sql);
			
			$mensaje='Cliente Eliminada Correctamente!!!';
		// }
		// else
		// {
			// $mensaje='La usuario no puede ser eliminada, por que esta siendo utilizada en el modulo de Usuarios.';
		// }		
		
		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
	}
	
	function nombre_imagen($id)
	{
		$conec= new ADO();
		
		$sql="select usu_foto from usuario where usu_id='".$id."'";
		
		$conec->ejecutar($sql);
		
		$objeto=$conec->get_objeto();
		
		return $objeto->usu_foto;
	}
	
	function eliminar_imagen()
	{
		$conec= new ADO();
		
		$mi=$_GET['img'];
		
		$mifile="imagenes/usuario/$mi";
				
		@unlink($mifile);
		
		$mifile2="imagenes/usuario/chica/$mi";
		
		@unlink($mifile2);
		
		$conec= new ADO();
		
		$sql="update usuario set 
						usu_foto=''
						where usu_id = '".$_GET['id']."'";
						
		$conec->ejecutar($sql);
		
		$mensaje='Imagen Eliminada Correctamente!';
			
		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=MODIFICAR&id='.$_GET['id']);
		
	}
}
?>