<?php

class CONTENIDO extends BUSQUEDA 
{
	var $formulario;
	var $mensaje;
	
	function CONTENIDO()
	{
		//permisos
		$this->ele_id=166;
		
		$this->busqueda();
		
		if(!($this->verificar_permisos('AGREGAR')))
		{
			$this->ban_agregar=false;
		}
		//fin permisos
		
		$this->num_registros=25;
		
		$this->coneccion= new ADO();
		
		$this->arreglo_campos[0]["nombre"]="pag_id";
		$this->arreglo_campos[0]["texto"]="ID Transanccion";
		$this->arreglo_campos[0]["tipo"]="cadena";
		$this->arreglo_campos[0]["tamanio"]=40;
		
		$this->link='gestor.php';
		
		$this->modulo='pagos_pendientes_suscripciones';
		
		$this->formulario = new FORMULARIO();
		
		$this->formulario->set_titulo('PAGOS PENDIENTES POR SUSCRIPCIONES');
		
		
	}
	
	
	function dibujar_busqueda()
	{
		
		$this->formulario->dibujar_cabecera();
		
		$this->dibujar_listado();
	}
	
		
	function set_opciones()
	{
				
		$nun=0;
		
		if($this->verificar_permisos('VER'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='VER';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_search.png';
			$this->arreglo_opciones[$nun]["nombre"]='VER';
			$nun++;
		}
		
		if($this->verificar_permisos('MODIFICAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='MODIFICAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_edit.png';
			$this->arreglo_opciones[$nun]["nombre"]='MODIFICAR';
			$nun++;
		}
		
		if($this->verificar_permisos('ELIMINAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='ELIMINAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_drop.png';
			$this->arreglo_opciones[$nun]["nombre"]='ELIMINAR';
			$nun++;
		}
		
		if($this->verificar_permisos('HABILITAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='HABILITAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/ok.png';
			$this->arreglo_opciones[$nun]["nombre"]='HABILITAR';
			$this->arreglo_opciones[$nun]["script"]="ok";
			$nun++;
		}
		
		if($this->verificar_permisos('APROBAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='APROBAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/ok.png';
			$this->arreglo_opciones[$nun]["nombre"]='APROBAR';
			$this->arreglo_opciones[$nun]["script"]="ok";
			$nun++;
		}
	}
	
	
	
	
	
	
	
	
	function verificar_existencia_otros_pagos($pag_id, $pub_id, $ser_id){
        $conec= new ADO();
        /*$sql = "
        SELECT p.pag_entidad, ps.fech_ini, ps.fech_fin FROM pagos p
        INNER JOIN pago_servicio ps ON (ps.pag_id = p.pag_id)
        INNER JOIN servicios ser ON (ser.`ser_id` = ps.`ser_id` AND ser.`ser_tipo` = '$ser_type')
        WHERE p.pag_pub_id = ".$pub_id." AND p.`pag_estado` = 'Pagado' AND p.pag_id <> '".$pag_id."'
        ORDER BY p.pag_fecha DESC LIMIT 1
        ";*/
        $sql = "
        SELECT p.pag_entidad, ps.fech_ini, ps.fech_fin FROM pagos p
        INNER JOIN pago_servicio ps ON (ps.pag_id = p.pag_id AND ps.ser_id = ".$ser_id." AND (CURDATE() >= ps.`fech_ini` AND CURDATE() <= ps.`fech_fin`) )
        WHERE p.pag_pub_id = ".$pub_id." AND p.`pag_id` <> '$pag_id'
        ORDER BY p.pag_fecha_pagado DESC, p.pag_fecha DESC
        LIMIT 1
        ";
        $result = $conec->ejecutar($sql);
        $resultado = $conec->get_objeto($result);

        return $resultado->fech_fin;
	}
	
	function obtener_id_publicacion($pag_id){
		$conec= new ADO();
		$sql = "
		SELECT pag_pub_id FROM pagos 
		WHERE pag_id = '".$pag_id."'
		LIMIT 1
		";
		$result = $conec->ejecutar($sql);
		$resultado = $conec->get_objeto($result);
		return $resultado->pag_pub_id;
	}
	
	function obtener_id_servicios($pag_id){
		$conec= new ADO();
		$sql = "
		SELECT ser_id FROM pago_servicio
		WHERE pag_id = '".$pag_id."'
		";
		$result = mysql_query($sql);
		
		while ($row = mysql_fetch_array($result)){
			$resultado[] = $row['ser_id'];
		}
		return $resultado;
	}
	
	function obtener_email_de_pago($pag_id){
		$conec= new ADO();
		$sql = "
		SELECT `usu_email` FROM pagos
		INNER JOIN publicacion ON pub_id = pag_pub_id
		INNER JOIN usuario ON usu_id = pub_usu_id
		WHERE pag_id =  '".$pag_id."'
		";
		$result = mysql_query($sql);
		$email = mysql_fetch_object($result);
		
		return $email->usu_email;
	}
	
	
	function obtener_id_publicacion_estado($pag_id){
		$conec= new ADO();
		$sql = "
		SELECT pag_pub_id FROM pagos 
		WHERE pag_id = '".$pag_id."'
		LIMIT 1
		";
		$result = mysql_query($sql);
		$resultado = mysql_fetch_object($result);
		return $resultado->pag_pub_id;
	}
	
	function obtener_id_inmueble($pub_id){
		$conec= new ADO();
		$sql = "
		SELECT inm_id, inm_nombre FROM publicacion
		INNER JOIN inmueble on (inm_id = pub_inm_id)
		WHERE pub_id = '".$pub_id."'
		LIMIT 1
		";
		$result = mysql_query($sql);
		$resultado = mysql_fetch_object($result);
		return $resultado->pub_inm_id;
	}

    function verificar_idpago_esta_pagado($pag_id){
        $conec= new ADO();
        $sql = "
		SELECT COUNT(p.`pag_id`) AS cantidad FROM pagos p
        WHERE p.`pag_id` = '$pag_id' AND p.`pag_estado` = 'Pagado'
		";
        $result = mysql_query($sql);
        $resultado = mysql_fetch_object($result);
        return $resultado->cantidad > 0 ? true : false;
    }

    function obtener_id_usuario($pag_id){
        $conec= new ADO();
        $sql = "
		SELECT s.`usuario_id` FROM suscripcion s
        WHERE s.`pago_id` = '$pag_id'
        LIMIT 1
		";
        $result = mysql_query($sql);
        $resultado = mysql_fetch_object($result);
        return $resultado->usuario_id;
    }

    function obtener_mayor_fecha_servicio($pub_id){
        $sql = "
        SELECT MAX(ps.`fech_fin`) AS mayor_fecha FROM pagos p
        INNER JOIN pago_servicio ps ON (ps.`pag_id` = p.`pag_id` AND CURDATE() <= ps.`fech_fin`)
        WHERE p.`pag_pub_id` = '$pub_id' AND p.`pag_estado` = 'Pagado'
        ";
        $result = mysql_query($sql);
        $resultado = mysql_fetch_object($result);
        return $resultado->mayor_fecha;
    }

    public function obtener_publicacion($pag_id){
        $conec= new ADO();
        $sql = "
        SELECT pub.`pub_id`, pub.`pub_vig_fin` FROM pagos p
        INNER JOIN publicacion pub ON (pub.`pub_id` = p.`pag_pub_id`)
        WHERE p.`pag_id` = '$pag_id'
        GROUP BY pub.`pub_id`
        ";
        $result = mysql_query($sql);
        $resultado = mysql_fetch_object($result);
        return $resultado;
    }

    public function get_payment_pending_subscription_by_user($user_id){
        $conec= new ADO();
        $sql = "
        SELECT pag.pag_id, pag.`pag_pub_id` FROM publicacion pub
        INNER JOIN pagos pag ON (pag.`pag_pub_id` = pub.`pub_id` AND pag.`pag_estado` = 'Pendiente' AND pag.`pag_concepto` = 'Anuncio')
        WHERE pub.`pub_usu_id` = '$user_id' AND CURDATE() <= pag.`pag_fecha_ven`
        GROUP BY pag.`pag_id`
        ";
        $result = mysql_query($sql);

        while ($row = mysql_fetch_array($result)){
            $resultado[] = $row;
        }
        return $resultado;
    }

    // Esta funcion se utiliza cuando se verifica el pago de la suscripcion
    public function pay_publications_pending($id_user){
        $conec= new ADO();
        $conec2 = new ADO();

        $payments = $this->get_payment_pending_subscription_by_user($id_user);

        if(count($payments) > 0){
            foreach($payments as $obj_pago){
                $id_pago = $obj_pago["pag_id"];


                $codigos_servicios = $this->obtener_id_servicios($id_pago);
                $fecha_pagado = date("Y-m-d H:i:s");

                $sql = "UPDATE pagos SET pag_estado = 'Pagado', pag_monto = 0, pag_entidad = 'Suscripcion', pag_fecha_pagado = '".$fecha_pagado."' WHERE pag_id = '" . $id_pago . "'";
                $conec->ejecutar($sql);

                $total = 0;
                foreach($codigos_servicios as $ser_id){

                    if($ser_id != ""){
                        $conec3= new ADO();

                        $sql = "SELECT * FROM servicios WHERE ser_id = " . $ser_id;
                        $conec3->ejecutar($sql);
                        $obj_servicio = $conec3->get_objeto();

                        $num = $conec3->get_num_registros($result);

                        $fecha_fin_anterior_pago = $this->verificar_existencia_otros_pagos($id_pago, $obj_pago["pag_pub_id"], $obj_servicio->ser_id);

                        if($num > 0 && !empty($fecha_fin_anterior_pago)){
                            $fech_inicio = $fecha_fin_anterior_pago;
                        }else{
                            $fech_inicio = date("Y-m-d");
                        }

                        $fec_vencimi = date("Y-m-d", strtotime($fech_inicio. " + " . ($obj_servicio->ser_dias - 1) . " days"));
                        $sql = "UPDATE pago_servicio SET fech_ini = '".$fech_inicio."', fech_fin = '".$fec_vencimi."' WHERE pag_id = '" . $id_pago . "' and ser_id = " . $ser_id;
                        $conec2->ejecutar($sql);

                        $total = $total + $obj_servicio->ser_precio;
                    }
                }

                $publicacion = $this->obtener_publicacion($id_pago);
                $mayor_fecha = $this->obtener_mayor_fecha_servicio($publicacion->pub_id);
                if($mayor_fecha > $publicacion->pub_vig_fin){
                    $pub_vig_fin = $mayor_fecha;
                    $sql = "UPDATE publicacion SET pub_estado='Aprobado', pub_vig_fin='". $pub_vig_fin ."' WHERE pub_id = '".$publicacion->pub_id."'";
                }else{
                    $sql = "UPDATE publicacion SET pub_estado='Aprobado' WHERE pub_id = '".$publicacion->pub_id."'";
                }

                $conec->ejecutar($sql);
                $obj_inmueble = $this->obtener_id_inmueble($publicacion->pub_id);
                $sql="update inmueble set inm_publicado='Si' where inm_id = '".$obj_inmueble->inm_id."'";
                $conec->ejecutar($sql);






            }
        }

    }
	
	function aprobar(){
		$conec= new ADO();
		$conec2= new ADO();			
		
		$id_pago = $_GET['id'];
        $sw_pago = $this->verificar_idpago_esta_pagado($id_pago);
        $id_user = $this->obtener_id_usuario($id_pago);

        if(!$sw_pago){
            $codigos_servicios = $this->obtener_id_servicios($id_pago);

            $sql = "UPDATE pagos SET pag_estado = 'Pagado', pag_entidad = 'Oficina', pag_fecha_pagado = NOW() WHERE pag_id = '" . $id_pago . "'";
            $conec->ejecutar($sql);

            $total = 0;
            foreach($codigos_servicios as $ser_id){

                if($ser_id != ""){
                    $conec3= new ADO();
                    $sql = "SELECT * FROM servicios WHERE ser_id = " . $ser_id;
                    $conec3->ejecutar($sql);

                    $obj_servicio = $conec3->get_objeto();
                    //$fecha_fin_anterior_pago = $this->verificar_existencia_otros_pagos($id_pago, $id_pub, $obj_servicio->ser_tipo);

                    /*if($num > 0 && $fecha_fin_anterior_pago != ""){
                        $fech_inicio = $fecha_fin_anterior_pago;
                    }else{*/
                        $fech_inicio = date("Y-m-d");
                    //}

                    $fec_vencimi = date("Y-m-d", strtotime($fech_inicio. " + " . ($obj_servicio->ser_dias - 1) . " days"));

                    $sql = "UPDATE pago_servicio SET fech_ini = '".$fech_inicio."', fech_fin = '".$fec_vencimi."' WHERE pag_id = '" . $id_pago . "' and ser_id = " . $ser_id;
                    $conec2->ejecutar($sql);

                    $total = $total + $obj_servicio->ser_precio;
                }
            }

            $sql = "UPDATE suscripcion SET estado='Activo', fecha_inicio='". $fech_inicio ."', fecha_fin='". $fec_vencimi ."' WHERE pago_id = '".$id_pago."'";
            $conec->ejecutar($sql);
            $this->pay_publications_pending($id_user);
        }
	}
	

	function dibujar_listado()
	{
		?>
		<script>		
		function ejecutar_script(id,tarea){
				var txt = 'Esta seguro que desea marcar como pagada esta publicacion?';
				
				$.prompt(txt,{ 
					buttons:{Aceptar:true, Cancelar:false},
					callback: function(v,m,f){
						
						if(v){
								location.href='gestor.php?mod=pagos_pendientes_suscripciones&tarea='+tarea+'&id='+id;
						}
												
					}
				});
			}

		</script>
		<?php
		
		$sql="
		SELECT p.pag_id, p.pag_fecha, p.pag_monto, p.pag_entidad, p.pag_estado, CONCAT(usu_nombre,' ',usu_apellido) AS nombre, usu_email FROM pagos p
        INNER JOIN suscripcion su ON (su.`pago_id` = p.`pag_id`)
        INNER JOIN usuario ON usu_id = su.`usuario_id`
        where p.pag_estado = 'Pendiente'
        AND p.pag_concepto = 'Suscripcion'
        AND CURDATE() BETWEEN DATE(p.pag_fecha) AND DATE(p.pag_fecha_ven)
		";
		
		$this->set_sql($sql,' ORDER BY pag_fecha DESC');
		
		$this->set_opciones();
		
		$this->dibujar();
		
	}
	
	function dibujar_encabezado()
	{
		?>
			<tr>
	        	<th>ID TRANSACCION</th>
				<th>Nombre Usuario</th>
                <th>Email</th>
				<th>Fecha Pago</th>
				<th>Monto</th>
				<th>Metodo</th>
				<th>Estado</th>
				<th class="tOpciones" width="100px">Opciones</th>
			</tr>
			
		<?PHP
	}
	
	function mostrar_busqueda()
	{
		$conversor = new convertir();
		
		for($i=0;$i<$this->numero;$i++)
			{
				
				$objeto=$this->coneccion->get_objeto();
				echo '<tr>';
									
					echo "<td>";
						echo $objeto->pag_id;
					echo "&nbsp;</td>";
					
					echo "<td>";
						echo $objeto->nombre;
					echo "&nbsp;</td>";
					
					echo "<td>";
						echo $objeto->usu_email;
					echo "&nbsp;</td>";
					
					echo "<td>";
						echo $objeto->pag_fecha;
					echo "&nbsp;</td>";
					
					echo "<td>";
						echo $objeto->pag_monto;
					echo "&nbsp;</td>";
					
					echo "<td>";
                        if(empty($objeto->pag_entidad)){
                            echo "No Definido";
                        }else{
                            echo $objeto->pag_entidad;
                        }
					echo "&nbsp;</td>";
					
					echo "<td>";
						echo $objeto->pag_estado;
					echo "&nbsp;</td>";
					
					echo "<td>";
						echo $this->get_opciones($objeto->pag_id);
					echo "</td>";
				echo "</tr>";
				
				$this->coneccion->siguiente();
			}
	}
	
	function mostrar_detalle_pago(){
		$id_pago = $_GET['id'];
		
		$conec=new ADO();		
		$sql="
		SELECT s.ser_descripcion, s.ser_precio, ps.fech_ini, ps.fech_fin FROM pago_servicio ps 
		INNER JOIN servicios s ON s.ser_id = ps.ser_id
		WHERE ps.pag_id = '".$_GET['id']."'
		";
		$result = mysql_query($sql);
		
		?>
		
		<div id="Contenedor_NuevaSentencia">
			<div id="FormSent" style="width:650px">
				<div class="Subtitulo">Detalle de Pago - <?php echo $_GET['id']; ?></div>
				<div id="ContenedorSeleccion">
				
				<table width="100%" class="tablaLista">
				<thead>
				<tr>
					<th>Servicio</th>
					<th>Fecha Inicio</th>
					<th>Fecha Fin</th>
					<th>Precio</th>
				</tr>
				</thead>
		
		<?php 
		while($row = mysql_fetch_array($result))
		{
			
			?>
			
			<tr>
				<td><?php echo $row['ser_descripcion']?></td>
				<td><?php echo $row['fech_ini']?></td>
				<td><?php echo $row['fech_fin']?></td>
				<td><?php echo $row['ser_precio']; ?></td>
			</tr>
			
			<?php
			$total = $total + $row['ser_precio'];
		}
		?>
				</table>
				
				
				<h2>
					Total: BS. <?php echo $total; ?>
				</h2>
				
				
				</div>
			</div>
		</div>
		
	<?php 
	}
	
	
}
?>