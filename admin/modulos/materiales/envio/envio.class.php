<?php

//require_once("clases/phpmailer/class.phpmailer.php");

class ENVIO extends BUSQUEDA
{
	var $formulario;
	var $mensaje;
	
	function ENVIO()
	{
		//permisos
		$this->ele_id=132;
		
		$this->busqueda();
		
		if(!($this->verificar_permisos('AGREGAR')))
		{
			$this->ban_agregar=false;
		}
		//fin permisos
		
		$this->num_registros=14;
		
		$this->coneccion= new ADO();		
		
		
		
		
		$this->arreglo_campos[0]["nombre"]="env_nombre";
		$this->arreglo_campos[0]["texto"]="Nombre";
		$this->arreglo_campos[0]["tipo"]="cadena";
		$this->arreglo_campos[0]["tamanio"]=25;
		
		$this->arreglo_campos[1]["nombre"]="env_emp_id";
		$this->arreglo_campos[1]["texto"]="Empresa";
		$this->arreglo_campos[1]["tipo"]="combosql";
		$this->arreglo_campos[1]["sql"]="select emp_id as codigo,emp_nombre as descripcion from empresa where emp_estado='Habilitado'";
		
		$this->arreglo_campos[2]["nombre"]="env_fecha_envio";
		$this->arreglo_campos[2]["texto"]="Fecha Envio";
		$this->arreglo_campos[2]["tipo"]="fecha";
		$this->arreglo_campos[0]["tamanio"]=25;	
		
		
		
		$this->link='gestor.php';
		
		$this->modulo='envio';
		
		$this->formulario = new FORMULARIO();
		
		$this->formulario->set_titulo('ENVIO DE NOTAS');
	}
	
	
	
	function dibujar_busqueda()
	{
		?>
		<script>		
		function ejecutar_script(id,tarea){
				var txt = 'Esta seguro de eliminar el Envio?';
				
				$.prompt(txt,{ 
					buttons:{Si:true, No:false},
					callback: function(v,m,f){
						
						if(v){
								location.href='gestor.php?mod=envio&tarea='+tarea+'&id='+id;
						}
												
					}
				});
		}

		</script>
		<?php
		$this->formulario->dibujar_cabecera();
		
		$this->dibujar_listado();
	}
	
	function set_opciones()
	{
		$nun=0;
		
		if($this->verificar_permisos('VER'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='VER';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_search.png';
			$this->arreglo_opciones[$nun]["nombre"]='VER';
			$nun++;
		}
		
		if($this->verificar_permisos('MODIFICAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='MODIFICAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_edit.png';
			$this->arreglo_opciones[$nun]["nombre"]='MODIFICAR';
			$nun++;
		}
		
		
		if($this->verificar_permisos('ELIMINAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='ELIMINAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_drop.png';
			$this->arreglo_opciones[$nun]["nombre"]='ELIMINAR';
			$this->arreglo_opciones[$nun]["script"]="ok";
			$nun++;
		}	

        if($this->verificar_permisos('ENVIAR_MAIL'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='ENVIAR_MAIL';
			$this->arreglo_opciones[$nun]["imagen"]='images/mail_send.png';
			$this->arreglo_opciones[$nun]["nombre"]='ENVIAR MAIL';
			$nun++;
		}		
	}
	
	function dibujar_listado()
	{
		$sql="SELECT 
				 env_id,env_nombre,env_fecha_creado,env_fecha_envio,env_hora_envio,env_emp_id,emp_nombre,env_estado
			  FROM 
				envio inner join empresa on (env_emp_id = emp_id)		
				";
		
		$this->set_sql($sql,' order by env_id');
		
		$this->set_opciones();
		
		$this->dibujar();
		
	}
	
	function dibujar_encabezado()
	{
		?>
			<tr>
			    <th>Nombre</th>
				<th>Fecha Creado</th>
	        	<th>Fecha Envio</th>
				<th>Hora Envio</th>
				<th>Empresa</th>
				<th>Estado</th>				
	            <th class="tOpciones" width="150px">Opciones</th>
			</tr>
		<?PHP
	}
	
	function mostrar_busqueda()
	{
		$conversor = new convertir();
		
		for($i=0;$i<$this->numero;$i++)
			{
				
				$objeto=$this->coneccion->get_objeto();
				echo '<tr>';
					echo "<td>";
						echo $objeto->env_nombre;
					echo "</td>";
					echo "<td>";
					    
					    echo $conversor->get_fecha_latina($objeto->env_fecha_creado);						
					echo "</td>";
					echo "<td>";
					    echo $conversor->get_fecha_latina($objeto->env_fecha_envio);						
					echo "</td>";
					echo "<td>";
						echo $objeto->env_hora_envio;
					echo "</td>";
					echo "<td>";
						echo $objeto->emp_nombre;
					echo "</td>";
					echo "<td>";
						echo $objeto->env_estado;
					echo "</td>";
					echo "<td>";
						echo $this->get_opciones($objeto->env_id);
					echo "</td>";
				echo "</tr>";
				
				$this->coneccion->siguiente();
			}
	}
	
	function cargar_datos()
	{
		$conec=new ADO();
		
		$sql="select 
			*  
		from envio
				where env_id = '".$_GET['id']."'";
		
		$conec->ejecutar($sql);
		
		$objeto=$conec->get_objeto();
		
		$_POST['env_id']=$objeto->env_id;
		$_POST['env_nombre']=$objeto->env_nombre;
		$_POST['env_fecha_creado']=$objeto->env_fecha_creado;
		$_POST['env_fecha_envio']=$objeto->env_fecha_envio;
		$_POST['env_hora_envio']=$objeto->env_hora_envio;
		$_POST['env_fecha_ini']=$objeto->env_fecha_ini;
		$_POST['env_fecha_fin']=$objeto->env_fecha_fin;
		$_POST['env_emp_id']=$objeto->env_emp_id;
		$_POST['env_emails']=$objeto->env_emails;
		$_POST['env_estado']=$objeto->env_estado;		
	}
	
	function datos()
	{
		if($_POST)
		{
			//texto,  numero,  real,  fecha,  mail.
			$num=0;
			// $valores[$num]["etiqueta"]="Fecha";
			// $valores[$num]["valor"]=$_POST['seg_fecha'];
			// $valores[$num]["tipo"]="fecha";
			// $valores[$num]["requerido"]=true;
			// $num++;
			$valores[$num]["etiqueta"]="Nombre";
			$valores[$num]["valor"]=$_POST['env_nombre'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			$valores[$num]["etiqueta"]="Emails";
			$valores[$num]["valor"]=$_POST['env_emails'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			
			$val=NEW VALIDADOR;
			
			$this->mensaje="";
			
			if($val->validar($valores))
			{
				return true;
			}
				
			else
			{
				$this->mensaje=$val->mensaje;
				return false;
			}
		}
			return false;
		
	}
	
	function formulario_tcp($tipo)
	{
		$conversor = new convertir();		
		switch ($tipo)
		{
			case 'ver':{
						$ver=true;
						break;
						}
					
			case 'cargar':{
						$cargar=true;
						break;
						}
		}
		
		$url=$this->link.'?mod='.$this->modulo;
		
		$red=$url.'&tarea=ACCEDER';
		
		if(!($ver))
		{
			$url.="&tarea=".$_GET['tarea'];
		}
		
		if($cargar)
		{
			$url.='&id='.$_GET['id'];
		}
		
		$this->formulario->dibujar_tarea();
		
		if($this->mensaje<>"")
		{
			$this->formulario->mensaje('Error',$this->mensaje);
		}
		   
		    $fun=NEW FUNCIONES;		
			?>
			 
			
			
			<script type="text/javascript">
			
			$().ready(function(){
			   
				
				
               $('#detper').delegate('.borrar-detalle','click',function(){
					if (confirm('Esta usted seguro de eliminar esta fila?')){				   
						var con=1;       					
						var $link = $(this);					
						$link.closest('tr').remove();						
						$("#detper tbody tr").each(function(){						
							 $(this).children('td').eq(0).text(con);
							 con++;
						});						
					}
				});				
			
			});
			
							
				function verificar(id)
				{
					var cant = $('#tprueba tbody').children().length;               
					var ban=true;
					if(cant > 0)
					{
						$('#tprueba tbody').children().each(function(){
						var dato=$(this).eq(0).children().eq(0).children().eq(0).attr('value');	
						
						if(id==dato)
						{
							ban=false;
						}
										 
						}); 				
					}  
					return ban;				
				}

				function enviar_formulario(){
					
						
							// var nume=$("#detper tbody tr").length;
							// if(nume > 0)
							// {
								document.frm_sentencia.submit();
							// }
							// else
							// {
								// $.prompt('Agregue al menos una seccion',{ opacity: 0.8 });			
							// }
						
					
				}
				
				function cargar_notas()
				{
				    var emp_id=document.frm_sentencia.env_emp_id.options[document.frm_sentencia.env_emp_id.selectedIndex].value;
					var fecha_ini = document.frm_sentencia.env_fecha_ini.value;
					var fecha_fin = document.frm_sentencia.env_fecha_fin.value;
					var	valores="tarea=notas&emp_id="+emp_id+"&fecha_ini="+fecha_ini+"&fecha_fin="+fecha_fin;	
                    if  ((emp_id!='') && (fecha_ini!='') && (fecha_fin!=''))					
					   ejecutar_ajax('ajax.php','notas',valores,'POST');	
					else
                        alert("Seleccione una Empresa, Escoja una fecha Inicial y Final");					
				}
				
				</script>
			<script type="text/javascript" src="js/ajax.js"></script>
			<div id="Contenedor_NuevaSentencia" >
			<form id="frm_sentencia" name="frm_sentencia" action="<?php echo $url;?>" method="POST" enctype="multipart/form-data">  
				<div id="FormSent" style="width:100%;">
				  
						<div class="Subtitulo">Datos</div>
						<div id="ContenedorSeleccion">						   
						
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">*</span>Nombre</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto"  name="env_nombre" id="env_nombre"  maxlength="200" size="40"  value="<?php echo $_POST['env_nombre'];?>">
							   </div>
							</div>
							<!--Fin-->	
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Fecha Creacion</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto"  name="env_fecha_creado" id="env_fecha_creado"  readonly='readonly' maxlength="200" size="12"  value="<?php echo $conversor->get_fecha_latina($_POST['env_fecha_creado']);?>">
							   </div>
							</div>
							<!--Fin-->	
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Fecha Envio</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto"  name="env_fecha_envio" id="env_fecha_envio"  readonly='readonly' maxlength="200" size="12"  value="<?php echo $conversor->get_fecha_latina($_POST['env_fecha_envio']);?>">
							   </div>
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Hora Envio</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto"  name="env_hora_envio" id="env_hora_envio"  readonly='readonly' maxlength="200" size="10"  value="<?php echo $_POST['env_hora_envio'];?>">
							   </div>
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">*</span>Empresa</div>
							   <div id="CajaInput">
							   <select name="env_emp_id" class="caja_texto">
							   <option value="">Seleccione</option>
							   <?php 		
								$fun=NEW FUNCIONES;		
								$fun->combo("select emp_id as id,emp_nombre as nombre from empresa order by emp_nombre asc",$_POST['env_emp_id']);				
								?>
							   </select>
							   </div>										   
							</div>
							<!--Fin-->
                            <!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">*</span>Fecha Noticia Inicio</div>
								 <div id="CajaInput">
									<input readonly="readonly" class="caja_texto" name="env_fecha_ini" id="env_fecha_ini" size="12" value="<?php echo $_POST['env_fecha_ini'];?>" type="text">
										<input name="but_fecha_ini" id="but_fecha_ini" class="boton_fecha" value="..." type="button">
										<script type="text/javascript">
														Calendar.setup({inputField     : "env_fecha_ini"
																		,ifFormat     :     "%Y-%m-%d",
																		button     :    "but_fecha_ini"
																		});
										</script>
								</div>		
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">*</span>Fecha Noticia Fin</div>
								 <div id="CajaInput">
									<input readonly="readonly" class="caja_texto" name="env_fecha_fin" id="env_fecha_fin" size="12" value="<?php echo $_POST['env_fecha_fin'];?>" type="text">
										<input name="but_fecha_fin" id="but_fecha_fin" class="boton_fecha" value="..." type="button">
										<script type="text/javascript">
														Calendar.setup({inputField     : "env_fecha_fin"
																		,ifFormat     :     "%Y-%m-%d",
																		button     :    "but_fecha_fin"
																		});
										</script>
								</div>		
							</div>
							<!--Fin-->
							
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">* </span>emails</div>
							   <div id="CajaInput">
							    <textarea rows="2" cols="50" name="env_emails" id="env_emails"><?php echo $_POST['env_emails'];?></textarea>																	 
							   </div>
							</div>
							<!--Fin-->
													
							<div id="ContenedorDiv" style="width:100%">
								<input type="button" class="boton botonagr" name="" value="Cargar Notas"  onclick="cargar_notas()">
								
							    <br>
								<br>								
								<div id="notas">
								<?
								   if($_GET['tarea']=='MODIFICAR' || $_GET['tarea']=='VER')
								       $this->cargar_notas();
								?>
								</div>
						    </div>
						</div>
						
						
					
						<div id="ContenedorDiv">
						   <div id="CajaBotones">
								<center>
								<?php
								if(!($ver))
								{
									?>
									<input type="button" class="boton" name="" value="Guardar"  onclick="javascript:enviar_formulario()">
									<input type="reset" class="boton" name="" value="Cancelar">
									<input type="button" class="boton" name="" value="Volver" onclick="javascript:location.href='<?php echo $red;?>';">
									<?php
								}
								else
								{
									?>
									<input type="button" class="boton" name="" value="Volver" onclick="javascript:location.href='<?php echo $red;?>';">
									<?php
								}
								?>
								</center>
						   </div>
						</div>
				</div>
			</form>
		</div>
		<?php
	}
	
	function insertar_tcp()
	{
		
		$conec= new ADO();
	
		$sql="insert into envio (
		env_nombre,
		env_fecha_creado, 		
		env_fecha_ini,
		env_fecha_fin,
		env_emp_id,
		env_estado,
		env_emails		
		) 
		values (
		'".$_POST['env_nombre']."',
		'".date('Y-m-d')."',
		'".$_POST['env_fecha_ini']."',
		'".$_POST['env_fecha_fin']."',
		'".$_POST['env_emp_id']."',
		'Pendiente',
		'".$_POST['env_emails']."'		
		)";
	
		$conec->ejecutar($sql,false);
		
		$llave=mysql_insert_id();
		
		
		$i=0;
		
		foreach($_POST['noticias'] as $not_id)
		{
			
			$sql="insert into envio_noticias(eno_env_id,eno_not_id) values 
							('".$llave."','".$not_id."')";

			$conec->ejecutar($sql);
			$i++;
		}
		
		$mensaje='Envio Agregado Correctamente.';
		
		$tipo='Correcto';
		
		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER'.'�'.$this->link.'?mod='.$this->modulo.'&tarea=AGREGAR','Ir al listado�Continuar Agregando',$tipo);
	}
	
	function modificar_tcp()
	{
		
	
			$conec= new ADO();
		
			$codigo="";
						
			
			$sql="update envio set 
									env_nombre='".$_POST['env_nombre']."',
									env_fecha_ini='".$_POST['env_fecha_ini']."',
									env_fecha_fin='".$_POST['env_fecha_fin']."',
									env_emp_id='".$_POST['env_emp_id']."',
									env_emails='".$_POST['env_emails']."'																
								where env_id='".$_GET['id']."'";
			
			$conec->ejecutar($sql);
			
			$sql="delete from envio_noticias  where eno_env_id='".$_GET['id']."'";

			$conec->ejecutar($sql);
			
			
			
			$i=0;
		
			foreach($_POST['noticias'] as $not_id)
			{
				
				$sql="insert into envio_noticias(eno_env_id,eno_not_id) values 
								('".$_GET['id']."','".$not_id."')";

				$conec->ejecutar($sql);
				$i++;
			}
			
			
			
			
			$mensaje='Envio modificado Correctamente';
			
			$tipo='correcto';
		
			$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER','',$tipo);
	}
	
	function eliminar_tcp()
	{
		$conec= new ADO();
		
		$sql="delete FROM envio where env_id='".$_GET['id']."'";

		$conec->ejecutar($sql);
		
		$sql="delete from envio_noticias where eno_env_id='".$_GET['id']."'";
		 
		$conec->ejecutar($sql);	
		
		$mensaje='Envio Eliminado Correctamente.';
		
		$tipo='Correcto';
		
		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER','',$tipo);
	}
	
	
	function obtener_categorias($conec)
	{	    	
		//Aqui obtengo todas las palabras claves
		$sql = "SELECT cat_nombre,eca_tags 
		        FROM empresa_categoria INNER JOIN categoria ON (eca_cat_id = cat_id)
				WHERE eca_emp_id =".$_POST["env_emp_id"]; 
		
		$conec->ejecutar($sql);
		$num=$conec->get_num_registros();
		$categorias = array();
		
		for($i=0;$i<$num;$i++)
		{		 
		    $aux = array();
		    $objeto  = $conec->get_objeto();
            $aux['nombre'] = $objeto->cat_nombre;
            $aux['tags']   = explode(",",$objeto->eca_tags);            
     		$categorias[]  = $aux;	
			$conec->siguiente();
		}	
  	   
		return	$categorias;   	
	}
	
	function verificar_nota(&$cat,$nota)
	{
	  	foreach($cat as $key=>$categoria)
		{
		    if (is_array($categoria['tags']) && (count($categoria['tags']) > 0))
			{
			    /*verifica si alguna palabra clave se encuentra en la nota*/
				
			    foreach($categoria['tags'] as $palabra)
				{
				    //echo $palabra."<br>";
					if (substr_count($nota['notacompleta'],$palabra) > 0)
				    {                     
					   $cat[$key]['notas'][] = $nota;					   
					   break;
					}	   
				}	
                /*-----------------------------------------------------------------*/				
			}		
		}	
	}
	
	function obtener_notas_envio ($conec)
	{
	   //Aqui obtengo todas las palabras claves
		$sql = "SELECT eno_not_id 
		        FROM envio_noticias 
				WHERE eno_env_id =".$_GET["id"]; 
		
		$conec->ejecutar($sql);
		$num=$conec->get_num_registros();
		$noticias = array();
		
		for($i=0;$i<$num;$i++)
		{			   
		    $objeto  = $conec->get_objeto();                      
     		$noticias[]  = $objeto->eno_not_id;	
			$conec->siguiente();
		}	
  	   
		return	$noticias;   	
	}
	
	 function obtener_palabras_claves($conec)
	{	    	
		//Aqui obtengo todas las palabras claves
		$sql = "SELECT cat_nombre,eca_tags 
		        FROM empresa_categoria INNER JOIN categoria ON (eca_cat_id = cat_id)
				WHERE eca_emp_id =".$_POST["env_emp_id"]; 
		
		$conec->ejecutar($sql);
		$num=$conec->get_num_registros();
		$palabras = array();
		
		for($i=0;$i<$num;$i++)
		{				    
		    $objeto  = $conec->get_objeto();  
			$aux  = explode(",",$objeto->eca_tags);
			$palabras = $palabras + $aux;
            //array_push($palabras,$aux);	   		
			$conec->siguiente();
		}	  	   
		return	$palabras;   	
	}
	
	function existe_nota($noticias,$not_id)
	{
	    $sw = false;
	    foreach($noticias as $nota)
		{
		    if ($nota == $not_id)
			{
			    $sw = true;
				break;
			}    
		}
		return $sw;
	}
	
	function resaltar($claves, $texto) { 		
		$clave = array_unique($claves);
		$num = count($clave); 
		for($i=0; $i < $num; $i++) 
			$texto = preg_replace("/(".trim($clave[$i]).")/i","<span style='background-color:#FFF29B'>\\1</span>",$texto);
		return $texto; 
    }
	
	function cargar_notas()
    {
	    $conec= new ADO();	
		$conec1= new ADO();	
		$conversor = new convertir();	
	
	    //Aqui obtengo todas las palabras claves
	    $categorias = $this->obtener_categorias($conec);		
		$noticias_envio =  $this->obtener_notas_envio($conec);	
       	$palabras = $this->obtener_palabras_claves($conec);
		
		$sql = "SELECT emp_banner,emp_titulares,emp_resumen,emp_notacompleta FROM empresa WHERE emp_id =".$_POST["env_emp_id"];			
		$conec->ejecutar($sql);								
		$empresa = $conec->get_objeto();        
		$sql = "SELECT distinct prd_id,prd_nombre,not_id,not_fecha,not_titulo,not_resumen,not_link,not_notacompleta,sec_nombre
				FROM empresa_periodico INNER JOIN periodico ON (epe_prd_id = prd_id and prd_estado = 'Habilitado') 
									   INNER JOIN noticias ON (not_prd_id = prd_id) 									  
                                       INNER JOIN seccion  ON (not_sec_id = sec_id)									   
				WHERE  epe_emp_id =".$_POST["env_emp_id"]." and not_fecha between '".$_POST["env_fecha_ini"]."' and '".$_POST["env_fecha_fin"]."'"; 	
		?>		
        <table   width="81%"  class="tablaReporte" cellpadding="0" cellspacing="5">
					<?php
						foreach($categorias as $key=>$cat)
						{
							if (is_array($cat['tags']) && (count($cat['tags']) > 0))
							{
							?>
								<thead>
								<tr>
									<th valign="MIDDLE" style="text-align:left; padding-left:10px">
										<b><? echo $cat['nombre'];?></b>
									</th>					
								</tr>
								</thead>
								<tbody>
							
							<?php
							
								$tags = implode("%' OR not_notacompleta LIKE '%",$cat['tags']);			
								$sqlaux = $sql." and (not_notacompleta LIKE '%".$tags."%')";								
								$conec->ejecutar($sqlaux);						
								$num=$conec->get_num_registros();
								for($i=0;$i<$num;$i++)
								{
									$objeto=$conec->get_objeto();
									$checked = '';
								    if ( $this->existe_nota($noticias_envio,$objeto->not_id) )
									    $checked = 'checked="checked"';
									
									?>
										<tr>
											
											<td style="text-align:left; padding-left:10px">
												<table border="0" cellpadding="5" cellspacing="0">
												<tr>
													<td width="20%"><input type="checkbox" name='noticias[]' value="<? echo $objeto->not_id;?>" <? echo  $checked; ?> ><? echo "<b>".utf8_encode($objeto->prd_nombre).":</b>"; ?></td>
													<td width="70%"><? echo "<h3><a href='".$objeto->not_link."' target='_blank'>".utf8_decode($objeto->not_titulo)."</a><h3>"; ?></td>
													<td width="10%"><? echo $conversor->get_fecha_latina($objeto->not_fecha); ?></td>		
												</tr>
												<tr>
													
													<td colspan="3" align="justify">
														<?  //if ($empresa->emp_resumen==1)  
														     //  echo $objeto->not_resumen; 
															//else
															   echo $this->resaltar($palabras,utf8_decode($objeto->not_notacompleta));  
														?>
													<td>
												</tr>
												<tr>
													<td>&nbsp;</td>
													<td colspan="2"><a href="<? echo $objeto->not_link; ?>" target="_blank"><? echo $objeto->not_link; ?></a><td>											
												</tr>
												</table>
											</td>					
										</tr>
									<?						
									$conec->siguiente();
								}
								?>
								</tbody>
								<?						
							}		
						}
					?> 		
					</table>
		<?
	}
	
	function enviar_mail()
	{
		
		if(trim($_POST['mail'])<>"")
		{
			
			require_once('clases/phpmailer/class.phpmailer.php');			
			
			$this->obtener_datos($server,$usu,$pass);			
			
			$cue_nombre=$usu;
				
			$cue_pass=$pass;
				
			$mail = new PHPMailer(true);
			
			$mail->SetLanguage('es');

			$mail->IsSMTP();

			$mail->SMTPAuth   = true;                              

			$mail->Host       = $server;      

			//$mail->SMTPDebug  = 2;   
			
			$mail->Timeout=30;

			$mail->CharSet = 'utf-8';
			
			
			$conec= new ADO();		
			$sql="select env_nombre,env_emails,env_emp_id from envio where env_id='".$_GET['id']."'";	
           
			$conec->ejecutar($sql);			
			$num=$conec->get_num_registros();

			if($num > 0)
			{
								
				$objeto=$conec->get_objeto();
								
				$mail->Subject = utf8_encode($objeto->env_nombre);
				
				$body =  $this->cargar_notas_mail();
				
				
					$mail->MsgHTML($body);
					
				$correos = explode(";",$objeto->env_emails);
				
				foreach($correos as  $correo)
				{
				   $mail->AddAddress(trim($correo));
				}			
					  
				$mail->SetFrom($cue_nombre, utf8_encode($objeto->env_nombre));		
						
				$mail->Username   = $cue_nombre;  

				$mail->Password   = $cue_pass;	

				try {
				
					$mail->Send(); 	
                    $sql = "UPDATE envio SET env_fecha_envio = '".date('Y-m-d')."',
                                             env_hora_envio  ='".date('h:i:s')."',
											 env_estado      = 'Enviado'";
                    //$sql = "SELECT emp_banner,emp_titulares,emp_resumen,emp_notacompleta FROM empresa WHERE emp_id =".$_POST["env_emp_id"];			
					$conec->ejecutar($sql);							

				} catch (phpmailerException $e) {

						// $sql="insert into errores(err_mensaje,err_fecha,err_hora) values('".$e->errorMessage()."','".date('Y:m:d')."','".date('H:i:s')."')";

						// $conec->ejecutar($sql);

				} catch (Exception $e) {

						// $sql="insert into errores(err_mensaje,err_fecha,err_hora) values('".$e->getMessage()."','".date('Y:m:d')."','".date('H:i:s')."')";

						// $conec->ejecutar($sql);
				}
			}
			else
			{
				echo '<center>El boletin es invalido.</center>';
			}
			
			$this->dibujar_busqueda();
			
		}
		else
		{
				 $this->formulario->dibujar_tarea();
			?>
			<table width="100%" cellpadding="0" cellspacing="1" style="border:1px solid #CCCCCC;">
				<tr>
					<td align="center" height="150">
								<h4>Esta Seguro que desea Enviar las Noticias?</h4>
								<form id="form_eliminacion" action="gestor.php?mod=envio&tarea=ENVIAR_MAIL&id=<?php echo $_GET['id'];?>" method="POST" enctype="multipart/form-data">
									<input type="hidden" value="envio" name="mail" class="caja_texto" size="50"><br><br>
									<input type="submit" value="Continuar" class="boton"> 
									<input type="button" value="Volver" class="boton" onclick="javascript:location.href='<?php echo $this->link.'?mod='.$this->modulo.'&tarea=ACCEDER';?>';">
								</form>
					</td>
				</tr>
			</table>
			</div>
		<?php
		}
	}
	
	function cargar_notas_mail()
	{
	    $conec= new ADO();	
		$conec1= new ADO();	
		$conversor = new convertir();	

        $base_url = "http://localhost/automata/imagenes/";		
		
		//Aqui obtengo todas las palabras claves
	    $categorias = $this->obtener_categorias($conec);
		
		$sql = "SELECT emp_banner,emp_titulares,emp_resumen,emp_notacompleta FROM empresa WHERE emp_id =".$_POST["env_emp_id"];			
		$conec->ejecutar($sql);						
		$empresa = $conec->get_objeto();
        
		$sql = "SELECT distinct prd_id,prd_nombre,not_fecha,not_titulo,not_resumen,not_link,not_notacompleta,sec_nombre
				FROM empresa_periodico INNER JOIN periodico ON (epe_prd_id = prd_id and prd_estado = 'Habilitado') 
									   INNER JOIN noticias ON (not_prd_id = prd_id) 
									   INNER JOIN envio_noticias ON (eno_not_id = not_id)
                                       INNER JOIN seccion  ON (not_sec_id = sec_id)									   
				WHERE  epe_emp_id =".$_POST["env_emp_id"]." and eno_env_id = ".$_GET['id']; 	

        //echo $sql;
		//exit;
		
		$myday = setear_fecha(strtotime(date('Y-m-d')));
$cadena = '
			<div id="contenido_reporte" style="clear:both;";>
			<center>
			
			<table style="font-size:12px;" width="766" cellpadding="5" cellspacing="0" >';
			
			if ($empresa->emp_banner!="")
			{
				$cadena .= '<tr>				    
				  <td colspan="2" align="center"><img src="imagenes/banner/'.$empresa->emp_banner.'"></td> 
				</tr>';
			}	
			$cadena.='	<tr>
					 <td align="center" colspan="2"><strong><h3>PANORAMA '.$conversor->get_fecha_latina(date("Y-m-d")).'</h3></strong></td>				    
				</tr>				 
			</table>						
			<table style="font-size:12px;" width="80%" cellpadding="5" cellspacing="3" >			
				<tr>
					<td colspan="2" align="right">
						<strong>Generado el: </strong> '.$myday.'<br><br>
					</td>
					
				</tr>				 
			</table>';
			
			
			   if ($empresa->emp_titulares == 1)
			   {			
				$cadena .='	
			
			      <strong><h3>TITULARES</h3></strong>
					<table   width="766"  class="tablaReporte" cellpadding="0" cellspacing="0" style="border:1px solid #8ec2ea;"> ';
					
					
						foreach($categorias as $key=>$cat)
						{
							if (is_array($cat['tags']) && (count($cat['tags']) > 0))
							{
							$cadena .='	
								<thead >
								<tr style="background-color:#0059A7;">
									<th valign="MIDDLE" style="text-align:left; padding:5px 5px 5px 5px; color:#FFF;	font-size:13px;	 border-right:1px solid #0059A7;" colspan="3">
										<b>'.$cat["nombre"].'</b>
									</th>					
								</tr>
								</thead>
								<tbody>';
							
						
							
								$tags = implode("%' OR not_notacompleta LIKE '%",$cat['tags']);			
								$sqlaux = $sql." and (not_notacompleta LIKE '%".$tags."%')";
								
								$conec->ejecutar($sqlaux);						
								$num=$conec->get_num_registros();
								for($i=0;$i<$num;$i++)
								{
									$objeto=$conec->get_objeto();
										$cadena .='
										<tr>
										    <td style="font-size:11px;	border-bottom:1px dashed #d6d8d7;	border-right:1px solid #cccccc;	padding:0px 0px 0px 5px;"><b>'.$objeto->prd_nombre.':</b></td>
											<td style="font-size:13px;	border-bottom:1px dashed #d6d8d7;	border-right:1px solid #cccccc; text-align:left; padding-left:10px">
											
												   <a href="'.$objeto->not_link.'" target="_blank">'.$objeto->not_titulo.'</a>
																				
											</td>
											<td style="font-size:11px;	border-bottom:1px dashed #d6d8d7;	border-right:1px solid #cccccc;	padding:0px 0px 0px 5px;">'.$conversor->get_fecha_latina($objeto->not_fecha).'</td>		
										</tr>';
														
									$conec->siguiente();
								}
								
								$cadena .='</tbody>';
													
							}		
						}				
					
					$cadena .='</table>';
			
			   }			
			
			   if (($empresa->emp_resumen==1) || ($empresa->emp_notacompleta==1))
			   {
					
					$cadena .='<strong><h3>NOTAS COMPLETAS</h3></strong>
					<table   width="766"  class="tablaReporte" cellpadding="0" cellspacing="0" style="border:1px solid #8ec2ea;">';
					
						foreach($categorias as $key=>$cat)
						{
							if (is_array($cat['tags']) && (count($cat['tags']) > 0))
							{
							$cadena .='
								<thead >
								<tr style="height:30px;	background-color:#0059A7">
									<th valign="MIDDLE" style="text-align:left; padding:5px 5px 5px 5px; color:#FFF;	font-size:13px;	 border-right:1px solid #0059A7;">
										<b>'.$cat['nombre'].'</b>
									</th>					
								</tr>
								</thead>
								<tbody>';
							
					
							
								$tags = implode("%' OR not_notacompleta LIKE '%",$cat['tags']);			
								$sqlaux = $sql." and (not_notacompleta LIKE '%".$tags."%')";
								$conec->ejecutar($sqlaux);						
								$num=$conec->get_num_registros();
								for($i=0;$i<$num;$i++)
								{
									$objeto=$conec->get_objeto();
									$cadena .='
										<tr>
											
											<td style="text-align:left; padding-left:10px font-size:11px;	border-bottom:1px dashed #d6d8d7;	border-right:1px solid #cccccc;	">
												<table border="0" cellpadding="5" cellspacing="0" width="100%">
												<tr>
													<td width="10%" style="font-size:11px;	 text-align:left; padding-left:10px"><b>'.$objeto->prd_nombre.':</b></td>
													<td width="80%" style="font-size:13px;	 text-align:left; padding-left:10px"><b>'.$objeto->not_titulo.'</b></td>
													<td width="10%" style="font-size:11px;	 text-align:left; padding-left:10px">'.$conversor->get_fecha_latina($objeto->not_fecha).'</td>		
												</tr>
												<tr>
													
													<td colspan="3" align="justify" style="font-size:12px;	 text-align:justify; padding-left:10px">';
												    
														if ($empresa->emp_resumen==1)  
														       $cadena .= $objeto->not_resumen; 
															else
															   $cadena .= $objeto->not_notacompleta; 
												
												$cadena .='	</td>
												</tr>
												<tr>
													<td style="text-align:right;font-size:11px;"><b>Link:</b></td>
													<td colspan="2" style="font-size:11px;	 text-align:left; padding-left:10px"><a href="'.$objeto->not_link.'" target="_blank">'.$objeto->not_link.'</a><td>											
												</tr>
												</table>
											</td>					
										</tr>';
													
									$conec->siguiente();
								}
							
								$cadena .='</tbody>';
													
							}		
						}
						
					$cadena .='</table>';
		
			
				}
			
		
		$cadena .='</center>
		</div><br>';
	
		
	return $cadena;
	}
	
	function obtener_datos(&$server,&$usu,&$pass)
	{
		$conec= new ADO();
		
		$sql="select 
				   par_smtp,par_salida,par_pas_salida
			  from 
			     ad_parametro		    
		";
		
		$conec->ejecutar($sql);
		
		$objeto=$conec->get_objeto();
		
		$server=$objeto->par_smtp;
		
		$usu=$objeto->par_salida;
		
		$pass=$objeto->par_pas_salida;
		
	}
	
}
?>