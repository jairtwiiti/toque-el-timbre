<?php

class NOTICIA extends BUSQUEDA 
{
	var $formulario;
	var $mensaje;
	
	function NOTICIA()
	{
		//permisos
		$this->ele_id=133;
		
		$this->busqueda();
		
		if(!($this->verificar_permisos('AGREGAR')))
		{
			$this->ban_agregar=false;
		}
		//fin permisos
		
		$this->num_registros=50;
		
		$this->coneccion= new ADO();
		
		$this->arreglo_campos[0]["nombre"]="not_titulo";
		$this->arreglo_campos[0]["texto"]="Titulo";
		$this->arreglo_campos[0]["tipo"]="cadena";
		$this->arreglo_campos[0]["tamanio"]=50;
		
		$this->arreglo_campos[1]["nombre"]="not_fecha";
		$this->arreglo_campos[1]["texto"]="Fecha";
		$this->arreglo_campos[1]["tipo"]="fecha";
		$this->arreglo_campos[1]["tamanio"]=12;
		
		$this->arreglo_campos[2]["nombre"]="not_prd_id";
		$this->arreglo_campos[2]["texto"]="Periodico";
		$this->arreglo_campos[2]["tipo"]="combosql";
		$this->arreglo_campos[2]["sql"]="select prd_id as codigo,prd_nombre as descripcion from periodico";
		
		$this->arreglo_campos[3]["nombre"]="not_sec_id";
		$this->arreglo_campos[3]["texto"]="Seccion";
		$this->arreglo_campos[3]["tipo"]="combosql";
		$this->arreglo_campos[3]["sql"]="select sec_id as codigo,sec_nombre as descripcion from seccion";
		
		$this->link='gestor.php';
		
		$this->modulo='noticia';
		
		$this->formulario = new FORMULARIO();
		
		$this->formulario->set_titulo('NOTICIAS');
		
		
	}
	
	
	function dibujar_busqueda()
	{
		
		$this->formulario->dibujar_cabecera();
		
		$this->dibujar_listado();
	}
	
		
	function set_opciones()
	{
				
		$nun=0;
		
		if($this->verificar_permisos('VER'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='VER';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_search.png';
			$this->arreglo_opciones[$nun]["nombre"]='VER';
			$nun++;
		}
		
		if($this->verificar_permisos('MODIFICAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='MODIFICAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_edit.png';
			$this->arreglo_opciones[$nun]["nombre"]='MODIFICAR';
			$nun++;
		}
		
		if($this->verificar_permisos('ELIMINAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='ELIMINAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_drop.png';
			$this->arreglo_opciones[$nun]["nombre"]='ELIMINAR';
			$nun++;
		}
	}
	
	function dibujar_listado()
	{
		$sql="SELECT not_id,not_fecha,not_titulo,not_link,prd_nombre,sec_nombre 
		      FROM noticias inner join periodico on (not_prd_id=prd_id)
			                inner join seccion on (not_sec_id = sec_id)";
		
		$this->set_sql($sql,' order by not_id desc ');
		
		$this->set_opciones();
		
		$this->dibujar();
		
	}
	
	function orden($tecnologia,$accion,$ant_orden)
	{
		$conec= new ADO();
		
		if($accion=='b')
			$cad=" where are_orden > $ant_orden order by are_orden asc";
		else
			$cad=" where are_orden < $ant_orden order by are_orden desc";

		$consulta = "
		select 
			are_id,are_orden 
		from 
			area
		$cad
		limit 0,1
		";	

		$conec->ejecutar($consulta);

		$num = $conec->get_num_registros();   

		if($num > 0)
		{
			$objeto=$conec->get_objeto();
			
			$nu_orden=$objeto->are_orden;
			
			$id=$objeto->are_id;
			
			$consulta = "update area set are_orden='$nu_orden' where are_id='$tecnologia'";	

			$conec->ejecutar($consulta);
			
			$consulta = "update area set are_orden='$ant_orden' where are_id='$id'";	

			$conec->ejecutar($consulta);
		}	
	}
	
	function dibujar_encabezado()
	{
		?>
			<tr>
	        	<th>Titulo</th>
				<th>Fecha</th>
				<th>Periodico</th>
				<th>Seccion</th>					
	            <th class="tOpciones" width="100px">Opciones</th>
			</tr>
			
		<?PHP
	}
	
	function mostrar_busqueda()
	{
		$conversor = new convertir();
		
		for($i=0;$i<$this->numero;$i++)
			{
				
				$objeto=$this->coneccion->get_objeto();
				echo '<tr>';
									
					echo "<td>";
						echo utf8_decode($objeto->not_titulo);
					echo "&nbsp;</td>";
					echo "<td>";
						echo $conversor->get_fecha_latina($objeto->not_fecha);
					echo "&nbsp;</td>";
					echo "<td>";
						echo $objeto->prd_nombre;
					echo "&nbsp;</td>";
					echo "<td>";
						echo $objeto->sec_nombre;
					echo "&nbsp;</td>";
					
					echo "<td>";
						echo $this->get_opciones($objeto->not_id);
					echo "</td>";
				echo "</tr>";
				
				$this->coneccion->siguiente();
			}
	}
	
	function cargar_datos()
	{
		$conec=new ADO();
		
		$sql="select * from noticias
				where not_id = '".$_GET['id']."'";
		
		$conec->ejecutar($sql);
		
		$objeto=$conec->get_objeto();
		 	 	 	 	 	 	 	 	 
		$_POST['not_titulo']=$objeto->not_titulo;		
		$_POST['not_fecha']=$objeto->not_fecha;		
		$_POST['not_link']=$objeto->not_link;		
		$_POST['not_prd_id']=$objeto->not_prd_id;		
		$_POST['not_sec_id']=$objeto->not_sec_id;
		$_POST['not_resumen']=$objeto->not_resumen;		
		$_POST['not_notacompleta']=$objeto->not_notacompleta;
		
	}
	
	function datos()
	{
		if($_POST)
		{
			//texto,  numero,  real,  fecha,  mail.
			$num=0;
			$valores[$num]["etiqueta"]="Titulo";
			$valores[$num]["valor"]=$_POST['not_titulo'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			$valores[$num]["etiqueta"]="Fecha";
			$valores[$num]["valor"]=$_POST['not_fecha'];
			$valores[$num]["tipo"]="fecha";
			$valores[$num]["requerido"]=true;
			$num++;
			$valores[$num]["etiqueta"]="Link";
			$valores[$num]["valor"]=$_POST['not_link'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			$valores[$num]["etiqueta"]="Resumen";
			$valores[$num]["valor"]=$_POST['not_resumen'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			$valores[$num]["etiqueta"]="Notacompleta";
			$valores[$num]["valor"]=$_POST['not_notacompleta'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			$valores[$num]["etiqueta"]="Periodico";
			$valores[$num]["valor"]=$_POST['not_prd_id'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			$valores[$num]["etiqueta"]="Seccion";
			$valores[$num]["valor"]=$_POST['not_sec_id'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			
			$val=NEW VALIDADOR;
			
			$this->mensaje="";
			
			if($val->validar($valores))
			{
				return true;
			}
				
			else
			{
				$this->mensaje=$val->mensaje;
				return false;
			}
		}
			return false;
	}
	
	function formulario_tcp($tipo)
	{
	   //include_once("js/fckeditor/fckeditor.php");
				switch ($tipo)
				{
					case 'ver':{
								$ver=true;
								break;
								}
							
					case 'cargar':{
								$cargar=true;
								break;
								}
				}
				
				$url=$this->link.'?mod='.$this->modulo;
				
				$red=$url.'&tarea=ACCEDER';
				
				if(!($ver))
				{
					$url.="&tarea=".$_GET['tarea'];
				}
				
				if($cargar)
				{
					$url.='&id='.$_GET['id'];
				}

		
		    $this->formulario->dibujar_tarea('NOTICIAS');
		
			if($this->mensaje<>"")
			{
				$this->formulario->mensaje('Error',$this->mensaje);
			}
			?>
			
			<script type="text/javascript" src="js/ajax.js"></script>
			<script>
			function cargar_seccion(id)
			{
				
				var	valores="tarea=seccion&sec="+id;			

				ejecutar_ajax('ajax.php','seccion',valores,'POST');									

			}
			</script>
			
		<div id="Contenedor_NuevaSentencia">
			<form id="frm_sentencia" name="frm_sentencia" action="<?php echo $url;?>" method="POST" enctype="multipart/form-data">  
				<div id="FormSent" style="width:85%;">
				  
					<div class="Subtitulo">Datos</div>
						<div id="ContenedorSeleccion">
						
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">* </span>Titulo</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto" name="not_titulo" id="not_titulo" maxlength="250"  size="100" value="<?php echo utf8_decode($_POST['not_titulo']);?>">
							   </div>
							</div>
							<!--Fin-->
							
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">* </span>Fecha</div>
								 <div id="CajaInput">
									<input readonly="readonly" class="caja_texto" name="not_fecha" id="not_fecha" size="12" value="<?php echo $_POST['not_fecha'];?>" type="text">
										<input name="but_fecha" id="but_fecha" class="boton_fecha" value="..." type="button">
										<script type="text/javascript">
														Calendar.setup({inputField     : "not_fecha"
																		,ifFormat     :     "%Y-%m-%d",
																		button     :    "but_fecha"
																		});
										</script>
								</div>		
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">* </span>Link</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto" name="not_link" id="not_link" maxlength="250"  size="100" value="<?php echo $_POST['not_link'];?>">
							   </div>
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">* </span>Periodico</div>
							   <div id="CajaInput">
							   <select name="not_prd_id" class="caja_texto"  onchange="cargar_seccion(this.value);">
							   <option value="">Seleccione</option>
							   <?php 		
								$fun=NEW FUNCIONES;		
								$fun->combo("select prd_id as id,prd_nombre as nombre from periodico",$_POST['not_prd_id']);				
								?>
							   </select>
							   </div>
							</div>
							<!--Fin-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">* </span>Seccion</div>
							   <div id="CajaInput">
							   <div id="seccion">
							   <select name="not_sec_id" class="caja_texto">
							   <option value="">Seleccione</option>
							   <?php 		
								if($_POST['not_prd_id']<>"")
								{
									$fun=NEW FUNCIONES;		
									$fun->combo("select sec_id as id,sec_nombre as nombre from seccion inner join periodico_seccion on (sec_id=pes_sec_id) where pes_prd_id='".$_POST['not_prd_id']."' ",$_POST['not_sec_id']);				
								}
								?>
							   </select>
							   </div>
							   </div>
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Resumen</div>
							   <div id="CajaInput">
							   <textarea class="area_texto" name="not_resumen" id="not_resumen" cols="75" rows="7"><?php echo utf8_decode($_POST['not_resumen'])?></textarea>
							   </div>
							</div>
							<!--Fin-->
						<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">* </span>Nota Completa</div>
							   <div id="CajaInput">
							      <textarea class="area_texto" name="not_notacompleta" id="not_notacompleta" cols="75" rows="35"><?php echo utf8_decode($_POST['not_notacompleta']);?></textarea>
							   <?php
						// $pagina=$_POST['are_descripcion'];
						// $oFCKeditor = new FCKeditor('are_descripcion') ;
						// $oFCKeditor->BasePath = 'js/fckeditor/';
						// $oFCKeditor->Width  = '550' ;
						// $oFCKeditor->Height = '400' ;
						
						// $oFCKeditor->Value = <<<EOF
// $pagina
// EOF;
						// $oFCKeditor->Create() ;
						?>		
							   </div>
							</div>
							<!--Fin-->
						</div>
					
						<div id="ContenedorDiv">
						   <div id="CajaBotones">
								<center>
								<?php
								if(!($ver))
								{
									?>
									<input type="submit" class="boton" name="" value="Guardar">
									<input type="reset" class="boton" name="" value="Cancelar">
									<input type="button" class="boton" name="" value="Volver" onclick="javascript:location.href='<?php echo $red;?>';">
									<?php
								}
								else
								{
									?>
									<input type="button" class="boton" name="" value="Volver" onclick="javascript:location.href='<?php echo $red;?>';">
									<?php
								}
								?>
								</center>
						   </div>
						</div>
				</div>
			</form>
		</div>
		<?php
	}
	
	function insertar_tcp()
	{
		$conec= new ADO();
		
		
		$sql="insert into noticias (not_titulo,not_fecha,not_link,not_resumen,not_notacompleta,not_prd_id,not_sec_id) values 
							('".$_POST['not_titulo']."','".$_POST['not_fecha']."','".$_POST['not_link']."','".$_POST['not_resumen']."','".$_POST['not_notacompleta']."','".$_POST['not_prd_id']."','".$_POST['not_sec_id']."')";

		$conec->ejecutar($sql);
		//echo $sql;
		$mensaje='Noticia Agregada Correctamente!!!';

		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
		
	}
	
	
	
	function modificar_tcp()
	{
		$conec= new ADO();
		
		
			
		$sql="update noticias set 
						not_titulo='".$_POST['not_titulo']."',
						not_fecha='".$_POST['not_fecha']."',
						not_link='".$_POST['not_link']."',
						not_resumen='".$_POST['not_resumen']."',
						not_notacompleta='".$_POST['not_notacompleta']."',
						not_prd_id='".$_POST['not_prd_id']."',
						not_sec_id='".$_POST['not_sec_id']."'
						where not_id = '".$_GET['id']."'";

		$conec->ejecutar($sql);
		//echo $sql;
		$mensaje='Noticia Modificada Correctamente!!!';

		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
		
	}
	
	function formulario_confirmar_eliminacion()
	{
		
		$mensaje='Esta seguro de eliminar la Noticia?';
		
		$this->formulario->ventana_confirmacion($mensaje,$this->link."?mod=$this->modulo".'&tarea=ELIMINAR','not_id');
	}
	
	function eliminar_tcp()
	{
		
			$llave=$_POST['not_id'];			
			
			$conec= new ADO();
			
			$sql="delete from noticias where not_id='".$_POST['not_id']."'";
			
			$conec->ejecutar($sql);
			
			$mensaje='Noticia Eliminada Correctamente!!!';
			
		
		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
	}
	
}
?>