<?php
	
	require_once('proveedor.class.php');
	
	$proveedor = new PROVEEDOR();
	
	if(!($proveedor->verificar_permisos($_GET['tarea'])))
	{
		?>
		<script>
			location.href="log_out.php";
		</script>
		<?php
	}
	
	switch ($_GET['tarea'])
	{
		case 'AGREGAR':{
						
							if($proveedor->datos())
							{
								$proveedor->insertar_tcp();
							}
							else 
							{
								$proveedor->formulario_tcp('blanco');
							}
					
						
						
						break;}
		case 'VER':{
						$proveedor->cargar_datos();
												
						$proveedor->formulario_tcp('ver');
						
						break;}
		case 'MODIFICAR':{
							if($_GET['acc']=='Imagen')
							{
								$proveedor->eliminar_imagen();
							}
							else
							{
								if($proveedor->datos())
								{
									$proveedor->modificar_tcp();
								}
								else 
								{
									if(!($_POST))
									{
										$proveedor->cargar_datos();
									}
									$proveedor->formulario_tcp('cargar');
								}
							}
						break;}
		case 'ELIMINAR':{
							
								if(isset($_POST['prv_id']))
								{
									if(trim($_POST['prv_id'])<>"")
									{
										$proveedor->eliminar_tcp();
									}
									else 
									{
										$proveedor->dibujar_busqueda();
									}
								}
								else 
								{
									$proveedor->formulario_confirmar_eliminacion();
								}
							
							
						break;}
		case 'ACCEDER':{	
						
						if($_GET['acc']<>"")
						{
							$proveedor->orden($_GET['tec'],$_GET['acc'],$_GET['or']);
						}
						$proveedor->dibujar_busqueda();
						break;
						}
		
	}
		
?>