<?php

class PROVEEDOR extends BUSQUEDA 
{
	var $formulario;
	var $mensaje;
	
	function PROVEEDOR()
	{
		//permisos
		$this->ele_id=137;
		
		$this->busqueda();
		
		if(!($this->verificar_permisos('AGREGAR')))
		{
			$this->ban_agregar=false;
		}
		//fin permisos
		
		$this->num_registros=14;
		
		$this->coneccion= new ADO();
		
		$this->arreglo_campos[0]["nombre"]="prv_nombre";
		$this->arreglo_campos[0]["texto"]="Nombre";
		$this->arreglo_campos[0]["tipo"]="cadena";
		$this->arreglo_campos[0]["tamanio"]=40;
		
		$this->link='gestor.php';
		
		$this->modulo='proveedor';
		
		$this->formulario = new FORMULARIO();
		
		$this->formulario->set_titulo('PROVEEDORES');
		
		
	}
	
	
	function dibujar_busqueda()
	{
		
		$this->formulario->dibujar_cabecera();
		
		$this->dibujar_listado();
	}
	
		
	function set_opciones()
	{
				
		$nun=0;
		
		if($this->verificar_permisos('VER'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='VER';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_search.png';
			$this->arreglo_opciones[$nun]["nombre"]='VER';
			$nun++;
		}
		
		if($this->verificar_permisos('MODIFICAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='MODIFICAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_edit.png';
			$this->arreglo_opciones[$nun]["nombre"]='MODIFICAR';
			$nun++;
		}
		
		if($this->verificar_permisos('ELIMINAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='ELIMINAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_drop.png';
			$this->arreglo_opciones[$nun]["nombre"]='ELIMINAR';
			$nun++;
		}
	}
	
	function dibujar_listado()
	{
		$sql="SELECT * FROM proveedor";
		
		$this->set_sql($sql,' order by prv_orden desc ');
		
		$this->set_opciones();
		
		$this->dibujar();
		
	}
	
	function orden($tecnologia,$accion,$ant_orden)
	{
		$conec= new ADO();
		
		if($accion=='s')
			$cad=" where prv_orden > $ant_orden order by prv_orden asc";
		else
			$cad=" where prv_orden < $ant_orden order by prv_orden desc";

		$consulta = "
		select 
			prv_id,prv_orden 
		from 
			proveedor
		$cad
		limit 0,1
		";	

		$conec->ejecutar($consulta);

		$num = $conec->get_num_registros();   

		if($num > 0)
		{
			$objeto=$conec->get_objeto();
			
			$nu_orden=$objeto->tec_orden;
			
			$id=$objeto->tec_id;
			
			$consulta = "update proveedor set prv_orden='$nu_orden' where prv_id='$tecnologia'";	

			$conec->ejecutar($consulta);
			
			$consulta = "update proveedor set prv_orden='$ant_orden' where prv_id='$id'";	

			$conec->ejecutar($consulta);
		}	
	}
	
	function dibujar_encabezado()
	{
		?>
			<tr>
	        	<th>Nombre</th>				
				<th>Orden</th>
	            <th class="tOpciones" width="100px">Opciones</th>
			</tr>
			
		<?PHP
	}
	
	function mostrar_busqueda()
	{
		$conversor = new convertir();
		
		for($i=0;$i<$this->numero;$i++)
			{
				
				$objeto=$this->coneccion->get_objeto();
				echo '<tr>';
									
					echo "<td>";
						echo $objeto->prv_nombre;
					echo "&nbsp;</td>";
					
					echo "<td>";
						?>
						<center><a href="<?php echo $this->link.'?mod='.$this->modulo.'&tarea=ACCEDER&tec='.$objeto->prv_id.'&acc=s&or='.$objeto->prv_orden;?>"><img src="images/subir.png" border="0"></a><a href="<?php echo $this->link.'?mod='.$this->modulo.'&tarea=ACCEDER&tec='.$objeto->prv_id.'&acc=b&or='.$objeto->prv_orden;?>"><img src="images/bajarr.png" border="0"></a></center>
						<?php
					echo "</td>";
					echo "<td>";
						echo $this->get_opciones($objeto->prv_id);
					echo "</td>";
				echo "</tr>";
				
				$this->coneccion->siguiente();
			}
	}
	
	function cargar_datos()
	{
		$conec=new ADO();
		
		$sql="select * from proveedor
				where prv_id = '".$_GET['id']."'";
		
		$conec->ejecutar($sql);
		
		$objeto=$conec->get_objeto();
		 	 	 	 	 	 	 	 	 
		$_POST['prv_nombre']=$objeto->prv_nombre;	
		
		
	}
	
	function datos()
	{
		if($_POST)
		{
			//texto,  numero,  real,  fecha,  mail.
			$num=0;
			$valores[$num]["etiqueta"]="Nombre";
			$valores[$num]["valor"]=$_POST['prv_nombre'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			
			$val=NEW VALIDADOR;
			
			$this->mensaje="";
			
			if($val->validar($valores))
			{
				return true;
			}
				
			else
			{
				$this->mensaje=$val->mensaje;
				return false;
			}
		}
			return false;
	}
	
	function formulario_tcp($tipo)
	{
				switch ($tipo)
				{
					case 'ver':{
								$ver=true;
								break;
								}
							
					case 'cargar':{
								$cargar=true;
								break;
								}
				}
				
				$url=$this->link.'?mod='.$this->modulo;
				
				$red=$url.'&tarea=ACCEDER';
				
				if(!($ver))
				{
					$url.="&tarea=".$_GET['tarea'];
				}
				
				if($cargar)
				{
					$url.='&id='.$_GET['id'];
				}

		
		    $this->formulario->dibujar_tarea('PROVEEDORES');
		
			if($this->mensaje<>"")
			{
				$this->formulario->dibujar_mensaje($this->mensaje);
			}
			?>
		<div id="Contenedor_NuevaSentencia">
			<form id="frm_sentencia" name="frm_sentencia" action="<?php echo $url;?>" method="POST" enctype="multipart/form-data">  
				<div id="FormSent">
				  
					<div class="Subtitulo">Datos</div>
						<div id="ContenedorSeleccion">
							
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">* </span>Nombre</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto" name="prv_nombre" id="prv_nombre" maxlength="250"  size="30" value="<?php echo $_POST['prv_nombre'];?>">
							   </div>
							</div>
							<!--Fin-->
							
						</div>
					
						<div id="ContenedorDiv">
						   <div id="CajaBotones">
								<center>
								<?php
								if(!($ver))
								{
									?>
									<input type="submit" class="boton" name="" value="Guardar">
									<input type="reset" class="boton" name="" value="Cancelar">
									<input type="button" class="boton" name="" value="Volver" onclick="javascript:location.href='<?php echo $red;?>';">
									<?php
								}
								else
								{
									?>
									<input type="button" class="boton" name="" value="Volver" onclick="javascript:location.href='<?php echo $red;?>';">
									<?php
								}
								?>
								</center>
						   </div>
						</div>
				</div>
			</form>
		</div>
		<?php
	}
	
	function insertar_tcp()
	{
		$conec= new ADO();
		
		$sql=" select max(prv_orden) as ultimo from proveedor";
		
		$conec->ejecutar($sql);
		
		$objeto=$conec->get_objeto();
		
		$orden=$objeto->ultimo + 1;
				
		
		$sql="insert into proveedor(prv_nombre,prv_orden) values 
								('".$_POST['prv_nombre']."','$orden')";

		$conec->ejecutar($sql);
		$mensaje='Proveedor Agregada Correctamente!!!';

		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
		
	}
	
	
	
	function modificar_tcp()
	{
		$conec= new ADO();	
			
		$sql="update proveedor set 
								prv_nombre='".$_POST['prv_nombre']."'
								
								where prv_id = '".$_GET['id']."'";

			$conec->ejecutar($sql);

			$mensaje='Proveedor Modificada Correctamente!!!';

			$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
		
	}
	
	function formulario_confirmar_eliminacion()
	{
		
		$mensaje='Esta seguro de eliminar la proveedor?';
		
		$this->formulario->ventana_confirmacion($mensaje,$this->link."?mod=$this->modulo".'&tarea=ELIMINAR','prv_id');
	}
	
	function eliminar_tcp()
	{
		
			$llave=$_POST['prv_id'];
		
			
			
			$conec= new ADO();
			
			$sql="delete from proveedor where prv_id='".$_POST['prv_id']."'";
			
			$conec->ejecutar($sql);
			
			$mensaje='Proveedor Eliminada Correctamente!!!';
			
		
		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
	}
	
	function nombre_imagen($id)
	{
		$conec= new ADO();
		
		$sql="select tec_imagen from tecnologia where tec_id='".$id."'";
		
		$conec->ejecutar($sql);
		
		$objeto=$conec->get_objeto();
		
		return $objeto->tec_imagen;
	}
	
	function eliminar_imagen()
	{
		$conec= new ADO();
		
		$mi=$_GET['img'];
		
		$mifile="imagenes/tecnologia/$mi";
				
		@unlink($mifile);
		
		
		$conec= new ADO();
		
		$sql="update tecnologia set 
						tec_imagen=''
						where tec_id = '".$_GET['id']."'";
						
		$conec->ejecutar($sql);
		
		$mensaje='Imagen Eliminada Correctamente!';
			
		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=MODIFICAR&id='.$_GET['id']);
		
	}
}
?>