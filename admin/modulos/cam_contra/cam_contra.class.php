<?php

class CAM_CONTRA
{
	var $mensaje;
	
	function CAM_CONTRA()
	{
		
		
		$this->link='gestor.php';
		
		$this->modulo='cam_contra';
		
		$this->formulario = new FORMULARIO();
		
		$this->formulario->set_titulo('CAMBIAR CONTRASEÑA');
		
	}

	function datos()
	{
		
		
		if($_POST)
		{
			$this->mensaje="";
			
			if(!(trim(($_POST['actual']))<>''))
			{
				$this->mensaje.="El campo <b>Contraseña actual</b> es requerido<br>";
			}
			
			if(!(trim(($_POST['nueva']))<>''))
			{
				$this->mensaje.="El campo <b>Contraseña nueva</b> es requerido<br>";
			}
			
			if(!(trim(($_POST['confirmacion']))<>''))
			{
				$this->mensaje.="El campo <b>Confirmar contraseña nueva</b> es requerido<br>";
			}
			
			if($this->mensaje=="")
		
				return true;
			
			else
			
				return false;
		}
		else
		
			return false;
		
		
	}
	
	function obtener_password($id)
	{
		$conec= new ADO();
		
		$sql="select usu_password from ad_usuario where usu_id='$id'";
		 
		$conec->ejecutar($sql);
		
		$objeto=$conec->get_objeto();
		
		return $objeto->usu_password; 
		
	}
	
	function gestionar(&$actual,&$nueva,&$confirmacion,&$color1,&$color2)
	{
		require_once('clases/usuario.class.php');
		
		$usuario=NEW USUARIO();
		
		$usu=$usuario->get_id();
		
		$actual_base=$this->obtener_password($usu);
		
		if(md5($actual)==$actual_base)
		{
			if($nueva==$confirmacion)
			{
				$pass=md5($nueva);
				$actual="";
				$nueva="";
				$confirmacion="";
				$this->mensaje="La contraseña fue actualizada correctamente";
				$color1='#49994B';
				$color2='#D3F6D4';
				
				$conec= new ADO();
		
				$sql="update ad_usuario set usu_password='$pass' where usu_id='$usu'";
		 
				$conec->ejecutar($sql);
			}
			else
			{
				$this->mensaje="La contraseña nueva no es igual a la contraseña de confirmación";
				$color1='#DD3C10';
				$color2='#FFEBE8';
			}
		}
		else
		{
			$this->mensaje="La contraseña actual es incorrecta";
			$color1='#DD3C10';
			$color2='#FFEBE8';
		}
		
	}

	function formulario_tcp()
	{
		$actual=trim($_POST['actual']);
			
		$nueva=trim($_POST['nueva']);
		
		$confirmacion=trim($_POST['confirmacion']);
		
		$color1='#DD3C10';
		
		$color2='#FFEBE8';
		
		if($this->datos())
		{	
			$this->gestionar($actual,$nueva,$confirmacion,$color1,$color2);
		}
		
		$url=$this->link.'?mod='.$this->modulo;
		
		$this->formulario->dibujar_tarea();
		
		
			if($this->mensaje<>"")
			{
			?>
				<table width="100%" cellpadding="0" cellspacing="1" style="border:1px solid <?php echo $color1;?>; color:<?php echo $color1;?>;">
				<tr bgcolor="<?php echo $color2;?>">
					<td align="center">
						<?php
							echo $this->mensaje;
						?>
					</td>
				</tr>
				</table>
			<?php
			}
			?>
			
			<div id="Contenedor_NuevaSentencia">
			<form id="frm_sentencia" name="frm_sentencia" action="<?php echo $url;?>" method="POST" enctype="multipart/form-data">  
				<div id="FormSent">
				  
					<div class="Subtitulo">Datos</div>
						<div id="ContenedorSeleccion">
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">* </span>Contraseña actual	</div>
							   <div id="CajaInput">
							   <input type="password" class="caja_texto" name="actual" id="actual" size="25" value="<?php echo $actual;?>">
							   </div>
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">* </span>Contraseña nueva		</div>
							   <div id="CajaInput">
							   <input type="password" class="caja_texto" name="nueva" id="nueva" size="25" value="<?php echo $nueva;?>">
							   </div>
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">* </span>Confirmar contraseña nueva	</div>
							   <div id="CajaInput">
							   <input type="password" class="caja_texto" name="confirmacion" id="confirmacion" size="25" value="<?php echo $confirmacion;?>">
							   </div>
							</div>
							<!--Fin-->
							
							
						</div>
					
						<div id="ContenedorDiv">
						   <div id="CajaBotones">
								<center>
								<?php
								if(!($ver))
								{
									?>
									<input type="submit" class="boton" name="" value="Guardar">
									<input type="reset" class="boton" name="" value="Cancelar">
									<input type="button" class="boton" name="" value="Volver" onclick="javascript:location.href='<?php echo $red;?>';">
									<?php
								}
								else
								{
									?>
									<input type="button" class="boton" name="" value="Volver" onclick="javascript:location.href='<?php echo $red;?>';">
									<?php
								}
								?>
								</center>
						   </div>
						</div>
				</div>
			</form>
		</div>	
			
		<?php
	}
}
?>