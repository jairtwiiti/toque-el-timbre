<?php

class CONTENIDO extends BUSQUEDA 
{
	var $formulario;
	var $mensaje;
	
	function CONTENIDO()
	{
		//permisos
		$this->ele_id=153;
		
		$this->busqueda();
		
		if(!($this->verificar_permisos('AGREGAR')))
		{
			$this->ban_agregar=false;
		}
		//fin permisos
		
		$this->num_registros=25;
		
		$this->coneccion= new ADO();
		
		$this->arreglo_campos[0]["nombre"]="con_titulo";
		$this->arreglo_campos[0]["texto"]="T�tulo";
		$this->arreglo_campos[0]["tipo"]="cadena";
		$this->arreglo_campos[0]["tamanio"]=40;
		
		$this->link='gestor.php';
		
		$this->modulo='sinrenovar';
		
		$this->formulario = new FORMULARIO();
		
		$this->formulario->set_titulo('PUBLICACIONES SIN RENOVAR');
		
		
	}
	
	
	function dibujar_busqueda()
	{
		
		$this->formulario->dibujar_cabecera();
		
		$this->dibujar_listado();
	}
	
		
	function set_opciones()
	{
				
		$nun=0;
		
		if($this->verificar_permisos('VER'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='VER';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_search.png';
			$this->arreglo_opciones[$nun]["nombre"]='VER';
			$nun++;
		}
		
		if($this->verificar_permisos('MODIFICAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='MODIFICAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_edit.png';
			$this->arreglo_opciones[$nun]["nombre"]='MODIFICAR';
			$nun++;
		}
		
		if($this->verificar_permisos('ELIMINAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='ELIMINAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_drop.png';
			$this->arreglo_opciones[$nun]["nombre"]='ELIMINAR';
			$nun++;
		}
		
		if($this->verificar_permisos('HABILITAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='HABILITAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/ok.png';
			$this->arreglo_opciones[$nun]["nombre"]='HABILITAR';
			$this->arreglo_opciones[$nun]["script"]="ok";
			$nun++;
		}
	}
	
	function aprobar(){
		$conec= new ADO();		
		$pub_vig_fin = date("Y-m-d" ,  mktime(0,0,0,date('m'),date('d'),date('Y')) +  2592000 ); //a�adimos 30 dias mas
			
		$sql="UPDATE publicacion SET pub_enviado='0',pub_vig_fin='$pub_vig_fin' WHERE pub_id=".$_GET['id'];			
		$conec->ejecutar($sql);	
	}
	
	function dibujar_listado()
	{
		?>
		<script>		
		function ejecutar_script(id,tarea){
				var txt = 'Esta seguro de renovar esta publicaci�n?';
				
				$.prompt(txt,{ 
					buttons:{Aceptar:true, Cancelar:false},
					callback: function(v,m,f){
						
						if(v){
								location.href='gestor.php?mod=sinrenovar&tarea='+tarea+'&id='+id;
						}
												
					}
				});
			}

		</script>
		<?php
		
		$fecha_actual = date("Y-m-d");
	
		$sql="SELECT * FROM publicacion,usuario,inmueble where usu_id=pub_usu_id and inm_id=pub_inm_id and pub_estado='Aprobado' and pub_vig_fin<'$fecha_actual' ";			
		
		$this->set_sql($sql,' order by pub_vig_fin asc');
		
		$this->set_opciones();
		
		$this->dibujar();
		
	}
	
	function dibujar_encabezado()
	{
		?>
			<tr>
	        	<th>ID</th>
				<th>Titulo</th>
				<th>Usuario</th>
				<th>Email</th>
				<th>Tel�fono</th>
				<th>Fecha Vigencia</th>
				<th class="tOpciones" width="100px">Opciones</th>
			</tr>
			
		<?PHP
	}
	
	function mostrar_busqueda()
	{
		$conversor = new convertir();
		
		for($i=0;$i<$this->numero;$i++)
			{
				
				$objeto=$this->coneccion->get_objeto();
				echo '<tr>';
									
					echo "<td>";
						echo $objeto->inm_id;
					echo "&nbsp;</td>";
					
					echo "<td>";
						echo $objeto->inm_nombre;
					echo "&nbsp;</td>";
					
					echo "<td>";
						echo $objeto->usu_nombre .' '. $objeto->usu_apellido ;
					echo "&nbsp;</td>";
					
					echo "<td>";
						echo $objeto->usu_email;
					echo "&nbsp;</td>";
					
					echo "<td>";
						echo $objeto->usu_telefono;
					echo "&nbsp;</td>";
					
					echo "<td>";
						echo $objeto->pub_vig_fin;
					echo "&nbsp;</td>";
					
					echo "<td>";
						echo $this->get_opciones($objeto->pub_id);
					echo "</td>";
				echo "</tr>";
				
				$this->coneccion->siguiente();
			}
	}
	
}
?>