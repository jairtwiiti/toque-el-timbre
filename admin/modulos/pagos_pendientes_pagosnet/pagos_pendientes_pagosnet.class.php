<?php

class CONTENIDO extends BUSQUEDA 
{
	var $formulario;
	var $mensaje;
	
	function CONTENIDO()
	{
		//permisos
		$this->ele_id=166;
		
		$this->busqueda();
		
		if(!($this->verificar_permisos('AGREGAR')))
		{
			$this->ban_agregar=false;
		}
		//fin permisos
		
		$this->num_registros=25;
		
		$this->coneccion= new ADO();
		
		$this->arreglo_campos[0]["nombre"]="pag_id";
		$this->arreglo_campos[0]["texto"]="ID Transanccion";
		$this->arreglo_campos[0]["tipo"]="cadena";
		$this->arreglo_campos[0]["tamanio"]=40;
		
		$this->link='gestor.php';
		
		$this->modulo='pagos_pendientes_pagosnet';
		
		$this->formulario = new FORMULARIO();
		
		$this->formulario->set_titulo('PAGOS PENDIENTES DE APROBAR');
		
		
	}
	
	
	function dibujar_busqueda()
	{
		
		$this->formulario->dibujar_cabecera();
		
		$this->dibujar_listado();
	}
	
		
	function set_opciones()
	{
				
		$nun=0;
		
		if($this->verificar_permisos('VER'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='VER';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_search.png';
			$this->arreglo_opciones[$nun]["nombre"]='VER';
			$nun++;
		}
		
		if($this->verificar_permisos('MODIFICAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='MODIFICAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_edit.png';
			$this->arreglo_opciones[$nun]["nombre"]='MODIFICAR';
			$nun++;
		}
		
		if($this->verificar_permisos('ELIMINAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='ELIMINAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_drop.png';
			$this->arreglo_opciones[$nun]["nombre"]='ELIMINAR';
			$nun++;
		}
		
		if($this->verificar_permisos('HABILITAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='HABILITAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/ok.png';
			$this->arreglo_opciones[$nun]["nombre"]='HABILITAR';
			$this->arreglo_opciones[$nun]["script"]="ok";
			$nun++;
		}
		
		if($this->verificar_permisos('APROBAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='APROBAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/ok.png';
			$this->arreglo_opciones[$nun]["nombre"]='APROBAR';
			$this->arreglo_opciones[$nun]["script"]="ok";
			$nun++;
		}
	}
	
	
	
	
	
	
	
	
	function verificar_existencia_otros_pagos($pag_id, $pub_id, $ser_id){
		$conec= new ADO();
		/*$sql = "
		SELECT p.pag_entidad, ps.fech_ini, ps.fech_fin FROM pagos p
		INNER JOIN pago_servicio ps ON (ps.pag_id = p.pag_id AND ps.ser_id = ".$ser_id.")
		WHERE p.pag_pub_id = ".$pub_id." AND p.pag_id <> '".$pag_id."'
		ORDER BY p.pag_fecha DESC LIMIT 1
		";*/
        $sql = "
        SELECT p.pag_entidad, ps.fech_ini, ps.fech_fin FROM pagos p
        INNER JOIN pago_servicio ps ON (ps.pag_id = p.pag_id AND ps.ser_id = ".$ser_id." AND (CURDATE() >= ps.`fech_ini` AND CURDATE() <= ps.`fech_fin`) )
        WHERE p.pag_pub_id = ".$pub_id." AND p.`pag_id` <> '$pag_id'
        ORDER BY p.pag_fecha_pagado DESC, p.pag_fecha DESC
        LIMIT 1
        ";
		$result = $conec->ejecutar($sql);
		$resultado = $conec->get_objeto($result);
		return $resultado->fech_fin;
	}

    function obtener_monto_total($pag_id){
        $conec= new ADO();
        $sql = "
		SELECT pa.`pag_monto` FROM pagos pa
        WHERE pa.`pag_id` = '$pag_id'
		";
        $result = mysql_query($sql);
        $resultado = mysql_fetch_object($result);
        return $resultado->pag_monto;
    }
	
	function obtener_id_publicacion($pag_id){
		$conec= new ADO();
		$sql = "
		SELECT pag_pub_id FROM pagos 
		WHERE pag_id = '".$pag_id."'
		LIMIT 1
		";
		$result = $conec->ejecutar($sql);
		$resultado = $conec->get_objeto($result);
		return $resultado->pag_pub_id;
	}

    function habilitar_usuario($pub_id){
        $conec= new ADO();
        $conec2 = new ADO();
        $sql = "
		SELECT pub_usu_id FROM publicacion p
		WHERE pub_id = '".$pub_id."'
		LIMIT 1
		";
        $result = $conec->ejecutar($sql);
        $resultado = $conec->get_objeto($result);
        $sql = "UPDATE usuario SET usu_estado = 'Habilitado' WHERE usu_id = ".$resultado->pub_usu_id;
        $conec2->ejecutar($sql);
    }
	
	function obtener_id_servicios($pag_id){
		$conec= new ADO();
		$sql = "
		SELECT ser_id FROM pago_servicio
		WHERE pag_id = '".$pag_id."'
		";
		$result = mysql_query($sql);
		
		while ($row = mysql_fetch_array($result)){
			$resultado[] = $row['ser_id'];
		}
		return $resultado;
	}
	
	function obtener_email_de_pago($pag_id){
		$conec= new ADO();
		$sql = "
		SELECT `usu_email` FROM pagos
		INNER JOIN publicacion ON pub_id = pag_pub_id
		INNER JOIN usuario ON usu_id = pub_usu_id
		WHERE pag_id =  '".$pag_id."'
		";
		$result = mysql_query($sql);
		$email = mysql_fetch_object($result);
		
		return $email->usu_email;
	}
	
	
	function obtener_id_publicacion_estado($pag_id){
		$conec= new ADO();
		$sql = "
		SELECT pag_pub_id FROM pagos 
		WHERE pag_id = '".$pag_id."'
		LIMIT 1
		";
		$result = mysql_query($sql);
		$resultado = mysql_fetch_object($result);
		return $resultado->pag_pub_id;
	}

    function obtener_mayor_fecha_servicio($pub_id){
        $sql = "
        SELECT MAX(ps.`fech_fin`) AS mayor_fecha FROM pagos p
        INNER JOIN pago_servicio ps ON (ps.`pag_id` = p.`pag_id` AND CURDATE() <= ps.`fech_fin`)
        WHERE p.`pag_pub_id` = '$pub_id' AND p.`pag_estado` = 'Pagado'
        ";
        $result = mysql_query($sql);
        $resultado = mysql_fetch_object($result);
        return $resultado->mayor_fecha;
    }

    public function obtener_publicacion($pag_id){
        $conec= new ADO();
        $sql = "
        SELECT pub.`pub_id`, pub.`pub_vig_fin` FROM pagos p
        INNER JOIN publicacion pub ON (pub.`pub_id` = p.`pag_pub_id`)
        WHERE p.`pag_id` = '$pag_id'
        GROUP BY pub.`pub_id`
        ";
        $result = mysql_query($sql);
        $resultado = mysql_fetch_object($result);
        return $resultado;
    }
	
	function obtener_id_inmueble($pub_id){
		$conec= new ADO();
		$sql = "
		SELECT inm_id, inm_nombre FROM publicacion
		INNER JOIN inmueble on (inm_id = pub_inm_id)
		WHERE pub_id = '".$pub_id."'
		LIMIT 1
		";
		$result = mysql_query($sql);
		$resultado = mysql_fetch_object($result);
		return $resultado;
	}
	
	function aprobar(){
		$conec= new ADO();
		$conec2= new ADO();			
		
		$id_pago = $_GET['id'];
		$codigos_servicios = $this->obtener_id_servicios($id_pago);
		$email = $this->obtener_email_de_pago($id_pago);
        $id_pub = $this->obtener_id_publicacion($id_pago);
        $this->habilitar_usuario($id_pub);

		$sql = "UPDATE pagos SET pag_estado = 'Pagado', pag_entidad='Oficina', pag_fecha_pagado = NOW(), pag_modificado = 1 WHERE pag_id = '" . $id_pago . "'";
		$conec->ejecutar($sql);
		
		$total = 0;
		foreach($codigos_servicios as $ser_id){
			
			if($ser_id != ""){
				$conec3 = new ADO();
				$sql = "SELECT * FROM servicios WHERE ser_id = " . $ser_id;
				$conec3->ejecutar($sql);
				$obj_servicio = $conec3->get_objeto();

				$aux = $this->verificar_existencia_otros_pagos($id_pago, $id_pub, $ser_id);
				if(!empty($aux)){
					$fech_inicio = $aux;
				}else{
					$fech_inicio = date("Y-m-d");
				}

				$fec_vencimi = date("Y-m-d", strtotime($fech_inicio. " + " . ($obj_servicio->ser_dias - 1 ) . " days"));
				$sql = "UPDATE pago_servicio SET fech_ini = '".$fech_inicio."', fech_fin = '".$fec_vencimi."' WHERE pag_id = '" . $id_pago . "' and ser_id = " . $ser_id;
				$conec2->ejecutar($sql);
				
				//$total = $total + $obj_servicio->ser_precio;
			}
		}

        $publicacion = $this->obtener_publicacion($id_pago);
        $total = $this->obtener_monto_total($id_pago);
        $mayor_fecha = $this->obtener_mayor_fecha_servicio($publicacion->pub_id);
        if($mayor_fecha > $publicacion->pub_vig_fin){
            $pub_vig_fin = $mayor_fecha;
            $sql = "UPDATE publicacion SET pub_estado='Aprobado', pub_vig_fin='". $pub_vig_fin ."' WHERE pub_id = '".$publicacion->pub_id."'";
        }else{
            $sql = "UPDATE publicacion SET pub_estado='Aprobado' WHERE pub_id = '".$publicacion->pub_id."'";
        }
        $conec->ejecutar($sql);
        $obj_inmueble = $this->obtener_id_inmueble($publicacion->pub_id);
        $sql="update inmueble set inm_publicado='Si' where inm_id = '".$obj_inmueble->inm_id."'";
        $conec->ejecutar($sql);

		$this->enviar_mail($total, $email, $obj_inmueble->inm_nombre);
	}
	
	
	function enviar_mail($total,$email, $inmueble)	{
		require_once('./clases/phpmailer/class.phpmailer.php');
	
		$sql="select * from ad_parametro limit 1";		 
		$rs = mysql_query($sql);		    	
		$fila=mysql_fetch_object($rs);
        $contenido = $this->obtener_contenido("http://toqueeltimbre.com/assets/template_mail/confirmacion_pago.html");
		
		$contenido = str_replace('@@monto',$total,$contenido);		
	    $contenido = str_replace('@@inmueble',$inmueble,$contenido);
	
		$cue_nombre=$fila->par_salida; 					
		$cue_pass=$fila->par_pas_salida;				
		$mail = new PHPMailer(true);				
		$mail->SetLanguage('es');		
		$mail->IsSMTP();			
		$mail->SMTPAuth   = true;          
		$mail->Host       = $fila->par_smtp;      			
		$mail->Timeout=30;		
		$mail->CharSet = 'utf-8';				
		$mail->Subject = "Pago Confirmado";		
		$body =  $contenido;					
		$mail->MsgHTML($body);					
		$mail->AddAddress($email);
        //$mail->AddAddress("cristian.inarra@gmail.com");
		$mail->SetFrom($cue_nombre, $fila->par_smtp);		
		$mail->Username   = $cue_nombre;  	
		$mail->Password   = $cue_pass;		
		try {				
			$mail->Send(); 	          
			return "el mensaje fue enviado correctamente";		
		} catch (phpmailerException $e) {		
			return  "el mensaje no puedo ser enviado";		
		} catch (Exception $e) {			
			return "el mensaje no puedo ser enviado";		
		}			
	}	
	
	function obtener_contenido($url)	{		
	$curl = curl_init();		
	curl_setopt($curl, CURLOPT_HEADER, 0);	
	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);		
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);		
	curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.3) Gecko/20070309 Firefox/2.0.0.3");		
	curl_setopt($curl, CURLOPT_URL, $url);   		
	$contenido = curl_exec($curl); 		
	curl_close($curl); 		
	return $contenido;	
	}	
	
	
	
	
	
	
	function dibujar_listado()
	{
		?>
		<script>		
		function ejecutar_script(id,tarea){
				var txt = 'Esta seguro que desea marcar como pagada esta publicaci�n?';
				
				$.prompt(txt,{ 
					buttons:{"Aceptar":true, "Cancelar":false},
					submit: function(v,m,f){
						if(v){
								location.href='gestor.php?mod=pagos_pendientes_pagosnet&tarea='+tarea+'&id='+id;
						}
					}
				});
			}

		</script>
		<?php
		
		$fecha_actual = date("Y-m-d");
	
		/*$sql="
		SELECT pag_id, pub_id, inm_nombre, pag_fecha, pag_monto, pag_entidad, pag_estado, CONCAT(usu_nombre,' ',usu_apellido) AS nombre FROM pagos
		INNER JOIN publicacion ON pub_id = pag_pub_id
		INNER JOIN inmueble ON inm_id = pub_inm_id
		INNER JOIN usuario ON usu_id = pub_usu_id
		where pag_estado = 'Pendiente' and pag_entidad = 'Pagosnet'
		AND CURDATE() BETWEEN DATE(pag_fecha) AND DATE(pag_fecha_ven)
		";*/
		
		$sql="
		SELECT pag_id, pub_id, inm_nombre, pag_fecha, pag_monto, pag_entidad, pag_estado, CONCAT(usu_nombre,' ',usu_apellido) AS nombre, usu_email FROM pagos
		INNER JOIN publicacion ON (pub_id = pag_pub_id AND pub_estado <> 'Eliminado')
		INNER JOIN inmueble ON inm_id = pub_inm_id
		INNER JOIN usuario ON usu_id = pub_usu_id
		where pag_estado = 'Pendiente' and pag_entidad = 'Pagosnet'
		";
		
		
		$this->set_sql($sql,' ORDER BY pag_fecha DESC');
		
		$this->set_opciones();
		
		$this->dibujar();
		
	}
	
	function dibujar_encabezado()
	{
		?>
			<tr>
	        	<th>ID TRANSACCION</th>
				<th>Nombre Inmueble</th>
				<th>Nombre Usuario</th>
                <th>Email</th>
				<th>Fecha Pago</th>
				<th>Monto</th>
				<th>Metodo</th>
				<th>Estado</th>
				<th class="tOpciones" width="100px">Opciones</th>
			</tr>
			
		<?PHP
	}
	
	function mostrar_busqueda()
	{
		$conversor = new convertir();
		
		for($i=0;$i<$this->numero;$i++)
			{
				
				$objeto=$this->coneccion->get_objeto();
				echo '<tr>';
									
					echo "<td>";
						echo $objeto->pag_id;
					echo "&nbsp;</td>";
					
					echo "<td>";
						echo $objeto->inm_nombre;
					echo "&nbsp;</td>";
					
					echo "<td>";
						echo $objeto->nombre;
					echo "&nbsp;</td>";
					
					echo "<td>";
						echo $objeto->usu_email;
					echo "&nbsp;</td>";
					
					echo "<td>";
						echo $objeto->pag_fecha;
					echo "&nbsp;</td>";
					
					echo "<td>";
						echo $objeto->pag_monto;
					echo "&nbsp;</td>";
					
					echo "<td>";
						echo $objeto->pag_entidad;
					echo "&nbsp;</td>";
					
					echo "<td>";
						echo $objeto->pag_estado;
					echo "&nbsp;</td>";
					
					echo "<td>";
						echo $this->get_opciones($objeto->pag_id);
					echo "</td>";
				echo "</tr>";
				
				$this->coneccion->siguiente();
			}
	}
	
	function mostrar_detalle_pago(){
		$id_pago = $_GET['id'];
		
		$conec=new ADO();		
		$sql="
		SELECT s.ser_descripcion, s.ser_precio, ps.fech_ini, ps.fech_fin FROM pago_servicio ps 
		INNER JOIN servicios s ON s.ser_id = ps.ser_id
		WHERE ps.pag_id = '".$_GET['id']."'
		";
		$result = mysql_query($sql);
		
		?>
		
		<div id="Contenedor_NuevaSentencia">
			<div id="FormSent" style="width:650px">
				<div class="Subtitulo">Detalle de Pago - <?php echo $_GET['id']; ?></div>
				<div id="ContenedorSeleccion">
				
				<table width="100%" class="tablaLista">
				<thead>
				<tr>
					<th>Servicio</th>
					<th>Fecha Inicio</th>
					<th>Fecha Fin</th>
					<th>Precio</th>
				</tr>
				</thead>
		
		<?php 
		while($row = mysql_fetch_array($result))
		{
			
			?>
			
			<tr>
				<td><?php echo $row['ser_descripcion']?></td>
				<td><?php echo $row['fech_ini']?></td>
				<td><?php echo $row['fech_fin']?></td>
				<td><?php echo $row['ser_precio']; ?></td>
			</tr>
			
			<?php
			$total = $total + $row['ser_precio'];
		}
		?>
				</table>
				
				
				<h2>
					Total: BS. <?php echo $total; ?>
				</h2>
				
				
				</div>
			</div>
		</div>
		
	<?php 
	}
	
	
}
?>