<?php
	
	require_once('categoria.class.php');
	
	$categoria = new CATEGORIA();
	
	if(!($categoria->verificar_permisos($_GET['tarea'])))
	{
		?>
		<script>
			location.href="log_out.php";
		</script>
		<?php
	}
	
	switch ($_GET['tarea'])
	{
		case 'AGREGAR':{
						
							if($categoria->datos())
							{
								$categoria->insertar_tcp();
							}
							else 
							{
								$categoria->formulario_tcp('blanco');
							}
					
						
						
						break;}
		case 'VER':{
						$categoria->cargar_datos();
												
						$categoria->formulario_tcp('ver');
						
						break;}
		case 'MODIFICAR':{
							if($_GET['acc']=='Imagen')
							{
								$categoria->eliminar_imagen();
							}
							else
							{
								if($categoria->datos())
								{
									$categoria->modificar_tcp();
								}
								else 
								{
									if(!($_POST))
									{
										$categoria->cargar_datos();
									}
									$categoria->formulario_tcp('cargar');
								}
							}
						break;}
		case 'ELIMINAR':{
							
								if(isset($_POST['cat_id']))
								{
									if(trim($_POST['cat_id'])<>"")
									{
										$categoria->eliminar_tcp();
									}
									else 
									{
										$categoria->dibujar_busqueda();
									}
								}
								else 
								{
									$categoria->formulario_confirmar_eliminacion();
								}
							
							
						break;}
		case 'ACCEDER':{	
						
						if($_GET['acc']<>"")
						{
							$categoria->orden($_GET['tec'],$_GET['acc'],$_GET['or']);
						}
						$categoria->dibujar_busqueda();
						break;
						}
		
	}
		
?>