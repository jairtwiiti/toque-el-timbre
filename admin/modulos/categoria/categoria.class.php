<?php

class CATEGORIA extends BUSQUEDA 
{
	var $formulario;
	var $mensaje;
	
	function CATEGORIA()
	{
		//permisos
		$this->ele_id=120;
		
		$this->busqueda();
		
		if(!($this->verificar_permisos('AGREGAR')))
		{
			$this->ban_agregar=false;
		}
		//fin permisos
		
		$this->num_registros=14;
		
		$this->coneccion= new ADO();
		
		$this->arreglo_campos[0]["nombre"]="cat_nombre";
		$this->arreglo_campos[0]["texto"]="Nombre";
		$this->arreglo_campos[0]["tipo"]="cadena";
		$this->arreglo_campos[0]["tamanio"]=40;
		
		$this->link='gestor.php';
		
		$this->modulo='categoria';
		
		$this->formulario = new FORMULARIO();
		
		$this->formulario->set_titulo('CATEGORIAS');
		
		
	}
	
	
	function dibujar_busqueda()
	{
		
		$this->formulario->dibujar_cabecera();
		
		$this->dibujar_listado();
	}
	
		
	function set_opciones()
	{
				
		$nun=0;
		
		if($this->verificar_permisos('VER'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='VER';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_search.png';
			$this->arreglo_opciones[$nun]["nombre"]='VER';
			$nun++;
		}
		
		if($this->verificar_permisos('MODIFICAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='MODIFICAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_edit.png';
			$this->arreglo_opciones[$nun]["nombre"]='MODIFICAR';
			$nun++;
		}
		
		if($this->verificar_permisos('ELIMINAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='ELIMINAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_drop.png';
			$this->arreglo_opciones[$nun]["nombre"]='ELIMINAR';
			$nun++;
		}
	}
	
	function dibujar_listado()
	{
		$sql="SELECT * FROM categoria";
		
		$this->set_sql($sql,' order by cat_orden desc ');
		
		$this->set_opciones();
		
		$this->dibujar();
		
	}
	
	function orden($tecnologia,$accion,$ant_orden)
	{
		$conec= new ADO();
		
		if($accion=='s')
			$cad=" where cat_orden > $ant_orden order by cat_orden asc";
		else
			$cad=" where cat_orden < $ant_orden order by cat_orden desc";

		$consulta = "
		select 
			cat_id,cat_orden 
		from 
			categoria
		$cad
		limit 0,1
		";	

		$conec->ejecutar($consulta);

		$num = $conec->get_num_registros();   

		if($num > 0)
		{
			$objeto=$conec->get_objeto();
			
			$nu_orden=$objeto->tec_orden;
			
			$id=$objeto->tec_id;
			
			$consulta = "update categoria set cat_orden='$nu_orden' where cat_id='$tecnologia'";	

			$conec->ejecutar($consulta);
			
			$consulta = "update categoria set cat_orden='$ant_orden' where cat_id='$id'";	

			$conec->ejecutar($consulta);
		}	
	}
	
	function dibujar_encabezado()
	{
		?>
			<tr>
	        	<th>Nombre</th>				
				<th>Orden</th>
	            <th class="tOpciones" width="100px">Opciones</th>
			</tr>
			
		<?PHP
	}
	
	function mostrar_busqueda()
	{
		$conversor = new convertir();
		
		for($i=0;$i<$this->numero;$i++)
			{
				
				$objeto=$this->coneccion->get_objeto();
				echo '<tr>';
									
					echo "<td>";
						echo $objeto->cat_nombre;
					echo "&nbsp;</td>";
					
					echo "<td>";
						?>
						<center><a href="<?php echo $this->link.'?mod='.$this->modulo.'&tarea=ACCEDER&tec='.$objeto->cat_id.'&acc=s&or='.$objeto->cat_orden;?>"><img src="images/subir.png" border="0"></a><a href="<?php echo $this->link.'?mod='.$this->modulo.'&tarea=ACCEDER&tec='.$objeto->cat_id.'&acc=b&or='.$objeto->cat_orden;?>"><img src="images/bajarr.png" border="0"></a></center>
						<?php
					echo "</td>";
					echo "<td>";
						echo $this->get_opciones($objeto->cat_id);
					echo "</td>";
				echo "</tr>";
				
				$this->coneccion->siguiente();
			}
	}
	
	function cargar_datos()
	{
		$conec=new ADO();
		
		$sql="select * from categoria
				where cat_id = '".$_GET['id']."'";
		
		$conec->ejecutar($sql);
		
		$objeto=$conec->get_objeto();
		 	 	 	 	 	 	 	 	 
		$_POST['cat_nombre']=$objeto->cat_nombre;	
		
		
	}
	
	function datos()
	{
		if($_POST)
		{
			//texto,  numero,  real,  fecha,  mail.
			$num=0;
			$valores[$num]["etiqueta"]="Nombre";
			$valores[$num]["valor"]=$_POST['cat_nombre'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			
			$val=NEW VALIDADOR;
			
			$this->mensaje="";
			
			if($val->validar($valores))
			{
				return true;
			}
				
			else
			{
				$this->mensaje=$val->mensaje;
				return false;
			}
		}
			return false;
	}
	
	function formulario_tcp($tipo)
	{
				switch ($tipo)
				{
					case 'ver':{
								$ver=true;
								break;
								}
							
					case 'cargar':{
								$cargar=true;
								break;
								}
				}
				
				$url=$this->link.'?mod='.$this->modulo;
				
				$red=$url.'&tarea=ACCEDER';
				
				if(!($ver))
				{
					$url.="&tarea=".$_GET['tarea'];
				}
				
				if($cargar)
				{
					$url.='&id='.$_GET['id'];
				}

		
		    $this->formulario->dibujar_tarea('CATEGORIAS');
		
			if($this->mensaje<>"")
			{
				$this->formulario->mensaje('Error',$this->mensaje);
			}
			?>
		<script type="text/javascript">
			
			$().ready(function(){
			   $('.botonagr').click(function(){
					var contextoClick = this;			
					var nuevo_indice_detcomp = $("#tprueba tbody tr").length;
					
					var valor=$('#caracteristica').attr('value');
					
					if(valor != '')
					{
						var valores=valor.split('�');
						
						if(verificar(valores[0]))
						{
							var data = "<tr><td>"+(nuevo_indice_detcomp+1)+"<input type='hidden' name='orden[]' value='"+(nuevo_indice_detcomp+1)+"'></td><td><input type='hidden' name='caracteristicas[]' value='"+valores[0]+"'>"+valores[1]+"</td><td><select name='estado[]' class='caja_texto' style='width:60px'><option value='Si'>Si</option><option value='No'>No</option></select></td><td><img src='images/b_drop.png' class='borrar-detalle' style='cursor:pointer'></td></tr>";
							$('#tprueba tbody').append(data);							
						}
						else
						{
							$.prompt('La  Caracteristica ya fue agredada a la lista',{ opacity: 0.8 });
						}
					}
					else
					{
						$.prompt('Seleccione la caracteristica',{ opacity: 0.8 });
					}						
						
				});	
				
				

               $('#tprueba').delegate('.borrar-detalle','click',function(){
					if (confirm('Esta usted seguro de eliminar esta fila?')){				   
						var con=1;       					
						var $link = $(this);					
						$link.closest('tr').remove();						
						$("#tprueba tbody tr").each(function(){						
							 $(this).children('td').eq(0).text(con);
							 con++;
						});						
					}
				});				
				
								
			});	

			function verificar(id)
			{			    
				var cant = $('#tprueba tbody').children().length;               
				var ban=true;
				if(cant > 0)
				{
					$('#tprueba tbody').children().each(function(){
					var dato=$(this).eq(0).children().eq(1).children().eq(0).attr('value');	
					
					if(id==dato)
					{
						ban=false;
					}
									 
					}); 				
				}  
				return ban;				
			}	
				
				
			function enviar_formulario(){				
				
				var numcar=$("#tprueba tbody tr").length;
				if (numcar>0)
				{
					  document.frm_sentencia.submit();
				}
				else
				{					  		
				   if (numcar==0)
					  $.prompt('Agregue al menos una caracteristica',{ opacity: 0.8 });				  
				}								
			}
		</script>
		
		<div id="Contenedor_NuevaSentencia">
			<form id="frm_sentencia" name="frm_sentencia" action="<?php echo $url;?>" method="POST" enctype="multipart/form-data">  
				<div id="FormSent" style="width:90%;">
				  
					<div class="Subtitulo">Datos</div>
						<div id="ContenedorSeleccion">
							
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">* </span>Nombre</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto" name="cat_nombre" id="cat_nombre" maxlength="250"  size="30" value="<?php echo $_POST['cat_nombre'];?>">
							   </div>
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">* </span>Caracteristicas:</div>
							   <div id="CajaInput">
							   
							   <select name="caracteristica" id="caracteristica" class="caja_texto">
							   <option value="">Seleccione</option>
							   <?php 		
								
								$fun=NEW FUNCIONES;		
								 
								$fun->combo("select concat(car_id,'�',car_descripcion) as id,car_descripcion as nombre from caracteristica  order by car_descripcion asc",$_POST['caracteristica']);				
								
								?>
							   </select>
							 
							   </div>
							      <div id="CajaInput">								  
								  <img src="images/boton_agregar.png" style='margin:0px 0px 0px 10px;cursor:pointer' class='botonagr'>
							   </div>
							   <div id="CajaInput" style="margin:0px 0px 0px 10px;">
								  <table  width="300"   class="tablaReporte" id="tprueba" cellpadding="0" cellspacing="0">
									<thead>
									<tr>
									    <th width="5%">&nbsp;#&nbsp;</th>
										<th width="40%">Caracteristica</th>
										<th width="40%">Estado</th>
										<th class="tOpciones" width="60">Eliminar</th>
									</tr>							
									</thead>
									<tbody>
									<?php
									if($_GET['tarea']=='MODIFICAR' || $_GET['tarea']=='VER')
										$this->cargar_filas_detalle($_GET['id']);
									?>
									</tbody>
									</table>
							   </div>
							</div>
							<!--Fin-->
							
						</div>
						
						<div id="ContenedorDiv">
						   <div id="CajaBotones">
								<center>
								<?php
								if(!($ver))
								{
									?>
									<input type="button" class="boton" name="" value="Guardar"  onclick="javascript:enviar_formulario()">
									<input type="reset" class="boton" name="" value="Cancelar">
									<input type="button" class="boton" name="" value="Volver" onclick="javascript:location.href='<?php echo $red;?>';">
									<?php
								}
								else
								{
									?>
									<input type="button" class="boton" name="" value="Volver" onclick="javascript:location.href='<?php echo $red;?>';">
									<?php
								}
								?>
								</center>
						   </div>
						</div>
					</div>	
				</div>
			</form>
		</div>
		<?php
	}
	
	function insertar_tcp()
	{
		$conec= new ADO();	
		$sql=" select max(cat_orden) as ultimo from categoria";		
		$conec->ejecutar($sql);	
		
		$objeto=$conec->get_objeto();		
		$orden=$objeto->ultimo + 1;		
		$sql="insert into categoria(cat_nombre,cat_orden) values 
								('".$_POST['cat_nombre']."','$orden')";

		$conec->ejecutar($sql,false);
		$llave=mysql_insert_id();
		
		
		$caracteristicas=$_POST['caracteristicas'];
		$i=0;			
		
		foreach($caracteristicas as $caracteristica)
		{				
			$sql="insert into categoria_caracteristica(cat_id,car_id,orden,estado) values 
							('".$llave."','".$caracteristica."','".$_POST['orden'][$i]."','".$_POST['estado'][$i]."')";

			$conec->ejecutar($sql);
			$i++;
		}		
		
		
		$mensaje='Categoria Agregada Correctamente!!!';

		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
		
	}
	
	
	
	function modificar_tcp()
	{
		$conec= new ADO();	
			
		$sql="update categoria set 
								cat_nombre='".$_POST['cat_nombre']."'
								
								where cat_id = '".$_GET['id']."'";

		$conec->ejecutar($sql);
		
		$sql="delete from categoria_caracteristica where cat_id='".$_GET['id']."'";
		$conec->ejecutar($sql);
		
		$caracteristicas=$_POST['caracteristicas'];
		$i=0;			
		
		foreach($caracteristicas as $caracteristica)
		{	
           
			$sql="insert into categoria_caracteristica(cat_id,car_id,orden,estado) values 
							('".$_GET['id']."','".$caracteristica."','".$_POST['orden'][$i]."','".$_POST['estado'][$i]."')";

			$conec->ejecutar($sql);
			$i++;
		}	

		$mensaje='Categoria Modificada Correctamente!!!';

		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
		
	}
	
	function formulario_confirmar_eliminacion()
	{
		
		$mensaje='Esta seguro de eliminar la categoria?';
		
		$this->formulario->ventana_confirmacion($mensaje,$this->link."?mod=$this->modulo".'&tarea=ELIMINAR','cat_id');
	}
	
	function eliminar_tcp()
	{
		
			$llave=$_POST['cat_id'];		
			
			$conec= new ADO();
			
			$sql="delete from categoria where cat_id='".$_POST['cat_id']."'";
			
			$conec->ejecutar($sql);
			
			$sql="delete from categoria_caracteristica where cat_id='".$_POST['cat_id']."'";
		    echo $sql;
		    $conec->ejecutar($sql);	
			
			$mensaje='Categoria Eliminada Correctamente!!!';
			
		
		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
	}
	
	function cargar_filas_detalle($id)
	{
		$conec= new ADO();
	
		$sql="select categoria_caracteristica.cat_id,categoria_caracteristica.car_id,car_descripcion,orden,estado from categoria_caracteristica inner join caracteristica on (categoria_caracteristica.car_id = caracteristica.car_id) where categoria_caracteristica.cat_id='$id' order by orden";		
		
		$conec->ejecutar($sql);
		
		$num=$conec->get_num_registros();
		
		$cad="";
		
		$fun=NEW FUNCIONES;		
		
		for($i=0;$i<$num;$i++)
		{
			
			$objeto=$conec->get_objeto();
			
			?>
			<tr>
			   <td width='5%'><? echo $objeto->orden;?><input type='hidden' name='orden[]' value='<? echo $objeto->orden;?>'></td>
			   <td><input type='hidden' name='caracteristicas[]' value='<? echo $objeto->car_id;?>'><? echo $objeto->car_descripcion;?></td>
			   <td><select name='estado[]' class='caja_texto' style='width:60px'><option value='Si' <? if ($objeto->estado=='Si') echo 'Selected' ?>>Si</option><option value='No' <? if ($objeto->estado=='No') echo 'Selected' ?>>No</option></select></td>		   
			   <td><img src='images/b_drop.png' class='borrar-detalle' style='cursor:pointer'></td>
			</tr>
			
			
			
			<?php
			
			$conec->siguiente();
		}
		
	}
}
?>