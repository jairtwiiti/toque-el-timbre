<?php

class ZONA extends BUSQUEDA 
{
	var $formulario;
	var $mensaje;
	
	function ZONA()
	{
		//permisos
		$this->ele_id=139;
		
		$this->busqueda();
		
		if(!($this->verificar_permisos('AGREGAR')))
		{
			$this->ban_agregar=false;
		}
		//fin permisos
		
		$this->num_registros=14;
		
		$this->coneccion= new ADO();
		
		$this->arreglo_campos[0]["nombre"]="zon_nombre";
		$this->arreglo_campos[0]["texto"]="Nombre";
		$this->arreglo_campos[0]["tipo"]="cadena";
		$this->arreglo_campos[0]["tamanio"]=30;
		
		$this->link='gestor.php';
		
		$this->modulo='zona';
		
		$this->formulario = new FORMULARIO();
		
		$this->formulario->set_titulo('ZONA');
		
		
	}
	
	
	function dibujar_busqueda()
	{
		
		$this->formulario->dibujar_cabecera();
		
		$this->dibujar_listado();
	}
	
		
	function set_opciones()
	{
				
		$nun=0;
		
		if($this->verificar_permisos('VER'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='VER';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_search.png';
			$this->arreglo_opciones[$nun]["Nombre (es)"]='VER';
			$nun++;
		}
		
		if($this->verificar_permisos('MODIFICAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='MODIFICAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_edit.png';
			$this->arreglo_opciones[$nun]["Nombre (es)"]='MODIFICAR';
			$nun++;
		}
		
		if($this->verificar_permisos('ELIMINAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='ELIMINAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_drop.png';
			$this->arreglo_opciones[$nun]["Nombre (es)"]='ELIMINAR';
			$nun++;
		}
	}
	
	function dibujar_listado()
	{
		$sql="SELECT zon_id,zon_nombre,zon_dep_id, dep_nombre FROM zona INNER JOIN departamento ON (zon_dep_id = dep_id) ";
		
		$this->set_sql($sql,'');
		
		$this->set_opciones();
		
		$this->dibujar();
		
	}
	
	function dibujar_encabezado()
	{
		?>
			<tr>
				<th>Nombre</th>
				<th>Departamento</th>
	            <th class="tOpciones" width="100px">Opciones</th>
			</tr>
			
		<?PHP
	}
	
	function mostrar_busqueda()
	{
		$conversor = new convertir();
		
		for($i=0;$i<$this->numero;$i++)
			{
				
				$objeto=$this->coneccion->get_objeto();
				echo '<tr>';
					echo "<td>";
						echo $objeto->zon_nombre;
					echo "&nbsp;</td>";
					echo "<td>";
						echo $objeto->dep_nombre;
					echo "&nbsp;</td>";
					echo "<td>";
						echo $this->get_opciones($objeto->zon_id);
					echo "</td>";
				echo "</tr>";
				
				$this->coneccion->siguiente();
			}
	}
	
	function cargar_datos()
	{
		$conec=new ADO();
		
		$sql="select * from zona
				where zon_id = '".$_GET['id']."'";
		
		$conec->ejecutar($sql);
		
		$objeto=$conec->get_objeto();
		 	 	 	 	 	 	 	 	 
		$_POST['zon_nombre']=$objeto->zon_nombre;		
		$_POST['zon_dep_id']=$objeto->zon_dep_id;		
		$_POST['zon_latitud']=$objeto->zon_latitud;		
		$_POST['zon_longitud']=$objeto->zon_longitud;		
	}
	
	function datos()
	{
		if($_POST)
		{
			//texto,  numero,  real,  fecha,  mail.
			$num=0;
			$valores[$num]["etiqueta"]="Nombre";
			$valores[$num]["valor"]=$_POST['zon_nombre'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			$valores[$num]["etiqueta"]="Departamento";
			$valores[$num]["valor"]=$_POST['zon_dep_id'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			$valores[$num]["etiqueta"]="Ubicacion Mapa";
			$valores[$num]["valor"]=$_POST['zon_latitud'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			
			
			$val=NEW VALIDADOR;
			
			$this->mensaje="";
			
			if($val->validar($valores))
			{
				return true;
			}
				
			else
			{
				$this->mensaje=$val->mensaje;
				return false;
			}
		}
			return false;
	}
	
	function formulario_tcp($tipo)
	{
				switch ($tipo)
				{
					case 'ver':{
								$ver=true;
								break;
								}
							
					case 'cargar':{
								$cargar=true;
								break;
								}
				}
				
				$url=$this->link.'?mod='.$this->modulo;
				
				$red=$url.'&tarea=ACCEDER';
				
				if(!($ver))
				{
					$url.="&tarea=".$_GET['tarea'];
				}
				
				if($cargar)
				{
					$url.='&id='.$_GET['id'];
				}

		
		    $this->formulario->dibujar_tarea('zona');
		
			if($this->mensaje<>"")
			{
				$this->formulario->mensaje('Error',$this->mensaje);
			}
			?>
			
		<script type="text/javascript">
		   var map;
		   var markers = [];
		   var geocoder;
		   var zooms = 5;		   
		   function initialize() {
				geocoder = new google.maps.Geocoder();				
				var myLatlng = new google.maps.LatLng(-16.930705,-64.782716); 		
				
				var myOptions = {
					zoom: zooms,
					center: myLatlng,
					mapTypeId: google.maps.MapTypeId.ROADMAP
				}
				
				map = new google.maps.Map(document.getElementById("map"), myOptions);						
				
				<?php
				    if (isset($_POST['zon_latitud']) && isset($_POST['zon_longitud']))
				    {				       
				?>
				       var mypoint = new google.maps.LatLng(<? echo $_POST['zon_latitud']; ?>,<? echo $_POST['zon_longitud']; ?>);					 
					   createMarker(mypoint);	
					   map.setCenter(mypoint);			
					   map.setZoom(15);	
				<?php
				    }
				?>
				
				google.maps.event.addListener(map, 'click', function(event) {
				
					if  (markers.length < 1)
					{	
						createMarker(event.latLng);							
					}   
					else
					{	
						markers[0].setPosition(event.latLng);
						actualizar_punto(markers[0]);								 
					}	
						
				});
				
				
		   }
		   
		   function actualizar_punto(point)
		   {
		        document.getElementById("latitud").value  =	point.getPosition().lat();	
				document.getElementById("longitud").value = point.getPosition().lng();	
		   }
		   
		   function createMarker(point) {
				
				var clickedLocation = new google.maps.LatLng(point);
				
				markers.push(new google.maps.Marker({
				  position: point,
				  map: map,
				  draggable: true,
				  animation: google.maps.Animation.DROP
				}));  
				
				actualizar_punto(markers[0]);				
				
				google.maps.event.addListener(markers[0], 'dragend', function() {						
					actualizar_punto(markers[0]);							
				});	
				  
			}

			function codeAddress() {
				
				var pais    = document.getElementById("map_pais").value;		
				//var ciudad  = document.getElementById("map_ciudad").value;
				//var tipo  = document.getElementById("map_tipo").value;
				var direccion = document.getElementById("map_direccion").value;
				var address;
				
				
				address = direccion+","+pais;	
				
				
				geocoder.geocode( { 'address': address}, function(results, status) {
				  if (status == google.maps.GeocoderStatus.OK) {
					map.setCenter(results[0].geometry.location);			
					map.setZoom(12);
					if (direccion != "")
					   map.setZoom(15);
					
					if  (markers.length < 1)
					{	
						createMarker(results[0].geometry.location);
					}
					else
					{
						markers[0].setPosition(results[0].geometry.location);
					}			
					
				  } else {
					alert("LA GEOPOSICION NO SE HA ENCONTRADO POR LA SIGUIENTE RAZON: " + status);
				  }
				});
			}	
			
		  
			function loadScript() {
			  var script = document.createElement("script");
			  script.type = "text/javascript";
			  script.src = "http://maps.google.com/maps/api/js?sensor=false&callback=initialize";
			  document.body.appendChild(script);
			}
			  
			window.onload = loadScript;
			
			$(document).ready(function() {
				$('.cerrarToogle').hide();				
				$('.cerrarToogle:first').show();
				$('.prueba').click(function(){
					var cat = $(this).parent().find('.cerrarToogle');
					if(cat.is(':visible')){
						$(cat).hide();
					} else {
						$(cat).slideDown('normal');
					};
				});	
			});
	
</script>	
			
		<div id="Contenedor_NuevaSentencia">
			<form id="frm_sentencia" name="frm_sentencia" action="<?php echo $url;?>" method="POST" enctype="multipart/form-data">  
				<div id="FormSent">
				  
					<div class="Subtitulo">Datos</div>
						<div id="ContenedorSeleccion">
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">*</span>Nombre</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto" name="zon_nombre" id="zon_nombre" maxlength="250"  size="30" value="<?php echo $_POST['zon_nombre'];?>">
							   </div>
							</div>
							<!--Fin-->						
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">* </span>Departamento</div>
							   <div id="CajaInput">
							   <select name="zon_dep_id" class="caja_texto" >
							   <option value="">Seleccione</option>
							   <?php 		
								$fun=NEW FUNCIONES;		
								$fun->combo("select dep_id as id,dep_nombre as nombre from departamento",$_POST['zon_dep_id']);				
								?>
							   </select>
							   </div>
							   
							</div>
							<!--Fin-->
							<!--Inicio-->
							<br/>
							<fieldset style='overflow:hidden;'>
							<legend class='prueba' id='mapaCargar'>&nbsp;Ubicaci�n&nbsp;</legend>
							<div class='cerrarToogle'>							
								<div id="ContenedorDiv">							  
									<div id="CajaInput">
								   <div style="padding:10px 0px 10px 15px">						   
										<input id="latitud" name="zon_latitud" type="hidden" value="<?php echo $_POST['zon_latitud'];?>">
										<input id="longitud" name="zon_longitud" type="hidden" value = "<?php echo $_POST['zon_longitud'];?>">									
										<input id="map_pais" name="map_pais" type="hidden" value="Bolivia">
										<table border="0" cellspacing="2">
											<tr>										
												<td><div style="color: #005C89; font-size:12px" >Direccion</div></td>
												<td>&nbsp;</td>
											</tr>
											<tr> 
												
												<td><input id="map_direccion" type="textbox" size="35" ></td>
												<td>
													<input type="button" class="boton" style="width:100px" value="Buscar Direcci�n" onclick="codeAddress()">
												</td>
											</tr>
										</table>									
								  </div>
									<div id="map" style="width: 500px; height: 400px;"></div>
								   </div>
								</div>								
							</div>	
							</fieldset>
							<!--Fin-->
							
						</div>
					
						<div id="ContenedorDiv">
						   <div id="CajaBotones">
								<center>
								<?php
								if(!($ver))
								{
									?>
									<input type="submit" class="boton" name="" value="Guardar">
									<input type="reset" class="boton" name="" value="Cancelar">
									<input type="button" class="boton" name="" value="Volver" onclick="javascript:location.href='<?php echo $red;?>';">
									<?php
								}
								else
								{
									?>
									<input type="button" class="boton" name="" value="Volver" onclick="javascript:location.href='<?php echo $red;?>';">
									<?php
								}
								?>
								</center>
						   </div>
						</div>
				</div>
			</form>
		</div>
		<?php
	}
	
	function insertar_tcp()
	{
		$conec= new ADO();				
		
		$sql="insert into zona (zon_nombre,zon_latitud,zon_longitud,zon_dep_id) values 
							('".$_POST['zon_nombre']."','".$_POST['zon_latitud']."','".$_POST['zon_longitud']."','".$_POST['zon_dep_id']."')";
							
		$conec->ejecutar($sql);
     
		$mensaje='Zona Agregado Correctamente!!!';
		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
		
	}
	
		
	function modificar_tcp()
	{
		$conec= new ADO();	
			
		$sql="update zona set 
					zon_nombre='".$_POST['zon_nombre']."',
					zon_latitud='".$_POST['zon_latitud']."',
					zon_longitud='".$_POST['zon_longitud']."',
					zon_dep_id='".$_POST['zon_dep_id']."'			
					where zon_id = '".$_GET['id']."'";
		
		$conec->ejecutar($sql);

		$mensaje='Zona Modificado Correctamente!!!';

		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
		
	}
	
	function formulario_confirmar_eliminacion()
	{
		
		$mensaje='Esta seguro de eliminar la Zona?';
		
		$this->formulario->ventana_confirmacion($mensaje,$this->link."?mod=$this->modulo".'&tarea=ELIMINAR','zon_id');
	}
	
	function eliminar_tcp()
	{	
		$llave=$_POST['zon_id'];	
		
		$conec= new ADO();		
		$sql="delete from zona where zon_id='".$_POST['zon_id']."'";		
		$conec->ejecutar($sql);		
		$mensaje='Zona Eliminado Correctamente!!!';		
		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
	}
}
?>