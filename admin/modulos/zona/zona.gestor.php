<?php
	
	require_once('zona.class.php');
	
	$zona = new ZONA();
	
	if(!($zona->verificar_permisos($_GET['tarea'])))
	{
		?>
		<script>
			location.href="log_out.php";
		</script>
		<?php
	}
	
	switch ($_GET['tarea'])
	{
		case 'AGREGAR':{
						
							if($zona->datos())
							{
								$zona->insertar_tcp();
							}
							else 
							{
								$zona->formulario_tcp('blanco');
							}
					
						
						
						break;}
		case 'VER':{
						$zona->cargar_datos();
												
						$zona->formulario_tcp('ver');
						
						break;}
		case 'MODIFICAR':{
							if($_GET['acc']=='Imagen')
							{
								$zona->eliminar_imagen();
							}
							else
							{
								if($zona->datos())
								{
									$zona->modificar_tcp();
								}
								else 
								{
									if(!($_POST))
									{
										$zona->cargar_datos();
									}
									$zona->formulario_tcp('cargar');
								}
							}
						break;}
		case 'ELIMINAR':{
							
								if(isset($_POST['zon_id']))
								{
									if(trim($_POST['zon_id'])<>"")
									{
										$zona->eliminar_tcp();
									}
									else 
									{
										$zona->dibujar_busqueda();
									}
								}
								else 
								{
									$zona->formulario_confirmar_eliminacion();
								}
							
							
						break;}
		case 'ACCEDER':{	
						
							$zona->dibujar_busqueda();
							break;
						}
		
	}
		
?>