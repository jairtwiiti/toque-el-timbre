<?php
	
	require_once('obras.class.php');
	
	require_once('imagenes.class.php');
	
	$obra = new OBRA();
	
	$imagenes = new IMAGENES();
	
	if(!($obra->verificar_permisos($_GET['tarea'])))
	{
		?>
		<script>
			location.href="log_out.php";
		</script>
		<?php
	}
	
	switch ($_GET['tarea'])
	{
		case 'AGREGAR':{
						
							if($obra->datos())
							{
								$obra->insertar_tcp();
							}
							else 
							{
								$obra->formulario_tcp('blanco');
							}
							
							break;
		}
		case 'VER':{
							$obra->cargar_datos();
													
							$obra->formulario_tcp('ver');
							
							break;
		}
		case 'MODIFICAR':{
							if($obra->datos())
							{
								$obra->modificar_tcp();
							}
							else 
							{
								if(!($_POST))
								{
									$obra->cargar_datos();
								}
								$obra->formulario_tcp('cargar');
							}
							break;
		}					
		case 'ELIMINAR':{
							
							$obra->eliminar_tcp();
								
							break;
		}
		case 'ACCEDER':{
							$obra->dibujar_busqueda();
							break;
			
		}
		case 'IMAGENES':{
						
							if($_GET['acc']=='ELIMINAR')
							{
								$imagenes->eliminar_tcp();
							}
							
							if(!($_GET['acc']<>""))
							{
								if($imagenes->datos())
								{
									$imagenes->insertar_tcp();
								}
								else 
								{
									$imagenes->formulario_tcp('blanco');
									
									$imagenes->dibujar_listado();
								}
																
							}
							
							break;
		}
	}
?>