<?php

class OBRA extends BUSQUEDA 
{
	var $formulario;
	var $mensaje;
	
	function OBRA()
	{
		//permisos
		$this->ele_id=133;
		
		$this->busqueda();
		
		if(!($this->verificar_permisos('AGREGAR')))
		{
			$this->ban_agregar=false;
		}
		//fin permisos
		
		$this->num_registros=14;
		
		$this->coneccion= new ADO();
		
		$this->arreglo_campos[0]["nombre"]="rec_blo_id";
		$this->arreglo_campos[0]["texto"]="Bloque";
		$this->arreglo_campos[0]["tipo"]="combosql";
		$this->arreglo_campos[0]["sql"]="select blo_id as codigo,blo_nombre as descripcion from bloque";
		
		$this->arreglo_campos[1]["nombre"]="rec_are_id";
		$this->arreglo_campos[1]["texto"]="Area";
		$this->arreglo_campos[1]["tipo"]="combosql";
		$this->arreglo_campos[1]["sql"]="select are_id as codigo,are_nombre as descripcion from area";
		
		$this->arreglo_campos[2]["nombre"]="rec_com_id";
		$this->arreglo_campos[2]["texto"]="Comunidad";
		$this->arreglo_campos[2]["tipo"]="combosql";
		$this->arreglo_campos[2]["sql"]="select com_id as codigo,com_nombre as descripcion from comunidad";
		
		$this->arreglo_campos[3]["nombre"]="obr_descripcion";
		$this->arreglo_campos[3]["texto"]="Descripci�n";
		$this->arreglo_campos[3]["tipo"]="cadena";
		$this->arreglo_campos[3]["tamanio"]=25;
		
		$this->link='gestor.php';
		
		$this->modulo='obras';
		
		$this->formulario = new FORMULARIO();
		
		$this->formulario->set_titulo('OBRA');
	}
	
	function dibujar_busqueda()
	{
		?>
		<script>		
		function ejecutar_script(id,tarea){
				var txt = 'Esta seguro de eliminar la obra?';
				
				$.prompt(txt,{ 
					buttons:{Si:true, No:false},
					callback: function(v,m,f){
						
						if(v){
								location.href='gestor.php?mod=obras&tarea='+tarea+'&id='+id;
						}
												
					}
				});
			}

		</script>
		<?php
		$this->formulario->dibujar_cabecera();
		
		$this->dibujar_listado();
	}
	
	function set_opciones()
	{
		$nun=0;
		
		if($this->verificar_permisos('VER'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='VER';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_search.png';
			$this->arreglo_opciones[$nun]["nombre"]='VER';
			$nun++;
		}
		
		if($this->verificar_permisos('MODIFICAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='MODIFICAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_edit.png';
			$this->arreglo_opciones[$nun]["nombre"]='MODIFICAR';
			$nun++;
		}
		
		if($this->verificar_permisos('IMAGENES'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='IMAGENES';
			$this->arreglo_opciones[$nun]["imagen"]='images/imagenes.png';
			$this->arreglo_opciones[$nun]["nombre"]='IMAGENES';
			$nun++;
		}
		
		if($this->verificar_permisos('ELIMINAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='ELIMINAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_drop.png';
			$this->arreglo_opciones[$nun]["nombre"]='ELIMINAR';
			$this->arreglo_opciones[$nun]["script"]="ok";
			$nun++;
		}
		
		
		
	}
	
	function dibujar_listado()
	{
		$sql="SELECT 
				obr_id,blo_nombre,are_nombre,obr_descripcion
			  FROM 
				obra inner join bloque on (obr_blo_id=blo_id)
				inner join area on (obr_are_id=are_id)
				
				";
		
		$this->set_sql($sql,' order by obr_id desc ');
		
		$this->set_opciones();
		
		$this->dibujar();
		
	}
	
	function dibujar_encabezado()
	{
		?>
			<tr>
	        	<th>Bloque</th>
				<th>Area</th>
				<th>Comunidades</th>
				<th>Descripci�n</th>
	            <th class="tOpciones" width="150px">Opciones</th>
			</tr>
		<?PHP
	}
	
	function mostrar_busqueda()
	{
		$conversor = new convertir();
		
		for($i=0;$i<$this->numero;$i++)
			{
				
				$objeto=$this->coneccion->get_objeto();
				echo '<tr>';
									
					echo "<td>";
						echo $objeto->blo_nombre;
					echo "</td>";
					echo "<td>";
						echo $objeto->are_nombre;
					echo "</td>";
					echo "<td>";
						echo  $this->comunidades($objeto->obr_id);
					echo "</td>";
						echo "<td>";
						echo $objeto->obr_descripcion;
					echo "</td>";
					echo "<td>";
						echo $this->get_opciones($objeto->obr_id);
					echo "</td>";
				echo "</tr>";
				
				$this->coneccion->siguiente();
			}
	}
	
	function cargar_datos()
	{
		$conec=new ADO();
		
		$sql="select 
			*  
		from obra
				where obr_id = '".$_GET['id']."'";
		
		$conec->ejecutar($sql);
		
		$objeto=$conec->get_objeto();
		
		$_POST['obr_id']=$objeto->obr_id;
		$_POST['rec_blo_id']=$objeto->obr_blo_id;
		$_POST['rec_are_id']=$objeto->obr_are_id;
		$_POST['rec_com_id']=$objeto->obr_com_id;
		$_POST['obr_descripcion']=$objeto->obr_descripcion;
		$_POST['obr_inicio']=$objeto->obr_inicio;
		$_POST['obr_fin']=$objeto->obr_fin;
		$_POST['obr_oru_id']=$objeto->obr_oru_id;
		$_POST['obr_monto']=$objeto->obr_monto;
	}
	
	function datos()
	{
		if($_POST)
		{
			//texto,  numero,  real,  fecha,  mail.
			$num=0;
			$valores[$num]["etiqueta"]="Bloque";
			$valores[$num]["valor"]=$_POST['rec_blo_id'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			$valores[$num]["etiqueta"]="Area";
			$valores[$num]["valor"]=$_POST['rec_are_id'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			$valores[$num]["etiqueta"]="Descripci�n";
			$valores[$num]["valor"]=$_POST['obr_descripcion'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			
			$val=NEW VALIDADOR;
			
			$this->mensaje="";
			
			if($val->validar($valores))
			{
				return true;
			}
				
			else
			{
				$this->mensaje=$val->mensaje;
				return false;
			}
		}
			return false;
		
	}
	
	function formulario_tcp($tipo)
	{
				
		switch ($tipo)
		{
			case 'ver':{
						$ver=true;
						break;
						}
					
			case 'cargar':{
						$cargar=true;
						break;
						}
		}
		
		$url=$this->link.'?mod='.$this->modulo;
		
		$red=$url.'&tarea=ACCEDER';
		
		if(!($ver))
		{
			$url.="&tarea=".$_GET['tarea'];
		}
		
		if($cargar)
		{
			$url.='&id='.$_GET['id'];
		}
		
		$this->formulario->dibujar_tarea('OBRA');
		
		if($this->mensaje<>"")
		{
			$this->formulario->mensaje('Error',$this->mensaje);
		}
		
			?>
			<script>
			
			function cargar_area(id)
			{

				var	valores="tarea=area2&blo="+id;			

				ejecutar_ajax('ajax.php','area',valores,'POST');
				
				cargar_comunidad(0);
				
				$('#tprueba tbody').remove();
				
				document.frm_sentencia.nfilas.value=0;
			}
			
			function cargar_comunidad(id)
			{

				var	valores="tarea=comunidad2&are="+id;			

				ejecutar_ajax('ajax.php','comunidad',valores,'POST');									

			}
			</script>
			<script type="text/javascript">
				
				function remove(row)
				{  	
				   var filas=parseFloat(document.frm_sentencia.nfilas.value);		
					
					document.frm_sentencia.nfilas.value=filas-1;
				   
				   var cant =  $(row).parent().parent().parent().children().length;  
				  
				    if (cant > 1)  
						$(row).parent().parent().parent().remove();	
					
				}


				function addTableRow(id,valor){ 	
					 $(id).append(valor);
				}

				function datos_fila()
				{
					
					var valor=document.frm_sentencia.rec_com_id.options[document.frm_sentencia.rec_com_id.selectedIndex].value;
					
					if(valor != '')
					{
						var valores=valor.split('|');
						
						if(verificar(valores[0]))
						{
							var filas=parseFloat(document.frm_sentencia.nfilas.value);		
					
							document.frm_sentencia.nfilas.value=filas+1;
							
							addTableRow('#tprueba','<tr><td><input name=\'comu[]\' type=\'hidden\' value=\''+valores[0]+'\'>'+valores[1]+'</td><td><center><img src=\'images/b_drop.png\' onclick=\'javascript:remove(this);\'></center></td></tr>');
						}
						else
						{
							$.prompt('La comunidad ya fue agredada a la obra',{ opacity: 0.8 });
						}
					}
					else
					{
						$.prompt('Seleccione la Comunidad',{ opacity: 0.8 });
					}	
				}
				
				function verificar(id)
				{
					var cant = $('#tprueba tbody').children().length;               
					var ban=true;
					if(cant > 0)
					{
						$('#tprueba tbody').children().each(function(){
						var dato=$(this).eq(0).children().eq(0).children().eq(0).attr('value');	
						
						if(id==dato)
						{
							ban=false;
						}
										 
						}); 				
					}  
					return ban;				
				}

				function enviar_formulario(){
					var descripcion=document.frm_sentencia.obr_descripcion.value;
					
					var nume=document.frm_sentencia.nfilas.value;
					
					if(nume > 0)
					{
						var descripcion=document.frm_sentencia.obr_descripcion.value;
						var rubro=document.frm_sentencia.obr_oru_id.value;
						
						if(rubro != '' && descripcion != '')
						{
							if(parseInt(document.frm_sentencia.obr_monto.value) > 0)
							{
								document.frm_sentencia.submit();
							}
							else
							{
								$.prompt('El monto esta vacio o el que ingreso es invalido',{ opacity: 0.8 });			   
							}
						}
						else
						{
							$.prompt('Seleccione el rubro y ingrese la descripci�n',{ opacity: 0.8 });			   
						}
					}
					else
					{
						$.prompt('Agregue al menos una comunidad',{ opacity: 0.8 });			   
					}
				}

				

				</script>
			<script type="text/javascript" src="js/ajax.js"></script>
			<div id="Contenedor_NuevaSentencia">
			<form id="frm_sentencia" name="frm_sentencia" action="<?php echo $url;?>" method="POST" enctype="multipart/form-data">  
				<div id="FormSent" style="width:90%;">
				  
						<div class="Subtitulo">Datos</div>
						<div id="ContenedorSeleccion">
							
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">* </span>Bloque</div>
							   <div id="CajaInput">
							   <select name="rec_blo_id" class="caja_texto" onchange="cargar_area(this.value);">
							   <option value="">Seleccione</option>
							   <?php 		
								$fun=NEW FUNCIONES;		
								$fun->combo("select blo_id as id,blo_nombre as nombre from bloque",$_POST['rec_blo_id']);				
								?>
							   </select>
							   </div>
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">* </span>Area</div>
							   <div id="CajaInput">
							   <div id="area">
							   <select name="rec_are_id" class="caja_texto"  onchange="cargar_comunidad(this.value);">
							   <option value="">Seleccione</option>
							   <?php 		
								if($_POST['rec_blo_id']<>"")
								{
									$fun=NEW FUNCIONES;		
									$fun->combo("select are_id as id,are_nombre as nombre from area where are_blo_id='".$_POST['rec_blo_id']."' ",$_POST['rec_are_id']);				
								}
								?>
							   </select>
							   </div>
							   </div>
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">* </span>Comunidad</div>
							   <div id="CajaInput">
							   <div id="comunidad">
							   <select name="rec_com_id" class="caja_texto" >
							   <option value="">Seleccione</option>
							   <?php 		
								if($_POST['rec_are_id']<>"")
								{
									$fun=NEW FUNCIONES;		
									$fun->combo("select concat(com_id,'|',com_nombre) as id,com_nombre as nombre from comunidad where com_are_id='".$_POST['rec_are_id']."' ",$_POST['rec_com_id']);				
								}
								?>
							   </select>
							   </div>
							   </div>
							   <div id="CajaInput">
								  <input type="hidden" name="nfilas" id="nfilas" value="0">
								  <img src="images/boton_agregar.png" style='margin:0px 0px 0px 10px' onclick="javascript:datos_fila();">
							   </div>
							   <div id="CajaInput" style="margin:0px 0px 0px 10px;">
								  <table  width="300"   class="tablaReporte" id="tprueba" cellpadding="0" cellspacing="0">
									<thead>
									<tr>
										<th>Comunidad</th>
										<th class="tOpciones">Eliminar</th>
									</tr>							
									</thead>
									<tbody>
									<?php
									if($_GET['tarea']=='MODIFICAR')
										$this->cargar_filas($_GET['id']);
									?>	
									</tbody>
									</table>
							   </div>
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">* </span>Rubro</div>
							   <div id="CajaInput">
							   <div id="area">
							   <select name="obr_oru_id" class="caja_texto">
							   <option value="">Seleccione</option>
							   <?php 		
								
								$fun=NEW FUNCIONES;		
								$fun->combo("select oru_id as id,oru_descripcion as nombre from obra_rubro",$_POST['obr_oru_id']);				
								
								?>
							   </select>
							   </div>
							   </div>
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">* </span>Descripci�n</div>
							   <div id="CajaInput">
							    <textarea rows="1" cols="50" name="obr_descripcion" id="obr_descripcion"><?php echo $_POST['obr_descripcion'];?></textarea>									
								 
							   </div>
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Fecha Inicio</div>
								 <div id="CajaInput">
									<input readonly="readonly" class="caja_texto" name="obr_inicio" id="obr_inicio" size="12" value="<?php echo $_POST['obr_inicio'];?>" type="text">
										<input name="but_fecha_pago" id="but_fecha_pago" class="boton_fecha" value="..." type="button">
										<script type="text/javascript">
														Calendar.setup({inputField     : "obr_inicio"
																		,ifFormat     :     "%Y-%m-%d",
																		button     :    "but_fecha_pago"
																		});
										</script>
								</div>		
							</div>
							<!--Fin-->	
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Fecha Fin</div>
								 <div id="CajaInput">
									<input readonly="readonly" class="caja_texto" name="obr_fin" id="obr_fin" size="12" value="<?php echo $_POST['obr_fin'];?>" type="text">
										<input name="but_fecha_pagoa" id="but_fecha_pagoa" class="boton_fecha" value="..." type="button">
										<script type="text/javascript">
														Calendar.setup({inputField     : "obr_fin"
																		,ifFormat     :     "%Y-%m-%d",
																		button     :    "but_fecha_pagoa"
																		});
										</script>
								</div>		
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">*</span>Monto</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto"  name="obr_monto" id="obr_monto"  maxlength="20" size="12"  value="<?php echo $_POST['obr_monto'];?>">&nbsp;$us
							   </div>
							</div>
							<!--Fin-->		
						</div>
						
						
					
						<div id="ContenedorDiv">
						   <div id="CajaBotones">
								<center>
								<?php
								if(!($ver))
								{
									?>
									<input type="button" class="boton" name="" value="Guardar"  onclick="javascript:enviar_formulario()">
									<input type="reset" class="boton" name="" value="Cancelar">
									<input type="button" class="boton" name="" value="Volver" onclick="javascript:location.href='<?php echo $red;?>';">
									<?php
								}
								else
								{
									?>
									<input type="button" class="boton" name="" value="Volver" onclick="javascript:location.href='<?php echo $red;?>';">
									<?php
								}
								?>
								</center>
						   </div>
						</div>
				</div>
			</form>
		</div>
		<?php
	}
	
	function insertar_tcp()
	{
		
		$conec= new ADO();
	
		$sql="insert into obra (
		obr_blo_id,
		obr_are_id,
		obr_oru_id,
		obr_descripcion,
		obr_inicio,
		obr_fin,
		obr_monto
		) 
		values (
		'".$_POST['rec_blo_id']."',
		'".$_POST['rec_are_id']."',
		'".$_POST['obr_oru_id']."',
		'".$_POST['obr_descripcion']."',
		'".$_POST['obr_inicio']."',
		'".$_POST['obr_fin']."',
		'".$_POST['obr_monto']."'
		)";
	
		$conec->ejecutar($sql,false);
		
		$llave=mysql_insert_id();
		
		$comunidades=$_POST['comu'];
		
		foreach($comunidades as $comunidad)
		{
			
			$sql="insert into obra_comunidades(oco_obr_id,oco_com_id) values 
							('".$llave."','".$comunidad."')";

			$conec->ejecutar($sql);
		}
		
		$mensaje='Obra Agregada Correctamente.';
		
		$tipo='Correcto';
		
		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER'.'�'.$this->link.'?mod='.$this->modulo.'&tarea=AGREGAR','Ir al listado�Continuar Agregando',$tipo);
	}
	
	function modificar_tcp()
	{
		
	
			$conec= new ADO();
		
			$codigo="";
						
			
			$sql="update obra set 
									obr_blo_id='".$_POST['rec_blo_id']."',
									obr_are_id='".$_POST['rec_are_id']."',
									obr_oru_id='".$_POST['obr_oru_id']."',
									obr_descripcion='".$_POST['obr_descripcion']."',
									obr_inicio='".$_POST['obr_inicio']."',
									obr_fin='".$_POST['obr_fin']."',
									obr_monto='".$_POST['obr_monto']."'
								where obr_id='".$_GET['id']."'";
			
			$conec->ejecutar($sql);
			
			$sql="delete from obra_comunidades where oco_obr_id='".$_GET['id']."'";

			$conec->ejecutar($sql);
			
			//
			$comunidades=$_POST['comu'];
		
			foreach($comunidades as $comunidad)
			{
				
				$sql="insert into obra_comunidades(oco_obr_id,oco_com_id) values 
								('".$_GET['id']."','".$comunidad."')";

				$conec->ejecutar($sql);
			}
			//
			
			$mensaje='Obra Modificada Correctamente';
			
			$tipo='correcto';
		
			$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER','',$tipo);
	}
	
	function eliminar_tcp()
	{
		$conec= new ADO();
	
		//eliminamos la imagenes//

		$sql="SELECT oim_id,oim_archivo 
		FROM 
		obra_imagen
		where 
		oim_obr_id='".$_GET['id']."'";

		$conec->ejecutar($sql);
		
		$num=$conec->get_num_registros();
		
		for($i=0;$i<$num;$i++)
		{
			
			$objeto=$conec->get_objeto();
			
			
			$mifile="imagenes/obra/".$objeto->oim_archivo;
		
			@unlink($mifile);
			
			$mifile2="imagenes/obra/chica/".$objeto->oim_archivo;
				
			@unlink($mifile2);
			
			
			$conec->siguiente();
		}
		
		$sql="delete FROM obra_imagen where oim_obr_id='".$_GET['id']."'";

		$conec->ejecutar($sql);
		
		$sql="delete FROM obra_comunidades where oco_obr_id='".$_GET['id']."'";

		$conec->ejecutar($sql);
		
		////
		
		$sql="delete from obra where obr_id='".$_GET['id']."'";
		 
		$conec->ejecutar($sql);
		
		$mensaje='Obra Eliminada Correctamente.';
		
		$tipo='Correcto';
		
		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER','',$tipo);
	}
	
	function comunidades($obr_id)
	{
		$conec= new ADO();
	
		$sql="select com_nombre from obra_comunidades inner join comunidad on(oco_obr_id='$obr_id' and oco_com_id=com_id) ";

		$conec->ejecutar($sql);
		
		$num=$conec->get_num_registros();
		
		$cad="";
		
		for($i=0;$i<$num;$i++)
		{
			
			$objeto=$conec->get_objeto();
			
			if($i==0)
				$cad.=$objeto->com_nombre;
			else
				$cad.=', '.$objeto->com_nombre;
			
			
			$conec->siguiente();
		}
		
		return $cad;
	}
	
	function cargar_filas($obr_id)
	{
		$conec= new ADO();
	
		$sql="select com_id,com_nombre from obra_comunidades inner join comunidad on(oco_obr_id='$obr_id' and oco_com_id=com_id) ";

		$conec->ejecutar($sql);
		
		$num=$conec->get_num_registros();
		
		$cad="";
		
		for($i=0;$i<$num;$i++)
		{
			
			$objeto=$conec->get_objeto();
			
			?>
			<tr>
				<td><input name="comu[]" type="hidden" value="<?php echo $objeto->com_id;?>"><?php echo $objeto->com_nombre;?></td>
				<td><center><img src="images/b_drop.png" onclick="javascript:remove(this);"></center></td>
			</tr>
			<?php
			
			$conec->siguiente();
		}
		?>
		<script>
		document.frm_sentencia.nfilas.value=<?php echo $num; ?>;
		</script>
		<?php
	}
}
?>