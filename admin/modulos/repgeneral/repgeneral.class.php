<?php

class REPBALANCE extends BUSQUEDA 
{
	var $formulario;
	var $mensaje;
	
	function REPBALANCE()
	{
		$this->coneccion= new ADO();
		
		$this->link='gestor.php';
		
		$this->modulo='repgeneral';
		
		$this->formulario = new FORMULARIO();
		
		$this->formulario->set_titulo('Anuncios');
	}
	
	
	function dibujar_busqueda()
	{
			$this->formulario();
	}
	
	function formulario()
	{	
		$this->formulario->dibujar_cabecera();
		$this->mostrar_reporte();
		$this->mostrar_reporte_usuario();
	}	
	
	
	function mostrar_reporte_usuario()
	{		
		$conec= new ADO();
		
		$bol=0;
		$dol=0;
		
		$pagina="'contenido_reporte1'";
		
		$page="'about:blank'";
		
		$extpage="'reportes'";
		
		$features="'left=100,width=800,height=500,top=0,scrollbars=yes'";
		
		$extra1="'<html><head><title>Vista Previa</title><head>
				<link href=css/estilos.css rel=stylesheet type=text/css />
			  </head>
			  <body>
			  <div id=imprimir>
			  <div id=status>
			  <p>";
		$extra1.=" <a href=javascript:window.print();>Imprimir</a> 
				  <a href=javascript:self.close();>Cerrar</a></td>
				  </p>
				  </div>
				  </div>
				  <center>'";
		$extra2="'</center></body></html>'"; 
		
		$myday = setear_fecha(strtotime(date('Y-m-d')));		
				
		?>		
				<?php echo '	<table align=right border=0><tr><td><a href="javascript:var c = window.open('.$page.','.$extpage.','.$features.');
				  c.document.write('.$extra1.');
				  var dato = document.getElementById('.$pagina.').innerHTML;
				  c.document.write(dato);
				  c.document.write('.$extra2.'); c.document.close();
				  ">
				<img src="images/printer.png" align="right" width="20" border="0" title="IMPRIMIR">
				</a></td><td><img src="images/back.png" align="right" width="20" border="0"  title="VOLVER" onclick="javascript:location.href=\'gestor.php?mod=repgeneral\';"></td></tr></table><br><br>
				';?>
						
				 

			<div id="contenido_reporte1" style="clear:both;";>
			<center>
			<table style="font-size:12px;" width="100%" cellpadding="5" cellspacing="0" >
				<tr>
					<td width="40%">
						<strong><?php echo 'ToqueElTimbre'; ?></strong><BR>
						<strong>Santa Cruz - Bolivia</strong><BR><BR>
					</td>
				   
					<td><p align="center" ><strong><h3>USUARIOS GENERAL</h3></strong><BR></p></td>
				    <td width="40%"><div align="right"><img src="imagenes/micro.png" width="" /></div></td>
				</tr>
				<tr>
					<td colspan="2">
					<strong>Impreso el: </strong> <?php echo $myday;?> <br><br></td>			
					<td></td>
				</tr>
				 
			</table>
			<table   width="90%"  class="tablaReporte" cellpadding="0" cellspacing="0">
				<thead>
					<tr>
						<th>
							<b></b>
						</th>
						<th>
							<b>Cantidad</b>
						</th>
					</tr>
				</thead>
				<tbody>
		<?php				
		
			$conec= new ADO();			
			$sql = "SELECT count(*) as cantidad from usuario";
			$conec->ejecutar($sql);		
			$objeto=$conec->get_objeto();
			echo '<tr>';			
					
				echo "<td>";
					echo "<b>Total</b>";
				echo "</td>";
					
				echo "<td>";
						echo "<b>". $objeto->cantidad ."</b>";
				echo "</td>";	
					
			echo "</tr>";
			
			
			$sql = "SELECT count(*) as cantidad from usuario where usu_estado='Habilitado'";
			$conec->ejecutar($sql);						
			$objeto=$conec->get_objeto();
			echo '<tr>';			
					
				echo "<td>";
					echo "<b>Habilitados</b>";
				echo "</td>";
					
				echo "<td>";
						echo "<b>". $objeto->cantidad ."</b>";
				echo "</td>";	
					
			echo "</tr>";
			
			$sql = "SELECT count(*) as cantidad from usuario where usu_estado='Deshabilitado'";
			$conec->ejecutar($sql);						
			$objeto=$conec->get_objeto();
			echo '<tr>';			
					
				echo "<td>";
					echo "<b>Deshabilitados</b>";
				echo "</td>";
					
				echo "<td>";
						echo "<b>". $objeto->cantidad ."</b>";
				echo "</td>";	
					
			echo "</tr>";
			
			$sql = "SELECT count(*) as cantidad from usuario where usu_tipo='Particular'";
			$conec->ejecutar($sql);						
			$objeto=$conec->get_objeto();
			echo '<tr>';			
					
				echo "<td>";
					echo "<b>Particulares</b>";
				echo "</td>";
					
				echo "<td>";
						echo "<b>". $objeto->cantidad ."</b>";
				echo "</td>";	
					
			echo "</tr>";
			
			
			$sql = "SELECT count(*) as cantidad from usuario where usu_tipo='Inmobiliaria'";
			$conec->ejecutar($sql);					
			$objeto=$conec->get_objeto();
			echo '<tr>';			
					
				echo "<td>";
					echo "<b>Inmobiliarias</b>";
				echo "</td>";
					
				echo "<td>";
						echo "<b>". $objeto->cantidad ."</b>";
				echo "</td>";	
					
			echo "</tr>";
			
		?>
	
		
		</tbody>
		</table>
		</center>
		<br><table align="right" border="0" style="font-size:12px;"><tr><td><b>Impreso: </b><?php echo setear_fecha(strtotime(date('Y-m-d'))).' '.date('H:i');?></td></tr></table>
		</div><br>
		<?php
	}
	
	
	
	function mostrar_reporte()
	{		
		$conec= new ADO();
		
		$bol=0;
		$dol=0;
		
		$pagina="'contenido_reporte'";
		
		$page="'about:blank'";
		
		$extpage="'reportes'";
		
		$features="'left=100,width=800,height=500,top=0,scrollbars=yes'";
		
		$extra1="'<html><head><title>Vista Previa</title><head>
				<link href=css/estilos.css rel=stylesheet type=text/css />
			  </head>
			  <body>
			  <div id=imprimir>
			  <div id=status>
			  <p>";
		$extra1.=" <a href=javascript:window.print();>Imprimir</a> 
				  <a href=javascript:self.close();>Cerrar</a></td>
				  </p>
				  </div>
				  </div>
				  <center>'";
		$extra2="'</center></body></html>'"; 
		
		$myday = setear_fecha(strtotime(date('Y-m-d')));		
				
		?>		
				<?php echo '	<table align=right border=0><tr><td><a href="javascript:var c = window.open('.$page.','.$extpage.','.$features.');
				  c.document.write('.$extra1.');
				  var dato = document.getElementById('.$pagina.').innerHTML;
				  c.document.write(dato);
				  c.document.write('.$extra2.'); c.document.close();
				  ">
				<img src="images/printer.png" align="right" width="20" border="0" title="IMPRIMIR">
				</a></td><td><img src="images/back.png" align="right" width="20" border="0"  title="VOLVER" onclick="javascript:location.href=\'gestor.php?mod=repgeneral\';"></td></tr></table><br><br>
				';?>
						
				 

			<div id="contenido_reporte" style="clear:both;";>
			<center>
			<table style="font-size:12px;" width="100%" cellpadding="5" cellspacing="0" >
				<tr>
					<td width="40%">
						<strong><?php echo 'ToqueElTimbre'; ?></strong><BR>
						<strong>Santa Cruz - Bolivia</strong><BR><BR>
					</td>
				   
					<td><p align="center" ><strong><h3>PUBLICACIONES GENERAL</h3></strong><BR></p></td>
				    <td width="40%"><div align="right"><img src="imagenes/micro.png" width="" /></div></td>
				</tr>
				<tr>
					<td colspan="2">
					<strong>Impreso el: </strong> <?php echo $myday;?> <br><br></td>			
					<td></td>
				</tr>
				 
			</table>
			<table   width="90%"  class="tablaReporte" cellpadding="0" cellspacing="0">
				<thead>
					<tr>
						<th>
							<b></b>
						</th>
						<th>
							<b>Cantidad</b>
						</th>
					</tr>
				</thead>
				<tbody>
		<?php				
		
			$conec= new ADO();			
			$sql = "SELECT count(*) as cantidad from publicacion";
			$conec->ejecutar($sql);		
			$objeto=$conec->get_objeto();
			echo '<tr>';			
					
				echo "<td>";
					echo "<b>Total</b>";
				echo "</td>";
					
				echo "<td>";
						echo "<b>". $objeto->cantidad ."</b>";
				echo "</td>";	
					
			echo "</tr>";
			
			
			$sql = "SELECT count(*) as cantidad from publicacion where pub_estado='Aprobado'";
			$conec->ejecutar($sql);						
			$objeto=$conec->get_objeto();
			echo '<tr>';			
					
				echo "<td>";
					echo "<b>Aprobados</b>";
				echo "</td>";
					
				echo "<td>";
						echo "<b>". $objeto->cantidad ."</b>";
				echo "</td>";	
					
			echo "</tr>";
			
			$sql = "SELECT count(*) as cantidad from publicacion where pub_estado='No Aprobado'";
			$conec->ejecutar($sql);						
			$objeto=$conec->get_objeto();
			echo '<tr>';			
					
				echo "<td>";
					echo "<b>No Aprobados</b>";
				echo "</td>";
					
				echo "<td>";
						echo "<b>". $objeto->cantidad ."</b>";
				echo "</td>";	
					
			echo "</tr>";
			
			$sql = "SELECT count(*) as cantidad from publicacion where pub_estado='Cancelado'";
			$conec->ejecutar($sql);						
			$objeto=$conec->get_objeto();
			echo '<tr>';			
					
				echo "<td>";
					echo "<b>Cancelado</b>";
				echo "</td>";
					
				echo "<td>";
						echo "<b>". $objeto->cantidad ."</b>";
				echo "</td>";	
					
			echo "</tr>";
			
			
			$fecha = date('Y-m-d');
			$sql = "SELECT count(*) as cantidad from publicacion where pub_estado='Aprobado' and  pub_vig_ini<='$fecha'  and pub_vig_fin>='$fecha'";
			$conec->ejecutar($sql);					
			$objeto=$conec->get_objeto();
			echo '<tr>';			
					
				echo "<td>";
					echo "<b>Vigentes</b>";
				echo "</td>";
					
				echo "<td>";
						echo "<b>". $objeto->cantidad ."</b>";
				echo "</td>";	
					
			echo "</tr>";
			
			
			$fecha = date('Y-m-d');
			$sql = "SELECT count(*) as cantidad from publicacion where pub_estado='Aprobado' and pub_vig_fin<'$fecha'";
			$conec->ejecutar($sql);						
			$objeto=$conec->get_objeto();
			echo '<tr>';			
					
				echo "<td>";
					echo "<b>Vencidos</b>";
				echo "</td>";
					
				echo "<td>";
						echo "<b>". $objeto->cantidad ."</b>";
				echo "</td>";	
					
			echo "</tr>";
		?>
	
		
		</tbody>
		</table>
		</center>
		<br><table align="right" border="0" style="font-size:12px;"><tr><td><b>Impreso: </b><?php echo setear_fecha(strtotime(date('Y-m-d'))).' '.date('H:i');?></td></tr></table>
		</div><br><br><br><br>
		<?php
	}
	
}
?>