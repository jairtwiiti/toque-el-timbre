<?php

class CONTENIDO extends BUSQUEDA 
{
	var $formulario;
	var $mensaje;
	
	function CONTENIDO()
	{
		//permisos
		$this->ele_id=168;
		
		$this->busqueda();
		
		if(!($this->verificar_permisos('AGREGAR')))
		{
			$this->ban_agregar=false;
		}
		//fin permisos
		
		$this->num_registros=14;
		
		$this->coneccion= new ADO();
		
		$this->arreglo_campos[0]["nombre"]="ban_id";
		$this->arreglo_campos[0]["texto"]="ID Banner";
		$this->arreglo_campos[0]["tipo"]="cadena";
		$this->arreglo_campos[0]["tamanio"]=40;
		
		$this->link='gestor.php';
		
		$this->modulo='banner_principal';
		
		$this->formulario = new FORMULARIO();
		
		$this->formulario->set_titulo('ANUNCIOS EN BANNER PRINCIPAL');
		
		
	}
	
	
	function dibujar_busqueda()
	{
		
		$this->formulario->dibujar_cabecera();
		
		$this->dibujar_listado();
	}
	
		
	function set_opciones()
	{
				
		$nun=0;
		
		if($this->verificar_permisos('VER'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='VER';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_search.png';
			$this->arreglo_opciones[$nun]["nombre"]='VER';
			$nun++;
		}
		
		if($this->verificar_permisos('MODIFICAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='MODIFICAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_edit.png';
			$this->arreglo_opciones[$nun]["nombre"]='MODIFICAR';
			$nun++;
		}
		
		if($this->verificar_permisos('ELIMINAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='ELIMINAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_drop.png';
			$this->arreglo_opciones[$nun]["nombre"]='ELIMINAR';
			$nun++;
		}
	}
	
	function dibujar_listado()
	{
		$sql="SELECT * FROM banner_principal";
		
		$this->set_sql($sql,'');
		
		$this->set_opciones();
		
		$this->dibujar();
		
	}
	
	function dibujar_encabezado()
	{
		?>
			<tr>
	        	<th width="80px">ID Inmueble</th>
                <th>T�tulo Inmueble</th>
                <th width="100px">Url</th>
                <th width="100px">Categoria</th>
                <th width="100px">Estado</th>
				<th class="tOpciones" width="100px">Opciones</th>
			</tr>
			
		<?PHP
	}
	
	function mostrar_busqueda()
	{
		$conversor = new convertir();
		
		for($i=0;$i<$this->numero;$i++)
			{
				
				$objeto=$this->coneccion->get_objeto();
				echo '<tr>';
					
					echo "<td>";
						echo $objeto->ban_inm_id;
					echo "&nbsp;</td>";									
					echo "<td>";
						echo $this->obtener_inmueble_id($objeto->ban_inm_id);
					echo "&nbsp;</td>";
					echo "<td style='text-align:center;'>";
						echo "<a href='".$this->obtener_url_inmueble($objeto->ban_inm_id)."' target='_blank'>Ver Inmueble</a>";
					echo "&nbsp;</td>";
					echo "<td style='text-align:center;'>";
						echo $objeto->ban_tipo;
					echo "&nbsp;</td>";
					echo "<td style='text-align:center;'>";
						echo $objeto->ban_estado;
					echo "&nbsp;</td>";
					echo "<td>";
						echo $this->get_opciones($objeto->ban_id);
					echo "</td>";
				echo "</tr>";
				
				$this->coneccion->siguiente();
			}
	}
	
	
	function obtener_inmueble_id($inm_id)
	{
		$conec=new ADO();
		
		$sql="select inm_nombre from inmueble
				where inm_id = ".$inm_id;
		
		$conec->ejecutar($sql);
		
		$objeto=$conec->get_objeto();
		 	 	 	 	 	 	 	 	 
		return $objeto->inm_nombre;
	}
	
	function obtener_url_inmueble($inm_id)
	{
		$conec=new ADO();
		
		$sql="
		SELECT * FROM inmueble 
		INNER JOIN ciudad ON ciu_id = inm_ciu_id
		INNER JOIN categoria ON cat_id = inm_cat_id
		INNER JOIN forma ON for_id = inm_for_id
		WHERE inm_id = ".$inm_id;
		
		$conec->ejecutar($sql);		
		$objeto=$conec->get_objeto();
		
		
		include_once('../class.SEO.php');
		$seo = new SEO();
		$ciudad_seo = $seo->scapeURL($objeto->ciu_nombre);	
		
		$url = "http://www.toqueeltimbre.com/".$ciudad_seo."/".$this->forma_categoria($objeto->for_descripcion,$objeto->cat_nombre)."/".$objeto->inm_seo.".html";
		 	 	 	 	 	 	 
		return $url;
	}
	
	function forma_categoria($forma, $categoria){
		$res =  $categoria. ' en ' . $forma ; 	
		$res = strtolower($res);
		$res = str_replace(" ", "-", $res);	
		return $res;
	}
	
	
	function cargar_datos()
	{
		$conec=new ADO();
		
		$sql="select * from banner_principal
				where ban_id = '".$_GET['id']."'";
		
		$conec->ejecutar($sql);
		
		$objeto=$conec->get_objeto();
		 	 	 	 	 	 	 	 	 
		$_POST['ban_id']=$objeto->ban_id;		
		$_POST['ban_inm_id']=$objeto->ban_inm_id;		
		$_POST['ban_tipo']=$objeto->ban_tipo;
		$_POST['ban_estado']=$objeto->ban_estado;
	}
	
	function datos()
	{
		if($_POST)
		{
			//texto,  numero,  real,  fecha,  mail.
			$num=0;
			/*$valores[$num]["etiqueta"]="T�tulo";
			$valores[$num]["valor"]=$_POST['con_titulo'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;*/
			
			
			$val=NEW VALIDADOR;
			
			$this->mensaje="";
			
			if($val->validar($valores))
			{
				return true;
			}
				
			else
			{
				$this->mensaje=$val->mensaje;
				return false;
			}
		}
			return false;
	}
	
	function formulario_tcp($tipo)
	{
				switch ($tipo)
				{
					case 'ver':{
								$ver=true;
								break;
								}
							
					case 'cargar':{
								$cargar=true;
								break;
								}
				}
				
				$url=$this->link.'?mod='.$this->modulo;
				
				$red=$url.'&tarea=ACCEDER';
				
				if(!($ver))
				{
					$url.="&tarea=".$_GET['tarea'];
				}
				
				if($cargar)
				{
					$url.='&id='.$_GET['id'];
				}

		
		    $this->formulario->dibujar_tarea('CONTENIDO');
		
			if($this->mensaje<>"")
			{
				$this->formulario->dibujar_mensaje($this->mensaje);
			}
			?>
		<div id="Contenedor_NuevaSentencia">
			<form id="frm_sentencia" name="frm_sentencia" action="<?php echo $url;?>" method="POST" enctype="multipart/form-data">  
				<div id="FormSent">
				  
					<div class="Subtitulo">Datos</div>
						<div id="ContenedorSeleccion">
							
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">* </span>Codigo del Inmueble</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto" name="ban_inm_id" id="ban_inm_id" maxlength="250"  size="60" value="<?php echo $_POST['ban_inm_id'];?>">
							   </div>
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Categoria en Banner Principal</div>
							   <div id="CajaInput">
							   <select name="ban_tipo">
                               		<option value="A" <?php $_POST['ban_inm_id'] == "A" ? 'selected="selected"':""; ?>>Categoria A</option>
                                    <option value="B" <?php $_POST['ban_inm_id'] == "B" ? 'selected="selected"':""; ?>>Categoria B</option>
                                    <option value="C" <?php $_POST['ban_inm_id'] == "C" ? 'selected="selected"':""; ?>>Categoria C</option>
                               </select>
							   </div>
							</div>
							<!--Fin-->
                            
                            <!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Estado</div>
							   <div id="CajaInput">
							   <select name="ban_estado">
                               		<option value="Visible" <?php $_POST['ban_estado'] == "Visible" ? 'selected="selected"':""; ?>>Visible</option>
                                    <option value="No Visible" <?php $_POST['ban_estado'] == "No Visible" ? 'selected="selected"':""; ?>>No Visible</option>
                               </select>
							   </div>
							</div>
							<!--Fin-->
							
						</div>
					
						<div id="ContenedorDiv">
						   <div id="CajaBotones">
								<center>
								<?php
								if(!($ver))
								{
									?>
									<input type="submit" class="boton" name="" value="Guardar">
									<input type="reset" class="boton" name="" value="Cancelar">
									<input type="button" class="boton" name="" value="Volver" onclick="javascript:location.href='<?php echo $red;?>';">
									<?php
								}
								else
								{
									?>
									<input type="button" class="boton" name="" value="Volver" onclick="javascript:location.href='<?php echo $red;?>';">
									<?php
								}
								?>
								</center>
						   </div>
						</div>
				</div>
			</form>
		</div>
		<?php
	}
	
	function insertar_tcp()
	{
		$conec= new ADO();
		
		$sql="insert into banner_principal(ban_inm_id,ban_tipo,ban_estado) values 
							(".$_POST['ban_inm_id'].",'".$_POST['ban_tipo']."','".$_POST['ban_estado']."')";

		$conec->ejecutar($sql);
		
		$mensaje='Agregado Correctamente!!!';

		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
		
	}
	
	function modificar_tcp()
	{
		$conec= new ADO();	
		
		$sql="update banner_principal set 
						ban_inm_id=".$_POST['ban_inm_id'].",
						ban_tipo='".$_POST['ban_tipo']."',
						ban_estado='".$_POST['ban_estado']."'
						where ban_id = '".$_GET['id']."'";

		//echo $sql;
		$conec->ejecutar($sql);

		$mensaje='Modificado Correctamente!!!';

		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
		
	}
	
	function formulario_confirmar_eliminacion()
	{
		
		$mensaje='Esta seguro de eliminar?';
		
		$this->formulario->ventana_confirmacion($mensaje,$this->link."?mod=$this->modulo".'&tarea=ELIMINAR','ban_id');
	}
	
	function eliminar_tcp()
	{
		$conec= new ADO();
		
		$sql="delete from banner_principal where ban_id='".$_POST['ban_id']."'";
		
		$conec->ejecutar($sql);
		
		$mensaje='Eliminada Correctamente!!!';
			
		
		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
	}
	
}
?>