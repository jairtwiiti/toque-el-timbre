<?php

class MONEDA extends BUSQUEDA 
{
	var $formulario;
	var $mensaje;
	
	function MONEDA()
	{
		//permisos
		$this->ele_id=140;
		
		$this->busqueda();
		
		if(!($this->verificar_permisos('AGREGAR')))
		{
			$this->ban_agregar=false;
		}
		//fin permisos
		
		$this->num_registros=14;
		
		$this->coneccion= new ADO();
		
		$this->arreglo_campos[0]["nombre"]="mon_descripcion";
		$this->arreglo_campos[0]["texto"]="Descripción";
		$this->arreglo_campos[0]["tipo"]="cadena";
		$this->arreglo_campos[0]["tamanio"]=40;
		
		
		$this->link='gestor.php';
		
		$this->modulo='moneda';
		
		$this->formulario = new FORMULARIO();
		
		$this->formulario->set_titulo('MONEDA');
		
		
	}
		
	function dibujar_busqueda()
	{
		
		$this->formulario->dibujar_cabecera();
		
		$this->dibujar_listado();
	}
	
		
	function set_opciones()
	{
				
		$nun=0;
		
		if($this->verificar_permisos('VER'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='VER';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_search.png';
			$this->arreglo_opciones[$nun]["nombre"]='VER';
			$nun++;
		}
		
		if($this->verificar_permisos('MODIFICAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='MODIFICAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_edit.png';
			$this->arreglo_opciones[$nun]["nombre"]='MODIFICAR';
			$nun++;
		}
		
		if($this->verificar_permisos('ELIMINAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='ELIMINAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_drop.png';
			$this->arreglo_opciones[$nun]["nombre"]='ELIMINAR';
			$nun++;
		}
	}
	
	function dibujar_listado()
	{
		$sql="SELECT * FROM moneda";
		
		$this->set_sql($sql);
		
		$this->set_opciones();
		
		$this->dibujar();
		
	}
	
	function dibujar_encabezado()
	{
		?>
			<tr>
	        	<th>Descripción</th>
				<th>Abreviatura</th>
				<th>Tipo</th>
	            <th class="tOpciones" width="100px">Opciones</th>
			</tr>
			
		<?PHP
	}
	
	function mostrar_busqueda()
	{
		$conversor = new convertir();
		
		for($i=0;$i<$this->numero;$i++)
			{
				
				$objeto=$this->coneccion->get_objeto();
				echo '<tr>';
									
					echo "<td>";
						echo $objeto->mon_descripcion;
					echo "&nbsp;</td>";
					
					echo "<td>";
						echo $objeto->mon_abreviado;
					echo "&nbsp;</td>";
					
					echo "<td>";
						if($objeto->mon_tipo=='1') echo 'Primaria'; else echo 'Secundaria';
					echo "</td>";
					
					echo "<td>";
						echo $this->get_opciones($objeto->mon_id);
					echo "</td>";
				echo "</tr>";
				
				$this->coneccion->siguiente();
			}
	}
	
	function cargar_datos()
	{
		$conec=new ADO();
		
		$sql="select * from moneda
				where mon_id = '".$_GET['id']."'";
		
		$conec->ejecutar($sql);
		
		$objeto=$conec->get_objeto();
		
		$_POST['mon_descripcion']=$objeto->mon_descripcion;
		
		$_POST['mon_abreviado']=$objeto->mon_abreviado;
		
		$_POST['mon_tipo']=$objeto->mon_tipo;
	}
	
	function datos()
	{
		if($_POST)
		{
			//texto,  numero,  real,  fecha,  mail.
			$num=0;
			$valores[$num]["etiqueta"]="Descripción";
			$valores[$num]["valor"]=$_POST['mon_descripcion'];
			$valores[$num]["tipo"]="texto";
			$valores[$num]["requerido"]=true;
			$num++;
			$valores[$num]["etiqueta"]="Abreviatura";
			$valores[$num]["valor"]=$_POST['mon_abreviado'];
			$valores[$num]["tipo"]="texto";
			$valores[$num]["requerido"]=true;
			$num++;
			$valores[$num]["etiqueta"]="Tipo";
			$valores[$num]["valor"]=$_POST['mon_tipo'];
			$valores[$num]["tipo"]="numero";
			$valores[$num]["requerido"]=true;
			$num++;
			
			$val=NEW VALIDADOR;
			
			$this->mensaje="";
			
			if($val->validar($valores))
			{
				return true;
			}
				
			else
			{
				$this->mensaje=$val->mensaje;
				return false;
			}
		}
			return false;
	}
	
	function formulario_tcp($tipo)
	{
				switch ($tipo)
				{
					case 'ver':{
								$ver=true;
								break;
								}
							
					case 'cargar':{
								$cargar=true;
								break;
								}
				}
				
				$url=$this->link.'?mod='.$this->modulo;
				
				$red=$url;
				
				if(!($ver))
				{
					$url.="&tarea=".$_GET['tarea'];
				}
				
				if($cargar)
				{
					$url.='&id='.$_GET['id'];
				}

		
		    $this->formulario->dibujar_tarea();
		
			if($this->mensaje<>"")
			{
				$this->formulario->dibujar_mensaje($this->mensaje);
			}
			?>
		<div id="Contenedor_NuevaSentencia">
			<form id="frm_sentencia" name="frm_sentencia" action="<?php echo $url;?>" method="POST" enctype="multipart/form-data">  
				<div id="FormSent">
				  
					<div class="Subtitulo">Datos</div>
						<div id="ContenedorSeleccion">
							
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">* </span>Descripción</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto" name="mon_descripcion" id="mon_descripcion" size="30" maxlength="250" value="<?php echo $_POST['mon_descripcion'];?>">
							   </div>
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">* </span>Abreviatura</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto" name="mon_abreviado" id="mon_abreviado" size="10" maxlength="250" value="<?php echo $_POST['mon_abreviado'];?>">
							   </div>
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">* </span>Tipo</div>
							   <div id="CajaInput">
								    <select name="mon_tipo" class="caja_texto">
									<option value="" >Seleccione</option>
									<option value="1" <?php if($_POST['mon_tipo']=='1') echo 'selected="selected"'; ?>>Primaria</option>
									<option value="0" <?php if($_POST['mon_tipo']=='0') echo 'selected="selected"'; ?>>Secundaria</option>
									</select>
							   </div>
							</div>
							<!--Fin-->
							
							
						</div>
					
						<div id="ContenedorDiv">
						   <div id="CajaBotones">
								<center>
								<?php
								if(!($ver))
								{
									?>
									<input type="submit" class="boton" name="" value="Guardar">
									<input type="reset" class="boton" name="" value="Cancelar">
									<input type="button" class="boton" name="" value="Volver" onclick="javascript:location.href='<?php echo $red;?>';">
									<?php
								}
								else
								{
									?>
									<input type="button" class="boton" name="" value="Volver" onclick="javascript:location.href='<?php echo $red;?>';">
									<?php
								}
								?>
								</center>
						   </div>
						</div>
				</div>
			</form>
		</div>
		<?php
	}
	
	function insertar_tcp()
	{
		$conec= new ADO();
				
		$sql="insert into moneda(mon_descripcion,mon_abreviado,mon_tipo) values 
							('".$_POST['mon_descripcion']."','".$_POST['mon_abreviado']."','".$_POST['mon_tipo']."')";

		$conec->ejecutar($sql);

		$mensaje='Moneda Agregada Correctamente!!!';

		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo);
		
	}
	
	function modificar_tcp()
	{
		$conec= new ADO();
		
		$sql="update moneda set 
							mon_descripcion='".$_POST['mon_descripcion']."',
							mon_abreviado='".$_POST['mon_abreviado']."',
							mon_tipo='".$_POST['mon_tipo']."'
							where mon_id = '".$_GET['id']."'";

		$conec->ejecutar($sql);

		$mensaje='Moneda Modificada Correctamente!!!';

		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo);
		
	}
	
	function formulario_confirmar_eliminacion()
	{
		
		$mensaje='Esta seguro de eliminar la Moneda?';
		
		$this->formulario->ventana_confirmacion($mensaje,$this->link."?mod=$this->modulo",'mon_id');
	}
	
	function eliminar_tcp()
	{
		$conec= new ADO();
		
		$sql="delete from moneda where mon_id='".$_POST['mon_id']."'";
		
		$conec->ejecutar($sql);
		
		$mensaje='Moneda Eliminada Correctamente!!!';
		
		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo);
	}
}
?>