<?php

function highlightStr($haystack, $needle, $highlightColorValue) 
{
		 // return $haystack if there is no highlight color or strings given, nothing to do.
		if (strlen($highlightColorValue) < 1 || strlen($haystack) < 1 || strlen($needle) < 1) {
			return $haystack;
		}
		preg_match_all("/$needle+/i", $haystack, $matches);
		if (is_array($matches[0]) && count($matches[0]) >= 1) {
			foreach ($matches[0] as $match) {
				$haystack = str_replace($match, '<span style="background-color:'.$highlightColorValue.';">'.$match.'</span>', $haystack);
			}
		}
		return $haystack;
}

function resaltar($buscar, $texto) { 
    $claves = explode(" ",$buscar); 
    $clave = array_unique($claves);
    $num = count($clave); 
    for($i=0; $i < $num; $i++) 
        $texto = preg_replace("/(".trim($clave[$i]).")/i","<span style='background-color:#FF0000'>\\1</span>",$texto);
    return $texto; 
}

function reemplazar($text, $words){
        foreach ($words as $word){
                $word = preg_quote($word);
                $text = preg_replace("/\b($word)\b/i", '<strong">\1</strong>', $text);
        }
        return $text;
} 

$texto = "hola com� estas en este dia Tan especial como hoy";
$buscar = "como tan";

  //echo highlightStr($texto, $buscar, "#FF0000") ;
echo resaltar($buscar, $texto);
//$palabras_que_buscar = explode(" ",$buscar);
//echo reemplazar($texto, $palabras_que_buscar); 

?>