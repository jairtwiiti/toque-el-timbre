<?php

//**clases genericas**//

	require_once('clases/pagina.class.php');

	require_once("clases/busqueda.class.php");

	require_once("clases/formulario.class.php");

	require_once('clases/coneccion.class.php');

	require_once('clases/conversiones.class.php');
	
	require_once('clases/usuario.class.php');
	
	require_once('clases/validar.class.php');
	
	require_once('clases/verificar.php');
	
	require_once('clases/funciones.class.php');
	
	require("clases/mytime_int.php");

	//****//

	$modulo=$_GET['mod'];

	$tarea=$_GET['tarea'];

	$pagina=new PAGINA();
	
	$pagina->verificar();
		
	$pagina->abrir_contenido();
	
	$archivo="modulos/$modulo/$modulo.gestor.php";
	
	if(is_file($archivo))
		require_once($archivo);
	else
		echo "<br><br><center><b>NO EXISTE EL ARCHIVO</b> --> modulos/$modulo/$modulo.gestor.php</center>";

	$pagina->cerrar_contenido();

?>