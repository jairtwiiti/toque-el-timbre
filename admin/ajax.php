﻿<?php
   	require_once('clases/pagina.class.php');
	require_once("clases/busqueda.class.php");
	require_once("clases/formulario.class.php");
	require_once('clases/coneccion.class.php');
	require_once('clases/conversiones.class.php');
	require_once('clases/usuario.class.php');
	require_once('clases/validar.class.php');
	require_once('clases/verificar.php');
	require_once('clases/funciones.class.php');
	
	
	if (isset($_POST['tid']))
   {       
      cargar_ciudades();	 	
   }
   else
   {
       if (isset($_POST['thid']))
	        cargar_ciudades_inmueble();
		else
		    if (isset($_POST['tzid']))
				cargar_zonas();
			else
                if (isset($_POST['tcid']))	
                    cargar_caracteristicas($_POST['tcid']);				
   }
   
   function cargar_caracteristicas($cat_id)
	{
	    $conec= new ADO();
	
		$sql="select caracteristica.* from categoria_caracteristica as catcar INNER JOIN caracteristica ON (catcar.car_id = caracteristica.car_id) 
		where catcar.cat_id='$cat_id' order by catcar.orden";		

		$conec->ejecutar($sql);
		
		$num=$conec->get_num_registros();
		
		$cad="";
		
		$fun=NEW FUNCIONES;		
		
		for($i=0;$i<$num;$i++)
		{
			
			$objeto=$conec->get_objeto();
			
			switch ($objeto->car_tipo)
			{
			    case 'select':{		             
					 ?>
					 
					    <!--Inicio-->
						<div id="ContenedorDiv">
						   <div class="Etiqueta" ><? echo utf8_encode($objeto->car_descripcion);?></div>
						   <div id="CajaInput">							   
						   <select name="<? echo $objeto->car_nombre;?>" class="caja_texto">
						   <option value="">Seleccione</option>
						   <?php 	                                 							   
								$fun=NEW FUNCIONES;		
								$fun->combo("select det_key as id,det_valor as nombre from caracteristica_detalle where det_car_id=".$objeto->car_id." order by det_orden",$_POST[$objeto->car_nombre]);												
							?>
						   </select>							   
						   </div>
						   
						</div>
						<!--Fin-->
					 
					 
					 
					 <?php
					 break;
				}
				
				case 'input':{
				    ?>
					       <!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><? echo $objeto->car_descripcion;?></div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto" name="<? echo $objeto->car_nombre;?>" id="<? echo $objeto->car_nombre;?>" maxlength="250"  size="<? echo $objeto->car_tamano;?>" value="<?php echo $_POST[$objeto->car_nombre];?>">
							   </div>
							</div>
							<!--Fin-->	
					<?
		            break;
				}
				
				case 'textarea':{
				    ?>
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><? echo $objeto->car_descripcion;?></div>
							   <div id="CajaInput">
									<textarea rows="<? echo $objeto->car_tamano;?>" cols="40" name="<? echo $objeto->car_nombre;?>" id="<? echo $objeto->car_nombre;?>"><?php echo $_POST[$objeto->car_nombre];?></textarea>									
							   </div>
							</div>
							<!--Fin-->
				  
					
				    <?php    
		             break;
				}
			}
					
			?>
			
				
			
			<?php
			
			$conec->siguiente();
		}
	
	
	}


    function cargar_ciudades()
    {
	   $tco_id = $_POST['tid'];    
	   echo '<select name="usu_ciu_id" id="usu_ciu_id" class="caja_texto" >';
	   echo "<option value=''>Seleccione</option>";       	   
	   $fun=NEW FUNCIONES;			
	   $fun->combo("select ciu_id as id,ciu_nombre as nombre from ciudad where ciu_dep_id = $tco_id",$_POST['usu_ciu_id']);				        
       echo '</select>'  ;
	   
	}
	
	function cargar_zonas()
    {
	   $tco_id = $_POST['tzid'];    
	   echo '<select name="inm_zon_id" id="inm_zon_id" class="caja_texto" >';
	   echo "<option value=''>Seleccione</option>";       	   
	   $fun=NEW FUNCIONES;			
	   $fun->combo("select zon_id as id,zon_nombre as nombre from zona where zon_dep_id = $tco_id",$_POST['inm_zon_id']);				        
       echo '</select>'  ;
	   
	}
	
	function cargar_ciudades_inmueble()
    {
	   $tco_id = $_POST['thid'];    
	   echo '<select name="inm_ciu_id" id="inm_ciu_id" class="caja_texto" >';
	   echo "<option value=''>Seleccione</option>";       	   
	   $fun=NEW FUNCIONES;			
	   $fun->combo("select ciu_id as id,ciu_nombre as nombre from ciudad where ciu_dep_id = $tco_id",$_POST['inm_ciu_id']);				        
       echo '</select>'  ;
	   
	}
   if ($_POST['tarea']=='notas')
   {       
       cargar_notas();	 
   }
   
   if ($_POST['tarea']=='seccion')
   {       
       obtener_seccion(); 
   }
   
   function obtener_seccion()
   {
	   echo '<select name="not_sec_id" id="not_sec_id" class="caja_texto" >';

	   echo "<option value=''>Seleccione</option>";       	   

	   $fun=NEW FUNCIONES;			

	   $fun->combo("select sec_id as id,sec_nombre as nombre from seccion inner join periodico_seccion on (sec_id=pes_sec_id) where pes_prd_id='".$_POST['sec']."' ",'','ok');				        

       echo '</select>'  ;
   }
   
   function obtener_categorias($conec)
	{	    	
		//Aqui obtengo todas las palabras claves
		$sql = "SELECT cat_nombre,eca_tags 
		        FROM empresa_categoria INNER JOIN categoria ON (eca_cat_id = cat_id)
				WHERE eca_emp_id =".$_POST["emp_id"]; 
		
		$conec->ejecutar($sql);
		$num=$conec->get_num_registros();
		$categorias = array();
		
		for($i=0;$i<$num;$i++)
		{		 
		    $aux = array();
		    $objeto  = $conec->get_objeto();
            $aux['nombre'] = $objeto->cat_nombre;
            $aux['tags']   = explode(",",$objeto->eca_tags);            
     		$categorias[]  = $aux;	
			$conec->siguiente();
		}	
  	   
		return	$categorias;   	
	}
	
	 function obtener_palabras_claves($conec)
	{	    	
		//Aqui obtengo todas las palabras claves
		$sql = "SELECT cat_nombre,eca_tags 
		        FROM empresa_categoria INNER JOIN categoria ON (eca_cat_id = cat_id)
				WHERE eca_emp_id =".$_POST["emp_id"]; 
		
		$conec->ejecutar($sql);
		$num=$conec->get_num_registros();
		$palabras = array();
		
		for($i=0;$i<$num;$i++)
		{				    
		    $objeto  = $conec->get_objeto();  
			$aux  = explode(",",$objeto->eca_tags);
			$palabras = $palabras + $aux;
            //array_push($palabras,$aux);	   		
			$conec->siguiente();
		}	  	   
		return	$palabras;   	
	}
	
	function verificar_nota(&$cat,$nota)
	{
	  	foreach($cat as $key=>$categoria)
		{
		    if (is_array($categoria['tags']) && (count($categoria['tags']) > 0))
			{
			    /*verifica si alguna palabra clave se encuentra en la nota*/
				
			    foreach($categoria['tags'] as $palabra)
				{
				    //echo $palabra."<br>";
					if (substr_count($nota['notacompleta'],$palabra) > 0)
				    {                     
					   $cat[$key]['notas'][] = $nota;					   
					   break;
					}	   
				}	
                /*-----------------------------------------------------------------*/				
			}		
		}	
	}
	

	function resaltar($claves, $texto) { 		
		$clave = array_unique($claves);
		$num = count($clave); 
		for($i=0; $i < $num; $i++) 
			$texto = preg_replace("/(".trim($clave[$i]).")/i","<span style='background-color:#FFF29B; font-weight:bold'>\\1</span>",$texto);
		return $texto; 
    }

	
	function cargar_notas()
    {
	    $conec= new ADO();	
		$conec1= new ADO();	
		$conversor = new convertir();	
	
	    //Aqui obtengo todas las palabras claves
	    $categorias = obtener_categorias($conec);
		$palabras = obtener_palabras_claves($conec);		
		$sql = "SELECT emp_banner,emp_titulares,emp_resumen,emp_notacompleta FROM empresa WHERE emp_id =".$_POST["emp_id"];			
		$conec->ejecutar($sql);						
		$empresa = $conec->get_objeto();
        
		$sql = "SELECT distinct prd_id,prd_nombre,not_id,not_fecha,not_titulo,not_resumen,not_link,not_notacompleta,sec_nombre
				FROM empresa_periodico INNER JOIN periodico ON (epe_prd_id = prd_id and prd_estado = 'Habilitado') 
									   INNER JOIN noticias ON (not_prd_id = prd_id) 
                                       INNER JOIN seccion  ON (not_sec_id = sec_id)									   
				WHERE  epe_emp_id =".$_POST["emp_id"]." and not_fecha between '".$_POST["fecha_ini"]."' and '".$_POST["fecha_fin"]."'"; 	
		?>		
        <table   width="81%"  class="tablaReporte" cellpadding="0" cellspacing="5">
					<?php
						foreach($categorias as $key=>$cat)
						{
							if (is_array($cat['tags']) && (count($cat['tags']) > 0))
							{
							?>
								<thead>
								<tr>
									<th valign="MIDDLE" style="text-align:left; padding-left:10px">
										<b><? echo $cat['nombre'];?></b>
									</th>					
								</tr>
								</thead>
								<tbody>
							
							<?php
							
								$tags = implode("%' OR not_notacompleta LIKE '%",$cat['tags']);			
								$sqlaux = $sql." and (not_notacompleta LIKE '%".$tags."%')";
								//echo $sqlaux;
								$conec->ejecutar($sqlaux);						
								$num=$conec->get_num_registros();
								for($i=0;$i<$num;$i++)
								{
									$objeto=$conec->get_objeto();
									?>
										<tr>
											
											<td style="text-align:left; padding-left:10px">
												<table border="0" cellpadding="5" cellspacing="0">
												<tr>
													<td width="20%"><input type="checkbox" name='noticias[]' value="<? echo $objeto->not_id;?>"  ><? echo "<b>".$objeto->prd_nombre.":</b>"; ?></td>
													<td width="70%"><? echo "<h3><a href='".$objeto->not_link."' target='_blank'>".$objeto->not_titulo."</a><h3>"; ?></td>
													<td width="10%"><? echo $conversor->get_fecha_latina($objeto->not_fecha); ?></td>		
												</tr>
												<tr>
													
													<td colspan="3" align="justify">
														<?  //if ($empresa->emp_resumen==1)  
														     //  echo $objeto->not_resumen; 
															//else
															   echo resaltar($palabras,$objeto->not_notacompleta); 
														?>
													<td>
												</tr>
												<tr>
													<td>&nbsp;</td>
													<td colspan="2"><a href="<? echo $objeto->not_link; ?>" target="_blank"><? echo $objeto->not_link; ?></a><td>											
												</tr>
												</table>
											</td>					
										</tr>
									<?						
									$conec->siguiente();
								}
								?>
								</tbody>
								<?						
							}		
						}
					?> 		
					</table>
		<?
	}
	
	
	
	



?>