function agregar_directiva(fraterno,cargo,gestion)
{
	document.frm_sentencia.cargo.value="";
	document.frm_sentencia.fraterno.value="";
	document.frm_sentencia.nombre_fraterno.value="";
	
	var	valores="";
	
	valores="fraterno="+fraterno+"&cargo="+cargo+"&gestion="+gestion+"&tarea=agregar_directiva";
	
	ejecutar_ajax("ajax.php","FraGes",valores,"POST");
}

function eliminar_directiva(directiva,gestion)
{
	var	valores="";
	
	valores="directiva="+directiva+"&gestion="+gestion+"&tarea=eliminar_directiva";
	
	ejecutar_ajax("ajax.php","FraGes",valores,"POST");
}

function enviar_mail(fraterno,id_ie,concepto,mail)
{
	var	valores="";
	
	valores="fraterno="+fraterno+"&id_ie="+id_ie+"&tarea=enviar_mail&concepto="+concepto+"&mail="+mail;
	
	ejecutar_ajax("ajax.php","EnvMail",valores,"POST");
}

function CreaAjax()
{
	var objetoAjax=false;
	try {
			objetoAjax = new ActiveXObject("Msxml2.XMLHTTP");
		} 
		catch (e) 
		{
			try 
			{
				objetoAjax = new ActiveXObject("Microsoft.XMLHTTP");
			}
			catch (E) 
			{
				objetoAjax = false;
			}
		}
	if (!objetoAjax && typeof XMLHttpRequest!='undefined') 
	{
		objetoAjax = new XMLHttpRequest();
	}
	return objetoAjax;
}
function ejecutar_ajax(url,capa,valores,metodo)
{
  
	var ajax=CreaAjax();	 
	var capaContenedora = document.getElementById(capa);
	
	if(metodo.toUpperCase()=='POST')
	{
			ajax.open ('POST', url, true);
			ajax.onreadystatechange = function() 
							{
								if (ajax.readyState==1) 
								{
									capaContenedora.innerHTML="<img src='ajax-loader.gif'>";
								}
								else if (ajax.readyState==4)
								{
									if(ajax.status==200)
									{
										document.getElementById(capa).innerHTML=ajax.responseText;
									}
									else if(ajax.status==404)
									{
										capaContenedora.innerHTML = "La direccion no existe";
									}
									else
									{
										capaContenedora.innerHTML = "Error: "+ajax.status;
									}
								}
							}
			ajax.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
			ajax.send(valores);
			return;
	}
	if (metodo.toUpperCase()=='GET')
	{
		ajax.open ('GET', url, true);
		ajax.onreadystatechange = function() 
			{
				if (ajax.readyState==1) 
				{
					capaContenedora.innerHTML="Cargando.......";
				}
				else if (ajax.readyState==4)
				{
					if(ajax.status==200)
					{
						document.getElementById(capa).innerHTML=ajax.responseText;
					}
					else if(ajax.status==404)
					{
						capaContenedora.innerHTML = "La direccion no existe";
					}
					else
					{
						capaContenedora.innerHTML = "Error: "+ajax.status;
					}
				}
			}
			ajax.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
			ajax.send(valores);
			return
	}
}