﻿<?php	
    
    require_once('clases/usuario.class.php');
	require_once('clases/coneccion.class.php');	
	require_once('clases/funciones.class.php');	
	require_once('clases/mytime_int.php');
    require_once('clases/magpierss/rss_fetch.inc');
	
	define('MAGPIE_CACHE_ON', true);
	define('MAGPIE_OUTPUT_ENCODING','UTF-8');
	
class AUTOMATA 
{
	var $notas;	
	var $conec; 
	var $conec1;
	var $conversor;
	var $curl;
	var $rutaimg;
	
	function AUTOMATA()
	{
		$this->conec= new ADO();	        
		$this->conec1= new ADO();		
        $this->notas = array();
        $this->rutaimg = "not_img/";		
		$this->init_curl();	
	}
	
	function init_curl()
	{
		$this->curl = curl_init();
		curl_setopt($this->curl, CURLOPT_HEADER, 0);
		curl_setopt($this->curl, CURLOPT_FOLLOWLOCATION, 1);		
		curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($this->curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.3) Gecko/20070309 Firefox/2.0.0.3");	
	}
	
	function url_curl($url)
	{
	    curl_setopt($this->curl, CURLOPT_URL, $url);   		
	}
	
	function ejecutar_curl()
	{
	   return curl_exec($this->curl); 
	}
	
	function close_curl()
	{
	    curl_close($this->curl);
	}
		
	function obtener_pagina($tmparchivo,$url,$codificacion)
	{
	    $tmpFile= $tmparchivo;        
		$this->url_curl($url);		
		$content= $this->ejecutar_curl();        
	    if ($codificacion=="Si")
		   $content = utf8_encode($content);
		   
		file_put_contents($tmpFile.".htm",$content);		
		
		$tidy_config = array('input-encoding' => "utf8", 		                       
							 'output-encoding' => "utf8",								
							 'output-xml'   => true, 															
							 'char-encoding' => "utf8",
							 'ascii-chars' => true,
							 'drop-font-tags' => true,
							 'drop-proprietary-attributes' => true,                             							  
							 'logical-emphasis' => true,
							 'doctype'      => 'omit',								
							 'indent'       => false,
							 'clean'        => true,
							 'show-body-only' => true,                    
							 'wrap'         => 200);								
		
		$beginTag="<?xml version=\"1.0\" encoding=\"UTF-8\"?>".
				  "<depure>";
		$endTag="</depure>";
		
		$xmltidy =  $beginTag.  tidy_repair_file($tmpFile.'.htm' ,$tidy_config,'utf8') .$endTag ; 
		$xmltidy=str_replace("&nbsp;", " ", $xmltidy);			
		file_put_contents($tmpFile.'.xml',$xmltidy); 	
	}
	
	function eliminar_pagina($tmparchivo)
	{
	    unlink($tmparchivo.'.htm');
		unlink($tmparchivo.'.xml');	
	}
	
	
	
	function guardar_imagen($url,$nombre) 
	{
	    if (is_array(@getimagesize($url)))
		{
		    			
		    $c = curl_init();
			curl_setopt($c,CURLOPT_URL,$url);
			curl_setopt($c,CURLOPT_HEADER,0);
			curl_setopt($c,CURLOPT_RETURNTRANSFER,true);
			$s = curl_exec($c);		
			curl_close($c);
			$f = fopen($this->rutaimg.$nombre, 'wb');
			$z = fwrite($f,$s);
			if ($z != false) return true;
			return false;		
		}
		else
		{
		   return false;
		}		
	}
	
	function obtener_imagenes($expreg_imagen)
	{
		$tmparchivo = "tmpfilenota"; 		
       // $this->obtener_pagina($tmparchivo,$url,$codificacion);
		//cargar xml
		$doc = new DOMDocument();
		$doc->preserveWhiteSpace = false;
		@$doc->Load($tmparchivo.'.xml');	
		$nodes= $doc->getElementsByTagName("depure");

		$xpath = new DOMXPath($doc);	
		$query = $expreg_imagen;				
		$result = $xpath->query($query);	
		$cad = array();
		foreach ($result as $entry)
		{   		  
         	$cadaux = $entry->getAttribute('src');           
            $cad[] = $cadaux; 			
		}		
		return $cad;
	}	
	
	function obtener_nota_completa($url,$expreg_nota,$codificacion)
	{		
		$tmparchivo = "tmpfilenota"; 		
        $this->obtener_pagina($tmparchivo,$url,$codificacion);
		//cargar xml
		$doc = new DOMDocument();
		$doc->preserveWhiteSpace = false;
		@$doc->Load($tmparchivo.'.xml');	
		$nodes= $doc->getElementsByTagName("depure");

		$xpath = new DOMXPath($doc);	
		$query = $expreg_nota;				
		$result = $xpath->query($query);	
		$cad = "";
		foreach ($result as $entry)
		{          
		  //$cad.=utf8_decode($entry->textContent)."<br/>";	  
         	$cadaux = trim(str_replace("//","",strip_tags($entry->textContent)));	  
            //$cad.=utf8_decode($cadaux);
            $cad.=$cadaux; 			
		}
		//$this->eliminar_pagina($tmparchivo);  	
		return $cad;
	}
	
	
	function existe_nota($link)
	{
	    $sql="SELECT not_link FROM noticias WHERE not_link like '".addslashes($link)."'";	
	    $this->conec1->ejecutar($sql);
	    $num=$this->conec1->get_num_registros();	
        if ($num > 0)
          return true;
		else
          return false;		
	}
	
	function guardar_notas()
	{	   
	    foreach($this->notas as $not)
		{		    
			if (!$this->existe_nota($not['link']))
			{		    
				$sql="INSERT INTO noticias (not_fecha,not_titulo,not_resumen,not_link,not_notacompleta,not_prd_id,not_sec_id)
		                  values('".date('Y-m-d')."','".addslashes($not['title'])."','".addslashes($not['description'])."','".addslashes($not['link'])."','".addslashes($not['notacompleta'])."','".$not['periodico_id']."','".$not['seccion_id']."')";							  						  
				
				$this->conec1->ejecutar($sql,false);
				
				//$llave=mysql_insert_id();
				
				// foreach($not['notaimagen'] as $img)
				// {
				    // $archivo = substr(strrchr($img, "/"), 1);					
					// $nrand=date('dmYHis').rand();			
					// $nombre = $nrand."_".$archivo;
					
				    // $sw =  $this->guardar_imagen($img,$nombre);
				    // if ($sw)
					// {
						// $sql = "INSERT INTO noticias_imagen (nim_not_id,nim_url_imagen,nim_imagen) VALUES (".$llave.",'".$img."','".$nombre."')";
						// $this->conec->ejecutar($sql,false);
					// }	
				// }
			}
		}	
	
	}
	
	function spider()
	{	
		$sql = "SELECT prd_id,prd_nombre,prd_url,prd_formato_fecha,sec_id,sec_nombre,pes_tipo,pes_url_sec,pes_expreg_sec,pes_expreg_nota,pes_expreg_titulo,pes_expreg_resumen,pes_expreg_link,prd_codificacion,prd_codigo,pes_expreg_imagen 
		        FROM periodico INNER JOIN periodico_seccion ON (pes_prd_id = prd_id)
                                       INNER JOIN seccion ON (pes_sec_id = sec_id) 									   
				WHERE prd_estado ='Habilitado'"; 
				
		$this->conec->ejecutar($sql);
		$num=$this->conec->get_num_registros();		
		
		for($i=0;$i<$num;$i++)
		{		    
			$objeto=$this->conec->get_objeto();
			$this->notas = array();
			if ($objeto->pes_tipo=="rss")
			{
			    if (($objeto->pes_url_sec!="") && ($objeto->pes_expreg_nota!=""))
				{
					$rss = @fetch_rss($objeto->pes_url_sec);
					$g=0;
					if ($rss)
					{					
						foreach ($rss->items as $item) {
							$item['seccion_id']   = $objeto->sec_id;
							$item['periodico_id'] = $objeto->prd_id;	
							$item['periodico']    = $objeto->prd_nombre;					
							$item['notacompleta'] = $this->obtener_nota_completa($item['link'],$objeto->pes_expreg_nota,$objeto->prd_codificacion);												
							// if ($item['description'] == "")
							// {
								// $resaux = explode(".",$item['notacompleta']);
								// if (count($resaux) > 0)
								   // $item['description'] = $resaux[0];
							// }						
							//$this->verificar_nota($categorias,$item);
							
							// if (!is_null($objeto->pes_expreg_imagen) && ($objeto->pes_expreg_imagen!="") )
								// $item['notaimagen'] = $this->obtener_imagenes($objeto->pes_expreg_imagen);
							// else
								// $item['notaimagen'] = array();			
							
							$this->notas[] = $item;
							// if ($g==2)
							  // break;
							// $g++;  
						}
					}
				}
			}
            else
            {
			    if (($objeto->pes_url_sec!="") && ($objeto->pes_expreg_sec!="") && 
				    ($objeto->pes_expreg_nota!="") && ($objeto->pes_expreg_titulo!="") && 
					($objeto->pes_expreg_resumen!="") && ($objeto->pes_expreg_link!=""))
				{
					$tmparchivo = "tmpfileseccion"; 
					
					if ($objeto->prd_formato_fecha!="")
					{
					   $objeto->pes_url_sec = str_replace("{fecha}",date($objeto->prd_formato_fecha),$objeto->pes_url_sec);
					}			
                    //echo $objeto->pes_url_sec;
					$this->obtener_pagina($tmparchivo,$objeto->pes_url_sec,$objeto->prd_codificacion);
					//cargar xml
					$doc = new DOMDocument();
					$doc->preserveWhiteSpace = false;
					@$doc->Load($tmparchivo.'.xml');	
					$nodes= $doc->getElementsByTagName("depure");
					$xpath = new DOMXPath($doc);	
					$query = $objeto->pes_expreg_sec;
					$result = $xpath->query($query);
					$i=0;				
					
					foreach ($result as $entry)
					{   
						$item = array();
						
						$titulo      = $xpath->query($objeto->pes_expreg_titulo,$entry);
						$descripcion = $xpath->query($objeto->pes_expreg_resumen,$entry);
                        $link        = $xpath->query($objeto->pes_expreg_link,$entry);	
   						
						$item['seccion_id']   = $objeto->sec_id;
						$item['periodico_id'] = $objeto->prd_id;					 
						// $item['title'] = utf8_decode($titulo->item(0)->textContent);						
						// $item['description'] = utf8_decode($descripcion->item(0)->textContent);
						$item['title'] = $titulo->item(0)->textContent;						
						$item['description'] = $descripcion->item(0)->textContent;	
						
						if (substr_count($link->item(0)->getAttribute('href'),$objeto->prd_url)==0)
						    $item['link'] = $objeto->prd_url."/".$link->item(0)->getAttribute('href');
						else					
							$item['link'] = $link->item(0)->getAttribute('href');							
							
						$item['periodico']    = $objeto->prd_nombre;
                        
						/* Excepciones para ciertos periodicos en Particular */	
							
						if ($objeto->prd_codigo == "correodelsur")
						{
						    $aux = explode("'",$link->item(0)->getAttribute('onclick'));
							$item['link'] = $objeto->pes_url_sec.$aux[1];								
						}
						
						/* Fin Excepciones para ciertos periodicos en Particular */
						
						 $item['notacompleta'] = $this->obtener_nota_completa($item['link'],$objeto->pes_expreg_nota,$objeto->prd_codificacion);												
						
						if ($item['description'] == "")
						{
						    $resaux = explode(".",$item['notacompleta']);
							if (count($resaux) > 0)
							   $item['description'] = $resaux[0].".";
						}		
						
						// if (!is_null($objeto->pes_expreg_imagen) && ($objeto->pes_expreg_imagen!="") )
							// $item['notaimagen'] = $this->obtener_imagenes($objeto->pes_expreg_imagen);
						// else
							// $item['notaimagen'] = array();							
						
						
                        $this->notas[] = $item; 	
                        //break; 						
					}
					//$this->eliminar_pagina($tmparchivo);
                }				
			} 	
			$this->guardar_notas();	
			$this->conec->siguiente();
		}
		$this->close_curl();
		//print_r($this->notas);
     
		
	}
}

$automata = new AUTOMATA();
$automata->spider();


?>