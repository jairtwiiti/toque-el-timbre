<?php
	class SEO {
	    /*
	    Method to replace characters not accepted in URLs
	    */
		function scapeURL ($cadena) { 			// Sepadador de palabras que queremos utilizar
			$separador = "-";
			// Eliminamos el separador si ya existe en la cadan actual
			$cadena = str_replace($separador, "",$cadena);
			// Convertimos la cadena a minusculas
			$cadena = strtolower($cadena);
			// Remplazo tildes y e�es
			$cadena = strtr($cadena, "��������$", "aeiouAnNs");
			// Remplazo cuarquier caracter que no este entre A-Za-z0-9 por un espacio vacio
			$cadena = trim(ereg_replace("[^ A-Za-z0-9]", "", $cadena));
			// Inserto el separador antes definido
			$cadena = ereg_replace("[ \t\n\r]+", $separador, $cadena);
   
			return $cadena; 
			
			/*$spacer = "-";
			$string = trim($string);
			$string = strtolower($string);
			$string = trim(ereg_replace("[^ A-Za-z0-9_]", " ", $string)); 
 
			$string = ereg_replace("[ \t\n\r]+", "-", $string);
			$string = str_replace(" ", $spacer, $string);
			$string = ereg_replace("[ -]+", "-", $string);
			return $string; */			 		}
	}
	/*$seo = new SEO();	
	echo $seo->scapeURL("esto es una pruba { } ; / ? : @ & = + $ , . ! ~ * ' ( )");*/
?>

