<?PHP

class ADO
{
	var $dblink_ok=0;
	var $db;
	var $result;
	var $usu;

	function ADO()
	{
		$this->verificar_tipo_base();
		
		$this->conectar();
		
		$this->usu=new USUARIO;
	}
	
	function verificar_tipo_base()
	{
		switch(_TIPO_BASE_DE_DATOS){
			case 'mysql':
		                    $dbf_nodate=NODATE_MYSQL;
							$dbf_nodatetime=NODATE_MYSQL.' 00:00:00';
		                    define('DBF_NODATE',NODATE_MYSQL);
							 define('DBF_NODATETIME',NODATE_MYSQL.' 00:00:00');
						    $sql_LIKE='LIKE';
						    break;
			case 'postgres7':
		                     $dbf_nodate=NODATE_POSTGRE;
							$dbf_nodatetime=NODATE_POSTGRE.' 00:00:00';
							 define('DBF_NODATE',NODATE_POSTGRE);
							 define('DBF_NODATETIME',NODATE_POSTGRE.' 00:00:00');
							 $sql_LIKE = 'ILIKE';
						     break;
			case 'postgres':
		                    $dbf_nodate=NODATE_POSTGRE;
							$dbf_nodatetime=NODATE_POSTGRE.' 00:00:00';
							define('DBF_NODATE',NODATE_POSTGRE);
							define('DBF_NODATETIME',NODATE_POSTGRE.' 00:00:00');
							$sql_LIKE='ILIKE';
			                break;

			default:        $dbf_nodate=NODATE_DEFAULT;
							$dbf_nodatetime=NODATE_DEFAULT.' 00:00:00';
							define('DBF_NODATE',NODATE_DEFAULT);
							define('DBF_NODATETIME',NODATE_DEFAULT.' 00:00:00');
							$sql_LIKE='LIKE';
		}
	}
	
	function conectar()
	{
		$this->dblink_ok=0;

		# ADODB connection


		$this->db = &ADONewConnection(_TIPO_BASE_DE_DATOS);


		$this->dblink_ok = $this->db->Connect(_SERVIDOR_BASE_DE_DATOS,_USUARIO_BASE_DE_DATOS,_PASSWORD_BASE_DE_DATOS,_BASE_DE_DATOS);
		
	}
	
	function ejecutar($sql,$log=true)
	{
		$this->result=$this->db->Execute($sql);
		
		if($log)
		{
			$this->log($sql);
		}
		
	}
	
	function get_num_registros()
	{
		return $this->result->RecordCount();
	}
	
	function Mover($row)
	{
	   return $this->result->Move($row);
	   
	}
	
	function get_datos()
	{
		return $this->result->GetArray();
	}
	
	function get_fila()
	{
		return $this->result->GetRow();
	}
	
	function get_objeto()
	{
		return $this->result->FetchObj();
	}
	
	function siguiente()
	{
		return $this->result->MoveNext();
	}
	
	function log($sql)
	{
		$cmd = 	"^\*\*\*|" .
				"^ *INSERT|^ *DELETE|^ *UPDATE|^ *insert|^ *delete|" .
				"^ *update";
			$sql=str_replace("'","/",$sql);
			
			
			$consulta="insert into ad_logs 
				(log_fecha,log_hora,log_tipo_accion,log_accion,log_usu_id,log_ip,log_pc)
				values
				('".date("Y-m-d")."','".date("H:i:s")."','QUERY','".$sql."','".$this->usu->get_id()."','".$_SERVER['REMOTE_ADDR']."','".gethostbyaddr($_SERVER['REMOTE_ADDR'])."')";
			
		
		
		if(eregi($cmd,$sql))
		{
			//echo $consulta;
			mysql_query($consulta);
		}
	}

}
?>