<?php 

	require_once('config/database.conf.php');
	require_once('clases/adodb/adodb.inc.php');
	require_once('clases/usuario.class.php');
	require_once('clases/coneccion.class.php');
	$usuario=new USUARIO;

if($usuario->get_aut()=="positron"){
?>

        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

        <html xmlns="http://www.w3.org/1999/xhtml">

        <head>

        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

        <title>Panel</title>

        <link href="css/include_admin.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>

        <script language="javascript" type="text/javascript">



        function initMenu() {

            $('.menuAccordion ul').hide();

            /*$('.menuAccordion ul:first').show();*/

            /*******   menu nivel 1   ******/

            $('.menuAccordion .mLink1').click(function(){

                var cat = $(this).next();

                $(".menuNivel1").slideUp('normal');

                if(cat.is(':visible')){

                    $(cat).slideUp('normal');

                } else {

                    $(cat).slideDown('normal');

                };

            });

        };


        $(document).ready(function() {
            initMenu();

            /* menu activo categorias */

            $("#mLink1Activo").addClass("mLink1Activo"); // activo

            $(".mLink1").bind("click", function(event){

                $(".mLink1").removeClass("mLink1Activo");

                $(this).addClass("mLink1Activo");

                event.stopPropagation();

            })

            /* menu activo subcategorias */

            $("#mLinkSubCatActivo").addClass("mLinkSubCatActivo"); // activo

            $(".mLinkSubCat").bind("click", function(event){

                $(".mLinkSubCat").removeClass("mLinkSubCatActivo");

                $(this).addClass("mLinkSubCatActivo");

                event.stopPropagation();

            })

        });


        </script>

        </head>

        <body>

            <div class="contenedor">

                <!--- top -->

                <div class="top">


                    <div class="usuario">

                        <div class="usuarioIzq"></div>

                        <div class="usuarioCent">

                            <span class="usuarioEspacio">

                                <div class="usuarioFoto"><img width="50" height="50" src="imagenes/persona/chica/<? if($usuario->get_foto()<>"") echo $usuario->get_foto(); else  echo 'sin_foto.gif';?>"/></div>

                                <h4>Bienvenido</h4>

                                <?

                                if(strlen(trim($usuario->get_nombre_completo()))>22)

                                {

                                    echo substr(utf8_encode(trim($usuario->get_nombre_completo())), 0,22)."...";

                                }

                                else

                                {

                                    echo utf8_encode(trim($usuario->get_nombre_completo()));

                                }

                                ?><br />

                                <a href="log_out.php">Cerrar Sesi&oacute;n</a>

                            </span>

                        </div>

                        <div class="usuarioDer"></div>

                    </div>

                </div>



                <!-- cuerpo s-->

                <div class="cuerpo">

                    <div class="cuerpoTop">

                        <div class="cuerpoTopIzq"></div>

                        <div class="cuerpoTopDer"></div>

                    </div>



                    <div class="fondo">

                        <div class="fondo2">





                        <div class="menuModulo">

                            <div class="menuTop">

                                <div class="menuTopIzq"></div>

                                <div class="menuTopCent">.: MENU :.</div>

                            </div>



                            <div class="menuMod">
                                <ul class="menuAccordion">
                                    <?php
                                    require_once('herramientas/menu/menu.class.php');
                                    $menu=NEW MENU($usuario->get_id());
                                    $menu->dibujar_menu();
                                    ?>
                                </ul>
                          </div>

                            <div class="menuInf">
                                <div class="menuInfDer"></div>
                            </div>
                        </div>

                        <div class="contenido">
                            <iframe id="contenido" name="contenido" src="inicio.php" class="iframeContenido" allowtransparency="true"  allowtransparency="allowtransparency" frameborder="0"></iframe>
                        </div>

                    </div>
                    </div>
                </div>

                <!-- Informacion -->
                <div class="inferior">
                    <div class="inferiorIzq"></div>
                    <div class="inferiorCent">Toqueeltimbre.com 2014</div>
                    <div class="inferiorDer"></div>
                </div>


            </div>
        </body>
        </html>
<?php
}else{
	header('location:index.php?aut=x');
}
?>

