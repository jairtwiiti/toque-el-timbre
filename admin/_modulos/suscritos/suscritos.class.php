<?php

class REPBALANCE extends BUSQUEDA 
{
	var $formulario;
	var $mensaje;
	
	function REPBALANCE()
	{
		$this->coneccion= new ADO();
		
		$this->link='gestor.php';
		
		$this->modulo='suscritos';
		
		$this->formulario = new FORMULARIO();
		
		$this->formulario->set_titulo('Anuncios');
	}
	
	
	function dibujar_busqueda()
	{
			$this->formulario();
	}
	
	function formulario()
	{
	
		$this->formulario->dibujar_cabecera();
		
		if(!($_POST['formu']=='ok'))
		{
		?>
		
						<?php
						if($this->mensaje<>"")
						{
						?>
							<table width="100%" cellpadding="0" cellspacing="1" style="border:1px solid #DD3C10; color:#DD3C10;">
							<tr bgcolor="#FFEBE8">
								<td align="center">
									<?php
										echo $this->mensaje;
									?>
								</td>
							</tr>
							</table>
						<?php
						}
						?>						
						<div id="Contenedor_NuevaSentencia">
						<form id="frm_sentencia" name="frm_sentencia" action="gestor.php?mod=suscritos" method="POST" enctype="multipart/form-data">  
									<div id="FormSent">
										<div class="Subtitulo">Filtro del Reporte</div>
					                    <div id="ContenedorSeleccion">
											<div id="ContenedorDiv">
					                           <div class="Etiqueta" >Fecha Inicio</div>
					                             <div id="CajaInput">
													<input readonly="readonly" class="caja_texto" name="inicio" id="inicio" size="12" value="<?php echo $_POST['inicio'];?>" type="text">
														<input name="but_fecha_pago2" id="but_fecha_pago2" class="boton_fecha" value="..." type="button">
														<script type="text/javascript">
																		Calendar.setup({inputField     : "inicio"
																						,ifFormat     :     "%Y-%m-%d",
																						button     :    "but_fecha_pago2"
																						});
														</script>
												</div>		
					                        </div>
											
											<div id="ContenedorDiv">
					                           <div class="Etiqueta" >Fecha Fin</div>
					                             <div id="CajaInput">
													<input readonly="readonly" class="caja_texto" name="fin" id="fin" size="12" value="<?php echo $_POST['fin'];?>" type="text">
														<input name="but_fecha_pago" id="but_fecha_pago" class="boton_fecha" value="..." type="button">
														<script type="text/javascript">
																		Calendar.setup({inputField     : "fin"
																						,ifFormat     :     "%Y-%m-%d",
																						button     :    "but_fecha_pago"
																						});
														</script>
												</div>		
					                        </div>									
										</div>
										
										<div id="ContenedorDiv">
					                           <div id="CajaBotones">
													<center>
													<input type="hidden" class="boton" name="formu" value="ok">
													<input type="submit" class="boton" name="" value="Generar Reporte" >
													</center>
											   </div>
					                    </div>
									</div>
						</form>	
						<div>
		<?php
		}
		
		if($_POST['formu']=='ok')
			$this->mostrar_reporte();
	}	
	
	
	function mostrar_reporte()
	{		
		$conec= new ADO();
		
		$bol=0;
		$dol=0;
		
		////
		$pagina="'contenido_reporte'";
		
		$page="'about:blank'";
		
		$extpage="'reportes'";
		
		$features="'left=100,width=800,height=500,top=0,scrollbars=yes'";
		
		$extra1="'<html><head><title>Vista Previa</title><head>
				<link href=css/estilos.css rel=stylesheet type=text/css />
			  </head>
			  <body>
			  <div id=imprimir>
			  <div id=status>
			  <p>";
		$extra1.=" <a href=javascript:window.print();>Imprimir</a> 
				  <a href=javascript:self.close();>Cerrar</a></td>
				  </p>
				  </div>
				  </div>
				  <center>'";
		$extra2="'</center></body></html>'"; 
		
		$myday = setear_fecha(strtotime(date('Y-m-d')));
		////
				
		?>		
				<?php echo '	<table align=right border=0><tr><td><a href="javascript:var c = window.open('.$page.','.$extpage.','.$features.');
				  c.document.write('.$extra1.');
				  var dato = document.getElementById('.$pagina.').innerHTML;
				  c.document.write(dato);
				  c.document.write('.$extra2.'); c.document.close();
				  ">
				<img src="images/printer.png" align="right" width="20" border="0" title="IMPRIMIR">
				</a></td><td><img src="images/back.png" align="right" width="20" border="0"  title="VOLVER" onclick="javascript:location.href=\'gestor.php?mod=suscritos\';"></td></tr></table><br><br>
				';?>
						
				 

			<div id="contenido_reporte" style="clear:both;";>
			<center>
			<table style="font-size:12px;" width="100%" cellpadding="5" cellspacing="0" >
				<tr>
					<td width="40%">
						<strong><?php echo 'ToqueElTimbre'; ?></strong><BR>
						<strong>Santa Cruz - Bolivia</strong><BR><BR>
					</td>
				   
					<td><p align="center" ><strong><h3>ANUNCIOS </h3></strong><BR><?if ($_POST['inicio']<>"") echo '<strong>Del:</strong> '.date('d/m/Y',strtotime($_POST['inicio']))?><?if ($_POST['fin']<>"") echo ' <strong>Al:</strong> '.date('d/m/Y',strtotime($_POST['fin']))?></p></td>
				    <td width="40%"><div align="right"><img src="imagenes/micro.png" width="" /></div></td>
				</tr>
				<tr>
					<td colspan="2">
					<strong>Impreso el: </strong> <?php echo $myday;?> <br><br></td>			
					<td></td>
				</tr>
				 
			</table>
			<table   width="90%"  class="tablaReporte" cellpadding="0" cellspacing="0">
				<thead>
					<tr>
						<th>
							<b>Fecha</b>
						</th>
						<th>
							<b>Cantidad Anuncios</b>
						</th>
					</tr>
				</thead>
				<tbody>
		<?php				
		
		$conec= new ADO();			
		$sql = "SELECT pub_creado,DATE_FORMAT(pub_creado, '%d/%m/%Y') as fecha, count(*) as cantidad FROM publicacion where pub_estado='Aprobado' and  pub_creado >= '".$_POST['inicio']."' and pub_creado <='".$_POST['fin']."' group by fecha order by pub_creado desc";
		$conec->ejecutar($sql);		
		$num=$conec->get_num_registros();		
		
		//print_r($sql);
		
		for($i=0;$i<$num;$i++)
		{
			$objeto=$conec->get_objeto();
			echo '<tr>';			
					
					echo "<td>";
						echo "<b>".$objeto->fecha."</b>";
					echo "</td>";
					
					echo "<td>";
						echo "<b>".$objeto->cantidad."</b>";
					echo "</td>";	
					
			echo "</tr>";				
			$conec->siguiente();
		}
		?>
	
		
		</tbody>
		</table>
		</center>
		<br><table align="right" border="0" style="font-size:12px;"><tr><td><b>Impreso: </b><?php echo setear_fecha(strtotime(date('Y-m-d'))).' '.date('H:i');?></td></tr></table>
		</div><br>
		<?php
	}
	
}
?>