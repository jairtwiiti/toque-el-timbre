<?php

class MARCA extends BUSQUEDA 
{
	var $formulario;
	var $mensaje;
	
	function MARCA()
	{
		//permisos
		$this->ele_id=137;
		
		$this->busqueda();
		
		if(!($this->verificar_permisos('AGREGAR')))
		{
			$this->ban_agregar=false;
		}
		//fin permisos
		
		$this->num_registros=14;
		
		$this->coneccion= new ADO();

		$this->arreglo_campos[0]["nombre"]="mar_nombre";
		$this->arreglo_campos[0]["texto"]="Nombre";
		$this->arreglo_campos[0]["tipo"]="cadena";
		$this->arreglo_campos[0]["tamanio"]=40;
		
		
		
		$this->link='gestor.php';
		
		$this->modulo='marca';
		
		$this->formulario = new FORMULARIO();
		
		$this->formulario->set_titulo('MARCA');
		
		
	}
		
	function dibujar_busqueda()
	{		
		$this->formulario->dibujar_cabecera();		
		$this->dibujar_listado();
	}
	
		
	function set_opciones()
	{
				
		$nun=0;
		
		if($this->verificar_permisos('VER'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='VER';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_search.png';
			$this->arreglo_opciones[$nun]["nombre"]='VER';
			$nun++;
		}
		
		if($this->verificar_permisos('MODIFICAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='MODIFICAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_edit.png';
			$this->arreglo_opciones[$nun]["nombre"]='MODIFICAR';
			$nun++;
		}
		
		if($this->verificar_permisos('ELIMINAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='ELIMINAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_drop.png';
			$this->arreglo_opciones[$nun]["nombre"]='ELIMINAR';
			$nun++;
		}
		
		if($this->verificar_permisos('MODELO'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='MODELO';
			$this->arreglo_opciones[$nun]["imagen"]='images/ciudad.png';
			$this->arreglo_opciones[$nun]["nombre"]='MODELO';
			$nun++;
		}
	}
	
	function dibujar_listado()
	{
		$sql="SELECT 
		mar_id,mar_nombre
		FROM 
		marca
		";
		
		$this->set_sql($sql,'');
		
		$this->set_opciones();
		
		$this->dibujar();
		
	}
	
	function dibujar_encabezado()
	{
		?>
			<tr>
	        	<th>Nombre</th>
	            <th class="tOpciones" width="100px">Opciones</th>
			</tr>
			
		<?PHP
	}
	
	function mostrar_busqueda()
	{
		$conversor = new convertir();
		
		for($i=0;$i<$this->numero;$i++)
			{
				
				$objeto=$this->coneccion->get_objeto();
				echo '<tr>';
					echo "<td>";
						echo $objeto->mar_nombre;
					echo "&nbsp;</td>";
					echo "<td>";
						echo $this->get_opciones($objeto->mar_id);
					echo "</td>";
				echo "</tr>";
				
				$this->coneccion->siguiente();
			}
	}
	
	function cargar_datos()
	{
		$conec=new ADO();
		
		$sql="select * from marca
				where mar_id = '".$_GET['id']."'";
		
		$conec->ejecutar($sql);
		
		$objeto=$conec->get_objeto();
		
		$_POST['mar_nombre']=$objeto->mar_nombre;		
	}
	
	function datos()
	{
		if($_POST)
		{
			//texto,  numero,  real,  fecha,  mail.
			$num=0;
			$valores[$num]["etiqueta"]="Nombre";
			$valores[$num]["valor"]=$_POST['mar_nombre'];
			$valores[$num]["tipo"]="texto";
			$valores[$num]["requerido"]=true;
			$num++;
			
			$val=NEW VALIDADOR;
			
			$this->mensaje="";
			
			if($val->validar($valores))
			{
				return true;
			}
				
			else
			{
				$this->mensaje=$val->mensaje;
				return false;
			}
		}
			return false;
	}
	
	function formulario_tcp($tipo)
	{
				switch ($tipo)
				{
					case 'ver':{
								$ver=true;
								break;
								}
							
					case 'cargar':{
								$cargar=true;
								break;
								}
				}
				
				$url=$this->link.'?mod='.$this->modulo;
				
				$red=$url;
				
				if(!($ver))
				{
					$url.="&tarea=".$_GET['tarea'];
				}
				
				if($cargar)
				{
					$url.='&id='.$_GET['id'];
				}

		
		    $this->formulario->dibujar_tarea('MARCA');
		
			if($this->mensaje<>"")
			{
				$this->formulario->mensaje('Error',$this->mensaje);
			}
			?>
		<div id="Contenedor_NuevaSentencia">
			<form id="frm_sentencia" name="frm_sentencia" action="<?php echo $url;?>" method="POST" enctype="multipart/form-data">  
				<div id="FormSent">
					<div class="Subtitulo">Datos</div>
						<div id="ContenedorSeleccion">
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">* </span>Nombre</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto" name="mar_nombre" id="mar_nombre" size="60" maxlength="250" value="<?php echo $_POST['mar_nombre'];?>">
							   </div>
							</div>
							<!--Fin-->							
							
						</div>
						<div id="ContenedorDiv">
						   <div id="CajaBotones">
								<center>
								<?php
								if(!($ver))
								{
									?>
									<input type="submit" class="boton" name="" value="Guardar">
									<input type="reset" class="boton" name="" value="Cancelar">
									<input type="button" class="boton" name="" value="Volver" onclick="javascript:location.href='<?php echo $red;?>';">
									<?php
								}
								else
								{
									?>
									<input type="button" class="boton" name="" value="Volver" onclick="javascript:location.href='<?php echo $red;?>';">
									<?php
								}
								?>
								</center>
						   </div>
						</div>
				</div>
			</form>
		</div>
		<?php
	}
	
	function insertar_tcp()
	{
		$conec= new ADO();	
		
	    $sql="insert into marca(mar_nombre) values 
						('".$_POST['mar_nombre']."')";

		$conec->ejecutar($sql);
		$mensaje='Marca Agregada Correctamente!!!';
		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo);
			
	}
	
	
	function modificar_tcp()
	{
		$conec= new ADO();
		
		$sql="update marca set 
						mar_nombre='".$_POST['mar_nombre']."'						
						where mar_id = '".$_GET['id']."'";

		$conec->ejecutar($sql);
		$mensaje='Marca Modificada Correctamente!!!';
		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo);
	    
		
	}
	
	function formulario_confirmar_eliminacion()
	{
		
		$mensaje='Esta seguro de eliminar la marca?';
		
		$this->formulario->ventana_confirmacion($mensaje,$this->link."?mod=$this->modulo",'mar_id');
	}
	
	function eliminar_tcp()
	{
	
	    $llave=$_POST['mar_id'];
		
			
		$conec= new ADO();
		
		$sql="delete from marca where mar_id='".$_POST['mar_id']."'";
		
		$conec->ejecutar($sql);
		
		$sql="delete from modelo where mod_mar_id='".$_POST['mar_id']."'";
		
		$conec->ejecutar($sql);
		
		$mensaje='Marca Eliminado Correctamente!!!';
		
		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo);
	}
	
		
	//////////////////////////////////////////////////////////////
	
	function guardar_producto()
	{		
		$conec= new ADO();

		$sql="insert into modelo(mod_nombre,mod_mar_id) 
		values ('".$_POST['mod_nombre']."','".$_GET['id']."')";

		$conec->ejecutar($sql);

		$this->limpiar();
		
	}

	function limpiar()
	{
		$_POST['mod_nombre']="";
	}

	

	function datos2()
	{

		if($_POST)
		{
			require_once('clases/validar.class.php');

			$i=0;
			$valores[$i]["etiqueta"]="Nombre";
			$valores[$i]["valor"]=$_POST['mod_nombre'];
			$valores[$i]["tipo"]="todo";
			$valores[$i]["requerido"]=true;	

			$val=NEW VALIDADOR;

			$this->mensaje="";

			if($val->validar($valores))
			{
				return true;
			}
			else
			{
				$this->mensaje=$val->mensaje;

				return false;

			}

		}

			return false;

	}

	function formulario_tcp2($tipo)
	{

		$url=$this->link.'?mod='.$this->modulo;

		$re=$url;

		$enlace="<a href='$url'>Marca</a>";

		echo "<tr>
					<td class='busqueda_enlace' colspan='3'>
						Volver a $enlace
					</td>
				</tr>"; 		

		switch ($tipo)
		{

			case 'ver':{
						$ver=true;
						
						break;
						}

			case 'cargar':{

						$cargar=true;

						break;
						}
		}

		if(!($ver))
		{
			$url.="&tarea=".$_GET['tarea'];
		}

		if($cargar)
		{
			$url.='&id='.$_GET['id'];
		}
		
		if($_GET['acc']=='MODIFICAR_PRODUCTO')
		{
			$url.='&acc=MODIFICAR_PRODUCTO';
		}
		
		$this->formulario->dibujar_tarea('modelo');
		
		if($this->mensaje<>"")
			{
				$this->formulario->mensaje('Error',$this->mensaje);
			}

		?>


					<div id="Contenedor_NuevaSentencia">

               <form id="frm_sentencia" name="frm_sentencia" action="<?php echo $url.'&mod_id='.$_GET['mod_id'];?>" method="POST" enctype="multipart/form-data">  

				<div id="FormSent">
					<div class="Subtitulo">Modelo</div>

                    <div id="ContenedorSeleccion">
						<!--Inicio-->
						<div id="ContenedorDiv">
						   <div class="Etiqueta" ><span class="flechas1">*</span>Nombre</div>
						   <div id="CajaInput">
						   <input type="text" class="caja_texto" name="mod_nombre" id="mod_nombre" size="60" maxlength="250" value="<?php echo $_POST['mod_nombre'];?>">
						   </div>
						</div>
				
                    </div>

					<div id="ContenedorDiv">

                           <div id="CajaBotones">

								<center>

								<input type="submit" class="boton" name="" value="Enviar">

							    <input type="reset" class="boton" name="" value="Cancelar">

								<input type="button" class="boton" name="" value="Volver" onclick="location.href='<?php if($_GET['acc']=="MODIFICAR_PRODUCTO") echo $this->link.'?mod='.$this->modulo."&tarea=MODELO&id=".$_GET['id']; else echo $this->link.'?mod='.$this->modulo;?>';">

								</center>

						   </div>

                        </div>

                </div>

            </div>					

		<?php
	}

	

	function dibujar_encabezado2()
	{

		?><div style="clear:both;"></div><center>

		<table class="tablaLista" cellpadding="0" cellspacing="0" width="60%">
			<thead>
				<tr>
					<th >

						Nombre

					</th>

					<th class="tOpciones" width="100px">

						Opciones

					</th>

				</tr>
			</thead>
			<tbody>
		<?PHP

	}

	

	function mostrar_busqueda2()
	{

		$conec=new ADO();

		$sql="select 
		mod_id,mod_nombre
		from 
		modelo 
		where 
		mod_mar_id='".$_GET['id']."' 
		order by 
		mod_id desc";
		//echo $sql;
		$conec->ejecutar($sql);

		$num=$conec->get_num_registros();

		for($i=0;$i<$num;$i++)
		{
			$objeto=$conec->get_objeto();

			echo '<tr class="busqueda_campos">';

			?>
				<td align="left">

					<?php echo $objeto->mod_nombre;?>

				</td>

				<td>

					<center>

					<a href="gestor.php?mod=marca&tarea=MODELO&acc=ELIMINAR&mod_id=<?php echo $objeto->mod_id;?>&id=<?php echo $_GET['id'];?>"><img src="images/b_drop.png" alt="ELIMINAR" title="ELIMINAR" border="0"></a>

					<a href="gestor.php?mod=marca&tarea=MODELO&acc=MODIFICAR_PRODUCTO&mod_id=<?php echo $objeto->mod_id;?>&id=<?php echo $_GET['id'];?>"><img src="images/b_edit.png" alt="MODIFICAR" title="MODIFICAR" border="0"></a>

					</center>

				</td>

			<?php

			echo "</tr>";

			

			

			$conec->siguiente();

		}

		echo "</tbody></table></center><br>";
	}
	
	function eliminar_producto($id_modelo)
	{

		$conec= new ADO();

		$sql="delete from modelo where mod_id='".$id_modelo."'";

		$conec->ejecutar($sql);
		
		$this->mensaje='Modelo Eliminado Correctamente';
	

	}
		
	function modificar_producto()
	{		
		$conec= new ADO();

		$mensaje='Modelo Modificado Correctamente';

		$sql="update modelo set 
		mod_nombre='".$_POST['mod_nombre']."'
		where mod_id= '".$_GET['mod_id']."'";

		$conec->ejecutar($sql);

		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=MODELO&id='.$_GET['id']);

	}
	
	function cargar_datos2()
	{
		$conec=new ADO();

		$sql="select * from modelo where mod_id = '".$_GET['mod_id']."'";

		$conec->ejecutar($sql);

		$objeto=$conec->get_objeto();

		$_POST['mod_nombre']=$objeto->mod_nombre;

	}
}
?>