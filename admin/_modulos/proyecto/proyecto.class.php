<?php

class PROYECTO extends BUSQUEDA 
{
	var $formulario;
	var $mensaje;
	
	function PROYECTO()
	{
		//permisos
		$this->ele_id=158;
		
		$this->busqueda();
		
		if(!($this->verificar_permisos('AGREGAR')))
		{
			$this->ban_agregar=false;
		}
		//fin permisos
		
		$this->num_registros=20;
		
		$this->coneccion= new ADO();
		
		$num=0;
		$this->arreglo_campos[$num]["nombre"]="fam_titulo";
		$this->arreglo_campos[$num]["texto"]="Titulo";
		$this->arreglo_campos[$num]["tipo"]="cadena";
		$this->arreglo_campos[$num]["tamanio"]=20;
		
		
		$this->link='gestor.php';
		
		$this->modulo='proyecto';
		
		$this->formulario = new FORMULARIO();
		
		$this->formulario->set_titulo('Proyectos');
		
	}
		
	function dibujar_busqueda()
	{
		
		$this->formulario->dibujar_cabecera();
		
		$this->dibujar_listado();
	}
	
		
	function set_opciones()
	{
				
		$nun=0;
		
		if($this->verificar_permisos('VER'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='VER';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_search.png';
			$this->arreglo_opciones[$nun]["nombre"]='VER';
			$nun++;
		}
		
		if($this->verificar_permisos('MODIFICAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='MODIFICAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_edit.png';
			$this->arreglo_opciones[$nun]["nombre"]='MODIFICAR';
			$nun++;
		}
		
		if($this->verificar_permisos('ELIMINAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='ELIMINAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_drop.png';
			$this->arreglo_opciones[$nun]["nombre"]='ELIMINAR';
			$nun++;
		}
		
		if($this->verificar_permisos('PUBLICACIONES'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='PUBLICACIONES';
			$this->arreglo_opciones[$nun]["imagen"]='images/publicaciones.jpg';
			$this->arreglo_opciones[$nun]["nombre"]='PUBLICACIONES';
			$nun++;
		}
		
		if($this->verificar_permisos('FOTOS'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='FOTOS';
			$this->arreglo_opciones[$nun]["imagen"]='images/fotos.png';
			$this->arreglo_opciones[$nun]["nombre"]='FOTOS';
			$nun++;
		}		
		
		if($this->verificar_permisos('TIPOLOGIA'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='TIPOLOGIA';
			$this->arreglo_opciones[$nun]["imagen"]='images/tipologia.jpg';
			$this->arreglo_opciones[$nun]["nombre"]='TIPOLOGIA';
			$nun++;
		}
		
		if($this->verificar_permisos('ACABADO'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='ACABADO';
			$this->arreglo_opciones[$nun]["imagen"]='images/acabado.jpg';
			$this->arreglo_opciones[$nun]["nombre"]='ACABADO';
			$nun++;
		}
		
		if($this->verificar_permisos('DISPONIBILIDAD'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='DISPONIBILIDAD';
			$this->arreglo_opciones[$nun]["imagen"]='images/disponibilidad.jpg';
			$this->arreglo_opciones[$nun]["nombre"]='DISPONIBILIDAD';
			$nun++;
		}
		
		if($this->verificar_permisos('AREASOCIAL'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='AREASOCIAL';
			$this->arreglo_opciones[$nun]["imagen"]='images/areasocial.png';
			$this->arreglo_opciones[$nun]["nombre"]='AREASOCIAL';
			$nun++;
		}
				
	}
	
	function dibujar_listado()
	{
		$sql="SELECT *from proyecto ";
		
		$this->set_sql($sql,' order by proy_id desc ');
		
		$this->set_opciones();
		
		$this->dibujar();
		
	}
	
	function orden($enlaces,$accion,$ant_orden)
	{
		$conec= new ADO();
		
		if($accion=='s')
			$cad=" where fam_orden < $ant_orden order by fam_orden desc";
		else

			$cad=" where fam_orden > $ant_orden order by fam_orden asc";			

		$consulta = "
		select 
			fam_id,fam_orden 
		from 
			familia_aplicaciones
		$cad
		limit 0,1
		";	

		$conec->ejecutar($consulta);

		$num = $conec->get_num_registros();   

		if($num > 0)
		{
			$objeto=$conec->get_objeto();
			
			$nu_orden=$objeto->fam_orden;
			
			$id=$objeto->fam_id;
			
			$consulta = "update familia_aplicaciones set fam_orden='$nu_orden' where fam_id='$enlaces'";	

			$conec->ejecutar($consulta);
			
			$consulta = "update familia_aplicaciones set fam_orden='$ant_orden' where fam_id='$id'";	

			$conec->ejecutar($consulta);
		}	
	}	
	
	function dibujar_encabezado()
	{
		?>
			<tr>
			
				<th>Proyecto</th>
				<th>Logo</th>			
	            <th class="tOpciones" width="100px">Opciones</th>
                
			</tr>
			
		<?PHP
	}
	
	function mostrar_busqueda()
	{
		$conversor = new convertir();
		
		for($i=0;$i<$this->numero;$i++)
			{
				
				$objeto=$this->coneccion->get_objeto();
				echo '<tr>';
									
					echo "<td>";
						echo $objeto->proy_nombre;
					echo "&nbsp;</td>";
					echo "<td>";
						echo '<img width="100" height="100" src="imagenes/proyecto/'.$objeto->proy_logo.'" />';
					echo "&nbsp;</td>";																
					echo "<td>";
						echo $this->get_opciones($objeto->proy_id);
					echo "</td>";
				echo "</tr>";
				
				$this->coneccion->siguiente();
			}
	}
	
	function cargar_datos()
	{
		$conec=new ADO();
		
		$sql="select * from proyecto
				where proy_id = '".$_GET['id']."'";
		
		$conec->ejecutar($sql);
		
		$objeto=$conec->get_objeto();
		
		$_POST['proy_nombre']=$objeto->proy_nombre;		
		$_POST['proy_logo']=$objeto->proy_logo;		
		$_POST['proy_video']=$objeto->proy_video;	
		$_POST['proy_descripcion']=$objeto->proy_descripcion;	
		$_POST['proy_latitud']=$objeto->proy_latitud;
		$_POST['proy_longitud']=$objeto->proy_longitud;
		$_POST['proy_ubicacion']=$objeto->proy_ubicacion;
		$_POST['proy_estado']=$objeto->proy_estado;		
		$_POST['proy_creado']=$objeto->proy_creado;
		$_POST['proy_contacto']=$objeto->proy_contacto;
        $_POST['proy_vig_ini']=$objeto->proy_vig_ini;		
		$_POST['proy_vig_fin']=$objeto->proy_vig_fin;	
		$_POST['inm_ciu_id']=$objeto->proy_ciu_id;	
		$_POST['inm_zon_id']=$objeto->proy_zon_id;	
		$_POST['proy_usu_id']=$objeto->proy_usu_id;	
		$_POST['proy_imagen_dis']=$objeto->proy_imagen_dis;	
		$_POST['proy_ubicacion_img']=$objeto->proy_ubicacion_img;	
		
		
		
		
		
		$sql="select * from ciudad
				where ciu_id = '".$_POST['inm_ciu_id']."'";
		
		$conec->ejecutar($sql);
		
		$objeto=$conec->get_objeto();
		
		$_POST['dep_id'] = $objeto->ciu_dep_id;
		
		
		$sql="select * from usuario
				where usu_id = '".$_POST['proy_usu_id']."'";
		
		$conec->ejecutar($sql);
		
		$objeto=$conec->get_objeto();
		
		$_POST['usu_nombre'] = $objeto->usu_nombre . ' ' . $objeto->usu_apellido;
	}
	
	function datos()
	{
		if($_POST)
		{
			//texto,  numero,  real,  fecha,  mail.
			$num=0;
			
			$valores[$num]["etiqueta"]="Usuario";
			$valores[$num]["valor"]=$_POST['proy_usu_id'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			
			$valores[$num]["etiqueta"]="Nombre";
			$valores[$num]["valor"]=$_POST['proy_nombre'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			
			/*$valores[$num]["etiqueta"]="Logo";
			$valores[$num]["valor"]=$_FILES['proy_logo']['name'];;
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;*/
			
			$valores[$num]["etiqueta"]="Descripci�n";
			$valores[$num]["valor"]=$_POST['proy_descripcion'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			
			/*$valores[$num]["etiqueta"]="Latitud";
			$valores[$num]["valor"]=$_POST['proy_latitud'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			
			$valores[$num]["etiqueta"]="Longitud";
			$valores[$num]["valor"]=$_POST['proy_longitud'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;*/
			
			$valores[$num]["etiqueta"]="Contacto";
			$valores[$num]["valor"]=$_POST['proy_contacto'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			
			
			/*$valores[$num]["etiqueta"]="Fecha Inicio";
			$valores[$num]["valor"]=$_POST['proy_vig_ini'];
			$valores[$num]["tipo"]="fecha";
			$valores[$num]["requerido"]=true;
			$num++;
			
			$valores[$num]["etiqueta"]="Fecha Fin";
			$valores[$num]["valor"]=$_POST['proy_vig_fin'];
			$valores[$num]["tipo"]="fecha";
			$valores[$num]["requerido"]=true;
			$num++;*/
			
			/*$valores[$num]["etiqueta"]="Categoria";
			$valores[$num]["valor"]=$_POST['categoria'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;*/
			
			$valores[$num]["etiqueta"]="Ubicaci�n";
			$valores[$num]["valor"]=$_POST['proy_ubicacion'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			
			$valores[$num]["etiqueta"]="Ciudad";
			$valores[$num]["valor"]=$_POST['inm_ciu_id'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			
			$valores[$num]["etiqueta"]="Zona";
			$valores[$num]["valor"]=$_POST['inm_zon_id'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			
			
			
			$val=NEW VALIDADOR;
			
			$this->mensaje="";
			
			if($val->validar($valores))
			{
				return true;
			}
				
			else
			{
				$this->mensaje=$val->mensaje;
				return false;
			}
		}
			return false;
	}
	
	function formulario_tcp($tipo)
	{
				switch ($tipo)
				{
					case 'ver':{
								$ver=true;
								break;
								}
							
					case 'cargar':{
								$cargar=true;
								break;
								}
				}
				
				$url=$this->link.'?mod='.$this->modulo;
				
				$red=$url."&tarea=ACCEDER";
				
				if(!($ver))
				{
					$url.="&tarea=".$_GET['tarea'];
				}
				
				if($cargar)
				{
					$url.='&id='.$_GET['id'];
				}

			$page="'gestor.php?mod=". $this->modulo ."&tarea=AGREGAR&acc=Emergente'";
			$extpage="'usuario'";
			$features="'left=325,width=600,top=200,height=420'";
			
		    $this->formulario->dibujar_tarea('PROYECTOS');
		
			if($this->mensaje<>"")
			{
				$this->formulario->mensaje('Error',$this->mensaje);
			}
			?>

		<script type="text/javascript">
		   var map;
		   var markers = [];
		   var geocoder;
		   var zooms = 5;		   
		   function initialize() {
				geocoder = new google.maps.Geocoder();				
				var myLatlng = new google.maps.LatLng(-16.930705,-64.782716); 		
				
				var myOptions = {
					zoom: zooms,
					center: myLatlng,
					mapTypeId: google.maps.MapTypeId.ROADMAP
				}
				
				map = new google.maps.Map(document.getElementById("map"), myOptions);						
				
				<?php
				    if ($_POST['proy_latitud'] != "" && $_POST['proy_longitud'] != "")
				    {				       
				?>
				       var mypoint = new google.maps.LatLng(<? echo $_POST['proy_latitud']; ?>,<? echo $_POST['proy_longitud']; ?>);					 
					   createMarker(mypoint);	
					   map.setCenter(mypoint);			
					   map.setZoom(15);	
				<?php
				    }
				?>
				
				google.maps.event.addListener(map, 'click', function(event) {
				
					if  (markers.length < 1)
					{	
						createMarker(event.latLng);	  
						
					}   
					else
					{	
						markers[0].setPosition(event.latLng);
						actualizar_punto(markers[0]);
					}	
						
				});
				
				
		   }
		   
		   function actualizar_punto(point)
		   {
		        document.getElementById("latitud").value  =	point.getPosition().lat();	
				document.getElementById("longitud").value = point.getPosition().lng();	
		   }
		   
		   function createMarker(point) {
				
				var clickedLocation = new google.maps.LatLng(point);
				
				markers.push(new google.maps.Marker({
				  position: point,
				  map: map,
				  draggable: true,
				  animation: google.maps.Animation.DROP
				}));  
				
				actualizar_punto(markers[0]);
					
				google.maps.event.addListener(markers[0], 'dragend', function() {	
					
				actualizar_punto(markers[0]);});
			}

			function codeAddress() {
				
				var pais    = document.getElementById("map_pais").value;		
				var ciudad  = document.getElementById("map_ciudad").value;
				var tipo  = document.getElementById("map_tipo").value;
				var direccion = document.getElementById("map_direccion").value;
				var address;
				
				if (direccion != "")		   
				   address = tipo+","+direccion+","+ciudad+","+pais;	
				else
				   address = ciudad+","+pais;					
				
				geocoder.geocode( { 'address': address}, function(results, status) {
				  if (status == google.maps.GeocoderStatus.OK) {
					map.setCenter(results[0].geometry.location);			
					map.setZoom(12);
					if (direccion != "")
					   map.setZoom(15);
					
					if  (markers.length < 1)
					{	
						createMarker(results[0].geometry.location);
					}
					else
					{
						markers[0].setPosition(results[0].geometry.location);
					}			
					
				  } else {
					alert("Geocode was not successful for the following reason: " + status);
				  }
				});
			}	
			
		  
			function loadScript() {
			  var script = document.createElement("script");
			  script.type = "text/javascript";
			  script.src = "http://maps.google.com/maps/api/js?sensor=false&callback=initialize";
			  document.body.appendChild(script);
			}
			  
			window.onload = loadScript;
			
			$(document).ready(function() {
				$('.cerrarToogle').hide();				
				$('.cerrarToogle:first').show();
				$('.prueba').click(function(){
					var cat = $(this).parent().find('.cerrarToogle');
					if(cat.is(':visible')){
						$(cat).hide();
					} else {
						$(cat).slideDown('normal');
					};
				});	
			});	

			function cargar_ciudad(id)
			{
				var	valores="thid="+id;			
				ejecutar_ajax('ajax.php','ciudad',valores,'POST');
				var	valores="tzid="+id;			
				ejecutar_ajax('ajax.php','zona',valores,'POST');	
			}
			
			function cargar_caracteristica(id)
			{
				var	valores="tcid="+id;			
				ejecutar_ajax('ajax.php','caracteristica',valores,'POST');				
			}			
			
		</script>				
			
			
		<?php include_once("js/fckeditor/fckeditor.php"); ?>	  
		<div id="Contenedor_NuevaSentencia">
			<form id="frm_sentencia" name="frm_sentencia" action="<?php echo $url;?>" method="POST" enctype="multipart/form-data">  
				<div id="FormSent">
				  
					<div class="Subtitulo">Datos</div>
						<div id="ContenedorSeleccion">
							
							
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">*</span>Usuario</div>
							   <div id="CajaInput">
								    <input name="proy_usu_id" id="proy_usu_id" readonly="readonly" type="hidden" class="caja_texto" value="<?php echo $_POST['proy_usu_id']?>" size="4">
									<input name="usu_nombre" id="usu_nombre" readonly="readonly"  type="text" class="caja_texto" value="<?php echo $_POST['usu_nombre']?>" size="40">
									
									<img src="images/ir.png"  onclick="javascript:window.open(<?php echo $page;?>,<?php echo $extpage;?>,<?php echo $features;?>);">
										
							   </div>

							</div>
							<!--Fin-->
							
							
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">* </span>Nombre</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto" name="proy_nombre" id="proy_nombre" size="60" maxlength="250" value="<?php echo $_POST['proy_nombre'];?>">
							   </div>
							</div>
							<!--Fin-->
							
							
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Logo</div>
							   <div id="CajaInput">
							   <?php
								if($_POST['proy_logo']<>"")
								{	$foto=$_POST['proy_logo'];
									$b=true;
								}	
								else
								{	$foto='sin_foto.gif';
									$b=false;
								}
								if(($ver)||($cargar))
								{	?>
									<img src="imagenes/proyecto/<?php echo $foto;?>" border="0" width="50"><br>
									<input   name="proy_logo" type="file" id="proy_logo" /><span class="flechas1"></span>
									<?php
								}
								else 
								{
									?>
									<input  name="proy_logo" type="file" id="proy_logo" /><span class="flechas1"></span>
									<?php
								}
								?>
								<input   name="fotooculta" type="hidden" id="fotooculta" value="<?php echo $_POST['proy_logo'].$_POST['fotooculta'];?>"/>
							   </div>
							</div>
							<!--Fin-->
							
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Imagen Disponibilidad</div>
							   <div id="CajaInput">
							   <?php
								if($_POST['proy_imagen_dis']<>"")
								{	$foto=$_POST['proy_imagen_dis'];
									$b=true;
								}	
								else
								{	$foto='sin_foto.gif';
									$b=false;
								}
								if(($ver)||($cargar))
								{	?>
									<img src="imagenes/proyecto/<?php echo $foto;?>" border="0" width="50"><br>
									<input   name="proy_imagen_dis" type="file" id="proy_imagen_dis" /><span class="flechas1"></span>
									<?php
								}
								else 
								{
									?>
									<input  name="proy_imagen_dis" type="file" id="proy_imagen_dis" /><span class="flechas1"></span>
									<?php
								}
								?>
								<input   name="fotooculta" type="hidden" id="fotooculta" value="<?php echo $_POST['proy_imagen_dis'].$_POST['fotooculta1'];?>"/>
							   </div>
							</div>
							<!--Fin-->
							
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1"> </span>Video</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto" name="proy_video" id="proy_video" size="60" maxlength="250" value="<?php echo $_POST['proy_video'];?>">
							   </div>
							</div>
							<!--Fin-->
							
							
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">*</span>Descripci�n</div>
							   <div id="CajaInput">
								<!-- <textarea name="proy_descripcion" id="proy_descripcion"><?php echo $_POST['proy_descripcion']; ?> </textarea> -->
									<?php
						$pagina=$_POST['proy_descripcion'];
						$oFCKeditor = new FCKeditor('proy_descripcion') ;
						$oFCKeditor->BasePath = 'js/fckeditor/';
						$oFCKeditor->Width  = '650' ;
						$oFCKeditor->Height = '350' ;
						//$oFCKeditor->ToolbarSet = 'Basic' ;
						$oFCKeditor->Value = <<<EOF
$pagina
EOF;
						$oFCKeditor->Create() ;
						?>     
							   </div>
							</div>
							<!--Fin-->   
							
								<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">*</span>Ubicaci�n</div>
							   <div id="CajaInput">
						<!--	   <textarea name="proy_ubicacion" id="proy_ubicacion"><?php echo $_POST['proy_ubicacion']; ?> </textarea>-->
								<?php
						$pagina=$_POST['proy_ubicacion'];
						$oFCKeditor = new FCKeditor('proy_ubicacion') ;
						$oFCKeditor->BasePath = 'js/fckeditor/';
						$oFCKeditor->Width  = '650' ;
						$oFCKeditor->Height = '200' ;
						//$oFCKeditor->ToolbarSet = 'Basic' ;
						$oFCKeditor->Value = $pagina;
						$oFCKeditor->Create() ;
						?>     
							   </div>
							</div>
							<!--Fin-->  


							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">*</span>Contacto</div>
							   <div id="CajaInput">
							   <!--<textarea name="proy_contacto" id="proy_contacto"><?php echo $_POST['proy_contacto']; ?> </textarea>-->
								<?php
								$pagina=$_POST['proy_contacto'];
								$oFCKeditor = new FCKeditor('proy_contacto') ;
								$oFCKeditor->BasePath = 'js/fckeditor/';
								$oFCKeditor->Width  = '650' ;
								$oFCKeditor->Height = '200' ;
								//$oFCKeditor->ToolbarSet = 'Basic' ;
								$oFCKeditor->Value = $pagina;
								$oFCKeditor->Create() ;
								?>     
							   </div>
							</div>
							<!--Fin-->   							
							
							
								<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Departamento</div>
							   <div id="CajaInput">
							   <select name="dep_id" class="caja_texto" onchange="cargar_ciudad(this.value);">
							   <option value="">Seleccione</option>
							   <?php 		
								$fun=NEW FUNCIONES;		
								$fun->combo("select dep_id as id,dep_nombre as nombre from departamento",$_POST['dep_id']);				
								?>
							   </select>
							   </div>
							   
							</div>
							<!--Fin-->
							
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Ciudad</div>
							   <div id="CajaInput">
							   <div id="ciudad">
							   <select name="inm_ciu_id" class="caja_texto">
							   <option value="">Seleccione</option>
							   <?php 	
                                 if ($_POST["dep_id"])							   
								 {							   
									$fun=NEW FUNCIONES;		
									$fun->combo("select ciu_id as id,ciu_nombre as nombre from ciudad",$_POST['inm_ciu_id']);				
								}
								?>
							   </select>
							   </div>
							   </div>
							   
							</div>
							<!--Fin-->
							
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Zona</div>
							   <div id="CajaInput">		
                                <div id="zona">		   
							   <select name="inm_zon_id" id="inm_zon_id" class="caja_texto">
							   <option value="">Seleccione</option>
							   <?php 	
                                 							   
									$fun=NEW FUNCIONES;		
									$fun->combo("select zon_id as id,zon_nombre as nombre from zona",$_POST['inm_zon_id']);				
								
								?>
							   </select>	
                               </div>							   
							   </div>
							   
							</div>
						
							
							<!--Inicio-->
							<!-- <div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1"> </span>Categorias</div>
								<div id="CajaInput">
									<table style="font-family: tahoma; font-size: 11px;">
										<tr>
											<td><input type="checkbox" name="categoria[]" value="casa"></td>
											<td>Casa</td>
										</tr>
										<tr>
											<td><input type="checkbox" name="categoria[]" value="departamento"> </td>
											<td>Departamento</td>
										</tr>
										<tr>
											<td><input type="checkbox" name="categoria[]" value="terreno"></td>
											<td>Terreno</td>
										</tr>
									</table>									 
								</div>
									
							</div> -->
							<!--Fin-->
							
							
							<!--Inicio-->
							<!--<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1"> </span>Habilitar</div>
							   <div id="CajaInput">
									<table>
										<tr>
											<td><input type="checkbox" name="habilitar" value=""></td>
										</tr>
									</table>
							   </div>
							</div>-->
							<!--Fin-->
							
							
							<!--Inicio-->
							<!--<div id="ContenedorDiv">
					            <div class="Etiqueta" >Fecha Inicio</div>
					            <div id="CajaInput">
									<input readonly="readonly" class="caja_texto" name="proy_vig_ini" id="proy_vig_ini" size="12" value="<?php echo $_POST['proy_vig_ini'];?>" type="text">
									<input name="but_fecha_pago2" id="but_fecha_pago2" class="boton_fecha" value="..." type="button">
									<script type="text/javascript">
																		Calendar.setup({inputField     : "proy_vig_ini"
																						,ifFormat     :     "%Y-%m-%d",
																						button     :    "but_fecha_pago2"
																						});
									</script>
								</div>		
					        </div>
											
							<div id="ContenedorDiv">
					            <div class="Etiqueta" >Fecha Fin</div>
					            <div id="CajaInput">
									<input readonly="readonly" class="caja_texto" name="proy_vig_fin" id="proy_vig_fin" size="12" value="<?php echo $_POST['proy_vig_fin'];?>" type="text">
									<input name="but_fecha_pago" id="but_fecha_pago" class="boton_fecha" value="..." type="button">
									<script type="text/javascript">
																		Calendar.setup({inputField     : "proy_vig_fin"
																						,ifFormat     :     "%Y-%m-%d",
																						button     :    "but_fecha_pago"
																						});
									</script>
								</div>		
					        </div>			-->			
							<!--Fin-->
							
						
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Imagen Ubicaci�n</div>
							   <div id="CajaInput">
							   <?php
								if($_POST['proy_ubicacion_img']<>"")
								{	$foto=$_POST['proy_ubicacion_img'];
									$b=true;
								}	
								else
								{	$foto='sin_foto.gif';
									$b=false;
								}
								if(($ver)||($cargar))
								{	?>
									<img src="imagenes/proyecto/<?php echo $foto;?>" border="0" width="50"><br>
									<input   name="proy_ubicacion_img" type="file" id="proy_ubicacion_img" /><span class="flechas1"></span>
									<?php
								}
								else 
								{
									?>
									<input  name="proy_ubicacion_img" type="file" id="proy_ubicacion_img" /><span class="flechas1"></span>
									<?php
								}
								?>
								<input   name="fotooculta" type="hidden" id="fotooculta" value="<?php echo $_POST['proy_ubicacion_img'].$_POST['fotooculta1'];?>"/>
							   </div>
							</div>
							<!--Fin-->
							
							
							<!--Inicio-->
							<br/>
							<fieldset style=" margin-left: 159px; width: 610px;">
							<legend class='prueba' id='mapaCargar'>&nbsp;Ubicaci�n&nbsp;</legend>
							<div class='cerrarToogle'>
							
							<div id="ContenedorDiv">							  
							    <div id="CajaInput">
							   <div style="padding:10px 0px 10px 15px">						   
							        <input id="latitud" name="proy_latitud" type="hidden" value="<?php echo $_POST['proy_latitud'];?>">
									<input id="longitud" name="proy_longitud" type="hidden" value = "<?php echo $_POST['proy_longitud'];?>">									
									<input id="map_pais" name="map_pais" type="hidden" value="Bolivia">
									<table border="0" cellspacing="2">
									    <tr>
										    <td><div style="color: #005C89; font-size:12px" >Ciudad</div></td>
											<td><div style="color: #005C89; font-size:12px" >Tipo</div></td>
											<td><div style="color: #005C89; font-size:12px" >Direccion</div></td>
											<td>&nbsp;</td>
										</tr>
										<tr>
										    <td>
												<select id="map_ciudad" name="map_ciudad" style="width:100px; ">
													 <option value="Santa Cruz de la Sierra">Santa Cruz</option>
													 <option value="La Paz">La Paz</option>	
													 <option value="Cochabamba">Cochabamba</option>	
													 <option value="Tarija">Tarija</option>	
													 <option value="Sucre">Sucre</option>	
													 <option value="Oruro">Oruro</option>	
													 <option value="Potosi">Potosi</option>	
													 <option value="Trinidad">Beni</option>	
													 <option value="Cobija">Pando</option>	
												</select>
											</td>
											<td>
												<select id="map_tipo" name="map_tipo" style="width:100px; ">
													 <option value="Calle">Calle</option>
													 <option value="Av.">Avenida</option>											 
												</select>
											</td>
											<td><input id="map_direccion" type="textbox" size="35" ></td>
											<td>
												<input type="button" class="boton" style="width:100px" value="Buscar Direcci�n" onclick="codeAddress()">
											</td>
										</tr>
									</table>
							  </div>
								<div id="map" style="width: 600px; height: 600px;"></div>
							   </div>
							</div>							
							</div>								
							</fieldset>							
							<!--Fin-->


							
						</div>
						
						<div id="ContenedorDiv">
						   <div id="CajaBotones">
								<center>
								<?php
								if(!($ver))
								{
									?>
									<input type="submit" class="boton" name="" value="Guardar">
									<input type="reset" class="boton" name="" value="Cancelar">
									<input type="button" class="boton" name="" value="Volver" onclick="javascript:location.href='<?php echo $red;?>';">
									<?php
								}
								else
								{
									?>
									<input type="button" class="boton" name="" value="Volver" onclick="javascript:location.href='<?php echo $red;?>';">
									<?php
								}
								?>
								</center>
						   </div>
						</div>
				</div>
			</form>
		</div>
		<?php
	}
	
	function emergente()
	{
		$this->formulario->dibujar_cabecera();
		
		$valor=trim($_POST['valor']);
	?>
			
			<script>
				function poner(id,valor)
				{
					opener.document.frm_sentencia.proy_usu_id.value=id;
					opener.document.frm_sentencia.usu_nombre.value=valor;
					window.close();
				}			
			</script>
			<br><center><form name="form" id="form" method="POST" action="gestor.php?mod=proyecto&tarea=AGREGAR&acc=Emergente">
				<table align="center">
					<tr>
						<td class="txt_contenido" colspan="2" align="center">
							<input name="valor" type="text" class="caja_texto" size="30" value="<?php echo $valor;?>">
							<input name="Submit" type="submit" class="boton" value="Buscar">
						</td>
					</tr>
				</table>
			</form><center>
			<?php
			
			$conec= new ADO();
		
			if($valor<>"")
			{
				$sql="select *from usuario where usu_tipo='Constructora' and usu_estado='Habilitado' and (usu_nombre like '%$valor%' or usu_apellido like '%$valor%') ";
			}
			else
			{
				$sql="select *from usuario where usu_tipo='Constructora' and usu_estado='Habilitado'";
			}
			
			$conec->ejecutar($sql);
			
			$num=$conec->get_num_registros();
			
			echo '<table class="tablaLista" cellpadding="0" cellspacing="0">
					<thead>
					<tr>
						<th>
							Nombre
						</th>
						<th>
							Apellido
						</th>
						<th width="80" class="tOpciones">
							Seleccionar
						</th>
				</tr>
				</thead>
				<tbody>
			';
			
			for($i=0;$i<$num;$i++)
			{
				$objeto=$conec->get_objeto();
				
				echo '<tr>
						 <td>'.$objeto->usu_nombre.'</td>
						 <td>'.$objeto->usu_apellido.'</td>
						 <td><a href="javascript:poner('."'".$objeto->usu_id."'".','."'".$objeto->usu_nombre.' '.$objeto->usu_apellido."'".');"><center><img src="images/select.png" border="0" width="20px" height="20px"></center></a></td>
					   </tr>';
				
				$conec->siguiente();
			}
			
			?>
			</tbody></table>
			<?php
	}
	
	function verificar_urlseo($url_seo,$id=0){
		//echo 'el valor de id es: ' . $id . '<br>';
		$sql = "select * from proyecto where proy_seo = '$url_seo'";
		if($id!=0){
			$sql = $sql . " and proy_id!=$id";
		}
		$conec= new ADO();
		$conec->ejecutar($sql,false);
		$rows = $conec->get_datos();
		$contador = count($rows) > 0 ? count($rows) : 0;
		
		/*echo $sql;
		exit(0);*/
		
		return $contador;  
	}
	
	
	function insertar_tcp()
	{
	
		include '../class.SEO.php';
		
		
		$conec= new ADO();
		
		$seo = new SEO();
		$url_seo = $seo->scapeURL($_POST['proy_nombre']);
		$contador = $this->verificar_urlseo($url_seo);
		
		if($contador > 0){
			$url_seo = $url_seo . "-" . $contador;
		}
		
				
		if($_FILES['proy_logo']['name']<>"" || $_FILES['proy_imagen_dis']['name']<>"" || $_FILES['proy_ubicacion_img']['name']<>"")
		{
			$nombre_archivo1 = "";
			if($_FILES['proy_imagen_dis']['name']<>""){
				$result1=$this->subir_imagen_proyecto($nombre_archivo1,$_FILES['proy_imagen_dis']['name'],$_FILES['proy_imagen_dis']['tmp_name']);
			}
			
			$nombre_archivo="";
			if($_FILES['proy_logo']['name']<>""){
				$result=$this->subir_imagen_proyecto($nombre_archivo,$_FILES['proy_logo']['name'],$_FILES['proy_logo']['tmp_name']);
			}
			
			$nombre_archivo2="";
			if($_FILES['proy_ubicacion_img']['name']<>""){
				$result2=$this->subir_imagen_proyecto($nombre_archivo2,$_FILES['proy_ubicacion_img']['name'],$_FILES['proy_ubicacion_img']['tmp_name']);
			}
			
			if( isset( $_POST['habilitar'] ) ){
				$estado = 'Habilitado';
			}else{
				$estado = 'No Habilitado';
			}
			
		/*	$sql="insert into proyecto(proy_nombre,proy_logo,proy_video,proy_descripcion,proy_latitud,proy_longitud,proy_ubicacion,proy_estado,proy_creado,proy_vig_ini,proy_vig_fin,proy_ciu_id,proy_zon_id) values 
							('". $_POST['proy_nombre'] ."','". $nombre_archivo  ."','". $_POST['proy_video'] ."','". $_POST['proy_descripcion'] ."','". $_POST['proy_latitud'] ."','" . $_POST['proy_longitud'] ."','" .
							$_POST['proy_ubicacion'] ."','". $estado ."','" .  date('Y-m-d') ."','". $_POST['proy_vig_ini'] ."','" . $_POST['proy_vig_fin'] ."','". $_POST['proy_ciu_id'] ."','". $_POST['proy_zon_id'] ."')";*/
			
			
			$sql="insert into proyecto(proy_nombre,proy_contacto,proy_logo,proy_ubicacion_img,proy_imagen_dis,proy_video,proy_descripcion,proy_latitud,proy_longitud,proy_ubicacion,proy_estado,proy_creado,proy_vig_ini,proy_vig_fin,proy_ciu_id,proy_zon_id,proy_seo,proy_usu_id) values 
							('". $_POST['proy_nombre'] ."','". $_POST['proy_contacto'] ."','". $nombre_archivo ."','". $nombre_archivo2 ."','". $nombre_archivo1 ."','". $_POST['proy_video'] ."','". $_POST['proy_descripcion'] ."','". $_POST['proy_latitud'] ."','" . $_POST['proy_longitud'] ."','" .
							$_POST['proy_ubicacion'] ."','No Aprobado','" .  date('Y-m-d') ."', '' , ''  ,'". $_POST['inm_ciu_id'] ."','". $_POST['inm_zon_id']. "','". $url_seo ."','". $_POST['proy_usu_id'] ."')";
			
			if(trim($result)<>'' || trim($result1)<>'' )
			{
				$this->formulario->ventana_volver($result.'<br>' .$result1 ,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
			}
			else 
			{
				
				$conec->ejecutar($sql);
				
				/*$proy_id = mysql_insert_id();
				
				for($i=0;$i<count($_POST['categoria']);$i++) {
					switch( $_POST['categoria'][$i] ){
						case 'casa':
								$sql="insert into inmueble(inm_nombre,inm_foto,inm_direccion,inm_precio,inm_superficie,inm_detalle,inm_ciu_id,inm_orden,inm_latitud,inm_longitud,inm_cat_id,inm_zon_id,inm_for_id,inm_tipo_superficie,inm_mon_id) values 
									('".$_POST['proy_nombre']."','".$nombre_archivo."','".$_POST['ubicacion']."','".$_POST['inm_precio']."','".$_POST['inm_superficie']."','".$_POST['inm_detalle']."','".$_POST['inm_ciu_id']."','".$orden."','".$_POST['inm_latitud']."','".$_POST['inm_longitud']."','".$_POST['inm_cat_id']."','".$_POST['inm_zon_id']."','".$_POST['inm_for_id']."','".$_POST['inm_tipo_superficie']."','".$_POST['inm_mon_id']."')";
						break;
					
						case 'departamento':
						break;
					
						case 'terreno':
						break;
					}
				}*/
				
				$mensaje='Proyecto Agregado Correctamente!!!';
				
				$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
			}
		}
		else
		{
			$sql="insert into proyecto(proy_nombre,proy_contacto,proy_video,proy_descripcion,proy_latitud,proy_longitud,proy_ubicacion,proy_estado,proy_creado,proy_vig_ini,proy_vig_fin,proy_usu_id) values 
							('". $_POST['proy_nombre'] . "','".$_POST['proy_contacto'] ."','". $_POST['proy_video'] ."','". $_POST['descripcion'] ."','". $_POST['proy_latitud'] ."','" . $_POST['proy_longitud'] ."','" .
							$_POST['proy_ubicacion'] ."','". $estado ."','" . $_POST['proy_vig_ini'] ."','" . $_POST['proy_vig_fin']."','". $_POST['proy_usu_id'] ."')";

			$conec->ejecutar($sql);

			$mensaje='Proyecto Agregado Correctamente!!!';

			$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
		}
	}
	
	
	function modificar_tcp()
	{
	
		include '../class.SEO.php';
		$conec= new ADO();
		
		if($_FILES['proy_logo']['name']<>"" || $_FILES['proy_imagen_dis']['name']<>"" || $_FILES['proy_ubicacion_img']['name']<>"" )
		{	 	 	 	 	 	 	
			 	 	 	 	
			$proy_logo = "";
			if($_FILES['proy_logo']['name']<>""){
				$result=$this->subir_imagen_proyecto($nombre_archivo,$_FILES['proy_logo']['name'],$_FILES['proy_logo']['tmp_name']);
				$proy_logo = "proy_logo='$nombre_archivo', " ;
			}
			
			$proy_imagen_dis="";
			if($_FILES['proy_imagen_dis']['name']<>""){
				$result1=$this->subir_imagen_proyecto($nombre_archivo1,$_FILES['proy_imagen_dis']['name'],$_FILES['proy_imagen_dis']['tmp_name']);
				$proy_imagen_dis = "proy_imagen_dis='$nombre_archivo1', " ;
			}
			
			
			$proy_ubicacion_img="";
			if($_FILES['proy_ubicacion_img']['name']<>"" ){
				$result2=$this->subir_imagen_proyecto($nombre_archivo2,$_FILES['proy_ubicacion_img']['name'],$_FILES['proy_ubicacion_img']['tmp_name']);
				$proy_ubicacion_img = "proy_ubicacion_img='$nombre_archivo2', " ;
			}
			
			
			
			
			$seo = new SEO();
			$url_seo = $seo->scapeURL($_POST['proy_nombre']);
			$id = $_GET['id'];
			$contador = $this->verificar_urlseo($url_seo,$id);
			
			if($contador > 0){
				$url_seo = $url_seo . "-" . $contador;
			}
			
			$sql="update proyecto set 
								proy_nombre='".$_POST['proy_nombre']."',
								proy_video='".$_POST['proy_video']."',
								proy_descripcion='".$_POST['proy_descripcion']."',".
								$proy_logo .
								$proy_imagen_dis .
								$proy_ubicacion_img .
								"proy_latitud='".$_POST['proy_latitud']."',
								proy_longitud='".$_POST['proy_longitud']."',
								proy_ubicacion='".$_POST['proy_ubicacion']."',
								proy_contacto='".$_POST['proy_contacto']."',
								proy_ciu_id='".$_POST['inm_ciu_id']."',
								proy_seo='". $url_seo ."',
								proy_usu_id='". $_POST['proy_usu_id'] ."',
								proy_zon_id='".$_POST['inm_zon_id']."'
								where proy_id = '".$_GET['id']."'";
								
			echo $sql;
			//exit(0);
			
			if(trim($result)<>'' || trim($result1)<>'')
			{
					
				$this->formulario->ventana_volver($result .' '. $result1 ,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
					
			}
			else
			{
				/*$llave=$_GET['id'];
				
				$mi=trim($_POST['fotooculta']);
				
				if($mi<>"")
				{
					$mifile="imagenes/proyecto/$mi";
				
					@unlink($mifile);
				
				}*/
				
				
				$conec->ejecutar($sql);
			
				$mensaje='Proyecto Modificado Correctamente!!!';
					
				$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
			}
		}
		else
		{
			$seo = new SEO();
			$url_seo = $seo->scapeURL($_POST['proy_nombre']);
			
			$id = $_GET['id'];
			$contador = $this->verificar_urlseo($url_seo,$id);
			
			if($contador > 0){
				$url_seo = $url_seo . "-" . $contador;
			}
			
			$sql="update proyecto set 
								proy_nombre='".$_POST['proy_nombre']."',
								proy_video='".$_POST['proy_video']."',
								proy_descripcion='".$_POST['proy_descripcion']."',						
								proy_latitud='".$_POST['proy_latitud']."',
								proy_longitud='".$_POST['proy_longitud']."',
								proy_ubicacion='".$_POST['proy_ubicacion']."',
								proy_contacto='".$_POST['proy_contacto']."',
								proy_ciu_id='".$_POST['inm_ciu_id']."',
								proy_seo='". $url_seo ."',
								proy_usu_id='". $_POST['proy_usu_id'] ."',
								proy_zon_id='".$_POST['inm_zon_id']."'
								where proy_id = '".$_GET['id']."'";

			$conec->ejecutar($sql);

			$mensaje='Proyecto Modificado Correctamente!!!';

			$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
		}
	}
	
	function formulario_confirmar_eliminacion()
	{
		
		$mensaje='Esta seguro de eliminar el Proyecto?';
		
		$this->formulario->ventana_confirmacion($mensaje,$this->link."?mod=$this->modulo",'proy_id');
	}
	
	function nombre_imagen_proyecto($id)
	{
		$conec= new ADO();
		
		$sql="select proy_logo as imagen from proyecto where proy_id='".$id."'";
		
		$conec->ejecutar($sql);
		
		$objeto=$conec->get_objeto();
		
		return $objeto->imagen;
	}
	
	function eliminar_tcp()
	{
		
			$conec= new ADO();
			
			$llave=$_POST['proy_id'];
			
			$mi=$this->nombre_imagen_proyecto($llave);
			
			if(trim($mi)<>"")
			{
				$mifile="imagenes/proyecto/$mi";
			
				@unlink($mifile);
				
			}
			
			$sql="delete from proyecto where proy_id='".$_POST['proy_id']."'";
			
			$conec->ejecutar($sql);
			
			$sql="delete from inmueble where inm_proy_id='".$_POST['proy_id']."'";
			
			$conec->ejecutar($sql);
			
			$mensaje='Proyecto Eliminado Correctamente!!!';
			
		
		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
	}
	
	
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	function guardar_publicacion()
	{		
		$conec= new ADO();
		
		$sql="select * from proyecto
				where proy_id = '".$_GET['id']."'";
		
		$conec->ejecutar($sql);
		$objeto=$conec->get_objeto();
		
		
		$sql="insert into inmueble(inm_nombre,inm_foto,inm_direccion,inm_precio,inm_superficie,inm_detalle,inm_ciu_id,inm_orden,inm_latitud,inm_longitud,inm_cat_id,inm_zon_id,inm_for_id,inm_tipo_superficie,inm_mon_id,inm_proy_id) values 
									('".$objeto->proy_nombre."','".$objeto->proy_logo."','".$objeto->proy_ubicacion."','".$_POST['inm_precio']."','".$_POST['inm_superficie']."','".$_POST['inm_detalle']."','".$objeto->proy_ciu_id."','0','".$objeto->proy_latitud."','".$objeto->proy_longitud."','".$_POST['inm_cat_id']."','".$objeto->proy_zon_id."','".$_POST['inm_for_id']."','".$_POST['inm_tipo_superficie']."','".$_POST['inm_mon_id']."','".$_GET['id']."')";
		
		$conec->ejecutar($sql);
		// revisar
		$sql="select max(inm_id) as id from inmueble;";
		$conec->ejecutar($sql);
		
		$objeto1=$conec->get_objeto();
		$insert_id = $objeto1->id;
		// revisar
		//$insert_id = mysql_insert_id();
		
		$sql="insert into publicacion(pub_usu_id,pub_creado,pub_vig_ini,pub_vig_fin,pub_monto,pub_inm_id,pub_estado) values 
									 ('" . $objeto->proy_usu_id . "',NOW(),'','','0','".$insert_id."','No Aprobado')";
									 
		$conec->ejecutar($sql);		
		
		$this->limpiar_publicacion();		
	}
	
	function cambiar_caracteres($cadena){
		$cadena = str_replace("�","&ntilde;",$cadena);
		$cadena = str_replace("�","&aacute;",$cadena);
		$cadena = str_replace("�","&eacute;",$cadena);
		$cadena = str_replace("�","&iacute;",$cadena);
		$cadena = str_replace("�","&oacute;",$cadena);
		$cadena = str_replace("�","&uacute;",$cadena);
		$cadena = str_replace("�","&Nacute;",$cadena);
		$cadena = str_replace("�","&Aacute;",$cadena);
		$cadena = str_replace("�","&Eacute;",$cadena);
		$cadena = str_replace("�","&Iacute;",$cadena);
		$cadena = str_replace("�","&Oacute;",$cadena);
		$cadena = str_replace("�","&Uacute;",$cadena);
		return $cadena;
	}
	
	function guardar_foto()
	{		
		$conec= new ADO();		
		
		$this->subir_imagen_foto($nombre_imagen,$_FILES['proy_fot_imagen']['name'],$_FILES['proy_fot_imagen']['tmp_name']);
		
		if($_POST['proy_fot_portada']){
				$sw = 'true';				
				$sql="update proy_foto set 								
								proy_fot_portada='false' 
								where proy_id = '".$_GET['proy_id']."'";
				$conec->ejecutar($sql);
		}else{
				$sw = 'false';
		}
		
		if($_POST['proy_fot_anuncio']){
				$sw_anu = 'true';				
				$sql="update proy_foto set 								
								proy_fot_anuncio='false' 
								where proy_id = '".$_GET['proy_id']."'";
				$conec->ejecutar($sql);
		}else{
				$sw_anu = 'false';
		}
		
		$sql="insert into proy_foto(proy_fot_imagen,proy_id,proy_fot_descripcion,proy_fot_portada,proy_fot_anuncio) values 
									('".$nombre_imagen."','".$_GET['id']."','".$this->cambiar_caracteres($_POST['proy_fot_descripcion'])."','$sw','$sw_anu')";
		
		$conec->ejecutar($sql);	
		
		$this->limpiar_foto();		
	}
	
	function guardar_tipologia()
	{		
		$conec= new ADO();		
		
		$this->subir_imagen_foto($nombre_imagen,$_FILES['tip_imagen']['name'],$_FILES['tip_imagen']['tmp_name']);
		$this->subir_imagen_foto($nombre_imagen_precio,$_FILES['tip_imagen_precio']['name'],$_FILES['tip_imagen_precio']['tmp_name']);
		
		$sql="insert into tipologia(tip_imagen,tip_imagen_precio,tip_titulo,tip_titulo_precio,tip_disponibilidad,tip_descripcion,tip_precio,tip_mts,tip_terreno_mts,tip_proy_id) values 
									('".$nombre_imagen."','". $nombre_imagen_precio ."','". $_POST['tip_titulo'] ."','". $_POST['tip_titulo_precio'] ."','". $_POST['tip_disponibilidad'] ."','". $_POST['tip_descripcion'] ."','". $_POST['tip_precio']."','". $_POST['tip_mts']."','". $_POST['tip_terreno_mts']."','".$_GET['id']."')";
		
		$conec->ejecutar($sql);	
		
		$this->limpiar_tipologia();		
	}
	
	function subir_imagen_foto(&$nombre_imagen,$name,$tmp)
	{	
		 require_once('clases/upload.class.php');

		 $nn=date('d_m_Y_H_i_s_').rand();

		 $upload_class = new Upload_Files();

		 $upload_class->temp_file_name = trim($tmp); 	
		 
		 $upload_class->file_name = $nn.substr(trim($name), -4, 4);

		 $nombre_imagen=$upload_class->file_name;		 		 

		 $upload_class->upload_dir = "imagenes/proyecto/"; 

		 $upload_class->upload_log_dir = "imagenes/proyecto/upload_logs/"; 

		 $upload_class->max_file_size = 1048576; 	

		 $upload_class->ext_array = array(".jpg",".gif",".png");

         $upload_class->crear_thumbnail=false;

		 $valid_ext = $upload_class->validate_extension(); 

		 $valid_size = $upload_class->validate_size(); 

		 $valid_user = $upload_class->validate_user(); 

		 $max_size = $upload_class->get_max_size(); 

		 $file_size = $upload_class->get_file_size(); 

		 $file_exists = $upload_class->existing_file(); 		

		if (!$valid_ext) { 				   

			$result = "La Extension de este Archivo es invalida, Intente nuevamente por favor!"; 

		} 

		elseif (!$valid_size) { 

			$result = "El Tama�o de este archivo es invalido, El maximo tama�o permitido es: $max_size y su archivo pesa: $file_size"; 

		}    

		elseif ($file_exists) { 

			$result = "El Archivo Existe en el Servidor, Intente nuevamente por favor."; 

		} 

		else 
		{		    
			$upload_file = $upload_class->upload_file_with_validation(); 

			if (!$upload_file) { 

				$result = "Su archivo no se subio correctamente al Servidor."; 
			}

			else 
			{ 
					$result = "";
			} 
		} 	

		return $result;	

	}
	
	function subir_imagen_proyecto(&$nombre_imagen,$name,$tmp)
	{	
		 require_once('clases/upload.class.php');

		 $nn=date('d_m_Y_H_i_s_').rand();

		 $upload_class = new Upload_Files();

		 $upload_class->temp_file_name = trim($tmp); 	
		 
		 $upload_class->file_name = $nn.substr(trim($name), -4, 4);

		 $nombre_imagen=$upload_class->file_name;		 		 

		 $upload_class->upload_dir = "imagenes/proyecto/"; 

		 $upload_class->upload_log_dir = "imagenes/proyecto/upload_logs/"; 

		 $upload_class->max_file_size = 1048576; 	

		 $upload_class->ext_array = array(".jpg",".gif",".png");

         $upload_class->crear_thumbnail=false;

		 $valid_ext = $upload_class->validate_extension(); 

		 $valid_size = $upload_class->validate_size(); 

		 $valid_user = $upload_class->validate_user(); 

		 $max_size = $upload_class->get_max_size(); 

		 $file_size = $upload_class->get_file_size(); 

		 $file_exists = $upload_class->existing_file(); 		

		if (!$valid_ext) { 				   

			$result = "La Extension de este Archivo es invalida, Intente nuevamente por favor!"; 

		} 

		elseif (!$valid_size) { 

			$result = "El Tama�o de este archivo es invalido, El maximo tama�o permitido es: $max_size y su archivo pesa: $file_size"; 

		}    

		elseif ($file_exists) { 

			$result = "El Archivo Existe en el Servidor, Intente nuevamente por favor."; 

		} 

		else 
		{		    
			$upload_file = $upload_class->upload_file_with_validation(); 

			if (!$upload_file) { 

				$result = "Su archivo no se subio correctamente al Servidor."; 
			}

			else 
			{ 
					$result = "";
			} 
		} 	

		return $result;	

	}
	
	function subir_imagen_pdf(&$nombre_imagen,$name,$tmp)
	{	
		 require_once('clases/upload.class.php');

		 $nn=date('d_m_Y_H_i_s_').rand();

		 $upload_class = new Upload_Files();

		 $upload_class->temp_file_name = trim($tmp); 	
		 
		 $upload_class->file_name = $nn.substr(trim($name), -4, 4);

		 $nombre_imagen=$upload_class->file_name;		 		 

		 $upload_class->upload_dir = "imagenes/familia_aplicaciones/"; 

		 $upload_class->upload_log_dir = "imagenes/familia_aplicaciones/upload_logs/"; 

		 $upload_class->max_file_size = 1048576; 	

		 $upload_class->ext_array = array(".pdf",".gif");

         $upload_class->crear_thumbnail=false;

		 $valid_ext = $upload_class->validate_extension(); 

		 $valid_size = $upload_class->validate_size(); 

		 $valid_user = $upload_class->validate_user(); 

		 $max_size = $upload_class->get_max_size(); 

		 $file_size = $upload_class->get_file_size(); 

		 $file_exists = $upload_class->existing_file(); 		

		if (!$valid_ext) { 				   

			$result = "La Extension de este Archivo es invalida, Intente nuevamente por favor!"; 

		} 

		elseif (!$valid_size) { 

			$result = "El Tama�o de este archivo es invalido, El maximo tama�o permitido es: $max_size y su archivo pesa: $file_size"; 

		}    

		elseif ($file_exists) { 

			$result = "El Archivo Existe en el Servidor, Intente nuevamente por favor."; 

		} 

		else 
		{		    
			$upload_file = $upload_class->upload_file_with_validation(); 

			if (!$upload_file) { 

				$result = "Su archivo no se subio correctamente al Servidor."; 
			}

			else 
			{ 
					$result = "";
			} 
		} 	

		return $result;	

	}
	
	function subir_imagen(&$nombre_imagen,$name,$tmp)
	{	
		 require_once('clases/upload.class.php');

		 $nn=date('d_m_Y_H_i_s_').rand();

		 $upload_class = new Upload_Files();

		 $upload_class->temp_file_name = trim($tmp); 	
		 
		 $upload_class->file_name = $nn.substr(trim($name), -4, 4);

		 $nombre_imagen=$upload_class->file_name;		 		 

		 $upload_class->upload_dir = "imagenes/aplicaciones_productos/"; 

		 $upload_class->upload_log_dir = "imagenes/aplicaciones_productos/upload_logs/"; 

		 $upload_class->max_file_size = 1048576; 	

		 $upload_class->ext_array = array(".jpg",".gif",".png");

         $upload_class->crear_thumbnail=false;

		 $valid_ext = $upload_class->validate_extension(); 

		 $valid_size = $upload_class->validate_size(); 

		 $valid_user = $upload_class->validate_user(); 

		 $max_size = $upload_class->get_max_size(); 

		 $file_size = $upload_class->get_file_size(); 

		 $file_exists = $upload_class->existing_file(); 		

		if (!$valid_ext) { 				   

			$result = "La Extension de este Archivo es invalida, Intente nuevamente por favor!"; 

		} 

		elseif (!$valid_size) { 

			$result = "El Tama�o de este archivo es invalido, El maximo tama�o permitido es: $max_size y su archivo pesa: $file_size"; 

		}    

		elseif ($file_exists) { 

			$result = "El Archivo Existe en el Servidor, Intente nuevamente por favor."; 

		} 

		else 
		{		    
			$upload_file = $upload_class->upload_file_with_validation(); 

			if (!$upload_file) { 

				$result = "Su archivo no se subio correctamente al Servidor."; 
			}

			else 
			{ 
					$result = "";
			} 
		} 	

		return $result;	

	}

	function limpiar_publicacion()
	{
		$_POST['inm_cat_id']="";
		$_POST['inm_for_id']="";
		$_POST['inm_superficie']="";
		$_POST['inm_precio']="";
		$_POST['inm_mon_id']="";
		$_POST['inm_detalle']="";
		$_POST['inm_tipo_superficie']="";
		
	}
	
	function limpiar_tipologia()
	{
		$_POST['tip_descripcion']="";
		$_POST['tip_precio']="";
		$_POST['tip_imagen']="";
		
	}
	
	function limpiar_foto()
	{
		$_FILES['proy_fot_imagen']="";
		
	}

	

	function datos_publicacion()
	{

		if($_POST)
		{
			require_once('clases/validar.class.php');

			$num=0;
			
			$valores[$num]["etiqueta"]="Categoria";
			$valores[$num]["valor"]=$_POST['inm_cat_id'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			$valores[$num]["etiqueta"]="Forma";
			$valores[$num]["valor"]=$_POST['inm_for_id'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			$valores[$num]["etiqueta"]="Superficie";
			$valores[$num]["valor"]=$_POST['inm_superficie'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			$valores[$num]["etiqueta"]="Precio";
			$valores[$num]["valor"]=$_POST['inm_precio'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			$valores[$num]["etiqueta"]="Moneda";
			$valores[$num]["valor"]=$_POST['inm_mon_id'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			$valores[$num]["etiqueta"]="Tipo superficie";
			$valores[$num]["valor"]=$_POST['inm_tipo_superficie'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;

			$val=NEW VALIDADOR;

			$this->mensaje="";

			if($val->validar($valores))
			{
				return true;
			}
			else
			{
				$this->mensaje=$val->mensaje;

				return false;

			}

		}

			return false;

	}
	
	function datos_tipologia()
	{

		if($_POST)
		{
			require_once('clases/validar.class.php');

			$num=0;
			
			$valores[$num]["etiqueta"]="Titulo";
			$valores[$num]["valor"]=$_POST['tip_titulo'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			
			/*$valores[$num]["etiqueta"]="Descripcion";
			$valores[$num]["valor"]=$_POST['tip_descripcion'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;*/
			
			$valores[$num]["etiqueta"]="Precio";
			$valores[$num]["valor"]=$_POST['tip_precio'];
			$valores[$num]["tipo"]="real";
			$valores[$num]["requerido"]=true;
			$num++;
			
			$valores[$num]["etiqueta"]="Disponibilidad";
			$valores[$num]["valor"]=$_POST['tip_disponibilidad'];
			$valores[$num]["tipo"]="numero";
			$valores[$num]["requerido"]=true;
			$num++;
			
			$valores[$num]["etiqueta"]="Superficie";
			$valores[$num]["valor"]=$_POST['tip_mts'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			

			$val=NEW VALIDADOR;

			$this->mensaje="";

			if($val->validar($valores))
			{
				return true;
			}
			else
			{
				$this->mensaje=$val->mensaje;

				return false;

			}

		}

			return false;

	}
	


	

	function formulario_tcp_publicacion($tipo)

	{

		$url=$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER';

		$re=$url;

		$enlace="<a href='$url'>Proyectos</a>";

		echo "<tr>
					<td class='busqueda_enlace' colspan='3'>
						Volver a $enlace
					</td>
				</tr>"; 		

		switch ($tipo)
		{

			case 'ver':{
						$ver=true;
						
						break;
						}

			case 'cargar':{

						$cargar=true;

						break;
						}
		}

		if(!($ver))
		{
			$url.="&tarea=".$_GET['tarea'];
		}

		if($cargar)
		{
			$url.='&id='.$_GET['id'];
		}
		
		if($_GET['acc']=='MODIFICAR_PUBLICACION')
		{
			$url.='&acc=MODIFICAR_PUBLICACION';
		}
		
		$this->formulario->dibujar_tarea('PUBLICACION');
		
		if($this->mensaje<>"")
		{
			$this->formulario->mensaje('Error',$this->mensaje);
		}

		?>
		<?php include_once("js/fckeditor/fckeditor.php"); ?>	  

			<div id="Contenedor_NuevaSentencia">

               <form id="frm_sentencia" name="frm_sentencia" action="<?php echo $url.'&inm_id='.$_GET['inm_id'];?>" method="POST" enctype="multipart/form-data">  

				<div id="FormSent" style="width:900px;">

                    

					<div class="Subtitulo">Publicacion</div>

                    <div id="ContenedorSeleccion">
						
						<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Categoria</div>
							   <div id="CajaInput">							   
							   <select name="inm_cat_id" class="caja_texto" onchange="">
							   <option value="">Seleccione</option>
							   <?php 	                                 							   
									$fun=NEW FUNCIONES;		
									$fun->combo("select cat_id as id,cat_nombre as nombre from categoria",$_POST['inm_cat_id']);												
								?>
							   </select>							   
							   </div>
							   
							</div>
						<!--Fin-->
						
						<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Tipo</div>
							   <div id="CajaInput">
							   
							   <select name="inm_for_id" class="caja_texto">
							   <option value="">Seleccione</option>
							   <?php 	
                                 							   
									$fun=NEW FUNCIONES;		
									$fun->combo("select for_id as id,for_descripcion as nombre from forma",$_POST['inm_for_id']);				
								
								?>
							   </select>
							  
							   </div>
							   
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Superficie</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto" name="inm_superficie" id="inm_superficie" maxlength="250"  size="10" value="<?php echo $_POST['inm_superficie'];?>">
							   <select name="inm_tipo_superficie" class="caja_texto">
									<option value="" >Seleccione</option>
									<option value="Metros Cuadrados" <?php if($_POST['inm_tipo_superficie']=='Metros Cuadrados') echo 'selected="selected"'; ?>>Metros Cuadrados</option>
									<option value="Hectareas" <?php if($_POST['inm_tipo_superficie']=='Hectareas') echo 'selected="selected"'; ?>>Hectareas</option>
									</select>
							   </div>
							</div>
							<!--Fin-->
							
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Precio</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto" name="inm_precio" id="inm_precio" maxlength="250"  size="40" value="<?php echo $_POST['inm_precio'];?>">
							   </div>
							</div>
							<!--Fin-->	
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Moneda</div>
							   <div id="CajaInput">							   
							   <select name="inm_mon_id" class="caja_texto">
							   <option value="">Seleccione</option>
							   <?php 	                                 							   
									$fun=NEW FUNCIONES;		
									$fun->combo("select mon_id as id,mon_descripcion as nombre from moneda",$_POST['inm_mon_id']);												
								?>
							   </select>							   
							   </div>
							   
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">*</span>Detalle</div>
							   <div id="CajaInput">							      
								<?php
								$pagina=$_POST['inm_detalle'];
								$oFCKeditor = new FCKeditor('inm_detalle') ;
								$oFCKeditor->BasePath = 'js/fckeditor/';
								$oFCKeditor->Width  = '650' ;
								$oFCKeditor->Height = '200' ;
								//$oFCKeditor->ToolbarSet = 'Basic' ;
								$oFCKeditor->Value = $pagina;
								$oFCKeditor->Create() ;
								?>     
							   </div>
							</div>
							<!--Fin-->	
						
                    </div>

					<div id="ContenedorDiv">

                           <div id="CajaBotones">

								<center>

								<input type="submit" class="boton" name="" value="Enviar">

							    <input type="reset" class="boton" name="" value="Cancelar">

								<input type="button" class="boton" name="" value="Volver" onclick="location.href='<?php if($_GET['acc']=="MODIFICAR_PRODUCTO") echo $this->link.'?mod='.$this->modulo."&tarea=PRODUCTOS&id=".$_GET['id']; else echo $this->link.'?mod='.$this->modulo.'&tarea=ACCEDER';?>';">

								</center>

						   </div>

                        </div>

                </div>

            </div>					

		<?php
	}
	
	
	function formulario_tcp_foto($tipo)

	{

		$url=$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER';

		$re=$url;

		$enlace="<a href='$url'>Proyectos</a>";

		echo "<tr>
					<td class='busqueda_enlace' colspan='3'>
						Volver a $enlace
					</td>
				</tr>"; 		

		switch ($tipo)
		{

			case 'ver':{
						$ver=true;
						
						break;
						}

			case 'cargar':{

						$cargar=true;

						break;
						}
		}

		if(!($ver))
		{
			$url.="&tarea=".$_GET['tarea'];
		}
		
		if($_GET['acc']=='MODIFICAR_FOTO')
		{
			$url.='&acc=MODIFICAR_FOTO';
		}
		
		if($cargar)
		{
			$url.='&id='.$_GET['id'];
		}
	
		
		$this->formulario->dibujar_tarea('FOTO');
		
		if($this->mensaje<>"")
		{
			$this->formulario->mensaje('Error',$this->mensaje);
		}

		?>


			<div id="Contenedor_NuevaSentencia">

               <form id="frm_sentencia" name="frm_sentencia" action="<?php echo $url.'&proy_fot_id='.$_GET['proy_fot_id'];?>" method="POST" enctype="multipart/form-data">  

				<div id="FormSent" style="width:900px;">

                    

					<div class="Subtitulo">Fotos</div>

                    <div id="ContenedorSeleccion">
						
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Imagen </div>
							   <div id="CajaInput">
							   <?php
								if($_POST['proy_fot_imagen']<>"")
								{	$foto=$_POST['proy_fot_imagen'];
									$b=true;
								}	
								else
								{	$foto='sin_foto.gif';
									$b=false;
								}
								if(($ver)||($cargar))
								{	?>
									<img src="imagenes/proyecto/<?php echo $foto;?>" border="0" width="50"><br>
									<input   name="proy_fot_imagen" type="file" id="proy_fot_imagen" /><span class="flechas1"></span>
									<?php
								}
								else 
								{
									?>
									<input  name="proy_fot_imagen" type="file" id="proy_fot_imagen" /><span class="flechas1"></span>
									<?php
								}
								?>
								<input   name="fotooculta" type="hidden" id="fotooculta" value="<?php echo $_POST['proy_fot_imagen'].$_POST['fotooculta'];?>"/>
							   </div>
							</div>
							<!--Fin-->
							
                            
                            <!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Descripcion</div>
							   <div id="CajaInput">
							   <textarea  class="caja_texto" name="proy_fot_descripcion" id="proy_fot_descripcion"><?php echo $_POST['proy_fot_descripcion'];?></textarea>   
							   </div>
							</div>
							<!--Fin-->
                            
                            
						
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Portada?</div>
							   <div id="CajaInput">
							   <input type="checkbox" name="proy_fot_portada" <?php if($_POST['proy_fot_portada']=='true'){ echo 'checked="checked"';} ?>  />							   
							   </div>
							</div>

							
							
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Anuncio?</div>
							   <div id="CajaInput">
							   <input type="checkbox" name="proy_fot_anuncio" <?php if($_POST['proy_fot_anuncio']=='true'){ echo 'checked="checked"';} ?>  />							   
							   </div>
							</div>
						
							
						
                    </div>

					<div id="ContenedorDiv">

                           <div id="CajaBotones">

								<center>

								<input type="submit" class="boton" name="" value="Enviar">

							    <input type="reset" class="boton" name="" value="Cancelar">

								<input type="button" class="boton" name="" value="Volver" onclick="location.href='<?php if($_GET['acc']=="MODIFICAR_FOTO") echo $this->link.'?mod='.$this->modulo."&tarea=FOTOS&id=".$_GET['id']; else echo $this->link.'?mod='.$this->modulo.'&tarea=ACCEDER';?>';">

								</center>

						   </div>

                        </div>

                </div>

            </div>					

		<?php
	}

	
	function formulario_tcp_tipologia($tipo)

	{

		$url=$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER';

		$re=$url;

		$enlace="<a href='$url'>Proyectos</a>";

		echo "<tr>
					<td class='busqueda_enlace' colspan='3'>
						Volver a $enlace
					</td>
				</tr>"; 		

		switch ($tipo)
		{

			case 'ver':{
						$ver=true;
						
						break;
						}

			case 'cargar':{

						$cargar=true;

						break;
						}
		}

		if(!($ver))
		{
			$url.="&tarea=".$_GET['tarea'];
		}

		if($cargar)
		{
			$url.='&id='.$_GET['id'];
		}
	
		if($_GET['acc']=='MODIFICAR_TIPOLOGIA')
		{
			$url.='&acc=MODIFICAR_TIPOLOGIA';
		}
		
		$this->formulario->dibujar_tarea('TIPOLOGIA');
		
		if($this->mensaje<>"")
		{
			$this->formulario->mensaje('Error',$this->mensaje);
		}
		
		
		?>
		<?php include_once("js/fckeditor/fckeditor.php"); ?>	  

			<div id="Contenedor_NuevaSentencia">

               <form id="frm_sentencia" name="frm_sentencia" action="<?php echo $url.'&tip_id='.$_GET['tip_id'];?>" method="POST" enctype="multipart/form-data">  

				<div id="FormSent" style="width:900px;">

                    

					<div class="Subtitulo">Publicacion</div>

                    <div id="ContenedorSeleccion">
						
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Imagen </div>
							   <div id="CajaInput">
							   <?php
								if($_POST['tip_imagen']<>"")
								{	$foto=$_POST['tip_imagen'];
									$b=true;
								}	
								else
								{	$foto='sin_foto.gif';
									$b=false;
								}
								if(($ver)||($cargar))
								{	?>
									<img src="imagenes/proyecto/<?php echo $foto;?>" border="0" width="50"><br>
									<input   name="tip_imagen" type="file" id="tip_imagen" /><span class="flechas1"></span>
									<?php
								}
								else 
								{
									?>
									<input  name="tip_imagen" type="file" id="tip_imagen" /><span class="flechas1"></span>
									<?php
								}
								?>
								<input   name="fotooculta" type="hidden" id="fotooculta" value="<?php echo $_POST['tip_imagen'].$_POST['fotooculta']; ?>"/>
							   </div>
							</div>
							<!--Fin-->
							
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Imagen Precio </div>
							   <div id="CajaInput">
							   <?php
								if($_POST['tip_imagen_precio']<>"")
								{	$foto=$_POST['tip_imagen_precio'];
									$b=true;
								}	
								else
								{	$foto='sin_foto.gif';
									$b=false;
								}
								if(($ver)||($cargar))
								{	?>
									<img src="imagenes/proyecto/<?php echo $foto;?>" border="0" width="50"><br>
									<input   name="tip_imagen_precio" type="file" id="tip_imagen_precio" /><span class="flechas1"></span>
									<?php
								}
								else 
								{
									?>
									<input  name="tip_imagen_precio" type="file" id="tip_imagen_precio" /><span class="flechas1"></span>
									<?php
								}
								?>
								<input   name="fotooculta" type="hidden" id="fotooculta1" value="<?php echo $_POST['tip_imagen_precio'].$_POST['fotooculta1']; ?>"/>
							   </div>
							</div>
							<!--Fin-->
							
							
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Titulo</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto" name="tip_titulo" id="tip_titulo" maxlength="250"  size="40" value="<?php echo $_POST['tip_titulo']; ?>">
							   </div>
							</div>
							<!--Fin-->
                            
                            <!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Titulo Precio</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto" name="tip_titulo_precio" id="tip_titulo_precio" maxlength="250"  size="40" value="<?php echo $_POST['tip_titulo_precio']; ?>">
							   </div>
							</div>
							<!--Fin-->	
							
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Precio</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto" name="tip_precio" id="tip_precio" maxlength="250"  size="40" value="<?php echo $_POST['tip_precio'];?>"> 
                               <input type="checkbox" name="tip_ver_precio" <?php echo $_POST['tip_ver_precio'] == 1 ? 'checked="checked"': "";?> value="1" /> <label for="tip_ver_precio">Mostrar en Precios</label>
							   </div>
							</div>
							<!--Fin-->	
							
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Descripcion</div>
							   <div id="CajaInput">
							   <!-- <textarea  class="caja_texto" name="tip_descripcion" id="tip_descripcion"><?php echo $_POST['tip_descripcion'];?></textarea> -->
							   <?php
								$pagina=$_POST['tip_descripcion'];
								$oFCKeditor = new FCKeditor('tip_descripcion') ;
								$oFCKeditor->BasePath = 'js/fckeditor/';
								$oFCKeditor->Width  = '650' ;
								$oFCKeditor->Height = '200' ;
								//$oFCKeditor->ToolbarSet = 'Basic' ;
								$oFCKeditor->Value = $pagina;
								$oFCKeditor->Create() ;
								?>     
							   </div>
							</div>
							<!--Fin-->	
							
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Disponibilidad</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto" name="tip_disponibilidad" id="tip_disponibilidad" maxlength="250"  size="40" value="<?php echo $_POST['tip_disponibilidad'];?>">
							   </div>
							</div>
							<!--Fin-->	
							
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Superficie Construida</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto" name="tip_mts" id="tip_mts" maxlength="250"  size="40" value="<?php echo $_POST['tip_mts'];?>">
                               <input type="checkbox" name="tip_ver_supcon" <?php echo $_POST['tip_ver_supcon'] == 1 ? 'checked="checked"': "";?> value="1" /> <label for="tip_ver_supcon">Mostrar en Precios</label>
							   </div>
							</div>
							<!--Fin-->	
							
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Superficie Total</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto" name="tip_terreno_mts" id="tip_terreno_mts" maxlength="250"  size="40" value="<?php echo $_POST['tip_terreno_mts'];?>">
                               <input type="checkbox" name="tip_ver_suptot" <?php echo $_POST['tip_ver_suptot'] == 1 ? 'checked="checked"': "";?> value="1" /> <label for="tip_ver_suptot">Mostrar en Precios</label>
							   </div>
							</div>
							<!--Fin-->	
							
						
                    </div>

					<div id="ContenedorDiv">

                           <div id="CajaBotones">

								<center>

								<input type="submit" class="boton" name="" value="Enviar">

							    <input type="reset" class="boton" name="" value="Cancelar">

								<input type="button" class="boton" name="" value="Volver" onclick="location.href='<?php if($_GET['acc']=="MODIFICAR_TIPOLOGIA") echo $this->link.'?mod='.$this->modulo."&tarea=TIPOLOGIA&id=".$_GET['id']; else echo $this->link.'?mod='.$this->modulo.'&tarea=ACCEDER';?>';">

								</center>

						   </div>

                        </div>

                </div>

            </div>					

		<?php
	}
	

	function dibujar_encabezado_publicacion()

	{

		?><div style="clear:both;"></div><center>

		<table class="tablaLista" cellpadding="0" cellspacing="0" width="60%">
			<thead>
				<tr>

					<th >

						Nro

					</th>
					
					<th >

						Precio

					</th>
					
					<th >

						Detalle

					</th>
								
					

					<th class="tOpciones" width="100px">

						Opciones

					</th>

				</tr>
			</thead>
			<tbody>
		<?PHP

	}
	
	function dibujar_encabezado_tipologia()

	{

		?><div style="clear:both;"></div><center>

		<table class="tablaLista" cellpadding="0" cellspacing="0" width="60%">
			<thead>
				<tr>
					
					<th >

						Titulo

					</th>
					
					<th >

						Descripcion

					</th>
					
					<th >

						Precio

					</th>
					
					<th >

						Disponibilidad

					</th>
					
					<th >

						Imagen

					</th>
								
					

					<th class="tOpciones" width="100px">

						Opciones

					</th>

				</tr>
			</thead>
			<tbody>
		<?PHP

	}
	
	function dibujar_encabezado_foto()

	{

		?><div style="clear:both;"></div><center>

		<table class="tablaLista" cellpadding="0" cellspacing="0" width="60%">
			<thead>
				<tr>

					<th >

						Imagen

					</th>
                    
                    <th >

						Descripcion

					</th>
					

					<th class="tOpciones" width="100px">

						Opciones

					</th>

				</tr>
			</thead>
			<tbody>
		<?PHP

	}


	

	function mostrar_busqueda_tipologia()
	{

		$conec=new ADO();

		
		
		$sql="select * 
		from 
		tipologia 
		where 
		tip_proy_id='".$_GET['id']."'  order by tip_id desc";
		
		$conec->ejecutar($sql);

		$num=$conec->get_num_registros();

		for($i=0;$i<$num;$i++)
		{
			$objeto=$conec->get_objeto();

			echo '<tr class="busqueda_campos">';

			?>
			
				<td align="left">

					<?php echo $objeto->tip_titulo; ?>
				
				</td>		
				
				<td align="left">

					<?php echo $objeto->tip_descripcion; ?>

				</td>
				
				<td align="left">

					<?php echo $objeto->tip_precio; ?>
				
				</td>		
				
				<td align="left">

					<?php echo $objeto->tip_disponibilidad; ?>
				
				</td>		
				
				<td align="left">

						<img width="100" height="100" src="imagenes/proyecto/<?php echo $objeto->tip_imagen; ?>" />

				</td>
						

				<td>

					<center>

					<a href="gestor.php?mod=proyecto&tarea=TIPOLOGIA&acc=ELIMINAR&tip_id=<?php echo $objeto->tip_id;?>&id=<?php echo $_GET['id'];?>"><img src="images/b_drop.png" alt="ELIMINAR" title="ELIMINAR" border="0"></a>

					<a href="gestor.php?mod=proyecto&tarea=TIPOLOGIA&acc=MODIFICAR_TIPOLOGIA&tip_id=<?php echo $objeto->tip_id;?>&id=<?php echo $_GET['id'];?>"><img src="images/b_edit.png" alt="MODIFICAR" title="MODIFICAR" border="0"></a>

					</center>

				</td>

			<?php

			echo "</tr>";

			

			

			$conec->siguiente();

		}

		echo "</tbody></table></center><br>";
	}
	
	function mostrar_busqueda_publicacion()
	{

		$conec=new ADO();

		
		$sql="select * 
		from 
		inmueble 
		where 
		inm_proy_id='".$_GET['id']."'  order by inm_id desc";
		
		$conec->ejecutar($sql);

		$num=$conec->get_num_registros();

		for($i=0;$i<$num;$i++)
		{
			$objeto=$conec->get_objeto();

			echo '<tr class="busqueda_campos">';

			?>
				
				<td align="left">

					<?php echo $i+1;?>

				</td>
				
				<td align="left">

					<?php echo $objeto->inm_precio; ?>
				
				</td>		
				
				<td align="left">

					&nbsp;<?php echo $objeto->inm_detalle;?>

				</td>
						

				<td>

					<center>

					<a href="gestor.php?mod=proyecto&tarea=PUBLICACIONES&acc=ELIMINAR&inm_id=<?php echo $objeto->inm_id;?>&id=<?php echo $_GET['id'];?>"><img src="images/b_drop.png" alt="ELIMINAR" title="ELIMINAR" border="0"></a>

					<a href="gestor.php?mod=proyecto&tarea=PUBLICACIONES&acc=MODIFICAR_PUBLICACION&inm_id=<?php echo $objeto->inm_id;?>&id=<?php echo $_GET['id'];?>"><img src="images/b_edit.png" alt="MODIFICAR" title="MODIFICAR" border="0"></a>

					</center>

				</td>

			<?php

			echo "</tr>";

			

			

			$conec->siguiente();

		}

		echo "</tbody></table></center><br>";
	}
	
	function mostrar_busqueda_foto()
	{

		$conec=new ADO();

		$sql="select * 
		from 
		proy_foto 
		where 
		proy_id='".$_GET['id']."'  order by proy_fot_id desc";
		
		$conec->ejecutar($sql);

		$num=$conec->get_num_registros();

		for($i=0;$i<$num;$i++)
		{
			$objeto=$conec->get_objeto();

			echo '<tr class="busqueda_campos">';

			?>
				
				<td align="left">

					<img width="100" height="100" src="imagenes/proyecto/<?php echo $objeto->proy_fot_imagen; ?>">
				
				</td>
                
                <td align="left">

					<?php echo $objeto->proy_fot_descripcion; ?>
				
				</td>		
			
				<td>

					<center>

					<a href="gestor.php?mod=proyecto&tarea=FOTOS&acc=ELIMINAR&proy_fot_id=<?php echo $objeto->proy_fot_id;?>&id=<?php echo $_GET['id'];?>"><img src="images/b_drop.png" alt="ELIMINAR" title="ELIMINAR" border="0"></a>
					
					<a href="gestor.php?mod=proyecto&tarea=FOTOS&acc=MODIFICAR_FOTO&proy_fot_id=<?php echo $objeto->proy_fot_id;?>&id=<?php echo $_GET['id'];?>"><img src="images/b_edit.png" alt="MODIFICAR" title="MODIFICAR" border="0"></a>	
					
					</center>

				</td>

			<?php

			echo "</tr>";

			

			

			$conec->siguiente();

		}

		echo "</tbody></table></center><br>";
	}
	
	
	function eliminar_publicacion($id_producto)
	{
		$conec= new ADO();

		$llave=$id_producto;
				
		/*$mi=$this->nombre_imagen($llave);*/
		
		$sql="delete from inmueble where inm_id='".$id_producto."'";

		$conec->ejecutar($sql);
		
		$this->mensaje='Inmueble eliminado correctamente';
	}
	
	
	function eliminar_tipologia($id_producto)
	{
		$conec= new ADO();

		$llave=$id_producto;
				
		/*$mi=$this->nombre_imagen($llave);*/
		
		$sql="delete from tipologia where tip_id='".$id_producto."'";

		$conec->ejecutar($sql);
		
		$this->mensaje='Tipologia eliminada correctamente';
	}
	
	
	function eliminar_foto($id_producto)
	{
		$conec= new ADO();

		$llave=$id_producto;
				
		/*$mi=$this->nombre_imagen($llave);*/
		
		$sql="delete from proy_foto where proy_fot_id='".$id_producto."'";

		$conec->ejecutar($sql);
		
		$this->mensaje='Foto eliminada correctamente';
	}
	
	function nombre_imagen($id)
	{
		$conec= new ADO();
		
		$sql="select pro_foto from aplicaciones_productos where pro_id='".$id."'";
		
		$conec->ejecutar($sql);
		
		$objeto=$conec->get_objeto();
		
		return $objeto->pro_foto;
	}
		
	function modificar_publicacion()
	{		
		$conec= new ADO();

		
		$mensaje='Publicacion Modificada Correctamente';

		$sql="update inmueble set 
		inm_cat_id='".$_POST['inm_cat_id']."',
		inm_for_id='".$_POST['inm_for_id']."',
		inm_superficie='".$_POST['inm_superficie']."',
		inm_precio='".$_POST['inm_precio']."',
		inm_mon_id='".$_POST['inm_mon_id']."',
		inm_detalle='".$_POST['inm_detalle']."',
		inm_tipo_superficie='".$_POST['inm_tipo_superficie']."'
		where inm_id= '".$_GET['inm_id']."'";

		$conec->ejecutar($sql);
		
		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=PUBLICACIONES&id='.$_GET['id']);

	}
	
	function modificar_tipologia()
	{		
		$conec= new ADO();
		
		if($_FILES['tip_imagen']['name']<>"" || $_FILES['tip_imagen_precio']['name']<>"")
		{	 	 	 	 	 	 	
			if( $_FILES['tip_imagen']['name']<>"" ){ 	 	 	 	
				$result=$this->subir_imagen_proyecto($nombre_archivo,$_FILES['tip_imagen']['name'],$_FILES['tip_imagen']['tmp_name']);
			}
			
			if( $_FILES['tip_imagen_precio']['name']<>"" ){ 	 	 	 	
				$result=$this->subir_imagen_proyecto($nombre_archivo_precio,$_FILES['tip_imagen_precio']['name'],$_FILES['tip_imagen_precio']['tmp_name']);
			}
			
	
			$sql="update tipologia set 
								tip_descripcion='".$_POST['tip_descripcion']."'," ;
								
			if(	$nombre_archivo!='' ){
				$sql = $sql . "tip_imagen='".$nombre_archivo."',";
			}
			
			if(	$nombre_archivo_precio!='' ){
				$sql = $sql . "tip_imagen_precio='".$nombre_archivo_precio."',";
			}
			
			$check_precio = 0;
			$check_supcon = 0;
			$check_suptot = 0;
			
			if(isset($_POST['tip_ver_precio']) && $_POST['tip_ver_precio'] != ""){
				$check_precio = 1;
			}
			if(isset($_POST['tip_ver_supcon']) && $_POST['tip_ver_supcon'] != ""){
				$check_supcon = 1;
			}
			if(isset($_POST['tip_ver_suptot']) && $_POST['tip_ver_suptot'] != ""){
				$check_suptot = 1;
			}
			
			
								
				$sql = $sql . "tip_precio='".$_POST['tip_precio']."',								
						tip_titulo='".$_POST['tip_titulo']."',
						tip_titulo_precio='".$_POST['tip_titulo_precio']."',								
						tip_disponibilidad='".$_POST['tip_disponibilidad']."',	
						tip_mts='".$_POST['tip_mts']."',
						tip_ver_precio=".$check_precio.",
						tip_ver_supcon=".$check_supcon.",
						tip_ver_suptot=".$check_suptot.",
						tip_terreno_mts='".$_POST['tip_terreno_mts']."' 								
					where tip_id = '".$_GET['tip_id']."'";
			
			if(trim($result)<>'')
			{
					
				$this->formulario->ventana_volver($result,$this->link.'?mod='.$this->modulo.'&tarea=TIPOLOGIA&id='.$_GET['id']);
					
			}
			else
			{
				$llave=$_GET['id'];
				
				$mi=trim($_POST['fotooculta']);
				
				if($mi<>"")
				{
					$mifile="imagenes/proyecto/$mi";
				
					@unlink($mifile);
				
				}
				
				
				$conec->ejecutar($sql);
			
				$mensaje='Tipologia Modificada Correctamente!!!';
					
				$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=TIPOLOGIA&id='.$_GET['id']);
			}
		}
		else
		{
			
			$check_precio = 0;
			$check_supcon = 0;
			$check_suptot = 0;
			
			if(isset($_POST['tip_ver_precio']) && $_POST['tip_ver_precio'] != ""){
				$check_precio = 1;
			}
			if(isset($_POST['tip_ver_supcon']) && $_POST['tip_ver_supcon'] != ""){
				$check_supcon = 1;
			}
			if(isset($_POST['tip_ver_suptot']) && $_POST['tip_ver_suptot'] != ""){
				$check_suptot = 1;
			}
			
			$sql="update tipologia set 
								tip_descripcion='".$_POST['tip_descripcion']."',
								tip_titulo='".$_POST['tip_titulo']."',
								tip_titulo_precio='".$_POST['tip_titulo_precio']."',
								tip_disponibilidad='".$_POST['tip_disponibilidad']."',	
								tip_mts='".$_POST['tip_mts']."',
								tip_ver_precio=".$check_precio.",
								tip_ver_supcon=".$check_supcon.",
								tip_ver_suptot=".$check_suptot.",		
								tip_terreno_mts='".$_POST['tip_terreno_mts']."',
								tip_precio='".$_POST['tip_precio']."' 
								where tip_id = '".$_GET['tip_id']."'";

			$conec->ejecutar($sql);

			$mensaje='Tipologia Modificada Correctamente!!!';

			$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=TIPOLOGIA&id='.$_GET['id']);
		}

	}
	
	function cargar_datos_publicacion()
	{
		$conec=new ADO();

		$sql="select * from inmueble where inm_id='".$_GET['inm_id']."'";

		$conec->ejecutar($sql);

		$objeto=$conec->get_objeto();
		
		$_POST['inm_cat_id']=$objeto->inm_cat_id;
		$_POST['inm_for_id']=$objeto->inm_for_id;
		$_POST['inm_superficie']=$objeto->inm_superficie;
		$_POST['inm_precio']=$objeto->inm_precio;
		$_POST['inm_mon_id']=$objeto->inm_mon_id;
		$_POST['inm_detalle']=$objeto->inm_detalle;
		$_POST['inm_tipo_superficie']=$objeto->inm_tipo_superficie;
		
	}
	
	function cargar_datos_tipologia()
	{
		$conec=new ADO();

		$sql="select * from tipologia where tip_id='".$_GET['tip_id']."'";

		$conec->ejecutar($sql);

		$objeto=$conec->get_objeto();
		
		$_POST['tip_descripcion']=$objeto->tip_descripcion;
		$_POST['tip_imagen']=$objeto->tip_imagen;
		$_POST['tip_imagen_precio']=$objeto->tip_imagen_precio;
		$_POST['tip_precio']=$objeto->tip_precio;
		$_POST['tip_titulo']=$objeto->tip_titulo;
		$_POST['tip_titulo_precio']=$objeto->tip_titulo_precio;
		$_POST['tip_disponibilidad']=$objeto->tip_disponibilidad;		
		$_POST['tip_mts']=$objeto->tip_mts;
		$_POST['tip_terreno_mts']=$objeto->tip_terreno_mts;
		
		
		$_POST['tip_ver_precio']=$objeto->tip_ver_precio;
		$_POST['tip_ver_supcon']=$objeto->tip_ver_supcon;
		$_POST['tip_ver_suptot']=$objeto->tip_ver_suptot;
		
	}
	
	function orden_publicacion($proyecto,$accion,$ant_orden,$ida)
	{
		$conec= new ADO();
		
		if($accion=='s')
			$cad=" where pro_fam_id='".$ida."' and  pro_orden < $ant_orden order by pro_orden desc";
		else
			$cad=" where pro_fam_id='".$ida."' and  pro_orden > $ant_orden order by pro_orden asc";			

		$consulta = "
		select 
			pro_id,pro_orden 
		from 
			aplicaciones_productos
		$cad
		limit 0,1
		";	
		$conec->ejecutar($consulta);
		$num = $conec->get_num_registros();   

		if($num > 0)
		{
			$objeto=$conec->get_objeto();
			
			$nu_orden=$objeto->pro_orden;
			
			$id=$objeto->pro_id;
			
			$consulta = "update aplicaciones_productos set pro_orden='$nu_orden' where pro_id='$proyecto'";	

			$conec->ejecutar($consulta);
			
			$consulta = "update aplicaciones_productos set pro_orden='$ant_orden' where pro_id='$id'";	

			$conec->ejecutar($consulta);
		}	
	}	
	
	
	function datos_acabado()
	{

		if($_POST)
		{
			require_once('clases/validar.class.php');

			$num=0;
			
			$valores[$num]["etiqueta"]="Descripcion";
			$valores[$num]["valor"]=$_POST['aca_descripcion'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;

			$val=NEW VALIDADOR;

			$this->mensaje="";

			if($val->validar($valores))
			{
				return true;
			}
			else
			{
				$this->mensaje=$val->mensaje;

				return false;

			}

		}

			return false;

	}
	
	function datos_areasocial()
	{

		if($_POST)
		{
			require_once('clases/validar.class.php');

			$num=0;
			
			$valores[$num]["etiqueta"]="Titulo";
			$valores[$num]["valor"]=$_POST['are_soc_titulo'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			
			$valores[$num]["etiqueta"]="Descripcion";
			$valores[$num]["valor"]=$_POST['are_soc_descripcion'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;

			$val=NEW VALIDADOR;

			$this->mensaje="";

			if($val->validar($valores))
			{
				return true;
			}
			else
			{
				$this->mensaje=$val->mensaje;

				return false;

			}

		}

			return false;

	}
	
	function datos_foto()
	{
		if($_POST)
		{
			require_once('clases/validar.class.php');

			$num=0;
			
			/*$valores[$num]["etiqueta"]="Titulo";
			$valores[$num]["valor"]=$_POST['proy_fot_imagen'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;*/

			$val=NEW VALIDADOR;

			$this->mensaje="";

			if($val->validar($valores))
			{
				return true;
			}
			else
			{
				$this->mensaje=$val->mensaje;

				return false;

			}

		}
			return false;
	}
	
	
	function datos_disponibilidad()
	{

		if($_POST)
		{
			require_once('clases/validar.class.php');

			$num=0;
			
			/*$valores[$num]["etiqueta"]="Departamento";
			$valores[$num]["valor"]=$_POST['dis_departamento'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;*/

			$val=NEW VALIDADOR;

			$this->mensaje="";

			if($val->validar($valores))
			{
				return true;
			}
			else
			{
				$this->mensaje=$val->mensaje;

				return false;

			}

		}

			return false;

	}
	
	
	function modificar_areasocial()
	{		
		$conec= new ADO();
		
		if($_FILES['are_soc_imagen']['name']<>"")
		{	 	 	 	 	 	 	
			 	 	 	 	
			$result=$this->subir_imagen_proyecto($nombre_archivo,$_FILES['are_soc_imagen']['name'],$_FILES['are_soc_imagen']['tmp_name']);
			
			$sql="update area_social set 
								are_soc_titulo='".$_POST['are_soc_titulo']."',					
								are_soc_descripcion='".$_POST['are_soc_descripcion']."',					
								are_soc_imagen='".$nombre_archivo."'
								where are_soc_id = '".$_GET['are_soc_id']."'";
			
			if(trim($result)<>'')
			{
					
				$this->formulario->ventana_volver($result,$this->link.'?mod='.$this->modulo.'&tarea=AREASOCIAL&id='.$_GET['id']);
					
			}
			else
			{
				$llave=$_GET['id'];
				
				$mi=trim($_POST['fotooculta']);
				
				if($mi<>"")
				{
					$mifile="imagenes/proyecto/$mi";
				
					@unlink($mifile);
				
				}
				
				
				$conec->ejecutar($sql);
			
				$mensaje='Area Social Modificado Correctamente!!!';
					
				$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=AREASOCIAL&id='.$_GET['id']);
			}
		}
		else
		{
			
				$sql="update area_social set 
								are_soc_titulo='".$_POST['are_soc_titulo']."', 						
								are_soc_descripcion='".$_POST['are_soc_descripcion']."' 						
								where are_soc_id = '".$_GET['are_soc_id']."'";

			$conec->ejecutar($sql);

			$mensaje='Area Social Modificado Correctamente!!!';

			$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=AREASOCIAL&id='.$_GET['id']);
		}

	}
	
	function modificar_foto()
	{		
		$conec= new ADO();
		
		if($_FILES['proy_fot_imagen']['name']<>"")
		{	 	 	 	 	 	 	
			 	 	 	 	
			$result = $this->subir_imagen_foto($nombre_imagen,$_FILES['proy_fot_imagen']['name'],$_FILES['proy_fot_imagen']['tmp_name']);			
			
			if($_POST['proy_fot_portada']){
				$sw = 'true';				
				$sql="update proy_foto set 								
								proy_fot_portada='false' 
								where proy_id = '".$_GET['proy_id']."'";
				$conec->ejecutar($sql);
			}else{
				$sw = 'false';
			}
			
			if($_POST['proy_fot_anuncio']){
				$sw_anu = 'true';				
				$sql="update proy_foto set 								
								proy_fot_anuncio='false' 
								where proy_id = '".$_GET['proy_id']."'";
				$conec->ejecutar($sql);
			}else{
				$sw_anu = 'false';
			}
			
			/*$sql="update proy_foto set 
								proy_fot_imagen='".$nombre_imagen."',					
								proy_fot_portada='".$sw."',
								proy_fot_anuncio='".$sw_anu."'
								where proy_fot_id = '".$_GET['proy_fot_id']."'";*/
			
			$sql="update proy_foto set 
								proy_fot_imagen='".$nombre_imagen."',
								proy_fot_descripcion='".$this->cambiar_caracteres($_POST['proy_fot_descripcion'])."',
								proy_fot_portada='".$sw."',
								proy_fot_anuncio='".$sw_anu."'
								where proy_fot_id = '".$_GET['proy_fot_id']."'";
			
			if(trim($result)<>'')
			{
					
				$this->formulario->ventana_volver($result,$this->link.'?mod='.$this->modulo.'&tarea=FOTOS&id='.$_GET['id']);
					
			}
			else
			{
				$llave=$_GET['id'];
				
				$mi=trim($_POST['fotooculta']);
				
				if($mi<>"")
				{
					$mifile="imagenes/proyecto/$mi";
				
					@unlink($mifile);
				
				}
				
				
				$conec->ejecutar($sql);
			
				$mensaje='Foto Modificada Correctamente!!!';
					
				$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=FOTOS&id='.$_GET['id']);
			}
		}
		else
		{
			/*if($_POST['proy_fot_portada']){
				$sw = 'true';
				$sql="update proy_foto set 								
								proy_fot_portada='false' 
								where proy_id = '".$_GET['id']."'";
				$conec->ejecutar($sql);
			}else{
				$sw = 'false';
			}
			
			$sql="update proy_foto set 								
								proy_fot_portada='".$sw."'
								where proy_fot_id = '".$_GET['proy_fot_id']."'";*/
			
			$sql="update proy_foto set 
								proy_fot_descripcion='".$this->cambiar_caracteres($_POST['proy_fot_descripcion'])."'
								where proy_fot_id = '".$_GET['proy_fot_id']."'";					
			

			$conec->ejecutar($sql);

			$mensaje='Foto Modificada Correctamente!!!';

			$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=FOTOS&id='.$_GET['id']);
		}

	}


	
	function
	modificar_acabado()
	{		
		$conec= new ADO();
		
		if($_FILES['aca_imagen']['name']<>"")
		{	 	 	 	 	 	 	
			 	 	 	 	
			$result=$this->subir_imagen_proyecto($nombre_archivo,$_FILES['aca_imagen']['name'],$_FILES['aca_imagen']['tmp_name']);
			
			$sql="update acabado set 
								aca_descripcion='".$_POST['aca_descripcion']."',					
								aca_imagen='".$nombre_archivo."'
								where aca_id = '".$_GET['aca_id']."'";
			
			if(trim($result)<>'')
			{
					
				$this->formulario->ventana_volver($result,$this->link.'?mod='.$this->modulo.'&tarea=ACABADO&id='.$_GET['id']);
					
			}
			else
			{
				$llave=$_GET['id'];
				
				$mi=trim($_POST['fotooculta']);
				
				if($mi<>"")
				{
					$mifile="imagenes/proyecto/$mi";
				
					@unlink($mifile);
				
				}
				
				
				$conec->ejecutar($sql);
			
				$mensaje='Acabado Modificado Correctamente!!!';
					
				$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACABADO&id='.$_GET['id']);
			}
		}
		else
		{
			
				$sql="update acabado set 
								aca_descripcion='".$_POST['aca_descripcion']."' 						
								where aca_id = '".$_GET['aca_id']."'";

			$conec->ejecutar($sql);

			$mensaje='Acabado Modificado Correctamente!!!';

			$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACABADO&id='.$_GET['id']);
		}

	}


	function modificar_disponibilidad()
	{		
		$conec= new ADO();
		
		$sql="update disponibilidad set 
								dis_departamento='".$_POST['dis_departamento']."',
								dis_superficie='".$_POST['dis_superficie']."',
								dis_precio=".$_POST['dis_precio'].",
								dis_estado='".$_POST['dis_estado']."'
								where dis_id = '".$_GET['dis_id']."'";
								

			$conec->ejecutar($sql);

			$mensaje='Item Modificado Correctamente!!!';

			$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=DISPONIBILIDAD&id='.$_GET['id']);

	}




function cargar_datos_acabado()
	{
		$conec=new ADO();

		$sql="select * from acabado where aca_id='".$_GET['aca_id']."'";

		$conec->ejecutar($sql);

		$objeto=$conec->get_objeto();
		
		$_POST['aca_descripcion']=$objeto->aca_descripcion;
		$_POST['aca_imagen']=$objeto->aca_imagen;
		
		
	}
	
	function cargar_datos_areasocial()
	{
		$conec=new ADO();

		$sql="select * from area_social where are_soc_id='".$_GET['are_soc_id']."'";

		$conec->ejecutar($sql);

		$objeto=$conec->get_objeto();
		
		$_POST['are_soc_descripcion']=$objeto->are_soc_descripcion;
		$_POST['are_soc_imagen']=$objeto->are_soc_imagen;
		$_POST['are_soc_titulo']=$objeto->are_soc_titulo;
		
		
	}
	
	
	function cargar_datos_foto()
	{
		$conec=new ADO();

		$sql="select * from proy_foto where proy_fot_id='".$_GET['proy_fot_id']."'";

		$conec->ejecutar($sql);

		$objeto=$conec->get_objeto();
		
		$_POST['proy_fot_id']=$objeto->proy_fot_id;
		$_POST['proy_fot_imagen']=$objeto->proy_fot_imagen;
		
		$_POST['proy_fot_descripcion']=$objeto->proy_fot_descripcion;
		
		$_POST['proy_fot_portada']=$objeto->proy_fot_portada;
		$_POST['proy_fot_anuncio']=$objeto->proy_fot_anuncio;
		
		
	}
	
function cargar_datos_disponibilidad()
	{
		$conec=new ADO();

		$sql="select * from disponibilidad where dis_id='".$_GET['dis_id']."'";

		$conec->ejecutar($sql);

		$objeto=$conec->get_objeto();
		
		$_POST['dis_departamento']=$objeto->dis_departamento;
		$_POST['dis_superficie']=$objeto->dis_superficie;
		$_POST['dis_precio']=$objeto->dis_precio;
		$_POST['dis_estado']=$objeto->dis_estado;
		
		
	}
	
function formulario_tcp_acabado($tipo)

	{

		$url=$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER';

		$re=$url;

		$enlace="<a href='$url'>Proyectos</a>";

		echo "<tr>
					<td class='busqueda_enlace' colspan='3'>
						Volver a $enlace
					</td>
				</tr>"; 		

		switch ($tipo)
		{

			case 'ver':{
						$ver=true;
						
						break;
						}

			case 'cargar':{

						$cargar=true;

						break;
						}
		}

		if(!($ver))
		{
			$url.="&tarea=".$_GET['tarea'];
		}

		if($cargar)
		{
			$url.='&id='.$_GET['id'];
		}
	
		if($_GET['acc']=='MODIFICAR_ACABADO')
		{
			$url.='&acc=MODIFICAR_ACABADO';
		}
		
		$this->formulario->dibujar_tarea('ACABADO');
		
		if($this->mensaje<>"")
		{
			$this->formulario->mensaje('Error',$this->mensaje);
		}

		?>
		<?php include_once("js/fckeditor/fckeditor.php"); ?>	  

			<div id="Contenedor_NuevaSentencia">

               <form id="frm_sentencia" name="frm_sentencia" action="<?php echo $url.'&aca_id='.$_GET['aca_id'];?>" method="POST" enctype="multipart/form-data">  

				<div id="FormSent" style="width:900px;">

                    

					<div class="Subtitulo">Publicacion</div>

                    <div id="ContenedorSeleccion">
						
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Imagen </div>
							   <div id="CajaInput">
							   <?php
								if($_POST['aca_imagen']<>"")
								{	$foto=$_POST['aca_imagen'];
									$b=true;
								}	
								else
								{	$foto='sin_foto.gif';
									$b=false;
								}
								if(($ver)||($cargar))
								{	?>
									<img src="imagenes/proyecto/<?php echo $foto;?>" border="0" width="50"><br>
									<input   name="aca_imagen" type="file" id="aca_imagen" /><span class="flechas1"></span>
									<?php
								}
								else 
								{
									?>
									<input  name="aca_imagen" type="file" id="aca_imagen" /><span class="flechas1"></span>
									<?php
								}
								?>
								<input   name="fotooculta" type="hidden" id="fotooculta" value="<?php echo $_POST['aca_imagen'].$_POST['fotooculta'];?>"/>
							   </div>
							</div>
							<!--Fin-->							
						
							
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Descripcion</div>
							   <div id="CajaInput">							   
							    <?php
								$pagina=$_POST['aca_descripcion'];
								$oFCKeditor = new FCKeditor('aca_descripcion') ;
								$oFCKeditor->BasePath = 'js/fckeditor/';
								$oFCKeditor->Width  = '650' ;
								$oFCKeditor->Height = '200' ;
								//$oFCKeditor->ToolbarSet = 'Basic' ;
								$oFCKeditor->Value = $pagina;
								$oFCKeditor->Create() ;
								?>     
							   </div>
							</div>
							<!--Fin-->	
							
						
                    </div>

					<div id="ContenedorDiv">

                           <div id="CajaBotones">

								<center>

								<input type="submit" class="boton" name="" value="Enviar">

							    <input type="reset" class="boton" name="" value="Cancelar">

								<input type="button" class="boton" name="" value="Volver" onclick="location.href='<?php if($_GET['acc']=="MODIFICAR_ACABADO") echo $this->link.'?mod='.$this->modulo."&tarea=ACABADO&id=".$_GET['id']; else echo $this->link.'?mod='.$this->modulo.'&tarea=ACCEDER';?>';">

								</center>

						   </div>

                        </div>

                </div>

            </div>					

		<?php
	}
	
	function formulario_tcp_areasocial($tipo)

	{

		$url=$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER';

		$re=$url;

		$enlace="<a href='$url'>Proyectos</a>";

		echo "<tr>
					<td class='busqueda_enlace' colspan='3'>
						Volver a $enlace
					</td>
				</tr>"; 		

		switch ($tipo)
		{

			case 'ver':{
						$ver=true;
						
						break;
						}

			case 'cargar':{

						$cargar=true;

						break;
						}
		}

		if(!($ver))
		{
			$url.="&tarea=".$_GET['tarea'];
		}

		if($cargar)
		{
			$url.='&id='.$_GET['id'];
		}
	
		if($_GET['acc']=='MODIFICAR_AREASOCIAL')
		{
			$url.='&acc=MODIFICAR_AREASOCIAL';
		}
		
		$this->formulario->dibujar_tarea('AREASOCIAL');
		
		if($this->mensaje<>"")
		{
			$this->formulario->mensaje('Error',$this->mensaje);
		}

		?>
		<?php include_once("js/fckeditor/fckeditor.php"); ?>	  

			<div id="Contenedor_NuevaSentencia">

               <form id="frm_sentencia" name="frm_sentencia" action="<?php echo $url.'&are_soc_id='.$_GET['are_soc_id'];?>" method="POST" enctype="multipart/form-data">  

				<div id="FormSent" style="width:900px;">

                    

					<div class="Subtitulo">Publicacion</div>

                    <div id="ContenedorSeleccion">
						
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Imagen </div>
							   <div id="CajaInput">
							   <?php
								if($_POST['are_soc_imagen']<>"")
								{	$foto=$_POST['are_soc_imagen'];
									$b=true;
								}	
								else
								{	$foto='sin_foto.gif';
									$b=false;
								}
								if(($ver)||($cargar))
								{	?>
									<img src="imagenes/proyecto/<?php echo $foto;?>" border="0" width="50"><br>
									<input   name="are_soc_imagen" type="file" id="are_soc_imagen" /><span class="flechas1"></span>
									<?php
								}
								else 
								{
									?>
									<input  name="are_soc_imagen" type="file" id="are_soc_imagen" /><span class="flechas1"></span>
									<?php
								}
								?>
								<input   name="fotooculta" type="hidden" id="fotooculta" value="<?php echo $_POST['are_soc_imagen'].$_POST['fotooculta'];?>"/>
							   </div>
							</div>
							<!--Fin-->							
						
							
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Titulo</div>
							   <div id="CajaInput">
							   <textarea  class="caja_texto" name="are_soc_titulo" id="are_soc_titulo"><?php echo $_POST['are_soc_titulo'];?></textarea>
							   </div>
							</div>
							<!--Fin-->		
							
							
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Descripcion</div>
							   <div id="CajaInput">							   
							    <?php
								$pagina=$_POST['are_soc_descripcion'];
								$oFCKeditor = new FCKeditor('are_soc_descripcion') ;
								$oFCKeditor->BasePath = 'js/fckeditor/';
								$oFCKeditor->Width  = '650' ;
								$oFCKeditor->Height = '200' ;
								//$oFCKeditor->ToolbarSet = 'Basic' ;
								$oFCKeditor->Value = $pagina;
								$oFCKeditor->Create() ;
								?>     
							   </div>
							</div>
							<!--Fin-->	
							
						
                    </div>

					<div id="ContenedorDiv">

                           <div id="CajaBotones">

								<center>

								<input type="submit" class="boton" name="" value="Enviar">

							    <input type="reset" class="boton" name="" value="Cancelar">

								<input type="button" class="boton" name="" value="Volver" onclick="location.href='<?php if($_GET['acc']=="MODIFICAR_AREASOCIAL") echo $this->link.'?mod='.$this->modulo."&tarea=AREASOCIAL&id=".$_GET['id']; else echo $this->link.'?mod='.$this->modulo.'&tarea=ACCEDER';?>';">

								</center>

						   </div>

                        </div>

                </div>

            </div>					

		<?php
	}


	function formulario_tcp_disponibilidad($tipo){

		$url=$this->link.'?mod='.$this->modulo;

		$re=$url;

		$enlace="<a href='$url'>Proyectos</a>";

		echo "<tr>
					<td class='busqueda_enlace' colspan='3'>
						Volver a $enlace
					</td>
				</tr>"; 		

		switch ($tipo)
		{

			case 'ver':{
						$ver=true;
						
						break;
						}

			case 'cargar':{

						$cargar=true;

						break;
						}
		}

		if(!($ver))
		{
			$url.="&tarea=".$_GET['tarea'];
		}

		if($cargar)
		{
			$url.='&id='.$_GET['id'];
		}
	
		if($_GET['acc']=='MODIFICAR_DISPONIBILIDAD')
		{
			$url.='&acc=MODIFICAR_DISPONIBILIDAD';
		}
		
		$this->formulario->dibujar_tarea('DISPONIBILIDAD');
		
		if($this->mensaje<>"")
		{
			$this->formulario->mensaje('Error',$this->mensaje);
		}

		?>


			<div id="Contenedor_NuevaSentencia">

               <form id="frm_sentencia" name="frm_sentencia" action="<?php echo $url.'&dis_id='.$_GET['dis_id'];?>" method="POST" enctype="multipart/form-data">  

				<div id="FormSent" style="width:900px;">

                    

					<div class="Subtitulo">Publicacion</div>

                    <div id="ContenedorSeleccion">
						
							
                            <!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Departamento</div>
							   <div id="CajaInput">
							   <textarea  class="caja_texto" name="dis_departamento" id="dis_departamento"><?php echo $_POST['dis_departamento'];?></textarea>
							   </div>
							</div>
							<!--Fin-->						
						
							
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Superficie</div>
							   <div id="CajaInput">
							   <input class="caja_texto" name="dis_superficie" id="dis_superficie" value="<?php echo $_POST['dis_superficie'];?>" />
							   </div>
							</div>
							<!--Fin-->
                            
                            <!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Precio</div>
							   <div id="CajaInput">
							   <input class="caja_texto" name="dis_precio" id="dis_precio" value="<?php echo $_POST['dis_precio'];?>" />
							   </div>
							</div>
							<!--Fin-->
                            
                             <!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Estado</div>
							   <div id="CajaInput">
							   	<select name="dis_estado" id="dis_estado">
                                	<option>Disponible</option>
                                    <option>Vendido</option>
                                </select>
                               	
							   </div>
							</div>
							<!--Fin-->	
							
						
                    </div>

					<div id="ContenedorDiv">

                           <div id="CajaBotones">

								<center>
								
                                <input type="hidden" name="dis_proy_id" value="<?php echo $_GET['id']; ?>"/>
                                
								<input type="submit" class="boton" name="" value="Enviar">

							    <input type="reset" class="boton" name="" value="Cancelar">

								<input type="button" class="boton" name="" value="Volver" onclick="location.href='<?php if($_GET['acc']=="MODIFICAR_DISPONIBILIDAD") echo $this->link.'?mod='.$this->modulo."&tarea=DISPONIBILIDAD&id=".$_GET['id']; else echo $this->link.'?mod='.$this->modulo.'&tarea=ACCEDER';?>';">

								</center>

						   </div>

                        </div>

                </div>

            </div>					

		<?php
	}
	
	
	function eliminar_acabado($id_producto)
	{
		$conec= new ADO();

		$llave=$id_producto;
				
		/*$mi=$this->nombre_imagen($llave);*/
		
		$sql="delete from acabado where aca_id='".$id_producto."'";

		$conec->ejecutar($sql);
		
		$this->mensaje='Acabado eliminado correctamente';
	}
	
	function eliminar_areasocial($id_producto)
	{
		$conec= new ADO();

		$llave=$id_producto;
				
		/*$mi=$this->nombre_imagen($llave);*/
		
		$sql="delete from area_social where are_soc_id='".$id_producto."'";

		$conec->ejecutar($sql);
		
		$this->mensaje='Area social eliminada correctamente';
	}
	
	function eliminar_disponibilidad($id_producto)
	{
		$conec= new ADO();

		$llave=$id_producto;
				
		/*$mi=$this->nombre_imagen($llave);*/
		
		$sql="delete from disponibilidad where dis_id='".$id_producto."'";

		$conec->ejecutar($sql);
		
		$this->mensaje='Item eliminado correctamente';
	}
	
	
	function guardar_disponibilidad()
	{		
		$conec= new ADO();		
		
		$sql="insert into disponibilidad(dis_departamento,dis_superficie,dis_precio,dis_estado,dis_proy_id) values 
									('".$_POST['dis_departamento']."','". $_POST['dis_superficie'] ."',".$_POST['dis_precio'].",'".$_POST['dis_estado']."','".$_GET['id']."')";
								
		
		$conec->ejecutar($sql);	
		
		$this->limpiar_disponibilidad();		
	}
	
	function guardar_acabado()
	{		
		$conec= new ADO();		
		
		$this->subir_imagen_foto($nombre_imagen,$_FILES['aca_imagen']['name'],$_FILES['aca_imagen']['tmp_name']);
		
		$sql="insert into acabado(aca_imagen,aca_descripcion,aca_proy_id) values 
									('".$nombre_imagen."','". $_POST['aca_descripcion'] ."','".$_GET['id']."')";
		
		$conec->ejecutar($sql);	
		
		$this->limpiar_acabado();		
	}
	
	function guardar_areasocial()
	{		
		$conec= new ADO();		
		
		$this->subir_imagen_foto($nombre_imagen,$_FILES['are_soc_imagen']['name'],$_FILES['are_soc_imagen']['tmp_name']);
		
		$sql="insert into area_social(are_soc_imagen,are_soc_descripcion,are_soc_titulo,are_soc_proy_id) values 
									('".$nombre_imagen."','". $_POST['are_soc_descripcion'] ."','". $_POST['are_soc_titulo']."','".$_GET['id']."')";
		
		$conec->ejecutar($sql);	
		
		$this->limpiar_acabado();		
	}
	
	function limpiar_areasocial()
	{
		$_POST['are_soc_descripcion']="";		
		$_POST['are_soc_imagen']="";
		$_POST['are_soc_titulo']="";
		
	}
	
	function limpiar_acabado()
	{
		$_POST['aca_descripcion']="";		
		$_POST['aca_imagen']="";		
	}
	
	function limpiar_disponibilidad()
	{
		$_POST['dis_departamento']="";		
		$_POST['dis_superficie']="";
		$_POST['dis_precio']=0;
		$_POST['dis_estado']="";
		
	}
	
	function dibujar_encabezado_acabado()

	{

		?><div style="clear:both;"></div><center>

		<table class="tablaLista" cellpadding="0" cellspacing="0" width="60%">
			<thead>
				<tr>
					
					<th >

						Descripcion

					</th>					
					
					<th >

						Imagen

					</th>
								
					

					<th class="tOpciones" width="100px">

						Opciones

					</th>

				</tr>
			</thead>
			<tbody>
		<?PHP

	}
	
	function dibujar_encabezado_areasocial()
	{

		?><div style="clear:both;"></div><center>

		<table class="tablaLista" cellpadding="0" cellspacing="0" width="60%">
			<thead>
				<tr>
					
					<th >

						Titulo

					</th>					
					
					<th >

						Imagen

					</th>
								
					

					<th class="tOpciones" width="100px">

						Opciones

					</th>

				</tr>
			</thead>
			<tbody>
		<?PHP

	}
	

	function mostrar_busqueda_acabado()
	{

		$conec=new ADO();

		
		
		$sql="select * 
		from 
		acabado 
		where 
		aca_proy_id='".$_GET['id']."'  order by aca_id desc";
		
		$conec->ejecutar($sql);

		$num=$conec->get_num_registros();

		for($i=0;$i<$num;$i++)
		{
			$objeto=$conec->get_objeto();

			echo '<tr class="busqueda_campos">';

			?>
				
				<td align="left">

					<?php echo $objeto->aca_descripcion; ?>

				</td>
				
				<td align="left">

						<img width="100" height="100" src="imagenes/proyecto/<?php echo $objeto->aca_imagen; ?>" />

				</td>
						

				<td>

					<center>

					<a href="gestor.php?mod=proyecto&tarea=ACABADO&acc=ELIMINAR&aca_id=<?php echo $objeto->aca_id;?>&id=<?php echo $_GET['id'];?>"><img src="images/b_drop.png" alt="ELIMINAR" title="ELIMINAR" border="0"></a>

					<a href="gestor.php?mod=proyecto&tarea=ACABADO&acc=MODIFICAR_ACABADO&aca_id=<?php echo $objeto->aca_id;?>&id=<?php echo $_GET['id'];?>"><img src="images/b_edit.png" alt="MODIFICAR" title="MODIFICAR" border="0"></a>

					</center>

				</td>

			<?php

			echo "</tr>";

			

			

			$conec->siguiente();

		}

		echo "</tbody></table></center><br>";
	}
	
	
	function mostrar_busqueda_areasocial()
	{

		$conec=new ADO();

		
		
		$sql="select * 
		from 
		area_social 
		where 
		are_soc_proy_id='".$_GET['id']."'  order by are_soc_id desc";
		
		$conec->ejecutar($sql);

		$num=$conec->get_num_registros();

		for($i=0;$i<$num;$i++)
		{
			$objeto=$conec->get_objeto();

			echo '<tr class="busqueda_campos">';

			?>
				
				<td align="left">

					<?php echo $objeto->are_soc_titulo; ?>

				</td>
				
				<td align="left">

						<img width="100" height="100" src="imagenes/proyecto/<?php echo $objeto->are_soc_imagen; ?>" />

				</td>
						

				<td>

					<center>

					<a href="gestor.php?mod=proyecto&tarea=AREASOCIAL&acc=ELIMINAR&are_soc_id=<?php echo $objeto->are_soc_id;?>&id=<?php echo $_GET['id'];?>"><img src="images/b_drop.png" alt="ELIMINAR" title="ELIMINAR" border="0"></a>

					<a href="gestor.php?mod=proyecto&tarea=AREASOCIAL&acc=MODIFICAR_AREASOCIAL&are_soc_id=<?php echo $objeto->are_soc_id;?>&id=<?php echo $_GET['id'];?>"><img src="images/b_edit.png" alt="MODIFICAR" title="MODIFICAR" border="0"></a>

					</center>

				</td>

			<?php

			echo "</tr>";

			

			

			$conec->siguiente();

		}

		echo "</tbody></table></center><br>";
	}
	
	
	
	
	
	
	function dibujar_encabezado_disponibilidad()

	{

		?><div style="clear:both;"></div><center>

		<table class="tablaLista" cellpadding="0" cellspacing="0" width="60%">
			<thead>
				<tr>
					
					<th >

						Departamento

					</th>					
					
					<th >

						Superficie

					</th>
                    <th >

						Precio

					</th>
                    
                    <th >

						Estado

					</th>
								
					

					<th class="tOpciones" width="100px">

						Opciones

					</th>

				</tr>
			</thead>
			<tbody>
		<?PHP

	}
	
	function mostrar_busqueda_disponibilidad()
	{

		$conec=new ADO();

		
		
		$sql="select * 
		from 
		disponibilidad 
		where 
		dis_proy_id='".$_GET['id']."'  order by dis_id desc";
		
		$conec->ejecutar($sql);

		$num=$conec->get_num_registros();

		for($i=0;$i<$num;$i++)
		{
			$objeto=$conec->get_objeto();

			echo '<tr class="busqueda_campos">';

			?>
				
				<td align="left">

					<?php echo $objeto->dis_departamento; ?>

				</td>
                
                <td align="left">

					<?php echo $objeto->dis_superficie; ?>

				</td>
                
                <td align="left">

					<?php echo $objeto->dis_precio; ?>

				</td>
                
                <td align="left">

					<?php echo $objeto->dis_estado; ?>

				</td>
				
				
				<td>

					<center>

					<a href="gestor.php?mod=proyecto&tarea=DISPONIBILIDAD&acc=ELIMINAR&dis_id=<?php echo $objeto->dis_id;?>&id=<?php echo $_GET['id'];?>"><img src="images/b_drop.png" alt="ELIMINAR" title="ELIMINAR" border="0"></a>

					<a href="gestor.php?mod=proyecto&tarea=DISPONIBILIDAD&acc=MODIFICAR_DISPONIBILIDAD&dis_id=<?php echo $objeto->dis_id;?>&id=<?php echo $_GET['id'];?>"><img src="images/b_edit.png" alt="MODIFICAR" title="MODIFICAR" border="0"></a>

					</center>

				</td>

			<?php

			echo "</tr>";

			

			

			$conec->siguiente();

		}

		echo "</tbody></table></center><br>";
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	function cargar_disponibilidad($id)
	{
		$url=$this->link.'?mod='.$this->modulo;
		
		$conec=new ADO();
		
		$sql="select * from disponibilidad
				where dis_proy_id = '".$id."'";
		
		$conec->ejecutar($sql);
				
			
	?>
    
	<link href="media/css/demo_page.css" rel="stylesheet" type="text/css" />
    <link href="media/css/demo_table.css" rel="stylesheet" type="text/css" />
    
	<script type="text/javascript" src="js/datatable/jquery.js"></script>
	<script type="text/javascript" src="js/datatable/jquery.dataTables.js"></script>
    <script type="text/javascript" src="js/datatable/jquery.jeditable.js"></script>
    
	<script type="text/javascript">
       $(document).ready(function() {
			/* Init DataTables */
			var oTable = $('#example').dataTable({
			    "bStateSave": true,
			    "bFilter": true,
			    "bLengthChange" : true,
			    "bAutoWidth": false
			   
			    
			});
			
			/* Apply the jEditable handlers to the table */
			oTable.$('td').editable( 'editar_participante.php', {
				"callback": function( sValue, y ) {
					var aPos = oTable.fnGetPosition( this );
					oTable.fnUpdate( sValue, aPos[0], aPos[1] );
				},
				"submitdata": function ( value, settings ) {
					return {
						"row_id": this.parentNode.getAttribute('id'),
						"camp_id": $("#idCamp").val(),
						"column": oTable.fnGetPosition( this )[2]
					};
				},
				"height": "14px",
				"width": "100%"
			} );
		} );
        
		
		var giCount = 1;
		 
		$(document).ready(function() {
			$('#example').dataTable();
		} );
		 
		function fnClickAddRow() {
			$('#example').dataTable().fnAddData( [
				giCount+".1",
				giCount+".2",
				giCount+".3",
				giCount+".4" ] );
			 
			giCount++;
		}
		
    </script>				
			
			
		
		<div id="Contenedor_NuevaSentencia">
			<form id="frm_sentencia" name="frm_sentencia" action="<?php echo $url;?>" method="POST" enctype="multipart/form-data">  
				<div id="FormSent">
				  
					<div class="Subtitulo">Datos</div>
						<div id="ContenedorSeleccion">
							
							
                            
                            <p><a href="javascript:void(0);" onclick="fnClickAddRow();">Click to add a new row</a></p>
                            
                            <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Nombre</th>
                                <th>Club</th>
                                <th>Punto</th>
                            </tr>
                            </thead>
                            <tbody>
                                            <tr id="355">
                                <td>1</td>
                                <td>Albania Bruckner</td>
                                <td>M.G.C.</td>
                                <td><a href="delete_participante_campeonato.php?idCamp=19&idPart=355&idCat=2"><img src="img/cancel.jpg" alt="cancel"/></a></td>
                            </tr>
                            </tbody>
                            </table>
                            


							
						</div>
						
						<div id="ContenedorDiv">
						   <div id="CajaBotones">
								<center>
									
									<input type="button" class="boton" name="" value="Volver" onclick="javascript:location.href='<?php echo $red;?>';">
								
								</center>
						   </div>
						</div>
				</div>
			</form>
		</div>
		<?php
	}
	
}
?>