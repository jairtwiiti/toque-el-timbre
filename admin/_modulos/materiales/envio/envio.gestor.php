<?php
	require_once('envio.class.php');
	
	$envio = new ENVIO();
	
	if(!($envio->verificar_permisos($_GET['tarea'])))
	{
		?>
		<script>
			location.href="log_out.php";
		</script>
		<?php
	}
	
	switch ($_GET['tarea'])
	{
		case 'AGREGAR':{
						
							if($envio->datos())
							{
								$envio->insertar_tcp();
							}
							else 
							{
								$envio->formulario_tcp('blanco');
							}
							
							break;
		}
		case 'VER':{
							$envio->cargar_datos();
													
							$envio->formulario_tcp('ver');
							
							break;
		}
		case 'MODIFICAR':{
							if($envio->datos())
							{
								$envio->modificar_tcp();
							}
							else 
							{
								if(!($_POST))
								{
									$envio->cargar_datos();
								}
								$envio->formulario_tcp('cargar');
							}
							break;
		}					
		case 'ELIMINAR':{
							
							$envio->eliminar_tcp();
								
							break;
		}
		case 'ACCEDER':{
							$envio->dibujar_busqueda();
							break;
		}
        case 'ENVIAR_MAIL':{
								
								$envio->cargar_datos();								
								$envio->enviar_mail();
								
								break;
					}		
	}
?>