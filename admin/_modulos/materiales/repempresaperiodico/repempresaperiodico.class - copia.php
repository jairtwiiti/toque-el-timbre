<?php

class REPEMPRESAPERIODICO extends BUSQUEDA 
{
	var $formulario;
	var $mensaje;
	
	function REPEMPRESAPERIODICO()
	{
		$this->coneccion= new ADO();
		
		$this->link='gestor.php';
		
		$this->modulo='repempresaperiodico';
		
		$this->formulario = new FORMULARIO();
		
		$this->formulario->set_titulo('REPORTE DE NOTAS POR PERIODICO');
	}
	
	
	function dibujar_busqueda()
	{
			$this->formulario();
	}
	
	function formulario()
	{
		$this->formulario->dibujar_cabecera();
		
		if(!($_POST['formu']=='ok'))
		{
		?>
		
						<?php
						if($this->mensaje<>"")
						{
						?>
							<table width="100%" cellpadding="0" cellspacing="1" style="border:1px solid #DD3C10; color:#DD3C10;">
							<tr bgcolor="#FFEBE8">
								<td align="center">
									<?php
										echo $this->mensaje;
									?>
								</td>
							</tr>
							</table>
						<?php
						}
						?>						
						<div id="Contenedor_NuevaSentencia">
						<form id="frm_sentencia" name="frm_sentencia" action="gestor.php?mod=repempresaperiodico" method="POST" enctype="multipart/form-data">  
									<div id="FormSent">
										<div class="Subtitulo">Filtro del Reporte</div>
					                    <div id="ContenedorSeleccion">
										
										<!--Inicio-->
										<div id="ContenedorDiv">
										   <div class="Etiqueta" ><span class="flechas1">*</span>Empresa</div>
										   <div id="CajaInput">
										   <select name="emp_id" class="caja_texto">
										   <option value="">Seleccione</option>
										   <?php 		
											$fun=NEW FUNCIONES;		
											$fun->combo("select emp_id as id,emp_nombre as nombre from empresa order by emp_nombre asc",$_POST['emp_id']);				
											?>
										   </select>
										   </div>										   
										</div>
										<!--Fin-->
												
												
												
										</div>
										
										<div id="ContenedorDiv">
					                           <div id="CajaBotones">
													<center>
													<input type="hidden" class="boton" name="formu" value="ok">
													<input type="submit" class="boton" name="" value="Generar Reporte">
													</center>
											   </div>
					                    </div>
									</div>
						</form>	
						<div>
		<?php
		}
		
		if($_POST['formu']=='ok')
			$this->mostrar_reporte();
	}
	
	function obtener_categorias($conec)
	{	    	
		//Aqui obtengo todas las palabras claves
		$sql = "SELECT cat_nombre,eca_tags 
		        FROM empresa_categoria INNER JOIN categoria ON (eca_cat_id = cat_id)
				WHERE eca_emp_id =".$_POST["emp_id"]; 
		
		$conec->ejecutar($sql);
		$num=$conec->get_num_registros();
		$categorias = array();
		
		for($i=0;$i<$num;$i++)
		{		 
		    $aux = array();
		    $objeto  = $conec->get_objeto();
            $aux['nombre'] = $objeto->cat_nombre;
            $aux['tags']   = explode(",",$objeto->eca_tags);
            $aux['notas']  = array();  			
     		$categorias[]  = $aux;	
			$conec->siguiente();
		}	
  	   
		return	$categorias;   	
	}
	
	function verificar_nota(&$cat,$nota)
	{
	  	foreach($cat as $key=>$categoria)
		{
		    if (is_array($categoria['tags']) && (count($categoria['tags']) > 0))
			{
			    /*verifica si alguna palabra clave se encuentra en la nota*/
				
			    foreach($categoria['tags'] as $palabra)
				{
				    //echo $palabra."<br>";
					if (substr_count($nota['notacompleta'],$palabra) > 0)
				    {                     
					   $cat[$key]['notas'][] = $nota;					   
					   break;
					}	   
				}	
                /*-----------------------------------------------------------------*/				
			}		
		}	
	}
	
	function obtener_pagina($tmparchivo,$url)
	{
	    $tmpFile= $tmparchivo;			
		$content = file($url);
		file_put_contents($tmpFile.".htm",$content);
		 $tidy_config = array('output-encoding' => 'utf8',								
							 'output-xml'   => true, 															
							 'char-encoding' => 'utf8',
							 'drop-font-tags' => true,
							 'drop-proprietary-attributes' => true, 
							 'logical-emphasis' => true,
							 'doctype'      => 'omit',								
							 'indent'       => false,
							 'clean'        => true,
							 'show-body-only' => true,                    
							 'wrap'         => 200);								
		
		$beginTag="<?xml version=\"1.0\" encoding=\"UTF-8\"?>".
				  "<depure>";
		$endTag="</depure>";
		
		$xmltidy =  $beginTag.  tidy_repair_file($tmpFile.'.htm' ,$tidy_config ,'utf8') .$endTag ; 
		$xmltidy=str_replace("&nbsp;", " ", $xmltidy);
			
		file_put_contents($tmpFile.'.xml',$xmltidy); 	
	}
	
	function eliminar_pagina($tmparchivo)
	{
	    unlink($tmparchivo.'.htm');
		unlink($tmparchivo.'.xml');	
	}
	
	function obtener_nota_completa($url,$expreg_nota)
	{		
		$tmparchivo = "tmpfilenota"; 
        $this->obtener_pagina($tmparchivo,$url);
		//cargar xml
		$doc = new DOMDocument();
		$doc->preserveWhiteSpace = false;
		$doc->Load($tmparchivo.'.xml');	
		$nodes= $doc->getElementsByTagName("depure");

		$xpath = new DOMXPath($doc);	
		$query = $expreg_nota;
		$result = $xpath->query($query);	
		$cad = "";
		foreach ($result as $entry)
		{          
		  $cad.=utf8_decode($entry->textContent)."<br/>";	  		  
		}
		$this->eliminar_pagina($tmparchivo);  	
		return $cad;
	}
	
	
	function mostrar_reporte()
	{		
		//echo $_POST['emp_id'];
		require_once('clases/magpierss/rss_fetch.inc');
		
		$conec= new ADO();	
		$conec1= new ADO();	
		$conversor = new convertir();			
		
		//Aqui obtengo todas las palabras claves
	    $categorias = $this->obtener_categorias($conec);	 		
		$sql = "SELECT prd_id,prd_nombre,prd_formato_fecha,sec_id,sec_nombre,pes_tipo,pes_url_sec,pes_expreg_sec,pes_expreg_nota,pes_expreg_titulo,pes_expreg_resumen,pes_expreg_link 
		        FROM empresa_periodico INNER JOIN periodico ON (epe_prd_id = prd_id)  
									   INNER JOIN periodico_seccion ON (pes_prd_id = prd_id)
                                       INNER JOIN seccion ON (pes_sec_id = sec_id) 									   
				WHERE epe_emp_id =".$_POST["emp_id"]; 		
		$conec->ejecutar($sql);
		$num=$conec->get_num_registros();		
		
		for($i=0;$i<$num;$i++)
		{		    
			$objeto=$conec->get_objeto();
			
			if ($objeto->pes_tipo=="rss")
			{
				$rss = fetch_rss($objeto->pes_url_sec);
				
				foreach ($rss->items as $item) {
				    $item['seccion_id']   = $objeto->sec_id;
					$item['periodico_id'] = $objeto->prd_id;	
					$item['periodico']    = $objeto->prd_nombre;					
					$item['notacompleta'] = $this->obtener_nota_completa($item['link'],$objeto->pes_expreg_nota);				
					$this->verificar_nota($categorias,$item);										
				}
			}
            else
            {
				$tmparchivo = "tmpfileseccion"; 
				$this->obtener_pagina($tmparchivo,$objeto->pes_url_sec);
				//cargar xml
				$doc = new DOMDocument();
				$doc->preserveWhiteSpace = false;
				$doc->Load($tmparchivo.'.xml');	
				$nodes= $doc->getElementsByTagName("depure");
				$xpath = new DOMXPath($doc);	
				$query = $objeto->pes_expreg_sec;
				$result = $xpath->query($query);
				foreach ($result as $entry)
				{   
				    $item = array();
					$item['seccion_id']   = $objeto->sec_id;
					$item['periodico_id'] = $objeto->prd_id;					 
					$item['title'] = $xpath->query($objeto->pes_expreg_titulo,$entry);
					$item['description'] = $xpath->query($objeto->pes_expreg_resumen,$entry);
					$item['link'] = $xpath->query($objeto->pes_expreg_link,$entry);				
					$item['periodico']    = $objeto->prd_nombre;					
					$item['notacompleta'] = $this->obtener_nota_completa($item['link'],$objeto->pes_expreg_nota);				
					$this->verificar_nota($categorias,$item);								  
				}
			    $this->eliminar_pagina($tmparchivo);  	 
			} 			
			$conec->siguiente();
		}

        foreach ($categorias as $cate) {
		
		    echo $cate['nombre']."<br><br>";
			foreach ($cate['notas'] as $nota) {
			    echo "<a href='".$nota['link']."'>".$nota['title']."</a><br>";
				echo $nota['notacompleta'];
			}
        }		
		//print_r($categorias);
		
		
		////
		$pagina="'contenido_reporte'";
		
		$page="'about:blank'";
		
		$extpage="'reportes'";
		
		$features="'left=100,width=800,height=500,top=0,scrollbars=yes'";
		
		$extra1="'<html><head><title>Vista Previa</title><head>
				<link href=css/estilos.css rel=stylesheet type=text/css />
			  </head>
			  <body>
			  <div id=imprimir>
			  <div id=status>
			  <p>";
		$extra1.=" <a href=javascript:window.print();>Imprimir</a> 
				  <a href=javascript:self.close();>Cerrar</a></td>
				  </p>
				  </div>
				  </div>
				  <center>'";
		$extra2="'</center></body></html>'"; 
		
		$myday = setear_fecha(strtotime(date('Y-m-d')));
		////
				
		?>		
				<?php echo '	<table align=right border=0><tr><td><a href="javascript:var c = window.open('.$page.','.$extpage.','.$features.');
				  c.document.write('.$extra1.');
				  var dato = document.getElementById('.$pagina.').innerHTML;
				  c.document.write(dato);
				  c.document.write('.$extra2.'); c.document.close();
				  ">
				<img src="images/printer.png" align="right" width="20" border="0" title="IMPRIMIR">
				</a></td><td><img src="images/back.png" align="right" width="20" border="0"  title="VOLVER" onclick="javascript:location.href=\'gestor.php?mod=repptranse\';"></td></tr></table><br><br>
				';?>
						
				 

			<div id="contenido_reporte" style="clear:both;";>
			<center>
			
			<table style="font-size:12px;" width="100%" cellpadding="5" cellspacing="0" >
				<tr>
				    <!--<td><img src="imagenes/logoreporte.jpg"></td>
				    <td align="center"><strong><h3>INFORME DIARIO DE MONITOREO RELACIONAMIENTO COMUNITARIO.</h3></strong></td> -->
				
				</tr>
				<tr>
					<td align="center" colspan="2"><strong><h3>PANORAMA <? echo $conversor->get_fecha_latina(date("Y-m-d"));?></h3></strong></td>
				    
				    
				</tr>				 
			</table>			
			<br>
			<br>
			
			<table style="font-size:12px;" width="81%" cellpadding="5" cellspacing="3" >			
				<tr>
					<td colspan="2" align="right">
						<strong>Impreso el: </strong> <?php echo $myday;?> <br><br>
					</td>
					
				</tr>				 
			</table>
			
			
			
			<table   width="81%"  class="tablaReporte" cellpadding="0" cellspacing="0">
				<thead>
				<tr>
					<th valign="MIDDLE" style="text-align:left; padding-left:10px">
						<b>TITULARES</b>
					</th>					
				</tr>
				</thead>
				<tbody>
		<?php
         // $sql = "SELECT 
				  // sed_descripcion,sed_comentarios,sed_acciones,sed_responsables,sed_fecha
			    // FROM 
				  // seguimiento_detalle 
				// WHERE
                  // sed_seg_id =".$_GET["id"]; 
		//echo $sql;
		// $conec->ejecutar($sql);
		// $objeto=$conec->get_objeto();
        // $num=$conec->get_num_registros();
		
		 // for($i=0;$i<$num;$i++)
		// {
			// $objeto=$conec->get_objeto();

			// echo '<tr>';					
					// echo "<td>";
						// echo ($i+1);
					// echo "</td>";
					
					// echo "<td valign='top'>";
						// echo $objeto->sed_descripcion."&nbsp;";
					// echo "</td>";
					
					// echo "<td valign='top'>";
						// echo $objeto->sed_comentarios."&nbsp;";
					// echo "</td>";
					
					// echo "<td valign='top'>";
						// echo $objeto->sed_acciones."&nbsp;";
					// echo "</td>";
					// echo "<td valign='top'>";
						// echo $conversor->get_fecha_latina($objeto->sed_fecha);
					// echo "</td>";
					// echo "<td valign='top'>";
						// echo $objeto->sed_responsables."&nbsp;";
					// echo "</td>";
					
				// echo "</tr>";
				
			
			// $conec->siguiente();
		// }
		?>
		
		
		</tbody>
		<thead>
				<tr>
					<th valign="MIDDLE" style="text-align:left; padding-left:10px">
						<b>NOTAS COMPLETAS</b>
					</th>					
				</tr>
		</thead>
		<tbody>
		</tbody>
			
		
		</table>
		</center>
		</div><br>
		
		<?php
		
		
	}
	
	
}
?>