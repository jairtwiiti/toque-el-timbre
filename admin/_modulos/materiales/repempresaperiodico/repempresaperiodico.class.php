<?php

class REPEMPRESAPERIODICO extends BUSQUEDA 
{
	var $formulario;
	var $mensaje;
	
	function REPEMPRESAPERIODICO()
	{
		$this->coneccion= new ADO();
		
		$this->link='gestor.php';
		
		$this->modulo='repempresaperiodico';
		
		$this->formulario = new FORMULARIO();
		
		$this->formulario->set_titulo('REPORTE DE NOTAS POR PERIODICO');
	}
	
	
	function dibujar_busqueda()
	{
			$this->formulario();
	}
	
	function formulario()
	{
		$this->formulario->dibujar_cabecera();
		
		if(!($_POST['formu']=='ok'))
		{
		?>
		
						<?php
						if($this->mensaje<>"")
						{
						?>
							<table width="100%" cellpadding="0" cellspacing="1" style="border:1px solid #DD3C10; color:#DD3C10;">
							<tr bgcolor="#FFEBE8">
								<td align="center">
									<?php
										echo $this->mensaje;
									?>
								</td>
							</tr>
							</table>
						<?php
						}
						?>						
						<div id="Contenedor_NuevaSentencia">
						<form id="frm_sentencia" name="frm_sentencia" action="gestor.php?mod=repempresaperiodico" method="POST" enctype="multipart/form-data">  
									<div id="FormSent">
										<div class="Subtitulo">Filtro del Reporte</div>
					                    <div id="ContenedorSeleccion">
										
										<!--Inicio-->
										<div id="ContenedorDiv">
										   <div class="Etiqueta" ><span class="flechas1">*</span>Empresa</div>
										   <div id="CajaInput">
										   <select name="emp_id" class="caja_texto">
										   <option value="">Seleccione</option>
										   <?php 		
											$fun=NEW FUNCIONES;		
											$fun->combo("select emp_id as id,emp_nombre as nombre from empresa order by emp_nombre asc",$_POST['emp_id']);				
											?>
										   </select>
										   </div>										   
										</div>
										<!--Fin-->
												
												
												
										</div>
										
										<div id="ContenedorDiv">
					                           <div id="CajaBotones">
													<center>
													<input type="hidden" class="boton" name="formu" value="ok">
													<input type="submit" class="boton" name="" value="Generar Reporte">
													</center>
											   </div>
					                    </div>
									</div>
						</form>	
						<div>
		<?php
		}
		
		if($_POST['formu']=='ok')
			$this->mostrar_reporte();
	}
	
	function obtener_categorias($conec)
	{	    	
		//Aqui obtengo todas las palabras claves
		$sql = "SELECT cat_nombre,eca_tags 
		        FROM empresa_categoria INNER JOIN categoria ON (eca_cat_id = cat_id)
				WHERE eca_emp_id =".$_POST["emp_id"]; 
		
		$conec->ejecutar($sql);
		$num=$conec->get_num_registros();
		$categorias = array();
		
		for($i=0;$i<$num;$i++)
		{		 
		    $aux = array();
		    $objeto  = $conec->get_objeto();
            $aux['nombre'] = $objeto->cat_nombre;
            $aux['tags']   = explode(",",$objeto->eca_tags);            
     		$categorias[]  = $aux;	
			$conec->siguiente();
		}	
  	   
		return	$categorias;   	
	}
	
	function verificar_nota(&$cat,$nota)
	{
	  	foreach($cat as $key=>$categoria)
		{
		    if (is_array($categoria['tags']) && (count($categoria['tags']) > 0))
			{
			    /*verifica si alguna palabra clave se encuentra en la nota*/
				
			    foreach($categoria['tags'] as $palabra)
				{
				    //echo $palabra."<br>";
					if (substr_count($nota['notacompleta'],$palabra) > 0)
				    {                     
					   $cat[$key]['notas'][] = $nota;					   
					   break;
					}	   
				}	
                /*-----------------------------------------------------------------*/				
			}		
		}	
	}
	
	
	function mostrar_reporte()
	{		
		$conec= new ADO();	
		$conec1= new ADO();	
		$conversor = new convertir();			
		
		//Aqui obtengo todas las palabras claves
	    $categorias = $this->obtener_categorias($conec);
		
		$sql = "SELECT emp_banner,emp_titulares,emp_resumen,emp_notacompleta FROM empresa WHERE emp_id =".$_POST["emp_id"];			
		$conec->ejecutar($sql);						
		$empresa = $conec->get_objeto();
        
		$sql = "SELECT distinct prd_id,prd_nombre,not_fecha,not_titulo,not_resumen,not_link,not_notacompleta,sec_nombre
				FROM empresa_periodico INNER JOIN periodico ON (epe_prd_id = prd_id and prd_estado = 'Habilitado') 
									   INNER JOIN noticias ON (not_prd_id = prd_id) 
                                       INNER JOIN seccion  ON (not_sec_id = sec_id)									   
				WHERE  epe_emp_id =".$_POST["emp_id"]; 	

        // foreach($categorias as $key=>$cat)
		// {
		    // if (is_array($cat['tags']) && (count($cat['tags']) > 0))
			// {
			    // $tags = implode("%' OR not_notacompleta LIKE '%",$cat['tags']);			
				// $sqlaux = $sql." and not_notacompleta LIKE '%".$tags."%'";
				
			// }		
		// }
		
		
		
		
		$pagina="'contenido_reporte'";
		
		$page="'about:blank'";
		
		$extpage="'reportes'";
		
		$features="'left=100,width=800,height=500,top=0,scrollbars=yes'";
		
		$extra1="'<html><head><title>Vista Previa</title><head>
				<link href=css/estilos.css rel=stylesheet type=text/css />
			  </head>
			  <body>
			  <div id=imprimir>
			  <div id=status>
			  <p>";
		$extra1.=" <a href=javascript:window.print();>Imprimir</a> 
				  <a href=javascript:self.close();>Cerrar</a></td>
				  </p>
				  </div>
				  </div>
				  <center>'";
		$extra2="'</center></body></html>'"; 
		
		$myday = setear_fecha(strtotime(date('Y-m-d')));
		////
				
		?>		
				<?php echo '	<table align=right border=0><tr><td><a href="javascript:var c = window.open('.$page.','.$extpage.','.$features.');
				  c.document.write('.$extra1.');
				  var dato = document.getElementById('.$pagina.').innerHTML;
				  c.document.write(dato);
				  c.document.write('.$extra2.'); c.document.close();
				  ">
				<img src="images/printer.png" align="right" width="20" border="0" title="IMPRIMIR">
				</a></td><td><img src="images/back.png" align="right" width="20" border="0"  title="VOLVER" onclick="javascript:location.href=\'gestor.php?mod=repptranse\';"></td></tr></table><br><br>
				';?>
						
				 

			<div id="contenido_reporte" style="clear:both;";>
			<center>
			
			<table style="font-size:12px;" width="100%" cellpadding="5" cellspacing="0" >
				<tr>
				    <!--<td><img src="imagenes/logoreporte.jpg"></td>
				    <td align="center"><strong><h3>INFORME DIARIO DE MONITOREO RELACIONAMIENTO COMUNITARIO.</h3></strong></td> -->
				  <td colspan="2" align="center"><img src="<? echo "imagenes/banner/".$empresa->emp_banner; ?>"></td> 
				</tr>
				<tr>
					 <td align="center" colspan="2"><strong><h3>PANORAMA <? echo $conversor->get_fecha_latina(date("Y-m-d"));?></h3></strong></td> 
				    
				    
				</tr>				 
			</table>			
			<br>
			<br>
			
			<table style="font-size:12px;" width="81%" cellpadding="5" cellspacing="3" >			
				<tr>
					<td colspan="2" align="right">
						<strong>Generado el: </strong> <?php echo $myday;?> <br><br>
					</td>
					
				</tr>				 
			</table>
			
			<?
			   if ($empresa->emp_titulares == 1)
			   {			
			?>		<strong><h2>TITULARES</h2></strong><br>
					<table   width="81%"  class="tablaReporte" cellpadding="0" cellspacing="0">
					
					<?php
						foreach($categorias as $key=>$cat)
						{
							if (is_array($cat['tags']) && (count($cat['tags']) > 0))
							{
							?>
								<thead>
								<tr>
									<th valign="MIDDLE" style="text-align:left; padding-left:10px" colspan="3">
										<b><? echo $cat['nombre'];?></b>
									</th>					
								</tr>
								</thead>
								<tbody>
							
							<?php
							
								$tags = implode("%' OR not_notacompleta LIKE '%",$cat['tags']);			
								$sqlaux = $sql." and not_notacompleta LIKE '%".$tags."%'";
								
								$conec->ejecutar($sqlaux);						
								$num=$conec->get_num_registros();
								for($i=0;$i<$num;$i++)
								{
									$objeto=$conec->get_objeto();
									?>
										<tr>
										    <td><? echo "<b>".$objeto->prd_nombre.":</b>"; ?></td>
											<td style="text-align:left; padding-left:10px">
											<?
												   echo "<a href='".$objeto->not_link."' target='_blank'><h3>".$objeto->not_titulo."</h3></a>";
											?>										
											</td>
											<td><? echo $conversor->get_fecha_latina($objeto->not_fecha); ?></td>		
										</tr>
									<?						
									$conec->siguiente();
								}
								?>
								</tbody>
								<?						
							}		
						}
					?>
					
					</table>
			<?
			   }
			?>
			
			<?
			   if (($empresa->emp_resumen==1) || ($empresa->emp_notacompleta==1))
			   {
			?>			
					<br><strong><h2>NOTAS COMPLETAS</h2></strong><br>
					<table   width="81%"  class="tablaReporte" cellpadding="0" cellspacing="5">
					<?php
						foreach($categorias as $key=>$cat)
						{
							if (is_array($cat['tags']) && (count($cat['tags']) > 0))
							{
							?>
								<thead>
								<tr>
									<th valign="MIDDLE" style="text-align:left; padding-left:10px">
										<b><? echo $cat['nombre'];?></b>
									</th>					
								</tr>
								</thead>
								<tbody>
							
							<?php
							
								$tags = implode("%' OR not_notacompleta LIKE '%",$cat['tags']);			
								$sqlaux = $sql." and not_notacompleta LIKE '%".$tags."%'";
								$conec->ejecutar($sqlaux);						
								$num=$conec->get_num_registros();
								for($i=0;$i<$num;$i++)
								{
									$objeto=$conec->get_objeto();
									?>
										<tr>
											
											<td style="text-align:left; padding-left:10px">
												<table border="0" cellpadding="5" cellspacing="0">
												<tr>
													<td width="20%"><? echo "<b>".$objeto->prd_nombre.":</b>"; ?></td>
													<td width="70%"><? echo "<h3><a href='".$objeto->not_link."' target='_blank'>".$objeto->not_titulo."</a><h3>"; ?></td>
													<td width="10%"><? echo $conversor->get_fecha_latina($objeto->not_fecha); ?></td>		
												</tr>
												<tr>
													
													<td colspan="3" align="justify">
														<?  if ($empresa->emp_resumen==1)  
														       echo $objeto->not_resumen; 
															else
															   echo $objeto->not_notacompleta; 
														?>
													<td>
												</tr>
												<tr>
													<td>&nbsp;</td>
													<td colspan="2"><a href="<? echo $objeto->not_link; ?>" target="_blank"><? echo $objeto->not_link; ?></a><td>											
												</tr>
												</table>
											</td>					
										</tr>
									<?						
									$conec->siguiente();
								}
								?>
								</tbody>
								<?						
							}		
						}
					?> 		
					</table>
		
			<?
				}
			?>
		
		</center>
		</div><br>
		
		<?php
		
		
	}
	
	
}
?>