<?php
	require_once('periodico.class.php');
	
	$periodico = new PERIODICO();
	
	if(!($periodico->verificar_permisos($_GET['tarea'])))
	{
		?>
		<script>
			location.href="log_out.php";
		</script>
		<?php
	}
	
	switch ($_GET['tarea'])
	{
		case 'AGREGAR':{
						
							if($periodico->datos())
							{
								$periodico->insertar_tcp();
							}
							else 
							{
								$periodico->formulario_tcp('blanco');
							}
							
							break;
		}
		case 'VER':{
							$periodico->cargar_datos();
													
							$periodico->formulario_tcp('ver');
							
							break;
		}
		case 'MODIFICAR':{
							if($periodico->datos())
							{
								$periodico->modificar_tcp();
							}
							else 
							{
								if(!($_POST))
								{
									$periodico->cargar_datos();
								}
								$periodico->formulario_tcp('cargar');
							}
							break;
		}					
		case 'ELIMINAR':{
							
							$periodico->eliminar_tcp();
								
							break;
		}
		case 'ACCEDER':{
							$periodico->dibujar_busqueda();
							break;
		}		
	}
?>