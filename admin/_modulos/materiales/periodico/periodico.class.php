<?php

//require_once("clases/phpmailer/class.phpmailer.php");

class PERIODICO extends BUSQUEDA
{
	var $formulario;
	var $mensaje;
	
	function PERIODICO()
	{
		//permisos
		$this->ele_id=128;
		
		$this->busqueda();
		
		if(!($this->verificar_permisos('AGREGAR')))
		{
			$this->ban_agregar=false;
		}
		//fin permisos
		
		$this->num_registros=14;
		
		$this->coneccion= new ADO();		
		
		
		
		
		$this->arreglo_campos[0]["nombre"]="prd_nombre";
		$this->arreglo_campos[0]["texto"]="Nombre";
		$this->arreglo_campos[0]["tipo"]="cadena";
		$this->arreglo_campos[0]["tamanio"]=25;
		
		
		
		$this->link='gestor.php';
		
		$this->modulo='periodico';
		
		$this->formulario = new FORMULARIO();
		
		$this->formulario->set_titulo('PERIODICO');
	}
	
	
	
	function dibujar_busqueda()
	{
		?>
		<script>		
		function ejecutar_script(id,tarea){
				var txt = 'Esta seguro de eliminar el Periodico?';
				
				$.prompt(txt,{ 
					buttons:{Si:true, No:false},
					callback: function(v,m,f){
						
						if(v){
								location.href='gestor.php?mod=periodico&tarea='+tarea+'&id='+id;
						}
												
					}
				});
			}

		</script>
		<?php
		$this->formulario->dibujar_cabecera();
		
		$this->dibujar_listado();
	}
	
	function set_opciones()
	{
		$nun=0;
		
		if($this->verificar_permisos('VER'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='VER';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_search.png';
			$this->arreglo_opciones[$nun]["nombre"]='VER';
			$nun++;
		}
		
		if($this->verificar_permisos('MODIFICAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='MODIFICAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_edit.png';
			$this->arreglo_opciones[$nun]["nombre"]='MODIFICAR';
			$nun++;
		}
		
		
		if($this->verificar_permisos('ELIMINAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='ELIMINAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_drop.png';
			$this->arreglo_opciones[$nun]["nombre"]='ELIMINAR';
			$this->arreglo_opciones[$nun]["script"]="ok";
			$nun++;
		}		
	}
	
	function dibujar_listado()
	{
		$sql="SELECT 
				distinct prd_id,prd_nombre,prd_url,prd_codigo,prd_codificacion,prd_estado
			  FROM 
				periodico 		
				";
		
		$this->set_sql($sql,' order by prd_id desc ');
		
		$this->set_opciones();
		
		$this->dibujar();
		
	}
	
	function dibujar_encabezado()
	{
		?>
			<tr>
			    <th>Nombre</th>
	        	<th>Web</th>
				<th>Codigo</th>
				<th>Codificacion</th>
				<th>Estado</th>				
	            <th class="tOpciones" width="150px">Opciones</th>
			</tr>
		<?PHP
	}
	
	function mostrar_busqueda()
	{
		$conversor = new convertir();
		
		for($i=0;$i<$this->numero;$i++)
			{
				
				$objeto=$this->coneccion->get_objeto();
				echo '<tr>';
					echo "<td>";
						echo $objeto->prd_nombre;
					echo "</td>";
					echo "<td>";
						echo  $objeto->prd_url;
					echo "</td>";
					echo "<td>";
						echo $objeto->prd_codigo;
					echo "</td>";
					echo "<td>";
						echo $objeto->prd_codificacion;
					echo "</td>";
					echo "<td>";
						echo $objeto->prd_estado;
					echo "</td>";
					echo "<td>";
						echo $this->get_opciones($objeto->prd_id);
					echo "</td>";
				echo "</tr>";
				
				$this->coneccion->siguiente();
			}
	}
	
	function cargar_datos()
	{
		$conec=new ADO();
		
		$sql="select 
			*  
		from periodico
				where prd_id = '".$_GET['id']."'";
		
		$conec->ejecutar($sql);
		
		$objeto=$conec->get_objeto();
		
		$_POST['prd_id']=$objeto->prd_id;
		$_POST['prd_nombre']=$objeto->prd_nombre;
		$_POST['prd_url']=$objeto->prd_url;
		$_POST['prd_codigo']=$objeto->prd_codigo;
		$_POST['prd_formato_fecha']=$objeto->prd_formato_fecha;
		$_POST['prd_codificacion']=$objeto->prd_codificacion;
		$_POST['prd_estado']=$objeto->prd_estado;		
	}
	
	function datos()
	{
		if($_POST)
		{
			//texto,  numero,  real,  fecha,  mail.
			$num=0;
			// $valores[$num]["etiqueta"]="Fecha";
			// $valores[$num]["valor"]=$_POST['seg_fecha'];
			// $valores[$num]["tipo"]="fecha";
			// $valores[$num]["requerido"]=true;
			// $num++;
			$valores[$num]["etiqueta"]="Nombre";
			$valores[$num]["valor"]=$_POST['prd_nombre'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			$valores[$num]["etiqueta"]="Codigo";
			$valores[$num]["valor"]=$_POST['prd_codigo'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			$valores[$num]["etiqueta"]="Estado";
			$valores[$num]["valor"]=$_POST['prd_estado'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			$valores[$num]["etiqueta"]="Codificacion";
			$valores[$num]["valor"]=$_POST['prd_codificacion'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$val=NEW VALIDADOR;
			
			$this->mensaje="";
			
			if($val->validar($valores))
			{
				return true;
			}
				
			else
			{
				$this->mensaje=$val->mensaje;
				return false;
			}
		}
			return false;
		
	}
	
	function formulario_tcp($tipo)
	{
				
		switch ($tipo)
		{
			case 'ver':{
						$ver=true;
						break;
						}
					
			case 'cargar':{
						$cargar=true;
						break;
						}
		}
		
		$url=$this->link.'?mod='.$this->modulo;
		
		$red=$url.'&tarea=ACCEDER';
		
		if(!($ver))
		{
			$url.="&tarea=".$_GET['tarea'];
		}
		
		if($cargar)
		{
			$url.='&id='.$_GET['id'];
		}
		
		$this->formulario->dibujar_tarea();
		
		if($this->mensaje<>"")
		{
			$this->formulario->mensaje('Error',$this->mensaje);
		}
		   
		    $fun=NEW FUNCIONES;		
			?>
			 
			
			
			<script type="text/javascript">
			
			$().ready(function(){
			   $('.botonagr').click(function(){
					var contextoClick = this;			
					var nuevo_indice_detcomp = $("#detper tbody tr").length;
					var data = "<tr><td>"+(nuevo_indice_detcomp+1)+"</td><td valign='top'><select name='sec_id[]' class='caja_texto'><?php $fun->combo("select sec_id as id,sec_nombre as nombre from seccion","");?></select></td><td valign='top'><select name='tipo[]' class='caja_texto'><option value='rss'>RSS</option><option value='url'>URL</option></select></td><td><textarea rows='8' cols='20' name='url_sec[]'></textarea></td><td><textarea rows='8' cols='20' name='expreg_sec[]'></textarea></td><td>expreg. titulo<textarea rows='2' cols='20' name='expreg_titulo[]'></textarea>expreg. resumen<textarea rows='2' cols='20' name='expreg_resumen[]'></textarea>expreg. link<textarea rows='2' cols='20' name='expreg_link[]'></textarea></td><td >expreg. Nota Completa<textarea rows='7' cols='20' name='expreg_nota[]'></textarea>expreg. imagenes<textarea rows='2' cols='20' name='expreg_imagen[]'></textarea></td><td><img src='images/b_drop.png' class='borrar-detalle' style='cursor:pointer'></td></tr>";
					$('#detper tbody').append(data);		
				});	
				
				
               $('#detper').delegate('.borrar-detalle','click',function(){
					if (confirm('Esta usted seguro de eliminar esta fila?')){				   
						var con=1;       					
						var $link = $(this);					
						$link.closest('tr').remove();						
						$("#detper tbody tr").each(function(){						
							 $(this).children('td').eq(0).text(con);
							 con++;
						});						
					}
				});				
			
			});
			
							
				function verificar(id)
				{
					var cant = $('#tprueba tbody').children().length;               
					var ban=true;
					if(cant > 0)
					{
						$('#tprueba tbody').children().each(function(){
						var dato=$(this).eq(0).children().eq(0).children().eq(0).attr('value');	
						
						if(id==dato)
						{
							ban=false;
						}
										 
						}); 				
					}  
					return ban;				
				}

				function enviar_formulario(){
					
						
							var nume=$("#detper tbody tr").length;
							if(nume > 0)
							{
								document.frm_sentencia.submit();
							}
							else
							{
								$.prompt('Agregue al menos una seccion',{ opacity: 0.8 });			
							}
						
					
				}
				</script>
			<script type="text/javascript" src="js/ajax.js"></script>
			<div id="Contenedor_NuevaSentencia" >
			<form id="frm_sentencia" name="frm_sentencia" action="<?php echo $url;?>" method="POST" enctype="multipart/form-data">  
				<div id="FormSent" style="width:100%;">
				  
						<div class="Subtitulo">Datos</div>
						<div id="ContenedorSeleccion">						   
						
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">*</span>Nombre</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto"  name="prd_nombre" id="prd_nombre"  maxlength="200" size="40"  value="<?php echo $_POST['prd_nombre'];?>">
							   </div>
							</div>
							<!--Fin-->	
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">*</span>Codigo</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto"  name="prd_codigo" id="prd_codigo"  maxlength="200" size="40"  value="<?php echo $_POST['prd_codigo'];?>">
							   </div>
							</div>
							<!--Fin-->	
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Web</div>
							   <div id="CajaInput">
							    <textarea rows="2" cols="50" name="prd_url" id="prd_url"><?php echo $_POST['prd_url'];?></textarea>									
								 
							   </div>
							</div>
							<!--Fin-->	
                            <!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Formato Fecha</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto"  name="prd_formato_fecha" id="prd_formato_fecha"  maxlength="200" size="40"  value="<?php echo $_POST['prd_formato_fecha'];?>">
							   </div>
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">* </span>Codificacion</div>
							   <div id="CajaInput">
								    <select name="prd_codificacion" class="caja_texto">
									<option value="" >Seleccione</option>
									<option value="Si" <?php if($_POST['prd_codificacion']=='Si') echo 'selected="selected"'; ?>>Si</option>
									<option value="No" <?php if($_POST['prd_codificacion']=='No') echo 'selected="selected"'; ?>>No</option>
									</select>
							   </div>
							</div>
							<!--Fin-->	
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">* </span>Estado?</div>
							   <div id="CajaInput">
								    <select name="prd_estado" class="caja_texto">
									<option value="" >Seleccione</option>
									<option value="Habilitado" <?php if($_POST['prd_estado']=='Habilitado') echo 'selected="selected"'; ?>>Habilitado</option>
									<option value="Deshabilitado" <?php if($_POST['prd_estado']=='Deshabilitado') echo 'selected="selected"'; ?>>Deshabilitado</option>
									</select>
							   </div>
							</div>
							<!--Fin-->
						
							<div id="ContenedorDiv" style="width:100%">
								<input type="button" class="boton botonagr" name="" value="Agregar Fila"  onclick="">
								</div>
							    <br>
								<br>								
								<table class="tablaReporte" width="100%" cellpadding="0" cellspacing="0" border="0" id="detper">
								<thead>
									<tr>
										<th>&nbsp;#&nbsp;</th>
										<th>SECCION</th>
										<th>TIPO</th>
										<th>URL SEC.</th>
										<th>EXP.REG. SEC</th>
										<th>DATOS NOTA</th>
                                        <th>EXP.REG. NOTA</th>										
										<th><?php echo "&nbsp;";?></th>
									</tr>
								</thead>  
								<tbody>
								<?php
									if($_GET['tarea']=='MODIFICAR' || $_GET['tarea']=='VER')
										$this->cargar_filas_detalle($_GET['id']);
									?>
									
									 
								</tbody>    
								</table> 
						
						</div>
						
						
					
						<div id="ContenedorDiv">
						   <div id="CajaBotones">
								<center>
								<?php
								if(!($ver))
								{
									?>
									<input type="button" class="boton" name="" value="Guardar"  onclick="javascript:enviar_formulario()">
									<input type="reset" class="boton" name="" value="Cancelar">
									<input type="button" class="boton" name="" value="Volver" onclick="javascript:location.href='<?php echo $red;?>';">
									<?php
								}
								else
								{
									?>
									<input type="button" class="boton" name="" value="Volver" onclick="javascript:location.href='<?php echo $red;?>';">
									<?php
								}
								?>
								</center>
						   </div>
						</div>
				</div>
			</form>
		</div>
		<?php
	}
	
	function insertar_tcp()
	{
		
		$conec= new ADO();
	
		$sql="insert into periodico (
		prd_nombre,
		prd_codigo,
		prd_formato_fecha,
		prd_url,
		prd_codificacion,	
		prd_estado		
		) 
		values (
		'".$_POST['prd_nombre']."',
		'".$_POST['prd_codigo']."',
		'".$_POST['prd_formato_fecha']."',
		'".$_POST['prd_url']."',
		'".$_POST['prd_codificacion']."',
		'".$_POST['prd_estado']."'				
		)";
	
		$conec->ejecutar($sql,false);
		
		$llave=mysql_insert_id();
		
		
		$i=0;
		
		foreach($_POST['sec_id'] as $seccion)
		{
			
			$sql="insert into periodico_seccion(pes_prd_id,pes_sec_id,pes_tipo,pes_url_sec,pes_expreg_sec,pes_expreg_nota,pes_expreg_titulo,pes_expreg_resumen,pes_expreg_link,pes_expreg_imagen) values 
							('".$llave."','".$seccion."','".$_POST['tipo'][$i]."','".$_POST['url_sec'][$i]."','".addslashes($_POST['expreg_sec'][$i])."','".addslashes($_POST['expreg_nota'][$i])."','".addslashes($_POST['expreg_titulo'][$i])."','".addslashes($_POST['expreg_resumen'][$i])."','".addslashes($_POST['expreg_link'][$i])."','".addslashes($_POST['expreg_imagen'][$i])."')";

			$conec->ejecutar($sql);
			$i++;
		}
		
		$mensaje='Periodico Agregado Correctamente.';
		
		$tipo='Correcto';
		
		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER'.'�'.$this->link.'?mod='.$this->modulo.'&tarea=AGREGAR','Ir al listado�Continuar Agregando',$tipo);
	}
	
	function modificar_tcp()
	{
		
	
			$conec= new ADO();
		
			$codigo="";
						
			
			$sql="update periodico set 
									prd_nombre='".$_POST['prd_nombre']."',
									prd_codigo='".$_POST['prd_codigo']."',
									prd_formato_fecha='".$_POST['prd_formato_fecha']."',
									prd_url='".$_POST['prd_url']."',
									prd_codificacion='".$_POST['prd_codificacion']."',
									prd_estado='".$_POST['prd_estado']."'								
								where prd_id='".$_GET['id']."'";
			
			$conec->ejecutar($sql);
			
			$sql="delete from periodico_seccion  where pes_prd_id='".$_GET['id']."'";

			$conec->ejecutar($sql);
			
			
			
			$i=0;
		
			foreach($_POST['sec_id'] as $seccion)
			{
				
				$sql="insert into periodico_seccion(pes_prd_id,pes_sec_id,pes_tipo,pes_url_sec,pes_expreg_sec,pes_expreg_nota,pes_expreg_titulo,pes_expreg_resumen,pes_expreg_link,pes_expreg_imagen) values 
								('".$_GET['id']."','".$seccion."','".$_POST['tipo'][$i]."','".$_POST['url_sec'][$i]."','".addslashes($_POST['expreg_sec'][$i])."','".addslashes($_POST['expreg_nota'][$i])."','".addslashes($_POST['expreg_titulo'][$i])."','".addslashes($_POST['expreg_resumen'][$i])."','".addslashes($_POST['expreg_link'][$i])."','".addslashes($_POST['expreg_imagen'][$i])."')";

				$conec->ejecutar($sql);
				$i++;
			}
			
			
			
			
			$mensaje='Periodico modificado Correctamente';
			
			$tipo='correcto';
		
			$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER','',$tipo);
	}
	
	function eliminar_tcp()
	{
		$conec= new ADO();
		
		$sql="delete FROM periodico where prd_id='".$_GET['id']."'";

		$conec->ejecutar($sql);
		
		$sql="delete from periodico_seccion where pes_prd_id='".$_GET['id']."'";
		 
		$conec->ejecutar($sql);	
		
		$mensaje='Periodico Eliminado Correctamente.';
		
		$tipo='Correcto';
		
		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER','',$tipo);
	}
	
	
	
	
	function cargar_filas_detalle($id)
	{
		$conec= new ADO();
	
		$sql="select * from periodico_seccion where pes_prd_id='$id'";

		$conec->ejecutar($sql);
		
		$num=$conec->get_num_registros();
		
		$cad="";
		$fun=NEW FUNCIONES;		
		for($i=0;$i<$num;$i++)
		{
			
			$objeto=$conec->get_objeto();
			
			?>
			<tr>
				<td><? echo ($i+1);?></td>
				<td valign="top"><select name='sec_id[]' class='caja_texto'><?php $fun->combo("select sec_id as id,sec_nombre as nombre from seccion",$objeto->pes_sec_id);?></select></td>
				<td valign="top">
				   <select name='tipo[]' class='caja_texto'>
				       <? 
					      if ($objeto->pes_tipo == 'rss')
					          echo "<option value='rss' selected>RSS</option>";
						  else
                              echo "<option value='rss'>RSS</option>";
                          
                          if ($objeto->pes_tipo == 'url')
                              echo "<option value='url' selected>URL</option>";
                          else
                              echo "<option value='url'>URL</option>"; 						  
					   ?>
				           
					  
					</select>
				</td>
				<td><textarea rows='8' cols='20' name='url_sec[]'><? echo $objeto->pes_url_sec;?></textarea></td>
				<td><textarea rows='8' cols='20' name='expreg_sec[]'><? echo $objeto->pes_expreg_sec;?></textarea></td>
				<td>expreg. titulo<textarea rows='2' cols='20' name='expreg_titulo[]'><? echo $objeto->pes_expreg_titulo;?></textarea>
				    expreg. resumen<textarea rows='2' cols='20' name='expreg_resumen[]'><? echo $objeto->pes_expreg_resumen;?></textarea>
					expreg. link<textarea rows='2' cols='20' name='expreg_link[]'><? echo $objeto->pes_expreg_link;?></textarea>
				</td>
				<td>expreg. Nota Completa<textarea rows='7' cols='20' name='expreg_nota[]'><? echo $objeto->pes_expreg_nota;?></textarea>
				    expreg. Imagen<textarea rows='2' cols='20' name='expreg_imagen[]'><? echo $objeto->pes_expreg_imagen;?></textarea>
				</td>
				<td><img src='images/b_drop.png' class='borrar-detalle' style='cursor:pointer'></td>
			</tr>
			
			
			
			
			<?php
			
			$conec->siguiente();
		}
		
	}
	
	
	function reporte()
	{
	    $conec= new ADO();	
		$conec1= new ADO();	
		$conversor = new convertir();		
		$cad="";		
		
		$sql = "SELECT 
				  distinct seg_id,seg_gerente,seg_etapa,seg_coordinadora,seg_monitor,seg_supervisor,seg_monitor,seg_fecha
			    FROM 
				  seguimiento 
				WHERE
                  seg_id =".$_GET["id"]; 
		
		$conec->ejecutar($sql);
		$objeto=$conec->get_objeto();
		
		
		$sql = "SELECT are_nombre FROM seguimiento_area inner join area on (sea_are_id = are_id)				  
				WHERE				  
                  sea_seg_id =".$_GET["id"]; 
		$conec1->ejecutar($sql);
		
		$num=$conec1->get_num_registros();
		$areas = array();
		for($i=0;$i<$num;$i++)
		{		    
			$objeto1=$conec1->get_objeto();
			$areas[] = $objeto1->are_nombre;
			$conec1->siguiente();
		}	 
		//$objeto=$conec->get_objeto();		  
		
		
		
		
		
		
		////
		$pagina="'contenido_reporte'";
		
		$page="'about:blank'";
		
		$extpage="'reportes'";
		
		$features="'left=100,width=800,height=500,top=0,scrollbars=yes'";
		
		$extra1="'<html><head><title>Vista Previa</title><head>
				<link href=css/estilos.css rel=stylesheet type=text/css />
			  </head>
			  <body>
			  <div id=imprimir>
			  <div id=status>
			  <p>";
		$extra1.=" <a href=javascript:window.print();>Imprimir</a> 
				  <a href=javascript:self.close();>Cerrar</a></td>
				  </p>
				  </div>
				  </div>
				  <center>'";
		$extra2="'</center></body></html>'"; 
		
		$myday = setear_fecha(strtotime(date('Y-m-d')));
		////
				
		?>		
				<?php echo '	<table align=right border=0><tr><td><a href="javascript:var c = window.open('.$page.','.$extpage.','.$features.');
				  c.document.write('.$extra1.');
				  var dato = document.getElementById('.$pagina.').innerHTML;
				  c.document.write(dato);
				  c.document.write('.$extra2.'); c.document.close();
				  ">
				<img src="images/printer.png" align="right" width="20" border="0" title="IMPRIMIR">
				</a></td><td><img src="images/back.png" align="right" width="20" border="0"  title="VOLVER" onclick="javascript:location.href=\'gestor.php?mod=repptranse\';"></td></tr></table><br><br>
				';?>
						
				 

			<div id="contenido_reporte" style="clear:both;";>
			<center>
			
			<table style="font-size:12px;" width="100%" cellpadding="5" cellspacing="0" >
				<tr>
				    <td><img src="imagenes/logoreporte.jpg"></td>
				    <td align="center"><strong><h3>INFORME DIARIO DE MONITOREO RELACIONAMIENTO COMUNITARIO.</h3></strong></td>
				    <!--<td><img src="imagenes/rrcc.jpg"></td> -->
				</tr>
				<tr>
					<td align="center"><strong><h3>INFORME</h3></strong></td>
				    <td align="center"><strong><? echo $conversor->get_fecha_latina($objeto->seg_fecha);?></strong></td>
				    
				</tr>				 
			</table>			
			<br>
			<br>
			<br>
			<table style="font-size:12px;" width="95%" cellpadding="5" cellspacing="3" >
				<tr>
				    <td colspan="2" align="center"><b>DATOS RESPONSABILIDADES</b><br><br></td>				    			    
				</tr>
				<tr>
				    <td width="220px"><b>&nbsp;�rea(s):</b></td>
				    <td ><b><? echo implode(" - ",$areas);?></b></td>				    
				</tr>
				<tr>
				    <td>&nbsp;<b>Gerente HSE-RRCC:</b></td>
				    <td><? echo $objeto->seg_gerente;?></td>				    
				</tr>
				<tr>
				    <td>&nbsp;<b>Coordinadora RR CC: </b></td>
				    <td><? echo $objeto->seg_coordinadora;?></td>				    
				</tr>
				<tr>
				    <td>&nbsp;<b>Etapa Monitoreo: </b></td>
				    <td><b><? echo $objeto->seg_etapa;?></b></td>				    
				</tr>
				<tr>
				    <td>&nbsp;<b>Monitor RRCC: </b></td>
				    <td><? echo $objeto->seg_monitor;?></td>				    
				</tr>
				<tr>
				    <td>&nbsp;<b>SUPERVISORES DE CAMPO:</b></td>
				    <td><? echo $objeto->seg_supervisor;?></td>				    
				</tr>
				<tr>
					<td colspan="2" align="right">
						<strong>Impreso el: </strong> <?php echo $myday;?> <br><br>
					</td>
					
				</tr>				 
			</table>
			
			
			
			<table   width="95%"  class="tablaReporte" cellpadding="0" cellspacing="0">
				<thead>
				<tr>
					<th valign="top">
						<b>Nro.</b>
					</th>
					<th valign="top">
						<b>RESUMEN Y DESCRIPCION DE ACTIVIDADES</b>
					</th>
					<th valign="top">
						<b>COMENTARIOS</b>
					</th>
					<th valign="top">
						<b>ACCIONES A REALIZAR</b>
					</th>
					<th valign="top">
						<b>FECHA DE CIERRE</b>
					</th>
					<th valign="top">
						<b>RESPONSABLES</b>
					</th>
				</tr>
				</thead>
				<tbody>
		<?php
         $sql = "SELECT 
				  sed_descripcion,sed_comentarios,sed_acciones,sed_responsables,sed_fecha
			    FROM 
				  seguimiento_detalle 
				WHERE
                  sed_seg_id =".$_GET["id"]; 
		//echo $sql;
		$conec->ejecutar($sql);
		$objeto=$conec->get_objeto();
        $num=$conec->get_num_registros();
		
		 for($i=0;$i<$num;$i++)
		{
			$objeto=$conec->get_objeto();

			echo '<tr>';					
					echo "<td>";
						echo ($i+1);
					echo "</td>";
					
					echo "<td valign='top'>";
						echo $objeto->sed_descripcion."&nbsp;";
					echo "</td>";
					
					echo "<td valign='top'>";
						echo $objeto->sed_comentarios."&nbsp;";
					echo "</td>";
					
					echo "<td valign='top'>";
						echo $objeto->sed_acciones."&nbsp;";
					echo "</td>";
					echo "<td valign='top'>";
						echo $conversor->get_fecha_latina($objeto->sed_fecha);
					echo "</td>";
					echo "<td valign='top'>";
						echo $objeto->sed_responsables."&nbsp;";
					echo "</td>";
					
				echo "</tr>";
				
			
			$conec->siguiente();
		}
		?>
		
		<?php		
		///
		echo "</tbody></table></center></div><br>";
	
	}
	
	
}
?>