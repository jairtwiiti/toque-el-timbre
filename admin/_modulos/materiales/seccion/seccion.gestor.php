<?php
	
	require_once('seccion.class.php');
	
	$seccion = new SECCION();
	
	if(!($seccion->verificar_permisos($_GET['tarea'])))
	{
		?>
		<script>
			location.href="log_out.php";
		</script>
		<?php
	}
	
	switch ($_GET['tarea'])
	{
		case 'AGREGAR':{
						
							if($seccion->datos())
							{
								$seccion->insertar_tcp();
							}
							else 
							{
								$seccion->formulario_tcp('blanco');
							}
					
						
						
						break;}
		case 'VER':{
						$seccion->cargar_datos();
												
						$seccion->formulario_tcp('ver');
						
						break;}
		case 'MODIFICAR':{
							if($_GET['acc']=='Imagen')
							{
								$seccion->eliminar_imagen();
							}
							else
							{
								if($seccion->datos())
								{
									$seccion->modificar_tcp();
								}
								else 
								{
									if(!($_POST))
									{
										$seccion->cargar_datos();
									}
									$seccion->formulario_tcp('cargar');
								}
							}
						break;}
		case 'ELIMINAR':{
							
								if(isset($_POST['sec_id']))
								{
									if(trim($_POST['sec_id'])<>"")
									{
										$seccion->eliminar_tcp();
									}
									else 
									{
										$seccion->dibujar_busqueda();
									}
								}
								else 
								{
									$seccion->formulario_confirmar_eliminacion();
								}
							
							
						break;}
		case 'ACCEDER':{	
						
						if($_GET['acc']<>"")
						{
							$seccion->orden($_GET['tec'],$_GET['acc'],$_GET['or']);
						}
						$seccion->dibujar_busqueda();
						break;
						}
		
	}
		
?>