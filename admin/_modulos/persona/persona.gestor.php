<?php
	
	require_once('persona.class.php');
	
	$persona = new PERSONA();
	
	if($_GET['tarea']<>"")
	{
		if(!($persona->verificar_permisos($_GET['tarea'])))
		{
			?>
			<script>
				location.href="log_out.php";
			</script>
			<?php
		}
	}
	
	switch ($_GET['tarea'])
	{
		case 'AGREGAR':{
						
							if($persona->datos())
							{
								$persona->insertar_tcp();
							}
							else 
							{
								$persona->formulario_tcp('blanco');
							}
					
						
						
						break;}
		case 'VER':{
						$persona->cargar_datos();
												
						$persona->formulario_tcp('ver');
						
						break;}
		case 'MODIFICAR':{
							if($_GET['acc']=='Imagen')
							{
								$persona->eliminar_imagen();
							}
							else
							{
								if($persona->datos())
								{
									$persona->modificar_tcp();
								}
								else 
								{
									if(!($_POST))
									{
										$persona->cargar_datos();
									}
									$persona->formulario_tcp('cargar');
								}
							}
						break;}
		case 'ELIMINAR':{
							
								if(isset($_POST['per_id']))
								{
									if(trim($_POST['per_id'])<>"")
									{
										$persona->eliminar_tcp();
									}
									else 
									{
										$persona->dibujar_busqueda();
									}
								}
								else 
								{
									$persona->formulario_confirmar_eliminacion();
								}
							
							
						break;}
		case 'ACCEDER':{
		             if($_GET['acc']<>"")
						{
							$persona->orden($_GET['pro'],$_GET['acc'],$_GET['or']);
						}
		             $persona->dibujar_busqueda();break;
		}
		
	}
		
?>