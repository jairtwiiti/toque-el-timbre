<?php

class PERSONA extends BUSQUEDA 
{
	var $formulario;
	var $mensaje;
	
	function PERSONA()
	{
		//permisos
		$this->ele_id=23;
		
		$this->busqueda();
		
		if(!($this->verificar_permisos('AGREGAR')))
		{
			$this->ban_agregar=false;
		}
		//fin permisos
		
		$this->num_registros=14;
		
		$this->coneccion= new ADO();
		
		$this->arreglo_campos[0]["nombre"]="per_nombre";
		$this->arreglo_campos[0]["texto"]="Nombre";
		$this->arreglo_campos[0]["tipo"]="cadena";
		$this->arreglo_campos[0]["tamanio"]=40;
		
		$this->arreglo_campos[1]["nombre"]="per_apellido";
		$this->arreglo_campos[1]["texto"]="Apellido";
		$this->arreglo_campos[1]["tipo"]="cadena";
		$this->arreglo_campos[1]["tamanio"]=25;
		
		$this->arreglo_campos[2]["nombre"]="per_ci";
		$this->arreglo_campos[2]["texto"]="CI";
		$this->arreglo_campos[2]["tipo"]="cadena";
		$this->arreglo_campos[2]["tamanio"]=25;
		
		$this->link='gestor.php';
		
		$this->modulo='persona';
		
		$this->formulario = new FORMULARIO();
		
		$this->formulario->set_titulo('PERSONA');
		
		
	}
	
	
	function dibujar_busqueda()
	{
		
		$this->formulario->dibujar_cabecera();
		
		$this->dibujar_listado();
	}
	
		
	function set_opciones()
	{
				
		$nun=0;
		
		if($this->verificar_permisos('VER'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='VER';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_search.png';
			$this->arreglo_opciones[$nun]["nombre"]='VER';
			$nun++;
		}
		
		if($this->verificar_permisos('MODIFICAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='MODIFICAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_edit.png';
			$this->arreglo_opciones[$nun]["nombre"]='MODIFICAR';
			$nun++;
		}
		
		if($this->verificar_permisos('ELIMINAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='ELIMINAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_drop.png';
			$this->arreglo_opciones[$nun]["nombre"]='ELIMINAR';
			$nun++;
		}
	}
	
	function dibujar_listado()
	{
		$sql="SELECT * FROM gr_persona";
		
		$this->set_sql($sql,' order by per_orden desc');
		
		$this->set_opciones();
		
		$this->dibujar();
		
	}
	
	function dibujar_encabezado()
	{
		?>
			<tr>
	        	<th>Nombre</th>
				<th>Apellido</th>
				<th>CI</th>
				<th>Celular</th>
				<th>Telefono</th>
				<th>Orden</th>
	            <th class="tOpciones" width="100px">Opciones</th>
			</tr>
			
		<?PHP
	}
	
	function mostrar_busqueda()
	{
		$conversor = new convertir();
		
		for($i=0;$i<$this->numero;$i++)
			{
				
				$objeto=$this->coneccion->get_objeto();
				echo '<tr>';
									
					echo "<td>";
						echo $objeto->per_nombre;
					echo "&nbsp;</td>";
					echo "<td>";
						echo $objeto->per_apellido;
					echo "&nbsp;</td>";
					echo "<td>";
						echo $objeto->per_ci;
					echo "&nbsp;</td>";
						echo "<td>";
						echo $objeto->per_celular;
					echo "&nbsp;</td>";
						echo "<td>";
						echo $objeto->per_telefono;
					echo "&nbsp;</td>";
					echo "<td>";
						?>
						<center><a href="<?php echo $this->link.'?mod='.$this->modulo.'&tarea=ACCEDER&pro='.$objeto->per_id.'&acc=s&or='.$objeto->per_orden;?>"><img src="images/subir.png" border="0"></a><a href="<?php echo $this->link.'?mod='.$this->modulo.'&tarea=ACCEDER&pro='.$objeto->per_id.'&acc=b&or='.$objeto->per_orden;?>"><img src="images/bajarr.png" border="0"></a></center>
						<?php
					echo "</td>";
					echo "<td>";
						echo $this->get_opciones($objeto->per_id);
					echo "</td>";
				echo "</tr>";
				
				$this->coneccion->siguiente();
			}
	}
	
	function orden($proyecto,$accion,$ant_orden)
	{
		$conec= new ADO();
		
		if($accion=='s')
			$cad=" where per_orden > $ant_orden order by per_orden asc";
		else
			$cad=" where per_orden < $ant_orden order by per_orden desc";

		$consulta = "
		select 
			per_id,per_orden 
		from 
			gr_persona
		$cad
		limit 0,1
		";	

		$conec->ejecutar($consulta);

		$num = $conec->get_num_registros();   

		if($num > 0)
		{
			$objeto=$conec->get_objeto();
			
			$nu_orden=$objeto->per_orden;
			
			$id=$objeto->per_id;
			
			$consulta = "update gr_persona set per_orden='$nu_orden' where per_id='$proyecto'";	

			$conec->ejecutar($consulta);
			
			$consulta = "update gr_persona set per_orden='$ant_orden' where per_id='$id'";	

			$conec->ejecutar($consulta);
		}	
	}
	
	function cargar_datos()
	{
		$conec=new ADO();
		
		$sql="select * from gr_persona
				where per_id = '".$_GET['id']."'";
		
		$conec->ejecutar($sql);
		
		$objeto=$conec->get_objeto();
		 	 	 	 	 	 	 	 	 
		$_POST['per_nombre']=$objeto->per_nombre;
		
		$_POST['per_apellido']=$objeto->per_apellido;
		
		$_POST['per_email']=$objeto->per_email;
		
		$_POST['per_foto']=$objeto->per_foto;
		
		$_POST['per_telefono']=$objeto->per_telefono;
		
		$_POST['per_celular']=$objeto->per_celular;
		
		$_POST['per_direccion']=$objeto->per_direccion;
		
		$_POST['per_ci']=$objeto->per_ci;
		
		$_POST['per_cargo']=$objeto->per_cargo;
		
		$_POST['per_estado']=$objeto->per_estado;
		
		$_POST['per_fecha_nacimiento']=$objeto->per_fecha_nacimiento;
		
		$_POST['per_fecha_ingreso']=$objeto->per_fecha_ingreso;
	}
	
	function datos()
	{
		if($_POST)
		{
			//texto,  numero,  real,  fecha,  mail.
			$num=0;
			$valores[$num]["etiqueta"]="Nombre";
			$valores[$num]["valor"]=$_POST['per_nombre'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			$valores[$num]["etiqueta"]="Apellido";
			$valores[$num]["valor"]=$_POST['per_apellido'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			$valores[$num]["etiqueta"]="Email";
			$valores[$num]["valor"]=$_POST['per_email'];
			$valores[$num]["tipo"]="mail";
			$valores[$num]["requerido"]=true;
			
			$val=NEW VALIDADOR;
			
			$this->mensaje="";
			
			if($val->validar($valores))
			{
				return true;
			}
				
			else
			{
				$this->mensaje=$val->mensaje;
				return false;
			}
		}
			return false;
	}
	
	function formulario_tcp($tipo)
	{
				switch ($tipo)
				{
					case 'ver':{
								$ver=true;
								break;
								}
							
					case 'cargar':{
								$cargar=true;
								break;
								}
				}
				
				$url=$this->link.'?mod='.$this->modulo;
				
				$red=$url;
				
				if(!($ver))
				{
					$url.="&tarea=".$_GET['tarea'];
				}
				
				if($cargar)
				{
					$url.='&id='.$_GET['id'];
				}

		
		    $this->formulario->dibujar_tarea('PERSONA');
		
			if($this->mensaje<>"")
			{
				$this->formulario->dibujar_mensaje($this->mensaje);
			}
			?>
		<div id="Contenedor_NuevaSentencia">
			<form id="frm_sentencia" name="frm_sentencia" action="<?php echo $url;?>" method="POST" enctype="multipart/form-data">  
				<div id="FormSent">
				  
					<div class="Subtitulo">Datos</div>
						<div id="ContenedorSeleccion">
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Foto</div>
							   <div id="CajaInput">
							   <?php
								if($_POST['per_foto']<>"")
								{	$foto=$_POST['per_foto'];
									$b=true;
								}	
								else
								{	$foto='sin_foto.gif';
									$b=false;
								}
								if(($ver)||($cargar))
								{	?>
									<img src="imagenes/persona/chica/<?php echo $foto;?>" border="0" ><?php if($b and $_GET['tarea']=='MODIFICAR') echo '<a href="'.$this->link.'?mod='.$this->modulo.'&tarea=MODIFICAR&id='.$_GET['id'].'&img='.$foto.'&acc=Imagen"><img src="images/b_drop.png" border="0"></a>';?><br>
									<input   name="per_foto" type="file" id="per_foto" />
									<?php
								}
								else 
								{
									?>
									<input  name="per_foto" type="file" id="per_foto" />
									<?php
								}
								?>
								<input   name="fotooculta" type="hidden" id="fotooculta" value="<?php echo $_POST['per_foto'].$_POST['fotooculta'];?>"/>
							   </div>
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">* </span>Nombre</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto" name="per_nombre" id="per_nombre" maxlength="250"  size="40" value="<?php echo $_POST['per_nombre'];?>">
							   </div>
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">* </span>Apellido</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto" name="per_apellido" id="per_apellido" maxlength="250"  size="40" value="<?php echo $_POST['per_apellido'];?>">
							   </div>
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >CI</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto" name="per_ci" id="per_ci" size="20" maxlength="250"  value="<?php echo $_POST['per_ci'];?>">
							   </div>
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Fecha de Nacimiento</div>
								 <div id="CajaInput">
									<input readonly="readonly" class="caja_texto" name="per_fecha_nacimiento" id="per_fecha_nacimiento" size="12" value="<?php echo $_POST['per_fecha_nacimiento'];?>" type="text">
										<input name="but_fecha_pago" id="but_fecha_pago" class="boton_fecha" value="..." type="button">
										<script type="text/javascript">
														Calendar.setup({inputField     : "per_fecha_nacimiento"
																		,ifFormat     :     "%Y-%m-%d",
																		button     :    "but_fecha_pago"
																		});
										</script>
								</div>		
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Fecha de Ingreso</div>
								 <div id="CajaInput">
									<input readonly="readonly" class="caja_texto" name="per_fecha_ingreso" id="per_fecha_ingreso" size="12" value="<?php echo $_POST['per_fecha_ingreso'];?>" type="text">
										<input name="but_fecha_pagos" id="but_fecha_pagos" class="boton_fecha" value="..." type="button">
										<script type="text/javascript">
														Calendar.setup({inputField     : "per_fecha_ingreso"
																		,ifFormat     :     "%Y-%m-%d",
																		button     :    "but_fecha_pagos"
																		});
										</script>
								</div>		
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Cargo</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto" name="per_cargo" id="per_cargo" size="50" value="<?php echo $_POST['per_cargo'];?>">
							   </div>
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">* </span>Email</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto" name="per_email" id="per_email" size="40" value="<?php echo $_POST['per_email'];?>">
							   </div>
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Telefono</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto" name="per_telefono" id="per_telefono" size="15" value="<?php echo $_POST['per_telefono'];?>">
							   </div>
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Celular</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto" name="per_celular" id="per_celular" size="15" value="<?php echo $_POST['per_celular'];?>">
							   </div>
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Direccion</div>
							   <div id="CajaInput">
							   <textarea class="area_texto" name="per_direccion" id="per_direccion" cols="31" rows="3"><?php echo $_POST['per_direccion']?></textarea>
							   </div>
							</div>
							<!--Fin-->
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">* </span>Visible?</div>
							   <div id="CajaInput">
								    <select name="per_estado" class="caja_texto">
									<option value="" >Seleccione</option>
									<option value="Si" <?php if($_POST['per_estado']=='Si') echo 'selected="selected"'; ?>>Si</option>
									<option value="No" <?php if($_POST['per_estado']=='No') echo 'selected="selected"'; ?>>No</option>
									</select>
							   </div>
							</div>
							<!--Fin-->
						</div>
					
						<div id="ContenedorDiv">
						   <div id="CajaBotones">
								<center>
								<?php
								if(!($ver))
								{
									?>
									<input type="submit" class="boton" name="" value="Guardar">
									<input type="reset" class="boton" name="" value="Cancelar">
									<input type="button" class="boton" name="" value="Volver" onclick="javascript:location.href='<?php echo $red;?>';">
									<?php
								}
								else
								{
									?>
									<input type="button" class="boton" name="" value="Volver" onclick="javascript:location.href='<?php echo $red;?>';">
									<?php
								}
								?>
								</center>
						   </div>
						</div>
				</div>
			</form>
		</div>
		<?php
	}
	
	function insertar_tcp()
	{
		
		$verificar=NEW VERIFICAR;
		
		$parametros[0]=array('per_nombre','per_apellido');
		$parametros[1]=array($_POST['per_nombre'],$_POST['per_apellido']);
		$parametros[2]=array('gr_persona');

		if($verificar->validar($parametros))
		{
		    $conec= new ADO();
		
			$sql=" select max(per_orden) as ultimo from gr_persona ";
			
			$conec->ejecutar($sql);
			
			$objeto=$conec->get_objeto();
			
			$orden=$objeto->ultimo + 1;
		
		
				
			if($_FILES['per_foto']['name']<>"")
			{
				$result=$this->subir_imagen($nombre_archivo,$_FILES['per_foto']['name'],$_FILES['per_foto']['tmp_name']);
				
				$sql="insert into gr_persona(per_nombre,per_apellido,per_email,per_foto,per_telefono,per_celular,per_direccion,per_ci,per_fecha_nacimiento,per_fecha_ingreso,per_cargo,per_estado,per_orden) values 
									('".$_POST['per_nombre']."','".$_POST['per_apellido']."','".$_POST['per_email']."','".$nombre_archivo."','".$_POST['per_telefono']."','".$_POST['per_celular']."','".$_POST['per_direccion']."','".$_POST['per_ci']."','".$_POST['per_fecha_nacimiento']."','".$_POST['per_fecha_ingreso']."','".$_POST['per_cargo']."','".$_POST['per_estado']."','".$orden."')";
				
				if(trim($result)<>'')
				{					
					$this->formulario->ventana_volver($result,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
				}
				else 
				{
					$conec->ejecutar($sql);
					
					$mensaje='Persona Agregada Correctamente!!!';
					
					$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
				}
			}
			else
			{
				$sql="insert into gr_persona(per_nombre,per_apellido,per_email,per_telefono,per_celular,per_direccion,per_ci,per_fecha_nacimiento,per_fecha_ingreso,per_cargo,per_estado,per_orden) values 
									('".$_POST['per_nombre']."','".$_POST['per_apellido']."','".$_POST['per_email']."','".$_POST['per_telefono']."','".$_POST['per_celular']."','".$_POST['per_direccion']."','".$_POST['per_ci']."','".$_POST['per_fecha_nacimiento']."','".$_POST['per_fecha_ingreso']."','".$_POST['per_cargo']."','".$_POST['per_estado']."','".$orden."')";

				$conec->ejecutar($sql);

				$mensaje='Persona Agregada Correctamente!!!';

				$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
			}
		}
		else
		{
			$mensaje='La persona no puede ser agregada, por que existe una persona con ese nombre y apellido.';
			
			$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
		}
	
		
	}
	
	function subir_imagen(&$nombre_imagen,$name,$tmp)
	{	
		 require_once('clases/upload.class.php');

		 $nn=date('d_m_Y_H_i_s_').rand();

		 $upload_class = new Upload_Files();

		 $upload_class->temp_file_name = trim($tmp); 	
		 
		 $upload_class->file_name = $nn.substr(trim($name), -4, 4);

		 $nombre_imagen=$upload_class->file_name;		 		 

		 $upload_class->upload_dir = "imagenes/persona/"; 

		 $upload_class->upload_log_dir = "imagenes/persona/upload_logs/"; 

		 $upload_class->max_file_size = 1048576; 	

		 $upload_class->ext_array = array(".jpg",".gif",".png");

         $upload_class->crear_thumbnail=false;

		 $valid_ext = $upload_class->validate_extension(); 

		 $valid_size = $upload_class->validate_size(); 

		 $valid_user = $upload_class->validate_user(); 

		 $max_size = $upload_class->get_max_size(); 

		 $file_size = $upload_class->get_file_size(); 

		 $file_exists = $upload_class->existing_file(); 		

		if (!$valid_ext) { 				   

			$result = "La Extension de este Archivo es invalida, Intente nuevamente por favor!"; 

		} 

		elseif (!$valid_size) { 

			$result = "El Tama�o de este archivo es invalido, El maximo tama�o permitido es: $max_size y su archivo pesa: $file_size"; 

		}    

		elseif ($file_exists) { 

			$result = "El Archivo Existe en el Servidor, Intente nuevamente por favor."; 

		} 

		else 
		{		    
			$upload_file = $upload_class->upload_file_with_validation(); 

			if (!$upload_file) { 

				$result = "Su archivo no se subio correctamente al Servidor."; 
			}

			else 
			{ 
					$result = "";

					require_once('clases/class.upload.php');

					$mifile='imagenes/persona/'.$upload_class->file_name;

					$handle = new upload($mifile);

					if ($handle->uploaded) 
					{
					    $handle->image_resize          = true;

						$handle->image_ratio           = true;

						$handle->image_y               = 100;

						$handle->image_x               = 100;

						$handle->process('imagenes/persona/chica/');

						if (!($handle->processed)) 
						{
					        echo 'error : ' . $handle->error;
					    }

					}
			} 
		} 	

		return $result;	

	}
	
	function modificar_tcp()
	{
		
			$conec= new ADO();
		
			if($_FILES['per_foto']['name']<>"")
			{	 	 	 	 	 	 	
				 	 	 	 	
				$result=$this->subir_imagen($nombre_archivo,$_FILES['per_foto']['name'],$_FILES['per_foto']['tmp_name']);
				
					$sql="update gr_persona set 
									per_nombre='".$_POST['per_nombre']."',
									per_apellido='".$_POST['per_apellido']."',
									per_email='".$_POST['per_email']."',
									per_foto='".$nombre_archivo."',
									per_telefono='".$_POST['per_telefono']."',
									per_celular='".$_POST['per_celular']."',
									per_direccion='".$_POST['per_direccion']."',
									per_ci='".$_POST['per_ci']."',
									per_fecha_nacimiento='".$_POST['per_fecha_nacimiento']."',
									per_fecha_ingreso='".$_POST['per_fecha_ingreso']."',
									per_cargo='".$_POST['per_cargo']."',
									per_estado='".$_POST['per_estado']."'
									
									where per_id = '".$_GET['id']."'";
							
				
				if(trim($result)<>'')
				{
						
					$this->formulario->ventana_volver($result,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
						
				}
				else
				{
					$llave=$_GET['id'];
					
					$mi=trim($_POST['fotooculta']);
					
					if($mi<>"")
					{
						$mifile="imagenes/persona/$mi";
					
						@unlink($mifile);
						
						$mifile2="imagenes/persona/chica/$mi";
						
						@unlink($mifile2);
					
					}
					
					
					$conec->ejecutar($sql);
				
					$mensaje='Persona Modificada Correctamente!!!';
						
					$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
				}
			}
			else
			{
				
					$sql="update gr_persona set 
									per_nombre='".$_POST['per_nombre']."',
									per_apellido='".$_POST['per_apellido']."',
									per_email='".$_POST['per_email']."',
									per_telefono='".$_POST['per_telefono']."',
									per_celular='".$_POST['per_celular']."',
									per_direccion='".$_POST['per_direccion']."',
									per_ci='".$_POST['per_ci']."',
									per_fecha_nacimiento='".$_POST['per_fecha_nacimiento']."',
									per_fecha_ingreso='".$_POST['per_fecha_ingreso']."',
									per_cargo='".$_POST['per_cargo']."',
									per_estado='".$_POST['per_estado']."'
									where per_id = '".$_GET['id']."'";

				$conec->ejecutar($sql);

				$mensaje='Persona Modificada Correctamente!!!';

				$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
			}
		
	}
	
	function formulario_confirmar_eliminacion()
	{
		
		$mensaje='Esta seguro de eliminar la persona?';
		
		$this->formulario->ventana_confirmacion($mensaje,$this->link."?mod=$this->modulo",'per_id');
	}
	
	function eliminar_tcp()
	{
		$verificar=NEW VERIFICAR;
		
		$parametros[0]=array('usu_per_id');
		$parametros[1]=array($_POST['per_id']);
		$parametros[2]=array('ad_usuario');
		
		if($verificar->validar($parametros))
		{
			$llave=$_POST['per_id'];
					
			$mi=$this->nombre_imagen($llave);
			
			if(trim($mi)<>"")
			{
				$mifile="imagenes/persona/$mi";
			
				@unlink($mifile);
				
				$mifile2="imagenes/persona/chica/$mi";
					
				@unlink($mifile2);
			}
			
			$conec= new ADO();
			
			$sql="delete from gr_persona where per_id='".$_POST['per_id']."'";
			
			$conec->ejecutar($sql);
			
			$mensaje='Persona Eliminada Correctamente!!!';
		}
		else
		{
			$mensaje='La persona no puede ser eliminada, por que esta siendo utilizada en el modulo de Usuarios.';
		}		
		
		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
	}
	
	function nombre_imagen($id)
	{
		$conec= new ADO();
		
		$sql="select per_foto from gr_persona where per_id='".$id."'";
		
		$conec->ejecutar($sql);
		
		$objeto=$conec->get_objeto();
		
		return $objeto->per_foto;
	}
	
	function eliminar_imagen()
	{
		$conec= new ADO();
		
		$mi=$_GET['img'];
		
		$mifile="imagenes/persona/$mi";
				
		@unlink($mifile);
		
		$mifile2="imagenes/persona/chica/$mi";
		
		@unlink($mifile2);
		
		$conec= new ADO();
		
		$sql="update gr_persona set 
						per_foto=''
						where per_id = '".$_GET['id']."'";
						
		$conec->ejecutar($sql);
		
		$mensaje='Imagen Eliminada Correctamente!';
			
		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=MODIFICAR&id='.$_GET['id']);
		
	}
}
?>