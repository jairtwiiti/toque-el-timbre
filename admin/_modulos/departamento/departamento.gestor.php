<?php
	
	require_once('departamento.class.php');
	
	$departamento = new DEPARTAMENTO();
	
	if($_GET['tarea']<>"")
	{
		if(!($departamento->verificar_permisos($_GET['tarea'])))
		{
			?>
			<script>
				location.href="log_out.php";
			</script>
			<?php
		}
	}
	
	switch ($_GET['tarea'])
	{
		case 'AGREGAR':{
					
							if($departamento->datos())
							{
								$departamento->insertar_tcp();
							}
							else 
							{
								$departamento->formulario_tcp('blanco');
							}

						break;}
		case 'VER':{
						$departamento->cargar_datos();
												
						$departamento->formulario_tcp('ver');
						
						break;}
		case 'MODIFICAR':{
						 
                        if($_GET['acc']=='Imagen')
						{
							$departamento->eliminar_imagen();
						}
						else
						{						 
							if($departamento->datos())
							{
								$departamento->modificar_tcp();
							}
							else 
							{
								if(!($_POST))
								{
									$departamento->cargar_datos();
								}
								$departamento->formulario_tcp('cargar');
							}
						 }	
						break;}
		case 'ELIMINAR':{
							
								if(isset($_POST['dep_id']))
								{
									if(trim($_POST['dep_id'])<>"")
									{
										$departamento->eliminar_tcp();
									}
									else 
									{
										$departamento->dibujar_busqueda();
									}
								}
								else 
								{
									$departamento->formulario_confirmar_eliminacion();
								}
							
							
						break;}
		
		case 'CIUDAD':{
						if($_GET['acc']=='MODIFICAR_PRODUCTO')
						{
							if($departamento->datos2())
							{
								$departamento->modificar_producto();
							}
							else
							{
								if(!($_POST))
								{
									$departamento->cargar_datos2();
								}
								$departamento->formulario_tcp2('cargar');
							}
						}
						else						
						{
							if($_GET['acc']=='ELIMINAR')
								{
									$departamento->eliminar_producto($_GET['ciu_id']);
								}
								
								if($departamento->datos2())
								{
									$departamento->guardar_producto();
								}
								
								$departamento->formulario_tcp2('cargar');
								
								$departamento->dibujar_encabezado2();
								
								$departamento->mostrar_busqueda2();
						}
								
								
						break;
					}
		
		default: $departamento->dibujar_busqueda();break;
	}
		
?>