<?php

class DEPARTAMENTO extends BUSQUEDA 
{
	var $formulario;
	var $mensaje;
	
	function DEPARTAMENTO()
	{
		//permisos
		$this->ele_id=138;
		
		$this->busqueda();
		
		if(!($this->verificar_permisos('AGREGAR')))
		{
			$this->ban_agregar=false;
		}
		//fin permisos
		
		$this->num_registros=14;
		
		$this->coneccion= new ADO();

		$this->arreglo_campos[0]["nombre"]="dep_nombre";
		$this->arreglo_campos[0]["texto"]="Nombre";
		$this->arreglo_campos[0]["tipo"]="cadena";
		$this->arreglo_campos[0]["tamanio"]=40;
		
		
		
		$this->link='gestor.php';
		
		$this->modulo='departamento';
		
		$this->formulario = new FORMULARIO();
		
		$this->formulario->set_titulo('DEPARTAMENTO');
		
		
	}
		
	function dibujar_busqueda()
	{		
		$this->formulario->dibujar_cabecera();		
		$this->dibujar_listado();
	}
	
		
	function set_opciones()
	{
				
		$nun=0;
		
		if($this->verificar_permisos('VER'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='VER';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_search.png';
			$this->arreglo_opciones[$nun]["nombre"]='VER';
			$nun++;
		}
		
		if($this->verificar_permisos('MODIFICAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='MODIFICAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_edit.png';
			$this->arreglo_opciones[$nun]["nombre"]='MODIFICAR';
			$nun++;
		}
		
		if($this->verificar_permisos('ELIMINAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='ELIMINAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_drop.png';
			$this->arreglo_opciones[$nun]["nombre"]='ELIMINAR';
			$nun++;
		}
		
		if($this->verificar_permisos('CIUDAD'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='CIUDAD';
			$this->arreglo_opciones[$nun]["imagen"]='images/ciudad.png';
			$this->arreglo_opciones[$nun]["nombre"]='CIUDAD';
			$nun++;
		}
	}
	
	function dibujar_listado()
	{
		$sql="SELECT 
		dep_id,dep_nombre
		FROM 
		departamento
		";
		
		$this->set_sql($sql,'');
		
		$this->set_opciones();
		
		$this->dibujar();
		
	}
	
	function dibujar_encabezado()
	{
		?>
			<tr>
	        	<th>Nombre</th>
	            <th class="tOpciones" width="100px">Opciones</th>
			</tr>
			
		<?PHP
	}
	
	function mostrar_busqueda()
	{
		$conversor = new convertir();
		
		for($i=0;$i<$this->numero;$i++)
			{
				
				$objeto=$this->coneccion->get_objeto();
				echo '<tr>';
					echo "<td>";
						echo $objeto->dep_nombre;
					echo "&nbsp;</td>";
					echo "<td>";
						echo $this->get_opciones($objeto->dep_id);
					echo "</td>";
				echo "</tr>";
				
				$this->coneccion->siguiente();
			}
	}
	
	function cargar_datos()
	{
		$conec=new ADO();
		
		$sql="select * from departamento
				where dep_id = '".$_GET['id']."'";
		
		$conec->ejecutar($sql);
		
		$objeto=$conec->get_objeto();
		
		$_POST['dep_nombre']=$objeto->dep_nombre;		
		$_POST['dep_pai_id']=$objeto->dep_pai_id;		
	}
	
	function datos()
	{
		if($_POST)
		{
			//texto,  numero,  real,  fecha,  mail.
			$num=0;
			$valores[$num]["etiqueta"]="Nombre";
			$valores[$num]["valor"]=$_POST['dep_nombre'];
			$valores[$num]["tipo"]="texto";
			$valores[$num]["requerido"]=true;
			$num++;		
			
			$val=NEW VALIDADOR;
			
			$this->mensaje="";
			
			if($val->validar($valores))
			{
				return true;
			}
				
			else
			{
				$this->mensaje=$val->mensaje;
				return false;
			}
		}
			return false;
	}
	
	function formulario_tcp($tipo)
	{
				switch ($tipo)
				{
					case 'ver':{
								$ver=true;
								break;
								}
							
					case 'cargar':{
								$cargar=true;
								break;
								}
				}
				
				$url=$this->link.'?mod='.$this->modulo;
				
				$red=$url;
				
				if(!($ver))
				{
					$url.="&tarea=".$_GET['tarea'];
				}
				
				if($cargar)
				{
					$url.='&id='.$_GET['id'];
				}

		
		    $this->formulario->dibujar_tarea('DEPARTAMENTO');
		
			if($this->mensaje<>"")
			{
				$this->formulario->mensaje('Error',$this->mensaje);
			}
			?>
		<div id="Contenedor_NuevaSentencia">
			<form id="frm_sentencia" name="frm_sentencia" action="<?php echo $url;?>" method="POST" enctype="multipart/form-data">  
				<div id="FormSent">
					<div class="Subtitulo">Datos</div>
						<div id="ContenedorSeleccion">
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">* </span>Nombre</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto" name="dep_nombre" id="dep_nombre" size="60" maxlength="250" value="<?php echo $_POST['dep_nombre'];?>">
							   </div>
							</div>
							<input type="hidden" name="dep_pai_id" id="dep_pai_id" value="1">
							<!--Fin-->							
							
							
						</div>
						<div id="ContenedorDiv">
						   <div id="CajaBotones">
								<center>
								<?php
								if(!($ver))
								{
									?>
									<input type="submit" class="boton" name="" value="Guardar">
									<input type="reset" class="boton" name="" value="Cancelar">
									<input type="button" class="boton" name="" value="Volver" onclick="javascript:location.href='<?php echo $red;?>';">
									<?php
								}
								else
								{
									?>
									<input type="button" class="boton" name="" value="Volver" onclick="javascript:location.href='<?php echo $red;?>';">
									<?php
								}
								?>
								</center>
						   </div>
						</div>
				</div>
			</form>
		</div>
		<?php
	}
	
	function insertar_tcp()
	{
		$conec= new ADO();
		
		
		
		   $sql="insert into departamento(dep_nombre,dep_pai_id) values 
							('".$_POST['dep_nombre']."','".$_POST['dep_pai_id']."')";

			$conec->ejecutar($sql);
			$mensaje='Departamento Agregado Correctamente!!!';
			$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo);
		
	}
	
		
	function modificar_tcp()
	{
		$conec= new ADO();
		
			
		
			$sql="update departamento set 
							dep_nombre='".$_POST['dep_nombre']."',							
							dep_pai_id='".$_POST['dep_pai_id']."'							
							where dep_id = '".$_GET['id']."'";

			$conec->ejecutar($sql);
			$mensaje='Departamento Modificado Correctamente!!!';
			$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo);
	    
		
	}
	
	function formulario_confirmar_eliminacion()
	{
		
		$mensaje='Esta seguro de eliminar el departamento?';
		
		$this->formulario->ventana_confirmacion($mensaje,$this->link."?mod=$this->modulo",'dep_id');
	}
	
	function eliminar_tcp()
	{
	
	    $llave=$_POST['dep_id'];
	
		$conec= new ADO();
		
		$sql="delete from departamento where dep_id='".$_POST['dep_id']."'";
		
		$conec->ejecutar($sql);
		
		$sql="delete from ciudad where ciu_dep_id='".$_POST['dep_id']."'";
		
		$conec->ejecutar($sql);
		
		$mensaje='Departamento Eliminado Correctamente!!!';
		
		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo);
	}
	
	
	//////////////////////////////////////////////////////////////
	
	function guardar_producto()
	{		
		$conec= new ADO();

		$sql="insert into ciudad(ciu_nombre,ciu_dep_id,ciu_latitud,ciu_longitud) 
		values ('".$_POST['ciu_nombre']."','".$_GET['id']."','".$_POST['ciu_latitud']."','".$_POST['ciu_longitud']."')";

		$conec->ejecutar($sql);

		$this->limpiar();
		
	}

	function limpiar()
	{
		$_POST['ciu_nombre']="";
		$_POST['ciu_latitud']="";
		$_POST['ciu_longitud']="";
	}

	

	function datos2()
	{

		if($_POST)
		{
			require_once('clases/validar.class.php');

			$i=0;
			$valores[$i]["etiqueta"]="Nombre";
			$valores[$i]["valor"]=$_POST['ciu_nombre'];
			$valores[$i]["tipo"]="todo";
			$valores[$i]["requerido"]=true;	

			$val=NEW VALIDADOR;

			$this->mensaje="";

			if($val->validar($valores))
			{
				return true;
			}
			else
			{
				$this->mensaje=$val->mensaje;

				return false;

			}

		}

			return false;

	}

	function formulario_tcp2($tipo)
	{

		$url=$this->link.'?mod='.$this->modulo;

		$re=$url;

		$enlace="<a href='$url'>Departamento</a>";

		echo "<tr>
					<td class='busqueda_enlace' colspan='3'>
						Volver a $enlace
					</td>
				</tr>"; 		

		switch ($tipo)
		{

			case 'ver':{
						$ver=true;
						
						break;
						}

			case 'cargar':{

						$cargar=true;

						break;
						}
		}

		if(!($ver))
		{
			$url.="&tarea=".$_GET['tarea'];
		}

		if($cargar)
		{
			$url.='&id='.$_GET['id'];
		}
		
		if($_GET['acc']=='MODIFICAR_PRODUCTO')
		{
			$url.='&acc=MODIFICAR_PRODUCTO';
		}
		
		$this->formulario->dibujar_tarea('ciudad');
		
		if($this->mensaje<>"")
		{
			$this->formulario->mensaje('Error',$this->mensaje);
		}

		?>
		
		<script type="text/javascript">
		   var map;
		   var markers = [];
		   var geocoder;
		   var zooms = 5;		   
		   function initialize() {
				geocoder = new google.maps.Geocoder();				
				var myLatlng = new google.maps.LatLng(-16.930705,-64.782716); 		
				
				var myOptions = {
					zoom: zooms,
					center: myLatlng,
					mapTypeId: google.maps.MapTypeId.ROADMAP
				}
				
				map = new google.maps.Map(document.getElementById("map"), myOptions);						
				
				<?php
				    if (isset($_POST['ciu_latitud']) && isset($_POST['ciu_longitud']))
				    {				       
				?>
				       var mypoint = new google.maps.LatLng(<? echo $_POST['ciu_latitud']; ?>,<? echo $_POST['ciu_longitud']; ?>);					 
					   createMarker(mypoint);	
					   map.setCenter(mypoint);			
					   map.setZoom(15);	
				<?php
				    }
				?>
				
				google.maps.event.addListener(map, 'click', function(event) {
				
					if  (markers.length < 1)
					{	
						createMarker(event.latLng);							
					}   
					else
					{	
						markers[0].setPosition(event.latLng);
						actualizar_punto(markers[0]);								 
					}	
						
				});
				
				
		   }
		   
		   function actualizar_punto(point)
		   {
		        document.getElementById("latitud").value  =	point.getPosition().lat();	
				document.getElementById("longitud").value = point.getPosition().lng();	
		   }
		   
		   function createMarker(point) {
				
				var clickedLocation = new google.maps.LatLng(point);
				
				markers.push(new google.maps.Marker({
				  position: point,
				  map: map,
				  draggable: true,
				  animation: google.maps.Animation.DROP
				}));  
				
				actualizar_punto(markers[0]);				
				
				google.maps.event.addListener(markers[0], 'dragend', function() {						
					actualizar_punto(markers[0]);							
				});	
				  
			}

			function codeAddress() {
				
				var pais    = document.getElementById("map_pais").value;		
				//var ciudad  = document.getElementById("map_ciudad").value;
				//var tipo  = document.getElementById("map_tipo").value;
				var direccion = document.getElementById("map_direccion").value;
				var address;
				
				
				address = direccion+","+pais;	
				
				
				geocoder.geocode( { 'address': address}, function(results, status) {
				  if (status == google.maps.GeocoderStatus.OK) {
					map.setCenter(results[0].geometry.location);			
					map.setZoom(12);
					if (direccion != "")
					   map.setZoom(15);
					
					if  (markers.length < 1)
					{	
						createMarker(results[0].geometry.location);
					}
					else
					{
						markers[0].setPosition(results[0].geometry.location);
					}			
					
				  } else {
					alert("Geocode was not successful for the following reason: " + status);
				  }
				});
			}	
			
		  
			function loadScript() {
			  var script = document.createElement("script");
			  script.type = "text/javascript";
			  script.src = "http://maps.google.com/maps/api/js?sensor=false&callback=initialize";
			  document.body.appendChild(script);
			}
			  
			window.onload = loadScript;
			
			$(document).ready(function() {
				$('.cerrarToogle').hide();				
				$('.cerrarToogle:first').show();
				$('.prueba').click(function(){
					var cat = $(this).parent().find('.cerrarToogle');
					if(cat.is(':visible')){
						$(cat).hide();
					} else {
						$(cat).slideDown('normal');
					};
				});	
			});
	
</script>

					<div id="Contenedor_NuevaSentencia">

               <form id="frm_sentencia" name="frm_sentencia" action="<?php echo $url.'&ciu_id='.$_GET['ciu_id'];?>" method="POST" enctype="multipart/form-data">  

				<div id="FormSent">
					<div class="Subtitulo">Ciudad</div>

                    <div id="ContenedorSeleccion">
						<!--Inicio-->
						<div id="ContenedorDiv">
						   <div class="Etiqueta" ><span class="flechas1">*</span>Nombre</div>
						   <div id="CajaInput">
						   <input type="text" class="caja_texto" name="ciu_nombre" id="ciu_nombre" size="60" maxlength="250" value="<?php echo $_POST['ciu_nombre'];?>">
						   </div>
						</div>
				        
						<!--Inicio-->
							<br/>
							<fieldset style='overflow:hidden;'>
							<legend class='prueba' id='mapaCargar'>&nbsp;Ubicaci�n&nbsp;</legend>
							<div class='cerrarToogle'>
							
							<div id="ContenedorDiv">							  
							    <div id="CajaInput">
							   <div style="padding:10px 0px 10px 15px">						   
							        <input id="latitud" name="ciu_latitud" type="hidden" value="<?php echo $_POST['ciu_latitud'];?>">
									<input id="longitud" name="ciu_longitud" type="hidden" value = "<?php echo $_POST['ciu_longitud'];?>">									
									<input id="map_pais" name="map_pais" type="hidden" value="Bolivia">
									<table border="0" cellspacing="2">
									    <tr>										
											<td><div style="color: #005C89; font-size:12px" >Direccion</div></td>
											<td>&nbsp;</td>
										</tr>
										<tr> 
											
											<td><input id="map_direccion" type="textbox" size="35" ></td>
											<td>
												<input type="button" class="boton" style="width:100px" value="Buscar Direcci�n" onclick="codeAddress()">
											</td>
										</tr>
									</table>
																		
									
									
									
									
							  </div>
								<div id="map" style="width: 500px; height: 400px;"></div>
							   </div>
							</div>
							<!--Fin-->	
							</div>	
							</fieldset>
						
                    </div>

					<div id="ContenedorDiv">

                           <div id="CajaBotones">

								<center>

								<input type="submit" class="boton" name="" value="Enviar">

							    <input type="reset" class="boton" name="" value="Cancelar">

								<input type="button" class="boton" name="" value="Volver" onclick="location.href='<?php if($_GET['acc']=="MODIFICAR_PRODUCTO") echo $this->link.'?mod='.$this->modulo."&tarea=CIUDAD&id=".$_GET['id']; else echo $this->link.'?mod='.$this->modulo;?>';">

								</center>

						   </div>

                        </div>

                </div>

            </div>					

		<?php
	}

	

	function dibujar_encabezado2()

	{

		?><div style="clear:both;"></div><center>

		<table class="tablaLista" cellpadding="0" cellspacing="0" width="60%">
			<thead>
				<tr>
					<th >

						Nombre

					</th>

					<th class="tOpciones" width="100px">

						Opciones

					</th>

				</tr>
			</thead>
			<tbody>
		<?PHP

	}

	

	function mostrar_busqueda2()
	{

		$conec=new ADO();

		$sql="select 
		ciu_id,ciu_nombre
		from 
		ciudad 
		where 
		ciu_dep_id='".$_GET['id']."' 
		order by 
		ciu_id desc";
		//echo $sql;
		$conec->ejecutar($sql);

		$num=$conec->get_num_registros();

		for($i=0;$i<$num;$i++)
		{
			$objeto=$conec->get_objeto();

			echo '<tr class="busqueda_campos">';

			?>
				<td align="left">

					<?php echo $objeto->ciu_nombre;?>

				</td>

				<td>

					<center>

					<a href="gestor.php?mod=departamento&tarea=CIUDAD&acc=ELIMINAR&ciu_id=<?php echo $objeto->ciu_id;?>&id=<?php echo $_GET['id'];?>"><img src="images/b_drop.png" alt="ELIMINAR" title="ELIMINAR" border="0"></a>

					<a href="gestor.php?mod=departamento&tarea=CIUDAD&acc=MODIFICAR_PRODUCTO&ciu_id=<?php echo $objeto->ciu_id;?>&id=<?php echo $_GET['id'];?>"><img src="images/b_edit.png" alt="MODIFICAR" title="MODIFICAR" border="0"></a>

					</center>

				</td>

			<?php

			echo "</tr>";

			

			

			$conec->siguiente();

		}

		echo "</tbody></table></center><br>";
	}
	
	function eliminar_producto($id_ciudad)
	{

		$conec= new ADO();

		$sql="delete from ciudad where ciu_id='".$id_ciudad."'";

		$conec->ejecutar($sql);
		
		$this->mensaje='Ciudad Eliminada Correctamente';
	

	}
		
	function modificar_producto()
	{		
		$conec= new ADO();

		$mensaje='Ciudad Modificada Correctamente';

		$sql="update ciudad set 
		ciu_nombre='".$_POST['ciu_nombre']."',
		ciu_latitud='".$_POST['ciu_latitud']."',
		ciu_longitud='".$_POST['ciu_longitud']."'
		where ciu_id= '".$_GET['ciu_id']."'";

		$conec->ejecutar($sql);

		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=CIUDAD&id='.$_GET['id']);

	}
	
	function cargar_datos2()
	{
		$conec=new ADO();

		$sql="select * from ciudad where ciu_id = '".$_GET['ciu_id']."'";

		$conec->ejecutar($sql);

		$objeto=$conec->get_objeto();
			
		$_POST['ciu_nombre']=$objeto->ciu_nombre;
		$_POST['ciu_latitud']=$objeto->ciu_latitud;
		$_POST['ciu_longitud']=$objeto->ciu_longitud;
		
		

	}
}
?>