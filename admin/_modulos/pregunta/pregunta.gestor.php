<?php
	
	require_once('pregunta.class.php');
	
	$noticia = new NOTICIA();
	
	if(!($noticia->verificar_permisos($_GET['tarea'])))
	{
		?>
		<script>
			location.href="log_out.php";
		</script>
		<?php
	}
	
	switch ($_GET['tarea'])
	{
		case 'AGREGAR':{
						
							if($noticia->datos())
							{
								$noticia->insertar_tcp();
							}
							else 
							{
								$noticia->formulario_tcp('blanco');
							}
					
						
						
						break;}
		case 'VER':{
						$noticia->cargar_datos();
												
						$noticia->formulario_tcp('ver');
						
						break;}
		case 'MODIFICAR':{
							
							if($noticia->datos())
							{
								$noticia->modificar_tcp();
							}
							else 
							{
								if(!($_POST))
								{
									$noticia->cargar_datos();
								}
								$noticia->formulario_tcp('cargar');
							}
							
						break;}
		case 'ELIMINAR':{
							
								if(isset($_POST['not_id']))
								{
									if(trim($_POST['not_id'])<>"")
									{
										$noticia->eliminar_tcp();
									}
									else 
									{
										$noticia->dibujar_busqueda();
									}
								}
								else 
								{
									$noticia->formulario_confirmar_eliminacion();
								}
							
							
						break;}
		case 'ACCEDER':{
							if($_GET['acc']<>"")
							{
								$noticia->orden($_GET['cli'],$_GET['acc'],$_GET['or']);
							}
							$noticia->dibujar_busqueda();
							break;
						}
		
	}
		
?>