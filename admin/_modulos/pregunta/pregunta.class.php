<?php

class NOTICIA extends BUSQUEDA 
{
	var $formulario;
	var $mensaje;
	
	function NOTICIA()
	{
		//permisos
		$this->ele_id=142;
		
		$this->busqueda();
		
		if(!($this->verificar_permisos('AGREGAR')))
		{
			$this->ban_agregar=false;
		}
		//fin permisos
		
		$this->num_registros=14;
		
		$this->coneccion= new ADO();
		
		$this->arreglo_campos[0]["nombre"]="not_nom";
		$this->arreglo_campos[0]["texto"]="Nombre";
		$this->arreglo_campos[0]["tipo"]="cadena";
		$this->arreglo_campos[0]["tamanio"]=40;
		
		$this->link='gestor.php';
		
		$this->modulo='pregunta';
		
		$this->formulario = new FORMULARIO();
		
		$this->formulario->set_titulo('PREGUNTAS');
		
	}
	
	
	function dibujar_busqueda()
	{
		//echo phpinfo();
		
		$this->formulario->dibujar_cabecera();
		
		$this->dibujar_listado();
	}
	
		
	function set_opciones()
	{
				
		$nun=0;
		
		if($this->verificar_permisos('VER'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='VER';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_search.png';
			$this->arreglo_opciones[$nun]["nombre"]='VER';
			$nun++;
		}
		
		if($this->verificar_permisos('MODIFICAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='MODIFICAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_edit.png';
			$this->arreglo_opciones[$nun]["nombre"]='MODIFICAR';
			$nun++;
		}
		
		if($this->verificar_permisos('ELIMINAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='ELIMINAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_drop.png';
			$this->arreglo_opciones[$nun]["nombre"]='ELIMINAR';
			$nun++;
		}
	}
	
	function dibujar_listado()
	{
		$sql="SELECT * FROM noticia";
		
		$this->set_sql($sql,' order by not_orden desc ');
		
		$this->set_opciones();
		
		$this->dibujar();
		
	}
	
	function dibujar_encabezado()
	{
		?>
			<tr>
	        	<th>Nro</th>
				<th>Pregunta</th>
	            <th>Respuesta</th>
				<th>Nombre</th>
				<th>Estado</th>
				<th>Orden</th>
				<th class="tOpciones" width="100px">Opciones</th>
			</tr>
			
		<?PHP
	}
	
	function mostrar_busqueda()
	{
		$conversor = new convertir();
		
		for($i=0;$i<$this->numero;$i++)
			{
				
				$objeto=$this->coneccion->get_objeto();
				echo '<tr>';
									
					echo "<td>";
						echo ($i+1);
					echo "&nbsp;</td>";
					echo "<td>";
						echo $objeto->not_pre;
					echo "&nbsp;</td>";
					echo "<td>";
						echo $objeto->not_res;
					echo "&nbsp;</td>";
					echo "<td>";
						echo $objeto->not_nom;
					echo "&nbsp;</td>";
					echo "<td>";
						if($objeto->not_estado=='1') echo 'Visible'; else echo 'No Visible';
					echo "</td>";
					echo "<td>";
						?>
						<center><a href="<?php echo $this->link.'?mod='.$this->modulo.'&tarea=ACCEDER&cli='.$objeto->not_id.'&acc=s&or='.$objeto->not_orden;?>"><img src="images/subir.png" border="0"></a><a href="<?php echo $this->link.'?mod='.$this->modulo.'&tarea=ACCEDER&cli='.$objeto->not_id.'&acc=b&or='.$objeto->not_orden;?>"><img src="images/bajarr.png" border="0"></a></center>
						<?php
					echo "</td>";
					echo "<td>";
						echo $this->get_opciones($objeto->not_id);
					echo "</td>";
				echo "</tr>";
				
				$this->coneccion->siguiente();
			}
	}
	
	function orden($noticia,$accion,$ant_orden)
	{
		$conec= new ADO();
		
		if($accion=='s')
			$cad=" where not_orden > $ant_orden order by not_orden asc";
		else
			$cad=" where not_orden < $ant_orden order by not_orden desc";

		$consulta = "
		select 
			not_id,not_orden 
		from 
			noticia
		$cad
		limit 0,1
		";	

		$conec->ejecutar($consulta);

		$num = $conec->get_num_registros();   

		if($num > 0)
		{
			$objeto=$conec->get_objeto();
			
			$nu_orden=$objeto->not_orden;
			
			$id=$objeto->not_id;
			
			$consulta = "update noticia set not_orden='$nu_orden' where not_id='$noticia'";	

			$conec->ejecutar($consulta);
			
			$consulta = "update noticia set not_orden='$ant_orden' where not_id='$id'";	

			$conec->ejecutar($consulta);
		}	
	}
	
	function cargar_datos()
	{
		$conec=new ADO();
		
		$sql="select * from noticia
				where not_id = '".$_GET['id']."'";
		
		$conec->ejecutar($sql);
		
		$objeto=$conec->get_objeto();
		 	 	 	 	 	 	 	 	 
		$_POST['not_titulo']=$objeto->not_titulo;
		
		$_POST['not_imagen']=$objeto->not_imagen;
		
		$_POST['not_descripcion']=$objeto->not_descripcion;
		
		$_POST['not_estado']=$objeto->not_estado;
		
		$_POST['not_fecha']=$objeto->not_fecha;
		
		$_POST['not_resumen']=$objeto->not_resumen;
		
		$_POST['not_nom']=$objeto->not_nom;
		$_POST['not_ema']=$objeto->not_ema;
		$_POST['not_tel']=$objeto->not_tel;
		$_POST['not_pre']=$objeto->not_pre;
		$_POST['not_res']=$objeto->not_res;
		
	}
	
	function datos()
	{
		if($_POST)
		{
			//texto,  numero,  real,  fecha,  mail.
			$num=0;
		/*	if($_GET['tarea']=='AGREGAR')
			{
				$valores[$num]["etiqueta"]="Imagen";
				$valores[$num]["valor"]=$_FILES['not_imagen']['name'];
				$valores[$num]["tipo"]="todo";
				$valores[$num]["requerido"]=true;
				$num++;
			}
			$valores[$num]["etiqueta"]="T�tulo";
			$valores[$num]["valor"]=$_POST['not_titulo'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			$valores[$num]["etiqueta"]="Resumen";
			$valores[$num]["valor"]=$_POST['not_resumen'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			$valores[$num]["etiqueta"]="Descripci�n";
			$valores[$num]["valor"]=$_POST['not_descripcion'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			$valores[$num]["etiqueta"]="Fecha";
			$valores[$num]["valor"]=$_POST['not_fecha'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			$valores[$num]["etiqueta"]="Estado";
			$valores[$num]["valor"]=$_POST['not_estado'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			*/
			
			$val=NEW VALIDADOR;
			
			$this->mensaje="";
			
			if($val->validar($valores))
			{
				return true;
			}
				
			else
			{
				$this->mensaje=$val->mensaje;
				return false;
			}
		}
			return false;
	}
	
	function formulario_tcp($tipo)
	{
				switch ($tipo)
				{
					case 'ver':{
								$ver=true;
								break;
								}
							
					case 'cargar':{
								$cargar=true;
								break;
								}
				}
				
				$url=$this->link.'?mod='.$this->modulo;
				
				$red=$url.'&tarea=ACCEDER';
				
				if(!($ver))
				{
					$url.="&tarea=".$_GET['tarea'];
				}
				
				if($cargar)
				{
					$url.='&id='.$_GET['id'];
				}

		
		    $this->formulario->dibujar_tarea('PREGUNTA');
		
			if($this->mensaje<>"")
			{
				$this->formulario->dibujar_mensaje2($this->mensaje);
			}
			?>
		
		<div id="Contenedor_NuevaSentencia">
			<form id="frm_sentencia" name="frm_sentencia" action="<?php echo $url;?>" method="POST" enctype="multipart/form-data">  
				<div id="FormSent" style="width:800px">
				  
					<div class="Subtitulo">Datos</div>
						<div id="ContenedorSeleccion">
							
							
							
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">* </span>Pregunta</div>
							   <div id="CajaInput">
								  <textarea rows="13" cols="70" name="not_pre" id="not_pre"><?php echo $_POST['not_pre'];?></textarea>									
							   </div>

							</div>
							<!--Fin-->
							
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">* </span>Respuesta</div>
							   <div id="CajaInput">
								  <textarea rows="13" cols="70" name="not_res" id="not_res"><?php echo $_POST['not_res'];?></textarea>									
							   </div>

							</div>
							<!--Fin-->
							
							
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">* </span>Nombre</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto" name="not_nom" id="not_nom" maxlength="250"  size="60" value="<?php echo $_POST['not_nom'];?>">
							   </div>
							</div>
							<!--Fin-->
							
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">* </span>Email</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto" name="not_ema" id="not_ema" maxlength="250"  size="60" value="<?php echo $_POST['not_ema'];?>">
							   </div>
							</div>
							<!--Fin-->
							
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Telefono</div>
							   <div id="CajaInput">
							   <input type="text" class="caja_texto" name="not_tel" id="not_tel" maxlength="250"  size="60" value="<?php echo $_POST['not_tel'];?>">
							   </div>
							</div>
							<!--Fin-->
							
							
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">* </span>Estado</div>
							   <div id="CajaInput">
								    <select name="not_estado" class="caja_texto">
									<option value="" >Seleccione</option>
									<option value="1" <?php if($_POST['not_estado']=='1') echo 'selected="selected"'; ?>>Visible</option>
									<option value="0" <?php if($_POST['not_estado']=='0') echo 'selected="selected"'; ?>>No Visible</option>
									</select>
							   </div>
							</div>
							<!--Fin-->
							
						</div>
					
						<div id="ContenedorDiv">
						   <div id="CajaBotones">
								<center>
								<?php
								if(!($ver))
								{
									?>
									<input type="submit" class="boton" name="" value="Guardar">
									<input type="reset" class="boton" name="" value="Cancelar">
									<input type="button" class="boton" name="" value="Volver" onclick="javascript:location.href='<?php echo $red;?>';">
									<?php
								}
								else
								{
									?>
									<input type="button" class="boton" name="" value="Volver" onclick="javascript:location.href='<?php echo $red;?>';">
									<?php
								}
								?>
								</center>
						   </div>
						</div>
				</div>
				
			</form>
		</div>
		<script>
			textCounter(document.frm_sentencia.not_resumen,document.frm_sentencia.remLen,200);
		</script>
		<?php
	}
	
	function insertar_tcp()
	{
		$conec= new ADO();
		
		$sql=" select max(not_orden) as ultimo from noticia ";
		
		$conec->ejecutar($sql);
		
		$objeto=$conec->get_objeto();
		
		$orden=$objeto->ultimo + 1;
				
		
		
		$sql="insert into noticia(not_nom,not_ema,not_orden,not_estado,not_tel,not_pre,not_res) values 
							('".$_POST['not_nom']."','".$_POST['not_ema']."','$orden','".$_POST['not_estado']."','".$_POST['not_tel']."','".$_POST['not_pre']."','".$_POST['not_res']."')";

		$conec->ejecutar($sql,false);
		$mensaje='Noticia Agregada Correctamente!!!';
		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
	}
	
	function subir_imagen(&$nombre_imagen,$name,$tmp)
	{	
		 require_once('clases/upload.class.php');

		 $nn=date('d_m_Y_H_i_s_').rand();

		 $upload_class = new Upload_Files();

		 $upload_class->temp_file_name = trim($tmp); 	
		 
		 $upload_class->file_name = $nn.substr(trim($name), -4, 4);

		 $nombre_imagen=$upload_class->file_name;		 		 

		  $upload_class->upload_dir = "imagenes/noticia/"; 

		 $upload_class->upload_log_dir = "imagenes/noticia/upload_logs/"; 

		 $upload_class->max_file_size = 1048576; 	

		 $upload_class->ext_array = array(".jpg",".gif",".png");

         $upload_class->crear_thumbnail=false;

		 $valid_ext = $upload_class->validate_extension(); 

		 $valid_size = $upload_class->validate_size(); 

		 $valid_user = $upload_class->validate_user(); 

		 $max_size = $upload_class->get_max_size(); 

		 $file_size = $upload_class->get_file_size(); 

		 $file_exists = $upload_class->existing_file(); 		

		if (!$valid_ext) { 				   

			$result = "La Extension de este Archivo es invalida, Intente nuevamente por favor!"; 

		} 

		elseif (!$valid_size) { 

			$result = "El Tama�o de este archivo es invalido, El maximo tama�o permitido es: $max_size y su archivo pesa: $file_size"; 

		}    

		elseif ($file_exists) { 

			$result = "El Archivo Existe en el Servidor, Intente nuevamente por favor."; 

		} 

		else 
		{		    
			$upload_file = $upload_class->upload_file_with_validation(); 

			if (!$upload_file) { 

				$result = "Su archivo no se subio correctamente al Servidor."; 
			}

			else 
			{ 
					$result = "";
			} 
		} 	

		return $result;	

	}
	
	function modificar_tcp()
	{
		$conec= new ADO();	
		
			$sql="update noticia set 
								not_nom='".$_POST['not_nom']."',
								not_ema='".$_POST['not_ema']."',
								not_tel='".$_POST['not_tel']."',
								not_estado='".$_POST['not_estado']."',
								not_pre='".$_POST['not_pre']."',
								not_res='".$_POST['not_res']."'
								where not_id = '".$_GET['id']."'";
			

			$conec->ejecutar($sql);
			
			$mensaje='Noticia Modificada Correctamente!!!';

			$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
			
	}
	
	function formulario_confirmar_eliminacion()
	{
		
		$mensaje='Esta seguro de eliminar la noticia?';
		
		$this->formulario->ventana_confirmacion($mensaje,$this->link."?mod=$this->modulo".'&tarea=ELIMINAR','not_id');
	}
	
	function eliminar_tcp()
	{
		
		$llave=$_POST['not_id'];
	
		$mi=$this->nombre_imagen($llave);
		
		if(trim($mi)<>"")
		{
			$mifile="imagenes/noticia/$mi";
		
			@unlink($mifile);
			
		}
		
		$conec= new ADO();
		
		$sql="delete from noticia where not_id='".$_POST['not_id']."'";
		
		$conec->ejecutar($sql);
		
		$mensaje='Noticia Eliminada Correctamente!!!';
			
		
		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
	}
	
	function nombre_imagen($id)
	{
		$conec= new ADO();
		
		$sql="select not_imagen from noticia where not_id='".$id."'";
		
		$conec->ejecutar($sql);
		
		$objeto=$conec->get_objeto();
		
		return $objeto->not_imagen;
	}
	
	

	
}
?>