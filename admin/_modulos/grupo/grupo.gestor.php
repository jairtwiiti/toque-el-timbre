<?php
	
	require_once('grupo.class.php');
	
	$grupo = new GRUPO();
	
	if($_GET['tarea']<>"")
	{
		if(!($grupo->verificar_permisos($_GET['tarea'])))
		{
			?>
			<script>
				location.href="log_out.php";
			</script>
			<?php
		}
	}
	
	switch ($_GET['tarea'])
		{
			case 'AGREGAR':{
							if($grupo->datos())
							{
								$grupo->insertar_tcp();
							}
							else 
							{
								$grupo->formulario_tcp('blanco');
							}
							break;}
			case 'VER':{
							$grupo->cargar_datos();
													
							$grupo->formulario_tcp('ver');
							
							break;}
			case 'MODIFICAR':{
							if($grupo->datos())
							{
								$grupo->modificar_tcp();
							}
							else 
							{
								if(!($_POST))
								{
									$grupo->cargar_datos();
								}
								$grupo->formulario_tcp('cargar');
							}
							break;}
									
			case 'ELIMINAR':{
							if(isset($_POST['gru_id']))
							{
								if(trim($_POST['gru_id'])<>"")
								{
									$grupo->eliminar_tcp();
								}
								else 
								{
									$grupo->dibujar_busqueda();
								}
							}
							else 
							{
								$grupo->formulario_confirmar_eliminacion();
							}
								
							break;}
			default: $grupo->dibujar_busqueda();
		}
	
		
		
	
?>