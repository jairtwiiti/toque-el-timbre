<?php
	
	require_once('inmuebles3.class.php');
	require_once('imagenes.class.php');
	$inmueble = new INMUEBLE();
	$imagenes = new IMAGENES();
	
	if($_GET['tarea'] <> "")
	{
		if(!($inmueble->verificar_permisos($_GET['tarea'])))
		{
			?>
			<script>
				location.href="log_out.php";
			</script>
			<?php
		}
	}
	
	switch ($_GET['tarea'])
	{
		case 'AGREGAR':{
						
							if($inmueble->datos())
							{
								$inmueble->insertar_tcp();
							}
							else 
							{
								$inmueble->formulario_tcp('blanco');
							}
					
						
						
						break;}
		case 'VER':{
						$inmueble->cargar_datos();
												
						$inmueble->formulario_tcp('ver');
						
						break;}
		case 'MODIFICAR':{
							if($_GET['acc']=='Imagen')
							{
								$inmueble->eliminar_imagen();
							}
							else
							{
								if($inmueble->datos())
								{
									$inmueble->modificar_tcp();
								}
								else 
								{
									if(!($_POST))
									{
										$inmueble->cargar_datos();
									}
									$inmueble->formulario_tcp('cargar');
								}
							}
						break;}
		case 'ELIMINAR':{
							
								if(isset($_POST['inm_id']))
								{
									if(trim($_POST['inm_id'])<>"")
									{
										$inmueble->eliminar_tcp();
									}
									else 
									{
										$inmueble->dibujar_busqueda();
									}
								}
								else 
								{
									$inmueble->formulario_confirmar_eliminacion();
								}
							
							
						break;}
		case 'ACCEDER':{
		             if($_GET['acc']<>"")
						{
							$inmueble->orden($_GET['pro'],$_GET['acc'],$_GET['or']);
						}
		             $inmueble->dibujar_busqueda();break;
		}
		
		case 'FOTOS':{
						
							if($_GET['acc']=='ELIMINAR')
							{
								$imagenes->eliminar_tcp();
							}
							
							if(!($_GET['acc']<>""))
							{
								if($imagenes->datos())
								{
									$imagenes->insertar_tcp();
								}
								else 
								{
									$imagenes->formulario_tcp('blanco');
									
									$imagenes->dibujar_listado();
								}
																
							}
							
							break;
		}
		
	}
		
?>