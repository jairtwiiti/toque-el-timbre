<?php

class CONTENIDO extends BUSQUEDA 
{
	var $formulario;
	var $mensaje;
	
	function CONTENIDO()
	{
		//permisos
		$this->ele_id=166;
		
		$this->busqueda();
		
		if(!($this->verificar_permisos('AGREGAR')))
		{
			$this->ban_agregar=false;
		}
		//fin permisos
		
		$this->num_registros=25;
		
		$this->coneccion= new ADO();
		
		$this->arreglo_campos[0]["nombre"]="pag_id";
		$this->arreglo_campos[0]["texto"]="ID Transanccion";
		$this->arreglo_campos[0]["tipo"]="cadena";
		$this->arreglo_campos[0]["tamanio"]=40;
		
		$this->link='gestor.php';
		
		$this->modulo='pagos_pendientes';
		
		$this->formulario = new FORMULARIO();
		
		$this->formulario->set_titulo('PAGOS PENDIENTES DE APROBAR');
		
		
	}
	
	
	function dibujar_busqueda()
	{
		
		$this->formulario->dibujar_cabecera();
		
		$this->dibujar_listado();
	}
	
		
	function set_opciones()
	{
				
		$nun=0;
		
		if($this->verificar_permisos('VER'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='VER';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_search.png';
			$this->arreglo_opciones[$nun]["nombre"]='VER';
			$nun++;
		}
		
		if($this->verificar_permisos('MODIFICAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='MODIFICAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_edit.png';
			$this->arreglo_opciones[$nun]["nombre"]='MODIFICAR';
			$nun++;
		}
		
		if($this->verificar_permisos('ELIMINAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='ELIMINAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_drop.png';
			$this->arreglo_opciones[$nun]["nombre"]='ELIMINAR';
			$nun++;
		}
		
		if($this->verificar_permisos('HABILITAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='HABILITAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/ok.png';
			$this->arreglo_opciones[$nun]["nombre"]='HABILITAR';
			$this->arreglo_opciones[$nun]["script"]="ok";
			$nun++;
		}
		
		if($this->verificar_permisos('APROBAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='APROBAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/ok.png';
			$this->arreglo_opciones[$nun]["nombre"]='APROBAR';
			$this->arreglo_opciones[$nun]["script"]="ok";
			$nun++;
		}
	}
	
	
	
	
	
	
	
	
	function verificar_existencia_otros_pagos($pag_id, $pub_id, $ser_id){
		$conec= new ADO();
		$sql = "
		SELECT p.pag_entidad, ps.fech_ini, ps.fech_fin FROM pagos p
		INNER JOIN pago_servicio ps ON (ps.pag_id = p.pag_id AND ps.ser_id = ".$ser_id.")
		WHERE p.pag_pub_id = ".$pub_id." AND p.pag_id <> '".$pag_id."'
		ORDER BY p.pag_fecha DESC LIMIT 1
		";
		$result = $conec->ejecutar($sql);
		$resultado = $conec->get_objeto($result);
		return $resultado->fech_fin;
	}
	
	function obtener_id_publicacion($pag_id){
		$conec= new ADO();
		$sql = "
		SELECT pag_pub_id FROM pagos 
		WHERE pag_id = '".$pag_id."'
		LIMIT 1
		";
		$result = $conec->ejecutar($sql);
		$resultado = $conec->get_objeto($result);
		return $resultado->pag_pub_id;
	}
	
	function obtener_id_servicios($pag_id){
		$conec= new ADO();
		$sql = "
		SELECT ser_id FROM pago_servicio
		WHERE pag_id = '".$pag_id."'
		";
		$result = mysql_query($sql);
		
		while ($row = mysql_fetch_array($result)){
			$resultado[] = $row['ser_id'];
		}
		return $resultado;
	}
	
	function obtener_email_de_pago($pag_id){
		$conec= new ADO();
		$sql = "
		SELECT `usu_email` FROM pagos
		INNER JOIN publicacion ON pub_id = pag_pub_id
		INNER JOIN usuario ON usu_id = pub_usu_id
		WHERE pag_id =  '".$pag_id."'
		";
		$result = mysql_query($sql);
		$email = mysql_fetch_object($result);
		
		return $email->usu_email;
	}
	
	
	function obtener_id_publicacion_estado($pag_id){
		$conec= new ADO();
		$sql = "
		SELECT pag_pub_id FROM pagos 
		WHERE pag_id = '".$pag_id."'
		LIMIT 1
		";
		$result = mysql_query($sql);
		$resultado = mysql_fetch_object($result);
		return $resultado->pag_pub_id;
	}
	
	function obtener_id_inmueble($pub_id){
		$conec= new ADO();
		$sql = "
		SELECT pub_inm_id FROM publicacion 
		WHERE pub_id = '".$pub_id."'
		LIMIT 1
		";
		$result = mysql_query($sql);
		$resultado = mysql_fetch_object($result);
		return $resultado->pub_inm_id;
	}
	
	function aprobar(){
		$conec= new ADO();
		$conec2= new ADO();			
		
		$id_pago = $_GET['id'];
		$codigos_servicios = $this->obtener_id_servicios($id_pago);
		$email = $this->obtener_email_de_pago($id_pago);
		
		
		
		$id_pub2 = $this->obtener_id_publicacion_estado($id_pago);
		$pub_vig_fin=date("Y-m-d" , mktime(0,0,0,date('m'),date('d'),date('Y')) +  2592000 );
		
		$sql="update publicacion set pub_estado='Aprobado', pub_vig_fin='". $pub_vig_fin ."' where pub_id = '".$id_pub2."'";
		//$sql="update publicacion set pub_estado='Aprobado' where pub_id = '".$id_pub2."'";
		$conec->ejecutar($sql);
		$id_inm2 = $this->obtener_id_inmueble($id_pub2);
		$sql="update inmueble set inm_publicado='Si' where inm_id = '".$id_inm2."'";
		$conec->ejecutar($sql);
		
		
		$sql = "UPDATE pagos SET pag_estado = 'Pagado' WHERE pag_id = '" . $id_pago . "'";
		$conec->ejecutar($sql);
		
		$total = 0;
		foreach($codigos_servicios as $ser_id){
			
			if($ser_id != ""){
				$conec3= new ADO();
				$sql = "SELECT * FROM servicios WHERE ser_id = " . $ser_id;
				$conec3->ejecutar($sql);
				
				$obj_servicio = $conec3->get_objeto();
				$num = $conec3->get_num_registros($result);
				$id_pub = $this->obtener_id_publicacion($id_pago);
				
				$aux = $this->verificar_existencia_otros_pagos($id_pago, $id_pub, $ser_id);
				
				if($num > 0 && $aux != ""){					
					//$fech_inicio = $aux;
					$fech_inicio = date("Y-m-d");
				}else{
					$fech_inicio = date("Y-m-d");
				}
				
				$fec_vencimi = date("Y-m-d", strtotime($fech_inicio. " + " . $obj_servicio->ser_dias . " days"));	
				
				$sql = "UPDATE pago_servicio SET fech_ini = '".$fech_inicio."', fech_fin = '".$fec_vencimi."' WHERE pag_id = '" . $id_pago . "' and ser_id = " . $ser_id;					
				$conec2->ejecutar($sql);
				
				$total = $total + $obj_servicio->ser_precio;
			}
		}
		
		$this->enviar_mail($total, $email);
	}
	
	
	function enviar_mail($total,$email)	{	  
		require_once('../clases/phpmailer/class.phpmailer.php');			
	
		$sql="select * from ad_parametro limit 1";		 
		$rs = mysql_query($sql);		    	
		$fila=mysql_fetch_object($rs);			
		$contenido = $this->obtener_contenido("http://www.toqueeltimbre.com/template_email/confirmacion_pago.html");	
		
		$contenido = str_replace('@@monto',$total,$contenido);		
	//	$contenido = str_replace('@@password',$pass,$contenido);	
	
		$cue_nombre=$fila->par_salida; 					
		$cue_pass=$fila->par_pas_salida;				
		$mail = new PHPMailer(true);				
		$mail->SetLanguage('es');		
		$mail->IsSMTP();			
		$mail->SMTPAuth   = true;          
		$mail->Host       = $fila->par_smtp;      			
		$mail->Timeout=30;		
		$mail->CharSet = 'utf-8';				
		$mail->Subject = "Pago Confirmado";		
		$body =  $contenido;					
		$mail->MsgHTML($body);					
		$mail->AddAddress($email);			
		$mail->SetFrom($cue_nombre, $fila->par_smtp);		
		$mail->Username   = $cue_nombre;  	
		$mail->Password   = $cue_pass;		
		try {				
			$mail->Send(); 	          
			return "el mensaje fue enviado correctamente";		
		} catch (phpmailerException $e) {		
			return  "el mensaje no puedo ser enviado";		
		} catch (Exception $e) {			
			return "el mensaje no puedo ser enviado";		
		}			
	}	
	
	function obtener_contenido($url)	{		
	$curl = curl_init();		
	curl_setopt($curl, CURLOPT_HEADER, 0);	
	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);		
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);		
	curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.3) Gecko/20070309 Firefox/2.0.0.3");		
	curl_setopt($curl, CURLOPT_URL, $url);   		
	$contenido = curl_exec($curl); 		
	curl_close($curl); 		
	return $contenido;	
	}	
	
	
	
	
	
	
	function dibujar_listado()
	{
		?>
		<script>		
		function ejecutar_script(id,tarea){
				var txt = 'Esta seguro que desea marcar como pagada esta publicación?';
				
				$.prompt(txt,{ 
					buttons:{Aceptar:true, Cancelar:false},
					callback: function(v,m,f){
						
						if(v){
								location.href='gestor.php?mod=pagos_pendientes&tarea='+tarea+'&id='+id;
						}
												
					}
				});
			}

		</script>
		<?php
		
		$fecha_actual = date("Y-m-d");
	
		$sql="
		SELECT pag_id, pub_id, inm_nombre, pag_fecha, pag_monto, pag_entidad, pag_estado, CONCAT(usu_nombre,' ',usu_apellido) AS nombre, usu_email FROM pagos
		INNER JOIN publicacion ON pub_id = pag_pub_id
		INNER JOIN inmueble ON inm_id = pub_inm_id
		INNER JOIN usuario ON usu_id = pub_usu_id
		where pag_estado = 'Pendiente' and (pag_entidad = 'Oficina' or pag_entidad = 'Banco')
		AND CURDATE() BETWEEN DATE(pag_fecha) AND DATE(pag_fecha_ven)
		";
		
		
		$this->set_sql($sql,' ORDER BY pag_fecha DESC');
		
		$this->set_opciones();
		
		$this->dibujar();
		
	}
	
	function dibujar_encabezado()
	{
		?>
			<tr>
	        	<th>ID TRANSACCION</th>
				<th>Nombre Inmueble</th>
				<th>Nombre Usuario</th>
                <th>Email</th>
				<th>Fecha Pago</th>
				<th>Monto</th>
				<th>Metodo</th>
				<th>Estado</th>
				<th class="tOpciones" width="100px">Opciones</th>
			</tr>
			
		<?PHP
	}
	
	function mostrar_busqueda()
	{
		$conversor = new convertir();
		
		for($i=0;$i<$this->numero;$i++)
			{
				
				$objeto=$this->coneccion->get_objeto();
				echo '<tr>';
									
					echo "<td>";
						echo $objeto->pag_id;
					echo "&nbsp;</td>";
					
					echo "<td>";
						echo $objeto->inm_nombre;
					echo "&nbsp;</td>";
					
					echo "<td>";
						echo $objeto->nombre;
					echo "&nbsp;</td>";
					
					echo "<td>";
						echo $objeto->usu_email;
					echo "&nbsp;</td>";
					
					echo "<td>";
						echo $objeto->pag_fecha;
					echo "&nbsp;</td>";
					
					echo "<td>";
						echo $objeto->pag_monto;
					echo "&nbsp;</td>";
					
					echo "<td>";
						echo $objeto->pag_entidad;
					echo "&nbsp;</td>";
					
					echo "<td>";
						echo $objeto->pag_estado;
					echo "&nbsp;</td>";
					
					echo "<td>";
						echo $this->get_opciones($objeto->pag_id);
					echo "</td>";
				echo "</tr>";
				
				$this->coneccion->siguiente();
			}
	}
	
	function mostrar_detalle_pago(){
		$id_pago = $_GET['id'];
		
		$conec=new ADO();		
		$sql="
		SELECT s.ser_descripcion, s.ser_precio, ps.fech_ini, ps.fech_fin FROM pago_servicio ps 
		INNER JOIN servicios s ON s.ser_id = ps.ser_id
		WHERE ps.pag_id = '".$_GET['id']."'
		";
		$result = mysql_query($sql);
		
		?>
		
		<div id="Contenedor_NuevaSentencia">
			<div id="FormSent" style="width:650px">
				<div class="Subtitulo">Detalle de Pago - <?php echo $_GET['id']; ?></div>
				<div id="ContenedorSeleccion">
				
				<table width="100%" class="tablaLista">
				<thead>
				<tr>
					<th>Servicio</th>
					<th>Fecha Inicio</th>
					<th>Fecha Fin</th>
					<th>Precio</th>
				</tr>
				</thead>
		
		<?php 
		while($row = mysql_fetch_array($result))
		{
			
			?>
			
			<tr>
				<td><?php echo $row['ser_descripcion']?></td>
				<td><?php echo $row['fech_ini']?></td>
				<td><?php echo $row['fech_fin']?></td>
				<td><?php echo $row['ser_precio']; ?></td>
			</tr>
			
			<?php
			$total = $total + $row['ser_precio'];
		}
		?>
				</table>
				
				
				<h2>
					Total: BS. <?php echo $total; ?>
				</h2>
				
				
				</div>
			</div>
		</div>
		
	<?php 
	}
	
	
}
?>