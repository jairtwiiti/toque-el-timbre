<?php
	
	require_once('pagos_pendientes_pagosnet.class.php');
	
	$contenido = new CONTENIDO();
	
	if(!($contenido->verificar_permisos($_GET['tarea'])))
	{
		?>
		<script>
			location.href="log_out.php";
		</script>
		<?php
	}
	
	switch ($_GET['tarea'])
	{
		case 'AGREGAR':{
						
							if($contenido->datos())
							{
								$contenido->insertar_tcp();
							}
							else 
							{
								$contenido->formulario_tcp('blanco');
							}
					
						
						
						break;}
		case 'VER':{
						//$contenido->cargar_datos();
												
						//$contenido->formulario_tcp('ver');
						
						$contenido->mostrar_detalle_pago();
						
						break;}
		case 'MODIFICAR':{
							
							if($contenido->datos())
							{
								$contenido->modificar_tcp();
							}
							else 
							{
								if(!($_POST))
								{
									$contenido->cargar_datos();
								}
								$contenido->formulario_tcp('cargar');
							}
							
						break;}
		case 'ELIMINAR':{
							
								if(isset($_POST['con_id']))
								{
									if(trim($_POST['con_id'])<>"")
									{
										$contenido->eliminar_tcp();
									}
									else 
									{
										$contenido->dibujar_busqueda();
									}
								}
								else 
								{
									$contenido->formulario_confirmar_eliminacion();
								}
							
							
						break;}
		case 'ACCEDER':{
							if($_GET['acc']<>"")
							{
								$contenido->orden($_GET['cli'],$_GET['acc'],$_GET['or']);
							}
							$contenido->dibujar_busqueda();
							break;
						}
						
		case 'HABILITAR':{
						$contenido->aprobar();	
						$contenido->dibujar_busqueda();
						break;
		}
		
		case 'APROBAR':{
						$contenido->aprobar();	
						$contenido->dibujar_busqueda();
						break;
		}
		
	}
		
?>