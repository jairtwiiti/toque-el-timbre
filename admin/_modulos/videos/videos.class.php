<?php

class VIDEOS extends BUSQUEDA 
{
	var $formulario;
	var $mensaje;
	
	function VIDEOS()
	{
		//permisos
		$this->ele_id=143;
		
		$this->busqueda();
		
		if(!($this->verificar_permisos('AGREGAR')))
		{
			$this->ban_agregar=false;
		}
		//fin permisos
		
		$this->num_registros=14;
		
		$this->coneccion= new ADO();
		
		$num=0;
		$this->arreglo_campos[$num]["nombre"]="cli_nombre";
		$this->arreglo_campos[$num]["texto"]="Cliente";
		$this->arreglo_campos[$num]["tipo"]="cadena";
		$this->arreglo_campos[$num]["tamanio"]=20;
		$num++;
		$this->arreglo_campos[$num]["nombre"]="vi_titulo";
		$this->arreglo_campos[$num]["texto"]="Nombre";
		$this->arreglo_campos[$num]["tipo"]="cadena";
		$this->arreglo_campos[$num]["tamanio"]=20;
		
		
		
		
		$this->link='gestor.php';
		
		$this->modulo='videos';
		
		$this->formulario = new FORMULARIO();
		
		$this->formulario->set_titulo('VIDEOS');
	}
	
	
	
	function dibujar_busqueda()
	{
		
		$this->formulario->dibujar_cabecera();
		
		$this->dibujar_listado();
	}
	
	function set_opciones()
	{
		$nun=0;
		
		if($this->verificar_permisos('VER'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='VER';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_search.png';
			$this->arreglo_opciones[$nun]["nombre"]='VER';
			$nun++;
		}
		
		if($this->verificar_permisos('MODIFICAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='MODIFICAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_edit.png';
			$this->arreglo_opciones[$nun]["nombre"]='MODIFICAR';
			$nun++;
		}
		
		if($this->verificar_permisos('ELIMINAR'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='ELIMINAR';
			$this->arreglo_opciones[$nun]["imagen"]='images/b_drop.png';
			$this->arreglo_opciones[$nun]["nombre"]='ELIMINAR';
			$nun++;
		}
		
		if($this->verificar_permisos('FOTOS'))
		{
			$this->arreglo_opciones[$nun]["tarea"]='FOTOS';
			$this->arreglo_opciones[$nun]["imagen"]='images/fotos.png';
			$this->arreglo_opciones[$nun]["nombre"]='FOTOS';
			$nun++;
		}
		
	}
	
	function dibujar_listado()
	{
		$sql=" select vi_id,vi_titulo,vi_orden from videos";

		$this->set_sql($sql,' order by vi_orden desc ');
		
		$this->set_opciones();
		
		$this->dibujar();
		
	}
	
	function dibujar_encabezado()
	{
		?>
			<tr>
				<th>Nombre</th>
				<th>Orden</th>
	            <th class="tOpciones" width="100px">Opciones</th>
			</tr>
		<?PHP
	}
	
	function mostrar_busqueda()
	{
		$conversor = new convertir();
		
		for($i=0;$i<$this->numero;$i++)
			{
				
				$objeto=$this->coneccion->get_objeto();
				echo '<tr>';
					echo "<td>";
						echo $objeto->vi_titulo;
					echo "</td>";
					
					echo "<td>";
						?>
						<center><a href="<?php echo $this->link.'?mod='.$this->modulo.'&tarea=ACCEDER&vi='.$objeto->vi_id.'&acc=s&or='.$objeto->vi_orden;?>"><img src="images/subir.png" border="0"></a><a href="<?php echo $this->link.'?mod='.$this->modulo.'&tarea=ACCEDER&vi='.$objeto->vi_id.'&acc=b&or='.$objeto->vi_orden;?>"><img src="images/bajarr.png" border="0"></a></center>
						<?php
					echo "</td>";
					echo "<td>";
						echo $this->get_opciones($objeto->vi_id);
					echo "</td>";
					
				echo "</tr>";
				
				$this->coneccion->siguiente();
			}
	}
	
	function orden($video,$accion,$ant_orden)
	{
		$conec= new ADO();
		
		if($accion=='s')
			$cad=" where vi_orden > $ant_orden order by vi_orden asc";
		else
			$cad=" where vi_orden < $ant_orden order by vi_orden desc";

		$consulta = "
		select 
			vi_id,vi_orden 
		from 
			videos
		$cad
		limit 0,1
		";	

		$conec->ejecutar($consulta);

		$num = $conec->get_num_registros();   

		if($num > 0)
		{
			$objeto=$conec->get_objeto();
			
			$nu_orden=$objeto->vi_orden;
			
			$id=$objeto->vi_id;
			
			$consulta = "update videos set vi_orden='$nu_orden' where vi_id='$video'";	

			$conec->ejecutar($consulta);
			
			$consulta = "update videos set vi_orden='$ant_orden' where vi_id='$id'";	

			$conec->ejecutar($consulta);
		}	
	}
	
	
	
	
	
	function cargar_datos()
	{
		$conec=new ADO();
		
		$sql="select 
					*
				from 
					videos	
				where 
					vi_id = '".$_GET['id']."'";
		
		$conec->ejecutar($sql);
		
		$objeto=$conec->get_objeto();
		
		$_POST['vi_id']=$objeto->vi_id;
		
		$_POST['vi_titulo']=$objeto->vi_titulo;
		$_POST['vi_resumen']=$objeto->vi_resumen;
		$_POST['vi_descripcion']=$objeto->vi_descripcion;
		$_POST['vi_cat_id']=$objeto->vi_cat_id;
		$_POST['vi_color']=$objeto->vi_color;
		$_POST['vi_fecha']=$objeto->vi_fecha;
			
	}
	
	function datos()
	{
		if($_POST)
		{
			//texto,  numero,  real,  fecha,  mail.
			$num=0;
			
			$valores[$num]["etiqueta"]="Nombre";
			$valores[$num]["valor"]=$_POST['vi_titulo'];
			$valores[$num]["tipo"]="todo";
			$valores[$num]["requerido"]=true;
			$num++;
			
		
			

			
			$val=NEW VALIDADOR;
			
			$this->mensaje="";
			
			if($val->validar($valores))
			{
				return true;
			}
				
			else
			{
				$this->mensaje=$val->mensaje;
				return false;
			}
		}
			return false;
		
	}
	
	function formulario_tcp($tipo)
	{
				
		switch ($tipo)
		{
			case 'ver':{
						$ver=true;
						break;
						}
					
			case 'cargar':{
						$cargar=true;
						break;
						}
		}
		
		$url=$this->link.'?mod='.$this->modulo;
		
		$red=$url.'&tarea=ACCEDER';
		
		if(!($ver))
		{
			$url.="&tarea=".$_GET['tarea'];
		}
		
		if($cargar)
		{
			$url.='&id='.$_GET['id'];
		}
	
		$this->formulario->dibujar_tarea();
		
		if($this->mensaje<>"")
		{
			$this->formulario->dibujar_mensaje($this->mensaje);
		}
		
			?>
			<div id="Contenedor_NuevaSentencia">
			<form id="frm_siniestro" name="frm_siniestro" action="<?php echo $url;?>" method="POST" enctype="multipart/form-data">  
				<div id="FormSent">
						<div class="Subtitulo">Datos</div>
						<div id="ContenedorSeleccion">		
							
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">* </span>Fecha</div>
								 <div id="CajaInput">
								 	<input readonly="readonly" class="caja_texto" name="vi_fecha" id="vi_fecha" size="12" value="<?php echo $_POST['vi_fecha'];?>" type="text">
								 		<input name="but_fecha_pagos" id="but_fecha_pagos" class="boton_fecha" value="..." type="button">
								 		<script type="text/javascript">
								 						Calendar.setup({inputField     : "vi_fecha"
								 										,ifFormat     :     "%Y-%m-%d",
								 										button     :    "but_fecha_pagos"
								 										});
								 		</script>
								 </div>							
							</div>
							<!--Fin-->
							
                            <!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">*</span>Codigo Link</div>
							   <div id="CajaInput">
								  <input type="text" class="caja_texto" name="vi_titulo" id="vi_titulo" size="40" maxlength="250"  value="<?php echo $_POST['vi_titulo'];?>">								
							   </div>
							</div>
							<!--Fin-->
                            
                            <!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" ><span class="flechas1">*</span>Titulo</div>
							   <div id="CajaInput">
								  <input type="text" class="caja_texto" name="vi_resumen" id="vi_resumen" size="40" maxlength="250"  value="<?php echo $_POST['vi_resumen'];?>">								
							   </div>
							</div>
							<!--Fin-->
                            
							<!--Inicio-->
							<div id="ContenedorDiv">
							   <div class="Etiqueta" >Descripcion</div>
							   <div id="CajaInput">
								  <textarea rows="10" cols="50" name="vi_descripcion" id="vi_descripcion"><?php echo $_POST['vi_descripcion'];?></textarea>									
							   </div>

							</div>
							<!--Fin-->
							
							
							</div>
						<div id="ContenedorDiv">
						   <div id="CajaBotones">
								<center>
								<?php
								if((!($ver)))
								{
									?>
									<input type="submit" class="boton" name="" value="Guardar">
									<input type="reset" class="boton" name="" value="Cancelar">
									<input type="button" class="boton" name="" value="Volver" onclick="javascript:location.href='<?php echo $red;?>';">
									<?php
								}
								else
								{
									?>
									<input type="button" class="boton" name="" value="Volver" onclick="javascript:location.href='<?php echo $red;?>';">
									<?php
								}
								?>
								</center>
						   </div>
						</div>
				</div>
			</form>
		</div>
		<?php
	}
	
	function insertar_tcp()
	{
		$conec= new ADO();
		
		$sql=" select max(vi_orden) as ultimo from videos";
		
		$conec->ejecutar($sql);
		
		$objeto=$conec->get_objeto();
		
		$orden=$objeto->ultimo + 1;
		
		$sql="insert into videos(vi_titulo,vi_orden,vi_resumen,vi_descripcion,vi_cat_id,vi_color,vi_fecha) 
		values (
		'".$_POST['vi_titulo']."',
		'".$orden."',
		'".$_POST['vi_resumen']."',
		'".$_POST['vi_descripcion']."',
		'".$_POST['vi_cat_id']."',
		'".$_POST['vi_color']."',
		'".$_POST['vi_fecha']."'
		)";
		
		$conec->ejecutar($sql);
		
		$mensaje='Video Agregado Correctamente';
		
		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
		
	}
	
	function modificar_tcp()
	{
		$conec= new ADO();

		$sql="update videos set 
		vi_titulo='".$_POST['vi_titulo']."',
		vi_resumen='".$_POST['vi_resumen']."',
		vi_descripcion='".$_POST['vi_descripcion']."',
		vi_cat_id='".$_POST['vi_cat_id']."',
		vi_color='".$_POST['vi_color']."',
		vi_fecha='".$_POST['vi_fecha']."'
		where vi_id='".$_GET['id']."'";
		
		
		$conec->ejecutar($sql);
		
		
		$mensaje='Video Modificado Correctamente';
		
		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
	}
	
	function formulario_confirmar_eliminacion()
	{
		
		$mensaje='Esta seguro de eliminar el Video?';
		
		$this->formulario->ventana_confirmacion($mensaje,$this->link."?mod=$this->modulo".'&tarea=ELIMINAR','vi_id');
	}
	
	function eliminar_tcp()
	{
		$conec= new ADO();
		
		$sql="delete from videos where vi_id='".$_POST['vi_id']."'";
		 
		$conec->ejecutar($sql);
		
		$mensaje='Video Eliminado Correctamente';
		
		$this->formulario->ventana_volver($mensaje,$this->link.'?mod='.$this->modulo.'&tarea=ACCEDER');
	}
	
}
?>