<?php
	
	require_once('moneda.class.php');
	
	$moneda = new MONEDA();
	
	if($_GET['tarea']<>"")
	{
		if(!($moneda->verificar_permisos($_GET['tarea'])))
		{
			?>
			<script>
				location.href="log_out.php";
			</script>
			<?php
		}
	}
	
	switch ($_GET['tarea'])
	{
		case 'AGREGAR':{
						if($_GET['acc']=='Emergente')
						{
							$moneda->emergente();
						}
						else
						{
							if($moneda->datos())
							{
								$moneda->insertar_tcp();
							}
							else 
							{
								$moneda->formulario_tcp('blanco');
							}
						}
						
						
						break;}
		case 'VER':{
						$moneda->cargar_datos();
												
						$moneda->formulario_tcp('ver');
						
						break;}
		case 'MODIFICAR':{
							
							if($moneda->datos())
							{
								$moneda->modificar_tcp();
							}
							else 
							{
								if(!($_POST))
								{
									$moneda->cargar_datos();
								}
								$moneda->formulario_tcp('cargar');
							}
							
						break;}
		case 'ELIMINAR':{
							
								if(isset($_POST['mon_id']))
								{
									if(trim($_POST['mon_id'])<>"")
									{
										$moneda->eliminar_tcp();
									}
									else 
									{
										$moneda->dibujar_busqueda();
									}
								}
								else 
								{
									$moneda->formulario_confirmar_eliminacion();
								}
							
							
						break;}
		
		default: $moneda->dibujar_busqueda();break;
	}
		
?>